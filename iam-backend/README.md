# Integrated Adherence Management

Integrated Adherence Management Backend (referred to as iam-backend) is created to abstract out the entity registration and management of adherence across all Everwell services.


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Things to be installed to run IAM

* Java 11
* IntelliJ IDEA / Eclipse or any editor capable of running Java
* Install Maven
* Add Lombok Plugin

For running pure development environment

* Install Postgres
* Install Redis
* Install Rabbitmq

### Installing and Deployment

A step by step series of examples that tell you how to get a development env running

Easy deployment shell script files have been created for easy startup options
The options for env are among dev, beta and prod.
```
sh deployment/<env>/deploy.sh
```
The deployment script builds a new package of the java project and runs the unit tests.
Once the deployment is done, we can look at the log to ascertain the application has started
```
2022-10-25 19:08:58,595 [com.everwell.iam.InitApplication.main():] org.apache.juli.logging.DirectJDKLog.log:173 INFO  - Starting ProtocolHandler ["http-nio-9090"]
2022-10-25 19:08:58,676 [com.everwell.iam.InitApplication.main():] o.s.b.w.e.tomcat.TomcatWebServer.start:204 INFO  - Tomcat started on port(s): 9090 (http) with context path ''
2022-10-25 19:08:58,688 [com.everwell.iam.InitApplication.main():] o.s.boot.StartupInfoLogger.logStarted:59 INFO  - Started InitApplication in 67.048 seconds (JVM running for 82.978)
```

### Building, Compilation and Packaging

For individual compiling of the project, we run the maven commands to build and compile

```
mvn clean compile
```

For packaging of the project in an executable format.
This would execute all the compilation build and test commands before packaging
```
mvn clean compile package
```

For individual running of the project, we run the maven commands

```
mvn spring-boot:run
```

## Running the tests

iam-backend uses JUnit to run tests

Run the following command to evaluate the entire test suite
```
mvn test
```

Run the following command to evaluate individual unit tests
```
mvn test -Dtest=<testfilename>
```

## Built With

* [Springboot](https://spring.io/projects/spring-boot) - The Java framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [Mockito](https://github.com/mockito/mockito) - Used to generate mocks for testing independently
* [PowerMock](https://github.com/powermock/powermock) - Used to mock static, final and other classes that are not supported by mockito.

## License

This project is licensed under the MIT License
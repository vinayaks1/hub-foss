FROM maven:3.6.3-ibmjava-8-alpine as mvncache
WORKDIR /iam-backend
COPY pom.xml .
RUN mvn dependency:go-offline
ARG MVN_PROFILE=dev
COPY src/ /iam-backend/src/
RUN mvn clean compile -P$MVN_PROFILE -DskipTests=true package

FROM openjdk:8-jdk-alpine as runimage
RUN apk update && \
   apk add ca-certificates curl && \
   update-ca-certificates && \
   rm -rf /var/cache/apk/* && \
   addgroup -S appgroup && \
   adduser -S appuser -G appgroup
USER appuser
WORKDIR /home/appuser
COPY --from=mvncache /iam-backend/target/iam-backend-1.0-SNAPSHOT.jar /home/appuser/iam-backend/target/iam-backend-1.0-SNAPSHOT.jar
COPY --from=mvncache /iam-backend/src/main/resources/db/migration/ /home/appuser/iam-backend/src/main/resources/db/migration/
COPY --from=mvncache /iam-backend/src/main/resources/applicationinsights-agent-3.4.4.jar /home/appuser/iam-backend/src/main/resources/applicationinsights-agent-3.4.4.jar
EXPOSE 9090
CMD java -javaagent:"iam-backend/src/main/resources/applicationinsights-agent-3.4.4.jar" -jar iam-backend/target/iam-backend-1.0-SNAPSHOT.jar

# The above images are present in our private repo and can be used instead if the build fails in the future
# registry.gitlab.com/everwell/devops/gitlab-ci/maven:3.6.3-ibmjava-8-alpine
# registry.gitlab.com/everwell/devops/gitlab-ci/openjdk:8-jdk-alpine

package tests.unit.service;

import com.everwell.iam.constants.Constants;
import com.everwell.iam.exceptions.ConflictException;
import com.everwell.iam.models.db.CAccess;
import com.everwell.iam.models.http.requests.RegisterClientRequest;
import com.everwell.iam.models.http.responses.ClientResponse;
import com.everwell.iam.repositories.CAccessRepository;
import com.everwell.iam.services.AuthenticationService;
import com.everwell.iam.services.impl.ClientServiceImpl;
import com.everwell.iam.utils.CacheUtils;
import com.everwell.iam.utils.RandomPasswordGenerator;
import org.apache.commons.text.RandomStringGenerator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import tests.TestUtils;

import java.util.Date;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ClientServiceTest {

    @Autowired
    @InjectMocks
    private ClientServiceImpl clientService;

    @Mock
    private CAccessRepository accessRepository;

    @Mock
    private AuthenticationService authenticationService;

    CacheUtils cacheUtils;

    @Mock
    RedisTemplate<String, Object> redisTemplate;

    @Mock
    private ValueOperations<String, Object> valueOperations;

    private static RegisterClientRequest SAMPLE_CLIENT_REGISRATION_REQUEST;

    @Before
    public void initMocks(){
        cacheUtils = new CacheUtils(redisTemplate);
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
    }

    private RegisterClientRequest getRegisterClientRequest() {
        RandomStringGenerator strGenerator = new RandomStringGenerator.Builder()
                .withinRange('a', 'z')
                .build();
        RandomPasswordGenerator passwordGenerator = new RandomPasswordGenerator.Builder()
                .useDigits(true)
                .build();


        String name = strGenerator.generate(8);
        String password = passwordGenerator.generate(6);
        return new RegisterClientRequest(name, password);
    }

    @Before
    public void setup() {
        // Client Registration
        SAMPLE_CLIENT_REGISRATION_REQUEST = getRegisterClientRequest();
    }

    @Test
    public void testClientCanBeCreatedSuccessfully() {
        Long mockId = 1L;
        String mockSecureToken = "secureEnough";

        Mockito.when(accessRepository.findByName(SAMPLE_CLIENT_REGISRATION_REQUEST.getName())).thenReturn(null);
        Mockito.when(accessRepository.save(Mockito.any(CAccess.class))).thenAnswer(new Answer<CAccess>() {
            @Override
            public CAccess answer(InvocationOnMock invocation) throws Throwable {
                CAccess access = (CAccess) invocation.getArguments()[0];
                access.setId(mockId);
                return access;
            }
        });
        Mockito.when(authenticationService.issueSecureToken(Mockito.anyString(), Mockito.anyString())).thenReturn(mockSecureToken);

        ClientResponse response = clientService.registerClient(SAMPLE_CLIENT_REGISRATION_REQUEST);
        Assert.assertEquals(response.getId(), mockId);
        Assert.assertEquals(response.getName(), SAMPLE_CLIENT_REGISRATION_REQUEST.getName());
        Assert.assertEquals(response.getPassword(), SAMPLE_CLIENT_REGISRATION_REQUEST.getPassword());
        Assert.assertEquals(response.getAuthToken(), mockSecureToken);
        Assert.assertTrue(response.getNextRefresh() > (new Date()).getTime());
        TestUtils.print("Successfully passed testClientCanBeCreatedSuccessfully");
    }

    @Test(expected = ConflictException.class)
    public void testClientCannotBeCreatedAgain() {
        Mockito.when(accessRepository.findByName(SAMPLE_CLIENT_REGISRATION_REQUEST.getName())).thenReturn(new CAccess());

        clientService.registerClient(SAMPLE_CLIENT_REGISRATION_REQUEST);
        TestUtils.print("Successfully passed testClientCannotBeCreatedAgain");
    }

    @Test()
    public void testNewAccessTokenAndRefreshTimeIsGeneratedOnOldGet() {
        Long mockClientId = 1L;
        String mockSecureToken = "secureEnough";
        CAccess oldAccess = new CAccess("abc", "def", "ijk", (new Date()).getTime() - Constants.REFRESH_EXPIRY);
        Mockito.when(accessRepository.findById(1L)).thenReturn(Optional.of(oldAccess));
        Mockito.when(accessRepository.save(Mockito.any(CAccess.class))).thenAnswer(new Answer<CAccess>() {
            @Override
            public CAccess answer(InvocationOnMock invocation) throws Throwable {
                CAccess access = (CAccess) invocation.getArguments()[0];
                access.setId(mockClientId);
                return access;
            }
        });
        Mockito.when(authenticationService.issueSecureToken(Mockito.anyString(), Mockito.anyString())).thenReturn(mockSecureToken);
        ClientResponse response = clientService.getClient(mockClientId);
        Assert.assertEquals(response.getName(), oldAccess.getName());
        Assert.assertEquals(response.getAuthToken(), oldAccess.getAccessToken());
        Assert.assertTrue(response.getNextRefresh() > (new Date()).getTime());
        Assert.assertTrue(oldAccess.getUpdatedDate().after(oldAccess.getCreatedDate()));
        verify(valueOperations, Mockito.times(1)).set(anyString(), any(), anyLong(), any());
    }
}

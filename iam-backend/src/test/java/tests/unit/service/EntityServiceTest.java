package tests.unit.service;

import com.everwell.iam.exceptions.ConflictException;
import com.everwell.iam.exceptions.NotFoundException;
import com.everwell.iam.exceptions.ValidationException;
import com.everwell.iam.models.db.AccessMapping;
import com.everwell.iam.models.http.responses.EntityResponse;
import com.everwell.iam.repositories.AccessMappingRepository;
import com.everwell.iam.repositories.RegistrationRepository;
import com.everwell.iam.services.EntityService;
import com.everwell.iam.utils.Utils;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import tests.BaseTest;

import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class EntityServiceTest extends BaseTest {

    @Autowired
    private EntityService entityService;

    @MockBean
    private AccessMappingRepository accessMappingRepository;

    @MockBean
    private RegistrationRepository registrationRepository;

    private static Long clientId = 4L;
    private static String entityId = "1234";
    private static String uniqueIdentifier = "5678";
    private static Long iamId = 1L;
    private static String imei= "123456789012345";

    @Test
    public void testRegisterEntitySuccess() {
        EntityResponse entityResponse = entityService.registerEntity(entityId, iamId, clientId);
        Assert.assertEquals(entityResponse.getIamId(), iamId);
    }

    @Test(expected = ConflictException.class)
    public void testValidateIamMappingWithDateActiveEntityAlreadyExists() {
        Long iamId = 5L;
        String entityId = "29";
        String startDate = "2019-07-22 00:00:00";
        AccessMapping dummyAccessMapping = new AccessMapping();
        dummyAccessMapping.setActive(true);
        when(accessMappingRepository.findActiveMappingByEntityIdAndClientIdAndDate(any(),any(), any())).thenReturn(dummyAccessMapping);

        entityService.validateIamMappingWithDate(iamId, entityId, startDate);
    }

    @Test
    public void testValidateIamMappingWithDate_Valid() {
        Date startDate = new Date();
        when(accessMappingRepository.findActiveMappingByEntityIdAndClientIdAndDate(entityId, clientId, startDate)).thenReturn(null);

        entityService.validateIamMappingWithDate(iamId, entityId, Utils.getFormattedDate(startDate));
    }

    @Test
    public void testToggleActiveAlreadyActive() {
        Long iamId = 5L;
        Long clientId = 29L;
        AccessMapping dummyAccessMapping = new AccessMapping();
        dummyAccessMapping.setActive(true);
        when(accessMappingRepository.getByIamIdInAndClientId(any(),any())).thenReturn(Arrays.asList(dummyAccessMapping));
        Assert.assertEquals(entityService.toggleActiveStatus(iamId, clientId, true), true);
    }

    @Test
    public void testValidateActiveIamMappingByEntityIdTrue() {
        String entityId = "5";
        Long clientId = 29L;
        AccessMapping dummyAccessMapping = new AccessMapping();
        dummyAccessMapping.setActive(true);
        when(accessMappingRepository.getByClientIdAndEntityIdInAndActiveIsTrue(any(),any())).thenReturn(Arrays.asList(dummyAccessMapping));

        boolean valid = entityService.validateActiveIamMappingByEntityId(clientId, entityId);
        Assert.assertTrue(valid);
    }

    @Test
    public void testGetEntityByIamIds() {
        String entityId = "1";
        Long clientId = 9L;
        List<Long> iamIds = Arrays.asList(1L, 2L);

        when(accessMappingRepository.getEntityIdByIamIdInAndClientIdAndActiveIsTrue(iamIds, clientId)).thenReturn(Arrays.asList(entityId));
        List<String> activeEntityId = entityService.getEntityByIamIds(iamIds, clientId);

        Assert.assertNotNull(activeEntityId);
        Assert.assertEquals(activeEntityId.get(0),entityId);
    }

    @Test
    public void testToggleInactiveAlreadyInactive() {
        Long iamId = 5L;
        Long clientId = 29L;
        AccessMapping dummyAccessMapping = new AccessMapping();
        dummyAccessMapping.setActive(false);
        when(accessMappingRepository.getByIamIdInAndClientId(any(),any())).thenReturn(Arrays.asList(dummyAccessMapping));
        Boolean success = entityService.toggleActiveStatus(iamId, clientId, false);
        Assert.assertEquals(success, true);
    }

    @Test
    public void testValidateActiveIamMappingTrue() {
        Long iamId = 5L;
        Long clientId = 29L;
        when(accessMappingRepository.findActiveAccessMappingByIamIdAndClientId(iamId, clientId)).thenReturn(new AccessMapping());

        Assert.assertTrue(entityService.validateActiveIamMapping(clientId, iamId));
    }

    @Test
    public void testValidateActiveIamMappingFalse() {
        Long iamId = 5L;
        Long clientId = 29L;
        when(accessMappingRepository.findActiveAccessMappingByIamIdAndClientId(iamId, clientId)).thenReturn(null);

        Assert.assertTrue(!entityService.validateActiveIamMapping(clientId, iamId));
    }

    @Test
    public void testSetActiveSuccess() {
        Long iamId = 5L;
        Long clientId = 29L;
        AccessMapping dummyAccessMapping = new AccessMapping();
        dummyAccessMapping.setActive(false);
        when(accessMappingRepository.getByIamIdInAndClientId(any(),any())).thenReturn(Arrays.asList(dummyAccessMapping));

        entityService.toggleActiveStatus(iamId, clientId, true);
        Assert.assertTrue(dummyAccessMapping.isActive());
    }

    @Test
    public void testSetActiveSuccess_Deactivate() {
        Long iamId = 5L;
        Long clientId = 29L;
        AccessMapping dummyAccessMapping = new AccessMapping();
        dummyAccessMapping.setActive(true);
        when(accessMappingRepository.getByIamIdInAndClientId(any(),any())).thenReturn(Arrays.asList(dummyAccessMapping));

        entityService.toggleActiveStatus(iamId, clientId, false);
        Assert.assertFalse(dummyAccessMapping.isActive());
    }

    @Test
    public void testGetMultipleEntityMappingSuccess() {
        List<AccessMapping> dummyMappingList = Arrays.asList(
                new AccessMapping(1L, 1L,"5"),
                new AccessMapping(1L, 2L,"5"),
                new AccessMapping(1L, 3L,"2"),
                new AccessMapping(1L, 4L,"2")
        );
        when(accessMappingRepository.findByEntityIdIn(any())).thenReturn(dummyMappingList);

        Map<String, List<AccessMapping>> multiEntityMappingList = entityService.getMultiEntityMapping(Arrays.asList());
        Assert.assertTrue(multiEntityMappingList.containsKey("5"));
        Assert.assertTrue(multiEntityMappingList.containsKey("2"));
        Assert.assertTrue(multiEntityMappingList.size() == 2);
        Assert.assertTrue(multiEntityMappingList.get("5").size() == 2);
        Assert.assertTrue(multiEntityMappingList.get("2").size() == 2);
    }

    @Test
    public void testGetMultipleEntityMappingSuccess_EmptyMappings() {
        when(accessMappingRepository.findByEntityIdIn(any())).thenReturn(new ArrayList<>());

        Map<String, List<AccessMapping>> multiEntityMappingList = entityService.getMultiEntityMapping(Arrays.asList());
        Assert.assertTrue(multiEntityMappingList.size() == 0);
    }

    @Test
    public void testGetAllIamMapping()
    {
        List<AccessMapping> accessMappings = new ArrayList<AccessMapping>(){
            {
                add(new AccessMapping(clientId, iamId, entityId));
            }
        };
        when(accessMappingRepository.getAllByEntityIdAndClientId(entityId, clientId)).thenReturn(accessMappings);

        List<AccessMapping> iamMappings = entityService.getAllIamMapping(clientId, entityId);
        Assert.assertEquals(iamMappings.get(0).getClientId(), clientId);
        Assert.assertEquals(iamMappings.get(0).getIamId(), iamId);
        Assert.assertEquals(iamMappings.get(0).getEntityId(), entityId);
    }

    @Test
    public void testGetIamMappingByEntityId_EntityIdList()
    {
        List<AccessMapping> accessMappings = new ArrayList<AccessMapping>(){
            {
                add(new AccessMapping(clientId, iamId, entityId));
            }
        };

        when(accessMappingRepository.getAllByEntityIdInAndClientId(Collections.singletonList(entityId), clientId)).thenReturn(accessMappings);
        List<AccessMapping> iamMappings = entityService.getIamMappingByEntityId(clientId, Collections.singletonList(entityId));
        Assert.assertEquals(iamMappings.get(0).getClientId(), clientId);
        Assert.assertEquals(iamMappings.get(0).getIamId(), iamId);
        Assert.assertEquals(iamMappings.get(0).getEntityId(), entityId);
    }

    @Test
    public void testGetIamMappingByEntityId_EntityId()
    {
        List<AccessMapping> accessMappings = new ArrayList<AccessMapping>(){
            {
                add(new AccessMapping(clientId, iamId, entityId));
            }
        };

        when(accessMappingRepository.getAllByEntityIdInAndClientId(Collections.singletonList(entityId), clientId)).thenReturn(accessMappings);
        List<AccessMapping> iamMappings = entityService.getIamMappingByEntityId(clientId, entityId);
        Assert.assertEquals(iamMappings.get(0).getClientId(), clientId);
        Assert.assertEquals(iamMappings.get(0).getIamId(), iamId);
        Assert.assertEquals(iamMappings.get(0).getEntityId(), entityId);
    }

    @Test
    public void testValidateMappingIamIdList_Valid()
    {
        List<AccessMapping> accessMappings = new ArrayList<AccessMapping>(){
            {
                add(new AccessMapping(clientId, iamId, entityId));
            }
        };
        when(accessMappingRepository.getByIamIdInAndClientId(Collections.singletonList(iamId), clientId)).thenReturn(accessMappings);

        boolean isValid = entityService.validateMapping(Collections.singletonList(iamId), clientId);
        Assert.assertTrue(isValid);
    }

    @Test
    public void testValidateMappingIamIdList_Invalid()
    {
        List<AccessMapping> accessMappings = new ArrayList<AccessMapping>(){
            {
                add(new AccessMapping(clientId, iamId, entityId));
                add(new AccessMapping(1L, 1L, "1"));
            }
        };
        when(accessMappingRepository.getByIamIdInAndClientId(Collections.singletonList(iamId), clientId)).thenReturn(accessMappings);

        boolean isValid = entityService.validateMapping(Collections.singletonList(iamId), clientId);
        Assert.assertFalse(isValid);
    }

    @Test
    public void testValidateMappingIamIdList_NullMapping()
    {
        when(accessMappingRepository.getByIamIdInAndClientId(Collections.singletonList(iamId), clientId)).thenReturn(null);

        boolean isValid = entityService.validateMapping(Collections.singletonList(iamId), clientId);
        Assert.assertFalse(isValid);
    }

    @Test
    public void testValidateMappingIamId_Valid()
    {
        List<AccessMapping> accessMappings = new ArrayList<AccessMapping>(){
            {
                add(new AccessMapping(clientId, iamId, entityId));
            }
        };
        when(accessMappingRepository.getByIamIdInAndClientId(Collections.singletonList(iamId), clientId)).thenReturn(accessMappings);

        boolean isValid = entityService.validateMapping(iamId, clientId);
        Assert.assertTrue(isValid);
    }

    @Test
    public void testValidateMappingIamId_Invalid()
    {
        List<AccessMapping> accessMappings = new ArrayList<AccessMapping>(){
            {
                add(new AccessMapping(clientId, iamId, entityId));
                add(new AccessMapping(1L, 1L, "1"));
            }
        };
        when(accessMappingRepository.getByIamIdInAndClientId(Collections.singletonList(iamId), clientId)).thenReturn(accessMappings);

        boolean isValid = entityService.validateMapping(iamId, clientId);
        Assert.assertFalse(isValid);
    }

    @Test
    public void testValidateMappingIamId_NullMapping()
    {
        when(accessMappingRepository.getByIamIdInAndClientId(Collections.singletonList(iamId), clientId)).thenReturn(null);

        boolean isValid = entityService.validateMapping(iamId, clientId);
        Assert.assertFalse(isValid);
    }

    @Test
    public void testValidateActiveIamMappingByEntityIdList_Valid()
    {
        List<AccessMapping> accessMappings = new ArrayList<AccessMapping>(){
            {
                add(new AccessMapping(clientId, iamId, entityId));
            }
        };
        when(accessMappingRepository.getByClientIdAndEntityIdInAndActiveIsTrue(clientId, Collections.singletonList(entityId))).thenReturn(accessMappings);

        boolean isValid = entityService.validateActiveIamMappingByEntityId(clientId, Collections.singletonList(entityId));
        Assert.assertTrue(isValid);
    }

    @Test
    public void testValidateActiveIamMappingByEntityIdList_Invalid()
    {
        List<AccessMapping> accessMappings = new ArrayList<>();
        when(accessMappingRepository.getByClientIdAndEntityIdInAndActiveIsTrue(clientId, Collections.singletonList(entityId))).thenReturn(accessMappings);

        boolean isValid = entityService.validateActiveIamMappingByEntityId(clientId, Collections.singletonList(entityId));
        Assert.assertFalse(isValid);
    }

    @Test
    public void testValidateActiveIamMappingByEntityIdList_NullMapping()
    {
        when(accessMappingRepository.getByClientIdAndEntityIdInAndActiveIsTrue(clientId, Collections.singletonList(entityId))).thenReturn(null);

        boolean isValid = entityService.validateActiveIamMappingByEntityId(clientId, Collections.singletonList(entityId));
        Assert.assertFalse(isValid);
    }

    @Test
    public void testValidateActiveIamMappingByEntityId_Valid()
    {
        List<AccessMapping> accessMappings = new ArrayList<AccessMapping>(){
            {
                add(new AccessMapping(clientId, iamId, entityId));
            }
        };
        when(accessMappingRepository.getByClientIdAndEntityIdInAndActiveIsTrue(clientId, Collections.singletonList(entityId))).thenReturn(accessMappings);

        boolean isValid = entityService.validateActiveIamMappingByEntityId(clientId, entityId);
        Assert.assertTrue(isValid);
    }

    @Test
    public void testValidateActiveIamMappingByEntityId_Invalid()
    {
        List<AccessMapping> accessMappings = new ArrayList<>();
        when(accessMappingRepository.getByClientIdAndEntityIdInAndActiveIsTrue(clientId, Collections.singletonList(entityId))).thenReturn(accessMappings);

        boolean isValid = entityService.validateActiveIamMappingByEntityId(clientId, entityId);
        Assert.assertFalse(isValid);
    }

    @Test
    public void testValidateActiveIamMappingByEntityId_NullMapping()
    {
        when(accessMappingRepository.getByClientIdAndEntityIdInAndActiveIsTrue(clientId, Collections.singletonList(entityId))).thenReturn(null);

        boolean isValid = entityService.validateActiveIamMappingByEntityId(clientId, entityId);
        Assert.assertFalse(isValid);
    }

    @Test(expected = NotFoundException.class)
    public void testValidateNoActiveIamMappingByEntityId_EntityNotFound_NullMapping()
    {
        when(accessMappingRepository.getAllByEntityIdInAndClientId(Collections.singletonList(entityId), clientId)).thenReturn(null);

        entityService.validateNoActiveIamMappingByEntityId(clientId, entityId);
    }

    @Test(expected = NotFoundException.class)
    public void testValidateNoActiveIamMappingByEntityId_EntityNotFound_EmptyMappings()
    {
        when(accessMappingRepository.getAllByEntityIdInAndClientId(Collections.singletonList(entityId), clientId)).thenReturn(new ArrayList<>());

        entityService.validateNoActiveIamMappingByEntityId(clientId, entityId);
    }

    @Test(expected = ConflictException.class)
    public void testValidateNoActiveIamMappingByEntityId_ActiveEntityExists()
    {
        List<AccessMapping> accessMappings = new ArrayList<AccessMapping>(){
            {
                add(new AccessMapping(1L, clientId, iamId, entityId, true));
            }
        };
        when(accessMappingRepository.getAllByEntityIdInAndClientId(Collections.singletonList(entityId), clientId)).thenReturn(accessMappings);

        entityService.validateNoActiveIamMappingByEntityId(clientId, entityId);
    }

    @Test
    public void testValidateNoActiveIamMappingByEntityId_Valid()
    {
        List<AccessMapping> accessMappings = new ArrayList<AccessMapping>(){
            {
                add(new AccessMapping(1L, clientId, iamId, entityId, false));
            }
        };
        when(accessMappingRepository.getAllByEntityIdInAndClientId(Collections.singletonList(entityId), clientId)).thenReturn(accessMappings);

        entityService.validateNoActiveIamMappingByEntityId(clientId, entityId);
    }

    @Test(expected = ValidationException.class)
    public void testGetActiveIamMappingUsingDate_InvalidDateFormat()
    {
        entityService.getActiveIamMappingUsingDate(clientId, entityId, "2020-08-31");
    }

    @Test
    public void testGetIamMapping_EmptyMappings()
    {
        when(accessMappingRepository.getByIamIdInAndClientId(Collections.singletonList(iamId), clientId)).thenReturn(new ArrayList<>());

        AccessMapping mapping = entityService.getIamMapping(clientId, iamId);
        Assert.assertNull(mapping);
    }

    @Test
    public void testGetIamMappingByIMEI_EntityNotFound_NullMapping(){
        when(accessMappingRepository.findEntityIdByIMEIAndClientId(imei, clientId)).thenReturn(new AccessMapping());
        AccessMapping mapping = entityService.getIamMappingByIMEI(imei, clientId);
        Assert.assertNull(mapping.getEntityId());
    }
}

package tests.unit.service;

import com.everwell.iam.exceptions.ConflictException;
import com.everwell.iam.exceptions.NotFoundException;
import com.everwell.iam.handlers.impl.MERMHandler;
import com.everwell.iam.handlers.impl.NNDHandler;
import com.everwell.iam.handlers.impl.NNDLiteHandler;
import com.everwell.iam.models.db.CAccess;
import com.everwell.iam.services.ClientService;
import com.everwell.iam.services.impl.AdherenceServiceImpl;
import com.everwell.iam.services.impl.EntityServiceImpl;
import com.everwell.iam.services.impl.EventServiceConsumerServiceImpl;
import com.rabbitmq.client.Channel;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import tests.BaseTestNoSpring;
import tests.unit.handlersTest.MermHandlerTest;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

public class EventServiceConsumerServiceTest extends BaseTestNoSpring {

    @Spy
    @InjectMocks
    EventServiceConsumerServiceImpl eventServiceConsumerService;

    @Mock
    NNDHandler nndHandler;

    @Mock
    NNDLiteHandler nndLiteHandler;

    @Mock
    EntityServiceImpl entityService;

    @Mock
    AdherenceServiceImpl adherenceService;

    @Mock
    ClientService clientService;

    @Mock
    MERMHandler mermHandler;

    @Test
    public void processCall_Sucess() throws IOException {
        String jsonPayload = "{\"EventName\":\"process_call\",\"Field\":{\"numberDialed\":\"1\",\"caller\":\"1\",\"utcDateTime\":\"2019-01-18 18:30:30\",\"postBody\":\"body\"}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        eventServiceConsumerService.processCallConsumer(message);
        Mockito.verify(nndHandler, Mockito.times(1)).processIncomingCall(any());
        Mockito.verify(nndLiteHandler, Mockito.times(1)).processIncomingCall(any());
    }

    @Test(expected = NotFoundException.class)
    public void registerEntity_NoClientId() throws IOException {
        String jsonPayload = "{\"EventName\":\"process_call\",\"Field\":{\"entityId\":\"10010\",\"adherenceType\":1,\"uniqueIdentifier\":\"9939139\",\"startDate\":\"2017-10-1318:30:00\",\"phoneNumbers\":[{\"phoneNumber\":\"9939139\"}]}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        eventServiceConsumerService.registerEntityConsumer(message);
    }

    @Test(expected = ConflictException.class)
    public void registerEntity_ActiveEntityExists() throws IOException {
        String jsonPayload = "{\"EventName\":\"process_call\",\"Field\":{\"clientId\":\"29\",\"entityId\":\"10010\",\"adherenceType\":1,\"uniqueIdentifier\":\"9939139\",\"startDate\":\"2017-10-1318:30:00\",\"phoneNumbers\":[{\"phoneNumber\":\"9939139\"}]}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        when(entityService.validateActiveIamMappingByEntityId(any(), anyString())).thenReturn(true);
        when(clientService.getClientById(any())).thenReturn(new CAccess());
        eventServiceConsumerService.registerEntityConsumer(message);
    }

    @Test(expected = ConflictException.class)
    public void registerEntity_ScheduleExists() throws IOException {
        String jsonPayload = "{\"EventName\":\"process_call\",\"Field\":{\"clientId\":\"29\",\"entityId\":\"10010\",\"adherenceType\":1,\"uniqueIdentifier\":\"9939139\",\"startDate\":\"2017-10-1318:30:00\",\"phoneNumbers\":[{\"phoneNumber\":\"9939139\"}]}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        when(adherenceService.hasScheduleMappings(any())).thenReturn(true);
        when(adherenceService.activeScheduleMappingExistsForEntityIdAndClientId(any(), anyString(), anyLong())).thenReturn(true);
        when(clientService.getClientById(any())).thenReturn(new CAccess());
        eventServiceConsumerService.registerEntityConsumer(message);
    }

    @Test
    public void registerEntity_Success() throws IOException {
        String jsonPayload = "{\"EventName\":\"process_call\",\"Field\":{\"clientId\":\"29\",\"entityId\":\"10010\",\"adherenceType\":1,\"uniqueIdentifier\":\"9939139\",\"startDate\":\"2017-10-1318:30:00\",\"phoneNumbers\":[{\"phoneNumber\":\"9939139\"}]}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        when(adherenceService.createIam(any())).thenReturn(1L);
        when(adherenceService.hasScheduleMappings(any())).thenReturn(false);
        when(entityService.validateActiveIamMappingByEntityId(any(), anyString())).thenReturn(false);
        when(clientService.getClientById(any())).thenReturn(new CAccess());
        eventServiceConsumerService.registerEntityConsumer(message);
        Mockito.verify(entityService, Mockito.times(1)).registerEntity(any(), any(), any());
    }

    @Test
    public void processMermEvent_Success() throws IOException {
        String jsonPayload = "{\"EventName\":\"q.iam.process_merm_event\",\"Field\":{\"sn\":\"@=02,CN=10810,SN=869587035557806,SI=0,T=100920181934,TU=100920204934,TT=G,DT=2,B=3900,V=03.13,S=31\",\"eventDateTime\":\"190320090000\"}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        eventServiceConsumerService.processMermEventConsumer(message);
        Mockito.verify(mermHandler, Mockito.times(1)).processIncomingEvent(any());
    }

    @Test
    public void testRegenListenerConcurrency() throws InterruptedException {
        String jsonPayload = "{\"eventName\":\"q.iam.regenerate_entity\",\"field\":{\"iamId\":123}}";
        Message message = new Message(jsonPayload.getBytes(), new MessageProperties());
        int numberOfThreads = 20;
        ExecutorService service = Executors.newFixedThreadPool(10);
        CountDownLatch latch = new CountDownLatch(numberOfThreads);
        doNothing().when(eventServiceConsumerService).processRegenList();
        for (int i = 0; i < numberOfThreads; i++) {
            service.submit(() -> {
                try {
                    eventServiceConsumerService.processRegenerateEntity(message);
                } catch (IOException ignored) {}
                latch.countDown();
            });
        }
        latch.await();
        assertEquals(numberOfThreads, eventServiceConsumerService.iamIdListToRegenerate.size());
    }
}

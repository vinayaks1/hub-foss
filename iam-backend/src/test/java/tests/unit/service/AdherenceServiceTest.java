package tests.unit.service;

import com.everwell.iam.constants.Constants;
import com.everwell.iam.enums.*;
import com.everwell.iam.exceptions.ConflictException;
import com.everwell.iam.exceptions.NotFoundException;
import com.everwell.iam.exceptions.ValidationException;
import com.everwell.iam.handlers.AdhTechHandlerMap;
import com.everwell.iam.handlers.ScheduleTypeHandlerMap;
import com.everwell.iam.handlers.impl.*;
import com.everwell.iam.models.db.AccessMapping;
import com.everwell.iam.models.db.CAccess;
import com.everwell.iam.models.db.Registration;
import com.everwell.iam.models.db.Schedule;
import com.everwell.iam.models.db.ScheduleMap;
import com.everwell.iam.models.dto.*;
import com.everwell.iam.models.dto.AdherenceData;
import com.everwell.iam.models.dto.PositiveAdherenceDto;
import com.everwell.iam.models.dto.RangeFilters;
import com.everwell.iam.models.http.requests.AdherenceGlobalConfigRequest;
import com.everwell.iam.models.http.requests.EntityRequest;
import com.everwell.iam.models.http.requests.RecordAdherenceRequest;
import com.everwell.iam.models.http.responses.*;
import com.everwell.iam.repositories.AccessMappingRepository;
import com.everwell.iam.repositories.AdhStringLogRepository;
import com.everwell.iam.repositories.RegistrationRepository;
import com.everwell.iam.services.ClientService;
import com.everwell.iam.services.impl.AdherenceServiceImpl;
import com.everwell.iam.utils.CacheUtils;
import com.everwell.iam.utils.Utils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.util.CollectionUtils;
import tests.BaseTest;
import tests.unit.handlersTest.AdherenceTestHandler;

import java.math.BigInteger;
import java.security.InvalidParameterException;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.groupingBy;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class AdherenceServiceTest extends BaseTest {

  @Spy
  @InjectMocks
  private AdherenceServiceImpl adherenceService;

  @MockBean
  private RegistrationRepository registrationRepository;

  @MockBean
  private AccessMappingRepository accessMappingRepository;

  @MockBean
  private AdhStringLogRepository adherenceStringLog;

  @MockBean
  private AdhTechHandlerMap adherenceTechHandlerMap;

  @MockBean
  private ScheduleTypeHandlerMap scheduleTypeHandlerMap;

  @MockBean
  private ClientService clientService;

  @Mock
  DailyScheduleHandler mockDailyHandler;

  @Mock
  WeeklyScheduleHandler mockWeeklyHandler;

  @Mock
  MultiFreqWeeklyScheduleHandler multiFreqWeeklyScheduleHandler;

  @SpyBean
  NoneHandler noneHandler;

  @SpyBean
  NNDHandler nndHandler;

  @Spy
  @InjectMocks
  AdherenceTestHandler adherenceTestHandler;

  CacheUtils cacheUtils;

  @Mock
  RedisTemplate<String, Object> redisTemplate;

  @Mock
  private ValueOperations<String, Object> valueOperations;


  @Before
  public void initMocks(){
    List<AccessMapping> accessMappingList = new ArrayList<>();
    AccessMapping access= new AccessMapping(1L,1L,"123");
    accessMappingList.add(access);
    adherenceTestHandler = Mockito.spy(new AdherenceTestHandler());
    MockitoAnnotations.initMocks(this);
    cacheUtils = new CacheUtils(redisTemplate);
    when(redisTemplate.opsForValue()).thenReturn(valueOperations);
    when(accessMappingRepository.getByIamId(any())).thenReturn(accessMappingList);
    when(adherenceStringLog.saveAll(any())).thenReturn(new ArrayList<>());
    when(adherenceTechHandlerMap.getHandler(any())).thenReturn(adherenceTestHandler);
    when(registrationRepository.save(any())).thenReturn(new Registration());
  }

  //Todo:This test should move inside the handler tests
  @Test
  public void checkCorrectAdherenceStringOnMultiDateRecord() throws ParseException {
    // +530hr
    Registration dummyRegistration1 = new Registration(1L, 1L, Utils.convertStringToDate("2018-12-31 18:30:00"), Utils.convertStringToDate("2019-01-04 18:30:00"), "1", "6666", Utils.convertStringToDate("2019-01-01 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"));
    dummyRegistration1.setScheduleTypeId(1L);
    Registration dummyRegistration2 = new Registration(2L, 5L, Utils.convertStringToDate("2019-01-04 18:30:00"), Utils.convertStringToDate("2019-01-11 18:30:00"), "1", "6666666", Utils.convertStringToDate("2019-01-01 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"));
    dummyRegistration2.setScheduleTypeId(1L);
    when(registrationRepository.findByIdIn(any())).thenReturn(Arrays.asList(dummyRegistration1, dummyRegistration2));

    RecordAdherenceRequest adherenceRequest = new RecordAdherenceRequest();

    Map<String, Character> dmvMap = new HashMap<>();

    dmvMap.put("2019-01-02 18:30:00.000000", '4');
    dmvMap.put("2019-01-03 18:30:00.000000", '5');
    dmvMap.put("2019-01-04 18:30:00.000000", '4');
    dmvMap.put("2019-01-05 18:30:00.000000", '5');

    adherenceRequest.setDatesToValueMapList(dmvMap);

    adherenceRequest.setEntityId("1");
    when(adherenceTechHandlerMap.getHandler(5L)).thenReturn(noneHandler);
    when(adherenceTechHandlerMap.getHandler(1L)).thenReturn(nndHandler);
    when(scheduleTypeHandlerMap.getHandler(eq(1L))).thenReturn(mockDailyHandler);
    when(mockDailyHandler.isLogsRequired()).thenReturn(false);
    doNothing().when(mockDailyHandler).shouldTakeDoseForDates(any(), any(), any());
    when(mockDailyHandler.getTransformedValue(eq('5'), any(), anyLong(), any(), any(), any())).thenReturn('5');
    when(mockDailyHandler.getTransformedValue(eq('4'), any(), anyLong(), any(), any(), any())).thenReturn('4');
    when(adherenceStringLog.findAllByIamAndStartDate(eq(dummyRegistration2.getStartDate()), eq(dummyRegistration2.getId()))).thenReturn(new ArrayList<>());
    CAccess cAccess = new CAccess();
    when(clientService.getClientById(anyLong())).thenReturn(cAccess);
    adherenceService.recordAdherence(adherenceRequest, Arrays.asList(dummyRegistration1.getId(), dummyRegistration2.getId()));

    assertEquals("6645", dummyRegistration1.getAdherenceString());
    assertEquals("4566666", dummyRegistration2.getAdherenceString());
  }

  //Todo:This test should move inside the handler tests
  @Test
  public void checkCorrectAdherenceStringOnSingleDataRecordOnActiveReg() throws ParseException {
    // -9 hr
    Date dummyStartDate = Utils.convertStringToDate("2019-09-18 15:00:00");
    int diffStart = (int) Utils.getDifferenceDays(dummyStartDate, Utils.getCurrentDate()) + 1;
    String dummyAdherenceString = IntStream.range(0, diffStart).mapToObj(i -> String.valueOf(AdherenceCodeEnum.NO_INFO.getCode())).collect(Collectors.joining(""));
    Registration dummyRegistration1 = new Registration(1L, 1L, dummyStartDate, null, "1", dummyAdherenceString, Utils.convertStringToDate("2019-01-01 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"));
    dummyRegistration1.setScheduleTypeId(1L);
    when(registrationRepository.findByIdIn(any())).thenReturn(Arrays.asList(dummyRegistration1));

    RecordAdherenceRequest adherenceRequest = new RecordAdherenceRequest();
    Map<String, Character> dmvMap = new HashMap<>();
    dmvMap.put("2019-09-20 15:00:00.000000", '6');
    dmvMap.put("2019-09-20 15:01:00.000000", '9');
    dmvMap.put("2019-09-20 15:02:00.000000", '6');
    dmvMap.put("2019-09-20 15:03:00.000000", '9');
    adherenceRequest.setDatesToValueMapList(dmvMap);

    adherenceRequest.setEntityId("1");
    when(adherenceTechHandlerMap.getHandler(1L)).thenReturn(nndHandler);
    when(scheduleTypeHandlerMap.getHandler(eq(1L))).thenReturn(mockDailyHandler);
    when(mockDailyHandler.isLogsRequired()).thenReturn(false);
    doNothing().when(mockDailyHandler).shouldTakeDoseForDates(any(), any(), any());
    when(mockDailyHandler.getTransformedValue(eq('6'), any(), anyLong(), any(), any(), any())).thenReturn('6');
    when(mockDailyHandler.getTransformedValue(eq('9'), any(), anyLong(), any(), any(), any())).thenReturn('9');

    CAccess cAccess = new CAccess();
    when(clientService.getClientById(anyLong())).thenReturn(cAccess);
    adherenceService.recordAdherence(adherenceRequest, Arrays.asList(1L));

    Date expectedDate = Utils.convertStringToDate("2019-09-20 15:00:56");
    int diffRecord = (int) Utils.getDifferenceDays(expectedDate, Utils.getCurrentDate()) + 1;
    //1 character less as the record day is already accounted for in the adh string
    String expectedAdherenceString = "669" + IntStream.range(1, diffRecord).mapToObj(i -> String.valueOf(AdherenceCodeEnum.NO_INFO.getCode())).collect(Collectors.joining(""));

    assertEquals(expectedAdherenceString, dummyRegistration1.getAdherenceString());
  }

  //Todo:This test should move inside the handler tests
  @Test
  public void checkCorrectAdherenceStringRecordPrecedenceOnSingleData() throws ParseException {
    // -9 hr
    Date dummyStartDate = Utils.convertStringToDate("2019-09-18 15:00:00");
    int diffStart = (int) Utils.getDifferenceDays(dummyStartDate, Utils.getCurrentDate()) + 1;
    String dummyAdherenceString = IntStream.range(0, diffStart).mapToObj(i -> String.valueOf(AdherenceCodeEnum.NO_INFO.getCode())).collect(Collectors.joining(""));
    Registration dummyRegistration1 = new Registration(1L, 1L, dummyStartDate, null, "1", dummyAdherenceString, Utils.convertStringToDate("2019-01-01 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"));
    dummyRegistration1.setScheduleTypeId(1L);

    when(registrationRepository.findByIdIn(any())).thenReturn(Arrays.asList(dummyRegistration1));

    RecordAdherenceRequest adherenceRequest = new RecordAdherenceRequest();
    Map<String, Character> dmvMap = new HashMap<>();
    dmvMap.put("2019-09-20 15:00:00.000000", '6');
    dmvMap.put("2019-09-20 15:01:00.000000", '4');
    dmvMap.put("2019-09-20 15:02:00.000000", '6');
    dmvMap.put("2019-09-20 15:03:00.000000", '9');
    adherenceRequest.setDatesToValueMapList(dmvMap);

    adherenceRequest.setEntityId("1");
    when(adherenceTechHandlerMap.getHandler(1L)).thenReturn(nndHandler);
    when(scheduleTypeHandlerMap.getHandler(eq(1L))).thenReturn(mockDailyHandler);
    when(mockDailyHandler.isLogsRequired()).thenReturn(false);
    doNothing().when(mockDailyHandler).shouldTakeDoseForDates(any(), any(), any());
    when(mockDailyHandler.getTransformedValue(eq('4'), any(), anyLong(), any(), any(), any())).thenReturn('4');
    when(mockDailyHandler.getTransformedValue(eq('6'), any(), anyLong(), any(), any(), any())).thenReturn('6');
    when(mockDailyHandler.getTransformedValue(eq('9'), any(), anyLong(), any(), any(), any())).thenReturn('9');
    CAccess cAccess = new CAccess();
    when(clientService.getClientById(anyLong())).thenReturn(cAccess);
    adherenceService.recordAdherence(adherenceRequest, Arrays.asList(1L));

    Date expectedDate = Utils.convertStringToDate("2019-09-20 15:00:56");
    int diffRecord = (int) Utils.getDifferenceDays(expectedDate, Utils.getCurrentDate()) + 1;
    //1 character less as the record day is already accounted for in the adh string
    String expectedAdherenceString = "664" + IntStream.range(1, diffRecord).mapToObj(i -> String.valueOf(AdherenceCodeEnum.NO_INFO.getCode())).collect(Collectors.joining(""));

    assertEquals(expectedAdherenceString, dummyRegistration1.getAdherenceString());
  }

  //Todo:This test should move inside the handler tests
  @Test
  public void checkCorrectAdherenceStringOnSingleDateWithoutDVMapForActiveIam() throws ParseException {
    // +9 hrs
    String dummyStartDateString = "2019-09-19 21:00:00";
    String recordDateString = "2019-09-21 21:00:00";
    String dummyEntityId = "1";

    Date dummyStrtDate = Utils.convertStringToDate(dummyStartDateString);
    int diffStart = (int) Utils.getDifferenceDays(dummyStrtDate, Utils.getCurrentDate()) + 1;
    String dummyAdherenceString = IntStream.range(0, diffStart).mapToObj(__ -> String.valueOf(AdherenceCodeEnum.NO_INFO.getCode())).collect(Collectors.joining(""));
    Registration dummyRegistration1 = new Registration(1L, 5L, dummyStrtDate, null, "1", dummyAdherenceString, Utils.convertStringToDate("2019-01-01 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"));
    dummyRegistration1.setScheduleTypeId(1L);
    when(adherenceTechHandlerMap.getHandler(5L)).thenReturn(noneHandler);
    when(registrationRepository.findByIdIn(any())).thenReturn(Arrays.asList(dummyRegistration1));
    when(scheduleTypeHandlerMap.getHandler(eq(1L))).thenReturn(mockDailyHandler);
    when(mockDailyHandler.isLogsRequired()).thenReturn(false);
    doNothing().when(mockDailyHandler).shouldTakeDoseForDates(any(), any(), any());
    when(mockDailyHandler.getTransformedValue(eq('5'), any(), anyLong(), any(), any(), any())).thenReturn('5');

    RecordAdherenceRequest adherenceRequest = new RecordAdherenceRequest('5', 5L);
    adherenceRequest.setDate(recordDateString);
    adherenceRequest.setEntityId(dummyEntityId);

    Date expectedDate = Utils.convertStringToDate(recordDateString);
    int diffRecord = (int) Utils.getDifferenceDays(expectedDate, Utils.getCurrentDate()) + 1;
    //1 character less as the record day is already accounted for in the changed adh string
    String expectedAdherenceString = "665" + IntStream.range(1, diffRecord).mapToObj(i -> String.valueOf(AdherenceCodeEnum.NO_INFO.getCode())).collect(Collectors.joining(""));
    CAccess cAccess = new CAccess();
    when(clientService.getClientById(anyLong())).thenReturn(cAccess);
    adherenceService.recordAdherence(adherenceRequest, Arrays.asList(1L));

    assertEquals(expectedAdherenceString, dummyRegistration1.getAdherenceString());
  }

  //Todo:This test should move inside the handler tests
  @Test
  public void checkCorrectAdherenceStringOnSingleDateWithoutDVMapForPassedIam() throws ParseException {
    Registration dummyRegistration1 = new Registration(1L, 5L, Utils.convertStringToDate("2019-09-11 13:00:00"), Utils.convertStringToDate("2019-09-17 13:00:00"), "1", "666666", Utils.convertStringToDate("2019-01-01 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"));
    dummyRegistration1.setScheduleTypeId(1L);

    when(registrationRepository.findByIdIn(any())).thenReturn(Arrays.asList(dummyRegistration1));
    when(adherenceTechHandlerMap.getHandler(5L)).thenReturn(noneHandler);
    when(scheduleTypeHandlerMap.getHandler(eq(1L))).thenReturn(mockDailyHandler);
    when(mockDailyHandler.isLogsRequired()).thenReturn(false);
    doNothing().when(mockDailyHandler).shouldTakeDoseForDates(any(), any(), any());
    when(mockDailyHandler.getTransformedValue(eq('5'), any(), anyLong(), any(), any(), any())).thenReturn('5');
    CAccess cAccess = new CAccess();
    when(clientService.getClientById(anyLong())).thenReturn(cAccess);
    RecordAdherenceRequest adherenceRequest = new RecordAdherenceRequest('5', 5L);
    adherenceRequest.setDate("2019-09-13 13:52:56.000000");
    adherenceRequest.setEntityId("1");

    adherenceService.recordAdherence(adherenceRequest, Arrays.asList(1L));

    assertEquals("665666", dummyRegistration1.getAdherenceString());
  }

  //Todo:This test should move inside the handler tests
  @Test
  public void checkCorrectAdherenceStringOnSingleDataRecordOnInactiveReg() throws ParseException {
    Registration dummyRegistration1 = new Registration(1L, 1L, Utils.convertStringToDate("2019-09-11 13:00:00"), Utils.convertStringToDate("2019-09-17 13:00:00"), "1", "666666", Utils.convertStringToDate("2019-01-01 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"));
    dummyRegistration1.setScheduleTypeId(1L);
    when(registrationRepository.findByIdIn(any())).thenReturn(Arrays.asList(dummyRegistration1));
    when(adherenceTechHandlerMap.getHandler(1L)).thenReturn(nndHandler);
    when(scheduleTypeHandlerMap.getHandler(eq(1L))).thenReturn(mockDailyHandler);
    when(mockDailyHandler.isLogsRequired()).thenReturn(false);
    doNothing().when(mockDailyHandler).shouldTakeDoseForDates(any(), any(), any());
    when(mockDailyHandler.getTransformedValue(eq('9'), any(), anyLong(), any(), any(), any())).thenReturn('9');
    when(mockDailyHandler.getTransformedValue(eq('6'), any(), anyLong(), any(), any(), any())).thenReturn('6');


    RecordAdherenceRequest adherenceRequest = new RecordAdherenceRequest();
    Map<String, Character> dmvMap = new HashMap<>();
    dmvMap.put("2019-09-13 13:52:56.000000", '6');
    dmvMap.put("2019-09-13 14:52:56.000000", '9');
    dmvMap.put("2019-09-13 15:52:56.000000", '6');
    dmvMap.put("2019-09-13 16:52:56.000000", '9');
    adherenceRequest.setDatesToValueMapList(dmvMap);

    adherenceRequest.setEntityId("1");
    CAccess cAccess = new CAccess();
    when(clientService.getClientById(anyLong())).thenReturn(cAccess);
    adherenceService.recordAdherence(adherenceRequest, Arrays.asList(1L));
    assertEquals("669666", dummyRegistration1.getAdherenceString());
  }

  //Todo:This test should move inside the handler tests
  @Test
  public void checkCorrectAdherenceStringOnMultiDateRecordOnSameDate() throws ParseException {
    Registration dummyRegistration1 = new Registration(1L, 1L, Utils.convertStringToDate("2018-12-31 21:00:00"), Utils.convertStringToDate("2019-01-06 21:00:00"), "1", "666666", Utils.convertStringToDate("2019-01-01 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"));
    dummyRegistration1.setScheduleTypeId(1L);
    when(registrationRepository.findByIdIn(any())).thenReturn(Arrays.asList(dummyRegistration1));
    when(adherenceTechHandlerMap.getHandler(1L)).thenReturn(nndHandler);
    when(scheduleTypeHandlerMap.getHandler(eq(1L))).thenReturn(mockDailyHandler);
    when(mockDailyHandler.isLogsRequired()).thenReturn(false);
    doNothing().when(mockDailyHandler).shouldTakeDoseForDates(any(), any(), any());
    when(mockDailyHandler.getTransformedValue(eq('9'), any(), anyLong(), any(), any(), any())).thenReturn('9');
    when(mockDailyHandler.getTransformedValue(eq('6'), any(), anyLong(), any(), any(), any())).thenReturn('6');

    RecordAdherenceRequest adherenceRequest = new RecordAdherenceRequest();
    Map<String, Character> dmvMap = new HashMap<>();
    dmvMap.put("2019-01-02 22:52:56.000000", '6');
    dmvMap.put("2019-01-02 23:52:56.000000", '9');
    dmvMap.put("2019-01-03 00:52:56.000000", '6');
    dmvMap.put("2019-01-03 01:52:56.000000", '9');
    adherenceRequest.setDatesToValueMapList(dmvMap);

    adherenceRequest.setEntityId("1");

    CAccess cAccess = new CAccess();
    when(clientService.getClientById(anyLong())).thenReturn(cAccess);
    adherenceService.recordAdherence(adherenceRequest, Arrays.asList(1L));

    assertEquals("669666", dummyRegistration1.getAdherenceString());
  }

  @Test
  public void testNoLogsReturnedWhenGetAdherenceWithFlagFalse() throws ParseException {
    Registration dummyRegistration1 = new Registration(1L, 1L, Utils.convertStringToDate("2018-12-31 21:00:00"), Utils.convertStringToDate("2019-01-06 21:00:00"), "1", "666666", Utils.convertStringToDate("2019-01-01 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"));
    when(registrationRepository.findById(eq(1L))).thenReturn(Optional.of(dummyRegistration1));
    when(adherenceTechHandlerMap.getHandler(any())).thenReturn(adherenceTestHandler);
    when(adherenceTestHandler.getAdhTechEnumerator()).thenReturn(AdherenceTech.VOT);
    when(adherenceTestHandler.getTechLogs(1L)).thenReturn(new ArrayList<>());
    when(adherenceTestHandler.getScheduleHandler(1L)).thenReturn(mockDailyHandler);
    when(scheduleTypeHandlerMap.getHandler(1L)).thenReturn(mockDailyHandler);
    when(mockDailyHandler.getScheduleTypeEnumerator()).thenReturn(ScheduleTypeEnum.DAILY);
    AdherenceResponse adherenceResponse = adherenceService.getAdherence(1L, false);
    Assert.assertTrue(CollectionUtils.isEmpty(adherenceResponse.getAdherenceData().get(0).getAdhTechLogsData()));
  }

  @Test(expected = NotFoundException.class)
  public void testExceptionWhenNoRegistration() throws ParseException {
    when(registrationRepository.findById(eq(1L))).thenReturn(Optional.empty());
    when(adherenceTechHandlerMap.getHandler(any())).thenReturn(adherenceTestHandler);

    adherenceService.getAdherence(1L, false);
  }

  @Test
  public void lastDosageFilterTest() throws ParseException, NumberFormatException {
    List<AdherenceResponse> adherenceResponses = new ArrayList<>();
    adherenceResponses.add(new AdherenceResponse("1", null, "666666", Utils.convertStringToDate("2019-01-01 01:00:00"),0, 0, 0, null,null,null, 0,0, new ArrayList<>()));
    adherenceResponses.add(new AdherenceResponse("1", null, "666666", Utils.convertStringToDate("2019-01-05 01:00:00"),0, 0, 0, null,null,null, 0,0, new ArrayList<>()));
    adherenceResponses.add(new AdherenceResponse("1", null, "666666", Utils.convertStringToDate("2019-01-10 01:00:00"),0, 0, 0, null,null,null, 0,0, new ArrayList<>()));

    RangeFilters rangeFilters = new RangeFilters(RangeFilterType.LAST_DOSAGE, "2019-01-01 17:00:00", "2019-01-10 01:00:00");
    assertEquals(1, adherenceService.filterList(adherenceResponses, rangeFilters).size());

    rangeFilters = new RangeFilters(RangeFilterType.LAST_DOSAGE, "2019-01-01 00:00:00", "2019-01-10 01:00:00");
    assertEquals(2, adherenceService.filterList(adherenceResponses, rangeFilters).size());

    rangeFilters = new RangeFilters(RangeFilterType.LAST_DOSAGE, "2019-01-02 00:00:00", null);
    assertEquals(2, adherenceService.filterList(adherenceResponses, rangeFilters).size());

    rangeFilters = new RangeFilters(RangeFilterType.LAST_DOSAGE, null, "2019-01-09 01:00:00");
    assertEquals(2, adherenceService.filterList(adherenceResponses, rangeFilters).size());

    rangeFilters = new RangeFilters(RangeFilterType.LAST_DOSAGE, null, null);
    assertEquals(3, adherenceService.filterList(adherenceResponses, rangeFilters).size());

    rangeFilters = new RangeFilters(RangeFilterType.LAST_DOSAGE, "2019-01-11 00:00:00", "2019-01-12 01:00:00");
    assertEquals(0, adherenceService.filterList(adherenceResponses, rangeFilters).size());

    rangeFilters = new RangeFilters(RangeFilterType.LAST_DOSAGE, "2019-01-07 01:00:00", "2019-01-10 01:00:00");
    assertEquals(0, adherenceService.filterList(adherenceResponses, rangeFilters).size());

    adherenceResponses.add(new AdherenceResponse("1", null, "666666", null,0, 0, 0, null,null,null, 0,0, new ArrayList<>()));
    assertEquals(0, adherenceService.filterList(adherenceResponses, rangeFilters).size());

  }

  @Test
  public void adherenceStatisticsFilterTest() throws ParseException, NumberFormatException {
    List<AdherenceResponse> adherenceResponses = new ArrayList<>();
    adherenceResponses.add(new AdherenceResponse("1", null, "649649649649649649", Utils.convertStringToDate("2019-01-01 01:00:00"), 6, 12, 18, Utils.convertStringToDate("2019-01-01 01:00:00"), Utils.convertStringToDate("2019-01-18 01:00:00"), null, 1L, 1L, new ArrayList<>()));
    RangeFilters rangeFilters = new RangeFilters(RangeFilterType.ADHERENCE_STATISTICS, "2018-12-01 01:00:00", "2019-01-20 01:00:00");
    assertEquals(adherenceService.filterList(adherenceResponses, rangeFilters).get(0).adherenceStatistics.getTechnologyDoses(), 6);
    assertEquals(adherenceService.filterList(adherenceResponses, rangeFilters).get(0).adherenceStatistics.getManualDoses(), 6);
    assertEquals(adherenceService.filterList(adherenceResponses, rangeFilters).get(0).adherenceStatistics.getTotalDoses(), 18);
    adherenceResponses.get(0).adherenceStatistics = null;
    rangeFilters = new RangeFilters(RangeFilterType.ADHERENCE_STATISTICS, "2019-01-01 01:00:00", "2019-01-18 01:00:00");
    assertEquals(adherenceService.filterList(adherenceResponses, rangeFilters).get(0).adherenceStatistics.getTechnologyDoses(), 6);
    assertEquals(adherenceService.filterList(adherenceResponses, rangeFilters).get(0).adherenceStatistics.getManualDoses(), 6);
    assertEquals(adherenceService.filterList(adherenceResponses, rangeFilters).get(0).adherenceStatistics.getTotalDoses(), 18);
    adherenceResponses.get(0).adherenceStatistics = null;
    rangeFilters = new RangeFilters(RangeFilterType.ADHERENCE_STATISTICS, "2019-01-04 00:00:00", "2019-01-20 01:00:00");
    assertEquals(adherenceService.filterList(adherenceResponses, rangeFilters).get(0).adherenceStatistics.getTechnologyDoses(), 5);
    assertEquals(adherenceService.filterList(adherenceResponses, rangeFilters).get(0).adherenceStatistics.getManualDoses(), 5);
    assertEquals(adherenceService.filterList(adherenceResponses, rangeFilters).get(0).adherenceStatistics.getTotalDoses(), 15);
    adherenceResponses.get(0).adherenceStatistics = null;
    rangeFilters = new RangeFilters(RangeFilterType.ADHERENCE_STATISTICS, "2019-01-01 00:00:00", "2019-01-15 01:00:00");
    assertEquals(adherenceService.filterList(adherenceResponses, rangeFilters).get(0).adherenceStatistics.getTechnologyDoses(), 5);
    assertEquals(adherenceService.filterList(adherenceResponses, rangeFilters).get(0).adherenceStatistics.getManualDoses(), 5);
    assertEquals(adherenceService.filterList(adherenceResponses, rangeFilters).get(0).adherenceStatistics.getTotalDoses(), 15);
    adherenceResponses.get(0).adherenceStatistics = null;
    rangeFilters = new RangeFilters(RangeFilterType.ADHERENCE_STATISTICS, "2019-01-04 00:00:00", "2019-01-15 01:00:00");
    assertEquals(adherenceService.filterList(adherenceResponses, rangeFilters).get(0).adherenceStatistics.getTechnologyDoses(), 4);
    assertEquals(adherenceService.filterList(adherenceResponses, rangeFilters).get(0).adherenceStatistics.getManualDoses(), 4);
    assertEquals(adherenceService.filterList(adherenceResponses, rangeFilters).get(0).adherenceStatistics.getTotalDoses(), 12);
    adherenceResponses.get(0).adherenceStatistics = null;
    rangeFilters = new RangeFilters(RangeFilterType.ADHERENCE_STATISTICS, "2018-12-01 00:00:00", "2018-12-15 01:00:00");
    assertEquals(adherenceService.filterList(adherenceResponses, rangeFilters).get(0).adherenceStatistics, null);
    adherenceResponses.get(0).adherenceStatistics = null;
    rangeFilters = new RangeFilters(RangeFilterType.ADHERENCE_STATISTICS, "2019-12-01 00:00:00", "2019-12-15 01:00:00");
    assertEquals(adherenceService.filterList(adherenceResponses, rangeFilters).get(0).adherenceStatistics, null);
    adherenceResponses.clear();
    adherenceResponses.add(new AdherenceResponse("1", null, "4", null, 0, 1, 1, Utils.getCurrentDate(), null, null, 1L,1L, new ArrayList<>()));
    rangeFilters = new RangeFilters(RangeFilterType.ADHERENCE_STATISTICS, Utils.getFormattedDate(Utils.getCurrentDate(), "yyyy-MM-dd HH:mm:ss"),Utils.getFormattedDate(Utils.getCurrentDate(), "yyyy-MM-dd HH:mm:ss"));
    assertEquals(adherenceService.filterList(adherenceResponses, rangeFilters).get(0).adherenceStatistics.getTechnologyDoses(), 1);
    assertEquals(adherenceService.filterList(adherenceResponses, rangeFilters).get(0).adherenceStatistics.getManualDoses(), 0);
    assertEquals(adherenceService.filterList(adherenceResponses, rangeFilters).get(0).adherenceStatistics.getTotalDoses(), 1);
  }

  @Test
  public void adherenceStatisticsFilterOneSidedTest() throws ParseException {
    List<AdherenceResponse> adherenceResponses = new ArrayList<>();
    adherenceResponses.add(new AdherenceResponse("1", null, "649649649649649649", Utils.convertStringToDate("2019-01-01 01:00:00"), 6, 12, 18, Utils.convertStringToDate("2019-01-01 01:00:00"), Utils.convertStringToDate("2019-01-18 01:00:00"), null, 1L,1L, new ArrayList<>()));
    RangeFilters rangeFilters = new RangeFilters(RangeFilterType.ADHERENCE_STATISTICS, null, "2019-01-15 01:00:00");
    assertEquals(adherenceService.filterList(adherenceResponses, rangeFilters).get(0).adherenceStatistics.getTechnologyDoses(), 5);
    assertEquals(adherenceService.filterList(adherenceResponses, rangeFilters).get(0).adherenceStatistics.getManualDoses(), 5);
    assertEquals(adherenceService.filterList(adherenceResponses, rangeFilters).get(0).adherenceStatistics.getTotalDoses(), 15);
    adherenceResponses.get(0).adherenceStatistics = null;
    rangeFilters = new RangeFilters(RangeFilterType.ADHERENCE_STATISTICS, "2019-01-04 00:00:00", null);
    assertEquals(adherenceService.filterList(adherenceResponses, rangeFilters).get(0).adherenceStatistics.getTechnologyDoses(), 5);
    assertEquals(adherenceService.filterList(adherenceResponses, rangeFilters).get(0).adherenceStatistics.getManualDoses(), 5);
    assertEquals(adherenceService.filterList(adherenceResponses, rangeFilters).get(0).adherenceStatistics.getTotalDoses(), 15);
    adherenceResponses.get(0).adherenceStatistics = null;
    rangeFilters = new RangeFilters(RangeFilterType.ADHERENCE_STATISTICS, null, null);
    assertEquals(adherenceService.filterList(adherenceResponses, rangeFilters).get(0).adherenceStatistics.getTechnologyDoses(), 6);
    assertEquals(adherenceService.filterList(adherenceResponses, rangeFilters).get(0).adherenceStatistics.getManualDoses(), 6);
    assertEquals(adherenceService.filterList(adherenceResponses, rangeFilters).get(0).adherenceStatistics.getTotalDoses(), 18);
  }

  @Test
  public void testGetIamIdsByUniqueIdentifierAndAdherenceTech() {
    String uniqueIdentifier = "ABCD";
    Long adhTechId = 1L;
    List<Long> iamids = Arrays.asList(1L, 2L);

    when(adherenceTechHandlerMap.getHandler(adhTechId)).thenReturn(adherenceTestHandler);
    when(registrationRepository.getIamIdsFromUniqueIdentifierAndAdTechId(eq(uniqueIdentifier), any())).thenReturn(iamids);

    List<Long> fetchedIamIds = adherenceService.getIamIdsByAdherenceTechAndUniqueIdentifier(uniqueIdentifier, adhTechId);
    Assert.assertEquals(fetchedIamIds, iamids);
  }

  @Test
  public void adherenceCacheTest() {
    String correctAdherenceJsonString = "{ \"entityId\": \"286434\", \"adherenceData\": [ { \"iamId\": 184853, \"startDate\": \"01-02-2020 22:00:00\", \"adherenceType\": 3, \"adherenceTypeString\": \"MERM Technology\", \"scheduleType\": 1, \"scheduleTypeString\": \"Daily\", \"currentSchedule\": \"\", \"uniqueIdentifier\": \"862810032579590\", \"adherenceString\": \"44556A99B6\", \"createdDate\": \"26-02-2020 12:32:59\", \"lastDosage\": \"12-05-2020 22:00:00\" } ], \"adherenceString\": \"44556A99B6\", \"lastDosage\": \"12-05-2020 22:00:00\" }";
    when(valueOperations.multiGet(any())).thenReturn(Arrays.asList(correctAdherenceJsonString));
    AdherenceCacheDataDto<AdherenceResponse> adherenceCacheData = adherenceService.getAdherenceFromCache(
            Arrays.asList("1"), Constants.CACHE_USER_PREFIX,
            (cacheEntry, allIdsSet) -> adherenceService.formatCacheForAdherence(cacheEntry, allIdsSet));
    AdherenceData data = adherenceCacheData.getAdherenceResponse().get(0).getAdherenceData().get(0);
    assertEquals(data.getAdherenceString(), "44556A99B6");
    assertEquals(String.valueOf(data.getAdherenceType()), "3");
    assertEquals(String.valueOf(data.getIamId()), "184853");
  }

  @Test
  public void adherenceCacheTestFail() {
    String entityId = "1";
    when(valueOperations.multiGet(any())).thenAnswer( invocation -> { throw new Exception("Redis Exception"); });
    AdherenceCacheDataDto<AdherenceResponse> adherenceCacheData = adherenceService.getAdherenceFromCache(
            Collections.singletonList(entityId), Constants.CACHE_USER_PREFIX,
            (cacheEntry, allIdsSet) -> adherenceService.formatCacheForAdherence(cacheEntry, allIdsSet));
    List<AdherenceResponse> responseList = adherenceCacheData.getAdherenceResponse();
    List<String> entityIdsLeft = adherenceCacheData.getEntityIds();
    assertEquals(responseList.size(), 0);
    assertEquals(entityIdsLeft.size(), 1);
    assertEquals(entityIdsLeft.get(0), entityId);
  }

  @Test
  public void positiveCalledCountNoFallBack() {
    List<String> entityIdList = new ArrayList<>();
    entityIdList.add("1");
    entityIdList.add("2");
    entityIdList.add("3");

    List<String> cacheData = new ArrayList<>();
    cacheData.add("MANUAL");
    cacheData.add("DIGITAL");
    cacheData.add("DIGITAL");

    doReturn(cacheData).when(valueOperations).multiGet(any());
    PositiveAdherenceDto positiveAdherenceDto = adherenceService.countPositiveCallsForToday(entityIdList);

    Assert.assertEquals(positiveAdherenceDto.getDigitalCount().intValue(), 2);
    Assert.assertEquals(positiveAdherenceDto.getManualCount().intValue(), 1);
  }

  @Test
  public void positiveCalledCountWithFallBack() {
    List<String> entityIdList = new ArrayList<>();
    entityIdList.add("1");
    entityIdList.add("2");
    entityIdList.add("3");

    List<String> cacheData = new ArrayList<>();
    cacheData.add("MANUAL");
    cacheData.add("DIGITAL");
    cacheData.add("DIGITAL");

    List<String> notInCacheIds = Arrays.asList(
        Constants.CACHE_ADH_STATUS_TODAY_PREFIX + "1234",
        Constants.CACHE_ADH_STATUS_TODAY_PREFIX + "1233",
        Constants.CACHE_ADH_STATUS_TODAY_PREFIX + "1231"
    );

    doReturn(cacheData).when(valueOperations).multiGet(any());
    doReturn(notInCacheIds).when(redisTemplate).execute(any(), any(), any(), any(), any());

    PositiveAdherenceDto positiveAdherenceDto = adherenceService.countPositiveCallsForToday(entityIdList, true);

    Assert.assertEquals(positiveAdherenceDto.getDigitalCount().intValue(), 2);
    Assert.assertEquals(positiveAdherenceDto.getManualCount().intValue(), 1);
    Assert.assertEquals(positiveAdherenceDto.getEntityIdList(), Arrays.asList("1234", "1233", "1231"));
  }

  @Test(expected = NotFoundException.class)
  public void testNotFoundExceptionForStopAdherence() throws ParseException {
    when(registrationRepository.findById(eq(1L))).thenReturn(Optional.empty());

    adherenceService.stopAdherence(1L, Utils.convertStringToDate("2019-01-01 01:00:00"));
  }

  @Test(expected = ValidationException.class)
  public void testValidationExceptionForStopAdherence() throws ParseException {
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-09-11 13:00:00"), null, "1", "666666", Utils.convertStringToDate("2019-02-01 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"));

    when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));

    adherenceService.stopAdherence(1L, Utils.convertStringToDate("2019-01-01 01:00:00"));
  }

  @Test
  public void testHasScheduleMapping() {
    when(scheduleTypeHandlerMap.getHandler(any())).thenReturn(mockDailyHandler);
    when(mockDailyHandler.hasScheduleMapping()).thenReturn(true);

    Assert.assertTrue(adherenceService.hasScheduleMappings(1L));

    when(mockDailyHandler.hasScheduleMapping()).thenReturn(false);

    Assert.assertFalse(adherenceService.hasScheduleMappings(1L));
  }

  @Test
  public void testActiveScheduleMappingExistsForEntityIdAndClientId() {
    Long iamId = 1L;
    List<Long> iamIdList = new ArrayList<>();
    iamIdList.add(iamId);
    Long clientId = 1L;
    String entityId = "123";
    Long scheduleTypeId = 1L;
    when(scheduleTypeHandlerMap.getHandler(any())).thenReturn(mockDailyHandler);
    when(mockDailyHandler.findAllIamWithActiveSchedule(any(),any())).thenReturn(null);

    Assert.assertFalse(adherenceService.activeScheduleMappingExistsForEntityIdAndClientId(clientId,entityId,scheduleTypeId));

    when(mockDailyHandler.findAllIamWithActiveSchedule(any(),any())).thenReturn(iamIdList);

    Assert.assertTrue(adherenceService.activeScheduleMappingExistsForEntityIdAndClientId(clientId,entityId,scheduleTypeId));
  }

  @Test
  public void testRegenerateAdherenceString() throws ParseException {
    Long iamId = 1L;
    List<Long> iamIdList = new ArrayList<>();
    iamIdList.add(iamId);
    List<Registration> registrations = new ArrayList<>();
    Registration registration1 = new Registration(1L, 1L, Utils.convertStringToDate("2019-09-11 13:00:00"), null, "1", "666666", Utils.convertStringToDate("2019-01-01 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"),null,1L);
    Registration registration2 = new Registration(2L, 2L, Utils.convertStringToDate("2019-09-11 13:00:00"), null, "1", "666666", Utils.convertStringToDate("2019-01-01 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"),null,1L);
    Registration registration3 = new Registration(3L, 3L, Utils.convertStringToDate("2019-09-11 13:00:00"), null, "1", "666666", Utils.convertStringToDate("2019-01-01 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"),null,1L);
    Registration registration4 = new Registration(4L, 3L, Utils.convertStringToDate("2019-09-11 13:00:00"), null, "1", "666666", Utils.convertStringToDate("2019-01-01 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"),null,1L);
    registrations.add(registration1);
    registrations.add(registration2);
    registrations.add(registration3);
    registrations.add(registration4);
    Map<Long, List<Registration>> adherenceCodeRegistrationMap = registrations.stream().collect(groupingBy(Registration::getAdTechId));

    when(registrationRepository.findByIdIn(any())).thenReturn(registrations);
    when(adherenceTechHandlerMap.getHandler(any())).thenReturn(adherenceTestHandler);

    adherenceService.regenerateAdherenceString(iamIdList);
    verify(adherenceTestHandler, times(adherenceCodeRegistrationMap.size())).regenerateAdherence(any());
  }

  @Test(expected = ValidationException.class)
  public void testValidateRegistrationsForDateInvalidDate() throws ParseException {
    List<Registration> registrations = new ArrayList<>();
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-09-11 13:00:00"), null, "1", "666666", Utils.convertStringToDate("2019-01-01 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"),null,1L);
    registrations.add(registration);
    String date = "2019-09-11 13:00:";

    adherenceService.validateRegistrationsForDate(registrations, date);
  }

  @Test(expected = ConflictException.class)
  public void testValidateRegistrationsForDateEndDateNull() throws ParseException {
    List<Registration> registrations = new ArrayList<>();
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-09-11 13:00:00"), null, "1", "666666", Utils.convertStringToDate("2019-01-01 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"),null,1L);
    registrations.add(registration);
    String date = "2019-09-11 13:00:00";

    adherenceService.validateRegistrationsForDate(registrations, date);
  }

  @Test(expected = ConflictException.class)
  public void testValidateRegistrationsForDateEndDateBeforeStartDate() throws ParseException {
    List<Registration> registrations = new ArrayList<>();
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-09-11 13:00:00"), Utils.convertStringToDate("2019-09-12 13:00:00"), "1", "666666", Utils.convertStringToDate("2019-01-01 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"),null,1L);
    registrations.add(registration);
    String date = "2019-09-11 13:00:00";

    adherenceService.validateRegistrationsForDate(registrations, date);
  }

    @Test
  public void testGetAdherenceTechHandler() throws ParseException {
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-09-11 13:00:00"), null, "1", "666666", Utils.convertStringToDate("2019-01-01 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"),null,1L);

    when(adherenceTechHandlerMap.getHandler(1L)).thenReturn(nndHandler);
    when(adherenceTechHandlerMap.getHandler(5L)).thenReturn(noneHandler);

    Assert.assertEquals(nndHandler.getAdhTechEnumerator(),adherenceService.getAdherenceTechHandler(registration).getAdhTechEnumerator());

    registration.setAdTechId(5L);
    Assert.assertEquals(noneHandler.getAdhTechEnumerator(),adherenceService.getAdherenceTechHandler(registration).getAdhTechEnumerator());
  }

  @Test
  public void testGetIamIdsByAdherenceTech() {
    BigInteger iamId1 = new BigInteger("1");
    BigInteger iamId2 = new BigInteger("2");
    List<BigInteger> iamIdList = new ArrayList<>();
    iamIdList.add(iamId1);
    iamIdList.add(iamId2);
    Long adTechId = 1L;
    Long size = 2L;

    when(registrationRepository.getByAdTechIdCustom(any(),any())).thenReturn(iamIdList);

    Assert.assertEquals(size.intValue(),adherenceService.getIamIdsByAdherenceTech(adTechId,size).size());
    verify(registrationRepository, Mockito.times(1)).getByAdTechIdCustom(any(),any());
  }

  @Test(expected = NotFoundException.class)
  public void testGetLastClosedRegistrationNotFound() {
    Long iamId = 1L;
    List<Long> iamIdList = new ArrayList<>();
    iamIdList.add(iamId);
    List<Registration> registrations = new ArrayList<>();

    when(registrationRepository.findByIdIn(any())).thenReturn(registrations);

    adherenceService.getLastClosedRegistration(iamIdList);
  }

  @Test(expected = ValidationException.class)
  public void testOrderedAdherenceMapNotFound() {
    String date = "2019-09-14 13:00:";
    Character code = '4';
    Map<String, Character> adherenceMap = new HashMap<>();
    adherenceMap.put(date,code);

    adherenceService.getOrderedAdherenceMap(adherenceMap);
  }

  @Test
  public void testGetLastClosedRegistration() throws ParseException {
    Long iamId = 1L;
    List<Long> iamIdList = new ArrayList<>();
    iamIdList.add(iamId);
    Registration registration1 = new Registration(1L, 1L, Utils.convertStringToDate("2018-09-11 13:00:00"), Utils.convertStringToDate("2019-05-01 12:23:26"), "1", "666666", Utils.convertStringToDate("2019-01-01 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"),null,1L);
    List<Registration> registrations = new ArrayList<>();
    registrations.add(registration1);

    when(registrationRepository.findByIdIn(any())).thenReturn(registrations);

    Assert.assertEquals(registration1,adherenceService.getLastClosedRegistration(iamIdList));

    Registration registration2 = new Registration(1L, 1L, Utils.convertStringToDate("2018-09-11 13:00:00"), Utils.convertStringToDate("2019-05-02 12:23:26"), "1", "666666", Utils.convertStringToDate("2019-01-01 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"),null,1L);
    registrations.add(registration2);

    Assert.assertEquals(registration2,adherenceService.getLastClosedRegistration(iamIdList));

    Registration registration3 = new Registration(1L, 1L, Utils.convertStringToDate("2018-09-11 13:00:00"), Utils.convertStringToDate("2019-05-02 12:23:26"), "1", "666666", Utils.convertStringToDate("2019-01-02 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"),null,1L);
    registrations.add(registration3);

    Assert.assertEquals(registration3,adherenceService.getLastClosedRegistration(iamIdList));

    Registration registration4 = new Registration(1L, 1L, Utils.convertStringToDate("2018-09-12 13:00:00"), Utils.convertStringToDate("2019-05-02 12:23:26"), "1", "666666", Utils.convertStringToDate("2019-01-02 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"),null,1L);
    registrations.add(registration4);

    Assert.assertEquals(registration4,adherenceService.getLastClosedRegistration(iamIdList));

  }

  @Test
  public void testUpdateEndDate() throws ParseException {
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-09-11 13:00:00"), null, "1", "666666", Utils.convertStringToDate("2019-01-01 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"),null,1L);
    String endDate = "2019-09-14 13:00:00";

    when(registrationRepository.save(any())).thenReturn(null);

    Assert.assertEquals(Utils.convertStringToDate(endDate),adherenceService.updateEndDate(registration,Utils.convertStringToDate(endDate)).getEndDate());
  }

  @Test
  public void testReopenEntity() throws ParseException {
    EntityRequest entityRequest = new EntityRequest("123",1,"123");
    Long iamId = 1L;
    List<Long> iamIdList = new ArrayList<>();
    iamIdList.add(iamId);
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-09-11 13:00:00"), Utils.convertStringToDate("2019-09-11 13:00:00"), "1", "666666", Utils.convertStringToDate("2019-01-02 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"),null,1L);
    List<Registration> registrations = new ArrayList<>();
    registrations.add(registration);

    when(registrationRepository.findByIdIn(any())).thenReturn(registrations);
    when(registrationRepository.save(any())).thenReturn(null);
    when(adherenceTechHandlerMap.getHandler(any())).thenReturn(adherenceTestHandler);
    when(scheduleTypeHandlerMap.getHandler(any())).thenReturn(mockDailyHandler);
    doNothing().when(mockDailyHandler).reopenSchedule(any());

    Assert.assertEquals(null,adherenceService.reopenEntity(entityRequest,iamIdList).getEndDate());
    verify(mockDailyHandler, Mockito.times(1)).reopenSchedule(any());
    verify(adherenceTestHandler, Mockito.times(1)).postRegistration(any(), any(), anyBoolean());
  }

  @Test(expected = ValidationException.class)
  public void testCreateIamValidationException() {
    EntityRequest entityRequest = new EntityRequest("123",1,"123");

    when(adherenceTechHandlerMap.getHandler(any())).thenReturn(null);
    when(scheduleTypeHandlerMap.getHandler(any())).thenReturn(mockDailyHandler);

    adherenceService.createIam(entityRequest);
  }

  @Test
  public void testCreateIam() throws ParseException {
    EntityRequest entityRequest = new EntityRequest("123",1,"123");
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-09-11 13:00:00"), Utils.convertStringToDate("2019-09-11 13:00:00"), "1", "666666", Utils.convertStringToDate("2019-01-02 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"),null,1L);

    when(adherenceTechHandlerMap.getHandler(any())).thenReturn(adherenceTestHandler);
    when(scheduleTypeHandlerMap.getHandler(any())).thenReturn(mockDailyHandler);
    doNothing().when(mockDailyHandler).validateScheduleAndSensitivityOverLap(any(),any(), any());
    doNothing().when(mockDailyHandler).save(any(),any(),any(),any(),any(), any());
    when(adherenceTestHandler.save(any())).thenReturn(registration);

    Assert.assertEquals(registration.getId(),adherenceService.createIam(entityRequest));
    verify(mockDailyHandler, Mockito.times(1)).save(any(),any(),any(),any(),any(), any());
    verify(adherenceTestHandler, Mockito.times(1)).postRegistration(any(), any(), anyBoolean());
    verify(adherenceTestHandler, Mockito.times(1)).save(any());
  }

  @Test
  public void testGetAdherenceCorrectAdherenceString() throws ParseException {
    Long iamId = 1L;
    List<Long> iamIdList = new ArrayList<>();
    iamIdList.add(iamId);
    String scheduleValue = "1000000";
    Registration registration = new Registration(1L, 1L, Utils.getCurrentDate(), null, "1", "6", Utils.convertStringToDate("2019-01-02 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"), null, 1L);

    when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));
    when(adherenceTechHandlerMap.getHandler(any())).thenReturn(adherenceTestHandler);
    when(scheduleTypeHandlerMap.getHandler(any())).thenReturn(mockDailyHandler);
    when(mockDailyHandler.getActiveScheduleString(any())).thenReturn(scheduleValue);
    when(adherenceTestHandler.getAdhTechEnumerator()).thenReturn(AdherenceTech.NNDOTS);
    when(mockDailyHandler.getScheduleTypeEnumerator()).thenReturn(ScheduleTypeEnum.DAILY);

    Assert.assertEquals(null, adherenceService.getAdherence(iamId, false).getAdherenceData().get(0).getAdhTechLogsData());
    Assert.assertEquals(null, adherenceService.getAdherence(iamId, null).getAdherenceData().get(0).getAdhTechLogsData());

    when(adherenceTestHandler.getTechLogs(any())).thenReturn(null);

    Assert.assertEquals(registration.getAdherenceString(), adherenceService.getAdherence(iamId, true).getAdherenceData().get(0).getAdherenceString());
  }

  @Test
  public void testGetAdherenceLongerAdherenceString() throws ParseException {
    Long iamId = 1L;
    List<Long> iamIdList = new ArrayList<>();
    iamIdList.add(iamId);
    String scheduleValue = "1000000";
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-09-11 13:00:00"), Utils.convertStringToDate("2019-09-12 13:00:00"), "1", "66666", Utils.convertStringToDate("2019-01-02 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"), 1L);
    AdherenceData adherenceData = new AdherenceData("9777776777", AdherenceTech.NNDOTS.getName());

    when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));
    when(adherenceTechHandlerMap.getHandler(any())).thenReturn(adherenceTestHandler);
    when(scheduleTypeHandlerMap.getHandler(any())).thenReturn(mockDailyHandler);
    when(mockDailyHandler.getActiveScheduleString(any())).thenReturn(scheduleValue);
    when(adherenceTestHandler.getAdhTechEnumerator()).thenReturn(AdherenceTech.NNDOTS);
    when(mockDailyHandler.getScheduleTypeEnumerator()).thenReturn(ScheduleTypeEnum.DAILY);
    when(adherenceTestHandler.getAdherenceDataForIam(any(), any(), any(), any(), any(),any())).thenReturn(adherenceData);
    when(registrationRepository.save(any())).thenReturn(null);

    Assert.assertEquals(null, adherenceService.getAdherence(iamId, false).getAdherenceData().get(0).getAdhTechLogsData());
    Assert.assertEquals(null, adherenceService.getAdherence(iamId, null).getAdherenceData().get(0).getAdhTechLogsData());
    Assert.assertEquals(registration.getId(), adherenceService.getAdherence(iamId, true).getAdherenceData().get(0).getIamId());
  }

  @Test
  public void testGetAdherenceShorterAdherenceString() throws ParseException {
    Long iamId = 1L;
    List<Long> iamIdList = new ArrayList<>();
    iamIdList.add(iamId);
    String scheduleValue = "1000000";

    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-09-11 13:00:00"), Utils.convertStringToDate("2019-09-18 13:00:00"), "1", "66", Utils.convertStringToDate("2019-01-02 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"),Utils.convertStringToDate("2019-01-10 12:23:26"),1L,Utils.convertStringToDate("2019-01-10 12:23:26"));

    when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));
    when(adherenceTechHandlerMap.getHandler(any())).thenReturn(adherenceTestHandler);
    when(scheduleTypeHandlerMap.getHandler(any())).thenReturn(mockDailyHandler);
    when(mockDailyHandler.getActiveScheduleString(any())).thenReturn(scheduleValue);
    when(adherenceTestHandler.getAdhTechEnumerator()).thenReturn(AdherenceTech.NNDOTS);
    when(mockDailyHandler.getScheduleTypeEnumerator()).thenReturn(ScheduleTypeEnum.DAILY);
    when(adherenceTestHandler.generateAdherenceString(any(),any(),any(),any())).thenReturn(null);
    when(adherenceTestHandler.computeLastDosage(any(),any(),any())).thenReturn(null);
    Assert.assertEquals(null,adherenceService.getAdherence(iamId,false).getAdherenceData().get(0).getAdhTechLogsData());
    Assert.assertEquals(null,adherenceService.getAdherence(iamId,null).getAdherenceData().get(0).getAdhTechLogsData());

    registration.setAdherenceString(null);
    when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));

    Assert.assertEquals(registration.getId(),adherenceService.getAdherence(iamId,true).getAdherenceData().get(0).getIamId());
  }

  @Test
  public void testFilterListConsecutiveDoses() throws ParseException, NumberFormatException {
    List<AdherenceResponse> adherenceResponses = new ArrayList<>();
    AdherenceResponse adherenceResponse1 = new AdherenceResponse("1", null, "666666", Utils.convertStringToDate("2019-01-01 01:00:00"),0, 0, 0, null,null,null, 0, 0, new ArrayList<>());
    AdherenceResponse adherenceResponse2 = new AdherenceResponse("1", null, "666666", Utils.convertStringToDate("2019-01-01 01:00:00"),0, 0, 0, null,null,null, 1,0, new ArrayList<>());
    AdherenceResponse adherenceResponse3 = new AdherenceResponse("1", null, "666666", Utils.convertStringToDate("2019-01-01 01:00:00"),0, 0, 0, null,null,null, 2,1, new ArrayList<>());
    AdherenceResponse adherenceResponse4 = new AdherenceResponse("1", null, "666666", Utils.convertStringToDate("2019-01-01 01:00:00"),0, 0, 0, null,null,null, 3,2, new ArrayList<>());
    AdherenceResponse adherenceResponse5 = new AdherenceResponse("1", null, "666666", Utils.convertStringToDate("2019-01-01 01:00:00"),0, 0, 0, null,null,null, 4,3, new ArrayList<>());
    adherenceResponses.add(adherenceResponse1);
    adherenceResponses.add(adherenceResponse2);
    adherenceResponses.add(adherenceResponse3);
    adherenceResponses.add(adherenceResponse4);
    adherenceResponses.add(adherenceResponse5);
    List<RangeFilters> rangeFilters = new ArrayList<>();
    RangeFilters rangeFilter = new RangeFilters(RangeFilterType.CONSECUTIVE_MISSED_DOSES,"1","3");
    rangeFilters.add(rangeFilter);
    List<AdherenceResponse> adherenceResponsesResult = adherenceService.filterList(adherenceResponses,rangeFilters);
    assertEquals(3,adherenceResponsesResult.size());
    rangeFilters.clear();
    rangeFilters.add(new RangeFilters(RangeFilterType.CONSECUTIVE_MISSED_DOSES,"1",null));
    adherenceResponsesResult = adherenceService.filterList(adherenceResponses,rangeFilters);
    assertEquals(4,adherenceResponsesResult.size());
    rangeFilters.clear();
    rangeFilters.add(new RangeFilters(RangeFilterType.CONSECUTIVE_MISSED_DOSES,null,"3"));
    adherenceResponsesResult = adherenceService.filterList(adherenceResponses,rangeFilters);
    assertEquals(4,adherenceResponsesResult.size());
  }

  @Test
  public void testFilterListConsecutiveDosesFromYesterday() throws ParseException, NumberFormatException {
    List<AdherenceResponse> adherenceResponses = new ArrayList<>();
    AdherenceResponse adherenceResponse1 = new AdherenceResponse("1", null, "666666", Utils.convertStringToDate("2019-01-01 01:00:00"),0, 0, 0, null,null,null, 0, 0, new ArrayList<>());
    AdherenceResponse adherenceResponse2 = new AdherenceResponse("1", null, "666666", Utils.convertStringToDate("2019-01-01 01:00:00"),0, 0, 0, null,null,null, 1,0, new ArrayList<>());
    AdherenceResponse adherenceResponse3 = new AdherenceResponse("1", null, "666666", Utils.convertStringToDate("2019-01-01 01:00:00"),0, 0, 0, null,null,null, 2,1, new ArrayList<>());
    AdherenceResponse adherenceResponse4 = new AdherenceResponse("1", null, "666666", Utils.convertStringToDate("2019-01-01 01:00:00"),0, 0, 0, null,null,null, 3,2, new ArrayList<>());
    AdherenceResponse adherenceResponse5 = new AdherenceResponse("1", null, "666666", Utils.convertStringToDate("2019-01-01 01:00:00"),0, 0, 0, null,null,null, 4,3, new ArrayList<>());
    adherenceResponses.add(adherenceResponse1);
    adherenceResponses.add(adherenceResponse2);
    adherenceResponses.add(adherenceResponse3);
    adherenceResponses.add(adherenceResponse4);
    adherenceResponses.add(adherenceResponse5);
    List<RangeFilters> rangeFilters = new ArrayList<>();
    RangeFilters rangeFilter = new RangeFilters(RangeFilterType.CONSECUTIVE_MISSED_DOSES_FROM_YESTERDAY,"1","3");
    rangeFilters.add(rangeFilter);
    List<AdherenceResponse> adherenceResponsesResult = adherenceService.filterList(adherenceResponses,rangeFilters);
    assertEquals(3,adherenceResponsesResult.size());
    rangeFilters.clear();
    rangeFilters.add(new RangeFilters(RangeFilterType.CONSECUTIVE_MISSED_DOSES_FROM_YESTERDAY,"1",null));
    adherenceResponsesResult = adherenceService.filterList(adherenceResponses,rangeFilters);
    assertEquals(3,adherenceResponsesResult.size());
    rangeFilters.clear();
    rangeFilters.add(new RangeFilters(RangeFilterType.CONSECUTIVE_MISSED_DOSES_FROM_YESTERDAY,null,"3"));
    adherenceResponsesResult = adherenceService.filterList(adherenceResponses,rangeFilters);
    assertEquals(5,adherenceResponsesResult.size());
  }


  @Test
  public void testGetAllAdherence() throws ParseException {
    Long iamId = 1L;
    List<Long> iamIdList = new ArrayList<>();
    iamIdList.add(iamId);
    String scheduleValue = "1000000";
    Registration registration1 = new Registration(1L, 1L, Utils.convertStringToDate("2019-09-01 13:00:00"), Utils.convertStringToDate("2019-09-05 13:00:00"), "1", "66666", Utils.convertStringToDate("2019-01-02 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"),null,1L);
    List<Registration> registrations = new ArrayList<>(Arrays.asList(registration1));

    when(registrationRepository.findByIdIn(any())).thenReturn(registrations);
    when(adherenceTechHandlerMap.getHandler(any())).thenReturn(adherenceTestHandler);
    when(scheduleTypeHandlerMap.getHandler(any())).thenReturn(mockDailyHandler);
    when(mockDailyHandler.getActiveScheduleString(any())).thenReturn(scheduleValue);
    when(adherenceTestHandler.getAdhTechEnumerator()).thenReturn(AdherenceTech.NNDOTS);
    when(mockDailyHandler.getScheduleTypeEnumerator()).thenReturn(ScheduleTypeEnum.DAILY);
    when(adherenceTestHandler.getEntityConsecutiveMissedDoses(any(),any(),any(),any(),anyBoolean())).thenReturn(1L);
    when(registrationRepository.getOne(any())).thenReturn(registration1);
    when(registrationRepository.findByUniqueIdentifierAndEndDateIsNull(any())).thenReturn(registration1);
    when(mockDailyHandler.getDoseTimes(any())).thenReturn(new ArrayList<>());
    assertEquals(null,adherenceService.getAllAdherence(iamIdList,false).getLastDosage());

    registrations.get(0).setLastDosage(Utils.convertStringToDate("2019-09-05 13:00:00"));
    assertEquals(registration1.getLastDosage(),adherenceService.getAllAdherence(iamIdList,false).getLastDosage());

    Registration registration2 = new Registration(1L, 1L, Utils.convertStringToDate("2019-09-06 13:00:00"), Utils.convertStringToDate("2019-09-10 13:00:00"), "1", "66666", Utils.convertStringToDate("2019-01-02 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"),Utils.convertStringToDate("2019-09-10 13:00:00"),1L);
    registrations.add(registration2);
    assertEquals(registration2.getLastDosage(),adherenceService.getAllAdherence(iamIdList,false).getLastDosage());
  }

    @Test(expected = NotFoundException.class)
    public void testUpdateIamNotFoundException() {
      EntityRequest entityRequest = new EntityRequest(1);
      Long iamId = 1L;
      when(registrationRepository.findById(any())).thenReturn(Optional.empty());
      adherenceService.updateIam(entityRequest,iamId);
    }

  @Test(expected = InvalidParameterException.class)
  public void testUpdateIamInvalidParameterException() throws ParseException {
    EntityRequest entityRequest = new EntityRequest("123",1,"123","2019-09-05 13:00",1L,"1000000",1L,1L,1L,1L, null);;
    Long iamId = 1L;
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-09-01 13:00:00"), Utils.convertStringToDate("2019-09-05 13:00:00"), "1", "66666", Utils.convertStringToDate("2019-01-02 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"),null,1L);
    when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));
    adherenceService.updateIam(entityRequest,iamId);
  }

  @Test
  public void testUpdateIamStartDateScheduleStringEmpty() throws ParseException {
    EntityRequest entityRequest = new EntityRequest("123", 1, "123", null, 1L, null, 1L, 1L, 1L,1L, null);
    Long iamId = 1L;
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-09-01 13:00:00"), Utils.convertStringToDate("2019-09-05 13:00:00"), "1", "66666", Utils.convertStringToDate("2019-01-02 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"), null, 1L);
    String adherenceString = "6666";

    when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));
    when(scheduleTypeHandlerMap.getHandler(any())).thenReturn(mockDailyHandler);
    when(adherenceTechHandlerMap.getHandler(any())).thenReturn(adherenceTestHandler);
    when(adherenceTestHandler.generateAdherenceString(any(),any(),any(),any())).thenReturn(adherenceString);
    when(registrationRepository.save(any())).thenReturn(null);

    Assert.assertTrue(adherenceService.updateIam(entityRequest, iamId));
    Assert.assertEquals(adherenceString,registration.getAdherenceString());
    verify(adherenceTestHandler, Mockito.times(1)).generateAdherenceString(any(), any(), any(), any());
    verify(registrationRepository, Mockito.times(1)).save(any());
  }

  @Test
  public void testUpdateIamActiveScheduleMapEmpty() throws ParseException {
    EntityRequest entityRequest = new EntityRequest("123", 1, "123", "2019-09-05 13:00:00", 1L, "1000000", 1L, 1L, 1L, 1L, null);
    Long iamId = 1L;
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-09-01 13:00:00"), Utils.convertStringToDate("2019-09-09 13:00:00"), "1", "66666", Utils.convertStringToDate("2019-01-02 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"), null, 1L);
    String adherenceString = "6666";

    when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));
    when(scheduleTypeHandlerMap.getHandler(any())).thenReturn(mockDailyHandler);
    when(adherenceTechHandlerMap.getHandler(any())).thenReturn(adherenceTestHandler);
    when(registrationRepository.save(any())).thenReturn(null);
    when(adherenceTestHandler.generateAdherenceString(any(),any(),any(),any())).thenReturn(adherenceString);
    doNothing().when(mockDailyHandler).updateStartDate(any(), any(), any());

    Assert.assertTrue(adherenceService.updateIam(entityRequest, iamId));
    Assert.assertEquals(Utils.convertStringToDate("2019-09-05 13:00:00"),registration.getStartDate());
    Assert.assertEquals(adherenceString,registration.getAdherenceString());
    verify(mockDailyHandler, Mockito.times(1)).updateStartDate(any(), any(), any());
    verify(adherenceTestHandler, Mockito.times(1)).generateAdherenceString(any(), any(), any(), any());
    verify(registrationRepository, Mockito.times(1)).save(any());
  }

  @Test
  public void testUpdateIam() throws ParseException {
    EntityRequest entityRequest = new EntityRequest("123", 1, "123", "2019-09-05 13:00:00", 1L, "1000000", 1L, 1L, 1L, 1L, null);
    Long iamId = 1L;
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-09-01 13:00:00"), Utils.convertStringToDate("2019-09-09 13:00:00"), "1", "66666", Utils.convertStringToDate("2019-01-02 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"), null, 1L);
    ScheduleMap scheduleMap = new ScheduleMap(1L,true,Utils.convertStringToDate("2019-09-01 13:00:00"),iamId,1L,1L,1L,Utils.convertStringToDate("2019-09-01 13:00:00"),Utils.convertStringToDate("2019-09-01 13:00:00"),1L);
    Schedule schedule = new Schedule(1L, "123",1L);
    String adherenceString = "6666";

    when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));
    when(scheduleTypeHandlerMap.getHandler(any())).thenReturn(mockDailyHandler);
    when(adherenceTechHandlerMap.getHandler(any())).thenReturn(adherenceTestHandler);
    when(adherenceTestHandler.generateAdherenceString(any(),any(),any(),any())).thenReturn(adherenceString);
    when(registrationRepository.save(any())).thenReturn(null);
    doNothing().when(mockDailyHandler).updateStartDate(any(), any(), any());

    Assert.assertTrue(adherenceService.updateIam(entityRequest,iamId));
    Assert.assertEquals(Utils.convertStringToDate("2019-09-05 13:00:00"),registration.getStartDate());
    Assert.assertEquals(adherenceString,registration.getAdherenceString());
    verify(mockDailyHandler, Mockito.times(1)).updateStartDate(any(), any(), any());
    verify(adherenceTestHandler, Mockito.times(1)).generateAdherenceString(any(), any(), any(), any());
    verify(registrationRepository, Mockito.times(1)).save(any());
}

  @Test
  public void testUpdateIamEntityRequestScheduleAndIamScheduleDifferent() throws ParseException {
    EntityRequest entityRequest = new EntityRequest("123", 1, "123", "2019-09-05 13:00:00", 1L, "1000000", 1L, 1L, 1L, 1L, null);
    Long iamId = 1L;
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-09-01 13:00:00"), Utils.convertStringToDate("2019-09-09 13:00:00"), "1", "66666", Utils.convertStringToDate("2019-01-02 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"), null, 2L);
    ScheduleMap scheduleMap = new ScheduleMap(1L,true,Utils.convertStringToDate("2019-09-01 13:00:00"),iamId,1L,1L,1L,Utils.convertStringToDate("2019-09-01 13:00:00"),Utils.convertStringToDate("2019-09-01 13:00:00"),1L);
    Schedule schedule = new Schedule(2L, "0100000",1L);
    String adherenceString = "6666";

    when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));
    when(scheduleTypeHandlerMap.getHandler(any())).thenReturn(mockWeeklyHandler);
    when(adherenceTechHandlerMap.getHandler(any())).thenReturn(adherenceTestHandler);
    when(adherenceTestHandler.generateAdherenceString(any(),any(),any(),any())).thenReturn(adherenceString);
    when(registrationRepository.save(any())).thenReturn(null);
    doNothing().when(mockWeeklyHandler).updateStartDate(any(), any(), any());
    doNothing().when(mockWeeklyHandler).validateScheduleAndSensitivityOverLap(any(), any(), any());
    when(mockWeeklyHandler.getScheduleValue(any())).thenReturn(null);
    when(mockWeeklyHandler.getActiveScheduleMapForIam(any())).thenReturn(scheduleMap);
    when(mockWeeklyHandler.findByScheduleValue(any())).thenReturn(schedule);
    doNothing().when(mockWeeklyHandler).stopSchedule(any(),any());
    doNothing().when(mockWeeklyHandler).save(any(),any(),any(),any(),any());
    when(mockWeeklyHandler.hasScheduleMapping()).thenReturn(true);

    Assert.assertTrue(adherenceService.updateIam(entityRequest,iamId));
    Assert.assertEquals(Utils.convertStringToDate("2019-09-01 13:00:00"),registration.getStartDate());
    Assert.assertEquals(adherenceString,registration.getAdherenceString());
    verify(mockWeeklyHandler, Mockito.times(1)).validateScheduleAndSensitivityOverLap(any(), any(), any());
    verify(mockWeeklyHandler, Mockito.times(1)).getActiveScheduleMapForIam(any());
    verify(mockWeeklyHandler, Mockito.times(1)).findByScheduleValue(any());
    verify(mockWeeklyHandler, Mockito.times(1)).stopSchedule(any(), any());
    verify(mockWeeklyHandler, Mockito.times(1)).save(any(), any(), any(), any(), any());
    verify(adherenceTestHandler, Mockito.times(1)).generateAdherenceString(any(), any(), any(), any());
    verify(registrationRepository, Mockito.times(1)).save(any());
  }

  @Test
  public void testGetEntityAdherenceStringSingle() throws ParseException {
    Registration registration1 = new Registration(1L, 1L, Utils.convertStringToDate("2019-09-01 13:00:00"), Utils.convertStringToDate("2019-09-05 13:00:00"), "1", "66666", Utils.convertStringToDate("2019-01-02 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"),null,1L);
    List<AdherenceData> adherenceDataList = new ArrayList<>();
    AdherenceData adherenceData = new AdherenceData(1L,Utils.convertStringToDate("2019-09-01 13:00:00"),Utils.convertStringToDate("2019-09-02 13:00:00"),1L,AdherenceTech.NNDOTS.getName(),1L,null,null,null,"6",Utils.convertStringToDate("2019-09-11 13:00:00"),null, null, 0, 0, 0,null);
    adherenceDataList.add(adherenceData);
    when(registrationRepository.getOne(any())).thenReturn(registration1);
    Assert.assertEquals(adherenceData.getAdherenceString(),adherenceService.getEntityAdherenceString(adherenceDataList));
  }

  @Test
  public void testGetEntityAdherenceStringMultipleIds() throws ParseException {
    Registration registration1 = new Registration(1L, 1L, Utils.convertStringToDate("2019-09-01 13:00:00"), Utils.convertStringToDate("2019-09-05 13:00:00"), "1", "66666", Utils.convertStringToDate("2019-01-02 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"),null,1L);
    List<AdherenceData> adherenceDataList = new ArrayList<>();
    AdherenceData adherenceData1 = new AdherenceData(1L,Utils.convertStringToDate("2019-09-01 18:30:00"),Utils.convertStringToDate("2019-09-03 18:30:00"),1L,AdherenceTech.NNDOTS.getName(),1L,null,null,null,"66",Utils.convertStringToDate("2019-09-11 13:00:00"),null,null,0,0,0,null);
    AdherenceData adherenceData2 = new AdherenceData(1L,Utils.convertStringToDate("2019-09-03 18:30:00"),Utils.convertStringToDate("2019-09-05 18:30:00"),1L,AdherenceTech.NNDOTS.getName(),1L,null,null,null,"466",Utils.convertStringToDate("2019-09-11 13:00:00"),null, null, 0, 0, 0,null);
    adherenceDataList.add(adherenceData1);
    adherenceDataList.add(adherenceData2);
    when(registrationRepository.getOne(any())).thenReturn(registration1);
    Assert.assertEquals("6466",adherenceService.getEntityAdherenceString(adherenceDataList));
  }

  @Test(expected = ValidationException.class)
  public void deleteIamIdsByStopDateTestValidationException() throws ParseException {
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2020-04-05 13:00:00"), Utils.convertStringToDate("2019-09-05 13:00:00"), "1", "66666", Utils.convertStringToDate("2019-01-02 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"), null, 1L);
    Registration registration1 = new Registration(2L, 1L, Utils.convertStringToDate("2020-04-10 13:00:00"), Utils.convertStringToDate("2019-09-05 13:00:00"), "1", "66666", Utils.convertStringToDate("2019-01-02 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"), null, 1L);
    Registration registration2 = new Registration(3L, 1L, Utils.convertStringToDate("2020-04-15 13:00:00"), Utils.convertStringToDate("2019-09-05 13:00:00"), "1", "66666", Utils.convertStringToDate("2019-01-02 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"), null, 1L);
    List<Registration> registrations = new ArrayList<>(Arrays.asList(registration, registration1, registration2));
    List<Long> iamIds = registrations.stream().map(Registration::getId).collect(Collectors.toList());
    Date stopDate = Utils.convertStringToDate("2020-04-02 18:29:59");
    when(registrationRepository.findAllById(iamIds)).thenReturn(registrations);
    doNothing().when(registrationRepository).deleteAllByIdIn(anySet());
    adherenceService.deleteIamIdsByStopDate(iamIds, stopDate);
  }

  @Test
  public void deleteIamIdsByStopDateTest() throws ParseException {
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2020-04-05 13:00:00"), Utils.convertStringToDate("2019-09-05 13:00:00"), "1", "66666", Utils.convertStringToDate("2019-01-02 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"), null, 1L);
    Registration registration1 = new Registration(2L, 2L, Utils.convertStringToDate("2020-04-10 13:00:00"), Utils.convertStringToDate("2019-09-05 13:00:00"), "1", "66666", Utils.convertStringToDate("2019-01-02 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"), null, 1L);
    Registration registration2 = new Registration(3L, 3L, Utils.convertStringToDate("2020-04-15 13:00:00"), Utils.convertStringToDate("2019-09-05 13:00:00"), "1", "66666", Utils.convertStringToDate("2019-01-02 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"), null, 1L);
    List<Registration> registrations = new ArrayList<>(Arrays.asList(registration, registration1, registration2));
    List<Long> iamIds = registrations.stream().map(Registration::getId).collect(Collectors.toList());
    Date stopDate = Utils.convertStringToDate("2020-04-06 18:29:59");
    when(registrationRepository.findAllById(iamIds)).thenReturn(registrations);
    doNothing().when(registrationRepository).deleteAllByIdIn(anySet());
    doReturn(adherenceTestHandler).when(adherenceTechHandlerMap).getHandler(anyLong());
    List<Long> deletedIds = adherenceService.deleteIamIdsByStopDate(iamIds, stopDate);
    assertEquals(deletedIds.size(), 2);
    assertEquals(deletedIds.get(0).intValue(), 2);
    assertEquals(deletedIds.get(1).intValue(), 3);
    verify(adherenceTestHandler, Mockito.times(2)).deleteRegistration(anySet());
  }

  @Test
  public void calculateAdherenceDaysForClosedPatientsTest() throws ParseException {
    EntityAdherenceDto response1 =
            new EntityAdherenceDto("1", "46649", Utils.convertStringToDate("2019-09-01 18:30:00"),Utils.convertStringToDate("2019-09-06 18:29:59"));
    EntityAdherenceDto response2 =
            new EntityAdherenceDto("2", "994", Utils.convertStringToDate("2019-09-02 18:30:00"),Utils.convertStringToDate("2019-09-05 18:29:59"));
    List<EntityAdherenceDto> responseList = Arrays.asList(response1,response2);

    CumulativeDoseCountDto adherenceStats = adherenceService.calculateAdherenceDays(responseList, null);

    IntervalAdherenceDto intervalCount = adherenceStats.getAvgAdherenceIntervals().get(AverageAdherenceIntervals.ALL_TIME);
    assertEquals(intervalCount.getDigitalDoses(), 3L);
    assertEquals(intervalCount.getManualDoses(), 3L);
    assertEquals(intervalCount.getTotalDoses(), 8L);
  }

  @Test
  public void calculateAdherenceDaysForClosedPatientsIntervalsTest() throws ParseException {
    EntityAdherenceDto response1 =
            new EntityAdherenceDto("1", "496646946649", Utils.convertStringToDate("2019-09-01 18:30:00"),Utils.convertStringToDate("2019-09-13 18:29:59"));
    EntityAdherenceDto response2 =
            new EntityAdherenceDto("2", "6664999994", Utils.convertStringToDate("2019-09-02 18:30:00"),Utils.convertStringToDate("2019-09-12 18:29:59"));
    List<EntityAdherenceDto> responseList = Arrays.asList(response1,response2);

    CumulativeDoseCountDto adherenceStats = adherenceService.calculateAdherenceDays(responseList, null);

    IntervalAdherenceDto intervalCount = adherenceStats.getAvgAdherenceIntervals().get(AverageAdherenceIntervals.ALL_TIME);
    assertEquals(intervalCount.getDigitalDoses(), 6L);
    assertEquals(intervalCount.getManualDoses(), 8L);
    assertEquals(intervalCount.getTotalDoses(), 22L);

    intervalCount = adherenceStats.getAvgAdherenceIntervals().get(AverageAdherenceIntervals.LAST_7_DAYS);
    assertEquals(intervalCount.getDigitalDoses(), 4L);
    assertEquals(intervalCount.getManualDoses(), 7L);
    assertEquals(intervalCount.getTotalDoses(), 14L);

    intervalCount = adherenceStats.getAvgAdherenceIntervals().get(AverageAdherenceIntervals.LAST_30_DAYS);
    assertEquals(intervalCount.getDigitalDoses(), 6L);
    assertEquals(intervalCount.getManualDoses(), 8L);
    assertEquals(intervalCount.getTotalDoses(), 22L);
  }

  @Test
  public void calculateAdherenceDaysForClosedWeeklyPatientsIntervalsTest() throws ParseException {
    EntityAdherenceDto response1 =
            new EntityAdherenceDto("1", "47777779", Utils.convertStringToDate("2019-09-01 18:30:00"),Utils.convertStringToDate("2019-09-09 18:29:59"));
    EntityAdherenceDto response2 =
            new EntityAdherenceDto("2", "7767777774", Utils.convertStringToDate("2019-09-02 18:30:00"),Utils.convertStringToDate("2019-09-12 18:29:59"));
    List<EntityAdherenceDto> responseList = Arrays.asList(response1,response2);

    CumulativeDoseCountDto adherenceStats = adherenceService.calculateAdherenceDays(responseList, null);

    IntervalAdherenceDto intervalCount = adherenceStats.getAvgAdherenceIntervals().get(AverageAdherenceIntervals.ALL_TIME);
    assertEquals(intervalCount.getDigitalDoses(), 2L);
    assertEquals(intervalCount.getManualDoses(), 1L);
    assertEquals(intervalCount.getTotalDoses(), 4L);

    intervalCount = adherenceStats.getAvgAdherenceIntervals().get(AverageAdherenceIntervals.LAST_7_DAYS);
    assertEquals(intervalCount.getDigitalDoses(), 1L);
    assertEquals(intervalCount.getManualDoses(), 1L);
    assertEquals(intervalCount.getTotalDoses(), 2L);
  }

  @Test
  public void calculateAdherenceDaysForActivePatientsTest() throws ParseException {
    EntityAdherenceDto response1 =
            new EntityAdherenceDto("1", "46649", Utils.convertStringToDate("2019-09-01 18:30:00"),null);
    EntityAdherenceDto response2 =
            new EntityAdherenceDto("2", "994", Utils.convertStringToDate("2019-09-02 18:30:00"),null);
    List<EntityAdherenceDto> responseList = Arrays.asList(response1,response2);

    CumulativeDoseCountDto adherenceStats = adherenceService.calculateAdherenceDays(responseList, null);

    IntervalAdherenceDto intervalCount = adherenceStats.getAvgAdherenceIntervals().get(AverageAdherenceIntervals.ALL_TIME);
    assertEquals(intervalCount.getDigitalDoses(), 3L);
    assertEquals(intervalCount.getManualDoses(), 3L);
    long totalDoses = 0L;
    totalDoses+= Utils.getDifferenceDays(response1.getStartDate(),Utils.getCurrentDate())+1;
    totalDoses+= Utils.getDifferenceDays(response2.getStartDate(),Utils.getCurrentDate())+1;
    assertEquals(intervalCount.getTotalDoses(), totalDoses);

    intervalCount = adherenceStats.getAvgAdherenceIntervals().get(AverageAdherenceIntervals.LAST_7_DAYS);
    assertEquals(intervalCount.getDigitalDoses(), 0L);
    assertEquals(intervalCount.getManualDoses(), 0L);
    assertEquals(intervalCount.getTotalDoses(), 14L);
  }

  @Test
  public void calculateAdherenceDaysPatientResponseListTest() throws ParseException {
    EntityAdherenceDto response1 =
            new EntityAdherenceDto("1", "46649", Utils.convertStringToDate("2019-09-01 18:30:00"),Utils.convertStringToDate("2019-09-06 18:29:59"));
    EntityAdherenceDto response2 =
            new EntityAdherenceDto("2", "994", Utils.convertStringToDate("2019-09-02 18:30:00"),Utils.convertStringToDate("2019-09-05 18:29:59"));
    List<EntityAdherenceDto> responseList = Arrays.asList(response1,response2);
    List<AvgAdherenceResponse> avgAdherenceResponseList = new ArrayList<>();
    CumulativeDoseCountDto adherenceStats = adherenceService.calculateAdherenceDays(responseList, avgAdherenceResponseList);

    IntervalAdherenceDto intervalCount = adherenceStats.getAvgAdherenceIntervals().get(AverageAdherenceIntervals.ALL_TIME);
    assertEquals(intervalCount.getDigitalDoses(), 3);
    assertEquals(intervalCount.getManualDoses(), 3);
    assertEquals(intervalCount.getTotalDoses(), 8);
    assertEquals(avgAdherenceResponseList.size(), 2);
    assertEquals(avgAdherenceResponseList.get(0).getDigitalAdherence(), 40, 10e-2);
    assertEquals(avgAdherenceResponseList.get(0).getTotalAdherence(), 60, 10e-2);
    assertEquals(avgAdherenceResponseList.get(1).getDigitalAdherence(), 33.3, 10e-2);
    assertEquals(avgAdherenceResponseList.get(1).getTotalAdherence(), 100, 10e-2);
  }

  @Test
  public void getDoseCountFromCacheTest() {
    String correctAdherenceJsonString = "{ " +
              "\"entityId\": \"1\", " +
              "\"avgAdherenceIntervals\" : " +
                "{ " +
                    "\"ALL_TIME\" : {\"digitalDoses\": \"10\", \"manualDoses\": \"20\", \"totalDoses\": \"50\"}, " +
                    "\"LAST_7_DAYS\" : {\"digitalDoses\": \"2\", \"manualDoses\": \"4\", \"totalDoses\": \"7\"} " +
                "}" +
            "}";
    when(valueOperations.multiGet(any())).thenReturn(Arrays.asList(correctAdherenceJsonString));
    AdherenceCacheDataDto<EntityDoseCountDto> avgAdherenceCacheData = adherenceService.getAdherenceFromCache(
            Arrays.asList("1"), Constants.CACHE_AVG_ADHERENCE_PREFIX,
            (cacheEntry, allIdsSet) -> adherenceService.formatCacheForAvgAdherence(cacheEntry, allIdsSet));
    EntityDoseCountDto data = avgAdherenceCacheData.getAdherenceResponse().get(0);

    IntervalAdherenceDto intervalCount = data.getAvgAdherenceIntervals().get(AverageAdherenceIntervals.ALL_TIME);
    assertEquals(intervalCount.getDigitalDoses(), 10L);
    assertEquals(intervalCount.getManualDoses(), 20L);
    assertEquals(intervalCount.getTotalDoses(), 50L);

    intervalCount = data.getAvgAdherenceIntervals().get(AverageAdherenceIntervals.LAST_7_DAYS);
    assertEquals(intervalCount.getDigitalDoses(), 2L);
    assertEquals(intervalCount.getManualDoses(), 4L);
    assertEquals(intervalCount.getTotalDoses(), 7L);
  }

  @Test
  public void formatCacheForAvgAdherenceTest() {
    String correctAdherenceJsonString = "{ " +
            "\"entityId\": \"1\", " +
            "\"avgAdherenceIntervals\" : " +
            "{ " +
            "\"ALL_TIME\" : {\"digitalDoses\": \"10\", \"manualDoses\": \"20\", \"totalDoses\": \"50\"}, " +
            "\"LAST_7_DAYS\" : {\"digitalDoses\": \"2\", \"manualDoses\": \"4\", \"totalDoses\": \"7\"} " +
            "}" +
            "}";
    Set<String> idsSet = new HashSet<>(Collections.singletonList("1"));
    EntityDoseCountDto data = adherenceService.formatCacheForAvgAdherence(correctAdherenceJsonString, idsSet);
    IntervalAdherenceDto intervalCount = data.getAvgAdherenceIntervals().get(AverageAdherenceIntervals.ALL_TIME);
    assertEquals(intervalCount.getDigitalDoses(), 10L);
    assertEquals(intervalCount.getManualDoses(), 20L);
    assertEquals(intervalCount.getTotalDoses(), 50L);
    assertEquals(idsSet.size(), 0);
  }

  @Test
  public void formatCacheForAdherenceTest() {
    String correctAdherenceJsonString = "{ \"entityId\": \"286434\", \"adherenceData\": [ { \"iamId\": 184853, \"startDate\": \"01-02-2020 22:00:00\", \"adherenceType\": 3, \"adherenceTypeString\": \"MERM Technology\", \"scheduleType\": 1, \"scheduleTypeString\": \"Daily\", \"currentSchedule\": \"\", \"uniqueIdentifier\": \"862810032579590\", \"adherenceString\": \"44556A99B6\", \"createdDate\": \"26-02-2020 12:32:59\", \"lastDosage\": \"12-05-2020 22:00:00\" } ], \"adherenceString\": \"44556A99B6\", \"lastDosage\": \"12-05-2020 22:00:00\" }";
    Set<String> idsSet = new HashSet<>(Collections.singletonList("286434"));
    AdherenceResponse cacheResponse = adherenceService.formatCacheForAdherence(correctAdherenceJsonString, idsSet);
    AdherenceData data = cacheResponse.getAdherenceData().get(0);
    assertEquals(data.getAdherenceString(), "44556A99B6");
    assertEquals(String.valueOf(data.getAdherenceType()), "3");
    assertEquals(String.valueOf(data.getIamId()), "184853");
    assertEquals(idsSet.size(), 0);
  }

  @Test
  public void getAllConfigNamesTest()
  {
    List<String> expectedConfigNameList = Arrays.stream(AdherenceConfig.values())
            .map(Enum::toString)
            .collect(Collectors.toList());

    List<String> configNameList = adherenceService.getAllConfigNames();
    assertEquals(expectedConfigNameList.size(), configNameList.size());
    assertEquals(expectedConfigNameList, configNameList);
  }

  @Test
  public void getAdherenceColorCodeConfig()
  {
    List<AdherenceCodeEnum> adherenceCodeEnumList = Arrays.stream(AdherenceCodeEnum.values())
            .collect(Collectors.toList());

    List<AdherenceCodeConfigResponse> adherenceCodeConfigResponseList = adherenceService.getAdherenceColorCodeConfig();

    AdherenceCodeEnum missedAdherenceCodeEnum = adherenceCodeEnumList
            .stream().filter(i->i.getCode() == '2').collect(Collectors.toList()).get(0);

    AdherenceCodeConfigResponse missedAdherenceCodeConfigResponse = adherenceCodeConfigResponseList
            .stream().filter(i->i.getCode() == '2').collect(Collectors.toList()).get(0);

    assertEquals(adherenceCodeEnumList.size(), adherenceCodeConfigResponseList.size());
    assertEquals(missedAdherenceCodeEnum.getTrackingEventName(), missedAdherenceCodeConfigResponse.getCodeName());
    assertEquals(missedAdherenceCodeEnum.getDescription(), missedAdherenceCodeConfigResponse.getLegendText());
    assertEquals(missedAdherenceCodeEnum.getDesign(), missedAdherenceCodeConfigResponse.getDesign());
  }

  @Test
  public void getAdherenceTechConfigTest()
  {
    List<AdherenceTech> adherenceTechList = Arrays.stream(AdherenceTech.values())
            .collect(Collectors.toList());

    List<GenericConfigResponse> genericConfigResponseList = adherenceService.getAdherenceTechConfig();

    AdherenceTech mermAdherenceTech =  adherenceTechList.stream().filter(i->i.getName().equals("MERM Technology"))
                    .collect(Collectors.toList()).get(0);

    GenericConfigResponse mermGenericConfigResponse = genericConfigResponseList.stream()
                    .filter(i->i.getName().equals("MERM Technology")).collect(Collectors.toList()).get(0);

    assertEquals(adherenceTechList.size(), genericConfigResponseList.size());
    assertEquals(mermAdherenceTech.getId(), mermGenericConfigResponse.getId().intValue());
  }


  @Test
  public void getScheduledTypeConfigTest()
  {
    List<ScheduleTypeEnum> scheduleTypeEnumList = Arrays.stream(ScheduleTypeEnum.values())
            .collect(Collectors.toList());

    List<GenericConfigResponse> genericConfigResponseList = adherenceService.getScheduleTypeConfig();

    ScheduleTypeEnum dailyScheduleType =  scheduleTypeEnumList.stream().filter(i->i.getName().equals("Daily"))
            .collect(Collectors.toList()).get(0);

    GenericConfigResponse dailyGenericConfigResponse = genericConfigResponseList.stream()
            .filter(i->i.getName().equals("Daily")).collect(Collectors.toList()).get(0);

    assertEquals(scheduleTypeEnumList.size(), genericConfigResponseList.size());
    assertEquals(dailyScheduleType.getId(), dailyGenericConfigResponse.getId());
  }

  @Test
  public void getAdherenceGlobalConfigForWithConfigNameListInputTest()
  {
    List<String> configNameList = new ArrayList<>();
    configNameList.add("ADHERENCE_TECH");
    configNameList.add("COLOR_CODE");
    configNameList.add("SCHEDULE_TYPE");
    AdherenceGlobalConfigRequest adherenceGlobalConfigRequest = new AdherenceGlobalConfigRequest(configNameList);
    List<AdherenceCodeConfigResponse> adherenceCodeConfigResponseList = new ArrayList<>();
    adherenceCodeConfigResponseList.add(new AdherenceCodeConfigResponse('2', "MISSED", new AdherenceCodeExtra(), "Missed doses"));
    List<GenericConfigResponse> adherenceTechResponseList = new ArrayList<>();
    adherenceTechResponseList.add(new GenericConfigResponse("MERM", 1));
    List<GenericConfigResponse> scheduleTypeResponseList = new ArrayList<>();
    adherenceTechResponseList.add(new GenericConfigResponse("Daily", 1));

    doReturn(adherenceCodeConfigResponseList).when(adherenceService).getAdherenceColorCodeConfig();
    doReturn(adherenceTechResponseList).when(adherenceService).getAdherenceTechConfig();
    doReturn(scheduleTypeResponseList).when(adherenceService).getScheduleTypeConfig();

    AdherenceGlobalConfigResponse adherenceGlobalConfigResponse = adherenceService.getAdherenceGlobalConfig(adherenceGlobalConfigRequest);

    assertEquals(adherenceCodeConfigResponseList.size(), adherenceGlobalConfigResponse.getAdherenceCodeConfigResponseList().size());
    assertEquals(scheduleTypeResponseList.size(), adherenceGlobalConfigResponse.getScheduleTypeConfigResponseList().size());
    assertEquals(adherenceTechResponseList.size(), adherenceGlobalConfigResponse.getAdherenceTechConfigResponseList().size());
    verify(adherenceService, Mockito.times(1)).getAdherenceTechConfig();
    verify(adherenceService, Mockito.times(1)).getAdherenceColorCodeConfig();
    verify(adherenceService, Mockito.times(1)).getScheduleTypeConfig();
  }

  @Test
  public void getAdherenceGlobalConfigForNullConfigNameListTest()
  {
    AdherenceGlobalConfigRequest adherenceGlobalConfigRequest = new AdherenceGlobalConfigRequest();
    List<AdherenceCodeConfigResponse> adherenceCodeConfigResponseList = new ArrayList<>();
    adherenceCodeConfigResponseList.add(new AdherenceCodeConfigResponse('2', "MISSED", new AdherenceCodeExtra(), "Missed doses"));
    List<GenericConfigResponse> adherenceTechResponseList = new ArrayList<>();
    adherenceTechResponseList.add(new GenericConfigResponse("MERM", 1));
    List<GenericConfigResponse> scheduleTypeResponseList = new ArrayList<>();
    adherenceTechResponseList.add(new GenericConfigResponse("Daily", 1));

    doReturn(adherenceCodeConfigResponseList).when(adherenceService).getAdherenceColorCodeConfig();
    doReturn(adherenceTechResponseList).when(adherenceService).getAdherenceTechConfig();
    doReturn(scheduleTypeResponseList).when(adherenceService).getScheduleTypeConfig();

    AdherenceGlobalConfigResponse adherenceGlobalConfigResponse = adherenceService.getAdherenceGlobalConfig(adherenceGlobalConfigRequest);

    assertEquals(adherenceCodeConfigResponseList.size(), adherenceGlobalConfigResponse.getAdherenceCodeConfigResponseList().size());
    assertEquals(scheduleTypeResponseList.size(), adherenceGlobalConfigResponse.getScheduleTypeConfigResponseList().size());
    assertEquals(adherenceTechResponseList.size(), adherenceGlobalConfigResponse.getAdherenceTechConfigResponseList().size());
    verify(adherenceService, Mockito.times(1)).getAdherenceTechConfig();
    verify(adherenceService, Mockito.times(1)).getAdherenceColorCodeConfig();
    verify(adherenceService, Mockito.times(1)).getScheduleTypeConfig();
  }


  @Test
  public void getAdherenceGlobalConfigForEmptyConfigNameListTest()
  {
    List<String> configNameList = new ArrayList<>();
    AdherenceGlobalConfigRequest adherenceGlobalConfigRequest = new AdherenceGlobalConfigRequest(configNameList);
    List<AdherenceCodeConfigResponse> adherenceCodeConfigResponseList = new ArrayList<>();
    adherenceCodeConfigResponseList.add(new AdherenceCodeConfigResponse('2', "MISSED", new AdherenceCodeExtra(), "Missed doses"));
    List<GenericConfigResponse> adherenceTechResponseList = new ArrayList<>();
    adherenceTechResponseList.add(new GenericConfigResponse("MERM", 1));
    List<GenericConfigResponse> scheduleTypeResponseList = new ArrayList<>();
    adherenceTechResponseList.add(new GenericConfigResponse("Daily", 1));

    doReturn(adherenceCodeConfigResponseList).when(adherenceService).getAdherenceColorCodeConfig();
    doReturn(adherenceTechResponseList).when(adherenceService).getAdherenceTechConfig();
    doReturn(scheduleTypeResponseList).when(adherenceService).getScheduleTypeConfig();

    AdherenceGlobalConfigResponse adherenceGlobalConfigResponse = adherenceService.getAdherenceGlobalConfig(adherenceGlobalConfigRequest);

    assertEquals(adherenceCodeConfigResponseList.size(), adherenceGlobalConfigResponse.getAdherenceCodeConfigResponseList().size());
    assertEquals(scheduleTypeResponseList.size(), adherenceGlobalConfigResponse.getScheduleTypeConfigResponseList().size());
    assertEquals(adherenceTechResponseList.size(), adherenceGlobalConfigResponse.getAdherenceTechConfigResponseList().size());
    verify(adherenceService, Mockito.times(1)).getAdherenceTechConfig();
    verify(adherenceService, Mockito.times(1)).getAdherenceColorCodeConfig();
    verify(adherenceService, Mockito.times(1)).getScheduleTypeConfig();
  }

  @Test
  public void testGetDoseTime() {
      when(scheduleTypeHandlerMap.getHandler(eq(3L))).thenReturn(multiFreqWeeklyScheduleHandler);
      when(multiFreqWeeklyScheduleHandler.getDoseTimes(eq(1L))).thenReturn(Collections.singletonList(new DoseTimeData(Utils.getCurrentDate(), Utils.getCurrentDate(),  Collections.singletonList("03:30:00"))));

      List<DoseTimeData> doseTimeList = adherenceService.getDoseTimeForEntity(1L, 3L);
      Assert.assertEquals("03:30:00", doseTimeList.get(0).getDoseTimeList().get(0));
  }

  @Test
  public void testIsDoseTimeSupported() {
    when(scheduleTypeHandlerMap.getHandler(eq(3L))).thenReturn(multiFreqWeeklyScheduleHandler);
    when(multiFreqWeeklyScheduleHandler.isDoseTimeSupported()).thenReturn(true);

    boolean doseTime = adherenceService.isDoseTimeSupportedForScheduleType(3L);
    assertTrue(doseTime);
  }

}

package tests.unit.service;

import com.everwell.iam.enums.AverageAdherenceIntervals;
import com.everwell.iam.enums.RangeFilterType;
import com.everwell.iam.handlers.AdhTechHandlerMap;
import com.everwell.iam.handlers.ScheduleTypeHandlerMap;
import com.everwell.iam.handlers.impl.DailyScheduleHandler;
import com.everwell.iam.models.db.AccessMapping;
import com.everwell.iam.models.db.Registration;
import com.everwell.iam.models.dto.*;
import com.everwell.iam.models.http.responses.AdherenceResponse;
import com.everwell.iam.models.http.responses.AllAvgAdherenceResponse;
import com.everwell.iam.models.http.responses.AvgAdherenceResponse;
import com.everwell.iam.repositories.RegistrationRepository;
import com.everwell.iam.repositories.RegistrationShortRepo;
import com.everwell.iam.services.AdherenceService;
import com.everwell.iam.services.impl.AdherenceServiceImpl;
import com.everwell.iam.utils.CacheUtils;
import com.everwell.iam.utils.Utils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import tests.BaseTest;
import tests.BaseTestNoSpring;
import tests.unit.handlersTest.AdherenceTestHandler;

import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.doNothing;

//adherence service test class which does not depend on spring beans
//will remove bean dependency from original adherence service in a separate task
public class AdherenceServiceNoBeanTest extends BaseTestNoSpring {

    @Spy
    @InjectMocks
    AdherenceServiceImpl adherenceService;

    @Spy
    @InjectMocks
    AdherenceTestHandler adherenceTestHandler;

    @Mock
    RegistrationRepository registrationRepository;

    CacheUtils cacheUtils;

    @Mock
    RedisTemplate<String, Object> redisTemplate;

    @Mock
    private ValueOperations<String, Object> valueOperations;

    @Mock
    private ScheduleTypeHandlerMap scheduleTypeHandlerMap;

    @Mock
    private AdhTechHandlerMap adherenceTechHandlerMap;

    @Mock
    DailyScheduleHandler mockDailyHandler;

    @Before
    public void init() {
        //cache mocks
        cacheUtils = new CacheUtils(redisTemplate);
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
        doNothing().when(valueOperations).multiSet(any());
        doReturn(null).when(redisTemplate).execute(any(), any(), any(), any(), any());
    }

    private Map<AverageAdherenceIntervals, IntervalAdherenceDto> getDummyAvgAdherenceCountIntervals () {
        HashMap<AverageAdherenceIntervals, IntervalAdherenceDto> dummyAverageAdherenceIntervals = new HashMap<>();
        dummyAverageAdherenceIntervals.put(AverageAdherenceIntervals.ALL_TIME, new IntervalAdherenceDto(75,25,150));
        dummyAverageAdherenceIntervals.put(AverageAdherenceIntervals.LAST_7_DAYS, new IntervalAdherenceDto(5,15,25));
        dummyAverageAdherenceIntervals.put(AverageAdherenceIntervals.LAST_30_DAYS, new IntervalAdherenceDto(10,15,50));
        dummyAverageAdherenceIntervals.put(AverageAdherenceIntervals.LAST_90_DAYS, new IntervalAdherenceDto(15,15,60));
        dummyAverageAdherenceIntervals.put(AverageAdherenceIntervals.LAST_365_DAYS, new IntervalAdherenceDto(25,50,100));
        return dummyAverageAdherenceIntervals;
    }

    private Map<AverageAdherenceIntervals, IntervalAdherenceDto> getDummyAvgAdherenceCountIntervals2 () {
        HashMap<AverageAdherenceIntervals, IntervalAdherenceDto> dummyAverageAdherenceIntervals = new HashMap<>();
        dummyAverageAdherenceIntervals.put(AverageAdherenceIntervals.ALL_TIME, new IntervalAdherenceDto(20,30,50));
        dummyAverageAdherenceIntervals.put(AverageAdherenceIntervals.LAST_7_DAYS, new IntervalAdherenceDto(5,2,7));
        dummyAverageAdherenceIntervals.put(AverageAdherenceIntervals.LAST_30_DAYS, new IntervalAdherenceDto(10,20,30));
        dummyAverageAdherenceIntervals.put(AverageAdherenceIntervals.LAST_90_DAYS, new IntervalAdherenceDto(20,30,50));
        dummyAverageAdherenceIntervals.put(AverageAdherenceIntervals.LAST_365_DAYS, new IntervalAdherenceDto(20,30,50));
        return dummyAverageAdherenceIntervals;
    }

    @Test
    public void getAdherenceBulkTest_OnlyCacheData() {
        //setup
        Map<String, List<AccessMapping>> accessMappingToEntityMap = new HashMap<>();
        List<AdherenceResponse> adherenceResponse = Arrays.asList(
          new AdherenceResponse("1", null, "66664444", null, 10, 12, 22, null, null, null, 12,11, new ArrayList<>()),
          new AdherenceResponse("12", null, "96443366", null, 10, 12, 22, null, null, null, 12,11, new ArrayList<>())
        );
        AdherenceCacheDataDto<AdherenceResponse> adherenceCacheDataDto = new AdherenceCacheDataDto<AdherenceResponse>();
        adherenceCacheDataDto.setEntityIds(new ArrayList<>());
        adherenceCacheDataDto.setAdherenceResponse(adherenceResponse);

        //mocking
        doReturn(adherenceCacheDataDto).when(adherenceService).getAdherenceFromCache(anyList(), anyString(), any());

        //to test
        List<AdherenceResponse> responses = adherenceService.getAdherenceBulk(accessMappingToEntityMap, null);

        //assertions
        Assert.assertEquals(responses.get(0).getAdherenceString(), "66664444");
        Assert.assertEquals(responses.get(1).getAdherenceString(), "96443366");
    }

    @Test
    public void getAdherenceBulkTest_CacheAndDbData() {
        //setup
        Map<String, List<AccessMapping>> accessMappingToEntityMap = new HashMap<>();
        List<AccessMapping> accessMapping = Arrays.asList(
            new AccessMapping(29L, 1L, "123")
        );
        accessMappingToEntityMap.put("123", accessMapping);
        List<AdherenceResponse> adherenceResponse = Arrays.asList(
            new AdherenceResponse("1", null, "66664444", null, 10, 12, 22, null, null, null, 12,11, new ArrayList<>()),
            new AdherenceResponse("12", null, "96443366", null, 10, 12, 22, null, null, null, 12,11, new ArrayList<>())
        );
        AdherenceCacheDataDto<AdherenceResponse> adherenceCacheDataDto = new AdherenceCacheDataDto<AdherenceResponse>();
        adherenceCacheDataDto.setEntityIds(Arrays.asList("123"));
        adherenceCacheDataDto.setAdherenceResponse(adherenceResponse);
        List<AdherenceData> adherenceData = Arrays.asList(
            new AdherenceData()
        );
        adherenceData.get(0).setStartDate(new Date());
        AdherenceResponse adherenceResponseNotFromCache = new AdherenceResponse("122", adherenceData, "6666449966", null, 10, 12, 22, null, null, null, 12,11, new ArrayList<>());

        //mocking
        doReturn(adherenceResponseNotFromCache).when(adherenceService).getAllAdherence(anyList(), anyBoolean());
        doReturn(adherenceCacheDataDto).when(adherenceService).getAdherenceFromCache(anyList(), anyString(), any());
        doReturn(new EntityDoseCountDto("1")).when(adherenceService).generateEntityDoseCountStats(any());

        //to test
        List<AdherenceResponse> responses = adherenceService.getAdherenceBulk(accessMappingToEntityMap, null);

        //assertions
        Assert.assertEquals(responses.get(0).getAdherenceString(), "6666449966");
        Assert.assertEquals(responses.get(1).getAdherenceString(), "66664444");
        Assert.assertEquals(responses.get(2).getAdherenceString(), "96443366");
    }

    @Test
    public void getAdherenceBulkTest_CacheAndDbDataWithFilters() {
        //setup
        Map<String, List<AccessMapping>> accessMappingToEntityMap = new HashMap<>();
        List<AccessMapping> accessMapping = Arrays.asList(
            new AccessMapping(29L, 1L, "123")
        );
        accessMappingToEntityMap.put("123", accessMapping);
        List<AdherenceResponse> adherenceResponse = Arrays.asList(
            new AdherenceResponse("1", null, "66664444", null, 10, 12, 22, null, null, null, 12,11, new ArrayList<>()),
            new AdherenceResponse("12", null, "96443366", null, 10, 12, 22, null, null, null, 12,11, new ArrayList<>())
        );
        AdherenceCacheDataDto<AdherenceResponse> adherenceCacheDataDto = new AdherenceCacheDataDto<AdherenceResponse>();
        adherenceCacheDataDto.setEntityIds(Arrays.asList("123"));
        adherenceCacheDataDto.setAdherenceResponse(adherenceResponse);
        List<AdherenceData> adherenceData = Arrays.asList(
            new AdherenceData()
        );
        adherenceData.get(0).setStartDate(new Date());
        AdherenceResponse adherenceResponseNotFromCache = new AdherenceResponse("122", adherenceData, "6666449966", null, 10, 12, 22, null, null, null, 12,11, new ArrayList<>());
        List<RangeFilters> rangeFilters = Arrays.asList(
            new RangeFilters(RangeFilterType.LAST_DOSAGE, "?", "?")
        );

        //mocking
        doReturn(adherenceResponseNotFromCache).when(adherenceService).getAllAdherence(anyList(), anyBoolean());
        doReturn(adherenceCacheDataDto).when(adherenceService).getAdherenceFromCache(anyList(), anyString(), any());
        doReturn(Arrays.asList(adherenceResponseNotFromCache)).when(adherenceService).filterList(anyList(), anyList());
        doReturn(new EntityDoseCountDto("1")).when(adherenceService).generateEntityDoseCountStats(any());

        //to test
        List<AdherenceResponse> adherenceResponseList = adherenceService.getAdherenceBulk(accessMappingToEntityMap, rangeFilters);

        //assertions
        Assert.assertEquals(adherenceResponseList.size(), 1);
        Assert.assertEquals(adherenceResponseList.get(0).getAdherenceString(), "6666449966");
    }

    @Test
    public void testStopAdherence() throws ParseException {
        Registration registration1 = new Registration(1L, 1L, Utils.convertStringToDate("2019-09-11 13:00:00"), null, "1", "666666", Utils.convertStringToDate("2019-01-01 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"),null,1L);
        Date endDate = Utils.convertStringToDate("2019-10-02 01:00:00");

        when(registrationRepository.findById(any())).thenReturn(Optional.of(registration1));
        when(registrationRepository.save(any())).thenReturn(null);
        when(scheduleTypeHandlerMap.getHandler(any())).thenReturn(mockDailyHandler);
        doNothing().when(mockDailyHandler).stopSchedule(any(),any());
        when(adherenceTechHandlerMap.getHandler(any())).thenReturn(adherenceTestHandler);
        doReturn(new AdherenceResponse()).when(adherenceService).getAdherence(any(Registration.class), anyBoolean());

        Assert.assertTrue(adherenceService.stopAdherence(1L, null));
        Assert.assertTrue(adherenceService.stopAdherence(1L, endDate));
        Assert.assertTrue(adherenceService.stopAdherence(1L));

        Registration registration2 = new Registration(1L, 1L, Utils.convertStringToDate("2019-09-11 13:00:00"), null, "1", "666666", Utils.convertStringToDate("2019-01-01 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"),Utils.convertStringToDate("2019-10-01 01:00:00"),1L);
        when(registrationRepository.findById(any())).thenReturn(Optional.of(registration2));
        Assert.assertTrue(adherenceService.stopAdherence(1L, endDate));

        Registration registration3 = new Registration(1L, 1L, Utils.convertStringToDate("2019-09-11 13:00:00"), null, "1", "666666", Utils.convertStringToDate("2019-01-01 12:23:26"), Utils.convertStringToDate("2019-01-10 12:23:26"),Utils.convertStringToDate("2019-10-03 01:00:00"),1L);
        when(registrationRepository.findById(any())).thenReturn(Optional.of(registration3));
        Assert.assertTrue(adherenceService.stopAdherence(1L, endDate));
    }

    @Test
    public void getAverageAdherenceWithoutCacheTest() throws ParseException {
        List<String> entityIds = new ArrayList<>();
        String entityId  = "123";
        RegistrationShortRepo dummyRegistration = new AdherenceData(1L, "49966", Utils.convertStringToDate("2019-01-04 18:30:00"), Utils.convertStringToDate("2019-01-09 18:29:59"), Utils.convertStringToDate("2019-01-04 18:30:00"), 1L);
        entityIds.add(entityId);
        Map<String, List<AccessMapping>> accessMappingToEntityMap = new HashMap<>();
        List<AccessMapping> accessMapping = Arrays.asList(
                new AccessMapping(29L, 1L, entityId)
        );
        accessMappingToEntityMap.put(entityId, accessMapping);
        CumulativeDoseCountDto adherenceStats = new CumulativeDoseCountDto(getDummyAvgAdherenceCountIntervals());

        doReturn(new EntityAdherenceDto()).when(adherenceService).getEntityAdherence(any(), anyList());
        doReturn(adherenceStats).when(adherenceService).calculateAdherenceDays(anyList(), any());
        doReturn(new AdherenceCacheDataDto<EntityDoseCountDto>(new ArrayList(), entityIds)).when(adherenceService).getAdherenceFromCache(anyList(), anyString(), any());
        when(registrationRepository.getAdherenceDetailsFromIamId(anyList())).thenReturn(Collections.singletonList(dummyRegistration));

        boolean patientDetailsRequired = false;
        AllAvgAdherenceResponse avgAdherenceResponse = adherenceService.getAverageAdherence(accessMappingToEntityMap, patientDetailsRequired, null);

        Assert.assertEquals(avgAdherenceResponse.getDigitalAdherence(), 50,10e-2);
        Assert.assertEquals(avgAdherenceResponse.getTotalAdherence(), 66.66,10e-2);
    }

    @Test
    public void getAverageAdherenceWithoutCacheIntervalTest() throws ParseException {
        List<String> entityIds = new ArrayList<>();
        String entityId  = "123";
        RegistrationShortRepo dummyRegistration = new AdherenceData(1L, "49966", Utils.convertStringToDate("2019-01-04 18:30:00"), Utils.convertStringToDate("2019-01-09 18:29:59"), Utils.convertStringToDate("2019-01-04 18:30:00"), 1L);
        entityIds.add(entityId);
        Map<String, List<AccessMapping>> accessMappingToEntityMap = new HashMap<>();
        List<AccessMapping> accessMapping = Arrays.asList(
                new AccessMapping(29L, 1L, entityId)
        );
        accessMappingToEntityMap.put(entityId, accessMapping);
        Map<AverageAdherenceIntervals,IntervalAdherenceDto> avgAdherenceCountIntervals = getDummyAvgAdherenceCountIntervals();
        CumulativeDoseCountDto adherenceStats = new CumulativeDoseCountDto(avgAdherenceCountIntervals);

        doReturn(new EntityAdherenceDto()).when(adherenceService).getEntityAdherence(any(), anyList());
        doReturn(adherenceStats).when(adherenceService).calculateAdherenceDays(anyList(), any());
        doReturn(new AdherenceCacheDataDto<EntityDoseCountDto>(new ArrayList(), entityIds)).when(adherenceService).getAdherenceFromCache(anyList(), anyString(), any());
        when(registrationRepository.getAdherenceDetailsFromIamId(anyList())).thenReturn(Collections.singletonList(dummyRegistration));

        boolean patientDetailsRequired = false;
        List<AverageAdherenceIntervals> allIntervals = Arrays.stream(AverageAdherenceIntervals.values()).collect(Collectors.toList());
        AllAvgAdherenceResponse avgAdherenceResponse = adherenceService.getAverageAdherence(accessMappingToEntityMap, patientDetailsRequired, allIntervals);

        Map<AverageAdherenceIntervals, IntervalAdherencePercentDto> averageAdherenceIntervals = avgAdherenceResponse.getAverageAdherenceIntervals();

        double expectedDigital, expectedTotal = 0;
        IntervalAdherencePercentDto intervalPercent;
        IntervalAdherenceDto intervalCount;

        intervalPercent = averageAdherenceIntervals.get(AverageAdherenceIntervals.ALL_TIME);
        intervalCount = avgAdherenceCountIntervals.get(AverageAdherenceIntervals.ALL_TIME);
        expectedDigital = intervalCount.getDigitalDoses()*100.0/intervalCount.getTotalDoses();
        expectedTotal = (intervalCount.getDigitalDoses()+intervalCount.getManualDoses())*100.0/ intervalCount.getTotalDoses();
        Assert.assertEquals(expectedDigital, intervalPercent.getDigitalAdherence(),10e-2);
        Assert.assertEquals(expectedTotal, intervalPercent.getTotalAdherence(),10e-2);

        intervalPercent = averageAdherenceIntervals.get(AverageAdherenceIntervals.LAST_7_DAYS);
        intervalCount = avgAdherenceCountIntervals.get(AverageAdherenceIntervals.LAST_7_DAYS);
        expectedDigital = intervalCount.getDigitalDoses()*100.0/intervalCount.getTotalDoses();
        expectedTotal = (intervalCount.getDigitalDoses()+intervalCount.getManualDoses())*100.0/ intervalCount.getTotalDoses();
        Assert.assertEquals(expectedDigital, intervalPercent.getDigitalAdherence(),10e-2);
        Assert.assertEquals(expectedTotal, intervalPercent.getTotalAdherence(),10e-2);

        intervalPercent = averageAdherenceIntervals.get(AverageAdherenceIntervals.LAST_30_DAYS);
        intervalCount = avgAdherenceCountIntervals.get(AverageAdherenceIntervals.LAST_30_DAYS);
        expectedDigital = intervalCount.getDigitalDoses()*100.0/intervalCount.getTotalDoses();
        expectedTotal = (intervalCount.getDigitalDoses()+intervalCount.getManualDoses())*100.0/ intervalCount.getTotalDoses();
        Assert.assertEquals(expectedDigital, intervalPercent.getDigitalAdherence(),10e-2);
        Assert.assertEquals(expectedTotal, intervalPercent.getTotalAdherence(),10e-2);

        intervalPercent = averageAdherenceIntervals.get(AverageAdherenceIntervals.LAST_90_DAYS);
        intervalCount = avgAdherenceCountIntervals.get(AverageAdherenceIntervals.LAST_90_DAYS);
        expectedDigital = intervalCount.getDigitalDoses()*100.0/intervalCount.getTotalDoses();
        expectedTotal = (intervalCount.getDigitalDoses()+intervalCount.getManualDoses())*100.0/ intervalCount.getTotalDoses();
        Assert.assertEquals(expectedDigital, intervalPercent.getDigitalAdherence(),10e-2);
        Assert.assertEquals(expectedTotal, intervalPercent.getTotalAdherence(),10e-2);

        intervalPercent = averageAdherenceIntervals.get(AverageAdherenceIntervals.LAST_365_DAYS);
        intervalCount = avgAdherenceCountIntervals.get(AverageAdherenceIntervals.LAST_365_DAYS);
        expectedDigital = intervalCount.getDigitalDoses()*100.0/intervalCount.getTotalDoses();
        expectedTotal = (intervalCount.getDigitalDoses()+intervalCount.getManualDoses())*100.0/ intervalCount.getTotalDoses();
        Assert.assertEquals(expectedDigital, intervalPercent.getDigitalAdherence(),10e-2);
        Assert.assertEquals(expectedTotal, intervalPercent.getTotalAdherence(),10e-2);
    }

    @Test
    public void getAverageAdherenceWithoutCacheTestForWeeklySchedule() throws ParseException {
        List<String> entityIds = new ArrayList<>();
        String entityId  = "123";
        RegistrationShortRepo dummyRegistration = new AdherenceData(1L, "4777777977777", Utils.convertStringToDate("2019-01-04 18:30:00"), Utils.convertStringToDate("2019-01-17 18:29:59"), Utils.convertStringToDate("2019-01-04 18:30:00"), 2L);
        entityIds.add(entityId);
        Map<String, List<AccessMapping>> accessMappingToEntityMap = new HashMap<>();
        List<AccessMapping> accessMapping = Arrays.asList(
                new AccessMapping(29L, 1L, entityId)
        );
        accessMappingToEntityMap.put(entityId, accessMapping);
        CumulativeDoseCountDto adherenceStats = new CumulativeDoseCountDto(getDummyAvgAdherenceCountIntervals());

        doReturn(new AdherenceResponse()).when(adherenceService).getAllAdherence(anyList(), anyBoolean());
        doReturn(adherenceStats).when(adherenceService).calculateAdherenceDays(anyList(), any());
        doReturn(new AdherenceCacheDataDto<EntityDoseCountDto>(new ArrayList(), entityIds)).when(adherenceService).getAdherenceFromCache(anyList(), anyString(), any());
        when(registrationRepository.getAdherenceDetailsFromIamId(anyList())).thenReturn(Collections.singletonList(dummyRegistration));

        boolean patientDetailsRequired = false;
        AllAvgAdherenceResponse avgAdherenceResponse = adherenceService.getAverageAdherence(accessMappingToEntityMap, patientDetailsRequired, null);

        Assert.assertEquals(avgAdherenceResponse.getDigitalAdherence(), 50,10e-2);
        Assert.assertEquals(avgAdherenceResponse.getTotalAdherence(), 66.66,10e-2);
    }

    @Test
    public void getAverageAdherenceWithCacheTest() {
        List<String> entityIds = new ArrayList<>();
        String entityId  = "123";
        entityIds.add(entityId);
        Map<String, List<AccessMapping>> accessMappingToEntityMap = new HashMap<>();
        List<AccessMapping> accessMapping = Arrays.asList(
                new AccessMapping(29L, 1L, entityId)
        );
        accessMappingToEntityMap.put(entityId, accessMapping);
        List<EntityDoseCountDto> doseCountListFromCache = new ArrayList<>();
        Map<AverageAdherenceIntervals, IntervalAdherenceDto> avgAdherenceCountIntervals = getDummyAvgAdherenceCountIntervals();
        doseCountListFromCache.add (new EntityDoseCountDto(entityId, avgAdherenceCountIntervals));
        AdherenceCacheDataDto<EntityDoseCountDto> cacheResponse = new AdherenceCacheDataDto<EntityDoseCountDto>(doseCountListFromCache, new ArrayList());
        doReturn(cacheResponse).when(adherenceService).getAdherenceFromCache(anyList(), anyString(), any());

        boolean patientDetailsRequired = false;
        AllAvgAdherenceResponse avgAdherenceResponse = adherenceService.getAverageAdherence(accessMappingToEntityMap, patientDetailsRequired, null);

        Assert.assertEquals(avgAdherenceResponse.getDigitalAdherence(), 50,10e-2);
        Assert.assertEquals(avgAdherenceResponse.getTotalAdherence(), 66.66,10e-2);
    }

    @Test
    public void getAverageAdherenceWithCachePatientDetailsTest() {
        List<String> entityIds = new ArrayList<>();
        String entityId1  = "123";
        String entityId2  = "456";
        entityIds.add(entityId1);
        entityIds.add(entityId2);
        Map<String, List<AccessMapping>> accessMappingToEntityMap = new HashMap<>();
        List<AccessMapping> accessMapping1 = Arrays.asList(
                new AccessMapping(29L, 1L, entityId1)
        );
        List<AccessMapping> accessMapping2 = Arrays.asList(
                new AccessMapping(29L, 1L, entityId2)
        );
        accessMappingToEntityMap.put(entityId1, accessMapping1);
        accessMappingToEntityMap.put(entityId2, accessMapping2);
        List<EntityDoseCountDto> doseCountListFromCache = new ArrayList<>();
        Map<AverageAdherenceIntervals,IntervalAdherenceDto> avgAdherenceCountIntervals = getDummyAvgAdherenceCountIntervals();
        Map<AverageAdherenceIntervals,IntervalAdherenceDto> avgAdherenceCountIntervals2 = getDummyAvgAdherenceCountIntervals2();
        doseCountListFromCache.add (new EntityDoseCountDto(entityId1, avgAdherenceCountIntervals));
        doseCountListFromCache.add (new EntityDoseCountDto(entityId2, avgAdherenceCountIntervals2));
        AdherenceCacheDataDto<EntityDoseCountDto> cacheResponse = new AdherenceCacheDataDto<EntityDoseCountDto>(doseCountListFromCache, new ArrayList());
        doReturn(cacheResponse).when(adherenceService).getAdherenceFromCache(anyList(), anyString(), any());

        boolean patientDetailsRequired = true;
        AllAvgAdherenceResponse avgAdherenceResponse = adherenceService.getAverageAdherence(accessMappingToEntityMap, patientDetailsRequired, null);
        AvgAdherenceResponse patientDetails1 = avgAdherenceResponse.getEntityAverageAdherenceList().get(0);
        AvgAdherenceResponse patientDetails2 = avgAdherenceResponse.getEntityAverageAdherenceList().get(1);
        // Calculated based on values in dummyAdherenceCounts 1 and 2
        Assert.assertEquals(avgAdherenceResponse.getDigitalAdherence(), 47.5,10e-2);
        Assert.assertEquals(avgAdherenceResponse.getTotalAdherence(), 75,10e-2);
        Assert.assertEquals(patientDetails1.getDigitalAdherence(), 50,10e-2);
        Assert.assertEquals(patientDetails1.getTotalAdherence(), 66.66,10e-2);
        Assert.assertEquals(patientDetails2.getDigitalAdherence(), 40,10e-2);
        Assert.assertEquals(patientDetails2.getTotalAdherence(), 100,10e-2);
    }

    @Test
    public void getEntityAdherenceTest() throws ParseException {

        String entityId = "1";
        RegistrationShortRepo dummyRegistration1 = new AdherenceData(1L, "66666", Utils.convertStringToDate("2019-01-04 18:30:00"), Utils.convertStringToDate("2019-01-09 18:29:59"), Utils.convertStringToDate("2019-01-04 18:30:00"), 1L);
        RegistrationShortRepo dummyRegistration2 = new AdherenceData(2L, "46666", Utils.convertStringToDate("2019-01-08 18:30:00"), Utils.convertStringToDate("2019-01-13 18:29:59"), Utils.convertStringToDate("2019-01-08 18:30:00"), 1L);
        String patientAdherenceString = "666646666";
        doReturn(patientAdherenceString).when(adherenceService).getEntityAdherenceString(anyList());
        List<RegistrationShortRepo> registrationList = Arrays.asList(dummyRegistration1,dummyRegistration2);
        EntityAdherenceDto entityAdherence = adherenceService.getEntityAdherence(entityId, registrationList);
        assertEquals(entityAdherence.getAdherenceString(), patientAdherenceString);
        assertEquals(entityAdherence.getStartDate(), Utils.convertStringToDate("2019-01-04 18:30:00"));
        assertEquals(entityAdherence.getEndDate(), Utils.convertStringToDate("2019-01-13 18:29:59"));
    }

}

package tests.unit.service;

import com.everwell.iam.services.AuthenticationService;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import tests.BaseTest;

public class AuthenticationServiceTest extends BaseTest {

    @Autowired
    private AuthenticationService authenticationService;

    private static String DEFAULT_PASSWORD = "eisforeverwell!";
    private static String DEFAULT_USERNAME = "everwell";

    @Test
    public void verifyTokenGeneration() {
        String token = authenticationService.issueSecureToken(DEFAULT_PASSWORD, DEFAULT_USERNAME);
        Assert.assertNotNull(token);
        //Todo: Convert the method to test the token generation when PowerMock supports java 9
    }
}

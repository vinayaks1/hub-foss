package tests.unit.handlersTest;

import com.everwell.iam.handlers.AdhTechHandlerMap;
import com.everwell.iam.models.db.AdherenceStringLog;
import com.everwell.iam.repositories.AdhStringLogRepository;
import com.everwell.iam.repositories.RegistrationRepository;
import com.everwell.iam.utils.Utils;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import tests.BaseTestNoSpring;

import java.text.ParseException;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

public abstract class AdherenceTechHandlerTest extends BaseTestNoSpring {

  /* TODO: reach a state where this test file has everything AdherenceTechHandler has */

  @Mock
  RegistrationRepository registrationRepository;

  @Mock
  AdhStringLogRepository adhStringLogRepository;

  @Mock
  protected AdhTechHandlerMap adherenceTechnologyHandlerMap;

  @InjectMocks
  AdherenceTestHandler testHandler;

  @Test
  public abstract void getTechLogsTest() throws Exception;

  @Test
  public abstract void deleteRegistration();

  @Test
  public void getAdherenceStringLogsTest() throws ParseException {
    AdherenceStringLog log = new AdherenceStringLog(1L, '4', Utils.convertStringToDate("2022-11-13 03:30:00"));
    Mockito.when(adhStringLogRepository.findAllByIamAndStartDate(any(), eq(1L))).thenReturn(Collections.singletonList(log));

    List<AdherenceStringLog> logs = testHandler.getAdherenceStringLogs(1L, Utils.getCurrentDate());
    Assert.assertEquals(1L, logs.size());
  }

}

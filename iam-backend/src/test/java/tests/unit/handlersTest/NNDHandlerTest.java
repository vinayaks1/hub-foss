package tests.unit.handlersTest;

import com.everwell.iam.constants.Constants;
import com.everwell.iam.constants.RabbitMQConstants;
import com.everwell.iam.enums.AdherenceCodeEnum;
import com.everwell.iam.handlers.AdherenceTechHandler;
import com.everwell.iam.handlers.ScheduleHandler;
import com.everwell.iam.handlers.ScheduleTypeHandlerMap;
import com.everwell.iam.handlers.SpringEventHandler;
import com.everwell.iam.handlers.impl.NNDHandler;

import com.everwell.iam.models.db.AdherenceStringLog;
import com.everwell.iam.models.db.CAccess;
import com.everwell.iam.models.db.CallLogs;
import com.everwell.iam.models.db.PhoneMap;
import com.everwell.iam.models.db.Registration;
import com.everwell.iam.models.db.Schedule;
import com.everwell.iam.models.db.ScheduleMap;
import com.everwell.iam.models.dto.AdhTechLogsData;
import com.everwell.iam.models.dto.PhoneEditDto;
import com.everwell.iam.models.http.requests.IncomingCallRequest;
import com.everwell.iam.models.http.requests.NNDEntityRequest;
import com.everwell.iam.models.http.requests.PhoneRequest;
import com.everwell.iam.repositories.*;
import com.everwell.iam.services.ClientService;
import com.everwell.iam.services.RabbitMQPublisherService;
import com.everwell.iam.utils.CacheUtils;
import com.everwell.iam.utils.Utils;
import com.everwell.iam.vendors.NNDController;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import tests.BaseTest;
import tests.BaseTestNoSpring;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

public class NNDHandlerTest extends BaseTestNoSpring {

  @Spy
  @InjectMocks
  NNDHandler nndHandler;

  @Spy
  PhoneMapRepository phoneMapRepository;

  @Spy
  AdhStringLogRepository adhStringLogRepository;

  @Spy
  CallLogRepository callLogRepository;

  @Spy
  RegistrationRepository registrationRepository;

  @Mock
  ScheduleTypeHandlerMap scheduleTypeHandlerMap;

  @Mock
  ScheduleHandler scheduleHandler;

  @MockBean
  @InjectMocks
  SpringEventHandler springEventHandler;

  @InjectMocks
  CacheUtils cacheUtils;

  @Mock
  RedisTemplate<String, Object> redisTemplate;

  @Mock
  private ValueOperations valueOperations;

  @Mock
  private ApplicationEventPublisher applicationEventPublisher;

  @Mock
  RabbitMQPublisherService rabbitMQPublisherService;

  @Mock
  ClientService clientService;

  private CallLogs callLogs;
  private List<AdherenceStringLog> logs = new ArrayList<>();

  private static Long clientId = 4L;
  private static String entityId = "1234";
  private static Long iamId = 1L;

  @Before
  public void init() {
    when(phoneMapRepository.saveAll(any())).thenReturn(new ArrayList<>());
    when(registrationRepository.findById(any())).thenReturn(Optional.empty());
    when(adhStringLogRepository.saveAll(any())).thenAnswer((Answer) invocation -> {
      logs = (List<AdherenceStringLog>) invocation.getArguments()[0];
      return logs;
    });

    cacheUtils = new CacheUtils(redisTemplate);
    when(redisTemplate.opsForValue()).thenReturn(valueOperations);
  }

  @Test
  public void postRegistration_PartialDummyPhones() throws ParseException {
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-01-01 18:30:00"), Utils.convertStringToDate("2019-01-16 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    NNDEntityRequest nndEntityRequest = new NNDEntityRequest();
    nndEntityRequest.setClientId(1L);
    nndEntityRequest.setPhoneNumbers(Arrays.asList(new PhoneRequest.PhoneNumbers("1", "2019-01-01 18:30:00")));
    when(phoneMapRepository.findAllByIamId(any())).thenReturn(getPartialDummyPhoneNumbers());
    when(cacheUtils.hasKey(eq("918826728088"))).thenReturn(true);
    List<BigInteger> bigIntegers = new ArrayList<>();
    when(phoneMapRepository.getActiveIamIdsByPhoneNumber(any())).thenReturn(bigIntegers);
    doNothing().when(nndHandler).deactivateOldPhonesAndAddNew(any());
    nndHandler.postRegistration(registration, nndEntityRequest, false);
  }

  @Test
  public void postRegistration_EmptyPhoneNumbers() throws ParseException {
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-01-01 18:30:00"), Utils.convertStringToDate("2019-01-16 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    NNDEntityRequest nndEntityRequest = new NNDEntityRequest();
    nndEntityRequest.setClientId(1L);
    when(phoneMapRepository.findAllByIamId(any())).thenReturn(getPartialDummyPhoneNumbers());
    when(cacheUtils.hasKey(eq("918826728088"))).thenReturn(true);
    List<BigInteger> bigIntegers = new ArrayList<>();
    when(phoneMapRepository.getActiveIamIdsByPhoneNumber(any())).thenReturn(bigIntegers);
    doNothing().when(nndHandler).deactivateOldPhonesAndAddNew(any());
    nndHandler.postRegistration(registration, nndEntityRequest, false);
  }

  @Test
  public void postRegistration_NoCorrectPhones() throws ParseException {
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-01-01 18:30:00"), Utils.convertStringToDate("2019-01-16 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    NNDEntityRequest nndEntityRequest = new NNDEntityRequest();
    nndEntityRequest.setClientId(1L);
    nndEntityRequest.setPhoneNumbers(Arrays.asList(new PhoneRequest.PhoneNumbers("1", "2019-01-01 18:30:00")));
    when(phoneMapRepository.findAllByIamId(any())).thenReturn(getPartialDummyPhoneNumbers());
    doNothing().when(nndHandler).deactivateOldPhonesAndAddNew(any());
    nndHandler.postRegistration(registration, nndEntityRequest, false);
  }

  @Test
  public void postRegistration_IsNew() throws ParseException {
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-01-01 18:30:00"), Utils.convertStringToDate("2019-01-16 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    NNDEntityRequest nndEntityRequest = new NNDEntityRequest();
    nndEntityRequest.setClientId(1L);
    nndEntityRequest.setPhoneNumbers(Arrays.asList(new PhoneRequest.PhoneNumbers("1", "2019-01-01 18:30:00")));
    when(phoneMapRepository.findAllByIamId(any())).thenReturn(getPartialDummyPhoneNumbers());
    nndHandler.postRegistration(registration, nndEntityRequest, true);
  }

  @Test
  public void postRegistration_AllDummyPhones() throws ParseException {
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-01-01 18:30:00"), Utils.convertStringToDate("2019-01-16 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    NNDEntityRequest nndEntityRequest = new NNDEntityRequest();
    nndEntityRequest.setPhoneNumbers(Arrays.asList(new PhoneRequest.PhoneNumbers("1", "2019-01-01 18:30:00")));
  }

  @Test
  public void postRegistration_StopDateError() throws ParseException {
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-01-01 18:30:00"), Utils.convertStringToDate("2019-01-16 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    NNDEntityRequest nndEntityRequest = new NNDEntityRequest();
    nndEntityRequest.setClientId(1L);
    nndEntityRequest.setPhoneNumbers(Arrays.asList(new PhoneRequest.PhoneNumbers("1", "2019-01-01")));
    nndHandler.postRegistration(registration, nndEntityRequest, true);
  }

  @Test
  public void postRegistration_StopDateEmpty() throws ParseException {
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-01-01 18:30:00"), Utils.convertStringToDate("2019-01-16 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    NNDEntityRequest nndEntityRequest = new NNDEntityRequest();
    nndEntityRequest.setClientId(1L);
    nndEntityRequest.setPhoneNumbers(Arrays.asList(new PhoneRequest.PhoneNumbers("1", "")));
    nndHandler.postRegistration(registration, nndEntityRequest, true);
  }

  //return partial dummy phones
  private List<PhoneMap> getPartialDummyPhoneNumbers() {
    List<PhoneMap> phoneMaps = new ArrayList<>();
    phoneMaps.add(new PhoneMap("910000000000", 1L));
    phoneMaps.add(new PhoneMap("911234567890", 1L));
    phoneMaps.add(new PhoneMap("918826728088", 1L));
    return phoneMaps;
  }

  @Test
  public void call_log_saved_on_process_call() throws ParseException, InterruptedException, ExecutionException {
    String DUMMY_CALLED_NUMBER = "1323123";
    String POST = "postBody";
    String MOB_NUMBER = "918712379111";
    Date CALLED_DATE = Utils.convertStringToDate("2019-12-11 00:00:00");
    when(callLogRepository.save(any())).thenAnswer((Answer) invocation -> {
      callLogs = (CallLogs) invocation.getArguments()[0];
      return callLogs;
    });
    List<BigInteger> iamIds = Arrays.asList(BigInteger.ONE);
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-08-31 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), MOB_NUMBER, "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    when(valueOperations.get(eq(MOB_NUMBER))).thenReturn(1);

    IncomingCallRequest incomingCallRequest = new IncomingCallRequest(DUMMY_CALLED_NUMBER, MOB_NUMBER, "2019-12-11 00:00:00", POST);
    CallLogs incomingCallLog = nndHandler.saveCallLog(incomingCallRequest);
    nndHandler.processIncomingCall(incomingCallLog).get();
    Assert.assertEquals(callLogs.getNumberDialled(), DUMMY_CALLED_NUMBER);
    Assert.assertEquals(callLogs.getPostBody(), POST);
    Assert.assertEquals(callLogs.getCaller(), MOB_NUMBER);
  }

  @Test
  public void testDeactivateOldPhonesAndAddNew()
  {
    PhoneEditDto request = new PhoneEditDto();
    request.setIamId(iamId);
    request.setEntityId(entityId);
    request.setPrimaryPhone("9988699888");
    request.setNumbers(new ArrayList<String>(){
      {
        add("9988699886");
        add("9988799887");
      }
    });

    List<PhoneMap> existingPhoneMap = Arrays.asList(
        new PhoneMap("9988799887", iamId)
    );
    when(phoneMapRepository.findAllByIamId(iamId)).thenReturn(existingPhoneMap);

    doNothing().when(registrationRepository).updateUniqueIdentifier(anyString(), anyLong(), any());
    doNothing().when(phoneMapRepository).updateEndDate(anyList(), anyLong(), any());

    nndHandler.deactivateOldPhonesAndAddNew(request);
  }

  @Test
  public void testDeactivateOldPhonesAndAddNew_ExistingNumbers()
  {
    PhoneEditDto request = new PhoneEditDto();
    request.setIamId(iamId);
    request.setEntityId(entityId);
    request.setPrimaryPhone("9988799887");
    request.setNumbers(new ArrayList<String>(){
      {
        add("9988799887");
      }
    });

    List<PhoneMap> existingPhoneMap = Arrays.asList(
            new PhoneMap("9988799887", iamId)
    );
    when(phoneMapRepository.findAllByIamId(iamId)).thenReturn(existingPhoneMap);

    doNothing().when(registrationRepository).updateUniqueIdentifier(anyString(), anyLong(), any());
    doNothing().when(phoneMapRepository).updateEndDate(anyList(), anyLong(), any());

    doNothing().when(nndHandler).emitEventForRegen(anyList());

    nndHandler.deactivateOldPhonesAndAddNew(request);
  }

  @Test
  public void testEligibleForUpdate_Before_RECEIVED_UNSCHEDULED_After_RECEIVED()
  {
    boolean isEligible = nndHandler.eligibleForUpdate(AdherenceCodeEnum.RECEIVED_UNSCHEDULED.getCode(), AdherenceCodeEnum.RECEIVED.getCode());
    Assert.assertTrue(isEligible);
  }

  @Test
  public void testEligibleForUpdate_Before_NOT_RECEIVED_After_RECEIVED_UNSCHEDULED()
  {
    boolean isEligible = nndHandler.eligibleForUpdate(AdherenceCodeEnum.QUIET.getCode(), AdherenceCodeEnum.RECEIVED_UNSCHEDULED.getCode());
    Assert.assertTrue(isEligible);
  }

  @Test
  public void testEligibleForUpdate_Before_RECEIVED_UNSCHEDULED()
  {
    boolean isEligible = nndHandler.eligibleForUpdate(AdherenceCodeEnum.RECEIVED_UNSCHEDULED.getCode(), AdherenceCodeEnum.QUIET.getCode());
    Assert.assertFalse(isEligible);
  }

  @Test
  public void testEligibleForUpdate_Before_TFN_REPEAT_RECEIVED()
  {
    boolean isEligible = nndHandler.eligibleForUpdate(AdherenceCodeEnum.TFN_REPEAT_RECEIVED.getCode(), AdherenceCodeEnum.QUIET.getCode());
    Assert.assertFalse(isEligible);
  }

  @Test
  public void testEligibleForUpdate_Before_NO_INFO()
  {
    boolean isEligible = nndHandler.eligibleForUpdate(AdherenceCodeEnum.NO_INFO.getCode(), AdherenceCodeEnum.QUIET.getCode());
    Assert.assertTrue(isEligible);
  }

  @Test
  public void testEligibleForUpdate_After_TFN_REPEAT_RECEIVED()
  {
    boolean isEligible = nndHandler.eligibleForUpdate(AdherenceCodeEnum.QUIET.getCode(), AdherenceCodeEnum.TFN_REPEAT_RECEIVED.getCode());
    Assert.assertTrue(isEligible);
  }

  @Test
  public void testEligibleForUpdate_Before_RECEIVED_After_TFN_REPEAT_RECEIVED_UNSURE()
  {
    boolean isEligible = nndHandler.eligibleForUpdate(AdherenceCodeEnum.RECEIVED.getCode(), AdherenceCodeEnum.TFN_REPEAT_RECEIVED_UNSURE.getCode());
    Assert.assertTrue(isEligible);
  }

  @Test
  public void testEligibleForUpdate_Before_RECEIVED_UNSURE_After_RECEIVED()
  {
    boolean isEligible = nndHandler.eligibleForUpdate(AdherenceCodeEnum.RECEIVED_UNSURE.getCode(), AdherenceCodeEnum.RECEIVED.getCode());
    Assert.assertTrue(isEligible);
  }

  @Test
  public void testEligibleForUpdate_Before_RECEIVED_UNSURE_After_TFN_REPEAT_RECEIVED_UNSURE()
  {
    boolean isEligible = nndHandler.eligibleForUpdate(AdherenceCodeEnum.RECEIVED_UNSURE.getCode(), AdherenceCodeEnum.TFN_REPEAT_RECEIVED_UNSURE.getCode());
    Assert.assertTrue(isEligible);
  }

  @Test
  public void testEligibleForUpdate_Before_MANUAL_After_NO_INFO()
  {
    boolean isEligible = nndHandler.eligibleForUpdate(AdherenceCodeEnum.MANUAL.getCode(), AdherenceCodeEnum.NO_INFO.getCode());
    Assert.assertTrue(isEligible);
  }

  @Test
  public void testEligibleForUpdate_Before_MANUAL_After_RECEIVED()
  {
    boolean isEligible = nndHandler.eligibleForUpdate(AdherenceCodeEnum.MANUAL.getCode(), AdherenceCodeEnum.RECEIVED.getCode());
    Assert.assertTrue(isEligible);
  }

  @Test
  public void testEligibleForUpdate_Before_MANUAL_After_TFN_REPEAT_RECEIVED_UNSURE()
  {
    boolean isEligible = nndHandler.eligibleForUpdate(AdherenceCodeEnum.MANUAL.getCode(), AdherenceCodeEnum.TFN_REPEAT_RECEIVED_UNSURE.getCode());
    Assert.assertTrue(isEligible);
  }

  @Test
  public void testEligibleForUpdate_Before_MANUAL_After_RECEIVED_UNSURE()
  {
    boolean isEligible = nndHandler.eligibleForUpdate(AdherenceCodeEnum.MANUAL.getCode(), AdherenceCodeEnum.RECEIVED_UNSURE.getCode());
    Assert.assertTrue(isEligible);
  }

  @Test
  public void testEligibleForUpdate_Before_MISSED_After_NO_INFO()
  {
    boolean isEligible = nndHandler.eligibleForUpdate(AdherenceCodeEnum.MISSED.getCode(), AdherenceCodeEnum.NO_INFO.getCode());
    Assert.assertTrue(isEligible);
  }

  @Test
  public void testEligibleForUpdate_Before_MISSED_After_RECEIVED()
  {
    boolean isEligible = nndHandler.eligibleForUpdate(AdherenceCodeEnum.MISSED.getCode(), AdherenceCodeEnum.RECEIVED.getCode());
    Assert.assertTrue(isEligible);
  }

  @Test
  public void testEligibleForUpdate_Before_MISSED_After_TFN_REPEAT_RECEIVED_UNSURE()
  {
    boolean isEligible = nndHandler.eligibleForUpdate(AdherenceCodeEnum.MISSED.getCode(), AdherenceCodeEnum.TFN_REPEAT_RECEIVED_UNSURE.getCode());
    Assert.assertTrue(isEligible);
  }

  @Test
  public void testEligibleForUpdate_Before_MISSED_After_RECEIVED_UNSURE()
  {
    boolean isEligible = nndHandler.eligibleForUpdate(AdherenceCodeEnum.MISSED.getCode(), AdherenceCodeEnum.RECEIVED_UNSURE.getCode());
    Assert.assertTrue(isEligible);
  }

  @Test
  public void testEligibleForUpdate_Default()
  {
    boolean isEligible = nndHandler.eligibleForUpdate(AdherenceCodeEnum.BLANK.getCode(), AdherenceCodeEnum.BLANK.getCode());
    Assert.assertFalse(isEligible);
  }

  @Test
  public void testGetTechLogs_NullDates()
  {
    List<PhoneMap> phoneMaps = Arrays.asList(
        new PhoneMap("9988799887", iamId)
    );
    when(phoneMapRepository.findAllByIamId(iamId)).thenReturn(phoneMaps);

    Registration registration = new Registration();
    registration.setStartDate(Utils.getCurrentDate());
    when(registrationRepository.getOne(iamId)).thenReturn(registration);

    List<CallLogs> callLogs = new ArrayList<>();
    CallLogs callLog = new CallLogs();
    callLog.setCaller("9988799887");
    callLogs.add(callLog);

    when(callLogRepository.getCallLogsByPhone(anyString(), any(), any(), any())).thenReturn(callLogs);

    List<AdhTechLogsData> logs = nndHandler.getTechLogs(iamId);
    Assert.assertEquals(logs.get(0).getPhoneNumber(), "9988799887");
  }

  @Test
  public void testGetTechLogs_ValidDates()
  {
    List<PhoneMap> phoneMaps = Arrays.asList(
            new PhoneMap("9988799887", iamId, Utils.getCurrentDate())
    );
    when(phoneMapRepository.findAllByIamId(iamId)).thenReturn(phoneMaps);

    Registration registration = new Registration();
    registration.setStartDate(Utils.getCurrentDate());
    registration.setEndDate(Utils.getCurrentDate());
    when(registrationRepository.getOne(iamId)).thenReturn(registration);

    List<CallLogs> callLogs = new ArrayList<>();
    CallLogs callLog = new CallLogs();
    callLog.setCaller("9988799887");
    callLogs.add(callLog);

    when(callLogRepository.getCallLogsByPhone(anyString(), any(), any(), any())).thenReturn(callLogs);

    List<AdhTechLogsData> logs = nndHandler.getTechLogs(iamId);
    Assert.assertEquals(logs.get(0).getPhoneNumber(), "9988799887");
  }

  @Test
  public void testDeactivatePhoneNumbersByIamIds()
  {
    doNothing().when(phoneMapRepository).deactivatePhonesByIamIds(anySet(), any());

    nndHandler.deactivatePhoneNumbersByIamIds(Collections.singleton(iamId));
  }

  @Test
  public void testSavePhoneNumbers()
  {
    doNothing().when(nndHandler).emitEventForRegen(anyList());
    boolean isSuccess = nndHandler.savePhoneNumbers(Arrays.asList(
            new PhoneMap("9988799887", iamId, Utils.getCurrentDate())
    ), clientId);

    Assert.assertTrue(isSuccess);
  }

  @Test
  public void testSavePhoneNumbers_DummyNumbers()
  {
    boolean isSuccess = nndHandler.savePhoneNumbers(Arrays.asList(
            new PhoneMap("9988799887", iamId, Utils.getCurrentDate())
    ), clientId);

    Assert.assertTrue(isSuccess);
  }


  @Test
  public void testComputeCompleteAdherenceString() throws Exception
  {
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-08-31 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), "9988799887", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));

    List<CallLogs> callLogs = new ArrayList<>();
    CallLogs callLog = new CallLogs();
    callLog.setNumberDialled("12345");
    callLog.setCaller("9988799887");
    callLog.setClientTime(Utils.getCurrentDate());
    callLogs.add(callLog);

    CallLogs callLog2 = new CallLogs();
    callLog.setNumberDialled("12345");
    callLog2.setCaller("9988799888");
    callLog2.setClientTime(Utils.getCurrentDate());
    callLogs.add(callLog2);

    when(scheduleTypeHandlerMap.getHandler(anyLong())).thenReturn(scheduleHandler);

    ScheduleMap scheduleMap = new ScheduleMap();
    scheduleMap.setScheduleId(1L);
    List<ScheduleMap> scheduleMaps = Collections.singletonList(scheduleMap);
    when(scheduleHandler.findAllScheduleMappings(any())).thenReturn(scheduleMaps);

    when(scheduleHandler.getAllSchedules(anyList())).thenReturn(Arrays.asList(new Schedule(1L, "1", 1L)));

    when(scheduleHandler.shouldTakeDose(anyLong(), any())).thenReturn(true);
    when(scheduleHandler.getDateToAttribute(anyList(), anyMap(), any())).thenReturn(Utils.getCurrentDate());

    when(scheduleHandler.shouldFlagTFNRepeatForNND()).thenReturn(true);

    doNothing().when(nndHandler).updateRegistrationAfterRegen(anyLong(), anyList());

    List<Long> adherenceStringIds = nndHandler.computeCompleteAdherenceString(callLogs, registration).get();
    Assert.assertEquals(adherenceStringIds.size(), 0);
  }

  @Test
  public void testComputeCompleteAdherenceString_repeated() throws Exception
  {
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2020-08-31 18:30:00"), Utils.convertStringToDate("2020-09-16 18:29:59"), "9988799887", "6666666666666666", Utils.convertStringToDate("2020-01-06 12:23:26"), Utils.convertStringToDate("2020-01-06 12:23:26"));
    when(registrationRepository.findByIdIn(anyList())).thenReturn(Collections.singletonList(registration));
    List<CallLogs> callLogs = new ArrayList<>();
    CallLogs callLog = new CallLogs();
    callLog.setNumberDialled("12345");
    callLog.setCaller("9988799887");
    callLog.setClientTime(Utils.convertStringToDate("2020-09-10 18:30:00"));
    callLogs.add(callLog);

    CallLogs callLog2 = new CallLogs();
    callLog2.setNumberDialled("12345");
    callLog2.setCaller("9988799888");
    callLog2.setClientTime(Utils.convertStringToDate("2020-09-10 18:30:00"));
    callLogs.add(callLog2);

    CallLogs callLog3 = new CallLogs();
    callLog3.setNumberDialled("12345");
    callLog3.setCaller("9988799887");
    callLog3.setClientTime(Utils.convertStringToDate("2020-09-11 18:30:00"));
    callLogs.add(callLog3);

    CallLogs callLog4 = new CallLogs();
    callLog4.setNumberDialled("12345");
    callLog4.setCaller("9988799887");
    callLog4.setClientTime(Utils.convertStringToDate("2020-09-12 18:30:00"));
    callLogs.add(callLog4);

    when(scheduleTypeHandlerMap.getHandler(anyLong())).thenReturn(scheduleHandler);

    ScheduleMap scheduleMap = new ScheduleMap();
    scheduleMap.setScheduleId(1L);
    List<ScheduleMap> scheduleMaps = Collections.singletonList(scheduleMap);
    when(scheduleHandler.findAllScheduleMappings(any())).thenReturn(scheduleMaps);

    when(scheduleHandler.getAllSchedules(anyList())).thenReturn(Arrays.asList(new Schedule(1L, "1", 1L)));

    when(scheduleHandler.shouldTakeDose(anyLong(), any())).thenReturn(true);
    when(scheduleHandler.getDateToAttribute(anyList(), anyMap(), any())).thenAnswer(invocation -> invocation.getArgument(2));

    when(scheduleHandler.shouldFlagTFNRepeatForNND()).thenReturn(true);

    doNothing().when(nndHandler).updateRegistrationAfterRegen(anyLong(), anyList());
    List<BigInteger> activeIams = Collections.singletonList(BigInteger.ONE);
    when(phoneMapRepository.findAllActiveIamIdsByPhoneNumbers(anyString(), any())).thenReturn(activeIams);

    List<Long> adherenceStringIds = nndHandler.computeCompleteAdherenceString(callLogs, registration).get();
    Assert.assertEquals(adherenceStringIds.size(), 3);
  }

  @Test
  public void testComputeCompleteAdherenceString_NonNND() throws Exception
  {
    Registration registration = new Registration(1L, 2L, Utils.convertStringToDate("2019-08-31 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), "9988799887", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));

    List<CallLogs> callLogs = new ArrayList<>();
    CallLogs callLog = new CallLogs();
    callLog.setCaller("9988799887");
    callLog.setClientTime(Utils.getCurrentDate());
    callLogs.add(callLog);

    List<Long> adherenceStringIds = nndHandler.computeCompleteAdherenceString(callLogs, registration).get();
    Assert.assertEquals(adherenceStringIds.size(), 0);
  }

  @Test
  public void testComputeCompleteAdherenceString_scheduleHandler() throws Exception
  {
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-08-31 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), "9988799887", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));

    List<CallLogs> callLogs = new ArrayList<>();
    CallLogs callLog = new CallLogs();
    callLog.setCaller("9988799887");
    callLog.setClientTime(Utils.getCurrentDate());
    callLogs.add(callLog);

    when(scheduleTypeHandlerMap.getHandler(anyLong())).thenReturn(scheduleHandler);

    ScheduleMap scheduleMap = new ScheduleMap();
    scheduleMap.setScheduleId(1L);
    List<ScheduleMap> scheduleMaps = Collections.singletonList(scheduleMap);
    when(scheduleHandler.findAllScheduleMappings(any())).thenReturn(scheduleMaps);

    when(scheduleHandler.getAllSchedules(anyList())).thenReturn(Arrays.asList(new Schedule(1L, "1", 1L)));

    when(scheduleHandler.shouldTakeDose(anyLong(), any())).thenReturn(false);

    doNothing().when(nndHandler).updateRegistrationAfterRegen(anyLong(), anyList());

    List<Long> adherenceStringIds = nndHandler.computeCompleteAdherenceString(callLogs, registration).get();
    Assert.assertEquals(adherenceStringIds.size(), 0);
  }

  @Test
  public void testComputeCompleteAdherenceString_scheduleHandler_unscheduled() throws Exception
  {
    Date startDate = Utils.convertStringToDate("2019-08-31 18:30:00");
    Date endDate = Utils.convertStringToDate("2019-09-08 18:29:59");
    Registration registration = new Registration(1L, 1L, startDate, endDate, "9988799887", "66666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    registration.setScheduleTypeId(2L);
    when(registrationRepository.findByIdIn(anyList())).thenReturn(Collections.singletonList(registration));

    List<CallLogs> callLogs = new ArrayList<>();
    CallLogs callLog = new CallLogs();
    callLog.setCaller("9988799887");
    // Assuming schedule day to be 1st Sept and 8th Sept, making a call on a unscheduled day
    Date callDate = Utils.convertStringToDate("2019-09-07 11:29:59");
    Date scheduleDate = Utils.convertStringToDate("2019-09-07 18:30:00");
    callLog.setClientTime(callDate);
    callLogs.add(callLog);

    when(scheduleTypeHandlerMap.getHandler(anyLong())).thenReturn(scheduleHandler);

    ScheduleMap scheduleMap = new ScheduleMap();
    scheduleMap.setScheduleId(1L);
    List<ScheduleMap> scheduleMaps = Collections.singletonList(scheduleMap);
    when(scheduleHandler.findAllScheduleMappings(any())).thenReturn(scheduleMaps);

    when(scheduleHandler.getAllSchedules(anyList())).thenReturn(Arrays.asList(new Schedule(1L, "1", 1L)));

    when(scheduleHandler.shouldTakeDose(anyLong(), any())).thenReturn(true);
    when(scheduleHandler.getDateToAttribute(any(), any(), any())).thenReturn(scheduleDate);

    doNothing().when(nndHandler).updateRegistrationAfterRegen(anyLong(), anyList());
    doNothing().when(nndHandler).applyTfnFlagging(any(), any(), any());
    List<BigInteger> activeIams = Collections.singletonList(BigInteger.ONE);
    when(phoneMapRepository.findAllActiveIamIdsByPhoneNumbers(anyString(), any())).thenReturn(activeIams);

    when(scheduleHandler.computeAdherenceCodeForDay(anyList(), anyMap(), any())).thenReturn(AdherenceCodeEnum.RECEIVED_UNSCHEDULED.getCode());

    List<Long> adherenceStringIds = nndHandler.computeCompleteAdherenceString(callLogs, registration).get();
    Assert.assertEquals(1, adherenceStringIds.size());
  }

  @Test
  public void testGetIamIdsByAdherenceTechAndUniqueIdentifier()
  {
    List<BigInteger> phoneNumbers = Collections.singletonList(BigInteger.valueOf(iamId));
    String phoneNumber = "9988799887";
    when(phoneMapRepository.getActiveIamIdsByPhoneNumber(Collections.singletonList(phoneNumber))).thenReturn(phoneNumbers);

    List<Long> iamIds = nndHandler.getIamIdsByAdherenceTechAndUniqueIdentifier(phoneNumber);
    Assert.assertEquals(iamIds.get(0), iamId);
  }

  @Test
  public void testComputeSingleAdherenceCodeForDay() throws Exception
  {
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-08-31 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), "9988799887", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));

    when(scheduleTypeHandlerMap.getHandler(anyLong())).thenReturn(scheduleHandler);

    ScheduleMap scheduleMap = new ScheduleMap();
    scheduleMap.setScheduleId(1L);
    List<ScheduleMap> scheduleMaps = Collections.singletonList(scheduleMap);
    when(scheduleHandler.findAllScheduleMappings(any())).thenReturn(scheduleMaps);

    when(scheduleHandler.getAllSchedules(anyList())).thenReturn(Arrays.asList(new Schedule(1L, "1", 1L)));

    when(scheduleHandler.shouldTakeDose(anyList(), anyMap(), any())).thenReturn(true);
    when(scheduleHandler.getDateToAttribute(anyList(), anyMap(), any())).thenAnswer(invocation -> invocation.getArgument(2));
    when(scheduleHandler.computeAdherenceCodeForDay(anyList(), anyMap(), any())).thenReturn(AdherenceCodeEnum.RECEIVED.getCode());

    when(scheduleHandler.shouldFlagTFNRepeatForNND()).thenReturn(true);

    List<CallLogs> callLogs = new ArrayList<>();
    CallLogs callLog = new CallLogs();
    callLog.setNumberDialled("12345");
    callLog.setCaller("9988799887");
    callLog.setClientTime(Utils.convertStringToDate("2020-09-10 18:30:00"));
    callLogs.add(callLog);

    when(callLogRepository.getAllByIamIdOrderByClientTimeLimitByDateCustom(anyLong(), any())).thenReturn(callLogs);

    List<AdherenceStringLog> adherenceStringLogsCreated = nndHandler.computeSingleAdherenceCodeForDay(Collections.singletonList(registration), Utils.getCurrentDate());
    Assert.assertEquals(adherenceStringLogsCreated.size(), 1);
    Assert.assertEquals(adherenceStringLogsCreated.get(0).getIamId().longValue(), 1L);
  }

  @Test
  public void testComputeSingleAdherenceCodeForDay_Adherence_RECEIVED_UNSCHEDULED() throws Exception
  {
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-08-31 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), "9988799887", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));

    when(scheduleTypeHandlerMap.getHandler(anyLong())).thenReturn(scheduleHandler);

    ScheduleMap scheduleMap = new ScheduleMap();
    scheduleMap.setScheduleId(1L);
    List<ScheduleMap> scheduleMaps = Collections.singletonList(scheduleMap);
    when(scheduleHandler.findAllScheduleMappings(any())).thenReturn(scheduleMaps);

    when(scheduleHandler.getAllSchedules(anyList())).thenReturn(Arrays.asList(new Schedule(1L, "1", 1L)));

    when(scheduleHandler.shouldTakeDose(anyList(), anyMap(), any())).thenReturn(true);
    when(scheduleHandler.getDateToAttribute(anyList(), anyMap(), any())).thenAnswer(invocation -> invocation.getArgument(2));
    when(scheduleHandler.computeAdherenceCodeForDay(anyList(), anyMap(), any())).thenReturn(AdherenceCodeEnum.RECEIVED_UNSCHEDULED.getCode());

    when(adhStringLogRepository.findAllByIamIdOrderByRecordDateDescCustom(anyLong(), any(), any())).thenReturn(logs);

    when(scheduleHandler.shouldFlagTFNRepeatForNND()).thenReturn(true);

    List<CallLogs> callLogs = new ArrayList<>();
    CallLogs callLog = new CallLogs();
    callLog.setNumberDialled("12345");
    callLog.setCaller("9988799887");
    callLog.setClientTime(Utils.convertStringToDate("2020-09-10 18:30:00"));
    callLogs.add(callLog);

    CallLogs callLog2 = new CallLogs();
    callLog2.setNumberDialled("12345");
    callLog2.setCaller("9988799888");
    callLog2.setClientTime(Utils.convertStringToDate("2020-09-10 18:30:00"));
    callLogs.add(callLog2);

    CallLogs callLog3 = new CallLogs();
    callLog3.setNumberDialled("12345");
    callLog3.setCaller("9988799887");
    callLog3.setClientTime(Utils.convertStringToDate("2020-09-08 18:30:00"));
    callLogs.add(callLog3);

    CallLogs callLog4 = new CallLogs();
    callLog4.setNumberDialled("12345");
    callLog4.setCaller("9988799887");
    callLog4.setClientTime(Utils.convertStringToDate("2020-09-09 18:30:00"));
    callLogs.add(callLog4);

    when(callLogRepository.getAllByIamIdOrderByClientTimeLimitByDateCustom(anyLong(), any())).thenReturn(callLogs);

    List<AdherenceStringLog> adherenceStringLogsCreated = nndHandler.computeSingleAdherenceCodeForDay(Collections.singletonList(registration), Utils.getCurrentDate());
    Assert.assertEquals(adherenceStringLogsCreated.size(), 1);
    Assert.assertEquals(adherenceStringLogsCreated.get(0).getIamId().longValue(), 1L);
  }

  @Test
  public void testComputeSingleAdherenceCodeForDay_Adherence_RECEIVED() throws Exception
  {
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-08-31 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), "9988799887", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));

    when(scheduleTypeHandlerMap.getHandler(anyLong())).thenReturn(scheduleHandler);

    ScheduleMap scheduleMap = new ScheduleMap();
    scheduleMap.setScheduleId(1L);
    List<ScheduleMap> scheduleMaps = Collections.singletonList(scheduleMap);
    when(scheduleHandler.findAllScheduleMappings(any())).thenReturn(scheduleMaps);

    when(scheduleHandler.getAllSchedules(anyList())).thenReturn(Arrays.asList(new Schedule(1L, "1", 1L)));

    when(scheduleHandler.shouldTakeDose(anyList(), anyMap(), any())).thenReturn(true);
    when(scheduleHandler.getDateToAttribute(anyList(), anyMap(), any())).thenAnswer(invocation -> invocation.getArgument(2));
    when(scheduleHandler.computeAdherenceCodeForDay(anyList(), anyMap(), any())).thenReturn(AdherenceCodeEnum.RECEIVED_UNSCHEDULED.getCode());

    List<AdherenceStringLog> adherenceStringLogs = Collections.singletonList(new AdherenceStringLog(iamId, AdherenceCodeEnum.RECEIVED.getCode()));
    when(adhStringLogRepository.findAllByIamIdOrderByRecordDateDescCustom(anyLong(), any(), any())).thenReturn(adherenceStringLogs);

    List<CallLogs> callLogs = new ArrayList<>();
    CallLogs callLog = new CallLogs();
    callLog.setNumberDialled("12345");
    callLog.setCaller("9988799887");
    callLog.setClientTime(Utils.convertStringToDate("2020-09-10 18:30:00"));
    callLogs.add(callLog);

    CallLogs callLog2 = new CallLogs();
    callLog2.setNumberDialled("12345");
    callLog2.setCaller("9988799888");
    callLog2.setClientTime(Utils.convertStringToDate("2020-09-10 18:30:00"));
    callLogs.add(callLog2);

    CallLogs callLog3 = new CallLogs();
    callLog3.setNumberDialled("12345");
    callLog3.setCaller("9988799887");
    callLog3.setClientTime(Utils.convertStringToDate("2020-09-08 18:30:00"));
    callLogs.add(callLog3);

    CallLogs callLog4 = new CallLogs();
    callLog4.setNumberDialled("12345");
    callLog4.setCaller("9988799887");
    callLog4.setClientTime(Utils.convertStringToDate("2020-09-09 18:30:00"));
    callLogs.add(callLog4);

    List<AdherenceStringLog> adherenceStringLogsCreated = nndHandler.computeSingleAdherenceCodeForDay(Collections.singletonList(registration), Utils.getCurrentDate());
    Assert.assertEquals(adherenceStringLogsCreated.size(), 0);
  }

  @Test
  public void testComputeSingleAdherenceCodeForDay_Adherence_TFN_REPEAT_RECEIVED_UNSURE() throws Exception
  {
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-08-31 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), "9988799887", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));

    when(scheduleTypeHandlerMap.getHandler(anyLong())).thenReturn(scheduleHandler);

    ScheduleMap scheduleMap = new ScheduleMap();
    scheduleMap.setScheduleId(1L);
    List<ScheduleMap> scheduleMaps = Collections.singletonList(scheduleMap);
    when(scheduleHandler.findAllScheduleMappings(any())).thenReturn(scheduleMaps);

    when(scheduleHandler.getAllSchedules(anyList())).thenReturn(Arrays.asList(new Schedule(1L, "1", 1L)));

    when(scheduleHandler.shouldTakeDose(anyList(), anyMap(), any())).thenReturn(true);
    when(scheduleHandler.getDateToAttribute(anyList(), anyMap(), any())).thenAnswer(invocation -> invocation.getArgument(2));
    when(scheduleHandler.computeAdherenceCodeForDay(anyList(), anyMap(), any())).thenReturn(AdherenceCodeEnum.RECEIVED_UNSCHEDULED.getCode());

    List<AdherenceStringLog> adherenceStringLogs = Collections.singletonList(new AdherenceStringLog(iamId, AdherenceCodeEnum.TFN_REPEAT_RECEIVED_UNSURE.getCode()));
    when(adhStringLogRepository.findAllByIamIdOrderByRecordDateDescCustom(anyLong(), any(), any())).thenReturn(adherenceStringLogs);

    when(scheduleHandler.shouldFlagTFNRepeatForNND()).thenReturn(true);

    List<CallLogs> callLogs = new ArrayList<>();
    CallLogs callLog = new CallLogs();
    callLog.setNumberDialled("12345");
    callLog.setCaller("9988799887");
    callLog.setClientTime(Utils.convertStringToDate("2020-09-10 18:30:00"));
    callLogs.add(callLog);

    CallLogs callLog2 = new CallLogs();
    callLog2.setNumberDialled("12345");
    callLog2.setCaller("9988799888");
    callLog2.setClientTime(Utils.convertStringToDate("2020-09-10 18:30:00"));
    callLogs.add(callLog2);

    CallLogs callLog3 = new CallLogs();
    callLog3.setNumberDialled("12345");
    callLog3.setCaller("9988799887");
    callLog3.setClientTime(Utils.convertStringToDate("2020-09-08 18:30:00"));
    callLogs.add(callLog3);

    CallLogs callLog4 = new CallLogs();
    callLog4.setNumberDialled("12345");
    callLog4.setCaller("9988799887");
    callLog4.setClientTime(Utils.convertStringToDate("2020-09-09 18:30:00"));
    callLogs.add(callLog4);

    when(callLogRepository.getAllByIamIdOrderByClientTimeLimitByDateCustom(anyLong(), any())).thenReturn(callLogs);

    List<AdherenceStringLog> adherenceStringLogsCreated = nndHandler.computeSingleAdherenceCodeForDay(Collections.singletonList(registration), Utils.getCurrentDate());
    Assert.assertEquals(adherenceStringLogsCreated.size(), 2);
    Assert.assertEquals(adherenceStringLogsCreated.get(0).getIamId().longValue(), 1L);
  }

  @Test
  public void testRegenerateAdherence() throws Exception
  {
    doNothing().when(nndHandler).deleteAdherenceLogByIamIdAndAdherenceCodes(anyList(), anyLong());
    doReturn(CompletableFuture.completedFuture(Collections.singletonList(1L))).when(nndHandler).computeCompleteAdherenceString(anyList(), any());
    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-08-31 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), "9988799887", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));

    List<Long> logs = nndHandler.regenerateAdherence(Collections.singleton(iamId)).get();
    Assert.assertEquals(logs.size(), 1);
    Assert.assertEquals(logs.get(0).longValue(), 1L);
  }

  @Test
  public void testProcessIncomingCall() throws Exception
  {
    when(phoneMapRepository.findAllActiveIamIdsByPhoneNumbers(anyString(), any())).thenReturn(Collections.singletonList(BigInteger.valueOf(iamId)));

    Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-08-31 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), "9988799887", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
    when(registrationRepository.findByIdIn(anyList())).thenReturn(Collections.singletonList(registration));

    when(scheduleTypeHandlerMap.getHandler(anyLong())).thenReturn(scheduleHandler);
    when(scheduleHandler.shouldTakeDose(anyLong(), any())).thenReturn(true);

    List<AdherenceStringLog> adherenceStringLogs = Arrays.asList(
            new AdherenceStringLog(iamId, '6'),
            new AdherenceStringLog(iamId, '4')
    );
    doReturn(adherenceStringLogs).when(nndHandler).computeSingleAdherenceCodeForDay(anyList(), any());
    doReturn(Collections.singletonList(1L)).when(nndHandler).updateAdherenceMap(any(), any());
    when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));
    IncomingCallRequest incomingCallRequest = new IncomingCallRequest("9988799887", "12345", Utils.getFormattedDate(Utils.getCurrentDate()), "postBody");
    CallLogs incomingCallLog = nndHandler.saveCallLog(incomingCallRequest);
    List<Long> logs = nndHandler.processIncomingCall(incomingCallLog).get();
    Assert.assertEquals(logs.size(), 1);
    Assert.assertEquals(logs.get(0).longValue(), 1L);
  }

  @Test
  public void processIncomingCall_UnregisteredPhoneNumber_InsertedLogsEmpty() throws ExecutionException, InterruptedException {
    when(phoneMapRepository.findAllActiveIamIdsByPhoneNumbers(anyString(), any())).thenReturn(null);
    CallLogs incomingCallLog = new CallLogs("9988799887", "12345", Utils.getCurrentDate(),Utils.getCurrentDate(), "postBody");
    List<Long> logs = nndHandler.processIncomingCall(incomingCallLog).get();
    Assert.assertEquals(0, logs.size());
  }

}

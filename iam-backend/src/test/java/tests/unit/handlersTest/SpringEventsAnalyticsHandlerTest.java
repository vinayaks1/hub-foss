package tests.unit.handlersTest;

import com.everwell.iam.constants.Constants;
import com.everwell.iam.constants.RabbitMQConstants;
import com.everwell.iam.enums.AdherenceCodeEnum;
import com.everwell.iam.enums.AdherenceTech;
import com.everwell.iam.enums.MatomoEventCategory;
import com.everwell.iam.handlers.AdhTechHandlerMap;
import com.everwell.iam.handlers.SpringEventsAnalyticsHandler;
import com.everwell.iam.models.db.AccessMapping;
import com.everwell.iam.models.db.CAccess;
import com.everwell.iam.models.db.Registration;
import com.everwell.iam.models.dto.eventstreaming.analytics.*;
import com.everwell.iam.repositories.AccessMappingRepository;
import com.everwell.iam.repositories.RegistrationRepository;
import com.everwell.iam.services.ClientEventFlowConfigMap;
import com.everwell.iam.services.ClientService;
import com.everwell.iam.services.RabbitMQPublisherService;
import com.everwell.iam.utils.Utils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import tests.BaseTestNoSpring;

import javax.swing.text.html.Option;
import java.io.IOException;
import java.lang.reflect.Array;
import java.text.ParseException;
import java.util.*;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.configuration.ConfigurationType.PowerMock;


@RunWith(PowerMockRunner.class)
@PowerMockIgnore({"javax.xml.*", "com.sun.org.apache.xerces.*", "org.slf4j.*", "org.xml.*","com.ibm.*"})
public class SpringEventsAnalyticsHandlerTest  {

    @InjectMocks
    SpringEventsAnalyticsHandler springEventsAnalyticsHandler;

    @Mock
    RegistrationRepository registrationRepository;

    @Mock
    AccessMappingRepository accessMappingRepository;

    @Mock
    private AdhTechHandlerMap adherenceTechnologyHandlerMap;

    @Mock
    AdherenceTestHandler nndHandler;

    @Mock
    AdherenceTestHandler noneHandler;

    @Mock
    RabbitMQPublisherService rabbitMQPublisherService;

    @Mock
    ClientService clientService;

    @Mock
    private ClientEventFlowConfigMap clientEventFlowConfigMap;

    @PrepareForTest({Utils.class})
    @Test
    public void trackRegistrationEventTest() throws IOException {
        RegisterEntityTrackingDto registerEntityTrackingDto = new RegisterEntityTrackingDto("1", 1L, 1L, true, AdherenceTech.NNDOTS.name());
        String eventAction = Constants.MatomoEventConstants.REG_WITH_EVENT + AdherenceTech.NNDOTS.name();
        List<AccessMapping> accessMappingList = new ArrayList<>();
        when(accessMappingRepository.getAllByEntityIdAndClientId(any(), anyLong())).thenReturn(accessMappingList);
        mockStatic(Utils.class);
        when(Utils.getCurrentTime()).thenReturn(30L);
        CAccess cAccess = new CAccess();
        when(clientService.getClientById(anyLong())).thenReturn(cAccess);
        springEventsAnalyticsHandler.trackRegistrationEvent(registerEntityTrackingDto);

        EventStreamingAnalyticsDto eventStreamingAnalyticsDto = new EventStreamingAnalyticsDto(
            "1" , eventAction, MatomoEventCategory.REGISTER_ENTITY.name()
        );

        Mockito.verify(rabbitMQPublisherService, Mockito.times(1))
            .send(eq(Utils.asJsonString(eventStreamingAnalyticsDto)), eq(""), eq(RabbitMQConstants.EVENTFLOW_EXCHANGE), any());
    }

    @PrepareForTest({Utils.class})
    @Test
    public void trackRegistrationSwitchEventTest() throws IOException, ParseException {
        RegisterEntityTrackingDto registerEntityTrackingDto = new RegisterEntityTrackingDto("2", 1L, 1L, true, AdherenceTech.NNDOTS.name());
        String eventAction = Constants.MatomoEventConstants.SWITCH_EVENT + AdherenceTech.NONE.name() + "_" + AdherenceTech.NNDOTS.name();
        List<AccessMapping> accessMappingList = Arrays.asList(
            new AccessMapping(1L,1L,"1"),
            new AccessMapping(1L,2L,"2")
        );

        List<Registration> registrationList = Arrays.asList(
            new Registration(1L, 1L, Utils.convertStringToDate("2019-08-31 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26")),
            new Registration(2L, 2L, Utils.convertStringToDate("2019-09-30 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"))
        );

        when(registrationRepository.findByIdIn(anyList())).thenReturn(registrationList);
        when(nndHandler.getAdhTechEnumerator()).thenReturn(AdherenceTech.NNDOTS);
        when(noneHandler.getAdhTechEnumerator()).thenReturn(AdherenceTech.NONE);
        when(adherenceTechnologyHandlerMap.getHandler(eq(1L))).thenReturn(nndHandler);
        when(adherenceTechnologyHandlerMap.getHandler(eq(2L))).thenReturn(noneHandler);
        when(accessMappingRepository.getAllByEntityIdAndClientId(any(), anyLong())).thenReturn(accessMappingList);
        mockStatic(Utils.class);
        when(Utils.getCurrentTime()).thenReturn(30L);
        CAccess cAccess = new CAccess();
        when(clientService.getClientById(anyLong())).thenReturn(cAccess);
        springEventsAnalyticsHandler.trackRegistrationEvent(registerEntityTrackingDto);

        EventStreamingAnalyticsDto eventStreamingAnalyticsDto = new EventStreamingAnalyticsDto(
            "2" , eventAction, MatomoEventCategory.REGISTER_ENTITY.name()
        );

        Mockito.verify(rabbitMQPublisherService, Mockito.times(1))
            .send(eq(Utils.asJsonString(eventStreamingAnalyticsDto)), eq(""), eq(RabbitMQConstants.EVENTFLOW_EXCHANGE), any());
    }
    @PrepareForTest({Utils.class})
    @Test
    public void trackReopenEventTest() throws IOException {
        RegisterEntityTrackingDto registerEntityTrackingDto = new RegisterEntityTrackingDto("1", 1L, 1L, false, AdherenceTech.NNDOTS.name());
        String eventAction = Constants.MatomoEventConstants.REOPEN_WITH_EVENT + AdherenceTech.NNDOTS.name();
        mockStatic(Utils.class);
        when(Utils.getCurrentTime()).thenReturn(30L);
        CAccess cAccess = new CAccess();
        when(clientService.getClientById(anyLong())).thenReturn(cAccess);
        springEventsAnalyticsHandler.trackRegistrationEvent(registerEntityTrackingDto);

        EventStreamingAnalyticsDto eventStreamingAnalyticsDto = new EventStreamingAnalyticsDto(
            "1" , eventAction, MatomoEventCategory.REOPEN_CASE.name()
        );

        Mockito.verify(rabbitMQPublisherService, Mockito.times(1))
            .send(eq(Utils.asJsonString(eventStreamingAnalyticsDto)), eq(""), eq(RabbitMQConstants.EVENTFLOW_EXCHANGE), any());
    }
    @PrepareForTest({Utils.class})
    @Test
    public void trackCloseCaseEventTest() throws IOException, ParseException {
        CloseCaseTrackingDto closeCaseTrackingDto = new CloseCaseTrackingDto("2", 1L, 29L);
        String eventAction = Constants.MatomoEventConstants.CLOSE_WITH_EVENT + AdherenceTech.NNDOTS.name();

        Registration registration = new Registration(2L, 2L, Utils.convertStringToDate("2019-09-30 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));

        when(registrationRepository.findById(any())).thenReturn(Optional.of(registration));
        when(nndHandler.getAdhTechEnumerator()).thenReturn(AdherenceTech.NNDOTS);
        when(adherenceTechnologyHandlerMap.getHandler(anyLong())).thenReturn(nndHandler);
        mockStatic(Utils.class);
        when(Utils.getCurrentTime()).thenReturn(30L);
        CAccess cAccess = new CAccess();
        when(clientService.getClientById(anyLong())).thenReturn(cAccess);
        springEventsAnalyticsHandler.trackCloseCaseEvent(closeCaseTrackingDto);

        EventStreamingAnalyticsDto eventStreamingAnalyticsDto = new EventStreamingAnalyticsDto(
            "2" , eventAction, MatomoEventCategory.CLOSE_ENTITY.name()
        );

        Mockito.verify(rabbitMQPublisherService, Mockito.times(1))
            .send(eq(Utils.asJsonString(eventStreamingAnalyticsDto)), eq(""), eq(RabbitMQConstants.EVENTFLOW_EXCHANGE), any());
    }
    @PrepareForTest({Utils.class})
    @Test
    public void trackRecordAdherenceMissedEventTest() throws IOException, ParseException {
        Registration registration = new Registration(2L, 2L, Utils.convertStringToDate("2019-09-30 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
        TreeMap<Date, AdherenceCodeEnum> map = new TreeMap<>();
        map.put(Utils.convertStringToDate("2019-09-05 18:30:00"), AdherenceCodeEnum.MISSED);
        AccessMapping accessMapping = new AccessMapping(1L,1L,"1");
        RecordAdherenceTrackingDto recordAdherenceTrackingDto = new RecordAdherenceTrackingDto(map, registration,  AdherenceTech.NNDOTS.name());
        String eventAction = AdherenceCodeEnum.MISSED.getTrackingEventName() + Constants.MatomoEventConstants.WITH + AdherenceTech.NNDOTS.name();
        CAccess cAccess = new CAccess();
        when(clientService.getClientById(anyLong())).thenReturn(cAccess);
        when(accessMappingRepository.getByIamId(any())).thenReturn(Arrays.asList(accessMapping));
        mockStatic(Utils.class);
        when(Utils.getCurrentTime()).thenReturn(30L);
        springEventsAnalyticsHandler.trackRecordAdherence(recordAdherenceTrackingDto);

        EventStreamingAnalyticsDto eventStreamingAnalyticsDto = new EventStreamingAnalyticsDto(
            "1" , eventAction, MatomoEventCategory.RECORD_ADHERENCE.name()
        );

        Mockito.verify(rabbitMQPublisherService, Mockito.times(1))
            .send(eq(Utils.asJsonString(eventStreamingAnalyticsDto)), eq(""), eq(RabbitMQConstants.EVENTFLOW_EXCHANGE), any());
    }
    @PrepareForTest({Utils.class})
    @Test
    public void trackRecordAdherenceManualEventTest() throws IOException, ParseException {
        Registration registration = new Registration(2L, 2L, Utils.convertStringToDate("2019-09-30 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
        TreeMap<Date, AdherenceCodeEnum> map = new TreeMap<>();
        map.put(Utils.convertStringToDate("2019-09-05 18:30:00"), AdherenceCodeEnum.MANUAL);
        map.put(Utils.convertStringToDate("2019-09-06 18:30:00"), AdherenceCodeEnum.MANUAL);
        AccessMapping accessMapping = new AccessMapping(1L,1L,"1");
        RecordAdherenceTrackingDto recordAdherenceTrackingDto = new RecordAdherenceTrackingDto(map, registration, AdherenceTech.NNDOTS.name());
        String eventAction = AdherenceCodeEnum.MANUAL.getTrackingEventName() + Constants.MatomoEventConstants.WITH + AdherenceTech.NNDOTS.name();
        CAccess cAccess = new CAccess();
        when(clientService.getClientById(anyLong())).thenReturn(cAccess);
        when(accessMappingRepository.getByIamId(any())).thenReturn(Arrays.asList(accessMapping));
        mockStatic(Utils.class);
        when(Utils.getCurrentTime()).thenReturn(30L);
        springEventsAnalyticsHandler.trackRecordAdherence(recordAdherenceTrackingDto);

        EventStreamingAnalyticsDto eventStreamingAnalyticsDto = new EventStreamingAnalyticsDto(
            "1" , eventAction, MatomoEventCategory.RECORD_ADHERENCE.name()
        );

        Mockito.verify(rabbitMQPublisherService, Mockito.times(2))
            .send(eq(Utils.asJsonString(eventStreamingAnalyticsDto)), eq(""), eq(RabbitMQConstants.EVENTFLOW_EXCHANGE), any());
    }
    @PrepareForTest({Utils.class})
    @Test
    public void trackRecordAdherenceDigitalEventTest() throws IOException, ParseException {
        Registration registration = new Registration(2L, 2L, Utils.convertStringToDate("2019-09-30 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
        TreeMap<Date, AdherenceCodeEnum> map = new TreeMap<>();
        map.put(Utils.convertStringToDate("2019-09-05 18:30:00"), AdherenceCodeEnum.RECEIVED);
        map.put(Utils.convertStringToDate("2019-09-06 18:30:00"), AdherenceCodeEnum.RECEIVED);
        AccessMapping accessMapping = new AccessMapping(1L,1L,"1");
        RecordAdherenceTrackingDto recordAdherenceTrackingDto = new RecordAdherenceTrackingDto(map, registration, AdherenceTech.NNDOTS.name());
        String eventAction = AdherenceCodeEnum.RECEIVED.getTrackingEventName() + Constants.MatomoEventConstants.WITH + AdherenceTech.NNDOTS.name();
        CAccess cAccess = new CAccess();
        when(clientService.getClientById(anyLong())).thenReturn(cAccess);
        when(accessMappingRepository.getByIamId(any())).thenReturn(Arrays.asList(accessMapping));
        mockStatic(Utils.class);
        when(Utils.getCurrentTime()).thenReturn(30L);
        springEventsAnalyticsHandler.trackRecordAdherence(recordAdherenceTrackingDto);

        EventStreamingAnalyticsDto eventStreamingAnalyticsDto = new EventStreamingAnalyticsDto(
            "1" , eventAction, MatomoEventCategory.RECORD_ADHERENCE.name()
        );

        Mockito.verify(rabbitMQPublisherService, Mockito.times(2))
            .send(eq(Utils.asJsonString(eventStreamingAnalyticsDto)), eq(""), eq(RabbitMQConstants.EVENTFLOW_EXCHANGE), any());
    }
    @PrepareForTest({Utils.class})
    @Test
    public void trackRecordAdherenceUnmarkTest() throws IOException, ParseException {
        Registration registration = new Registration(2L, 2L, Utils.convertStringToDate("2019-09-30 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
        TreeMap<Date, AdherenceCodeEnum> map = new TreeMap<>();
        map.put(Utils.convertStringToDate("2019-09-05 18:30:00"), AdherenceCodeEnum.NO_INFO);
        map.put(Utils.convertStringToDate("2019-09-06 18:30:00"), AdherenceCodeEnum.NO_INFO);
        AccessMapping accessMapping = new AccessMapping(1L,1L,"1");
        RecordAdherenceTrackingDto recordAdherenceTrackingDto = new RecordAdherenceTrackingDto(map, registration, AdherenceTech.NNDOTS.name());
        String eventAction = AdherenceCodeEnum.NO_INFO.getTrackingEventName() + Constants.MatomoEventConstants.WITH + AdherenceTech.NNDOTS.name();
        CAccess cAccess = new CAccess();
        when(clientService.getClientById(anyLong())).thenReturn(cAccess);
        when(accessMappingRepository.getByIamId(any())).thenReturn(Arrays.asList(accessMapping));
        mockStatic(Utils.class);
        when(Utils.getCurrentTime()).thenReturn(30L);
        springEventsAnalyticsHandler.trackRecordAdherence(recordAdherenceTrackingDto);

        EventStreamingAnalyticsDto eventStreamingAnalyticsDto = new EventStreamingAnalyticsDto(
            "1" , eventAction, MatomoEventCategory.RECORD_ADHERENCE.name()
        );

        Mockito.verify(rabbitMQPublisherService, Mockito.times(2))
            .send(eq(Utils.asJsonString(eventStreamingAnalyticsDto)), eq(""), eq(RabbitMQConstants.EVENTFLOW_EXCHANGE), any());
    }
    @PrepareForTest({Utils.class})
    @Test
    public void trackRecordAdherencePatientManualTest() throws IOException, ParseException {
        Registration registration = new Registration(2L, 2L, Utils.convertStringToDate("2019-09-30 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
        TreeMap<Date, AdherenceCodeEnum> map = new TreeMap<>();
        map.put(Utils.convertStringToDate("2019-09-05 18:30:00"), AdherenceCodeEnum.PATIENT_MANUAL);
        map.put(Utils.convertStringToDate("2019-09-06 18:30:00"), AdherenceCodeEnum.PATIENT_MANUAL);
        AccessMapping accessMapping = new AccessMapping(1L,1L,"1");
        RecordAdherenceTrackingDto recordAdherenceTrackingDto = new RecordAdherenceTrackingDto(map, registration, AdherenceTech.NNDOTS.name());
        String eventAction = AdherenceCodeEnum.PATIENT_MANUAL.getTrackingEventName() + Constants.MatomoEventConstants.WITH + AdherenceTech.NNDOTS.name();
        CAccess cAccess = new CAccess();
        when(clientService.getClientById(anyLong())).thenReturn(cAccess);
        when(accessMappingRepository.getByIamId(any())).thenReturn(Arrays.asList(accessMapping));
        mockStatic(Utils.class);
        when(Utils.getCurrentTime()).thenReturn(30L);
        springEventsAnalyticsHandler.trackRecordAdherence(recordAdherenceTrackingDto);

        EventStreamingAnalyticsDto eventStreamingAnalyticsDto = new EventStreamingAnalyticsDto(
            "1" , eventAction, MatomoEventCategory.RECORD_ADHERENCE.name()
        );

        Mockito.verify(rabbitMQPublisherService, Mockito.times(2))
            .send(eq(Utils.asJsonString(eventStreamingAnalyticsDto)), eq(""), eq(RabbitMQConstants.EVENTFLOW_EXCHANGE), any());
    }
    @PrepareForTest({Utils.class})
    @Test
    public void trackRecordAdherencePatientManualMissedTest() throws IOException, ParseException {
        Registration registration = new Registration(2L, 2L, Utils.convertStringToDate("2019-09-30 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
        TreeMap<Date, AdherenceCodeEnum> map = new TreeMap<>();
        map.put(Utils.convertStringToDate("2019-09-05 18:30:00"), AdherenceCodeEnum.PATIENT_MISSED);
        map.put(Utils.convertStringToDate("2019-09-06 18:30:00"), AdherenceCodeEnum.PATIENT_MISSED);
        AccessMapping accessMapping = new AccessMapping(1L,1L,"1");
        RecordAdherenceTrackingDto recordAdherenceTrackingDto = new RecordAdherenceTrackingDto(map, registration, AdherenceTech.NNDOTS.name());
        String eventAction = AdherenceCodeEnum.PATIENT_MISSED.getTrackingEventName() + Constants.MatomoEventConstants.WITH + AdherenceTech.NNDOTS.name();
        CAccess cAccess = new CAccess();
        when(clientService.getClientById(anyLong())).thenReturn(cAccess);
        when(accessMappingRepository.getByIamId(any())).thenReturn(Arrays.asList(accessMapping));
        mockStatic(Utils.class);
        when(Utils.getCurrentTime()).thenReturn(30L);
        springEventsAnalyticsHandler.trackRecordAdherence(recordAdherenceTrackingDto);

        EventStreamingAnalyticsDto eventStreamingAnalyticsDto = new EventStreamingAnalyticsDto(
            "1" , eventAction, MatomoEventCategory.RECORD_ADHERENCE.name()
        );

        Mockito.verify(rabbitMQPublisherService, Mockito.times(2))
            .send(eq(Utils.asJsonString(eventStreamingAnalyticsDto)), eq(""), eq(RabbitMQConstants.EVENTFLOW_EXCHANGE), any());
    }

}

package tests.unit.handlersTest;

import com.everwell.iam.enums.AdherenceTech;
import com.everwell.iam.handlers.AdherenceTechHandler;
import com.everwell.iam.handlers.ScheduleHandler;
import com.everwell.iam.models.db.Registration;
import com.everwell.iam.models.dto.AdhTechLogsData;
import com.everwell.iam.models.dto.AdherenceData;
import com.everwell.iam.models.http.requests.EntityRequest;


import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

public class AdherenceTestHandler extends AdherenceTechHandler {
    @Override
    public void postRegistration(Registration registration, EntityRequest entityRequest, boolean isNew) { }

    @Override
    public void preDeletion(Registration registration) { }

    @Override
    public AdherenceTech getAdhTechEnumerator() {
        return null;
    }

    @Override
    public CompletableFuture<List<Long>> regenerateAdherence(Set<Long> iamIds) {
        return null;
    }

    @Override
    public List<AdhTechLogsData> getTechLogs(Long iamId) {
        return null;
    }

    @Override
    public Registration save(EntityRequest entityRequest) {
        return null;
    }

    @Override
    public String generateAdherenceString(Long iamId, Date startDate, Date endDate, Long scheduleTypeId) {
        return null;
    }

    @Override
    public ScheduleHandler getScheduleHandler(Long scheduleTypeId) {
        return null;
    }

    @Override
    public AdherenceData getAdherenceDataForIam(Long iamId, Date startDate, Date endDate, Long scheduleTypeId, Date lastDosage, Date LastMissedDosage) { return null; }

    @Override
    public Date computeLastDosage(String adherenceString, Date endDate, Boolean isMissedDosage) { return null; }

    @Override
    public void deleteRegistration(Set<Long> iamId) {}
}

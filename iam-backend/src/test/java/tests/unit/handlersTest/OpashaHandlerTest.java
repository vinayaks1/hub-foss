package tests.unit.handlersTest;

import com.everwell.iam.handlers.impl.OpAshaHandler;
import com.everwell.iam.models.db.CAccess;
import com.everwell.iam.models.dto.AdhTechLogsData;
import com.everwell.iam.repositories.CAccessRepository;
import org.apache.commons.lang3.NotImplementedException;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

public class OpashaHandlerTest extends AdherenceTechHandlerTest {

  @InjectMocks
  OpAshaHandler opAshaHandler;

  @Mock
  private CAccessRepository cAccessRepository;

  private static final Long OPASHA_ID = 4L;

  @Override
  public void getTechLogsTest() throws Exception {
    List<AdhTechLogsData> adhTechLogsData = opAshaHandler.getTechLogs(1L);
    assertEquals(0, adhTechLogsData.size());
  }

  @Test(expected= NotImplementedException.class)
  public void testRegenerateAdherence() {
    Set<Long> iamds = new HashSet<>();
    opAshaHandler.regenerateAdherence(iamds);
  }

  public void testGetClientIdSuccess() {
    CAccess dummyAccess = new CAccess();
    dummyAccess.setId(29L);
    when(cAccessRepository.findByName(any())).thenReturn(dummyAccess);
    Assert.assertEquals(opAshaHandler.getClientId(),dummyAccess.getId());
  }

  @Override
  public void deleteRegistration() {
    opAshaHandler.deleteRegistration(anySet());
    Mockito.verify(registrationRepository, Mockito.times(1)).deleteAllByIdIn(anySet());
    Mockito.verify(adhStringLogRepository, Mockito.times(1)).deleteAllByIamIds(anySet());
  }

}

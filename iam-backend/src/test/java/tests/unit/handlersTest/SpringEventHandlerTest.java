package tests.unit.handlersTest;

import com.everwell.iam.constants.RabbitMQConstants;
import com.everwell.iam.enums.CacheAdherenceStatusToday;
import com.everwell.iam.exceptions.NotFoundException;
import com.everwell.iam.handlers.AdhTechHandlerMap;
import com.everwell.iam.handlers.SpringEventHandler;
import com.everwell.iam.models.db.AccessMapping;
import com.everwell.iam.models.db.CAccess;
import com.everwell.iam.models.db.Registration;
import com.everwell.iam.models.dto.springevents.AnyAdherenceEvent;
import com.everwell.iam.models.dto.springevents.EpisodeEvent;
import com.everwell.iam.models.http.requests.NNDEntityRequest;
import com.everwell.iam.models.http.requests.ProcessUnregisteredPhoneNumberRequest;
import com.everwell.iam.models.http.requests.RegenerateAdherenceRequest;
import com.everwell.iam.repositories.AccessMappingRepository;
import com.everwell.iam.repositories.RegistrationRepository;
import com.everwell.iam.services.AdherenceService;
import com.everwell.iam.services.ClientService;
import com.everwell.iam.services.RabbitMQPublisherService;
import com.everwell.iam.utils.CacheUtils;
import com.everwell.iam.utils.Utils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import tests.BaseTestNoSpring;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;

public class SpringEventHandlerTest extends BaseTestNoSpring {

    @Spy
    @InjectMocks
    SpringEventHandler springEventhandler;

    @Mock
    RabbitMQPublisherService rabbitMQPublisherService;

    @Mock
    RegistrationRepository registrationRepository;

    @Mock
    AccessMappingRepository accessMappingRepository;

    CacheUtils cacheUtils;

    @Mock
    RedisTemplate<String, Object> redisTemplate;

    @Mock
    private ValueOperations<String, Object> valueOperations;

    @Mock
    AdherenceService adherenceService;

    @Mock
    AdhTechHandlerMap adherenceTechnologyHandlerMap;

    @Mock
    AdherenceTestHandler adherenceTestHandler;

    @Mock
    ClientService clientService;

    @Before
    public void init() {
        cacheUtils = new CacheUtils(redisTemplate);
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
    }

    @Test
    public void test_publish_last_dosage() throws NotFoundException, ParseException {
        List<Registration> registrations = new ArrayList<>();
        Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-01-01 18:30:00"), Utils.convertStringToDate("2019-01-16 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
        registrations.add(registration);
        List<AccessMapping> accessMappingList = new ArrayList<>();
        AccessMapping access= new AccessMapping(1L,1L,"123");
        accessMappingList.add(access);
        doNothing().when(rabbitMQPublisherService).send(any(),any(),anyString(),anyMap());
        AnyAdherenceEvent event = new AnyAdherenceEvent(null, registration, false, true, false,false,true);
        CAccess cAccess = new CAccess();
        when(clientService.getClientById(anyLong())).thenReturn(cAccess);
        when(accessMappingRepository.getAllByEntityIdAndClientId(any(), anyLong())).thenReturn(accessMappingList);
        when(registrationRepository.findByIdIn(any())).thenReturn(registrations);
        springEventhandler.lastDosageChangeEventImpl(event, "123", 1L);
        verify(rabbitMQPublisherService, Mockito.times(2)).send(any(),any(),anyString(),anyMap());
    }

    @Test
    public void delete_call_key_from_cache() throws ParseException {
        Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-01-01 18:30:00"), Utils.convertStringToDate("2019-01-16 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
        AnyAdherenceEvent event = new AnyAdherenceEvent(CacheAdherenceStatusToday.NO_INFO, registration, false, false, false,false,true);
        springEventhandler.insertAdherenceStatusInCache(event.getAdherenceStatusType(), "1", registration);
        verify(redisTemplate, Mockito.times(1)).delete(anyString());
    }

    @Test
    public void testRegenerateAdherenceEvent() throws ParseException {
        List<Long> iamIdList = Arrays.asList(1L,2L);
        RegenerateAdherenceRequest regenRequest = new RegenerateAdherenceRequest(iamIdList);
        doNothing().when(rabbitMQPublisherService).send(any(),any(),anyString(),anyMap());
        springEventhandler.regenerateAdherenceEvent(regenRequest);
        verify(rabbitMQPublisherService, Mockito.times(iamIdList.size())).send(any(),any(),anyString(),anyMap());
    }

    @Test
    public void receivedCallFromUnregisteredPhoneEvent_success_rabbitMQInvoked() {
        ProcessUnregisteredPhoneNumberRequest unregisteredPhoneNumberRequest = new ProcessUnregisteredPhoneNumberRequest("911234567890", "1800180180");
        doNothing().when(rabbitMQPublisherService).send(any(),any(),anyString(),anyMap());
        springEventhandler.receivedCallFromUnregisteredPhoneEvent(unregisteredPhoneNumberRequest);
        verify(rabbitMQPublisherService, Mockito.times(1)).send(any(),any(),anyString(),anyMap());
    }

    @Test
    public void insert_into_cache_test() throws ParseException {
        Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-01-01 18:30:00"), Utils.convertStringToDate("2019-01-16 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
        springEventhandler.insertAdherenceStatusInCache(CacheAdherenceStatusToday.DIGITAL, "1", registration);
        verify(valueOperations, Mockito.times(1)).set(anyString(), any(), anyLong(), any());
    }

    @Test
    public void update_adherence_string() throws ParseException {
        Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-01-01 18:30:00"), Utils.convertStringToDate("2019-01-16 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
        AnyAdherenceEvent event = new AnyAdherenceEvent(null, registration, false, false, false,true,true);
        when(adherenceTechnologyHandlerMap.getHandler(anyLong())).thenReturn(adherenceTestHandler);
        when(adherenceTestHandler.generateAdherenceString(anyLong(), any(), any(), anyLong())).thenReturn("6644666295");
        when(registrationRepository.save(any())).thenReturn(null);
        springEventhandler.updateAdherenceString(event);
        Assert.assertEquals(registration.getAdherenceString(), "6644666295");
    }

    @Test
    public void emitEventForEpisodeTest() throws ParseException {
        Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-01-01 18:30:00"), Utils.convertStringToDate("2019-01-16 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
        NNDEntityRequest nndEntityRequest = new NNDEntityRequest();
        nndEntityRequest.setClientId(1L);
        EpisodeEvent episodeEvent = new EpisodeEvent(registration, nndEntityRequest);
        CAccess cAccess = new CAccess();
        when(clientService.getClientById(anyLong())).thenReturn(cAccess);
        springEventhandler.emitEventForEpisode(episodeEvent);
        Mockito.verify(rabbitMQPublisherService, Mockito.times(1))
                .send(anyString(), eq(RabbitMQConstants.Q_EPISODE_UPDATE_TRACKER), eq(RabbitMQConstants.EX_DIRECT), anyMap());
    }

}

package tests.unit.handlersTest;

import com.everwell.iam.exceptions.ValidationException;
import com.everwell.iam.handlers.ScheduleHandler;
import com.everwell.iam.handlers.impl.DailyScheduleHandler;
import com.everwell.iam.handlers.impl.MultiFreqWeeklyScheduleHandler;
import com.everwell.iam.handlers.impl.WeeklyScheduleHandler;
import com.everwell.iam.models.db.AdherenceStringLog;
import com.everwell.iam.models.db.Schedule;
import com.everwell.iam.models.db.ScheduleMap;
import com.everwell.iam.models.db.ScheduleTimeMap;
import com.everwell.iam.models.dto.DoseTimeData;
import com.everwell.iam.models.dto.ScheduleSensitivity;
import com.everwell.iam.repositories.ScheduleMapRepository;
import com.everwell.iam.repositories.ScheduleRepository;
import com.everwell.iam.repositories.ScheduleTimeMapRepository;
import com.everwell.iam.utils.Utils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import tests.BaseTestNoSpring;

import java.security.InvalidParameterException;
import java.sql.Time;
import java.text.ParseException;
import java.util.*;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

public class MultiFrequencyWeeklyScheduleHandlerTest extends BaseTestNoSpring {

    @Spy
    @InjectMocks
    MultiFreqWeeklyScheduleHandler multiFreqWeeklyScheduleHandler;

    @Mock
    ScheduleMapRepository scheduleMapRepository;

    @Mock
    ScheduleRepository scheduleRepository;

    @Mock
    private ScheduleTimeMapRepository scheduleTimeMapRepository;

    //to test the hasScheduleMapping() checks
    @Spy
    DailyScheduleHandler dailyScheduleHandler;

    @Spy
    WeeklyScheduleHandler weeklyScheduleHandler;

    @Before
    public void init() {
        weeklyScheduleHandler.setScheduleTypeId(2L);
        dailyScheduleHandler.setScheduleTypeId(1L);
        multiFreqWeeklyScheduleHandler.setScheduleTypeId(3L);
    }

    @Test
    public void validate_schedule_sensitivity_no_overlap() {
        ScheduleSensitivity scheduleSensitivity = new ScheduleSensitivity(0L, 0L);
        List<String> doseTime = Arrays.asList("03:30:00", "07:30:00");

        //no exceptions, successfully validated
        multiFreqWeeklyScheduleHandler.validateScheduleAndSensitivityOverLap(null, scheduleSensitivity, doseTime);
    }

    @Test(expected = InvalidParameterException.class)
    public void validate_schedule_sensitivity_overlap() {
        ScheduleSensitivity scheduleSensitivity = new ScheduleSensitivity(0L, 0L);
        List<String> doseTime = Arrays.asList("03:30:00", "04:00:00", "03:30:00");

        multiFreqWeeklyScheduleHandler.validateScheduleAndSensitivityOverLap(null, scheduleSensitivity, doseTime);
    }

    @Test
    public void saveScheduleAndCreateTimeMap() {
        ScheduleMap scheduleMap = new ScheduleMap();
        scheduleMap.setScheduleId(1L);
        List<String> doseTime = Arrays.asList("03:30:00", "04:00:00");

        when(scheduleMapRepository.save(any())).thenReturn(scheduleMap);

        multiFreqWeeklyScheduleHandler.save(1L, Utils.getCurrentDate(), null, null, new ScheduleSensitivity(0L, 0L), doseTime);

        Mockito.verify(scheduleTimeMapRepository, Mockito.times(1)).saveAll(any());
    }

    @Test(expected = ValidationException.class)
    public void shouldTakeDoseForDatesDoseExists() throws ParseException {
        AdherenceStringLog log = new AdherenceStringLog(1L, '4', Utils.convertStringToDate("2022-11-13 03:30:00"));
        Set<Date> dates = new HashSet<>(Collections.singletonList(Utils.convertStringToDate("2022-11-13 03:30:00")));

        multiFreqWeeklyScheduleHandler.shouldTakeDoseForDates(dates, 1L, Collections.singletonList(log));
    }

    @Test(expected = ValidationException.class)
    public void shouldTakeDoseForEmptySchedule() throws ParseException {
        AdherenceStringLog log = new AdherenceStringLog(1L, '4', Utils.convertStringToDate("2022-11-13 03:30:00"));
        Set<Date> dates = new HashSet<>(Collections.singletonList(Utils.convertStringToDate("2022-11-13 11:30:00")));

        List<ScheduleMap> scheduleMapList = new ArrayList<>();
        scheduleMapList.add(new ScheduleMap(1L,true, 1L, Utils.convertStringToDate("2020-01-01 18:30:00"), null, Utils.convertStringToDate("2020-01-01 18:30:00"), Utils.convertStringToDate("2023-01-25 18:29:00"), 0L, new ScheduleSensitivity(0L, 0L)));
        when(scheduleMapRepository.findAllByIamId(eq(1L))).thenReturn(scheduleMapList);
        when(scheduleTimeMapRepository.findAllByScheduleMapIdIn(eq(Collections.singletonList(1L)))).thenReturn(Collections.emptyList());

        multiFreqWeeklyScheduleHandler.shouldTakeDoseForDates(dates, 1L, Collections.singletonList(log));
    }

    @Test(expected = ValidationException.class)
    public void shouldTakeDoseWithNoSchedules() throws ParseException {
        AdherenceStringLog log = new AdherenceStringLog(1L, '4', Utils.convertStringToDate("2022-11-13 03:30:00"));
        Set<Date> dates = new HashSet<>(Collections.singletonList(Utils.convertStringToDate("2022-11-13 11:30:00")));

        when(scheduleMapRepository.findAllByIamId(eq(1L))).thenReturn(new ArrayList<>());
        when(scheduleTimeMapRepository.findAllByScheduleMapIdIn(any())).thenReturn(Collections.emptyList());

        multiFreqWeeklyScheduleHandler.shouldTakeDoseForDates(dates, 1L, Collections.singletonList(log));
    }

    @Test(expected = ValidationException.class)
    public void shouldTakeDoseForDatesTimeDoesNotExist() throws ParseException {
        Set<Date> dates = new HashSet<>(Collections.singletonList(Utils.convertStringToDate("2022-11-13 11:30:00")));

        List<ScheduleMap> scheduleMapList = new ArrayList<>();
        scheduleMapList.add(new ScheduleMap(1L,true, 1L, Utils.convertStringToDate("2020-01-01 18:30:00"), null, Utils.convertStringToDate("2020-01-01 18:30:00"), Utils.convertStringToDate("2023-01-25 18:29:00"), 0L, new ScheduleSensitivity(0L, 0L)));
        when(scheduleMapRepository.findAllByIamId(eq(1L))).thenReturn(scheduleMapList);
        when(scheduleTimeMapRepository.findAllByScheduleMapIdIn(any())).thenReturn(Collections.singletonList(new ScheduleTimeMap(1L, true, Utils.getCurrentDate(), 1L, Time.valueOf("03:30:00"))));

        multiFreqWeeklyScheduleHandler.shouldTakeDoseForDates(dates, 1L, Collections.emptyList());
    }

    @Test
    public void shouldTakeDoseForDatesNoException() throws ParseException {
        Set<Date> dates = new HashSet<>(Collections.singletonList(Utils.convertStringToDate("2022-11-13 03:30:00")));

        List<ScheduleMap> scheduleMapList = new ArrayList<>();
        scheduleMapList.add(new ScheduleMap(1L,true, 1L, Utils.convertStringToDate("2020-01-01 18:30:00"), null, Utils.convertStringToDate("2020-01-01 18:30:00"), Utils.convertStringToDate("2023-01-25 18:29:00"), 0L, new ScheduleSensitivity(0L, 0L)));
        List<ScheduleTimeMap> scheduleTimeMapList = new ArrayList<>();
        scheduleTimeMapList.add(new ScheduleTimeMap(1L, true, null, 1L, Time.valueOf("03:30:00")));
        when(scheduleMapRepository.findAllByIamId(eq(1L))).thenReturn(scheduleMapList);
        when(scheduleTimeMapRepository.findAllByScheduleMapIdIn(eq(Collections.singletonList(1L)))).thenReturn(scheduleTimeMapList);

        //no exceptions, successfully validated
        multiFreqWeeklyScheduleHandler.shouldTakeDoseForDates(dates, 1L, Collections.emptyList());

        Mockito.verify(scheduleMapRepository, Mockito.times(1)).findAllByIamId(any());
        Mockito.verify(scheduleTimeMapRepository, Mockito.times(1)).findAllByScheduleMapIdIn(any());
    }

    @Test
    public void getTransformedValueReceived() throws ParseException {
        AdherenceStringLog log = new AdherenceStringLog(1L, '4', Utils.convertStringToDate("2022-11-13 03:30:00"));
        List<ScheduleMap> scheduleMapList = new ArrayList<>();
        scheduleMapList.add(new ScheduleMap(1L,true, 1L, Utils.convertStringToDate("2020-01-01 18:30:00"), null, Utils.convertStringToDate("2020-01-01 18:30:00"), Utils.convertStringToDate("2023-01-01 18:30:00"), 0L, new ScheduleSensitivity(0L, 0L)));
        List<ScheduleTimeMap> scheduleTimeMapList = new ArrayList<>();
        scheduleTimeMapList.add(new ScheduleTimeMap(1L, true, null, 1L, Time.valueOf("03:30:00")));
        when(scheduleMapRepository.findAllByIamId(eq(1L))).thenReturn(scheduleMapList);
        when(scheduleTimeMapRepository.findAllByScheduleMapId(eq(1L))).thenReturn(scheduleTimeMapList);

        Character value = multiFreqWeeklyScheduleHandler.getTransformedValue('9', 0L, 1L, Collections.singletonList(log), Utils.convertStringToDate("2022-11-12 18:30:00"), Utils.convertStringToDate("2022-11-13 18:30:00"));

        Assert.assertEquals('4', value.charValue());
        Mockito.verify(scheduleMapRepository, Mockito.times(1)).findAllByIamId(any());
        Mockito.verify(scheduleTimeMapRepository, Mockito.times(1)).findAllByScheduleMapId(any());
    }

    @Test
    public void getTransformedValuePartial() throws ParseException {
        AdherenceStringLog log = new AdherenceStringLog(1L, '4', Utils.convertStringToDate("2022-11-13 03:30:00"));
        List<ScheduleMap> scheduleMapList = new ArrayList<>();
        scheduleMapList.add(new ScheduleMap(1L,true, 1L, Utils.convertStringToDate("2020-01-01 18:30:00"), null, Utils.convertStringToDate("2020-01-01 18:30:00"), Utils.convertStringToDate("2023-01-01 18:30:00"), 0L, new ScheduleSensitivity(0L, 0L)));
        List<ScheduleTimeMap> scheduleTimeMapList = new ArrayList<>();
        scheduleTimeMapList.add(new ScheduleTimeMap(1L, true, null, 1L, Time.valueOf("03:30:00")));
        scheduleTimeMapList.add(new ScheduleTimeMap(2L, true, null, 1L, Time.valueOf("07:30:00")));
        when(scheduleMapRepository.findAllByIamId(eq(1L))).thenReturn(scheduleMapList);
        when(scheduleTimeMapRepository.findAllByScheduleMapId(eq(1L))).thenReturn(scheduleTimeMapList);

        Character value = multiFreqWeeklyScheduleHandler.getTransformedValue('9', 0L, 1L, Collections.singletonList(log), Utils.convertStringToDate("2022-11-12 18:30:00"), Utils.convertStringToDate("2022-11-13 18:30:00"));

        Assert.assertEquals('F', value.charValue());
        Mockito.verify(scheduleMapRepository, Mockito.times(1)).findAllByIamId(any());
        Mockito.verify(scheduleTimeMapRepository, Mockito.times(1)).findAllByScheduleMapId(any());
    }

    @Test
    public void getTransformedValueNoInfo() throws ParseException {
        AdherenceStringLog log = new AdherenceStringLog(1L, '4', Utils.convertStringToDate("2022-11-13 03:30:00"));
        List<ScheduleMap> scheduleMapList = new ArrayList<>();
        scheduleMapList.add(new ScheduleMap(1L,true, 1L, Utils.convertStringToDate("2020-01-01 18:30:00"), null, Utils.convertStringToDate("2020-01-01 18:30:00"), Utils.convertStringToDate("2023-01-01 18:30:00"), 0L, new ScheduleSensitivity(0L, 0L)));
        List<ScheduleTimeMap> scheduleTimeMapList = new ArrayList<>();
        scheduleTimeMapList.add(new ScheduleTimeMap(1L, true, null, 1L, Time.valueOf("03:30:00")));
        when(scheduleMapRepository.findAllByIamId(eq(1L))).thenReturn(scheduleMapList);
        when(scheduleTimeMapRepository.findAllByScheduleMapId(eq(1L))).thenReturn(scheduleTimeMapList);

        Character value = multiFreqWeeklyScheduleHandler.getTransformedValue('9', 0L, 1L, Collections.emptyList(), Utils.convertStringToDate("2022-11-12 18:30:00"), Utils.convertStringToDate("2022-11-13 18:30:00"));

        Assert.assertEquals('6', value.charValue());
        Mockito.verify(scheduleMapRepository, Mockito.times(1)).findAllByIamId(any());
        Mockito.verify(scheduleTimeMapRepository, Mockito.times(1)).findAllByScheduleMapId(any());
    }

    @Test
    public void testGenerateAdherenceString() throws ParseException {
        ScheduleMap scheduleMap = new ScheduleMap(1L, null, Utils.convertStringToDate("2022-11-13 22:30:00"), Utils.convertStringToDate("2022-11-13 18:30:00"), 0L, new ScheduleSensitivity(0L, 0L));
        scheduleMap.setId(1L);
        HashMap<Integer, Integer> doseFrequencyMap = new HashMap<>();
        doseFrequencyMap.put(0, 1);
        doseFrequencyMap.put(1, 2);
        doseFrequencyMap.put(2, 0);
        when(scheduleMapRepository.findAllByIamId(eq(1L))).thenReturn(Collections.singletonList(scheduleMap));
        when(scheduleTimeMapRepository.findAllByScheduleMapIdIn(any())).thenReturn(Arrays.asList(new ScheduleTimeMap(1L, true, Utils.convertStringToDate("2022-11-13 22:30:00"), 1L, Time.valueOf("03:30:00")), new ScheduleTimeMap(1L, true, Utils.convertStringToDate("2022-11-13 22:30:00"), 1L, Time.valueOf("11:30:00"))));

        String adherenceString = multiFreqWeeklyScheduleHandler.generateAdherenceString(new HashMap<>(), Utils.convertStringToDate("2022-11-13 18:30:00"), Utils.convertStringToDate("2022-11-16 18:29:59"), 1L, doseFrequencyMap);
        Assert.assertEquals("F46", adherenceString);
    }

    @Test
    public void testGenerateAdherenceStringWithEmptySchedule() throws ParseException {
        List<ScheduleMap> scheduleMapList = new ArrayList<>();
        ScheduleMap scheduleMapOld = new ScheduleMap(1L, null, Utils.convertStringToDate("2022-11-13 22:30:00"), Utils.convertStringToDate("2022-11-13 18:30:00"), 0L, new ScheduleSensitivity(0L, 0L));
        scheduleMapOld.setId(1L);
        scheduleMapOld.setEndDate(Utils.convertStringToDate("2022-11-16 00:00:00"));
        scheduleMapList.add(scheduleMapOld);
        ScheduleMap scheduleMapNew = new ScheduleMap(1L, null, Utils.convertStringToDate("2022-11-16 00:00:00"), Utils.convertStringToDate("2022-11-16 00:00:00"), 0L, new ScheduleSensitivity(0L, 0L));
        scheduleMapNew.setId(2L);
        scheduleMapList.add(scheduleMapNew);
        List<ScheduleTimeMap> scheduleTimeMapList = Arrays.asList(new ScheduleTimeMap(1L, true, Utils.convertStringToDate("2022-11-13 22:30:00"), 1L, Time.valueOf("03:30:00")), new ScheduleTimeMap(1L, true, Utils.convertStringToDate("2022-11-13 22:30:00"), 1L, Time.valueOf("11:30:00")));
        HashMap<Integer, Integer> doseFrequencyMap = new HashMap<>();
        doseFrequencyMap.put(0, 1);
        doseFrequencyMap.put(1, 2);
        doseFrequencyMap.put(2, 0);
        doseFrequencyMap.put(3, 0);
        when(scheduleMapRepository.findAllByIamId(eq(1L))).thenReturn(scheduleMapList);
        when(scheduleTimeMapRepository.findAllByScheduleMapIdIn(any())).thenReturn(scheduleTimeMapList);
        String adherenceString = multiFreqWeeklyScheduleHandler.generateAdherenceString(new HashMap<>(), Utils.convertStringToDate("2022-11-13 18:30:00"), Utils.convertStringToDate("2022-11-17 18:29:59"), 1L, doseFrequencyMap);
        Assert.assertEquals("F46G", adherenceString);
    }

    @Test
    public void testGenerateAdherenceStringWithNoSchedules() throws ParseException {
        HashMap<Integer, Integer> doseFrequencyMap = new HashMap<>();
        doseFrequencyMap.put(0, 1);
        doseFrequencyMap.put(1, 2);
        doseFrequencyMap.put(2, 0);
        doseFrequencyMap.put(3, 0);
        when(scheduleMapRepository.findAllByIamId(eq(1L))).thenReturn(new ArrayList<>());
        when(scheduleTimeMapRepository.findAllByScheduleMapIdIn(any())).thenReturn(new ArrayList<>());
        String adherenceString = multiFreqWeeklyScheduleHandler.generateAdherenceString(new HashMap<>(), Utils.convertStringToDate("2022-11-13 18:30:00"), Utils.convertStringToDate("2022-11-17 18:29:59"), 1L, doseFrequencyMap);
        Assert.assertEquals("GGGG", adherenceString);
    }

    @Test
    public void updateEndDateAndActiveStatus() throws ParseException {
        when(scheduleMapRepository.save(any())).thenReturn(new ScheduleMap());
        when(scheduleTimeMapRepository.findAllByScheduleMapId(any())).thenReturn(Collections.singletonList(new ScheduleTimeMap(1L, true, null, 1L, Time.valueOf("03:30:00"))));
        when(scheduleTimeMapRepository.saveAll(any())).thenReturn(Collections.singletonList(new ScheduleTimeMap()));

        ScheduleMap scheduleMap = new ScheduleMap(1L,false, 1L, Utils.convertStringToDate("2020-01-01 18:30:00"), null, Utils.convertStringToDate("2020-01-01 18:30:00"), Utils.convertStringToDate("2020-01-25 18:29:00"), 0L, new ScheduleSensitivity(0L, 0L));
        multiFreqWeeklyScheduleHandler.updateEndDateAndActiveStatus(scheduleMap, Utils.getCurrentDate(), true);
        Mockito.verify(scheduleTimeMapRepository, Mockito.times(1)).saveAll(any());
    }

    @Test
    public void testGetDoseTime() throws ParseException {
        List<ScheduleMap> scheduleMapList = new ArrayList<>();
        scheduleMapList.add(new ScheduleMap(1L,true, 1L, Utils.convertStringToDate("2020-01-01 18:30:00"), null, Utils.convertStringToDate("2020-01-01 18:30:00"), Utils.convertStringToDate("2020-01-25 18:29:00"), 0L, new ScheduleSensitivity(0L, 0L)));
        List<ScheduleTimeMap> scheduleTimeMapList = new ArrayList<>();
        scheduleTimeMapList.add(new ScheduleTimeMap(1L, true, null, 1L, Time.valueOf("03:30:00")));
        when(scheduleMapRepository.findAllByIamId(eq(1L))).thenReturn(scheduleMapList);
        when(scheduleTimeMapRepository.findAllByScheduleMapIdIn(eq(Collections.singletonList(1L)))).thenReturn(scheduleTimeMapList);

        List<DoseTimeData> doseTimeList = multiFreqWeeklyScheduleHandler.getDoseTimes(1L);

        Assert.assertEquals(scheduleMapList.size(), doseTimeList.size());
        Assert.assertEquals("03:30:00", doseTimeList.get(0).getDoseTimeList().get(0));
        Mockito.verify(scheduleMapRepository, Mockito.times(1)).findAllByIamId(any());
        Mockito.verify(scheduleTimeMapRepository, Mockito.times(1)).findAllByScheduleMapIdIn(any());
    }

    @Test
    public void saveScheduleForFirstTime() {
        Long iamId = 1L;
        List<String> doseTimeList = Arrays.asList("03:30:00", "04:00:00", "03:30:00");
        ScheduleMap scheduleMap = new ScheduleMap();
        scheduleMap.setScheduleId(1L);
        when(scheduleMapRepository.findAllByIamId(eq(iamId))).thenReturn(new ArrayList<>());
        when(scheduleMapRepository.save(any())).thenReturn(scheduleMap);

        multiFreqWeeklyScheduleHandler.save(iamId, Utils.getCurrentDate(), null, null, new ScheduleSensitivity(0L, 0L), doseTimeList);

        Mockito.verify(scheduleMapRepository, Mockito.times(1)).findAllByIamId(eq(iamId));
        Mockito.verify(scheduleMapRepository, Mockito.times(1)).save(any());
        Mockito.verify(scheduleTimeMapRepository, Mockito.times(1)).saveAll(any());
    }

    @Test
    public void saveScheduleForFirstTimeForEmptyDoseTimeList() {
        Long iamId = 1L;
        List<String> doseTimeList = new ArrayList<>();
        ScheduleMap scheduleMap = new ScheduleMap();
        scheduleMap.setScheduleId(1L);
        when(scheduleMapRepository.findAllByIamId(eq(iamId))).thenReturn(new ArrayList<>());

        multiFreqWeeklyScheduleHandler.save(iamId, Utils.getCurrentDate(), null, null, new ScheduleSensitivity(0L, 0L), doseTimeList);

        Mockito.verify(scheduleMapRepository, Mockito.times(1)).findAllByIamId(eq(iamId));
        Mockito.verify(scheduleMapRepository, Mockito.times(0)).save(any());
        Mockito.verify(scheduleTimeMapRepository, Mockito.times(0)).saveAll(any());
    }

    @Test
    public void saveScheduleForAddingNewDoseTime() throws ParseException {
        Long iamId = 1L;
        List<String> doseTimeList = Arrays.asList("03:30:00", "07:30:00");
        List<ScheduleMap> scheduleMapList = new ArrayList<>();
        ScheduleMap scheduleMap = new ScheduleMap(1L, null, Utils.convertStringToDate("2022-11-13 00:00:00"), Utils.convertStringToDate("2022-11-13 00:00:00"), 0L, new ScheduleSensitivity(0L, 0L));
        scheduleMap.setId(1L);
        scheduleMapList.add(scheduleMap);
        List<ScheduleTimeMap> scheduleTimeMapList = new ArrayList<>();
        scheduleTimeMapList.add(new ScheduleTimeMap(1L, true, null, 1L, Time.valueOf("03:30:00")));

        when(scheduleMapRepository.findAllByIamId(eq(iamId))).thenReturn(scheduleMapList);
        when(scheduleTimeMapRepository.findAllByScheduleMapId(eq(1L))).thenReturn(scheduleTimeMapList);

        multiFreqWeeklyScheduleHandler.save(iamId, Utils.convertStringToDate("2022-11-13 00:00:00"), null, null, new ScheduleSensitivity(0L, 0L), doseTimeList);

        Mockito.verify(scheduleMapRepository, Mockito.times(1)).findAllByIamId(eq(iamId));
        Mockito.verify(scheduleMapRepository, Mockito.times(0)).save(any());
        Mockito.verify(scheduleTimeMapRepository, Mockito.times(1)).saveAll(any());
    }

    @Test
    public void saveScheduleForDeletingDoseTime() throws ParseException {
        Long iamId = 1L;
        List<String> doseTimeList = Arrays.asList("03:30:00");
        List<ScheduleMap> scheduleMapList = new ArrayList<>();
        ScheduleMap scheduleMap = new ScheduleMap(1L, null, Utils.convertStringToDate("2022-11-13 00:00:00"), Utils.convertStringToDate("2022-11-13 00:00:00"), 0L, new ScheduleSensitivity(0L, 0L));
        scheduleMap.setId(1L);
        scheduleMapList.add(scheduleMap);
        List<ScheduleTimeMap> scheduleTimeMapList = new ArrayList<>();
        scheduleTimeMapList.add(new ScheduleTimeMap(1L, true, null, 1L, Time.valueOf("03:30:00")));
        scheduleTimeMapList.add(new ScheduleTimeMap(1L, true, null, 1L, Time.valueOf("07:30:00")));

        when(scheduleMapRepository.findAllByIamId(eq(iamId))).thenReturn(scheduleMapList);
        when(scheduleTimeMapRepository.findAllByScheduleMapId(eq(1L))).thenReturn(scheduleTimeMapList);

        multiFreqWeeklyScheduleHandler.save(iamId, Utils.convertStringToDate("2022-11-13 00:00:00"), null, null, new ScheduleSensitivity(0L, 0L), doseTimeList);

        Mockito.verify(scheduleMapRepository, Mockito.times(1)).findAllByIamId(eq(iamId));
        Mockito.verify(scheduleMapRepository, Mockito.times(0)).save(any());
        Mockito.verify(scheduleTimeMapRepository, Mockito.times(1)).deleteAll(any());
    }

    @Test
    public void stopScheduleForFirstTimeAddFlow() throws ParseException {
        Long iamId = 1L;
        List<ScheduleMap> scheduleMapList = new ArrayList<>();
        ScheduleMap scheduleMap = new ScheduleMap(1L, null, Utils.convertStringToDate("2022-11-13 00:00:00"), Utils.convertStringToDate("2022-11-13 00:00:00"), 0L, new ScheduleSensitivity(0L, 0L));
        scheduleMap.setId(1L);
        scheduleMapList.add(scheduleMap);
        when(scheduleMapRepository.findAllByIamId(eq(iamId))).thenReturn(scheduleMapList);

        multiFreqWeeklyScheduleHandler.stopSchedule(iamId, Utils.convertStringToDate("2022-11-18 00:00:00"));

        Mockito.verify(scheduleMapRepository, Mockito.times(1)).findAllByIamId(eq(iamId));
        Mockito.verify(scheduleMapRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void stopScheduleForEditFlow() throws ParseException {
        Long iamId = 1L;
        List<ScheduleMap> scheduleMapList = new ArrayList<>();
        ScheduleMap scheduleMapOld = new ScheduleMap(1L, null, Utils.convertStringToDate("2022-11-13 22:30:00"), Utils.convertStringToDate("2022-11-13 18:30:00"), 0L, new ScheduleSensitivity(0L, 0L));
        scheduleMapOld.setId(1L);
        scheduleMapOld.setEndDate(Utils.convertStringToDate("2022-11-16 00:00:00"));
        scheduleMapList.add(scheduleMapOld);
        ScheduleMap scheduleMapNew = new ScheduleMap(1L, null, Utils.convertStringToDate("2022-11-16 00:00:00"), Utils.convertStringToDate("2022-11-16 00:00:00"), 0L, new ScheduleSensitivity(0L, 0L));
        scheduleMapNew.setId(2L);
        scheduleMapList.add(scheduleMapNew);

        multiFreqWeeklyScheduleHandler.stopSchedule(iamId, Utils.convertStringToDate("2022-11-16 00:00:00"));

        Mockito.verify(scheduleMapRepository, Mockito.times(1)).findAllByIamId(eq(iamId));
        Mockito.verify(scheduleMapRepository, Mockito.times(0)).save(any());
    }
}

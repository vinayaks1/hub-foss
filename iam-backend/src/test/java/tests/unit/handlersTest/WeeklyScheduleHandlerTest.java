package tests.unit.handlersTest;

import com.everwell.iam.enums.AdherenceCodeEnum;
import com.everwell.iam.exceptions.ConflictException;
import com.everwell.iam.exceptions.NotFoundException;
import com.everwell.iam.handlers.impl.DailyScheduleHandler;
import com.everwell.iam.handlers.impl.WeeklyScheduleHandler;
import com.everwell.iam.models.db.*;
import com.everwell.iam.models.dto.ScheduleSensitivity;
import com.everwell.iam.repositories.ScheduleMapRepository;
import com.everwell.iam.repositories.ScheduleRepository;
import com.everwell.iam.utils.Utils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import tests.BaseTestNoSpring;

import java.security.InvalidParameterException;
import java.text.ParseException;
import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class WeeklyScheduleHandlerTest extends BaseTestNoSpring {

    @Spy
    @InjectMocks
    WeeklyScheduleHandler weeklyScheduleHandler;

    @Mock
    ScheduleMapRepository scheduleMapRepository;

    @Mock
    ScheduleRepository scheduleRepository;

    //to test the hasScheduleMapping() checks
    @Spy
    DailyScheduleHandler dailyScheduleHandler;

    @Before
    public void init() {
        weeklyScheduleHandler.setScheduleTypeId(2L);
        dailyScheduleHandler.setScheduleTypeId(1L);
    }

    @Test
    public void test_has_schedule_mapping() throws NotFoundException {
        boolean hasMapping = weeklyScheduleHandler.hasScheduleMapping();
        assertEquals(true, hasMapping);
    }

    @Test
    public void test_last_closed_schedule() throws ParseException {
        when(scheduleMapRepository.findAllByIamId(any())).thenReturn(scheduleMaps_inactive());
        ScheduleMap scheduleMap = weeklyScheduleHandler.getLastCreatedSchedule(1L);
        assertEquals(6L, scheduleMap.getId().longValue());
    }

    @Test(expected = NotFoundException.class)
    public void test_last_closed_schedule_exception() {
        when(scheduleMapRepository.findAllByIamId(any())).thenReturn(new ArrayList<>());
        weeklyScheduleHandler.getLastCreatedSchedule(1L);
    }

    @Test
    public void should_take_dose_true_testCase1() throws ParseException {
        Date calledDate = Utils.convertStringToDate("2020-01-01 19:30:00");
        boolean actualValue = weeklyScheduleHandler.shouldTakeDose(scheduleMap_active(), idScheduleMap(), calledDate);
        assertTrue(actualValue);
    }

    @Test
    public void should_take_dose_false_testCase1() throws ParseException {
        Date calledDate = Utils.convertStringToDate("2020-01-02 19:30:00");
        boolean actualValue = weeklyScheduleHandler.shouldTakeDose(scheduleMap_active(), idScheduleMap(), calledDate);
        assertFalse(actualValue);
    }

    @Test
    public void should_take_dose_by_iamId() throws ParseException {
        ScheduleMap scheduleMap = scheduleMap_active().get(0);
        when(scheduleMapRepository.findActiveScheduleMapForDate(any(), any())).thenReturn(scheduleMap);
        when(scheduleRepository.findById(any())).thenReturn(Optional.of(idScheduleMap().get(scheduleMap.getScheduleId())));
        boolean actualValue = weeklyScheduleHandler.shouldTakeDose(1L, Utils.convertStringToDate("2020-01-01 19:30:00"));
        assertTrue(actualValue);
    }

    @Test
    public void compute_adherenceCode_for_day_testCase1_received() throws ParseException {
        Date calledDate = Utils.convertStringToDate("2020-01-01 19:30:00");
        when(scheduleMapRepository.findAllByIamId(any())).thenReturn(scheduleMap_active());
        when(scheduleRepository.findAllById(any())).thenReturn(schedules());
        char adherenceCode = weeklyScheduleHandler.computeAdherenceCodeForDay(1L, calledDate);
        assertEquals(AdherenceCodeEnum.RECEIVED.getCode(), adherenceCode);
    }

    @Test
    public void get_date_to_attribute_testCase1_scheduledDay() throws ParseException {
        Date calledDate = Utils.convertStringToDate("2020-01-01 18:30:00");
        Date expectedDate = Utils.convertStringToDate("2020-01-01 18:30:00");
        Date attributeDay = weeklyScheduleHandler.getDateToAttribute(scheduleMap_with_sensitivity(), idScheduleMap(),calledDate);
        assertEquals(expectedDate, attributeDay);
    }

    @Test
    public void compute_adherenceCode_for_day_testCase1_received_unscheduled() throws ParseException {
        Date calledDate = Utils.convertStringToDate("2020-01-02 19:30:00");
        when(scheduleMapRepository.findAllByIamId(any())).thenReturn(scheduleMap_with_sensitivity());
        when(scheduleRepository.findAllById(any())).thenReturn(schedules());
        char adherenceCode = weeklyScheduleHandler.computeAdherenceCodeForDay(1L, calledDate);
        assertEquals(AdherenceCodeEnum.RECEIVED_UNSCHEDULED.getCode(), adherenceCode);
    }

    @Test
    public void get_date_to_attribute_testCase1_unscheduledDay() throws ParseException {
        Date calledDate = Utils.convertStringToDate("2020-01-02 19:30:00");
        Date expectedDate = Utils.convertStringToDate("2020-01-02 18:29:00");
        Date attributeDay = weeklyScheduleHandler.getDateToAttribute(scheduleMap_with_sensitivity(), idScheduleMap(),calledDate);
        assertEquals(expectedDate, attributeDay);
    }

    @Test
    public void scheduleValue_correctness_testCase1() {
        String scheduleString = "FMUHW";
        assertEquals("1011101", weeklyScheduleHandler.getScheduleValue(scheduleString));
    }

    @Test
    public void scheduleString_correctness_testCase() {
        String scheduleValue = "1001000";
        assertEquals("MH", weeklyScheduleHandler.getScheduleString(scheduleValue));
    }

    @Test
    public void find_all_active_schedules_mock() {
        weeklyScheduleHandler.findAllIamWithActiveSchedule(1L, "1L");
        Mockito.verify(scheduleMapRepository, Mockito.times(1)).findAllIamIdsWithActiveSchedulesByClientIdAndEntityId(any(), any());
    }

    @Test(expected = InvalidParameterException.class)
    public void validate_schedule_sensitivity_overlap_null_schedule() {
        ScheduleSensitivity scheduleSensitivity = new ScheduleSensitivity(72L, 72L);
        weeklyScheduleHandler.validateScheduleAndSensitivityOverLap("H", scheduleSensitivity, new ArrayList<>());
    }

    @Test(expected = InvalidParameterException.class)
    public void validate_schedule_sensitivity_overlap_left_sensitivity_null() {
        ScheduleSensitivity scheduleSensitivity = new ScheduleSensitivity(null, 72L);

        weeklyScheduleHandler.validateScheduleAndSensitivityOverLap("H", scheduleSensitivity, new ArrayList<>());
    }

    @Test(expected = InvalidParameterException.class)
    public void validate_schedule_sensitivity_overlap_right_sensitivity_null() {
        ScheduleSensitivity scheduleSensitivity = new ScheduleSensitivity(72L, null);
        weeklyScheduleHandler.validateScheduleAndSensitivityOverLap("H", scheduleSensitivity, new ArrayList<>());
    }

    @Test(expected = InvalidParameterException.class)
    public void validate_schedule_sensitivity_overlap_left_sensitivity_less_than_zero() {
        ScheduleSensitivity scheduleSensitivity = new ScheduleSensitivity(-1L, 72L);
        weeklyScheduleHandler.validateScheduleAndSensitivityOverLap("H", scheduleSensitivity, new ArrayList<>());
    }

    @Test(expected = InvalidParameterException.class)
    public void validate_schedule_sensitivity_overlap_right_sensitivity_less_than_zero() {
        ScheduleSensitivity scheduleSensitivity = new ScheduleSensitivity(72L, -1L);
        weeklyScheduleHandler.validateScheduleAndSensitivityOverLap("H", scheduleSensitivity, new ArrayList<>());
    }

    @Test
    public void validate_schedule_sensitivity_no_overlap() {
        ScheduleSensitivity scheduleSensitivity = new ScheduleSensitivity(72L, 72L);
        when(scheduleRepository.findByScheduleTypeIdAndValue(any(), anyString())).thenReturn(
            new Schedule(4L, "0001000", 2L)
        );
        //no exceptions, successfully validated
        weeklyScheduleHandler.validateScheduleAndSensitivityOverLap("H", scheduleSensitivity, new ArrayList<>());
    }

    @Test(expected = InvalidParameterException.class)
    public void validate_schedule_sensitivity_with_overlap() {
        ScheduleSensitivity scheduleSensitivity = new ScheduleSensitivity(100L, 72L);
        when(scheduleRepository.findByScheduleTypeIdAndValue(any(), anyString())).thenReturn(
            new Schedule(4L, "0001000", 2L)
        );
        weeklyScheduleHandler.validateScheduleAndSensitivityOverLap("H", scheduleSensitivity, new ArrayList<>());
    }

    @Test
    public void save() {
        ScheduleSensitivity scheduleSensitivity = new ScheduleSensitivity(100L, 72L);
        when(scheduleRepository.findByScheduleTypeIdAndValue(any(), anyString())).thenReturn(
            new Schedule(4L, "0001000", 2L)
        );
        //successful completion, code working as expected
        weeklyScheduleHandler.save(1L, Utils.getCurrentDate(), "H", 1L, scheduleSensitivity);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void update_start_date_more_than_one() throws ParseException {
        when(scheduleMapRepository.findAllByIamId(any())).thenReturn(scheduleMaps_inactive());
        weeklyScheduleHandler.updateStartDate(1L, Utils.convertStringToDate("2020-01-01 18:30:00"), 1440L);
    }

    @Test
    public void update_start_date() throws ParseException {
        when(scheduleMapRepository.findAllByIamId(any())).thenReturn(scheduleMap_active());
        weeklyScheduleHandler.updateStartDate(1L, Utils.convertStringToDate("2020-01-01 18:30:00"), 1440L);
        ArgumentCaptor<ScheduleMap> captor = ArgumentCaptor.forClass(ScheduleMap.class);
        verify(scheduleMapRepository).save(captor.capture());
        ScheduleMap argument = captor.getValue();
        Mockito.verify(scheduleMapRepository, Mockito.times(1)).save(any());
        Assert.assertEquals(argument.getStartDate(), Utils.convertStringToDate("2020-01-01 18:30:00"));
    }

    @Test
    public void update_end_date_and_active_status() throws ParseException {
        weeklyScheduleHandler.updateEndDateAndActiveStatus(scheduleMap_single_inactive(), Utils.convertStringToDate("2020-01-20 18:29:00"), true);
        ArgumentCaptor<ScheduleMap> captor = ArgumentCaptor.forClass(ScheduleMap.class);
        verify(scheduleMapRepository).save(captor.capture());
        ScheduleMap argument = captor.getValue();
        Mockito.verify(scheduleMapRepository, Mockito.times(1)).save(any());
        Assert.assertEquals(argument.getEndDate(), Utils.convertStringToDate("2020-01-20 18:29:00"));
        Assert.assertEquals(argument.isActive(), true);
    }

    @Test(expected = ConflictException.class)
    public void stop_schedule_conflict() throws ParseException {
        doReturn(null).when(weeklyScheduleHandler).getActiveScheduleMapForIam(any());
        weeklyScheduleHandler.stopSchedule(1L, Utils.convertStringToDate("2020-01-20 18:29:00"));
    }

    @Test
    public void stop_schedule() throws ParseException {
        doReturn(scheduleMap_single_inactive()).when(weeklyScheduleHandler).getActiveScheduleMapForIam(any());
        weeklyScheduleHandler.stopSchedule(1L, Utils.convertStringToDate("2020-01-20 18:29:00"));
        Mockito.verify(weeklyScheduleHandler, Mockito.times(1))
            .updateEndDateAndActiveStatus(any(), eq(Utils.convertStringToDate("2020-01-20 18:29:00")), eq(false));
    }

    @Test
    public void reopen_schedule() throws ParseException {
        Registration registration = new Registration(1L, 1L, Utils.convertStringToDate("2019-08-31 18:30:00"), Utils.convertStringToDate("2019-09-06 18:30:00"), "91929399495", "666666", Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"), Utils.convertStringToDate("2019-01-06 12:23:26"));
        doReturn(scheduleMap_single_inactive()).when(weeklyScheduleHandler).getLastCreatedSchedule(anyLong());
        weeklyScheduleHandler.reopenSchedule(registration);
        Mockito.verify(weeklyScheduleHandler, Mockito.times(1))
            .updateEndDateAndActiveStatus(any(), eq(null), eq(true));
    }

    @Test
    public void get_date_to_attribute() throws ParseException {
        doReturn(Utils.convertStringToDate("2020-01-21 14:30:00")).when(weeklyScheduleHandler).getDateToAttribute(any(), any(), any());
        Date date = weeklyScheduleHandler.getDateToAttribute(1L, Utils.convertStringToDate("2020-01-21 14:30:00"));
        assertEquals(date, Utils.convertStringToDate("2020-01-21 14:30:00"));
    }

    @Test
    public void get_active_schedule_string_null() throws ParseException {
        doReturn(null).when(weeklyScheduleHandler).getLastCreatedSchedule(anyLong());
        String s = weeklyScheduleHandler.getActiveScheduleString(anyLong());
        assertEquals(s, "");
    }

    @Test
    public void get_active_schedule_string() throws ParseException {
        doReturn(scheduleMap_single_inactive()).when(weeklyScheduleHandler).getLastCreatedSchedule(anyLong());
        when(scheduleRepository.findById(anyLong())).thenReturn(Optional.of(new Schedule(4L, "0001000", 2L)));
        assertEquals(weeklyScheduleHandler.getActiveScheduleString(anyLong()), "H");
        when(scheduleRepository.findById(anyLong())).thenReturn(Optional.of(new Schedule(1L, "1000000", 2L)));
        assertEquals(weeklyScheduleHandler.getActiveScheduleString(anyLong()), "M");
    }

    @Test
    public void get_active_schedule_map_for_iam_daily_schedule_daily() {
        Assert.assertNull(dailyScheduleHandler.getActiveScheduleMapForIam(1L));
    }

    @Test
    public void find_all_iam_with_active_schedule_daily() {
        Assert.assertEquals(new ArrayList<>(),
            dailyScheduleHandler.findAllIamWithActiveSchedule(1L, "1")
        );
    }

    @Test
    public void get_last_created_schedule_daily() {
        Assert.assertNull(dailyScheduleHandler.getLastCreatedSchedule(1L));
    }

    @Test
    public void validate_schedule_and_sensitivity_overLap_daily() {
        dailyScheduleHandler.validateScheduleAndSensitivityOverLap(null, null, new ArrayList<>());
        Mockito.verify(dailyScheduleHandler, Mockito.times(0)).findByScheduleValue(null);
    }

    @Test
    public void save_daily() {
        dailyScheduleHandler.save(1L, null, null, null, null);
        Mockito.verify(scheduleMapRepository, Mockito.times(0)).save(any());
    }

    @Test
    public void update_start_date_daily() {
        dailyScheduleHandler.updateStartDate(1L, null, null);
        Mockito.verify(scheduleMapRepository, Mockito.times(0)).save(any());
    }

    @Test
    public void update_endDate_and_active_status_daily() {
        dailyScheduleHandler.updateEndDateAndActiveStatus(null, null, false);
        Mockito.verify(scheduleMapRepository, Mockito.times(0)).save(any());
    }

    @Test
    public void stop_schedule_daily() {
        dailyScheduleHandler.stopSchedule(1L, null);
        Mockito.verify(scheduleMapRepository, Mockito.times(0)).save(any());
    }

    @Test
    public void reopen_schedule_daily() {
        dailyScheduleHandler.reopenSchedule(null);
        Mockito.verify(scheduleMapRepository, Mockito.times(0)).save(any());
    }

    @Test
    public void find_by_id() {
        when(scheduleRepository.findById(eq(1L))).thenReturn(Optional.of(idScheduleMap().get(1L)));
        Assert.assertEquals("1000000", weeklyScheduleHandler.findById(1L).getValue());
    }

    @Test
    public void find_by_id_null() {
        when(scheduleRepository.findById(eq(1L))).thenReturn(Optional.empty());
        Assert.assertNull(weeklyScheduleHandler.findById(1L));
    }

    @Test
    public void should_fag_TFN_repeat_for_NND() {
        Assert.assertFalse(weeklyScheduleHandler.shouldFlagTFNRepeatForNND());
    }

    @Test
    public void is_scheduled_day_true() throws ParseException {
        Assert.assertTrue(
            weeklyScheduleHandler.isScheduledDay(
                new ScheduleMap(1L,true, 1L, Utils.convertStringToDate("2021-01-01 18:30:00"), 4L, Utils.convertStringToDate("2021-01-01 18:30:00"), 0L, new ScheduleSensitivity(0L, 0L)),
                Utils.convertStringToDate("2021-01-15 18:30:00")
            )
        );
    }

    @Test
    public void is_scheduled_day_false() throws ParseException {
        Assert.assertFalse(
            weeklyScheduleHandler.isScheduledDay(
                new ScheduleMap(1L,true, 1L, Utils.convertStringToDate("2021-01-01 18:30:00"), 4L, Utils.convertStringToDate("2021-01-01 18:30:00"), 0L, new ScheduleSensitivity(0L, 0L)),
                Utils.convertStringToDate("2021-01-16 18:30:00")
            )
        );
    }

    @Test
    public void generate_adherence_string() throws ParseException {
        HashMap<Integer, AdherenceStringLog> map = new HashMap<>();
        map.put(0, new AdherenceStringLog(1L, '4', Utils.convertStringToDate("2021-01-01 18:30:00")));
        when(scheduleMapRepository.findAllByIamId(any())).thenReturn(scheduleMaps_active());
        String adherenceString = weeklyScheduleHandler.generateAdherenceString(map, Utils.convertStringToDate("2021-01-01 18:30:00"), Utils.convertStringToDate("2021-01-20 18:29:00"), 1L, new HashMap<>());
        assertEquals("4777777677777767777", adherenceString);
    }

    @Test
    public void generate_adherence_string_2() throws ParseException {
        HashMap<Integer, AdherenceStringLog> map = new HashMap<>();
        map.put(0, new AdherenceStringLog(1L, '4', Utils.convertStringToDate("2021-01-01 18:30:00")));
        when(scheduleMapRepository.findAllByIamId(any())).thenReturn(scheduleMaps_inactive());
        String adherenceString = weeklyScheduleHandler.generateAdherenceString(map, Utils.convertStringToDate("2020-01-01 18:30:00"), Utils.convertStringToDate("2020-01-19 18:30:00"), 1L, new HashMap<>());
        assertEquals("477777767777776777", adherenceString);
    }

    @Test(expected = InvalidParameterException.class)
    public void get_schedule_week_day_value_map() {
        weeklyScheduleHandler.getScheduleWeekDayValueMap("X");
    }

    @Test(expected = InvalidParameterException.class)
    public void get_schedule_value() {
        weeklyScheduleHandler.getScheduleValue("X");
    }

    @Test
    public void should_take_dose_false() {
        when(scheduleMapRepository.findActiveScheduleMapForDate(any(), any())).thenReturn(null);
        Assert.assertFalse(weeklyScheduleHandler.shouldTakeDose(1L, null));
    }

    @Test
    public void should_take_dose_2_false() throws ParseException {
        Assert.assertFalse(weeklyScheduleHandler.shouldTakeDose(scheduleMaps_inactive(), null, Utils.convertStringToDate("2021-01-01 18:30:00")));
    }

    @Test
    public void compute_adherence_code_for_day_blank() throws ParseException {
        Assert.assertEquals(
            AdherenceCodeEnum.BLANK.getCode(),
            weeklyScheduleHandler.computeAdherenceCodeForDay(scheduleMaps_inactive(), null, Utils.convertStringToDate("2021-01-01 18:30:00"))
        );
    }

    private List<ScheduleMap> scheduleMap_active() throws ParseException {
        List<ScheduleMap> scheduleMaps = new ArrayList<>();
        scheduleMaps.add(new ScheduleMap(1L,true, 1L, Utils.convertStringToDate("2020-01-01 18:30:00"), 4L, Utils.convertStringToDate("2020-01-01 18:30:00"), 0L, new ScheduleSensitivity(0L, 0L)));
        return  scheduleMaps;
    }

    private ScheduleMap scheduleMap_single_inactive() throws ParseException {
        return new ScheduleMap(1L,true, 1L, Utils.convertStringToDate("2020-01-01 18:30:00"), 4L, Utils.convertStringToDate("2020-01-01 18:30:00"), Utils.convertStringToDate("2020-01-25 18:29:00"), 0L, new ScheduleSensitivity(0L, 0L));
    }

    private List<ScheduleMap> scheduleMap_with_sensitivity() throws ParseException {
        List<ScheduleMap> scheduleMaps = new ArrayList<>();
        scheduleMaps.add(new ScheduleMap(1L,true, 1L, Utils.convertStringToDate("2020-01-01 18:30:00"), 4L, Utils.convertStringToDate("2020-01-01 18:30:00"), 0L, new ScheduleSensitivity(24L, 24L)));
        return  scheduleMaps;
    }

    private List<Schedule> schedules() {
        List<Schedule> schedules = new ArrayList<>();
        schedules.add(new Schedule(1L, "1000000", 2L));
        schedules.add(new Schedule(2L, "0100000", 2L));
        schedules.add(new Schedule(3L, "0010000", 2L));
        schedules.add(new Schedule(4L, "0001000", 2L));
        schedules.add(new Schedule(5L, "0000100", 2L));
        schedules.add(new Schedule(6L, "0000010", 2L));
        schedules.add(new Schedule(7L, "0000001", 2L));
        return schedules;
    }

    private TreeMap<Long, Schedule> idScheduleMap() {
        TreeMap<Long, Schedule> map = new TreeMap<>();
        map.put(1L, new Schedule(1L, "1000000", 2L));
        map.put(2L, new Schedule(2L, "0100000", 2L));
        map.put(3L, new Schedule(3L, "0010000", 2L));
        map.put(4L, new Schedule(4L, "0001000", 2L));
        map.put(5L, new Schedule(5L, "0000100", 2L));
        map.put(6L, new Schedule(6L, "0000010", 2L));
        map.put(7L, new Schedule(7L, "0000001", 2L));
        return  map;
    }

    private List<ScheduleMap> scheduleMaps_inactive() throws ParseException {
        List<ScheduleMap> scheduleMaps = new ArrayList<>();
        scheduleMaps.add(new ScheduleMap(1L,false, 1L, Utils.convertStringToDate("2020-01-01 18:30:00"), 4L, Utils.convertStringToDate("2020-01-01 18:30:00"), Utils.convertStringToDate("2020-01-19 18:30:00"), 0L, new ScheduleSensitivity(0L, 0L)));
        scheduleMaps.add(new ScheduleMap(2L,false, 1L, Utils.convertStringToDate("2020-01-19 18:30:00"), 1L, Utils.convertStringToDate("2020-01-19 18:30:00"), Utils.convertStringToDate("2020-01-23 18:30:00"), 0L, new ScheduleSensitivity(0L, 0L)));
        scheduleMaps.add(new ScheduleMap(3L,false, 1L, Utils.convertStringToDate("2020-01-23 18:30:00"), 5L, Utils.convertStringToDate("2020-01-23 18:30:00"), Utils.convertStringToDate("2020-01-25 18:30:00"), 0L, new ScheduleSensitivity(0L, 0L)));
        scheduleMaps.add(new ScheduleMap(4L,false, 1L, Utils.convertStringToDate("2020-01-25 18:30:00"), 7L, Utils.convertStringToDate("2020-01-25 18:30:00"), Utils.convertStringToDate("2020-01-27 18:30:00"), 0L, new ScheduleSensitivity(0L, 0L)));
        scheduleMaps.add(new ScheduleMap(5L,false, 1L, Utils.convertStringToDate("2020-01-27 18:30:00"), 2L, Utils.convertStringToDate("2020-01-27 18:30:00"), Utils.convertStringToDate("2020-01-29 18:30:00"), 0L, new ScheduleSensitivity(0L, 0L)));
        scheduleMaps.add(new ScheduleMap(6L,false, 1L, Utils.convertStringToDate("2020-01-29 18:30:00"), 4L, Utils.convertStringToDate("2020-01-29 18:30:00"), Utils.convertStringToDate("2020-01-29 18:30:00"), 0L, new ScheduleSensitivity(0L, 0L)));
        return  scheduleMaps;
    }

    private List<ScheduleMap> scheduleMaps_active() throws ParseException {
        List<ScheduleMap> scheduleMaps = new ArrayList<>();
        scheduleMaps.add(new ScheduleMap(6L,false, 1L, Utils.convertStringToDate("2021-01-01 18:30:00"), 4L, Utils.convertStringToDate("2021-01-01 18:30:00"), 0L, new ScheduleSensitivity(0L, 0L)));
        return  scheduleMaps;
    }

}

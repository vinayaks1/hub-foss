package tests.unit.repository;

import com.everwell.iam.models.db.AccessMapping;
import com.everwell.iam.repositories.AccessMappingRepository;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import tests.BaseTest;

public class AccessMappingRepositoryTest extends BaseTest {

    @Autowired
    private AccessMappingRepository mappingRepository;

    private static AccessMapping SAVED_MAPPING;

    @Before
    public void setUp() {
        AccessMapping access= new AccessMapping(1L,1L,"123");
        mappingRepository.save(access);
        SAVED_MAPPING = access;
    }

    @After
    public void cleanUp() {
        AccessMapping mapping = mappingRepository.findById(SAVED_MAPPING.getId()).get();
        mappingRepository.delete(mapping);
    }

    @Test
    public void createdMappingIsActive() {
        Assert.assertTrue(SAVED_MAPPING.isActive());
    }

}

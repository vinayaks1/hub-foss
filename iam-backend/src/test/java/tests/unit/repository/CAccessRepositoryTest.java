package tests.unit.repository;

import com.everwell.iam.models.db.CAccess;
import com.everwell.iam.repositories.CAccessRepository;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import tests.BaseTest;


public class CAccessRepositoryTest extends BaseTest {

    @Autowired
    private CAccessRepository accessRepository;

    private static CAccess SAVED_ACCESS;

    @Before
    public void setUp() {
        CAccess access= new CAccess("Bob", "test-passwd", "KhnQ8EZv8O9e5YhBSud7Ttc7O", 1551202106L);
        accessRepository.save(access);
        SAVED_ACCESS = access;
    }

    @After
    public void cleanUp() {
        accessRepository.delete(SAVED_ACCESS);
    }

    @Test
    public void testFindByName(){
        /*Test data retrieval*/
        CAccess access = accessRepository.findByName("Bob");
        Assert.assertNotNull(access);
        Assert.assertEquals(access.getId(), SAVED_ACCESS.getId());
        Assert.assertNotNull(access.getId());
    }

    @Test
    public void createdAndUpdatedDateIsSet() {
        Assert.assertNotNull(SAVED_ACCESS.getCreatedDate());
        Assert.assertNotNull(SAVED_ACCESS.getUpdatedDate());
    }
}

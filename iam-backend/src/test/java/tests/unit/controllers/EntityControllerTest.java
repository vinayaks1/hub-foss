package tests.unit.controllers;

import com.everwell.iam.controllers.EntityController;
import com.everwell.iam.exceptions.CustomExceptionHandler;
import com.everwell.iam.models.db.AccessMapping;
import com.everwell.iam.models.db.Registration;
import com.everwell.iam.models.dto.eventstreaming.analytics.RegisterEntityTrackingDto;
import com.everwell.iam.models.http.requests.EntityRequest;
import com.everwell.iam.models.http.requests.MERMEntityRequest;
import com.everwell.iam.models.http.responses.EntityResponse;
import com.everwell.iam.services.AdherenceService;
import com.everwell.iam.services.EntityService;
import com.everwell.iam.utils.CacheUtils;
import com.everwell.iam.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import tests.BaseTestNoSpring;

import java.util.*;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class EntityControllerTest extends BaseTestNoSpring {

    private MockMvc mockMvc;

    @Mock
    private EntityService entityService;

    @Mock
    private AdherenceService adherenceService;

    @Mock
    RedisTemplate<String, Object> redisTemplate;

    CacheUtils cacheUtils;

    @InjectMocks
    private EntityController entityController;

    //Note: Mocking alone makes the applicationEventPublisher.publishEvent(), do nothing
    @Mock
    private ApplicationEventPublisher applicationEventPublisher;

    private static Long clientId = 4L;
    private static String entityId = "1234";
    private static String uniqueIdentifier = "5678";
    private static Long scheduleTypeId = 1L;
    private static Long iamId = 1L;
    private static String imei = "123451234512345";

    private static String MESSAGE_ACTIVE_ENTITY_ALREADY_EXISTS = "Active entity already exists";
    private static String MESSAGE_ACTIVE_SCHEDULE_EXISTS = "Active schedule exits";
    private static String MESSAGE_ENTITY_NOT_FOUND = "entity was not found";
    private static String MESSAGE_ACTIVE_SCHEDULE_DOES_NOT_EXIST = "Active schedule does not exists";
    private static String MESSAGE_ENTITY_SUCCCESSFULLY_UPDATED = "entity successfully updated";
    private static String MESSAGE_RE_OPENED_SUCCESSFULLY = "Re-opened Successfully!";

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(entityController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
        cacheUtils = new CacheUtils(redisTemplate);
        when(redisTemplate.delete(anyString())).thenReturn(true);
    }

    @Test
    public void testGetEntitySuccess() throws Exception {
        String uri = "/v1/entity";
        String uniqueIdentifier = "ABCD";
        Long adtechId = 2L;
        List<Long> iamIds = Arrays.asList(1L, 11L);
        Long clientId = 4L;
        List<String> entityIds = Arrays.asList("618");

        when(entityService.getEntityByIamIds(iamIds, clientId)).thenReturn(entityIds);
        when(adherenceService.getIamIdsByAdherenceTechAndUniqueIdentifier(eq(uniqueIdentifier), any())).thenReturn(iamIds);

        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                .param("adherenceType", adtechId.toString())
                .param("uniqueIdentifier", uniqueIdentifier)
                .header("X-IAM-Client-Id", clientId.toString())
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.entityId").value(entityIds.get(0)));
    }

    @Test
    public void testGetEntityBadRequestOnEmptyUniqueIdentfier() throws Exception {
        String uri = "/v1/entity";
        String uniqueIdentifier = "";
        Long adtechId = 2L;
        List<Long> iamIds = Arrays.asList(1L, 11L);
        Long clientId = 4L;
        List<String> entityIds = Arrays.asList("618");

        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                .param("adherenceType", adtechId.toString())
                .param("uniqueIdentifier", uniqueIdentifier)
                .header("X-IAM-Client-Id", clientId.toString())
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("invalid unique identifier"));
    }

    @Test
    public void testGetEntityBadRequestOnNoAdherenceType() throws Exception {
        String uri = "/v1/entity";
        String uniqueIdentifier = "ABCD";
        Long clientId = 4L;

        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                .param("uniqueIdentifier", uniqueIdentifier)
                .header("X-IAM-Client-Id", clientId.toString())
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("invalid adherence type"));
    }

    @Test
    public void testGetEntityNotFoundOnNoEntityFound() throws Exception {
        String uri = "/v1/entity";
        String uniqueIdentifier = "ABCD";
        List<Long> iamIds = Arrays.asList(1L, 11L);
        Long clientId = 4L;
        Long adtechId = 2L;

        when(entityService.getEntityByIamIds(iamIds, clientId)).thenReturn(new ArrayList<>());
        when(adherenceService.getIamIdsByAdherenceTechAndUniqueIdentifier(eq(uniqueIdentifier), any())).thenReturn(iamIds);

        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                .param("uniqueIdentifier", uniqueIdentifier)
                .param("adherenceType", adtechId.toString())
                .header("X-IAM-Client-Id", clientId.toString())
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("entity was not found"));
    }

    @Test
    public void testGetEntityNotFoundOnNoIamIdsFound() throws Exception {
        String uri = "/v1/entity";
        String uniqueIdentifier = "ABCD";
        List<Long> iamIds = Arrays.asList(1L, 11L);
        Long clientId = 4L;
        Long adtechId = 2L;
        List<String> entityIds = Arrays.asList("618");

        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                .param("uniqueIdentifier", uniqueIdentifier)
                .param("adherenceType", adtechId.toString())
                .header("X-IAM-Client-Id", clientId.toString())
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("entity was not found"));
    }

    @Test
    public void testGetEntityConflictOnManyEntityFound() throws Exception {
        String uri = "/v1/entity";
        String uniqueIdentifier = "ABCD";
        List<Long> iamIds = Arrays.asList(1L, 11L);
        Long clientId = 4L;
        Long adtechId = 2L;
        List<String> entityIds = Arrays.asList("618", "619");

        when(entityService.getEntityByIamIds(iamIds, clientId)).thenReturn(entityIds);
        when(adherenceService.getIamIdsByAdherenceTechAndUniqueIdentifier(eq(uniqueIdentifier), any())).thenReturn(iamIds);

        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                .param("uniqueIdentifier", uniqueIdentifier)
                .param("adherenceType", adtechId.toString())
                .header("X-IAM-Client-Id", clientId.toString())
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isConflict())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("more than one active entity is mapped"));
    }

    @Test
    public void testRegisterEntityAlreadyExists() throws Exception
    {
        String uri = "/v1/entity";

        MERMEntityRequest request = new MERMEntityRequest();
        request.setEntityId(entityId);
        request.setUniqueIdentifier(uniqueIdentifier);
        request.setStartDate(Utils.getFormattedDate(new Date()));
        request.setImei(imei);

        when(entityService.validateActiveIamMappingByEntityId(clientId, entityId)).thenReturn(true);

        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .content(Utils.asJsonString(request))
                .contentType(MediaType.APPLICATION_JSON)
                .header("X-IAM-Client-Id", clientId.toString()))
                .andExpect(status().isConflict())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_ACTIVE_ENTITY_ALREADY_EXISTS));
    }

    @Test
    public void testRegisterEntityActiveScheduleExists() throws Exception
    {
        String uri = "/v1/entity";

        MERMEntityRequest request = new MERMEntityRequest();
        request.setEntityId(entityId);
        request.setUniqueIdentifier(uniqueIdentifier);
        request.setStartDate(Utils.getFormattedDate(new Date()));
        request.setImei(imei);

        when(entityService.validateActiveIamMappingByEntityId(clientId, entityId)).thenReturn(false);

        when(adherenceService.hasScheduleMappings(scheduleTypeId)).thenReturn(true);
        when(adherenceService.activeScheduleMappingExistsForEntityIdAndClientId(clientId, entityId, scheduleTypeId)).thenReturn(true);

        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .content(Utils.asJsonString(request))
                .contentType(MediaType.APPLICATION_JSON)
                .header("X-IAM-Client-Id", clientId.toString()))
                .andExpect(status().isConflict())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_ACTIVE_SCHEDULE_EXISTS));
    }

    @Test
    public void testRegisterEntityDoseTimeNotSupported() throws Exception
    {
        String uri = "/v1/entity";

        MERMEntityRequest request = new MERMEntityRequest();
        request.setEntityId(entityId);
        request.setUniqueIdentifier(uniqueIdentifier);
        request.setStartDate(Utils.getFormattedDate(new Date()));
        request.setImei(imei);
        request.setDoseTimeList(Collections.singletonList("03:30:00"));

        when(entityService.validateActiveIamMappingByEntityId(clientId, entityId)).thenReturn(false);

        when(adherenceService.hasScheduleMappings(scheduleTypeId)).thenReturn(false);

        when(adherenceService.isDoseTimeSupportedForScheduleType(any())).thenReturn(false);


        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                        .content(Utils.asJsonString(request))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-IAM-Client-Id", clientId.toString()))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testRegisterEntityDoseTimeRequired() throws Exception
    {
        String uri = "/v1/entity";

        MERMEntityRequest request = new MERMEntityRequest();
        request.setEntityId(entityId);
        request.setUniqueIdentifier(uniqueIdentifier);
        request.setStartDate(Utils.getFormattedDate(new Date()));
        request.setImei(imei);

        when(entityService.validateActiveIamMappingByEntityId(clientId, entityId)).thenReturn(false);

        when(adherenceService.hasScheduleMappings(scheduleTypeId)).thenReturn(false);

        when(adherenceService.isDoseTimeSupportedForScheduleType(any())).thenReturn(true);


        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                        .content(Utils.asJsonString(request))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-IAM-Client-Id", clientId.toString()))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testRegisterEntitySuccess() throws Exception
    {
        String uri = "/v1/entity";

        MERMEntityRequest request = new MERMEntityRequest();
        request.setEntityId(entityId);
        request.setUniqueIdentifier(uniqueIdentifier);
        request.setStartDate(Utils.getFormattedDate(new Date()));
        request.setImei(imei);

        when(entityService.validateActiveIamMappingByEntityId(clientId, entityId)).thenReturn(false);

        when(adherenceService.hasScheduleMappings(scheduleTypeId)).thenReturn(false);

        when(adherenceService.createIam(ArgumentMatchers.any(EntityRequest.class))).thenReturn(iamId);

        EntityResponse response = new EntityResponse(iamId);
        when(entityService.registerEntity(entityId, iamId, clientId)).thenReturn(response);

        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .content(Utils.asJsonString(request))
                .contentType(MediaType.APPLICATION_JSON)
                .header("X-IAM-Client-Id", clientId.toString()))
                .andExpect(status().isAccepted())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.iamId").value(iamId));
    }

    @Test
    public void testRegisterEntityInActiveScheduleExists() throws Exception
    {
        String uri = "/v1/entity";

        MERMEntityRequest request = new MERMEntityRequest();
        request.setEntityId(entityId);
        request.setUniqueIdentifier(uniqueIdentifier);
        request.setStartDate(Utils.getFormattedDate(new Date()));
        request.setImei(imei);

        when(entityService.validateActiveIamMappingByEntityId(clientId, entityId)).thenReturn(false);

        when(adherenceService.hasScheduleMappings(scheduleTypeId)).thenReturn(true);
        when(adherenceService.activeScheduleMappingExistsForEntityIdAndClientId(clientId, entityId, scheduleTypeId)).thenReturn(false);

        when(adherenceService.createIam(ArgumentMatchers.any(EntityRequest.class))).thenReturn(iamId);

        EntityResponse response = new EntityResponse(iamId);
        when(entityService.registerEntity(entityId, iamId, clientId)).thenReturn(response);

        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .content(Utils.asJsonString(request))
                .contentType(MediaType.APPLICATION_JSON)
                .header("X-IAM-Client-Id", clientId.toString()))
                .andExpect(status().isAccepted())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.iamId").value(iamId));
    }

    @Test
    public void testUpdateEntity_EntityNotFound() throws Exception
    {
        String uri = "/v1/entity";

        when(entityService.getActiveIamMappingByEntityId(clientId, entityId)).thenReturn(null);

        MERMEntityRequest request = new MERMEntityRequest();
        request.setEntityId(entityId);
        request.setUniqueIdentifier(uniqueIdentifier);
        request.setStartDate(Utils.getFormattedDate(new Date()));
        request.setImei("imei");

        mockMvc.perform(MockMvcRequestBuilders.put(uri)
                .content(Utils.asJsonString(request))
                .contentType(MediaType.APPLICATION_JSON)
                .header("X-IAM-Client-Id", clientId.toString()))
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_ENTITY_NOT_FOUND));


    }

    @Test
    public void testUpdateEntity_ActiveScheduleDoesNotExist() throws Exception
    {
        String uri = "/v1/entity";

        MERMEntityRequest request = new MERMEntityRequest();
        request.setEntityId(entityId);
        request.setUniqueIdentifier(uniqueIdentifier);
        request.setStartDate(Utils.getFormattedDate(new Date()));
        request.setImei("imei");

        AccessMapping accessMapping = new AccessMapping(clientId, iamId, entityId);
        when(entityService.getActiveIamMappingByEntityId(clientId, entityId)).thenReturn(accessMapping);

        when(adherenceService.hasScheduleMappings(scheduleTypeId)).thenReturn(true);
        when(adherenceService.activeScheduleMappingExistsForEntityIdAndClientId(clientId, entityId, scheduleTypeId)).thenReturn(false);

        mockMvc.perform(MockMvcRequestBuilders.put(uri)
                .content(Utils.asJsonString(request))
                .contentType(MediaType.APPLICATION_JSON)
                .header("X-IAM-Client-Id", clientId.toString()))
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_ACTIVE_SCHEDULE_DOES_NOT_EXIST));
    }

    @Test
    public void testUpdateEntity_Success() throws Exception
    {
        String uri = "/v1/entity";

        MERMEntityRequest request = new MERMEntityRequest();
        request.setEntityId(entityId);
        request.setUniqueIdentifier(uniqueIdentifier);
        request.setStartDate(Utils.getFormattedDate(new Date()));
        request.setImei(imei);

        AccessMapping accessMapping = new AccessMapping(clientId, iamId, entityId);
        when(entityService.getActiveIamMappingByEntityId(clientId, entityId)).thenReturn(accessMapping);

        when(adherenceService.hasScheduleMappings(scheduleTypeId)).thenReturn(true);
        when(adherenceService.activeScheduleMappingExistsForEntityIdAndClientId(clientId, entityId, scheduleTypeId)).thenReturn(true);

        when(adherenceService.updateIam(ArgumentMatchers.any(EntityRequest.class), ArgumentMatchers.anyLong())).thenReturn(true);

        mockMvc.perform(MockMvcRequestBuilders.put(uri)
                .content(Utils.asJsonString(request))
                .contentType(MediaType.APPLICATION_JSON)
                .header("X-IAM-Client-Id", clientId.toString()))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_ENTITY_SUCCCESSFULLY_UPDATED));
    }

    @Test
    public void testUpdateEntity_ScheduleDoesNotExist() throws Exception
    {
        String uri = "/v1/entity";

        MERMEntityRequest request = new MERMEntityRequest();
        request.setEntityId(entityId);
        request.setUniqueIdentifier(uniqueIdentifier);
        request.setStartDate(Utils.getFormattedDate(new Date()));
        request.setImei(imei);

        AccessMapping accessMapping = new AccessMapping(clientId, iamId, entityId);
        when(entityService.getActiveIamMappingByEntityId(clientId, entityId)).thenReturn(accessMapping);

        when(adherenceService.hasScheduleMappings(scheduleTypeId)).thenReturn(false);

        when(adherenceService.updateIam(ArgumentMatchers.any(EntityRequest.class), ArgumentMatchers.anyLong())).thenReturn(true);

        mockMvc.perform(MockMvcRequestBuilders.put(uri)
                .content(Utils.asJsonString(request))
                .contentType(MediaType.APPLICATION_JSON)
                .header("X-IAM-Client-Id", clientId.toString()))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_ENTITY_SUCCCESSFULLY_UPDATED));
    }

    @Test
    public void testReopenEntity_ActiveScheduleExists() throws Exception
    {
        String uri = "/v1/entity/reopen";

        MERMEntityRequest request = new MERMEntityRequest();
        request.setEntityId(entityId);
        request.setUniqueIdentifier(uniqueIdentifier);
        request.setStartDate(Utils.getFormattedDate(new Date()));
        request.setImei(imei);

        doNothing().when(entityService).validateNoActiveIamMappingByEntityId(clientId, entityId);

        when(adherenceService.hasScheduleMappings(scheduleTypeId)).thenReturn(true);
        when(adherenceService.activeScheduleMappingExistsForEntityIdAndClientId(clientId, entityId, scheduleTypeId)).thenReturn(true);

        mockMvc.perform(MockMvcRequestBuilders.put(uri)
                .content(Utils.asJsonString(request))
                .contentType(MediaType.APPLICATION_JSON)
                .header("X-IAM-Client-Id", clientId.toString()))
                .andExpect(status().isConflict())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_ACTIVE_SCHEDULE_EXISTS));
    }

    @Test
    public void testReopenEntity_Success() throws Exception
    {
        String uri = "/v1/entity/reopen";

        MERMEntityRequest request = new MERMEntityRequest();
        request.setEntityId(entityId);
        request.setUniqueIdentifier(uniqueIdentifier);
        request.setStartDate(Utils.getFormattedDate(new Date()));
        request.setImei(imei);

        doNothing().when(entityService).validateNoActiveIamMappingByEntityId(clientId, entityId);

        when(adherenceService.hasScheduleMappings(scheduleTypeId)).thenReturn(false);

        List<AccessMapping> accessMappings = new ArrayList<AccessMapping>(){
            {
                add(new AccessMapping(clientId, iamId, entityId));
            }
        };
        when(entityService.getIamMappingByEntityId(clientId, entityId)).thenReturn(accessMappings);

        Registration reopenedEntity = new Registration(iamId, scheduleTypeId);
        when(adherenceService.reopenEntity(ArgumentMatchers.any(EntityRequest.class), ArgumentMatchers.anyList())).thenReturn(reopenedEntity);

        when(entityService.toggleActiveStatus(reopenedEntity.getId(), clientId, true)).thenReturn(true);

        mockMvc.perform(MockMvcRequestBuilders.put(uri)
                .content(Utils.asJsonString(request))
                .contentType(MediaType.APPLICATION_JSON)
                .header("X-IAM-Client-Id", clientId.toString()))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_RE_OPENED_SUCCESSFULLY))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.iamId").value(iamId));
    }

    @Test
    public void testReopenEntity_InactiveScheduleExists() throws Exception {
        String uri = "/v1/entity/reopen";

        MERMEntityRequest request = new MERMEntityRequest();
        request.setEntityId(entityId);
        request.setUniqueIdentifier(uniqueIdentifier);
        request.setStartDate(Utils.getFormattedDate(new Date()));
        request.setImei(imei);

        doNothing().when(entityService).validateNoActiveIamMappingByEntityId(clientId, entityId);

        when(adherenceService.hasScheduleMappings(scheduleTypeId)).thenReturn(true);
        when(adherenceService.activeScheduleMappingExistsForEntityIdAndClientId(clientId, entityId, scheduleTypeId)).thenReturn(false);

        List<AccessMapping> accessMappings = new ArrayList<AccessMapping>() {
            {
                add(new AccessMapping(clientId, iamId, entityId));
            }
        };
        when(entityService.getIamMappingByEntityId(clientId, entityId)).thenReturn(accessMappings);

        Registration reopenedEntity = new Registration(iamId, scheduleTypeId);
        when(adherenceService.reopenEntity(ArgumentMatchers.any(EntityRequest.class), ArgumentMatchers.anyList())).thenReturn(reopenedEntity);

        when(entityService.toggleActiveStatus(reopenedEntity.getId(), clientId, true)).thenReturn(true);

        mockMvc.perform(MockMvcRequestBuilders.put(uri)
            .content(Utils.asJsonString(request))
            .contentType(MediaType.APPLICATION_JSON)
            .header("X-IAM-Client-Id", clientId.toString()))
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_RE_OPENED_SUCCESSFULLY))
            .andExpect(MockMvcResultMatchers.jsonPath("$.data.iamId").value(iamId));
    }

    public void deleteEntityTest_WrongDateFormat() throws Exception {
        String uri = "/v1/entity";
        String params = "?entityId=12312&endDate=20-2020 18:29:59";
        String clientId = "29";
        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .delete(uri + params)
                    .contentType(MediaType.APPLICATION_JSON)
                    .header("X-IAM-Client-Id", clientId)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isBadRequest())
            .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("end date format is incorrect"));
    }

    @Test
    public void deleteRegistrationTestEntityMappingNotFound() throws Exception {
        String uri = "/v1/entity";
        String params = "?entityId=12312&endDate=2020-08-02 18:29:59";
        String clientId = "29";
        when(entityService.getAllIamMapping(anyLong(), anyString())).thenReturn(new ArrayList<>());
        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .delete(uri + params)
                    .contentType(MediaType.APPLICATION_JSON)
                    .header("X-IAM-Client-Id", clientId)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isNotFound())
            .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("entity mapping not found!"));
    }

    @Test
    public void deleteRegistrationTestZeroEntitiesToDelete() throws Exception {
        String uri = "/v1/entity";
        String clientId = "29";
        String params = "?entityId=1&endDate=2020-04-04 18:30:00&deleteIfRequired=true";
        List<AccessMapping> accessMappings = Arrays.asList(
            new AccessMapping(29L, 1L, "1"),
            new AccessMapping(29L, 2L, "2"),
            new AccessMapping(29L, 3L, "3")
        );
        when(adherenceService.deleteIamIdsByStopDate(anyList(), any())).thenReturn(new ArrayList<>());
        when(entityService.getAllIamMapping(anyLong(), anyString())).thenReturn(accessMappings);
        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .delete(uri + params)
                    .contentType(MediaType.APPLICATION_JSON)
                    .header("X-IAM-Client-Id", clientId)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isBadRequest())
            .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("more than one active entity exists"));
    }

    @Test
    public void deleteEntity_SuccessDeleteSwitch() throws Exception {
        String uri = "/v1/entity";
        String clientId = "29";
        String params = "?entityId=1&endDate=2020-04-04 18:30:00&deleteIfRequired=true";
        List<AccessMapping> accessMappings = Arrays.asList(
            new AccessMapping(29L, 1L, "1"),
            new AccessMapping(29L, 2L, "2")
        );
        accessMappings.get(0).setActive(false);
        accessMappings.get(1).setActive(false);
        when(adherenceService.deleteIamIdsByStopDate(anyList(), any())).thenReturn(Arrays.asList(1L, 2L));
        when(entityService.getAllIamMapping(anyLong(), anyString())).thenReturn(accessMappings);
        when(adherenceService.stopAdherence(any(), any())).thenReturn(true);
        when(adherenceService.getLastClosedRegistration(anyList())).thenReturn(new Registration(1L, 2L));
        doNothing().when(entityService).deleteAccessMappingByIamIds(any());
        when(redisTemplate.delete(anyString())).thenReturn(true);
        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .delete(uri + params)
                    .contentType(MediaType.APPLICATION_JSON)
                    .header("X-IAM-Client-Id", clientId)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("entity successfully closed"));
        Mockito.verify(entityService, Mockito.times(1)).deleteAccessMappingByIamIds(any());
        Mockito.verify(adherenceService, Mockito.times(1)).stopAdherence(any(), any());
        Mockito.verify(redisTemplate, Mockito.times(2)).delete(anyString());
    }

    @Test
    public void deleteEntity_SuccessStopEntity() throws Exception {
        String uri = "/v1/entity";
        String clientId = "29";
        String params = "?entityId=1&endDate=2020-04-04 18:30:00&deleteIfRequired=true";
        List<AccessMapping> accessMappings = Arrays.asList(
            new AccessMapping(29L, 1L, "1"),
            new AccessMapping(29L, 2L, "2")
        );
        accessMappings.get(0).setActive(false);

        when(adherenceService.deleteIamIdsByStopDate(anyList(), any())).thenReturn(new ArrayList<>());
        when(entityService.getAllIamMapping(anyLong(), anyString())).thenReturn(accessMappings);
        when(adherenceService.stopAdherence(any(), any())).thenReturn(true);
        when(entityService.toggleActiveStatus(any(), any(), anyBoolean())).thenReturn(true);
        when(redisTemplate.delete(anyString())).thenReturn(true);
        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .delete(uri + params)
                    .contentType(MediaType.APPLICATION_JSON)
                    .header("X-IAM-Client-Id", clientId)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("entity successfully closed"));
        Mockito.verify(redisTemplate, Mockito.times(2)).delete(anyString());
        Mockito.verify(adherenceService, Mockito.times(1)).stopAdherence(any(), any());
        Mockito.verify(entityService, Mockito.times(1)).toggleActiveStatus(any(), any(), anyBoolean());
    }

    @Test
    public void deleteEntity_EntityAlreadyClosed() throws Exception {
        String uri = "/v1/entity";
        String clientId = "29";
        String params = "?entityId=1&endDate=2020-04-04 18:30:00&deleteIfRequired=false";
        List<AccessMapping> accessMappings = Arrays.asList(
            new AccessMapping(29L, 1L, "1"),
            new AccessMapping(29L, 2L, "2")
        );
        accessMappings.get(0).setActive(false);
        accessMappings.get(1).setActive(false);
        when(entityService.getAllIamMapping(anyLong(), anyString())).thenReturn(accessMappings);
        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .delete(uri + params)
                    .contentType(MediaType.APPLICATION_JSON)
                    .header("X-IAM-Client-Id", clientId)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isBadRequest())
            .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("entity already closed"));
    }

    @Test
    public void deleteEntity_EntityAlreadyClosed_ButCloseAgain() throws Exception {
        String uri = "/v1/entity";
        String clientId = "29";
        String params = "?entityId=1&endDate=2020-04-04 18:30:00&deleteIfRequired=false&updateEndDate=true";
        List<AccessMapping> accessMappings = Arrays.asList(
            new AccessMapping(29L, 1L, "1"),
            new AccessMapping(29L, 2L, "2")
        );
        accessMappings.get(0).setActive(false);
        accessMappings.get(1).setActive(false);

        when(entityService.getAllIamMapping(anyLong(), anyString())).thenReturn(accessMappings);
        when(adherenceService.stopAdherence(any(), any())).thenReturn(true);
        when(adherenceService.getLastClosedRegistration(anyList())).thenReturn(new Registration(1L, 2L));
        when(redisTemplate.delete(anyString())).thenReturn(true);
        when(entityService.toggleActiveStatus(any(), any(), anyBoolean())).thenReturn(true);

        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .delete(uri + params)
                    .contentType(MediaType.APPLICATION_JSON)
                    .header("X-IAM-Client-Id", clientId)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("entity successfully closed"));
    }
}

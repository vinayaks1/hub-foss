package tests;

import com.everwell.iam.utils.Utils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class TestUtils {

    public static void print(Object e) {
        System.out.println(e);
    }

    @Test
    public void removeAllTest() throws ParseException {
        List<String> oldNumbers = new ArrayList<>();
        oldNumbers.add("1");
        oldNumbers.add("2");
        oldNumbers.add("3");
        oldNumbers.add("4");

        List<String> newNumbers = new ArrayList<>();
        newNumbers.add("1");
        newNumbers.add("3");
        newNumbers.add("6");

        List<String> ans = new ArrayList<>();
        ans.add("6");

        List<String> numbersToAdd = Utils.removeAll(oldNumbers, newNumbers);
        assertEquals(ans, numbersToAdd);

        ans = new ArrayList<>();
        ans.add("2");
        ans.add("4");

        List<String> numbersToDeactivate = Utils.removeAll(newNumbers, oldNumbers);
        assertEquals(ans, numbersToDeactivate);

        oldNumbers.clear();
        oldNumbers.add("1");
        oldNumbers.add("2");
        oldNumbers.add("3");

        newNumbers = new ArrayList<>();
        newNumbers.add("5");
        newNumbers.add("6");

        ans = new ArrayList<>();
        ans.add("5");
        ans.add("6");

        numbersToAdd = Utils.removeAll(oldNumbers, newNumbers);
        assertEquals(ans, numbersToAdd);

        ans = new ArrayList<>();
        ans.add("1");
        ans.add("2");
        ans.add("3");

        numbersToDeactivate = Utils.removeAll(newNumbers, oldNumbers);
        assertEquals(ans, numbersToDeactivate);
    }

    @Test
    public void testSameDayTimeNegative_1() throws ParseException {
        Date startDate = Utils.convertStringToDate("2019-01-18 18:30:30");
        Date firstDate = Utils.convertStringToDate("2019-01-21 20:30:30");
        Date secondDate = Utils.convertStringToDate("2019-01-22 19:30:30");

        Assert.assertFalse(Utils.checkIfSameDayByTime(startDate, firstDate, secondDate));
    }

    @Test
    public void testSameDayTimeNegative_2() throws ParseException {
        Date startDate = Utils.convertStringToDate("2019-01-18 18:30:30");
        Date firstDate = Utils.convertStringToDate("2019-01-19 18:29:59");
        Date secondDate = Utils.convertStringToDate("2019-01-20 18:30:30");

        Assert.assertFalse(Utils.checkIfSameDayByTime(startDate, firstDate, secondDate));
    }


    @Test
    public void testSameDayTimePositive() throws ParseException {
        Date startDate = Utils.convertStringToDate("2019-01-18 18:30:30");
        Date firstDate = Utils.convertStringToDate("2019-01-19 20:30:30");
        Date secondDate = Utils.convertStringToDate("2019-01-20 16:30:30");

        Assert.assertTrue(Utils.checkIfSameDayByTime(startDate, firstDate, secondDate));
    }

    @Test
    public void utilsFilerTest() {
        List<Integer> stringList = new ArrayList<>();
        stringList.add(1);
        stringList.add(2);
        stringList.add(3);
        stringList.add(4);
        stringList.add(5);
        assertEquals(2, Utils.filter(stringList, f -> f > 2 && f < 5).size());
    }

    @Test
    public void utilsTimeToEntityEndDate() throws ParseException {
        Date startDate = Utils.convertStringToDate("2020-01-18 00:00:00");
        Long actualMinutes = Utils.timeToEntityEndDate(startDate, TimeUnit.MINUTES);
        Date endDateTime = DateUtils.addDays(startDate, 1);
        String currentTime = Utils.getFormattedDate(Utils.getCurrentDate(), "HH:mm:ss");
        Date mockCurrentTime = Utils.convertStringToDate("2020-01-18 " + currentTime);
        Long expectedMinutes = Utils.getDifference(mockCurrentTime, endDateTime, TimeUnit.MINUTES);
        Assert.assertEquals(actualMinutes, expectedMinutes);
    }

}

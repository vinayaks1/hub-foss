ALTER TABLE iam_caccess
  DROP COLUMN IF EXISTS matomo_url,
  DROP COLUMN IF EXISTS matomo_site_id;

ALTER TABLE iam_caccess
  ADD COLUMN IF NOT EXISTS event_flow_id int;

UPDATE iam_caccess
SET event_flow_id = 1
where id = 29;

UPDATE iam_caccess
SET event_flow_id = 2
where id = 63;

UPDATE iam_caccess
SET event_flow_id = 2
where id = 83;

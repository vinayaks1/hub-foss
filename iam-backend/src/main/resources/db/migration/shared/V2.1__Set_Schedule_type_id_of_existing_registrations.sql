-- Set schedule_type_id of all registrations in the table as 1
UPDATE iam_registration
set scheduletype_id = 1 where scheduletype_id is null
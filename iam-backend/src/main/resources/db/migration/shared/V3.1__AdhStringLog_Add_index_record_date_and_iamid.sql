-- AdhStringLog table
-- Create new Index on record_date & iam_id
CREATE INDEX IF NOT EXISTS idx_record_iamid
	ON iam_adhstring_log (record_date, iam_id);

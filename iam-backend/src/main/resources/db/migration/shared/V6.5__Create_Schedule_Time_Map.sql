create table if not exists iam_schedule_time_map
(
  id bigserial not null
    constraint iam_schedule_time_map_pkey
      primary key,
  created_date date,
  active boolean not null default 0,
  schedule_map_id bigint,
  FOREIGN KEY (schedule_map_id) REFERENCES iam_schedule_map(id),
  dose_time TIME not null
);

create index if not exists iam_schedule_time_map_schedule_map_id_idx on iam_schedule_time_map(schedule_map_id);

create index if not exists iam_schedule_time_map_schedule_map_id_active_idx on iam_schedule_time_map(schedule_map_id, active);
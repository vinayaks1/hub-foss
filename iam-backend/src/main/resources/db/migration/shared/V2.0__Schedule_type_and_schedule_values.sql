-- insert Schedule Type data into table
INSERT into iam_schedule_type(id,created_date, name) values (1,current_timestamp, 'Daily');
INSERT into iam_schedule_type(id,created_date, name) values (2,current_timestamp, 'Weekly');


-- insert Schedules into table

-- Schedules for weekly once dosage
INSERT into iam_schedule(id,value,scheduletype_id) values (1,'1000000', 2);
INSERT into iam_schedule(id,value,scheduletype_id) values (2,'0100000', 2);
INSERT into iam_schedule(id,value,scheduletype_id) values (3,'0010000', 2);
INSERT into iam_schedule(id,value,scheduletype_id) values (4,'0001000', 2);
INSERT into iam_schedule(id,value,scheduletype_id) values (5,'0000100', 2);
INSERT into iam_schedule(id,value,scheduletype_id) values (6,'0000010', 2);
INSERT into iam_schedule(id,value,scheduletype_id) values (7,'0000001', 2);
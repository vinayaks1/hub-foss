package com.everwell.iam.models.http.requests;

import com.everwell.iam.exceptions.ValidationException;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;

@Getter
@Setter
@ToString
public class MERMEntityRequest extends EntityRequest {

  private String imei;

  public MERMEntityRequest() {
    super(3);
  }

  @Override
  public void validate(EntityRequest entityRequest) {
    super.validate(entityRequest);
    MERMEntityRequest request = (MERMEntityRequest) entityRequest;
    if (StringUtils.isEmpty(request.getImei()))
      throw new ValidationException("imei is required");
  }

}

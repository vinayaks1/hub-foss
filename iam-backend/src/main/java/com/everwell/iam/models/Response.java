package com.everwell.iam.models;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.NoArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.io.Serializable;

@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
public class Response<T> implements Serializable {

    @Getter
    private boolean success;

    @Getter
    private String message;

    @Getter
    private T data;

    public Response(String message) {
        // All error responses
        this.success = false;
        this.message = message;

    }

    public Response(boolean success, T data, String message) {
        // All error responses
        this.success = success;
        this.message = message;
        this.data = data;
    }

    public Response(T data, String message) {
        // All success responses
        this.success = true;
        this.data = data;
        this.message = message;
    }

    public Response(boolean success, T data) {
        this.success = success;
        this.data = data;
    }
}

package com.everwell.iam.models.http.requests;

import com.everwell.iam.exceptions.ValidationException;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Getter
@Setter
@ToString
public class NNDLiteEntityRequest extends EntityRequest implements NNDCommonEntityRequest{

    private List<PhoneRequest.PhoneNumbers> phoneNumbers;

    public NNDLiteEntityRequest() {
        super(6);
    }

    @Override
    public void validate(EntityRequest entityRequest) {
        super.validate(entityRequest);
        NNDLiteEntityRequest request = (NNDLiteEntityRequest) entityRequest;
        if (CollectionUtils.isEmpty(request.getPhoneNumbers()))
            throw new ValidationException("phoneNumber numbers are required");
    }

}

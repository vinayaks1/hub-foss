package com.everwell.iam.models.dto;

import com.everwell.iam.enums.AverageAdherenceIntervals;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

@NoArgsConstructor
@Getter
@Setter
public class EntityDoseCountDto extends CumulativeDoseCountDto {

    public String entityId;

    public EntityDoseCountDto(String entityId) {
        super();
        this.entityId = entityId;
    }

    public EntityDoseCountDto(String entityId, Map<AverageAdherenceIntervals, IntervalAdherenceDto> avgAdherenceIntervals) {
        super(avgAdherenceIntervals);
        this.entityId = entityId;
    }
}

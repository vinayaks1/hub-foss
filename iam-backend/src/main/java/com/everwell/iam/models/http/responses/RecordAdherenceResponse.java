package com.everwell.iam.models.http.responses;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.List;

@ToString
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RecordAdherenceResponse {

    @Getter
    @Setter
    private List<Long> iamLogIds;

}

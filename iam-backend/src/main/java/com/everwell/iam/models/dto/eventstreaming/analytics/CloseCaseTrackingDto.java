package com.everwell.iam.models.dto.eventstreaming.analytics;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class CloseCaseTrackingDto {
    String entityId;
    Long iamId;
    Long clientId;
}

package com.everwell.iam.models.http.requests;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ReopenEntityRequest {
  String entityId;
  List<String> phoneNumbers;
}

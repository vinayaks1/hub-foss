package com.everwell.iam.models.db;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "IAM_ScheduleType")
public class ScheduleType {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    private Long id;

    @Getter
    @Column(name = "name")
    private String name;

    @Column(name = "created_date")
    @Temporal(TemporalType.DATE)
    private Date createdDate;

}

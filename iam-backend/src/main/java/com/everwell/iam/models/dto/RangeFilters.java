package com.everwell.iam.models.dto;

import com.everwell.iam.enums.RangeFilterType;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RangeFilters {
  private RangeFilterType type;
  private String from;
  private String to;
}

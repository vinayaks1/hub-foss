package com.everwell.iam.models.http.responses;

import com.everwell.iam.models.dto.AdherenceData;
import com.everwell.iam.models.dto.DoseTimeData;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.Date;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class AdherenceResponse {

    @Setter
    @Getter
    public String entityId;

    @Setter
    @Getter
    public List<AdherenceData> adherenceData;

    @Setter
    @Getter
    @ApiModelProperty(notes = "Combined adherence string of all Adherence Methods")
    private String adherenceString;

    @Setter
    @Getter
    @ApiModelProperty(notes = "Last Dosage of the latest Adherence Method")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private Date lastDosage;

    @Setter
    @Getter
    private int technologyDoses;

    @Setter
    @Getter
    private int manualDoses;

    @Setter
    @Getter
    private int totalDoses;

    @Setter
    @Getter
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private Date startDate;

    @Setter
    @Getter
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private Date endDate;

    @Setter
    @Getter
    public AdherenceStatisticsResponse adherenceStatistics;

    @Setter
    @Getter
    private long consecutiveMissedDoses;

    @Setter
    @Getter
    private long consecutiveMissedDosesFromYesterday;

    @Setter
    @Getter
    private List<DoseTimeData> doseTimeList;

}

package com.everwell.iam.models.http.requests;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VOTEntityRequest extends EntityRequest {

  private VOTEntityRequest() {
    super(2);
  }

}

package com.everwell.iam.models.db;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "IAM_AccessMapping")
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class AccessMapping {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "client_id")
    private Long clientId;

    @Column(name = "iam_id")
    private Long iamId;

    @Column(name = "entity_id")
    private String entityId;

    @Setter
    @Column(name = "active")
    private boolean active;

    public AccessMapping(Long clientId, Long iamId, String entityId) {
        this.clientId = clientId;
        this.iamId = iamId;
        this.entityId = entityId;
        this.active = true;
    }

}

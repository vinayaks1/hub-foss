package com.everwell.iam.models.http.responses;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@ToString
public class AllAdherenceResponse {

    private List<AdherenceResponse> adherenceResponseList;

    public AllAdherenceResponse(List<AdherenceResponse> responseList) {
        this.adherenceResponseList = responseList;
    }
}

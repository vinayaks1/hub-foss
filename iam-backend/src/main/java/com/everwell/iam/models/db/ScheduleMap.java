package com.everwell.iam.models.db;

import com.everwell.iam.models.dto.ScheduleSensitivity;
import com.everwell.iam.utils.Utils;
import lombok.*;
import org.apache.commons.lang3.tuple.Pair;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "IAM_Schedule_Map")
@NoArgsConstructor
@AllArgsConstructor
public class ScheduleMap {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Setter
    @Getter
    @Column(name = "active")
    private boolean active;

    @Getter
    @Column(name = "created_date")
    private Date createdDate;

    @Getter
    @Column(name = "iam_id")
    private Long iamId;

    @Getter
    @Setter
    @Column(name = "schedule_id")
    private Long scheduleId;

    @Getter
    @Column(name = "positive_sensitivity")
    private Long positiveSensitivity;

    @Getter
    @Column(name = "negative_sensitivity")
    private Long negativeSensitivity;

    @Getter
    @Setter
    @Column(name = "start_date")
    private Date startDate;

    @Getter
    @Setter
    @Column(name = "end_date")
    private Date endDate;

    @Getter
    @Setter
    @Column(name = "first_dose_offset")
    private Long firstDoseOffset;

    public ScheduleMap(Long iamId, Long scheduleId, Date createdDate, Date startDate, Long firstDoseOffset, ScheduleSensitivity sensitivity) {
        this.createdDate = Utils.formatDate(createdDate);
        this.active = true;
        this.iamId = iamId;
        this.scheduleId = scheduleId;
        this.firstDoseOffset = firstDoseOffset;
        this.negativeSensitivity = sensitivity.getLeftBuffer();
        this.positiveSensitivity = sensitivity.getRightBuffer();
        this.startDate = Utils.formatDate(startDate);
        this.endDate = null;
    }

    public ScheduleMap(Long iamId, Long scheduleId, Date startDate, Long firstDoseOffset, ScheduleSensitivity sensitivity) {
        this(iamId, scheduleId, Utils.getCurrentDate(), startDate, firstDoseOffset, sensitivity);
    }

    public ScheduleMap(Long id, boolean active, Long iamId, Date createdDate, Long scheduleId, Date startDate, Date endDate, Long firstDoseOffset, ScheduleSensitivity sensitivity) {
        this(id, active,iamId, createdDate, scheduleId, startDate, firstDoseOffset, sensitivity);
        this.endDate = Utils.formatDate(endDate);
    }

    public ScheduleMap(Long id, boolean active, Long iamId, Date createdDate, Long scheduleId, Date startDate, Long firstDoseOffSet, ScheduleSensitivity sensitivity) {
        this.id = id;
        this.active = active;
        this.createdDate = Utils.formatDate(createdDate);
        this.iamId = iamId;
        this.scheduleId = scheduleId;
        this.startDate = Utils.formatDate(startDate);
        this.negativeSensitivity = sensitivity.getLeftBuffer();
        this.positiveSensitivity = sensitivity.getRightBuffer();
        this.firstDoseOffset = firstDoseOffSet;
        this.endDate = null;
    }
}

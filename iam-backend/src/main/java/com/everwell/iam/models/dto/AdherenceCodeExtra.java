package com.everwell.iam.models.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
public class AdherenceCodeExtra {
    String color;
    String icon;
    String iconColor;

    public AdherenceCodeExtra(String color) {
        this.color = color;
    }

    public AdherenceCodeExtra() {}
}

package com.everwell.iam.models.http.responses;

import com.everwell.iam.models.db.CAccess;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ClientResponse {
    private Long id;
    private String name;
    private String password;
    private String authToken;
    private Long nextRefresh;

    public ClientResponse(CAccess access) {
        this.id = access.getId();
        this.name = access.getName();
        this.authToken = access.getAccessToken();
        this.nextRefresh = access.getNextRefresh();
    }
}

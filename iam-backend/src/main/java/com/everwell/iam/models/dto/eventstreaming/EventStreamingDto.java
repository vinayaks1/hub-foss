package com.everwell.iam.models.dto.eventstreaming;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class EventStreamingDto<T> {
  @JsonAlias("EventName")
  String eventName;
  @JsonAlias("Field")
  T field;
}


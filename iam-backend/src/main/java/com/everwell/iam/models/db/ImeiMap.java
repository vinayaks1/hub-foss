package com.everwell.iam.models.db;

import com.everwell.iam.utils.Utils;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "iam_imei_map")
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter @Setter @ToString
@NoArgsConstructor
public class ImeiMap {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Setter
  private Long id;

  @Column(name = "imei")
  private String imei;

  @Column(name = "iam_id")
  private Long iamId;

  @Column(name = "created_date")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  private Date createdDate;

  @Column(name = "start_date")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  private Date startDate;

  @Column(name = "stop_date")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  private Date stopDate;

  public ImeiMap(String imei, Long iamId, Date startDate) {
    this.imei = imei;
    this.iamId = iamId;
    this.createdDate = Utils.getCurrentDate();
    this.startDate = startDate;
  }
}



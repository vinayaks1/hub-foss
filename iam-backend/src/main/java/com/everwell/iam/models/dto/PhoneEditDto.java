package com.everwell.iam.models.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PhoneEditDto {
  Long iamId;
  String entityId;
  String primaryPhone;
  List<String> numbers;

  public PhoneEditDto(Long iamId, List<String> numbers, String primaryPhone) {
    this.iamId = iamId;
    this.numbers = numbers;
    this.primaryPhone = primaryPhone;
  }

}
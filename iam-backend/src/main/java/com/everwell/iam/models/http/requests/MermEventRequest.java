package com.everwell.iam.models.http.requests;

import com.everwell.iam.exceptions.ValidationException;
import lombok.*;
import org.apache.commons.lang3.StringUtils;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class MermEventRequest {
  private String sn;
  private String eventDateTime;

  public void validate() {
    if(StringUtils.isBlank(sn)) {
      throw new ValidationException("invalid merm details");
    }
  }
}

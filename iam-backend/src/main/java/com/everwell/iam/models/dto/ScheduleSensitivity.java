package com.everwell.iam.models.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ScheduleSensitivity {
    private Long leftBuffer;
    private Long rightBuffer;

    public ScheduleSensitivity(Long leftBuffer, Long rightBuffer) {
        this.leftBuffer = leftBuffer;
        this.rightBuffer = rightBuffer;
    }
}

package com.everwell.iam.models.db;

import com.everwell.iam.constants.Constants;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "iam_call_logs")
@Getter
@Setter
@NoArgsConstructor
public class CallLogs {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Setter
  private Long id;
  @Column(name = "number_dialled")
  private String numberDialled;
  @Column(name = "post_body")
  private String postBody;
  @Column(name = "added_on")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
  private Date addedOn;
  @Column(name = "client_time")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
  private Date clientTime;
  private String caller;

  public CallLogs(String numberDialled, String postBody, Date addedOn, Date clientTime, String caller) {
    this.numberDialled = numberDialled;
    this.postBody = postBody;
    this.addedOn = addedOn;
    this.clientTime = clientTime;
    this.caller = caller;
  }
}

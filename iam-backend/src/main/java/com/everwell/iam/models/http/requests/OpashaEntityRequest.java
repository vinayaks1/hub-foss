package com.everwell.iam.models.http.requests;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OpashaEntityRequest extends EntityRequest {

  private OpashaEntityRequest() {
    super(4);
  }

}

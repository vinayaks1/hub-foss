package com.everwell.iam.models;

import lombok.Getter;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class AuthenticationToken extends AbstractAuthenticationToken {
    @Getter
    private final String token;

    @Getter
    private final Long id;

    public AuthenticationToken(String token, Long id) {
        super(null);
        this.token = token;
        this.id = id;
        setAuthenticated(false);
    }

    public AuthenticationToken(String token, Long id, Collection<? extends GrantedAuthority> authorities) {
        //note that the constructor needs a collection of GrantedAuthority
        //but our User have a collection of our UserRole's
        super(authorities);

        this.token = token;
        this.id = id;
        setAuthenticated(true);
    }

    @Override
    public Object getCredentials() {
        return getToken();
    }

    @Override
    public Object getPrincipal() {
        return getId();
    }
}

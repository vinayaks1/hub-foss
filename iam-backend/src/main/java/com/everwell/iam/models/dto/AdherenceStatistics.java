package com.everwell.iam.models.dto;

import com.everwell.iam.enums.AdherenceCodeEnum;
import com.everwell.iam.utils.Utils;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
public class AdherenceStatistics implements Serializable {

    private String adherenceString;
    private int receivedDoses;
    private int receivedUnsureDoses;
    private int missedDoses;
    private int manualDoses;
    private int totalDoses;
    private int repeatReceived;
    private int unsureRepeatReceived;
    private int receivedUnscheduledDoses;

    public AdherenceStatistics(String adherenceString) {
        adherenceString = Utils.nullAsEmpty(adherenceString);
        this.adherenceString = adherenceString;
        receivedDoses = Utils.countChar(adherenceString, AdherenceCodeEnum.RECEIVED.getCode());
        receivedUnsureDoses = Utils.countChar(adherenceString, AdherenceCodeEnum.RECEIVED_UNSURE.getCode())
                + Utils.countChar(adherenceString, AdherenceCodeEnum.UNVALIDATED.getCode());
        missedDoses = Utils.countChar(adherenceString, AdherenceCodeEnum.MISSED.getCode())
                + Utils.countChar(adherenceString, AdherenceCodeEnum.NO_INFO.getCode());
        manualDoses = Utils.countChar(adherenceString, AdherenceCodeEnum.MANUAL.getCode()) + Utils.countChar(adherenceString, AdherenceCodeEnum.PATIENT_MANUAL.getCode());
        repeatReceived = Utils.countChar(adherenceString, AdherenceCodeEnum.TFN_REPEAT_RECEIVED.getCode());
        unsureRepeatReceived = Utils.countChar(adherenceString, AdherenceCodeEnum.TFN_REPEAT_RECEIVED_UNSURE.getCode());
        receivedUnscheduledDoses = Utils.countChar(adherenceString, AdherenceCodeEnum.RECEIVED_UNSCHEDULED.getCode());
        totalDoses = receivedDoses + receivedUnsureDoses + missedDoses +
                manualDoses + unsureRepeatReceived + repeatReceived + receivedUnscheduledDoses;
    }

    public int getTechnologyDoses() {
        return receivedDoses + receivedUnsureDoses + unsureRepeatReceived + repeatReceived + receivedUnscheduledDoses;
    }

}

package com.everwell.iam.models.http.responses;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EntityResponse {

    @Getter
    @Setter
    Long iamId;

    @Getter
    @Setter
    String entityId;

    public EntityResponse (String entityId) {
        this.entityId = entityId;
    }

    public EntityResponse (Long iamId) {
        this.iamId = iamId;
    }
}

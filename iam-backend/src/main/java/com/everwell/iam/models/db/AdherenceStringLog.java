package com.everwell.iam.models.db;

import com.everwell.iam.utils.Utils;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "IAM_Adhstring_Log")
@NoArgsConstructor
@ToString
public class AdherenceStringLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private Long id;

    @Column(name = "iam_id")
    @Getter
    private Long iamId;

    @Column(name = "value")
    @Getter
    @Setter
    private Character value;

    @Column(name = "record_date")
    @Temporal(TemporalType.TIMESTAMP)
    @Setter
    @Getter
    private Date recordDate;

    @Column(name = "created_date")
    @Temporal(TemporalType.DATE)
    private Date createdDate;

    @Column(name = "updated_date")
    @Temporal(TemporalType.TIMESTAMP)
    @Getter
    private Date updatedDate;

    public AdherenceStringLog(Long iamId, Character value) {
        this(iamId, value, new Date());
    }

    public AdherenceStringLog(Long iamId, Character value, Date recordDate) {
        this.iamId = iamId;
        this.value = value;
        this.recordDate = recordDate;
        this.createdDate = Utils.getCurrentDate();
        this.updatedDate = Utils.getCurrentDate();
    }

}

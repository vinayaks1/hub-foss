package com.everwell.iam.models.db;

import com.everwell.iam.utils.Utils;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "iam_merm_logs")
@Getter
@NoArgsConstructor
public class MermLogs {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Setter
  private Long id;

  @Column(name = "imei")
  private String imei;

  @Column(name = "created_date")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
  private Date createdDate;

  @Column(name = "client_time")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
  private Date clientTime;


  public MermLogs(String imei, Date clientTime) {
    this.imei = imei;
    this.createdDate = Utils.getCurrentDate();
    this.clientTime = clientTime;
  }
}

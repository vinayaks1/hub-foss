package com.everwell.iam.models.http.requests;

import lombok.*;

import java.util.List;

@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class AdherenceGlobalConfigRequest {
    List<String> configNameList;
}

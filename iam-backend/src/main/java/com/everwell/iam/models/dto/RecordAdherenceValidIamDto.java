package com.everwell.iam.models.dto;

import com.everwell.iam.models.db.Registration;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;

@AllArgsConstructor
@Getter
public class RecordAdherenceValidIamDto {
  Date date;
  Character value;
}

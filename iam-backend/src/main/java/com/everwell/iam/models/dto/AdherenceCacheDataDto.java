package com.everwell.iam.models.dto;

import com.everwell.iam.models.http.responses.AdherenceResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AdherenceCacheDataDto<T> {
  List<T> adherenceResponse;
  List<String> entityIds;
}

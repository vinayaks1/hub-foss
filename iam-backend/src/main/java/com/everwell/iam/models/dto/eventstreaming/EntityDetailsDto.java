package com.everwell.iam.models.dto.eventstreaming;

import com.everwell.iam.enums.DosageTypeEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.util.Date;


@AllArgsConstructor
@NoArgsConstructor
@Data
public class EntityDetailsDto {
  String entityId;
  DosageTypeEnum dosageType;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
  Date date;
}


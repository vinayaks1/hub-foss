package com.everwell.iam.models.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class IntervalAdherenceDto {
    long digitalDoses;
    long manualDoses;
    long totalDoses;
}

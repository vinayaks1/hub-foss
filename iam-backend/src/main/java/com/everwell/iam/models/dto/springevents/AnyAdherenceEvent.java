package com.everwell.iam.models.dto.springevents;

import com.everwell.iam.enums.CacheAdherenceStatusToday;
import com.everwell.iam.models.db.Registration;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class AnyAdherenceEvent {
  CacheAdherenceStatusToday adherenceStatusType;
  Registration registration;
  boolean eventDelayed;
  boolean updateLastDosage;
  boolean updateLastMissedDosage;
  boolean updateAdherenceString;
  boolean cacheNullDateEntity;
}

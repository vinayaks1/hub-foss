package com.everwell.iam.models.http.requests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * DTO request class for unregisteredPhoneNumber event handling
 * @author Ashish Shrivastava
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProcessUnregisteredPhoneNumberRequest {
    // Number from which call is received
    private String callerNumber;
    // Number to which call was targeted
    private String numberDialled;
}

package com.everwell.iam.models.db;


import lombok.*;

import javax.persistence.*;
import java.sql.Time;
import java.util.Date;

@Entity
@Table(name = "iam_schedule_time_map")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ScheduleTimeMap {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "active")
    private boolean active;

    @Column(name = "created_date")
    private Date createdDate;

    @Column(name = "schedule_map_id")
    private Long scheduleMapId;

    @Column(name = "dose_time")
    private Time doseTime;
}

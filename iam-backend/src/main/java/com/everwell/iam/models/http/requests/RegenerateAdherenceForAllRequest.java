package com.everwell.iam.models.http.requests;

import lombok.*;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RegenerateAdherenceForAllRequest {
  Long adherenceType;
  Long size;
}

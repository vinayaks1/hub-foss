package com.everwell.iam.models.db;

import com.everwell.iam.constants.Constants;
import com.everwell.iam.utils.Utils;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "iam_phone_map")
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class PhoneMap {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Setter
  private Long id;
  @Column(name = "phone_number")
  private String phoneNumber;
  @Column(name = "iam_id")
  private Long iamId;
  @Column(name = "added_on")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  private Date addedOn = Utils.getCurrentDate();
  @Column(name = "stop_date")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  private Date stopDate;
  @Transient
  private String entityId;

  public PhoneMap(String phoneNumber, Long iamId) {
    this.phoneNumber = phoneNumber;
    this.iamId = iamId;
    this.addedOn = Utils.getCurrentDate();
  }

  public PhoneMap(String phoneNumber, Long iamId, Date stopDate) {
    this.phoneNumber = phoneNumber;
    this.iamId = iamId;
    this.addedOn = Utils.getCurrentDate();
    this.stopDate = stopDate;
  }
}



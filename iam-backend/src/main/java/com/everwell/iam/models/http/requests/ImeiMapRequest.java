package com.everwell.iam.models.http.requests;

import com.everwell.iam.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

@Getter
@AllArgsConstructor
@ToString
public class ImeiMapRequest {
  String entityId;
  String imei;
  String startDate;

  public void validate(ImeiMapRequest imeiMapRequest) {
    if (StringUtils.isEmpty(imeiMapRequest.getEntityId())) {
      throw new ValidationException("entity mapping is required");
    }
  }
}

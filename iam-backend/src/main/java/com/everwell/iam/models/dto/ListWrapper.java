package com.everwell.iam.models.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
public class ListWrapper<T> {
  private List<T> data;
}

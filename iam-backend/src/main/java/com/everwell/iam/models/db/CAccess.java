package com.everwell.iam.models.db;

import com.everwell.iam.utils.Utils;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "IAM_CAccess")
@NoArgsConstructor
public class CAccess implements Serializable {

    @Id
    @Getter
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Setter
    private Long id;

    @Getter
    private String name;

    @Getter
    private String password;

    @Getter
    @Setter
    @Column(name = "access_token")
    private String accessToken;

    @Getter
    @Setter
    @Column(name = "next_refresh")
    private Long nextRefresh;

    @Getter
    @Setter
    @Column(name = "created_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @Getter
    @Setter
    @Column(name = "updated_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;

    @Getter
    @Setter
    @Column(name = "event_flow_id")
    private Long eventFlowId;

    @Getter
    @Column(name = "external", columnDefinition = "boolean default false")
    private boolean external;

    public CAccess(String name, String password, String token, Long nextRefresh) {
        this.name = name;
        this.password = password;
        this.accessToken = token;
        this.nextRefresh = nextRefresh;
        this.createdDate = Utils.getCurrentDate();
        this.updatedDate = Utils.getCurrentDate();

    }
}

package com.everwell.iam.models.http.requests;

import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class RegenerateAdherenceRequest {
  List<String> entityIdList;
  List<Long> iamIdList;

  public RegenerateAdherenceRequest(List<Long> iamIdList) {
    this.iamIdList = iamIdList;
  }
}

package com.everwell.iam.models.dto.springevents;

import com.everwell.iam.models.db.Registration;
import com.everwell.iam.models.http.requests.EntityRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class EpisodeEvent {
    private Registration registration;
    private EntityRequest entityRequest;
}

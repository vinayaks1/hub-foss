package com.everwell.iam.models.http.requests;

import com.everwell.iam.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IncomingCallRequest {
  private String numberDialed;
  private String caller;
  private String utcDateTime;
  private String postBody;

  public void validateRequest () {
    if (StringUtils.isBlank(getNumberDialed())) {
      throw new ValidationException("numberDialed is required");
    }
    if (StringUtils.isBlank(getCaller())) {
      throw new ValidationException("caller is required");
    }
    if (StringUtils.isBlank(getUtcDateTime())) {
      throw new ValidationException("utcDateTime is required");
    }
  }

}

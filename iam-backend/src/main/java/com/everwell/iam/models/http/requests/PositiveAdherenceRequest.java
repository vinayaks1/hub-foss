package com.everwell.iam.models.http.requests;

import com.everwell.iam.exceptions.ValidationException;
import lombok.*;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Getter
@ToString
@NoArgsConstructor
@Setter
public class PositiveAdherenceRequest {
    private List<String> entityIdList;

    public boolean validate() throws ValidationException {
        if(CollectionUtils.isEmpty(entityIdList)) {
            throw new ValidationException("invalid entity list");
        }
        return true;
    }
}

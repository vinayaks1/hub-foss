package com.everwell.iam.models.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@AllArgsConstructor
@Setter
@Getter
public class PositiveAdherenceDto {
    Long manualCount;
    Long digitalCount;
    Long missedCount;
    List<String> entityIdList;
}

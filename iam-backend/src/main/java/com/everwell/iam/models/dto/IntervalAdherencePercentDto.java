package com.everwell.iam.models.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class IntervalAdherencePercentDto {
    private double digitalAdherence;
    private double totalAdherence;
}

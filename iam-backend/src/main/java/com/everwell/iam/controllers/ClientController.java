package com.everwell.iam.controllers;

import com.everwell.iam.models.Response;
import com.everwell.iam.models.http.requests.RegisterClientRequest;
import com.everwell.iam.models.http.responses.ClientResponse;
import com.everwell.iam.services.ClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClientController {

    private static Logger LOGGER = LoggerFactory.getLogger(ClientController.class);

    @Autowired
    private ClientService clientService;

    @RequestMapping(value = "/v1/client", method = RequestMethod.POST)
    public ResponseEntity<Response<ClientResponse>> registerClient(@RequestBody RegisterClientRequest clientRequest) {
        LOGGER.debug("[registerClient] request received : " + clientRequest);
        ClientResponse clientResponse = clientService.registerClient(clientRequest);

        return new ResponseEntity<>(new Response<>(true, clientResponse), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/v1/client", method = RequestMethod.GET)
    public ResponseEntity<Response<ClientResponse>> getClientDetails(@RequestHeader(name = "X-IAM-Client-Id") Long id) {
        LOGGER.debug("[getClientDetails] fetching client details for id " + id);
        ClientResponse clientResponse = clientService.getClient(id);
        return new ResponseEntity<>(new Response<>(true, clientResponse), HttpStatus.OK);
    }
}

package com.everwell.iam.controllers;

import com.everwell.iam.annotations.IsAuthorized;
import com.everwell.iam.constants.Constants;
import com.everwell.iam.exceptions.ConflictException;
import com.everwell.iam.exceptions.NotFoundException;
import com.everwell.iam.exceptions.ValidationException;
import com.everwell.iam.models.Response;
import com.everwell.iam.models.db.AccessMapping;
import com.everwell.iam.models.db.Registration;
import com.everwell.iam.models.dto.eventstreaming.analytics.CloseCaseTrackingDto;
import com.everwell.iam.models.http.requests.EntityRequest;
import com.everwell.iam.models.http.responses.EntityResponse;
import com.everwell.iam.services.AdherenceService;
import com.everwell.iam.services.EntityService;
import com.everwell.iam.utils.CacheUtils;
import com.everwell.iam.utils.Utils;
import com.everwell.iam.validators.EntityValidator;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@IsAuthorized
@RestController
public class EntityController {

    private static Logger LOGGER = LoggerFactory.getLogger(EntityController.class);

    @Autowired
    private EntityService entityService;

    @Autowired
    private AdherenceService adherenceService;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    ConcurrentHashMap<String, Integer> concurrentHashMap = new ConcurrentHashMap<>();

    @RequestMapping(value = "/v1/entity", method = RequestMethod.POST)
    public ResponseEntity<Response<EntityResponse>> registerEntity(@RequestBody EntityRequest entityRequest,
                     @RequestHeader(name = "X-IAM-Client-Id") Long clientId){
        LOGGER.info("[registerEntity] request received: " + entityRequest);
        //controller level validation
        entityRequest.validate(entityRequest);
        //concurrent hashmap returns previous value if already added,
        //using it as a pessimistic locking mechanism, using in-memory hashmap as opposed to redis
        //to introduce as less latency as possible for our registration flow.
        Integer val = concurrentHashMap.putIfAbsent(entityRequest.getEntityId(), 1);
        if (val != null) {
            throw new ConflictException("Active entity already exists");
        }
        //db level -- validate specific adh tech details -- abort if any of these validation fails
        if (entityService.validateActiveIamMappingByEntityId(clientId, entityRequest.getEntityId())) {
            LOGGER.error("[registerEntity] Active entity already exists: " + entityRequest.getEntityId());
            throw new ConflictException("Active entity already exists");
        }
        if(adherenceService.hasScheduleMappings(entityRequest.getScheduleTypeId()) && adherenceService.activeScheduleMappingExistsForEntityIdAndClientId(clientId, entityRequest.getEntityId(), entityRequest.getScheduleTypeId())) {
            throw new ConflictException("Active schedule exits");
        }
        if (!CollectionUtils.isEmpty(entityRequest.getDoseTimeList()) && !adherenceService.isDoseTimeSupportedForScheduleType(entityRequest.getScheduleTypeId())) {
            throw new ValidationException("Dose timings not supported for the schedule");
        }

        if (CollectionUtils.isEmpty(entityRequest.getDoseTimeList()) && adherenceService.isDoseTimeSupportedForScheduleType(entityRequest.getScheduleTypeId())) {
            throw new ValidationException("Dose timings is required for the schedule");
        }
        entityRequest.setClientId(clientId);
        Long iamId = adherenceService.createIam(entityRequest);
        EntityResponse response = entityService.registerEntity(entityRequest.getEntityId(), iamId, clientId);
        CacheUtils.deleteKey(Constants.CACHE_USER_PREFIX + entityRequest.getEntityId());
        CacheUtils.deleteKey(Constants.CACHE_AVG_ADHERENCE_PREFIX + entityRequest.getEntityId());
        concurrentHashMap.remove(entityRequest.getEntityId());
        LOGGER.info("[registerEntity] response generated: " + response);
        return new ResponseEntity<>(new Response<>(response, "entity successfully created"), HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/v1/entity", method = RequestMethod.GET)
    public ResponseEntity<Response<EntityResponse>> getEntityId(String uniqueIdentifier, Long adherenceType, @RequestHeader(name = "X-IAM-Client-Id") Long clientId){
        LOGGER.info("[getEntityId] request received: uniqueIdentifier " + uniqueIdentifier + " adherenceType " + adherenceType);
        if(StringUtils.isEmpty(uniqueIdentifier)) {
            throw new ValidationException("invalid unique identifier");
        }
        if(null == adherenceType) {
            throw new ValidationException("invalid adherence type");
        }

        List<Long> iamIds = adherenceService.getIamIdsByAdherenceTechAndUniqueIdentifier(uniqueIdentifier, adherenceType);
        if(CollectionUtils.isEmpty(iamIds)) {
            throw new NotFoundException("entity was not found");
        }
        List<String> entityIds = entityService.getEntityByIamIds(iamIds, clientId);
        if(CollectionUtils.isEmpty(entityIds)) {
            throw new NotFoundException("entity was not found");
        } else if(entityIds.size() >= 2) {
            throw new ConflictException("more than one active entity is mapped");
        }
        EntityResponse response = new EntityResponse(entityIds.get(0));
        LOGGER.info("[getEntityId] response generated: " + response);
        return new ResponseEntity<>(new Response<>(response,"data fetched successfully"), HttpStatus.OK);
    }

    @ApiOperation(
        value = "Stop treatment on IAM + Delete switch pockets if needed",
        notes = "Treatment will be stopped of a particular entity id on its current Monitoring Method, " +
            "Also used in switching of MM, we stop the previous one and start another." +
            "Partial deletion of entity is allowed (i.e. switch pockets). IAM does not allow " +
            "complete deletion of entities."
    )
    @RequestMapping(value = "/v1/entity", method = RequestMethod.DELETE)
    public ResponseEntity<Response<String>> deleteEntity(@RequestParam(value = "entityId") String entityId, @RequestParam(value = "endDate") String endDate,
                                                         @RequestParam(value = "updateEndDate", defaultValue = "false") boolean updateEndDate,
                                                         @RequestParam(value = "deleteIfRequired", defaultValue = "false") boolean deleteIfRequired,
                                                         @RequestHeader(name = "X-IAM-Client-Id") Long clientId) {
        LOGGER.debug("[deleteEntity] request received : " + entityId + ", " + endDate + ", " + deleteIfRequired + ", " + updateEndDate);
        Date stopDate;
        try {
            stopDate = Utils.convertStringToDate(endDate);
        } catch (ParseException e) {
            throw new ValidationException("end date format is incorrect");
        }
        List<AccessMapping> accessMappings = entityService.getAllIamMapping(clientId, entityId);
        if (CollectionUtils.isEmpty(accessMappings))
            throw new NotFoundException("entity mapping not found!");
        Response<String> response;
        Long iamIdToClose;
        //try to delete if required
        List<Long> iamIdsToUnmap = new ArrayList<>();
        if (deleteIfRequired) {
            iamIdsToUnmap.addAll(
                adherenceService.deleteIamIdsByStopDate(
                    accessMappings.stream().map(AccessMapping::getIamId).collect(Collectors.toList()),
                    stopDate
                )
            );
        }
        if (!CollectionUtils.isEmpty(iamIdsToUnmap)) {
            entityService.deleteAccessMappingByIamIds(iamIdsToUnmap);
            iamIdToClose = adherenceService.getLastClosedRegistration(
                Utils.removeAll(
                    iamIdsToUnmap,
                    accessMappings.stream().map(AccessMapping::getIamId).collect(Collectors.toList())
                )
            ).getId();
        } else {
            List<AccessMapping> mapping = accessMappings.stream().filter(AccessMapping::isActive).collect(Collectors.toList());
            //just in case we have corrupted data with more than 1 active
            if (mapping.size() == 1) {
                iamIdToClose = mapping.get(0).getIamId();
            } else if (mapping.size() > 1) {
                throw new ValidationException("more than one active entity exists");
            } else {
                //just in case the case is already closed, and client requests to close it again (can be a diff date)
                if (updateEndDate)
                    iamIdToClose = adherenceService.getLastClosedRegistration(
                        accessMappings.stream().map(AccessMapping::getIamId).collect(Collectors.toList())
                    ).getId();
                else
                    throw new ValidationException("entity already closed");
            }
        }
        adherenceService.stopAdherence(iamIdToClose, stopDate);
        entityService.toggleActiveStatus(iamIdToClose, clientId, false);
        CacheUtils.deleteKey(Constants.CACHE_USER_PREFIX + entityId);
        CacheUtils.deleteKey(Constants.CACHE_AVG_ADHERENCE_PREFIX + entityId);
        //case close succeeded, send events to Matomo
        applicationEventPublisher.publishEvent(new CloseCaseTrackingDto(entityId, iamIdToClose, clientId));
        response = new Response<>("stop success", "entity successfully closed");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/entity", method = RequestMethod.PUT)
    public ResponseEntity<Response<EntityResponse>> updateEntity(@RequestBody EntityRequest entityRequest,
                                                 @RequestHeader(name = "X-IAM-Client-Id") Long clientId) {
        LOGGER.info("[updateEntity] request received : " + entityRequest);
        EntityValidator.validateUpdateEntityRequest(entityRequest);
        AccessMapping mapping = entityService.getActiveIamMappingByEntityId(clientId, entityRequest.getEntityId());
        if (null == mapping) {
            LOGGER.error("entity was not found!");
            throw new NotFoundException("entity was not found");
        }
        if (adherenceService.hasScheduleMappings(entityRequest.getScheduleTypeId()) && !adherenceService.activeScheduleMappingExistsForEntityIdAndClientId(clientId, entityRequest.getEntityId(), entityRequest.getScheduleTypeId())) {
            throw new NotFoundException("Active schedule does not exists");
        }
        adherenceService.updateIam(entityRequest, mapping.getIamId());
        EntityResponse response = new EntityResponse(mapping.getIamId());
        CacheUtils.deleteKey(Constants.CACHE_USER_PREFIX + entityRequest.getEntityId());
        CacheUtils.deleteKey(Constants.CACHE_AVG_ADHERENCE_PREFIX + entityRequest.getEntityId());
        LOGGER.info("[updateEntity] response generated : " + response);
        return new ResponseEntity<>(new Response<>(response, "entity successfully updated"), HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/entity/reopen", method = RequestMethod.PUT)
    public ResponseEntity<Response<EntityResponse>> reopenEntity(@RequestBody EntityRequest entityRequest, @RequestHeader(name = "X-IAM-Client-Id") Long clientId) {
        LOGGER.info("[reopenEntity] request received : " + entityRequest);
        entityService.validateNoActiveIamMappingByEntityId(clientId, entityRequest.getEntityId());
        if(adherenceService.hasScheduleMappings(entityRequest.getScheduleTypeId()) && adherenceService.activeScheduleMappingExistsForEntityIdAndClientId(clientId, entityRequest.getEntityId(), entityRequest.getScheduleTypeId())) {
            throw new ConflictException("Active schedule exits");
        }
        List<AccessMapping> accessMappings = entityService.getIamMappingByEntityId(clientId, entityRequest.getEntityId());
        List<Long> iamIds = accessMappings.stream().map(AccessMapping::getIamId).collect(Collectors.toList());
        entityRequest.setClientId(clientId);
        Registration reopenedEntity = adherenceService.reopenEntity(entityRequest, iamIds);
        entityService.toggleActiveStatus(reopenedEntity.getId(), clientId, true);
        EntityResponse entityResponse = new EntityResponse(reopenedEntity.getId());
        Response<EntityResponse> response = new Response<>(entityResponse, "Re-opened Successfully!");
        CacheUtils.deleteKey(Constants.CACHE_USER_PREFIX + entityRequest.getEntityId());
        CacheUtils.deleteKey(Constants.CACHE_AVG_ADHERENCE_PREFIX + entityRequest.getEntityId());
        LOGGER.info("[reopenEntity] response generated : " + response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}

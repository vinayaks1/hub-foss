package com.everwell.iam.controllers;

import com.everwell.iam.annotations.IsAuthorized;
import com.everwell.iam.constants.Constants;
import com.everwell.iam.enums.AdherenceCodeEnum;
import com.everwell.iam.exceptions.NotFoundException;
import com.everwell.iam.exceptions.ValidationException;
import com.everwell.iam.models.Response;
import com.everwell.iam.models.db.AccessMapping;
import com.everwell.iam.models.dto.*;
import com.everwell.iam.models.dto.eventstreaming.analytics.CloseCaseTrackingDto;
import com.everwell.iam.models.http.requests.*;
import com.everwell.iam.models.http.responses.*;
import com.everwell.iam.services.AdherenceService;
import com.everwell.iam.services.ClientService;
import com.everwell.iam.services.EntityService;
import com.everwell.iam.utils.CacheUtils;
import com.everwell.iam.utils.Utils;
import io.swagger.annotations.ApiOperation;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.everwell.iam.constants.Constants.CACHE_USER_PREFIX;

@RestController
public class AdherenceController {

    private static Logger LOGGER = LoggerFactory.getLogger(AdherenceController.class);

    @Autowired
    private AdherenceService adherenceService;

    @Autowired
    private EntityService entityService;

    @Autowired
    ClientService clientService;

    @Setter
    @Value("${iam.cache.fallback.enabled}")
    private boolean fallbackForCache;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @IsAuthorized
    @ApiOperation(
        value = "Deprecated, Use 'DELETE' of /v1/entity",
        notes = "Treatment will be stopped of a particular entity id on its current Monitoring Method, " +
            "Also used in switching of MM, we stop the previous one and start another."
    )
    @Deprecated
    @RequestMapping(value = "/v1/adherence/stop", method = RequestMethod.POST)
    public ResponseEntity<Response<String>> stopAdherence(@RequestBody StopAdherenceRequest adherenceRequest,
                                                  @RequestHeader(name = "X-IAM-Client-Id") Long clientId) {
        LOGGER.debug("[stopAdherence] request received : " + adherenceRequest);

        AccessMapping mapping = entityService.getActiveIamMappingByEntityId(clientId, adherenceRequest.getEntityId());
        if(null == mapping) {
            LOGGER.error("[stopAdherence] active entity was not found for id " + adherenceRequest.getEntityId());
            throw new NotFoundException("active entity was not found");
        }
        Response<String> response = null;
        try {
            Date endDate = null;
            if(StringUtils.isNotBlank(adherenceRequest.getEndDate())) {
                endDate = Utils.convertStringToDate(adherenceRequest.getEndDate());
            }
            boolean adherenceStopSuccess = adherenceService.stopAdherence(mapping.getIamId(), endDate);
            boolean makeInactiveSuccess = entityService.toggleActiveStatus(mapping.getIamId(), clientId, false);
            if (makeInactiveSuccess && adherenceStopSuccess) {
                response = new Response<>("Stop success", "entity adherence successfully stopped");
                CacheUtils.deleteKey(Constants.CACHE_USER_PREFIX + mapping.getEntityId());
                CacheUtils.deleteKey(Constants.CACHE_AVG_ADHERENCE_PREFIX + mapping.getEntityId());
            } else {
                response = new Response<>("entity adherence failed to stop");
            }
        } catch (ParseException e) {
            throw new ValidationException("failed to parse date");
        }
        //case close succeeded, send events to Matomo
        applicationEventPublisher.publishEvent(new CloseCaseTrackingDto(mapping.getEntityId(), mapping.getIamId(), clientId));
        LOGGER.info("[stopAdherence] response generated: " + response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @IsAuthorized
    @ApiOperation(
        value = "Get patient adherence",
        notes = "Used to get patient adherence across all its treatment pockets (like NONE, 99D, MERM, etc). " +
            "Can also get the technology logs from this, if required"
    )
    @RequestMapping(value = "/v1/adherence", method = RequestMethod.GET)
    public ResponseEntity<Response<AdherenceResponse>> getAdherence(@RequestParam(value = "iamId", required = false) Long iamId, @RequestParam(value = "entityId", required = false) String entityId,
                                               @RequestParam(value = "logsRequired", required = false) Boolean logsRequired, @RequestHeader(name = "X-IAM-Client-Id") Long clientId) {
        LOGGER.info("[getAdherence] request received for entity id: " + entityId);
        if(StringUtils.isBlank(entityId) && null == iamId) {
            throw new ValidationException("entity identification required");
        }

        AdherenceResponse response = null;

        if (logsRequired != null && !logsRequired && !StringUtils.isEmpty(entityId)) {
            AdherenceCacheDataDto<AdherenceResponse> adherenceCacheDataDto = adherenceService.getAdherenceFromCache(
                    Collections.singletonList(entityId), CACHE_USER_PREFIX,
                    (cacheEntry, allIdsSet) -> adherenceService.formatCacheForAdherence(cacheEntry, allIdsSet));
            if (CollectionUtils.isEmpty(adherenceCacheDataDto.getEntityIds()) && !CollectionUtils.isEmpty(adherenceCacheDataDto.getAdherenceResponse())) {
                response = adherenceCacheDataDto.getAdherenceResponse().get(0);
            }
        }

        if (null == response) {
            List<Long> iamIdList = new ArrayList<>();
            if(null == iamId) {
                List<AccessMapping> accessMappings = entityService.getAllIamMapping(clientId, entityId);
                if(!CollectionUtils.isEmpty(accessMappings)) {
                    for (AccessMapping mapping : accessMappings) {
                        iamIdList.add(mapping.getIamId());
                    }
                } else {
                    LOGGER.error("[getAdherence] entity mapping does not exist for entity id: " + entityId);
                    throw new NotFoundException("entity mapping does not exist");
                }
            } else {
                iamIdList = Arrays.asList(iamId);
                List<AccessMapping> accessMappings = entityService.getIamMapping(clientId, iamIdList);
                if (CollectionUtils.isEmpty(accessMappings)) {
                    LOGGER.error("[getAdherence] entity mapping does not exist for iamId: " + iamId);
                    throw new NotFoundException("entity mapping does not exist for this IAM Id");
                }
            }
            response = adherenceService.getAllAdherence(iamIdList, logsRequired);
            if (entityId != null) {
                response.setEntityId(entityId);
                CacheUtils.putIntoCache(CACHE_USER_PREFIX + entityId, Utils.asJsonString(response), Utils.timeToEntityEndDate(response.getAdherenceData().get(0).getStartDate(), TimeUnit.SECONDS));
                EntityDoseCountDto entityDoseCount = adherenceService.generateEntityDoseCountStats(new EntityAdherenceDto(entityId, response.getAdherenceString(), response.getStartDate(), response.getEndDate()));
                CacheUtils.putIntoCache(Constants.CACHE_AVG_ADHERENCE_PREFIX + entityId, Utils.asJsonString(entityDoseCount), Utils.timeToEntityEndDate(response.getAdherenceData().get(0).getStartDate(), TimeUnit.SECONDS));
            }
        }
        LOGGER.info("[getAdherence] response generated: " + response);
        return new ResponseEntity<>(new Response<>(response,"data fetched successfully"), HttpStatus.OK);
    }

    @IsAuthorized
    @ApiOperation(
        value = "Get bulk patient adherence",
        notes = "Used to get bulk patient adherence across all their treatment pockets (like NONE, 99D, MERM, etc)"
    )
    @RequestMapping(value = "/v1/adherence/search", method = RequestMethod.POST)
    public ResponseEntity<Response<AllAdherenceResponse>> searchForAdherence(@RequestBody SearchAdherenceRequest adherenceRequest,
                                                     @RequestHeader(name = "X-IAM-Client-Id") Long clientId) {
        LOGGER.info("[searchForAdherence] search adherence request " + adherenceRequest);
        adherenceRequest.validate();
        Map<String, List<AccessMapping>> accessMappingToEntityMap = entityService.getMultiEntityMapping(adherenceRequest.getEntityIdList());
        List<AdherenceResponse> allEntityResponseList = adherenceService.getAdherenceBulk(accessMappingToEntityMap, adherenceRequest.getFilters());
        AllAdherenceResponse response = new AllAdherenceResponse(allEntityResponseList);
        LOGGER.info("[searchForAdherence] response generated: " + response);
        return new ResponseEntity<>(new Response<>(response,"data fetched successfully"), HttpStatus.OK);
    }

    @IsAuthorized
    @ApiOperation(
            value = "Get average digital and total adherence for patient list",
            notes = "Used to get average adherence "
    )
    @RequestMapping(value = "/v1/adherence/average", method = RequestMethod.POST)
    public ResponseEntity<Response<AllAvgAdherenceResponse>> getAverageAdherenceCached(@RequestBody AverageAdherenceRequest averageAdherenceRequest,
                                                                             @RequestHeader(name = "X-IAM-Client-Id") Long clientId) {
        LOGGER.info("[getAverageAdherence] average adherence request " + averageAdherenceRequest);
        averageAdherenceRequest.validate();
        Map<String, List<AccessMapping>> accessMappingToEntityMap = entityService.getMultiEntityMapping(averageAdherenceRequest.getEntityIdList());
        if(CollectionUtils.isEmpty(accessMappingToEntityMap)){
            LOGGER.error("[getAverageAdherence] entity mapping does not exist for any entity id");
            throw new NotFoundException("entity mapping not found for any entity id");
        }
        AllAvgAdherenceResponse avgAdherenceResponse = adherenceService.getAverageAdherence(accessMappingToEntityMap, averageAdherenceRequest.isPatientDetailsRequired(), averageAdherenceRequest.getAverageAdherenceIntervals());
        LOGGER.info("[getAverageAdherence] response generated: " + avgAdherenceResponse);
        return new ResponseEntity<>(new Response<>(avgAdherenceResponse,"average adherence calculated successfully"), HttpStatus.OK);
    }

    @IsAuthorized
    @ApiOperation(
        value = "Get counts of DIGITAL & MANUAL Events for 'Today'"
    )
    @RequestMapping(value = "/v1/adherence/positive-events", method = RequestMethod.POST)
    public ResponseEntity<Response<PositiveCountResponse>> getPositiveEventsCount(@RequestBody PositiveAdherenceRequest adherenceRequest, @RequestHeader(name = "X-IAM-Client-Id") Long clientId) {
        adherenceRequest.validate();
        PositiveAdherenceDto positiveAdherenceDto = adherenceService.countPositiveCallsForToday(adherenceRequest.getEntityIdList(), fallbackForCache);
        if (fallbackForCache && positiveAdherenceDto.getEntityIdList().size() > 0) {
            Map<String, List<AccessMapping>> accessMappingToEntityMap = entityService.getMultiEntityMapping(positiveAdherenceDto.getEntityIdList());
            List<AdherenceResponse> adherenceStringData = adherenceService.getAdherenceBulk(accessMappingToEntityMap, null);
            long digitalCount = Utils.filter(adherenceStringData, f -> AdherenceCodeEnum.getDigitallyConfirmedEnumList().contains(AdherenceCodeEnum.getByCode(f.getAdherenceString().charAt(f.getAdherenceString().length() - 1)))).size();
            long manualCount = Utils.filter(adherenceStringData, f -> AdherenceCodeEnum.getManuallyConfirmedEnumList().contains(AdherenceCodeEnum.getByCode(f.getAdherenceString().charAt(f.getAdherenceString().length() - 1)))).size();
            long missedCount = Utils.filter(adherenceStringData, f -> AdherenceCodeEnum.getManuallyMissedEnumList().contains(AdherenceCodeEnum.getByCode(f.getAdherenceString().charAt(f.getAdherenceString().length() - 1)))).size();
            positiveAdherenceDto.setDigitalCount(positiveAdherenceDto.getDigitalCount() + digitalCount);
            positiveAdherenceDto.setManualCount(positiveAdherenceDto.getManualCount() + manualCount);
            positiveAdherenceDto.setMissedCount(positiveAdherenceDto.getMissedCount() + missedCount);
        }
        Response<PositiveCountResponse> response = new Response<>(true, new PositiveCountResponse(positiveAdherenceDto.getManualCount(), positiveAdherenceDto.getDigitalCount(), positiveAdherenceDto.getMissedCount()));
        LOGGER.info("[getPositiveEventsCount] response generated: " + response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @IsAuthorized
    @ApiOperation(
        value = "Record Adherence",
        notes = "Takes in a date and Adherence Code to mark adherence for that day"
    )
    @RequestMapping(value = "/v1/adherence/record", method = RequestMethod.POST)
    public ResponseEntity<Response<RecordAdherenceResponse>> recordAdherence(@RequestBody RecordAdherenceRequest adherenceRequest,
                                                  @RequestHeader(name = "X-IAM-Client-Id") Long clientId) {
      /*
       * Only entityId is used for adherence recording as
       * 1. recording can happen when treatment is closed
       * 2. recording can happen for previous inactive iam
       */
      LOGGER.info("[recordAdherence] record adherence request " + adherenceRequest);
      String entityId = adherenceRequest.getEntityId();
      List<AccessMapping> mappingList = entityService.getAllIamMapping(clientId, entityId);
      if(CollectionUtils.isEmpty(mappingList)) {
          LOGGER.error("entity was not found " + adherenceRequest.getEntityId());
          throw new NotFoundException("entity with id " + adherenceRequest.getEntityId() + " was not found");
      }
      List<Long> iamIdList = mappingList.stream().map(AccessMapping::getIamId).collect(Collectors.toList());
      RecordAdherenceResponse response = adherenceService.recordAdherence(adherenceRequest, iamIdList);
      LOGGER.info("[recordAdherence] response generated: " + response);
      return new ResponseEntity<>(new Response<>(response, "entity request accepted"), HttpStatus.OK);
    }

    @IsAuthorized
    @ApiOperation(
        value = "Regenerate Adherence",
        notes = "Regenerate Adherence of 1 or more patients. It will re-evaluate all call logs/merm events/maunal/missed dose etc and generate new adherence string"
    )
    @RequestMapping(value = "/v1/adherence/regenerate", method = RequestMethod.POST)
    public ResponseEntity<Response<List<String>>> regenerateAdherence(@RequestBody RegenerateAdherenceRequest regenerateAdherenceRequest, @RequestHeader(name = "X-IAM-Client-Id") Long clientId) {
        LOGGER.info("[regenerateAdherence] received: " + regenerateAdherenceRequest);
        if (CollectionUtils.isEmpty(regenerateAdherenceRequest.getIamIdList())) {
            List<AccessMapping> accessMappings = entityService.getIamMappingByEntityId(clientId, regenerateAdherenceRequest.getEntityIdList());
            List<Long> iamIds = accessMappings.stream().map(AccessMapping::getIamId).distinct().collect(Collectors.toList());
            List<String> entityIds = accessMappings.stream().map(AccessMapping::getEntityId).distinct().collect(Collectors.toList());
            if (entityIds.size() != regenerateAdherenceRequest.getEntityIdList().size()) {
                return new ResponseEntity<>(new Response<>(false, Utils.removeAll(entityIds, regenerateAdherenceRequest.getEntityIdList()), "Entity ids not found!"), HttpStatus.OK);
            }
            regenerateAdherenceRequest.setIamIdList(iamIds);
        }
        LOGGER.info("[regenerateAdherence] entities size: " + regenerateAdherenceRequest.getIamIdList().size());
        applicationEventPublisher.publishEvent(new RegenerateAdherenceRequest(regenerateAdherenceRequest.getIamIdList()));
        Response<List<String>> response = new Response<>(true, null, "Adherence Regenerated Successfully!");
        LOGGER.info("[regenerateAdherence] response generated: " + response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @IsAuthorized
    @RequestMapping(value = "/v1/adherence/regenerate/all", method = RequestMethod.POST)
    public ResponseEntity<Response<String>> regenerateAdherenceForAll(@RequestBody RegenerateAdherenceForAllRequest request) {
        LOGGER.info("[regenerateAdherenceForAll] request received: " + request);
        if (request.getAdherenceType() == null) {
            throw new ValidationException("Adherence Type is missing");
        }
        if (request.getSize() == null) {
            LOGGER.warn("[regenerateAdherenceForAll] size missing, defaulting to 1000");
            request.setSize(1000L);
        }
        if (request.getSize() > 10000) {
            LOGGER.error("[regenerateAdherenceForAll] Size should be less than 10,000!");
            return new ResponseEntity<>(new Response<>(false, "Size should be less than 10,000!"), HttpStatus.OK);
        }
        List<Long> iamIds = adherenceService.getIamIdsByAdherenceTech(request.getAdherenceType(), request.getSize());
        if (iamIds.size() == 0) {
            LOGGER.error("[regenerateAdherenceForAll] No entity found!");
            return new ResponseEntity<>(new Response<>(false, "No entity found!"), HttpStatus.OK);
        }
        applicationEventPublisher.publishEvent(new RegenerateAdherenceRequest(iamIds));
        LOGGER.info("[regenerateAdherenceForAll] response generated: Adherence Regenerated Successfully!");
        return new ResponseEntity<>(new Response<>(true, "Adherence Regenerated Successfully!"), HttpStatus.OK);
    }

    @ApiOperation(
        value = "Get default adherence code colors & their description",
        notes = "Source of truth for all adherence code colors and their description. Useful for calendar and legend"
    )
    @RequestMapping(value = "/v1/adherence-config", method = RequestMethod.GET)
    public ResponseEntity<Response<List<AdherenceCodeConfigResponse>>> getAdherenceCodeConfig(@RequestHeader(name = "X-IAM-Client-Id") Long clientId) {
        LOGGER.info("[getAdherenceCodeConfig] request received");
        return new ResponseEntity<>(new Response<>(adherenceService.getAdherenceColorCodeConfig(),"data fetched successfully"), HttpStatus.OK);
    }

    @IsAuthorized
    @ApiOperation(
            value = "Get the adherence config names",
            notes = "Get the names of all the adherence config supported by the module"
    )
    @RequestMapping(value = "/v1/adherence-config-names", method = RequestMethod.GET)
    public ResponseEntity<Response<List<String>>> getAdherenceConfigNames() {
        LOGGER.info("[getAdherenceConfigNames] request received");
        return new ResponseEntity<>(new Response<>(adherenceService.getAllConfigNames(),"Adherence config names fetched successfully"), HttpStatus.OK);
    }

    @IsAuthorized
    @ApiOperation(
            value = "Get global adherence configs",
            notes = "Get all the adherence configs details supported by the module"
    )
    @RequestMapping(value = "/v1/adherence-global-config", method = RequestMethod.POST)
    public ResponseEntity<Response<AdherenceGlobalConfigResponse>> getAdherenceGlobalConfig(@RequestBody AdherenceGlobalConfigRequest configRequest) {
        LOGGER.info("[getAdherenceGlobalConfig] request received "+ configRequest);
        return new ResponseEntity<>(new Response<>(adherenceService.getAdherenceGlobalConfig(configRequest),"Adherence global configs fetched successfully"), HttpStatus.OK);
    }

}
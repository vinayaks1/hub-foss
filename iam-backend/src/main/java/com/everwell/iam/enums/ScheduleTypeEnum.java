package com.everwell.iam.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@RequiredArgsConstructor
public enum ScheduleTypeEnum {

    DAILY("Daily", 1),
    WEEKLY("Weekly", 2),
    MULTI_FREQUENCY("Multi Frequency", 3);

    @Getter
    private final String name;

    @Getter
    private final Integer id;

    private static Map<String, ScheduleTypeEnum> typeNameMap = new HashMap<String, ScheduleTypeEnum>() {
        {
            put(ScheduleTypeEnum.DAILY.name.toLowerCase(), ScheduleTypeEnum.DAILY);
            put(ScheduleTypeEnum.WEEKLY.name.toLowerCase(), ScheduleTypeEnum.WEEKLY);
            put(ScheduleTypeEnum.MULTI_FREQUENCY.name.toLowerCase(), ScheduleTypeEnum.MULTI_FREQUENCY);
        }
    };
    public static ScheduleTypeEnum getMatch(String name) {
        return typeNameMap.getOrDefault(name.toLowerCase(), null);
    }

}

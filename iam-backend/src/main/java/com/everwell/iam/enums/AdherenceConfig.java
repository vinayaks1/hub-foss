package com.everwell.iam.enums;

public enum AdherenceConfig {
    COLOR_CODE,
    ADHERENCE_TECH,
    SCHEDULE_TYPE
}

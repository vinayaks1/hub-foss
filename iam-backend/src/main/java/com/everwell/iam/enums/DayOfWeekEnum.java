package com.everwell.iam.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public enum DayOfWeekEnum {

    MONDAY(0, 'M'),
    TUESDAY(1, 'T'),
    WEDNESDAY(2, 'W'),
    THURSDAY(3, 'H'),
    FRIDAY(4, 'F'),
    SATURDAY(5, 'S'),
    SUNDAY(6, 'U');

    @Getter
    private final Integer placeValue;

    @Getter
    private final char value;

    public static Map<Character, Integer> charValueMap = Arrays.stream(DayOfWeekEnum.values()).collect(Collectors.toMap(DayOfWeekEnum::getValue, DayOfWeekEnum::getPlaceValue));

    public static Map<Integer, Character> valueCharMap = Arrays.stream(DayOfWeekEnum.values()).collect(Collectors.toMap(DayOfWeekEnum::getPlaceValue, DayOfWeekEnum::getValue));

}

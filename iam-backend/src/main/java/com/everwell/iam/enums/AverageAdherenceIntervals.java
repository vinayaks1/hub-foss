package com.everwell.iam.enums;

import com.everwell.iam.models.dto.EntityAdherenceDto;
import com.everwell.iam.models.dto.IntervalAdherenceDto;
import com.everwell.iam.utils.Utils;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Date;

@RequiredArgsConstructor
public enum AverageAdherenceIntervals {
    ALL_TIME("ALL_TIME", Integer.MAX_VALUE),
    LAST_7_DAYS("LAST_7_DAYS",7),
    LAST_30_DAYS("LAST_30_DAYS",30),
    LAST_90_DAYS("LAST_90_DAYS",90),
    LAST_365_DAYS("LAST_365_DAYS",365);

    @Getter
    private final String name;

    @Getter
    private final int value;

    public IntervalAdherenceDto getIntervalDoseCounts(EntityAdherenceDto entityAdherence) {
        int intervalLength = this.value;
        String adherenceString = entityAdherence.getAdherenceString();
        Date endDate = entityAdherence.getEndDate();
        Date startDate = entityAdherence.getStartDate();
        long entityDigitalDays, entityManualDays, entityBlankDays;
        long totalAdherenceLength = Utils.getDifferenceDays(startDate, endDate == null ? Utils.getCurrentDate() : endDate) + 1;
        long entityTotalDays = Math.min(totalAdherenceLength, intervalLength);

        if (totalAdherenceLength > intervalLength) {
            adherenceString = (int)totalAdherenceLength - intervalLength < adherenceString.length() ?
                    adherenceString.substring((int)totalAdherenceLength - intervalLength) : "";
        }

        entityDigitalDays = AdherenceCodeEnum.count(adherenceString, AdherenceCodeEnum.getDigitallyConfirmedEnumList());
        entityManualDays = AdherenceCodeEnum.count(adherenceString, AdherenceCodeEnum.getManuallyConfirmedEnumList());
        entityBlankDays = AdherenceCodeEnum.BLANK.count(adherenceString);

        entityTotalDays -= entityBlankDays;
        return new IntervalAdherenceDto(entityDigitalDays,entityManualDays,entityTotalDays);
    }

}

package com.everwell.iam.enums;

import com.everwell.iam.models.dto.AdherenceCodeExtra;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.List;

@RequiredArgsConstructor
public enum AdherenceCodeEnum {

    QUIET('0', "QUIET", new AdherenceCodeExtra(), "no_response_for_today"),
    ENDED('1', "ENDED", new AdherenceCodeExtra("#6a98dd"), "treatment_start_end"),
    MISSED('2', "MISSED", new AdherenceCodeExtra("#990000"), "manually_reported_missed_dose"),
    UNVALIDATED('3', "DIGITAL", new AdherenceCodeExtra("#5dbe55", "exclamation", "#000000"), "digitally_reported_dose_unreviewed"),
    RECEIVED('4', "DIGITAL", new AdherenceCodeExtra("#5dbe55"), "digitally_reported_dose"),
    RECEIVED_UNSURE('5', "DIGITAL", new AdherenceCodeExtra("#ffff00"), "digitally_reported_from_shared_phone_number"),
    NO_INFO('6', "UNMARK", new AdherenceCodeExtra("#ff7878"), "unreported_dose"),
    BLANK('7', "BLANK", new AdherenceCodeExtra("#FFFFFF"), "no_adherence_tracking"),
    ENROLLMENT('8', "ENROLLMENT", new AdherenceCodeExtra("#6a98dd"), "treatment_start_end"),
    MANUAL('9', "MANUAL", new AdherenceCodeExtra("#91dd90"), "manually_reported_dose"),
    TFN_REPEAT_RECEIVED('A', "DIGITAL", new AdherenceCodeExtra("#5dbe55", "exclamation", "#000000"), "digitally_reported_using_same_toll_free_number"),
    TFN_REPEAT_RECEIVED_UNSURE('B', "DIGITAL", new AdherenceCodeExtra("#FFFF00", "exclamation", "#000000"), "digitally_reported_using_same_toll_free_number_and_shared_phone_number"),
    RECEIVED_UNSCHEDULED('C', "DIGITAL", new AdherenceCodeExtra("#FFA500"), "dose_digitally_reported_within_dosing_schedule"),
    PATIENT_MANUAL('D', "PATIENT_MANUAL", new AdherenceCodeExtra("#91dd90"),"patient_reported_manual_dose"),
    PATIENT_MISSED('E', "PATIENT_MISSED", new AdherenceCodeExtra("#990000"), "patient_reported_missed_dose"),
    MULTI_DOSE_PARTIAL('F', "PARTIAL_DOSE", new AdherenceCodeExtra("#ffff00"), "patient_taken_partial_dose"),
    EMPTY_SCHEDULE('G', "EMPTY_SCHEDULE", new AdherenceCodeExtra("#ffffff"), "patient_on_empty_schedule");

    @Getter
    private final char code;

    @Getter
    private final String trackingEventName;

    @Getter
    private final AdherenceCodeExtra design;

    @Getter
    private final String description;

    public int count(String adherenceString) {
        return (int) adherenceString.chars().filter(ch -> ch == this.code).count();
    }

    public static List<AdherenceCodeEnum> getBlankAdherenceEnumList() {
        return Arrays.asList(QUIET, BLANK);
    }

    public static List<AdherenceCodeEnum> getCalledAdherenceEnumList() {
        return Arrays.asList(RECEIVED, RECEIVED_UNSURE, MANUAL, ENROLLMENT, TFN_REPEAT_RECEIVED, TFN_REPEAT_RECEIVED_UNSURE, UNVALIDATED, RECEIVED_UNSCHEDULED,PATIENT_MANUAL);
    }

    public static List<AdherenceCodeEnum> getDigitallyConfirmedEnumList() {
      return Arrays.asList(RECEIVED, RECEIVED_UNSURE, ENROLLMENT, TFN_REPEAT_RECEIVED, TFN_REPEAT_RECEIVED_UNSURE, RECEIVED_UNSCHEDULED, UNVALIDATED);
    }

    public static List<AdherenceCodeEnum> getManuallyConfirmedEnumList() {
        return Arrays.asList(MANUAL, PATIENT_MANUAL);
    }

    public static List<AdherenceCodeEnum> getManuallyMissedEnumList() {
        return Arrays.asList(MISSED, PATIENT_MISSED);
    }

    public static AdherenceCodeEnum[] values;

    public static AdherenceCodeEnum getByCode(char code) {
        if (values == null) {
            // Apparently this can be expensive so it's good to cache it.
            values = AdherenceCodeEnum.values();
        }
        for (AdherenceCodeEnum aCode : values) {
            if (aCode.getCode() == code) {
                return aCode;
            }
        }
        return null;
    }

    public static AdherenceCodeEnum mergeCodesByPriority(AdherenceCodeEnum codeEnum, AdherenceCodeEnum newCodeEnum) {
        if(getDigitallyConfirmedEnumList().contains(newCodeEnum)) {
            return newCodeEnum;
        }
        else if(getCalledAdherenceEnumList().contains(codeEnum) || getBlankAdherenceEnumList().contains(newCodeEnum)) {
            newCodeEnum = codeEnum;
        }
        return newCodeEnum;
    }

    public static int count(String adherenceString, List<AdherenceCodeEnum> codes) {
        return codes.stream().mapToInt(f -> f.count(adherenceString)).sum();
    }

}

package com.everwell.iam.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import static com.everwell.iam.constants.RabbitMQConstants.*;

@Getter
@AllArgsConstructor
public enum RabbitMQEventEnum {
    LAST_DOSAGE_EVENT(LAST_DOSAGE, HEADERS_EXCHANGE),
    REGENERATE_ADHERENCE_EVENT(REGENERATE_ADHERENCE, EX_DIRECT),
    UNREGISTERED_PHONE_NUMBER_EVENT(UNREGISTERED_PHONE_NUMBER, EX_DIRECT);

    private String eventName;
    private String exchange;
}


package com.everwell.iam.validators;

import com.everwell.iam.exceptions.ValidationException;
import com.everwell.iam.models.http.requests.EntityRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class EntityValidator {

    public static boolean validateUpdateEntityRequest(EntityRequest entityRequest) throws ValidationException {
        return validateEntityMappingMandatory(entityRequest.getEntityId()) && validateDateMandatory(entityRequest.getStartDate());
    }

    public static boolean validateEntityMappingMandatory(String entityId) {
        if(StringUtils.isBlank(entityId)) {
            throw new ValidationException("entity mapping is required");
        }
        return true;
    }

    public static boolean validateDateMandatory(String date) {
        if (StringUtils.isBlank(date)) {
            throw new ValidationException("no modified parameters");
        }
        return true;
    }

}

package com.everwell.iam.vendors.models.db;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "video_entity_mapping")
@NoArgsConstructor
public class VideoEntityMapping {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    private Long id;

    @Getter
    @Column(name = "video_id")
    private String videoId;

    @Getter
    @Column(name = "iam_id")
    private Long iamId;


    @Getter
    @Setter
    private Integer status;

    @Column(name = "upload_date")
    @Setter
    @Getter
    private Date uploadDate;

    public VideoEntityMapping(String videoId, Long iamId, Integer status) {
        this.videoId = videoId;
        this.iamId = iamId;
        this.status = status;
        this.uploadDate = new Date();
    }

    public VideoEntityMapping(String videoId, Long iamId, Integer status, Date uploadDate) {
        this.videoId = videoId;
        this.iamId = iamId;
        this.status = status;
        this.uploadDate = uploadDate;
    }
}

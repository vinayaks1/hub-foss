package com.everwell.iam.vendors;

import com.everwell.iam.annotations.IsAuthorized;
import com.everwell.iam.controllers.AdherenceController;
import com.everwell.iam.enums.AdherenceTech;
import com.everwell.iam.exceptions.NotFoundException;
import com.everwell.iam.exceptions.ValidationException;
import com.everwell.iam.handlers.impl.NNDHandler;
import com.everwell.iam.handlers.impl.NNDLiteHandler;
import com.everwell.iam.models.Response;
import com.everwell.iam.models.db.AccessMapping;
import com.everwell.iam.models.db.CallLogs;
import com.everwell.iam.models.db.PhoneMap;
import com.everwell.iam.models.db.Registration;
import com.everwell.iam.models.dto.PhoneDeleteDto;
import com.everwell.iam.models.dto.PhoneEditDto;
import com.everwell.iam.models.http.requests.IncomingCallRequest;
import com.everwell.iam.models.http.requests.PhoneRequest;
import com.everwell.iam.services.AdherenceService;
import com.everwell.iam.services.EntityService;
import com.everwell.iam.utils.Utils;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

@RestController
public class NNDController {
    /**
     * NNDController Manages:
     * 1. Operations on calls
     * 2. Operations on phones
     **/
    @Autowired
    private NNDHandler nndHandler;
    @Autowired
    private NNDLiteHandler nndLiteHandler;
    @Autowired
    private EntityService entityService;
    @Autowired
    private AdherenceService adherenceService;

    private static final Logger LOGGER = LoggerFactory.getLogger(NNDController.class);

    @ApiOperation(
        value = "Deprecated, Use 'POST' processCall",
        notes = "Processes only India Specific call events"
    )
    //sample post - ?CallNumber=18003136110&msisdn=919812107115&date=Fri%20Aug%2009%202019%2023:05:36%20GMT%200530%20(IST)&voiceid=1565372136_9091355658&noforward=1
    @Deprecated
    @RequestMapping(value = "/v1/call/process", method = RequestMethod.GET)
    public ResponseEntity<Response<String>> processCall(@RequestParam("CallNumber") String numberDialed, @RequestParam("msisdn") String caller, @RequestParam String date) {
        LOGGER.info("Call received with caller=" + caller + ", TFN=" + numberDialed + ", client time=" + date);
        String post = "?CallNumber=" + numberDialed + "&msisdn=" + caller + "&date=" + date;
        post = post.replaceAll("\\s", "%20");
        date = date.replaceAll("%20", " ");
        Date calledDate = Utils.getCurrentDate();
        if (!StringUtils.isEmpty(date)) {
            String postDate = Utils.prefix(date, 15);
            try {
                // Example date: Thu Feb 23 2017 18:39:53 GMT+0530 (IST), Thu Feb 02 2017 09:09:06 GMT+0530 (IST)
                // The suffix " GMT+0530 (IST)" is 15 characters.
                calledDate = Utils.dateToUTC(Utils.convertStringToDate(postDate, "E MMM dd yyyy HH:mm:ss"), "IST"); //IMI is always in IST
            } catch (ParseException e) {
                LOGGER.warn("[processIncomingCall] couldn't parse date, defaulting to current time");
            }
        }
        IncomingCallRequest incomingCallRequest = new IncomingCallRequest(numberDialed, caller, Utils.getFormattedDate(calledDate), post);
        // Invoking saveCallLog only through nndHandler (and not nndLiteHandler) to avoid doubling of callLog entries
        CallLogs callLog =  nndHandler.saveCallLog(incomingCallRequest);
        nndHandler.processIncomingCall(callLog);
        nndLiteHandler.processIncomingCall(callLog);
        Response<String> response = new Response<>(null, "Call Registered!");
        LOGGER.info("[processCall] response generated: " + response);
        return new  ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(
        value = "Register call for any incoming call",
        notes = "Used as a common entry point for any 'call' which might contribute to adherence"
    )
    @RequestMapping(value = "/v1/call/process", method = RequestMethod.POST)
    public ResponseEntity<Response<String>> processCall(@RequestBody IncomingCallRequest incomingCallRequest ) {
        LOGGER.info("[processCall] Call received with caller=" + incomingCallRequest.getCaller() + ", TFN=" + incomingCallRequest.getNumberDialed() + ", utc time=" + incomingCallRequest.getUtcDateTime());
        incomingCallRequest.validateRequest();
        // Invoking saveCallLog only through nndHandler (and not nndLiteHandler) to avoid doubling of callLog entries
        CallLogs callLog =  nndHandler.saveCallLog(incomingCallRequest);
        /*
         * TO-DO - POC to replace the current dual async calls with suggested bridge class implementation
         * Controlled will call Bridge which will do common data operations and processing
         * after which it will call Handlers for individual processing
         */
        nndHandler.processIncomingCall(callLog);
        nndLiteHandler.processIncomingCall(callLog);
        Response<String> response = new Response<>(null, "Call Registered!");
        LOGGER.info("[processCall] response generated: " + response);
        return new  ResponseEntity<>(response, HttpStatus.OK);
    }

    @IsAuthorized
    @RequestMapping(value = "/v1/phone", method = RequestMethod.POST)
    public ResponseEntity<Response<String>> addPhones(@RequestBody PhoneRequest phoneMapRequest, @RequestHeader(name = "X-IAM-Client-Id") Long clientId) {
        LOGGER.info("[addPhones] request received: " + phoneMapRequest);
        if (StringUtils.isEmpty(phoneMapRequest.getEntityId())) {
            throw new ValidationException("entity mapping is required");
        }
        // marks only for active iam ids
        if (!entityService.validateActiveIamMappingByEntityId(clientId, phoneMapRequest.getEntityId())) {
            LOGGER.error("[addPhones] entity not found!");
            throw new NotFoundException("entity not found!");
        }
        AccessMapping accessMapping = entityService.getActiveIamMappingByEntityId(clientId, phoneMapRequest.getEntityId());
        List<PhoneMap> phoneMap = phoneMapRequest
                                    .getPhones()
                                    .stream()
                                    .map(p -> new PhoneMap(p.getPhoneNumber(), accessMapping.getIamId()))
                                    .collect(Collectors.toList());
        Registration registration = adherenceService.getRegistrationsByIamId(Collections.singletonList(accessMapping.getIamId())).get(0);
        boolean success = false;
        if (AdherenceTech.NNDOTS.getId() == registration.getAdTechId())
            success = nndHandler.savePhoneNumbers(phoneMap, clientId);
        else if (AdherenceTech.NNDLITE.getId() == registration.getAdTechId())
            success = nndLiteHandler.savePhoneNumbers(phoneMap, clientId);
        Response<String> response = new Response<>(success, null);
        LOGGER.info("[addPhones] response generated: " + response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @IsAuthorized
    @RequestMapping(value = "/v1/phone", method = RequestMethod.DELETE)
    public ResponseEntity<Response<String>> removePhones(@RequestBody PhoneDeleteDto phoneMapRequest, @RequestHeader(name = "X-IAM-Client-Id") Long clientId) {
        //Please Note: iamId is honoured if present, else we check for entityId
        LOGGER.info("[removePhones] received with body: " + phoneMapRequest);
        List<Long> validIamIds = new ArrayList<>();
        if (!CollectionUtils.isEmpty(phoneMapRequest.getIamIds())) {
            boolean valid = entityService.validateMapping(phoneMapRequest.getIamIds(), clientId);
            if (!valid) {
                LOGGER.error("[removePhones] one or more entity mapping not found!");
                throw new NotFoundException("one or more entity mapping not found!");
            }
            validIamIds = phoneMapRequest.getIamIds();
        } else if (!CollectionUtils.isEmpty(phoneMapRequest.getEntityIds())) {
            if (!entityService.validateActiveIamMappingByEntityId(clientId, phoneMapRequest.getEntityIds())) {
                LOGGER.error("[removePhones] entity not found!");
                throw new NotFoundException("entity not found!");
            }
            List<AccessMapping> mapping = entityService.getActiveIamMappingByEntityId(clientId, phoneMapRequest.getEntityIds());
            validIamIds.addAll(mapping.stream().map(AccessMapping::getIamId).collect(Collectors.toList()));
        }
        List<Registration> registrationList = adherenceService.getRegistrationsByIamId(validIamIds);
        List<Long> nndIds = registrationList.stream().filter(r -> r.getAdTechId() == AdherenceTech.NNDOTS.getId()).map(Registration::getId).collect(Collectors.toList());
        List<Long> nndLiteIds = registrationList.stream().filter(r -> r.getAdTechId() == AdherenceTech.NNDLITE.getId()).map(Registration::getId).collect(Collectors.toList());
        if (nndIds.size() != 0)
            nndHandler.deactivatePhoneNumbersByIamIds(new HashSet<>(nndIds));
        if (nndLiteIds.size() != 0)
            nndLiteHandler.deactivatePhoneNumbersByIamIds(new HashSet<>(nndLiteIds));
        Response<String> response = new Response<>(true, null);
        LOGGER.info("[removePhones] response generated: " + response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @IsAuthorized
    @RequestMapping(value = "/v1/phone", method = RequestMethod.PUT)
    public ResponseEntity<Response<String>> updatePhones(@RequestBody PhoneEditDto phone, @RequestHeader(name = "X-IAM-Client-Id") Long clientId) {
        LOGGER.info("[updatePhones] received with body: " + phone);
        if (StringUtils.isEmpty(phone.getPrimaryPhone())) {
            throw new ValidationException("Primary Phone Not Found!");
        }
        if (phone.getIamId() == null) {
            if (!StringUtils.isEmpty(phone.getEntityId())) {
                if (!entityService.validateActiveIamMappingByEntityId(clientId, phone.getEntityId())) {
                    LOGGER.error("[updatePhones] entity not found");
                    throw new NotFoundException("entity not found!");
                }
            } else {
                throw new ValidationException("invalid identifier");
            }
            AccessMapping accessMapping = entityService.getActiveIamMappingByEntityId(clientId, phone.getEntityId());
            phone.setIamId(accessMapping.getIamId());
        }
        Registration registration = adherenceService.getRegistrationsByIamId(Collections.singletonList(phone.getIamId())).get(0);
        if (AdherenceTech.NNDOTS.getId() == registration.getAdTechId())
            nndHandler.deactivateOldPhonesAndAddNew(phone);
        else if (AdherenceTech.NNDLITE.getId() == registration.getAdTechId())
            nndLiteHandler.deactivateOldPhonesAndAddNew(phone);
        Response<String> response = new Response<>(true, null);
        LOGGER.info("[updatePhones] response generated: " + response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}

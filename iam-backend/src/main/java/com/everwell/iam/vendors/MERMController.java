package com.everwell.iam.vendors;

import com.everwell.iam.enums.AdherenceTech;
import com.everwell.iam.exceptions.ConflictException;
import com.everwell.iam.exceptions.NotFoundException;
import com.everwell.iam.handlers.impl.MERMHandler;
import com.everwell.iam.models.Response;
import com.everwell.iam.models.db.AccessMapping;
import com.everwell.iam.models.db.ImeiMap;
import com.everwell.iam.models.db.Registration;
import com.everwell.iam.models.http.requests.GetBulkImeiRequest;
import com.everwell.iam.models.http.requests.ImeiEditRequest;
import com.everwell.iam.models.http.requests.MermEventRequest;
import com.everwell.iam.models.http.requests.SearchAdherenceRequest;
import com.everwell.iam.models.http.responses.ImeiResponse;
import com.everwell.iam.services.AdherenceService;
import com.everwell.iam.services.EntityService;
import com.everwell.iam.utils.Utils;
import com.everwell.iam.validators.EntityValidator;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

@RestController
public class MERMController {
    /**
     * MERMController Manages:
     * 1. Operations on event logs
     * 2. Operations on imei
     **/
    @Autowired
    private MERMHandler mermHandler;
    @Autowired
    private EntityService entityService;
    @Autowired
    private AdherenceService adherenceService;

    private static Logger LOGGER = LoggerFactory.getLogger(MERMController.class);

    @ApiOperation(
        value = "Process Merm Event",
        notes = "Accepts a MERM event and calculates adherence code based on that and adds it to the adherence string"
    )
    //sample post - sn=869587035813290&TU=211019110113
    @RequestMapping(value = "/v1/merm/event", method = RequestMethod.POST)
    public ResponseEntity<Response<List<Long>>> processEvent(@RequestBody MermEventRequest post) {
        post.validate();
        List<Long> incomingEventLogs = mermHandler.processIncomingEvent(post);
        Response<List<Long>> response = new Response<>(incomingEventLogs, "Event Registered!");
        LOGGER.info("[processEvent] response generated: " + response);
        return new  ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(
        value = "Fetch active/inactive IMEIs using entityIds"
    )
    @RequestMapping(value = "/v1/imei", method = RequestMethod.GET)
    public ResponseEntity<Response<List<ImeiResponse>>> getImei(String entityId, boolean active, @RequestHeader(name = "X-IAM-Client-Id") Long clientId) {
        LOGGER.info("[getImei] request received for: " + entityId);
        EntityValidator.validateEntityMappingMandatory(entityId);
        List<AccessMapping> accessMappings = new ArrayList<>();
        if(active)
            accessMappings.add(entityService.getActiveIamMappingByEntityId(clientId, entityId));
        else
            accessMappings.addAll(entityService.getAllIamMapping(clientId, entityId));

        if(Utils.isEmptyOrAllNull(accessMappings)) {
            LOGGER.error("[getImei] entity was not found:" + entityId);
            throw new NotFoundException("entity was not found");
        }
        List<ImeiMap> imeiMapList = mermHandler.getImei(
            accessMappings.stream().map(AccessMapping::getIamId).collect(Collectors.toList()),
            active
        );
        List<ImeiResponse> imeiPatientMap = imeiMapList
            .stream()
            .map(m -> new ImeiResponse(m.getImei(), entityId, m.getStartDate()))
            .collect(Collectors.toList());
        Response<List<ImeiResponse>> response = new Response<>(true, imeiPatientMap);
        LOGGER.info("[getImei] response generated: " + response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(
            value = "Fetch active IMEIs using list of entityIds"
    )
    @RequestMapping(value = "/v1/imei/bulk", method = RequestMethod.POST)
    public ResponseEntity<Response<List<ImeiResponse>>> getImeiBulk(@RequestBody GetBulkImeiRequest imeiRequest, @RequestHeader(name = "X-IAM-Client-Id") Long clientId) {
        imeiRequest.validate();
        List<String> entityIds = imeiRequest.getEntityIdList();
        LOGGER.info("[getImeiBulk] request received for: " + entityIds);

        List<AccessMapping> accessMappings = new ArrayList<>(entityService.getActiveIamMappingByEntityId(clientId, entityIds));
        if(Utils.isEmptyOrAllNull(accessMappings)) {
            LOGGER.error("[getImeiBulk]  No entities found:" + entityIds);
            throw new NotFoundException(" No entities found");
        }
        Map<Long, String> iamIdToEntityId = accessMappings.stream().collect(Collectors.toMap(AccessMapping::getIamId, AccessMapping::getEntityId));
        List<ImeiMap> imeiMapList = mermHandler.getImei(
                accessMappings.stream().map(AccessMapping::getIamId).collect(Collectors.toList()),
                false
        );
        List<ImeiResponse> imeiPatientMap = imeiMapList
                .stream()
                .map(m -> new ImeiResponse(m.getImei(), iamIdToEntityId.get(m.getIamId()), m.getStartDate()))
                .collect(Collectors.toList());
        Response<List<ImeiResponse>> response = new Response<>(true, imeiPatientMap);
        LOGGER.info("[getImeiBulk] response generated: " + response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/imei", method = RequestMethod.PUT)
    public ResponseEntity<Response<Long>> reallocateImei(@RequestBody ImeiEditRequest imeiEditRequest , @RequestHeader(name = "X-IAM-Client-Id") Long clientId) {
        imeiEditRequest.validate(imeiEditRequest);
        boolean isAllocated = mermHandler.isMERMAllocated(imeiEditRequest.getImei());
        if(isAllocated) {
            throw new ConflictException("IMEI is already allocated");
        }
        Registration registration = mermHandler.getActiveIam(imeiEditRequest.getEntityId(), clientId);
        adherenceService.updateUniqueIdentifier(imeiEditRequest.getImei(), registration.getId());
        mermHandler.deactivateIMEIByIamId(registration.getId(), Utils.getCurrentDate());
        ImeiMap imeiMap = new ImeiMap(imeiEditRequest.getImei(), registration.getId(), Utils.getCurrentDate());
        mermHandler.saveIMEINumbers(imeiMap);
        Response<Long> response = new Response<>(true, imeiMap.getId());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/imei/entity", method = RequestMethod.GET)
    public ResponseEntity<Response<List<ImeiResponse>>> getActiveEntityFromImei(String imei, @RequestHeader(name = "X-IAM-Client-Id") Long clientId) {
        LOGGER.info("[getActiveEntityFromImei] request received: " + imei);
        AccessMapping accessMapping = entityService.getIamMappingByIMEI(imei, clientId);
        if(Utils.isEmptyOrAllNull(Arrays.asList(accessMapping))) {
            LOGGER.error("[getActiveEntityFromImei] no active entity found for imei: "+ imei);
            throw new NotFoundException("entity was not found");
        }
        List<ImeiMap> imeiMap = mermHandler.getImei(Arrays.asList(accessMapping.getIamId()), true);

        List<ImeiResponse> imeiPatientMap = Arrays.asList(new ImeiResponse(imei, accessMapping.getEntityId(), imeiMap.get(0).getStartDate()));
        Response<List<ImeiResponse>> response = new Response<>(true, imeiPatientMap);
        LOGGER.info("[getActiveEntityFromImei] response generated: " + response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}

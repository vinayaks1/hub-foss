package com.everwell.iam.vendors.repositories;

import com.everwell.iam.vendors.models.db.VideoEntityMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

@Repository
public interface VideoEntityMappingRepository extends JpaRepository<VideoEntityMapping, Long> {

    VideoEntityMapping findByVideoIdAndIamId(String videoId, Long iamId);

    VideoEntityMapping findByVideoId(String videoId);

    List<VideoEntityMapping> findByIamIdIn(List<Long> iamIds);

    @Query(value = "select v from VideoEntityMapping v where v.iamId = :iamId and v.status in :statusList")
    List<VideoEntityMapping> findByIamIdForStatus(Long iamId, List<Integer> statusList);

    @Modifying
    @Transactional
    @Query("DELETE from VideoEntityMapping where iamId in :iamIds")
    void deleteAllByIamIdIn(Set<Long> iamIds);

}

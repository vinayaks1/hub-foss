package com.everwell.iam.filters;

import com.everwell.iam.handlers.AuthFailureHandler;
import com.everwell.iam.models.AuthenticationToken;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;

/**
 * This class is responsible for read the token from a custom header.
 * In our case we use the header "x-iam-auth-token" and "x-iam-client-id"
 */
public class AuthFilter extends AbstractAuthenticationProcessingFilter {

    private static final String X_IAM_CLIENT_ID = "X-IAM-Client-Id";
    private static final String X_IAM_AUTH_TOKEN = "X-IAM-Access-Token";

    public AuthFilter(RequestMatcher requestMatcher) {
        super(requestMatcher);
        this.setAuthenticationSuccessHandler((req, res, authentication) -> { });
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, final FilterChain chain) throws IOException, ServletException {
        final String token = getHeaderValue((HttpServletRequest) request, X_IAM_AUTH_TOKEN);
        final String clientId = getHeaderValue((HttpServletRequest) request, X_IAM_CLIENT_ID);

        //This filter only applies if the header is present
        if (StringUtils.isEmpty(token) || StringUtils.isEmpty(clientId)) {
            chain.doFilter(request, response);
            return;
        }

        this.setAuthenticationFailureHandler(new AuthFailureHandler());

        super.doFilter(request, response, chain);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse httpServletResponse) throws AuthenticationException, IOException, ServletException {
        final String tokenValue = getHeaderValue(request, X_IAM_AUTH_TOKEN);
        final String clientIdStr = getHeaderValue(request, X_IAM_CLIENT_ID);

        if (StringUtils.isEmpty(tokenValue) || StringUtils.isEmpty(clientIdStr)) {
            //Doing this check is kinda dumb because we check for it up above in doFilter
            //..but this is a public method and we can't do much if we don't have the header
            //also we can't do the check only here because we don't have the chain available
            return null;
        }

        AuthenticationToken token = new AuthenticationToken(tokenValue, Long.parseLong(clientIdStr));
        token.setDetails(authenticationDetailsSource.buildDetails(request));

        return this.getAuthenticationManager().authenticate(token);
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res, FilterChain chain, Authentication auth) throws IOException, ServletException {
        super.successfulAuthentication(req, res, chain, auth);
        // On success keep going on the chain
        chain.doFilter(req, res);
    }

    private String getHeaderValue(HttpServletRequest req, String headerVal) {
        //find the header which contains our token (ignore the header-name case)
        return Collections.list(req.getHeaderNames()).stream()
                .filter(header -> null != header)
                .filter(header -> header.toLowerCase().equalsIgnoreCase(headerVal.toLowerCase()))
                .map(header -> req.getHeader(header))
                .findFirst()
                .orElse(null);
    }
}

package com.everwell.iam.services.impl;

import com.everwell.iam.exceptions.UnauthorizedException;
import com.everwell.iam.services.AuthenticationService;
import com.everwell.iam.utils.AuthenticationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.security.spec.InvalidKeySpecException;
import java.util.Base64;
import java.util.Date;

@Service("authenticationService")
public class AuthenticationServiceImpl implements AuthenticationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationServiceImpl.class);

    @Override
    public String issueSecureToken(String password, String name) throws UnauthorizedException {
        // A combination of the name and a random string to keep it dynamic
        String salt = AuthenticationUtil.generateSalt(30);
        String accessTokenMaterial = name + salt;
        byte[] encryptedAccessToken = null;
        try {
            encryptedAccessToken = AuthenticationUtil.encrypt(password, accessTokenMaterial);
        } catch (InvalidKeySpecException ex) {
            LOGGER.error("[issueSecureToken] : ", ex);
            throw new UnauthorizedException("Faled to issue secure access token");
        }
        String encryptedAccessTokenBase64Encoded = Base64.getEncoder().encodeToString(encryptedAccessToken);
        return encryptedAccessTokenBase64Encoded;
    }

}

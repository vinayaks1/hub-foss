package com.everwell.iam.services.impl;

import com.everwell.iam.exceptions.ConflictException;
import com.everwell.iam.exceptions.NotFoundException;
import com.everwell.iam.exceptions.ValidationException;
import com.everwell.iam.models.db.AccessMapping;
import com.everwell.iam.models.http.responses.EntityResponse;
import com.everwell.iam.repositories.AccessMappingRepository;
import com.everwell.iam.repositories.RegistrationRepository;
import com.everwell.iam.services.EntityService;
import com.everwell.iam.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class EntityServiceImpl implements EntityService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EntityServiceImpl.class);

    @Autowired
    private AccessMappingRepository accessMappingRepository;

    @Autowired
    private RegistrationRepository registrationRepository;

    /**
     * This method is to be called only when it is ascertained that no such entityId-iamid-clientId duplicate exists
     * @param entityId
     * @param iamId
     * @param clientId
     * @return
     */
    @Override
    public EntityResponse registerEntity(String entityId, Long iamId, Long clientId) {
        AccessMapping newMapping = new AccessMapping(clientId, iamId, entityId);
        accessMappingRepository.save(newMapping);

        return new EntityResponse(iamId);
    }

    @Override
    public void validateIamMappingWithDate(Long clientId, String entityId, String startDate) {
        AccessMapping existsMapping = getActiveIamMappingUsingDate(clientId, entityId, startDate);
        if(null != existsMapping) {
            throw new ConflictException("active entity already exists");
        }
    }

    @Override
    public AccessMapping getActiveIamMappingByEntityId(Long clientId, String entityId) {
        AccessMapping mapping = accessMappingRepository.findActiveAccessMappingByEntityIdAndClientId(entityId, clientId);
        return mapping;
    }

    @Override
    public List<AccessMapping> getActiveIamMappingByEntityId(Long clientId, List<String> entityId) {
        List<AccessMapping> mapping = accessMappingRepository.getByClientIdAndEntityIdInAndActiveIsTrue(clientId, entityId);
        return mapping;
    }

    @Override
    public boolean validateActiveIamMappingByEntityId(Long clientId, String entityId) {
        List<AccessMapping> existsMapping = getActiveIamMappingByEntityId(clientId, new ArrayList<>(Arrays.asList(entityId)));
        return (null != existsMapping && existsMapping.size() > 0);
    }

    @Override
    public boolean validateActiveIamMappingByEntityId(Long clientId, List<String> entityIds) {
        List<AccessMapping> existsMapping = getActiveIamMappingByEntityId(clientId, entityIds);
        return (null != existsMapping && existsMapping.size() >= entityIds.size());
    }

    @Override
    public List<AccessMapping> getIamMappingByEntityId(Long clientId, String entityId) {
        return getIamMappingByEntityId(clientId, new ArrayList<>(Arrays.asList(entityId)));
    }

    @Override
    public List<AccessMapping> getIamMappingByEntityId(Long clientId, List<String> entityId) {
        List<AccessMapping> mapping = accessMappingRepository.getAllByEntityIdInAndClientId(entityId, clientId);
        return mapping;
    }

    @Override
    public AccessMapping getIamMappingByIMEI(String imei, Long clientId) {
        return  accessMappingRepository.findEntityIdByIMEIAndClientId(imei, clientId);
    }

    @Override
    public void validateNoActiveIamMappingByEntityId(Long clientId, String entityId) {
        List<AccessMapping> accessMappings = getIamMappingByEntityId(clientId, entityId);
        if(null != accessMappings && accessMappings.size() > 0) {
            if (accessMappings.stream().anyMatch(AccessMapping::isActive))
                throw new ConflictException("active entity exists, cannot reopen it!");
        } else {
            throw new NotFoundException("entity was not found!");
        }
    }

    @Override
    public AccessMapping getActiveIamMapping(Long clientId, Long iamId) {
        AccessMapping mapping = accessMappingRepository.findActiveAccessMappingByIamIdAndClientId(iamId, clientId);
        return mapping;
    }

    @Override
    public boolean validateActiveIamMapping(Long clientId, Long iamId) {
        AccessMapping existsMapping = getActiveIamMapping(clientId, iamId);
        return (null != existsMapping);
    }

    @Override
    public AccessMapping getIamMapping(Long clientId, Long iamId) {
        List<Long> ids = new ArrayList<>();
        ids.add(iamId);
        List<AccessMapping> accessMappings = getIamMapping(clientId, ids);
        return accessMappings.size() > 0 ? accessMappings.get(0) : null;
    }

    @Override
    public List<AccessMapping> getIamMapping(Long clientId, List<Long> iamId) {
        List<AccessMapping> mappings = accessMappingRepository.getByIamIdInAndClientId(iamId, clientId);
        return mappings;
    }

    @Override
    public boolean validateMapping(Long iamId, Long clientId) {
        List<Long> ids = new ArrayList<>();
        ids.add(iamId);
        return validateMapping(ids, clientId);
    }

    @Override
    public boolean validateMapping(List<Long> iamId, Long clientId) {
        List<AccessMapping> mappings = getIamMapping(clientId, iamId);
        return (null !=  mappings && mappings.size() == iamId.size());
    }

    @Override
    public boolean toggleActiveStatus(Long iamId, Long clientId, boolean active) {
        AccessMapping accessMapping = getIamMapping(clientId, iamId);
        //if state is what is was before, do nothing and return true
        if (active != accessMapping.isActive()) {
            accessMapping.setActive(active);
            accessMappingRepository.save(accessMapping);
        }
        return true;
    }

    @Override
    public AccessMapping getActiveIamMappingUsingDate(Long clientId, String entityId, String startDate) {
        try {
            Date date = Utils.convertStringToDate(startDate);
            return accessMappingRepository.findActiveMappingByEntityIdAndClientIdAndDate(entityId, clientId, date);
        } catch (ParseException e) {
            throw new ValidationException("date with invalid format");
        }
    }

    @Override
    public List<AccessMapping> getAllIamMapping(Long clientId, String entityId) {
        List<AccessMapping> accessMappingList = accessMappingRepository.getAllByEntityIdAndClientId(entityId, clientId);
        return accessMappingList;
    }

    @Override
    public Map<String, List<AccessMapping>> getMultiEntityMapping(List<String> entityIdList) {
        Map<String, List<AccessMapping>> multiEntityMap = new HashMap<>();
        List<AccessMapping> mappingList = accessMappingRepository.findByEntityIdIn(entityIdList);
        if(!CollectionUtils.isEmpty(mappingList)) {
            multiEntityMap = mappingList.stream()
                    .collect(Collectors.groupingBy(AccessMapping::getEntityId));
        }
        return multiEntityMap;
    }

    @Override
    public List<String> getEntityByIamIds(List<Long> iamIds, Long clientId) {
        List<String> entityIds = accessMappingRepository.getEntityIdByIamIdInAndClientIdAndActiveIsTrue(iamIds, clientId);
        return entityIds;
    }

    @Override
    public void deleteAccessMappingByIamIds(List<Long> iamIdsList) {
        accessMappingRepository.deleteAllByIamIdInCustom(iamIdsList);
    }

}

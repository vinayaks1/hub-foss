package com.everwell.iam.services;

import com.everwell.iam.enums.AuthorizedRole;
import com.everwell.iam.models.AuthenticationToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import java.util.Collections;


public abstract class AuthProvider implements AuthenticationProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthProvider.class);

    protected abstract void validate(String token, Long id);

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        final AuthenticationToken authenticationToken = (AuthenticationToken) authentication;
        final String token = authenticationToken.getToken();
        final Long id = authenticationToken.getId();

        validate(token, id);
        return new AuthenticationToken(token, id, Collections.singletonList(AuthorizedRole.AUTHORIZED));
    }

    @Override
    public boolean supports(Class<?> authentication) {
        //this class is only responsible for AuthTokenContainers
        return AuthenticationToken.class.isAssignableFrom(authentication);
    }
}

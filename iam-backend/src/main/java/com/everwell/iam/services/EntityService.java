package com.everwell.iam.services;

import com.everwell.iam.exceptions.ConflictException;
import com.everwell.iam.models.db.AccessMapping;
import com.everwell.iam.models.db.AdherenceTechnology;
import com.everwell.iam.models.http.responses.EntityResponse;

import java.util.List;
import java.util.Map;

public interface EntityService {

    EntityResponse registerEntity(String entityId, Long iamId, Long clientId);

    //considers 'active = true' while validating
    AccessMapping getActiveIamMappingByEntityId(Long clientId, String entityId);

    boolean toggleActiveStatus(Long iamId, Long clientId, boolean active);

    AccessMapping getActiveIamMappingUsingDate(Long clientId, String entityId, String startDate);

    AccessMapping getIamMapping(Long clientId, Long iamId);

    List<AccessMapping> getIamMapping(Long clientId, List<Long> iamId);

    //does not consider 'active = true' while validating
    boolean validateMapping(Long iamId, Long clientId);

    boolean validateMapping(List<Long> iamId, Long clientId);

    boolean validateActiveIamMapping(Long clientId, Long iamId);

    AccessMapping getActiveIamMapping(Long clientId, Long iamId);

    List<AccessMapping> getAllIamMapping(Long clientId, String entityId);

    List<AccessMapping> getActiveIamMappingByEntityId(Long clientId, List<String> entityId);

    boolean validateActiveIamMappingByEntityId(Long clientId, String entityId);

    boolean validateActiveIamMappingByEntityId(Long clientId, List<String> entityId);

    List<AccessMapping> getIamMappingByEntityId(Long clientId, String entityId);

    List<AccessMapping> getIamMappingByEntityId(Long clientId, List<String> entityId);

    void validateNoActiveIamMappingByEntityId(Long clientId, String entityId);

    void validateIamMappingWithDate(Long clientId, String entityId, String startDate);

    Map<String, List<AccessMapping>> getMultiEntityMapping(List<String> entityIdList);

    List<String> getEntityByIamIds(List<Long> iamIds, Long clientId);

    void deleteAccessMappingByIamIds(List<Long> iamIdsList);

    AccessMapping getIamMappingByIMEI(String imei, Long clientId);
}

package com.everwell.iam.services;

import com.everwell.iam.repositories.CAccessRepository;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Service
public class ClientEventFlowConfigMap {

    @Getter
    private Map<Long, Long> clientConfigMap = new HashMap<>();

    @Autowired
    CAccessRepository cAccessRepository;

    @PostConstruct
    public void initialise() {
        cAccessRepository
            .findAll()
            .forEach(cAccess -> clientConfigMap.put(cAccess.getId(), cAccess.getEventFlowId()));
    }
}

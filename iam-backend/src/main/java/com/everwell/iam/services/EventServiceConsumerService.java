package com.everwell.iam.services;

import org.springframework.amqp.core.Message;

import java.io.IOException;

public interface EventServiceConsumerService {
    void processCallConsumer(Message message) throws IOException;
    void processRegenerateEntity(Message message) throws IOException;
    void registerEntityConsumer(Message message) throws IOException;
    void processMermEventConsumer(Message message) throws IOException;
}

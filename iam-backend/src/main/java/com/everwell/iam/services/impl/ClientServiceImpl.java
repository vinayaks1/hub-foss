package com.everwell.iam.services.impl;

import com.everwell.iam.constants.Constants;
import com.everwell.iam.exceptions.ConflictException;
import com.everwell.iam.exceptions.NotFoundException;
import com.everwell.iam.exceptions.ServiceException;
import com.everwell.iam.exceptions.ValidationException;
import com.everwell.iam.models.db.CAccess;
import com.everwell.iam.models.http.requests.RegisterClientRequest;
import com.everwell.iam.models.http.responses.ClientResponse;
import com.everwell.iam.repositories.CAccessRepository;
import com.everwell.iam.services.AuthenticationService;
import com.everwell.iam.services.ClientService;
import com.everwell.iam.utils.AuthenticationUtil;
import com.everwell.iam.utils.CacheUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.security.spec.InvalidKeySpecException;
import java.util.Date;

@Service
public class ClientServiceImpl implements ClientService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientServiceImpl.class);

    @Autowired
    private CAccessRepository accessRepository;

    @Autowired
    @Qualifier("authenticationService")
    private AuthenticationService authenticationService;

    @Override
    public ClientResponse registerClient(RegisterClientRequest clientRequest) {
        String salt = AuthenticationUtil.generateSalt(30);

        CAccess existingAccess = accessRepository.findByName(clientRequest.getName());
        if(null != existingAccess) {
            throw new ConflictException("Client already exists");
        }

        // Create encrypted password
        String encryptedPassword;
        try {
            encryptedPassword = AuthenticationUtil.
                    generateSecurePassword(clientRequest.getPassword(), salt);
        } catch (InvalidKeySpecException ex) {
            throw new ServiceException(ex.getLocalizedMessage());
        }
        // Issue a new Access token
        String secureUserToken = authenticationService.issueSecureToken(clientRequest.getPassword(),clientRequest.getName());

        Long refreshTime = (new Date()).getTime() + Constants.REFRESH_EXPIRY;

        CAccess access = new CAccess(clientRequest.getName(), encryptedPassword, secureUserToken, refreshTime);
        accessRepository.save(access);

        ClientResponse response = new ClientResponse(access);
        response.setPassword(clientRequest.getPassword());

        return response;
    }

    /**
     * Do not use this method directly.
     * Make sure all the entities for this client is removed before we do this
     * @param id
     * @return
     */
    @Override
    public boolean deleteClient(Long id) {
        accessRepository.deleteById(id);
        return true;
    }

    @Override
    public ClientResponse getClient(Long id) {
        if(null == id) {
            throw new ValidationException("Client Id cannot be null");
        }
        LOGGER.debug("[getClient] client fetch requested for id " + id);
        CAccess client = getClientById(id);
        if(null == client) {
            throw new NotFoundException("access not found");
        }
        Long currentTimeMillis = (new Date()).getTime();
        if (client.getNextRefresh() > new Date().getTime()){
            long timeToExpiry = client.getNextRefresh() - currentTimeMillis;
            CacheUtils.putIntoCache(Constants.CACHE_ACCESS_TOKEN_PREFIX+id, client.getAccessToken(), timeToExpiry/1000);
        }

        // Generate new access token if refresh expiry time is passed
        if(client.getNextRefresh() < new Date().getTime()) {
            // using encrypted password to generate new access token
            String secureUserToken = authenticationService.issueSecureToken(client.getPassword(), client.getName());
            Long refreshTime = (new Date()).getTime() + Constants.REFRESH_EXPIRY;
            client.setAccessToken(secureUserToken);
            client.setNextRefresh(refreshTime);
            client.setUpdatedDate(new Date());
            accessRepository.save(client);
            CacheUtils.putIntoCache(Constants.CACHE_ACCESS_TOKEN_PREFIX+id, secureUserToken, refreshTime/1000);
        }

        return new ClientResponse(client);
    }

    @Override
    public CAccess getClientById(Long id) {
        return accessRepository.findById(id).orElse(null);
    }
}

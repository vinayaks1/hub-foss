package com.everwell.iam.services;

import com.everwell.iam.models.db.CAccess;
import com.everwell.iam.models.http.requests.RegisterClientRequest;
import com.everwell.iam.models.http.responses.ClientResponse;

public interface ClientService {

    ClientResponse registerClient(RegisterClientRequest clientRequest);

    boolean deleteClient(Long id);

    ClientResponse getClient(Long id);

    CAccess getClientById(Long id);
}

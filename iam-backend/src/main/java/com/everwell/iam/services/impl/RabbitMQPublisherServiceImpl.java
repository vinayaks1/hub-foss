package com.everwell.iam.services.impl;

import com.everwell.iam.models.dto.eventstreaming.EventStreamingDto;
import com.everwell.iam.services.RabbitMQPublisherService;
import com.everwell.iam.utils.Utils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

@Service
public class RabbitMQPublisherServiceImpl implements RabbitMQPublisherService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RabbitMQPublisherServiceImpl.class);

   @Autowired
   private RabbitTemplate template;

    @Override
    public <T> void send(EventStreamingDto<T> message, String exchange) {
        if(message.getField() != null){
            this.template.convertAndSend(exchange, message.getEventName(), Utils.asJsonString(message));
            LOGGER.info("[publish] eventName: " + message.getEventName() + " message: "+ message);
        }
    }

    @Override
    public void send(String message, String routingKey, String exchange, Map<String, Object> map) {
        send(message, routingKey, exchange, null, map);
    }

    @Override
    public void send(String message, String routingKey, String exchange, String expirationTime, Map<String, Object> map) {
        if(!StringUtils.isEmpty(message)){
            this.template.convertAndSend(exchange, routingKey, message, m -> {
                map.forEach((k, v) -> m.getMessageProperties().setHeader(k, v));
                if (!StringUtils.isEmpty(expirationTime))
                    m.getMessageProperties().setExpiration(expirationTime);
                return m;
            });
            LOGGER.info("[publish] eventName: " + routingKey + " message: "+ message + " headers: " + map);
        }
    }
}

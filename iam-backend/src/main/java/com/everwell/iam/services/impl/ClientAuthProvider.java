package com.everwell.iam.services.impl;

import com.everwell.iam.constants.Constants;
import com.everwell.iam.models.db.CAccess;
import com.everwell.iam.repositories.CAccessRepository;
import com.everwell.iam.services.AuthProvider;
import com.everwell.iam.utils.CacheUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;

import java.util.Date;

public class ClientAuthProvider extends AuthProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientAuthProvider.class);

    @Autowired
    private CAccessRepository cAccessRepository;

    @Override
    protected void validate(String token, Long id) {
        boolean isVerified = false;
        String cacheKey = Constants.CACHE_ACCESS_TOKEN_PREFIX + id;
        if (CacheUtils.hasKey(cacheKey)) {
            String secureUserToken = CacheUtils.getFromCache(cacheKey);
            isVerified = (secureUserToken.equals(token));
        } else {
            CAccess access = cAccessRepository.findById(id).orElse(null);
            Long currentTimeMillis = (new Date()).getTime();
            if (access != null && access.getAccessToken().equals(token) && access.getNextRefresh() > currentTimeMillis) {
                isVerified = true;
                long timeToExpiry = access.getNextRefresh() - currentTimeMillis;
                CacheUtils.putIntoCache(cacheKey, token, timeToExpiry/1000);
            }
        }
        if(!isVerified) {
            LOGGER.warn("[authenticate] Access denied for " + id + " with token " + token);
            throw new BadCredentialsException("Invalid token - " + token);
        }
    }
}

package com.everwell.iam.services;

import com.everwell.iam.exceptions.UnauthorizedException;

public interface AuthenticationService {
    String issueSecureToken(String password, String name) throws UnauthorizedException;
}

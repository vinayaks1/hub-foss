package com.everwell.iam.services;

import com.everwell.iam.enums.AdherenceCodeEnum;
import com.everwell.iam.enums.AverageAdherenceIntervals;
import com.everwell.iam.handlers.AdherenceTechHandler;
import com.everwell.iam.models.db.AccessMapping;
import com.everwell.iam.models.db.Registration;
import com.everwell.iam.models.dto.*;
import com.everwell.iam.models.http.requests.AdherenceGlobalConfigRequest;
import com.everwell.iam.models.http.requests.RecordAdherenceRequest;
import com.everwell.iam.models.http.requests.EntityRequest;
import com.everwell.iam.models.http.responses.*;
import com.everwell.iam.repositories.RegistrationShortRepo;

import java.util.*;
import java.util.function.BiFunction;

public interface AdherenceService {

    boolean stopAdherence(Long iamId, Date endDate);

    boolean stopAdherence(Long iamId);

    AdherenceResponse getAdherence(Long iamId, Boolean logsRequired);

    AdherenceResponse getAdherence(Registration registration, Boolean logsRequired);

    PositiveAdherenceDto countPositiveCallsForToday(List<String> entityIdList, boolean fallback);

    List<AdherenceResponse> getAdherenceBulk(Map<String, List<AccessMapping>> accessMappingToEntityMap, List<RangeFilters> filters);

    EntityDoseCountDto generateEntityDoseCountStats(EntityAdherenceDto entityAdherence);

    void updateCumulativeDoseCountStats(CumulativeDoseCountDto cumulativeDoseCounts ,CumulativeDoseCountDto entityDoseCounts);

    CumulativeDoseCountDto calculateAdherenceDays(List<EntityAdherenceDto> entityAdherenceList, List<AvgAdherenceResponse> avgAdherenceResponseList);

    AllAvgAdherenceResponse getAverageAdherence(Map<String, List<AccessMapping>> accessMappingToEntityMap, boolean patientDetailsRequired, List<AverageAdherenceIntervals> intervals);

    EntityAdherenceDto getEntityAdherence(String entityId, List<RegistrationShortRepo> registrationList);

    <T> AdherenceCacheDataDto<T> getAdherenceFromCache(List<String> entityIdList, String prefix, BiFunction<String, Set<String>, T> func);

    AdherenceResponse formatCacheForAdherence(String cacheEntry, Set<String> allIdsSet);

    EntityDoseCountDto formatCacheForAvgAdherence(String cacheEntry, Set<String> allIdsSet);

    AdherenceResponse getAllAdherence(List<Long> iamIdList, Boolean logsRequired);

    PositiveAdherenceDto countPositiveCallsForToday(List<String> entityIdList);

    List<AdherenceResponse> filterList(List<AdherenceResponse> adherenceResponseList, List<RangeFilters> filters);

    List<AdherenceResponse> filterList(List<AdherenceResponse> adherenceResponseList, RangeFilters filters);

    RecordAdherenceResponse recordAdherence(RecordAdherenceRequest recordAdherenceRequest, List<Long> iamIdList);

    Long createIam(EntityRequest entityRequest);

    boolean updateIam(EntityRequest entityRequest, Long iamId);

    void validateRegistrationsForDate(List<Registration> registrations, String date);

    void regenerateAdherenceString(List<Long> iamIds);

    EntityResponse updateUniqueIdentifier(String phone, Long iamId);

    Registration updateEndDate(Registration registration, Date endDate);

    List<Registration> getRegistrationsByIamId(List<Long> iamIds);

    Registration getLastClosedRegistration(List<Long> iamIds);

    List<Long> getIamIdsByAdherenceTech(Long adherenceType, Long size);

    AdherenceTechHandler getAdherenceTechHandler(Registration registration);

    Registration reopenEntity(EntityRequest entityRequest, List<Long> iamIds);

    boolean activeScheduleMappingExistsForEntityIdAndClientId(Long clientId, String entityId, Long scheduleTypeId);
    
    List<Long> getIamIdsByAdherenceTechAndUniqueIdentifier(String uniqueIdentifier, Long adhtechId);

    boolean hasScheduleMappings(Long scheduleTypeId);

    String getEntityAdherenceString(List<AdherenceData> adherentDataList);

    TreeMap<Date, AdherenceCodeEnum> getOrderedAdherenceMap(Map<String, Character> adherenceMap);

    List<Long> deleteIamIdsByStopDate(List<Long> iamIds, Date stopDate);

    List<String> getAllConfigNames();

    List<AdherenceCodeConfigResponse> getAdherenceColorCodeConfig();

    List<GenericConfigResponse> getAdherenceTechConfig();

    List<GenericConfigResponse> getScheduleTypeConfig();

    AdherenceGlobalConfigResponse getAdherenceGlobalConfig(AdherenceGlobalConfigRequest configRequest);

    boolean isDoseTimeSupportedForScheduleType(Long scheduleTypeId);

    List<DoseTimeData> getDoseTimeForEntity(Long iamId, Long scheduleTypeId);
}

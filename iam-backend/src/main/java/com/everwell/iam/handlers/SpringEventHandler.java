package com.everwell.iam.handlers;

import com.everwell.iam.constants.Constants;
import com.everwell.iam.constants.RabbitMQConstants;
import com.everwell.iam.enums.AdherenceTech;
import com.everwell.iam.enums.CacheAdherenceStatusToday;
import com.everwell.iam.enums.DosageTypeEnum;
import com.everwell.iam.enums.RabbitMQEventEnum;
import com.everwell.iam.models.db.AccessMapping;
import com.everwell.iam.models.db.Registration;
import com.everwell.iam.models.dto.eventstreaming.EntityDetailsDto;
import com.everwell.iam.models.dto.eventstreaming.EventStreamingDto;
import com.everwell.iam.models.dto.springevents.AnyAdherenceEvent;
import com.everwell.iam.models.dto.springevents.EpisodeEvent;
import com.everwell.iam.models.http.requests.EntityRequest;
import com.everwell.iam.models.http.requests.ProcessUnregisteredPhoneNumberRequest;
import com.everwell.iam.models.http.requests.RegenerateAdherenceRequest;
import com.everwell.iam.models.http.requests.RegenerateEventRequest;
import com.everwell.iam.models.http.responses.EpisodeFieldsReponse;
import com.everwell.iam.repositories.AccessMappingRepository;
import com.everwell.iam.repositories.RegistrationRepository;
import com.everwell.iam.services.ClientService;
import com.everwell.iam.services.RabbitMQPublisherService;
import com.everwell.iam.utils.CacheUtils;
import com.everwell.iam.utils.Utils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
public class SpringEventHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(SpringEventHandler.class);

  @Autowired
  RegistrationRepository registrationRepository;

  @Autowired
  AccessMappingRepository accessMappingRepository;

  @Autowired
  RabbitMQPublisherService rabbitMQPublisherService;

  @Autowired
  ClientService clientService;

  @Autowired
  private TaskScheduler taskScheduler;

  @Value("${iam.access.mapping.delay}")
  private Long delayInSeconds;

  @Autowired
  private AdhTechHandlerMap adherenceTechnologyHandlerMap;

  @Async
  @EventListener
  public void anyAdherenceEvent(AnyAdherenceEvent event) {
    if (event.isEventDelayed())
      scheduleEvent(() -> anyAdherenceEventImpl(event));
    else
      anyAdherenceEventImpl(event);
  }
  
  @EventListener
  public void regenerateAdherenceEvent(RegenerateAdherenceRequest event) {
    List<Long> iamIdList = event.getIamIdList();
    for (Long iamId : iamIdList){
      EventStreamingDto<RegenerateEventRequest> regenerateEvent = new EventStreamingDto<>(RabbitMQConstants.REGENERATE_ADHERENCE, new RegenerateEventRequest(iamId));
      Map<String, Object> headers = new HashMap<>();
      rabbitMQPublisherService.send(Utils.asJsonString(regenerateEvent), RabbitMQEventEnum.REGENERATE_ADHERENCE_EVENT.getEventName(), RabbitMQEventEnum.REGENERATE_ADHERENCE_EVENT.getExchange(), headers);
    }
  }

  /**
   * Event listener for event of receiving call from Unregistered phone number
   * Listener will publish Rabbit MQ event for the same which can be handled at client end
   * @param event - Unregistered Phone number request containing caller number
   */
  @Async
  @EventListener
  public void receivedCallFromUnregisteredPhoneEvent(ProcessUnregisteredPhoneNumberRequest event) {
    String callerNumber = event.getCallerNumber();
    if(null != callerNumber){
      EventStreamingDto<ProcessUnregisteredPhoneNumberRequest> unregisteredPhoneNumberRequest = new EventStreamingDto<>(RabbitMQConstants.UNREGISTERED_PHONE_NUMBER, event);
      Map<String, Object> headers = new HashMap<>();
      // Invoke Rabbit MQ publisher service for publishing event
      rabbitMQPublisherService.send(Utils.asJsonString(unregisteredPhoneNumberRequest), RabbitMQEventEnum.UNREGISTERED_PHONE_NUMBER_EVENT.getEventName(), RabbitMQEventEnum.UNREGISTERED_PHONE_NUMBER_EVENT.getExchange(), headers);
    }
  }

  public void lastDosageChangeEventImpl(AnyAdherenceEvent event, String entityId, Long clientId) {
    if (event.isUpdateLastDosage() || event.isUpdateLastMissedDosage()) {
      List<AccessMapping> accessMappingsByEntityId = accessMappingRepository.getAllByEntityIdAndClientId(entityId, clientId);
      List<Registration> registrations = registrationRepository.findByIdIn(
          accessMappingsByEntityId.stream().map(AccessMapping::getIamId).collect(Collectors.toList())
      );
      if(event.isUpdateLastDosage()) {
        publishLastDosage(entityId, registrations, clientId,DosageTypeEnum.LAST_DOSAGE);
      }
      
      if(event.isUpdateLastMissedDosage()) {
        publishLastDosage(entityId, registrations, clientId,DosageTypeEnum.LAST_MISSED_DOSAGE);
      }
    }
  }


  public void anyAdherenceEventImpl(AnyAdherenceEvent event) {
    AccessMapping accessMapping = getAccessMapping(event.getRegistration().getId());
    if (accessMapping == null) return;
    String entityId = accessMapping.getEntityId();
    invalidateCache(entityId);
    insertAdherenceStatusInCache(event.getAdherenceStatusType(), entityId, event.getRegistration());
    lastDosageChangeEventImpl(event, entityId, accessMapping.getClientId());
    updateAdherenceString(event);
    cacheNullDateEntity(entityId,accessMapping.getClientId().toString(),event);
  }

  public void updateAdherenceString(AnyAdherenceEvent event) {
    if (event.isUpdateAdherenceString()) {
      //each tech pocket(99d, merm, etc) will call this, so no point in doing regen for all switch pockets
      AdherenceTechHandler handler = adherenceTechnologyHandlerMap.getHandler(event.getRegistration().getAdTechId());
      Registration registration = event.getRegistration();
      String adherenceString = handler.generateAdherenceString(registration.getId(), registration.getStartDate(), registration.getEndDate(), registration.getScheduleTypeId());
      registration.setAdherenceString(adherenceString);
      //event emitting function takes care of the last dosage
      registrationRepository.save(registration);
    }
  }

  public void insertAdherenceStatusInCache(CacheAdherenceStatusToday status, String entityId, Registration registration) {
    if (status == null){
      return;
    } else if (status == CacheAdherenceStatusToday.NO_INFO) {
      CacheUtils.deleteKey(Constants.CACHE_ADH_STATUS_TODAY_PREFIX + entityId);
      LOGGER.info("Deleted from cache, key = "+Constants.CACHE_ADH_STATUS_TODAY_PREFIX + entityId+ ", entity_id = "+entityId);
    } else{
      CacheUtils.putIntoCache(Constants.CACHE_ADH_STATUS_TODAY_PREFIX + entityId, status.getStatus(), Utils.timeToEntityEndDate(registration.getStartDate(), TimeUnit.SECONDS));
      LOGGER.info("Inserted into cache, key = "+Constants.CACHE_ADH_STATUS_TODAY_PREFIX + entityId+ ", value = "+status.getStatus() +" with time "+Utils.timeToEntityEndDate(registration.getStartDate(), TimeUnit.SECONDS));
    }
  }

  private void invalidateCache(String entityId) {
      CacheUtils.deleteKey(Constants.CACHE_USER_PREFIX + entityId);
      CacheUtils.deleteKey(Constants.CACHE_AVG_ADHERENCE_PREFIX + entityId);
  }

  private void publishLastDosage(String entityId, List<Registration> registrations, Long clientId,DosageTypeEnum dosageType) {
    boolean external = clientService.getClientById(clientId).isExternal();
    if (!external) {
      Optional<Date> maxDateOptional = Optional.empty();
      Map<String, Object> fieldsToUpdate = new HashMap<>();
      fieldsToUpdate.put(Constants.ID, entityId);
      if (dosageType.equals(DosageTypeEnum.LAST_DOSAGE)) {
        maxDateOptional = registrations.stream().filter(f -> f.getLastDosage() != null).map(Registration::getLastDosage).max(Date::compareTo);
        if(maxDateOptional.isPresent()) {
          fieldsToUpdate.put(Constants.LAST_DOSAGE, Utils.getFormattedDate(maxDateOptional.orElse(null)));
        }
      } else if (dosageType.equals(DosageTypeEnum.LAST_MISSED_DOSAGE)) {
        maxDateOptional = registrations.stream().filter(f -> f.getLastMissedDosage() != null).map(Registration::getLastMissedDosage).max(Date::compareTo);
        if(maxDateOptional.isPresent()) {
          fieldsToUpdate.put(Constants.LAST_DOSAGE, Utils.getFormattedDate(maxDateOptional.orElse(null)));
        }
      }
      Date maxDate = maxDateOptional.orElse(null);
      EntityDetailsDto lastDosage = new EntityDetailsDto(entityId, dosageType, maxDate);
      EventStreamingDto<EntityDetailsDto> event = new EventStreamingDto<>(RabbitMQConstants.HEADERS_EXCHANGE, lastDosage);
      EventStreamingDto<EpisodeFieldsReponse> episodeEvent = new EventStreamingDto<>(RabbitMQConstants.EX_DIRECT, new EpisodeFieldsReponse(Constants.MY_MODULE, fieldsToUpdate));
      Map<String, Object> headers = new HashMap<>();
      headers.put(Constants.RMQ_HEADER_CLIENT_ID, String.valueOf(clientId));
      headers.put(RabbitMQConstants.CLIENT_ID_CAMEL_CASE, String.valueOf(clientId));
      rabbitMQPublisherService.send(Utils.asJsonString(event), RabbitMQEventEnum.LAST_DOSAGE_EVENT.getEventName(), RabbitMQEventEnum.LAST_DOSAGE_EVENT.getExchange(), headers);
      rabbitMQPublisherService.send(Utils.asJsonString(episodeEvent), RabbitMQConstants.Q_EPISODE_UPDATE_TRACKER, RabbitMQConstants.EX_DIRECT, headers);
    }
  }

  /* Method to schedule an event to be fired in some time */
  public void scheduleEvent(Runnable runnable) {
    taskScheduler.schedule(
        runnable,
        DateUtils.addSeconds(Utils.getCurrentDate(), delayInSeconds.intValue())
    );
  }

  /* Methods to obtain entity Id using IamId from database */
  private AccessMapping getAccessMapping(Long iamId) {
    return getAccessMapping(iamId, 0);
  }

  private AccessMapping getAccessMapping(Long iamId, int retry) {
    List<AccessMapping> accessMappingByIamId = accessMappingRepository.getByIamId(Arrays.asList(iamId));
    if (CollectionUtils.isEmpty(accessMappingByIamId)) {
      if (retry < 3) {
        getAccessMapping(iamId, retry + 1);
      } else {
        LOGGER.error("[updateAdherenceMapEventDelayed] Entity Id does not getKeys for iam id - " + iamId);
      }
      return null;
    }
    return accessMappingByIamId.get(0);
  }

  private void cacheNullDateEntity(String entityId,String clientId, AnyAdherenceEvent event) {
      if(event.isCacheNullDateEntity()) {
        CacheUtils.putSetIntoCache(Constants.CACHE_NULL_DATE_ENTITY_PREFIX + clientId, entityId);
      }
  }

  @EventListener
  public void emitEventForEpisode(EpisodeEvent episodeEvent) {
    boolean external = clientService.getClientById(episodeEvent.getEntityRequest().getClientId()).isExternal();
    if (!external) {
      emitEventForEpisode(episodeEvent.getRegistration(), episodeEvent.getEntityRequest());
    }
  }

  private void emitEventForEpisode(Registration registration, EntityRequest entityRequest) {
    Map<String, Object> fieldsToUpdate = new HashMap<>();
    fieldsToUpdate.put(Constants.MONITORING_METHOD, AdherenceTech.getMatch(registration.getAdTechId()).getTechKey());
    fieldsToUpdate.put(Constants.LAST_DOSAGE, registration.getLastDosage());
    fieldsToUpdate.put(Constants.LAST_MISSED_DOSAGE, registration.getLastMissedDosage());
    fieldsToUpdate.put(Constants.ID, entityRequest.getEntityId());
    EventStreamingDto<EpisodeFieldsReponse> eventStreamingDto = new EventStreamingDto<>(
            RabbitMQConstants.Q_EPISODE_UPDATE_TRACKER, new EpisodeFieldsReponse(Constants.MY_MODULE, fieldsToUpdate)
    );
    Map<String, Object> headers = new HashMap<>();
    headers.put(RabbitMQConstants.CLIENT_ID_CAMEL_CASE, entityRequest.getClientId());
    rabbitMQPublisherService.send(Utils.asJsonString(eventStreamingDto), RabbitMQConstants.Q_EPISODE_UPDATE_TRACKER, RabbitMQConstants.EX_DIRECT, headers);
  }

}

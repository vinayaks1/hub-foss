package com.everwell.iam.handlers.impl;

import com.everwell.iam.enums.AdherenceCodeEnum;
import com.everwell.iam.enums.ScheduleTypeEnum;
import com.everwell.iam.exceptions.ValidationException;
import com.everwell.iam.handlers.ScheduleHandler;
import com.everwell.iam.models.db.AdherenceStringLog;
import com.everwell.iam.models.db.Schedule;
import com.everwell.iam.models.db.ScheduleMap;
import com.everwell.iam.models.db.ScheduleTimeMap;
import com.everwell.iam.models.dto.DoseTimeData;
import com.everwell.iam.models.dto.ScheduleSensitivity;
import com.everwell.iam.utils.Utils;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.security.InvalidParameterException;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
public class MultiFreqWeeklyScheduleHandler extends ScheduleHandler {
    @Override
    public ScheduleTypeEnum getScheduleTypeEnumerator() {
        return ScheduleTypeEnum.MULTI_FREQUENCY;
    }

    @Override
    public void validateNoOverlapSchedule(Schedule schedule, ScheduleSensitivity sensitivity) {

    }

    @Override
    public String getScheduleValue(String scheduleString) {
        return null;
    }

    @Override
    protected String getScheduleString(String scheduleValue) {
        return null;
    }

    @Override
    public boolean shouldTakeDose(Long iamId, Date calledDate) {
        return false;
    }

    @Override
    public boolean shouldTakeDoseForSchedule(ScheduleMap scheduleMap, Date calledDate) {
        return false;
    }

    @Override
    public boolean shouldTakeDoseForSchedule(ScheduleMap scheduleMap, Schedule schedule, Date calledDate) {
        return false;
    }

    @Override
    public boolean shouldTakeDose(List<ScheduleMap> scheduleMaps, Map<Long, Schedule> idScheduleMap, Date calledDate) {
        return false;
    }

    @Override
    public char computeAdherenceCodeForDay(List<ScheduleMap> scheduleMaps, Map<Long, Schedule> idScheduleMap, Date calledDate) {
        return 0;
    }

    @Override
    public Date getDateToAttribute(List<ScheduleMap> scheduleMaps, Map<Long, Schedule> idScheduleMap, Date calledDate) {
        return null;
    }

    private ScheduleMap getActiveScheduleMapForDate(List<ScheduleMap> scheduleMaps, Date date) {
        return scheduleMaps.stream()
                .filter(map -> map.getStartDate().compareTo(date) <= 0 && (null == map.getEndDate() || map.getEndDate().compareTo(date) >0))
                .findFirst()
                .orElse(null);
    }

    @Override
    public String generateAdherenceString(HashMap<Integer, AdherenceStringLog> callMap, Date startDate, Date endDate, long iamId, HashMap<Integer, Integer> doseFrequencyMap) {
        List<ScheduleMap> allScheduleMaps = findAllScheduleMappings(iamId).stream()
                .filter(scheduleMap -> {
                    if (null == scheduleMap.getEndDate()) {
                        return true;
                    }
                    return (scheduleMap.getStartDate().compareTo(startDate) <=0 &&  scheduleMap.getEndDate().compareTo(startDate) >= 0) ||
                            (scheduleMap.getStartDate().compareTo(endDate) <= 0 &&  scheduleMap.getEndDate().compareTo(endDate) >= 0) ||
                            (scheduleMap.getStartDate().compareTo(startDate) >= 0 && scheduleMap.getEndDate().compareTo(endDate)  <= 0);
                })
                .collect(Collectors.toList());
        List<Long> allScheduleMapIds = allScheduleMaps.stream().map(ScheduleMap::getId).collect(Collectors.toList());
        List<ScheduleTimeMap> scheduleTimeMapList = scheduleTimeMapRepository.findAllByScheduleMapIdIn(allScheduleMapIds);
        Map<Long, List<ScheduleTimeMap>> scheduleIdTimeMap = new HashMap<>();
        scheduleTimeMapList.forEach(s -> {
            scheduleIdTimeMap.putIfAbsent(s.getScheduleMapId(), new ArrayList<>());
            scheduleIdTimeMap.get(s.getScheduleMapId()).add(s);
        });
        StringBuilder adherenceStringBuilder = new StringBuilder();
        Calendar start = Calendar.getInstance();
        start.setTime(startDate);
        Calendar end = Calendar.getInstance();
        end.setTime(endDate);
        for (Date date = start.getTime(); start.before(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
            char adherenceCode = AdherenceCodeEnum.NO_INFO.getCode();
            ScheduleMap scheduleMap = getActiveScheduleMapForDate(allScheduleMaps, date);
            List<ScheduleTimeMap> currentScheduleTimeMap = null != scheduleMap ? scheduleIdTimeMap.getOrDefault(scheduleMap.getId(), new ArrayList<>()) : new ArrayList<>();
            int doseCount = currentScheduleTimeMap.size();
            if (doseCount > 0) {
                long minutes = TimeUnit.MINUTES.convert(date.getTime() - startDate.getTime(), TimeUnit.MILLISECONDS);
                int day = (int) Math.floor((minutes / 60f) / 24f);
                int numberOfDoseTakenForDay = doseFrequencyMap.getOrDefault(day, 0);
                if (numberOfDoseTakenForDay > 0) {
                    adherenceCode = numberOfDoseTakenForDay >= doseCount ? AdherenceCodeEnum.RECEIVED.getCode() : AdherenceCodeEnum.MULTI_DOSE_PARTIAL.getCode();
                }
            } else {
                adherenceCode = AdherenceCodeEnum.EMPTY_SCHEDULE.getCode();
            }
            adherenceStringBuilder.append(adherenceCode);
        }
        return adherenceStringBuilder.toString();
    }

    @Override
    public boolean shouldFlagTFNRepeatForNND() {
        return false;
    }

    @Override
    public int getNumCharsInBuffer(Long registrationId, Date registrationStartDate, Date registrationEndDate) {
        return 0;
    }

    @Override
    protected void validateNoOverlapSchedule(ScheduleSensitivity sensitivity, List<String> doseTime) {
        List<Time> timeList = doseTime.stream().map(Time::valueOf).collect(Collectors.toList());
        long distinctTimeCount = timeList.stream().distinct().count();
        if (distinctTimeCount != doseTime.size()) {
            throw new InvalidParameterException("Schedule can't overlap");
        }
    }

    @Override
    protected void saveScheduleAndCreateTimeMap(Long iamId, Date startDate, Long firstScheduleOffset, ScheduleSensitivity sensitivity, List<String> doseTime) {
        sensitivity.setLeftBuffer(0L);
        sensitivity.setRightBuffer(0L);
        ScheduleMap scheduleMap = new ScheduleMap(iamId, null, startDate, 0L, sensitivity);
        Long scheduleMapId = scheduleMapRepository.save(scheduleMap).getId();
        List<ScheduleTimeMap> scheduleTimeMapList = new ArrayList<>();
        doseTime.forEach(d -> {
            ScheduleTimeMap scheduleTimeMap = new ScheduleTimeMap(null, true, Utils.formatDate(Utils.getCurrentDate()), scheduleMapId, Time.valueOf(d));
            scheduleTimeMapList.add(scheduleTimeMap);
        });
        scheduleTimeMapRepository.saveAll(scheduleTimeMapList);
    }

    @Override
    public void shouldTakeDoseForDates(Set<Date> dates, Long iamId, List<AdherenceStringLog> logs) {
        boolean doseAlreadyExists = logs.stream().anyMatch(l -> dates.contains(l.getRecordDate()));
        if (doseAlreadyExists) {
            throw new ValidationException("Dose has already been marked");
        }
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        List<ScheduleMap> scheduleMapList =  scheduleMapRepository.findAllByIamId(iamId);
        List<Long> scheduleMapIdList = scheduleMapList
                .stream()
                .map(ScheduleMap::getId)
                .collect(Collectors.toList());
        Map<Long, List<ScheduleTimeMap>> scheduleIdToScheduleTimeMap = scheduleTimeMapRepository.findAllByScheduleMapIdIn(scheduleMapIdList)
                .stream()
                .collect(Collectors.groupingBy(ScheduleTimeMap::getScheduleMapId));
        dates.forEach(date -> {
            ScheduleMap scheduleMap = getActiveScheduleMapForDate(scheduleMapList, date);
            List<ScheduleTimeMap> scheduleTimeMapList = scheduleMap != null
                    ? scheduleIdToScheduleTimeMap.get(scheduleMap.getId())
                    : new ArrayList<>();
            if (CollectionUtils.isEmpty(scheduleTimeMapList)) {
                throw new ValidationException("Following schedule time does not exist");
            }
            List<String> doseTime = scheduleTimeMapList
                    .stream()
                    .map(s -> String.valueOf(s.getDoseTime()))
                    .collect(Collectors.toList());
            String timeString = sdf.format(date);
            if (!doseTime.contains(timeString)) {
                throw new ValidationException("Current time is not scheduled");
            }
        });
    }

    private HashMap<Integer, Integer> getDoseFrequencyMap(List<AdherenceStringLog> logList, Date startDate) {
        HashMap<Integer, Integer> doseFrequencyMap = new HashMap<>();
        logList.forEach(al -> {
            long minutes = TimeUnit.MINUTES.convert(al.getRecordDate().getTime() - startDate.getTime(), TimeUnit.MILLISECONDS);
            int day = (int) Math.floor((minutes / 60f) / 24f);
            doseFrequencyMap.put(day, doseFrequencyMap.getOrDefault(day, 0) + 1);
        });
        return doseFrequencyMap;
    }

    @Override
    public Character getTransformedValue(Character value, Long recordDays, long iamId, List<AdherenceStringLog> logs, Date startDate, Date date) {
        ScheduleMap scheduleMap = getActiveScheduleMapForDate(scheduleMapRepository.findAllByIamId(iamId), date);
        List<ScheduleTimeMap> scheduleTimeMapList =  null != scheduleMap
                ? scheduleTimeMapRepository.findAllByScheduleMapId(scheduleMap.getId())
                : new ArrayList<>();
        int doseCount = scheduleTimeMapList.size();
        HashMap<Integer, Integer> doseFrequencyMap = getDoseFrequencyMap(logs, startDate);
        int numberOfDoseTaken = doseFrequencyMap.getOrDefault(recordDays.intValue(), 0);
        char transformedValue = AdherenceCodeEnum.NO_INFO.getCode();
        if (numberOfDoseTaken > 0) {
            transformedValue = numberOfDoseTaken >= doseCount ? AdherenceCodeEnum.RECEIVED.getCode() : AdherenceCodeEnum.MULTI_DOSE_PARTIAL.getCode();
        }
        return transformedValue;
    }

    @Override
    public List<DoseTimeData> getDoseTimes(Long iamId) {
        List<DoseTimeData> doseTimeDataList = new ArrayList<>();
        Map<Long, ScheduleMap> scheduleIdToScheduleMap  =  scheduleMapRepository.findAllByIamId(iamId)
                .stream()
                .collect(Collectors.toMap(ScheduleMap::getId, i->i));
        List<Long> scheduleMapIdList =  new ArrayList<>(scheduleIdToScheduleMap.keySet());
        Map<Long, List<ScheduleTimeMap>> scheduleIdToScheduleTimeMap = scheduleTimeMapRepository.findAllByScheduleMapIdIn(scheduleMapIdList)
                .stream()
                .collect(Collectors.groupingBy(ScheduleTimeMap::getScheduleMapId));
        for (Map.Entry<Long, ScheduleMap> entry : scheduleIdToScheduleMap.entrySet()) {
            List<String> doseTimes = new ArrayList<>();
            scheduleIdToScheduleTimeMap.getOrDefault(entry.getKey(), new ArrayList<>()).forEach(s -> doseTimes.add(String.valueOf(s.getDoseTime())));
            doseTimeDataList.add(new DoseTimeData(entry.getValue().getStartDate(), entry.getValue().getEndDate(), doseTimes));
        }
        return doseTimeDataList;
    }

    @Override
    public boolean isLogsRequired() {
        return true;
    }

    @Override
    public void stopSchedule(Long iamId, Date endDate) {
        if (hasScheduleMapping()) {
            List<ScheduleMap> scheduleMapList = scheduleMapRepository.findAllByIamId(iamId);
            ScheduleMap scheduleMapWithNullEndDate = scheduleMapList
                    .stream()
                    .filter(i-> null == i.getEndDate() && i.getStartDate().compareTo(endDate) != 0)
                    .findFirst().orElse(null);
            if (null != scheduleMapWithNullEndDate) {
                scheduleMapWithNullEndDate.setEndDate(endDate);
                scheduleMapRepository.save(scheduleMapWithNullEndDate);
            }
        }
    }

    private void updateScheduleTimeMapDoseTiming(Long scheduleMapId, List<String> doseTimeList) {
        List<ScheduleTimeMap> existingScheduleTimeMapList = scheduleTimeMapRepository.findAllByScheduleMapId(scheduleMapId);
        Set<String> existingDoseTimeSet = existingScheduleTimeMapList
                .stream()
                .map(i->i.getDoseTime().toString())
                .collect(Collectors.toSet());
        Set<String> updatedDoseTimeSet = new HashSet<>(doseTimeList);
        if (updatedDoseTimeSet.size() < existingDoseTimeSet.size()) {
            List<ScheduleTimeMap> toDeleteScheduleTimeMapList = existingScheduleTimeMapList
                    .stream()
                    .filter(i->!updatedDoseTimeSet.contains(i.getDoseTime().toString()))
                    .collect(Collectors.toList());
            scheduleTimeMapRepository.deleteAll(toDeleteScheduleTimeMapList);
        } else {
            List<ScheduleTimeMap> newScheduleTimeMapList = new ArrayList<>();
            for (String doseTime : doseTimeList) {
                if (!existingDoseTimeSet.contains(doseTime)) {
                    ScheduleTimeMap scheduleTimeMap = new ScheduleTimeMap(null, true, Utils.formatDate(Utils.getCurrentDate()), scheduleMapId, Time.valueOf(doseTime));
                    newScheduleTimeMapList.add(scheduleTimeMap);
                }
            }
            scheduleTimeMapRepository.saveAll(newScheduleTimeMapList);
        }
    }

    @Override
    public void save(Long iamId, Date startDate, String scheduleString, Long firstScheduleOffset, ScheduleSensitivity sensitivity, List<String> doseTimes) {
        if (null == firstScheduleOffset) {
            firstScheduleOffset = 0L;
        }
        List<ScheduleMap> scheduleMapList = scheduleMapRepository.findAllByIamId(iamId);
        ScheduleMap scheduleMap = scheduleMapList
                .stream()
                .filter(i-> i.getStartDate().compareTo(startDate) == 0)
                .findFirst().orElse(null);
        if (null == scheduleMap) {
            if (!CollectionUtils.isEmpty(doseTimes)) {
                saveScheduleAndCreateTimeMap(iamId, startDate, firstScheduleOffset, sensitivity, doseTimes);
            }
        } else {
            updateScheduleTimeMapDoseTiming(scheduleMap.getId(), doseTimes);
        }
    }

    @Override
    public void validateScheduleAndSensitivityOverLap(String scheduleString, ScheduleSensitivity sensitivity, List<String> doseTimeList) {
        if (!CollectionUtils.isEmpty(doseTimeList)) {
            validateNoOverlapSchedule(sensitivity, doseTimeList);
        }
    }
}

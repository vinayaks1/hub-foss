package com.everwell.iam.handlers;

import com.everwell.iam.enums.ScheduleTypeEnum;
import com.everwell.iam.exceptions.ServiceException;
import com.everwell.iam.models.db.ScheduleType;
import com.everwell.iam.repositories.ScheduleTypeRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.security.InvalidParameterException;
import java.util.*;

@NoArgsConstructor
@Service
public class ScheduleTypeHandlerMap {

    private Map<Long, ScheduleHandler> scheduleTypeMap = new HashMap<>();

    private Map<ScheduleTypeEnum, ScheduleHandler> handlerMap = new HashMap<>();

    @Autowired
    private ScheduleTypeRepository scheduleTypeRepository;

    @Autowired
    public ScheduleTypeHandlerMap(List<ScheduleHandler> scheduleHandlerList) {
        if(!CollectionUtils.isEmpty(scheduleHandlerList)) {
            for(ScheduleHandler scheduleHandler : scheduleHandlerList) {
               handlerMap.put(scheduleHandler.getScheduleTypeEnumerator(), scheduleHandler);
            }
        }
    }

    @PostConstruct
    public void initialize() {
        for (ScheduleType type : scheduleTypeRepository.findAll()) {
            ScheduleTypeEnum matchedType =  ScheduleTypeEnum.getMatch(type.getName());
            if(null == matchedType) {
                throw new ServiceException("Error in initialising the Schedule Type Map");
            }
            ScheduleHandler handler = handlerMap.get(matchedType);
            handler.setScheduleTypeId(type.getId());
            scheduleTypeMap.put(type.getId(), handler);
        }
    }

    public ScheduleHandler getHandler(Long scheduleTypeId) {
        if(!scheduleTypeMap.containsKey(scheduleTypeId)) {
            throw new InvalidParameterException("Invalid schedule type");
        }
        return scheduleTypeMap.get(scheduleTypeId);
    }

}

package com.everwell.iam.handlers.impl;

import com.everwell.iam.enums.AdherenceCodeEnum;
import com.everwell.iam.enums.ScheduleTypeEnum;
import com.everwell.iam.exceptions.NotFoundException;
import com.everwell.iam.enums.DayOfWeekEnum;
import com.everwell.iam.handlers.ScheduleHandler;
import com.everwell.iam.models.db.AdherenceStringLog;
import com.everwell.iam.models.db.Schedule;
import com.everwell.iam.models.db.ScheduleMap;
import com.everwell.iam.models.dto.DoseTimeData;
import com.everwell.iam.models.dto.ScheduleSensitivity;
import com.everwell.iam.utils.Utils;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Component;

import java.security.InvalidParameterException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
public class WeeklyScheduleHandler extends ScheduleHandler {

    private char trueBit = '1';
    private char falseBit = '0';
    private int scheduleValueBitsCount = 7;

    private static final int dosageDayTimeWindow  = 24;

    @Override
    public ScheduleTypeEnum getScheduleTypeEnumerator() {
        return ScheduleTypeEnum.WEEKLY;
    }

    public TreeMap<Integer, Character> getScheduleWeekDayValueMap(String value) {
        TreeMap<Integer, Character> map = new TreeMap<>();
        char[] valueArray = value.toCharArray();
        if(valueArray.length != scheduleValueBitsCount) {
            throw new InvalidParameterException("incorrect schedule string");
        }
        for(int i = 0 ; i < valueArray.length ; i++) {
            map.put(i, valueArray[i]);
        }
        return map;
    }

    @Override
    public String getScheduleValue(String scheduleString) {
        char[] week = new char[DayOfWeekEnum.values().length];
        Arrays.fill(week, falseBit);
        for (char c : scheduleString.toCharArray()) {
            if(!DayOfWeekEnum.charValueMap.containsKey(c)) {
                throw new InvalidParameterException("Invalid schedule string");
            }
            week[DayOfWeekEnum.charValueMap.get(c)] = trueBit;
        }
        return new String(week);
    }

    @Override
    public String getScheduleString(String scheduleValue) {
        int i = 0;
        StringBuilder stringBuilder = new StringBuilder();
        for(char c : scheduleValue.toCharArray()) {
            stringBuilder.append(trueBit == c ? DayOfWeekEnum.valueCharMap.get(i) : "");
            i++;
        }
        return stringBuilder.toString();
    }

    /**
     * Validates that the scheduled day with sensitivity does not overlap
     * within the schedule week and next immediate week
     *
     * @param schedule
     * @param sensitivity
     */
    @Override
    public void validateNoOverlapSchedule(Schedule schedule, ScheduleSensitivity sensitivity) {
        TreeMap<Integer, Character> dayValueMap = getScheduleWeekDayValueMap(schedule.getValue());
        TreeMap<Integer, ScheduleSensitivity> daySensitivityRangeMap = getScheduledDaySensitivityRangeMap(dayValueMap, sensitivity);

        Integer previousEntryIndex = -1;
        ScheduleSensitivity previousEntryValue = null;
        for (Map.Entry<Integer, ScheduleSensitivity> entry : daySensitivityRangeMap.entrySet()) {
            if(previousEntryIndex > -1 && null != previousEntryValue) {
                if(previousEntryValue.getRightBuffer() > entry.getValue().getLeftBuffer()) {
                    throw new InvalidParameterException("The provided sensitivity for schedule overlaps");
                }
            } else {
                previousEntryIndex = entry.getKey();
                previousEntryValue = entry.getValue();
            }
        }
    }

    private TreeMap<Integer, ScheduleSensitivity> getScheduledDaySensitivityRangeMap(TreeMap<Integer, Character> dayValueMap, ScheduleSensitivity sensitivity) {
        TreeMap<Integer, ScheduleSensitivity> daySensitivityRangeMap= new TreeMap<>();
        int i = 0;
        for (Map.Entry<Integer, Character> entry : dayValueMap.entrySet()) {
            if( trueBit == entry.getValue()) {
                for(int j = 0; j < 2 ; j++) {
                    int index = i + (j*7);
                    Long leftExtreme = (long) (index * dosageDayTimeWindow) - sensitivity.getLeftBuffer();
                    Long rightExtreme = (long) ((index + 1) * dosageDayTimeWindow) + sensitivity.getRightBuffer();
                    daySensitivityRangeMap.put(index, new ScheduleSensitivity(leftExtreme, rightExtreme));
                }
            }
            i++;
        }
        return daySensitivityRangeMap;
    }

    @Override
    public boolean shouldTakeDose(Long iamId, Date calledDate) {
        ScheduleMap scheduleMap = scheduleMapRepository.findActiveScheduleMapForDate(iamId, calledDate);
        if(null == scheduleMap) {
            return false;
        }
        return shouldTakeDoseForSchedule(scheduleMap, calledDate);
    }

    @Override
    public boolean shouldTakeDoseForSchedule(ScheduleMap scheduleMap, Date calledDate) {
        Schedule activeSchedule = scheduleRepository.findById(scheduleMap.getScheduleId()).orElse(null);
        return shouldTakeDoseForSchedule(scheduleMap, activeSchedule, calledDate);
    }

    @Override
    public boolean shouldTakeDoseForSchedule(ScheduleMap scheduleMap, Schedule schedule, Date calledDate) {
        boolean shouldTakeDose = false;
        Pair<Date, Date> scheduledDayRange = getScheduledDayRange(scheduleMap, calledDate);
        Pair<Date, Date> scheduleDayRangeWithBuffer = getScheduledDayRangeWithSensitivity(scheduledDayRange, new ScheduleSensitivity(scheduleMap.getNegativeSensitivity(), scheduleMap.getPositiveSensitivity()));
        if(calledDate.compareTo(scheduleDayRangeWithBuffer.getLeft()) >= 0 && scheduleDayRangeWithBuffer.getRight().compareTo(calledDate) >= 0) {
            shouldTakeDose = true;
        }
        return shouldTakeDose;
    }

    private Pair<Date, Date> getScheduledDayRangeWithSensitivity(Pair<Date, Date> scheduledDayRange, ScheduleSensitivity sensitivity) {
        Date rightExtreme = DateUtils.addHours(scheduledDayRange.getRight(), sensitivity.getLeftBuffer().intValue());
        Date leftExtreme = DateUtils.addHours(scheduledDayRange.getLeft(), -(sensitivity.getLeftBuffer().intValue()));
        return Pair.of(leftExtreme, rightExtreme);
    }

    /**
     * This return the date range map for the scheduled day.
     * Implemented only for weekly once for now.
     * We'l have to revisit the logic once we have monitoring of multiple days in a week
     * Todo: take in -offset(if first day is before start date) buffer to determine the first day of the schedule from monitoring start date
     *
     * @param scheduleMap
     * @param calledDate
     * @return
     */
    private Pair<Date, Date> getScheduledDayRange(ScheduleMap scheduleMap, Date calledDate) {
        ScheduleSensitivity scheduleSensitivity = new ScheduleSensitivity(scheduleMap.getNegativeSensitivity(), scheduleMap.getPositiveSensitivity());
        Date startDate = DateUtils.addMinutes(scheduleMap.getStartDate(), scheduleMap.getFirstDoseOffset().intValue());
        long diffMinutes = TimeUnit.MINUTES.convert(calledDate.getTime() - startDate.getTime(), TimeUnit.MILLISECONDS);
        int currentWeek = diffMinutes < 0 ? 0 : (int) Math.floor(((diffMinutes/60f)/24)/7);
        Date curWeekSD = DateUtils.addWeeks(startDate, currentWeek);
        Date curWeekLE = new Date(curWeekSD.getTime());
        Date curWeekRE = DateUtils.addHours(curWeekSD, 24);
        curWeekRE = DateUtils.addMinutes(curWeekRE, -1);

        Date nextWeekLE = DateUtils.addWeeks(curWeekLE, 1);
        Date nextWeekRE = DateUtils.addWeeks(curWeekRE, 1);
        Pair<Date, Date> nextWeekWithSensitivity = getScheduledDayRangeWithSensitivity(Pair.of(nextWeekLE, nextWeekRE), scheduleSensitivity);

        Date leftExtreme = new Date(curWeekLE.getTime());
        Date rightExtreme = new Date(curWeekRE.getTime());

        if(calledDate.compareTo(nextWeekWithSensitivity.getLeft()) >= 0 && calledDate.compareTo(nextWeekWithSensitivity.getRight()) <= 0) {
            leftExtreme = new Date(nextWeekLE.getTime());
            rightExtreme = new Date(nextWeekRE.getTime());
        }

        return Pair.of(leftExtreme, rightExtreme);
    }

    @Override
    public boolean shouldTakeDose(List<ScheduleMap> scheduleMaps, Map<Long, Schedule> idScheduleMap, Date calledDate) {
        ScheduleMap activeScheduleMap = getActiveScheduleMapForDate(scheduleMaps, calledDate);
        if(null == activeScheduleMap) {
            return false;
        }
        Schedule activeSchedule = idScheduleMap.get(activeScheduleMap.getScheduleId());
        return shouldTakeDoseForSchedule(activeScheduleMap, activeSchedule, calledDate);
    }

    @Override
    public char computeAdherenceCodeForDay(List<ScheduleMap> scheduleMaps, Map<Long, Schedule> idScheduleMap, Date calledDate) {
        ScheduleMap activeScheduleMap = getActiveScheduleMapForDate(scheduleMaps, calledDate);
        if(null == activeScheduleMap) {
            return AdherenceCodeEnum.BLANK.getCode();
        }
        Pair<Date, Date> scheduledDayRange = getScheduledDayRange(activeScheduleMap, calledDate);
        if(calledDate.compareTo(scheduledDayRange.getLeft()) >= 0 && scheduledDayRange.getRight().compareTo(calledDate) >= 0) {
            return AdherenceCodeEnum.RECEIVED.getCode();
        }
        return AdherenceCodeEnum.RECEIVED_UNSCHEDULED.getCode();
    }

    private ScheduleMap getActiveScheduleMapForDate(List<ScheduleMap> scheduleMaps, Date date) {
        return scheduleMaps.stream()
                .filter(map -> map.getStartDate().compareTo(date) <= 0 && (null == map.getEndDate() || map.getEndDate().compareTo(date) >=0))
                .findFirst()
                .orElse(null);
    }

    @Override
    public Date getDateToAttribute(List<ScheduleMap> scheduleMaps, Map<Long, Schedule> idScheduleMap, Date calledDate) {
        ScheduleMap scheduleMap = getActiveScheduleMapForDate(scheduleMaps, calledDate);
        Pair<Date, Date> scheduledDayRange = getScheduledDayRange(scheduleMap, calledDate);
        Pair<Date, Date> scheduledDayRangeWithBuffer = getScheduledDayRangeWithSensitivity(scheduledDayRange, new ScheduleSensitivity(scheduleMap.getNegativeSensitivity(), scheduleMap.getPositiveSensitivity()));
        if(calledDate.compareTo(scheduledDayRangeWithBuffer.getLeft()) >= 0 && scheduledDayRange.getLeft().compareTo(calledDate) >0) {
            calledDate =  scheduledDayRange.getLeft();
        } else if (calledDate.compareTo(scheduledDayRange.getRight()) > 0 && scheduledDayRangeWithBuffer.getRight().compareTo(calledDate) >= 0) {
            calledDate = scheduledDayRange.getRight();
        }
        return calledDate;
    }

    @Override
    public String generateAdherenceString(HashMap<Integer, AdherenceStringLog> callMap, Date startDate, Date endDate, long iamId, HashMap<Integer, Integer> doseFrequencyMap) {
        List<ScheduleMap> allScheduleMaps = findAllScheduleMappings(iamId).stream()
                .filter(scheduleMap -> {
                    if(null == scheduleMap.getEndDate()) {
                        return true;
                    }
                    return (scheduleMap.getStartDate().compareTo(startDate) <=0 &&  scheduleMap.getEndDate().compareTo(startDate) >= 0) ||
                            (scheduleMap.getStartDate().compareTo(endDate) <= 0 &&  scheduleMap.getEndDate().compareTo(endDate) >= 0) ||
                            (scheduleMap.getStartDate().compareTo(startDate) >= 0 && scheduleMap.getEndDate().compareTo(endDate)  <= 0);
                })
                .collect(Collectors.toList());
        Calendar start = Calendar.getInstance();
        start.setTime(startDate);
        Calendar end = Calendar.getInstance();
        end.setTime(endDate);
        StringBuilder adherenceStringBuilder = new StringBuilder();
        for (Date date = start.getTime(); start.before(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
            ScheduleMap scheduleMap = getActiveScheduleMapForDate(allScheduleMaps, date);
            //check for null map if null map skip
            long minutes = TimeUnit.MINUTES.convert(date.getTime() - startDate.getTime(), TimeUnit.MILLISECONDS);
            int day = (int) Math.floor((minutes / 60f) / 24f);
            if(isScheduledDay(scheduleMap, date)) {
                adherenceStringBuilder.append((null == callMap.get(day)) ? AdherenceCodeEnum.NO_INFO.getCode() : callMap.get(day).getValue());
            } else {
                adherenceStringBuilder.append(AdherenceCodeEnum.BLANK.getCode());
            }
        }

        return adherenceStringBuilder.toString();
    }

    public boolean isScheduledDay(ScheduleMap scheduleMap, Date date) {
        boolean isScheduledDay = false;
        Pair<Date, Date> scheduledDayRange = getScheduledDayRange(scheduleMap, date);
        if(date.compareTo(scheduledDayRange.getLeft()) >= 0 && scheduledDayRange.getRight().compareTo(date) >= 0) {
            isScheduledDay = true;
        }
        return isScheduledDay;
    }

    @Override
    public boolean shouldFlagTFNRepeatForNND() {
        return false;
    }

    @Override
    public int getNumCharsInBuffer(Long registrationId, Date registrationStartDate, Date registrationEndDate) {
        Date today = new Date();

        if (registrationEndDate != null && registrationEndDate.before(today)) {
            return 0;
        }

        Long daysInRegistration = Utils.getDifferenceDays(registrationStartDate, today);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(today);
        int todayInd = calendar.get(Calendar.DAY_OF_WEEK) - 2;
        
        ScheduleMap scheduleMap = getActiveScheduleMapForIam(registrationId);
        if (scheduleMap == null) {
            throw new NotFoundException("active schedule not found for weekly schedule type");
        }
        return 1;
    }

    @Override
    protected void validateNoOverlapSchedule(ScheduleSensitivity sensitivity, List<String> doseTime) {

    }

    @Override
    protected void saveScheduleAndCreateTimeMap(Long iamId, Date startDate, Long firstScheduleOffset, ScheduleSensitivity sensitivity, List<String> doseTime) {

    }

    @Override
    public void shouldTakeDoseForDates(Set<Date> dates, Long iamId, List<AdherenceStringLog> logs) {
    }

    @Override
    public Character getTransformedValue(Character value, Long recordDays, long iamId, List<AdherenceStringLog> logs, Date startDate, Date date) {
        return value;
    }

    @Override
    public List<DoseTimeData> getDoseTimes(Long iamId) {
        return new ArrayList<>();
    }

    @Override
    public boolean isLogsRequired() {
        return false;
    }
}

package com.everwell.iam.handlers.impl;

import com.everwell.iam.enums.AdherenceCodeEnum;
import com.everwell.iam.enums.AdherenceTech;
import com.everwell.iam.exceptions.NotFoundException;
import com.everwell.iam.handlers.AdherenceTechHandler;
import com.everwell.iam.handlers.ScheduleHandler;
import com.everwell.iam.handlers.ScheduleTypeHandlerMap;
import com.everwell.iam.models.db.*;
import com.everwell.iam.models.dto.AdhTechLogsData;
import com.everwell.iam.models.http.requests.*;
import com.everwell.iam.repositories.ImeiMapRepository;
import com.everwell.iam.repositories.MermLogRepository;
import com.everwell.iam.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.text.ParseException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Component
public class MERMHandler extends AdherenceTechHandler {

    @Autowired
    MermLogRepository mermLogRepository;

    @Autowired
    ImeiMapRepository imeiMapRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(MERMHandler.class);

    @Override
    public void postRegistration(Registration registration, EntityRequest entityRequest, boolean isNew) {
        super.postRegistration(registration, entityRequest, isNew);
        LOGGER.info("[postRegistration] request received: " + registration + " new case: " + isNew);
        MERMEntityRequest request = (MERMEntityRequest) entityRequest;
        if (isNew) {
            Date startDate = Utils.getCurrentDate();
            try {
                startDate = Utils.convertStringToDate(entityRequest.getStartDate());
            } catch (ParseException e) {
                LOGGER.warn("[save] could not parse date");
            }
            ImeiMap imeiMap = new ImeiMap(request.getImei(), registration.getId(), startDate);
            imeiMapRepository.save(imeiMap);
        }
        applicationEventPublisher.publishEvent(new RegenerateAdherenceRequest(Collections.singletonList(registration.getId())));
    }

    @Override
    public void preDeletion(Registration registration) {
        // Deallocate MERM Boxes
        LOGGER.info("[preDeletion] request received: " + registration);
        deactivateIMEIByIamId(registration.getId(), registration.getEndDate());
        applicationEventPublisher.publishEvent(new RegenerateAdherenceRequest(Collections.singletonList(registration.getId())));
    }

    @Override
    public AdherenceTech getAdhTechEnumerator() {
        return AdherenceTech.MERM;
    }

    public List<Long> processIncomingEvent(MermEventRequest post) {
        String imei = post.getSn();
        Date eventDate = Utils.getCurrentDate();
        try {
            eventDate = Utils.convertStringToDate(post.getEventDateTime(), "ddMMyyHHmmss");
        } catch (ParseException e) {
            LOGGER.warn("[processEvent] Failed to parse event date");
        }
        //Save to MermLogs
        MermLogs mermLog = new MermLogs(imei, eventDate);
        mermLogRepository.save(mermLog);
        //Save to AdherenceStringLog
        List<ImeiMap> imeiMap = imeiMapRepository.findActiveIamIdByImei(imei, eventDate);
        List<Long> insertedLogs = new ArrayList<>();
        if(!imeiMap.isEmpty()) {
            List<Registration> registrations = registrationRepository.findByIdIn(imeiMap.stream().map(ImeiMap::getIamId).collect(Collectors.toList()));
            for (Registration reg : registrations) {
                char adherenceCode = AdherenceCodeEnum.RECEIVED.getCode();
                ScheduleHandler scheduleHandler = scheduleTypeHandlerMap.getHandler(reg.getScheduleTypeId());
                Date dayToAttribute = eventDate;
                if (scheduleHandler.shouldTakeDose(reg.getId(), eventDate)) {
                    adherenceCode = scheduleHandler.computeAdherenceCodeForDay(reg.getId(), eventDate);
                    dayToAttribute = scheduleHandler.getDateToAttribute(reg.getId(), eventDate);
                }
                TreeMap<Date, AdherenceCodeEnum> datesToAdhCode = new TreeMap<>();
                datesToAdhCode.put(dayToAttribute, AdherenceCodeEnum.getByCode(adherenceCode));
                insertedLogs.addAll(updateAdherenceMap(datesToAdhCode, reg));
            }
        }
        return insertedLogs;
    }

    @Override
    public CompletableFuture<List<Long>> regenerateAdherence(Set<Long> iamIds) {
        LOGGER.info("Regenerate Adherence started for entities: " + iamIds.size());
        List<Long> logs = new ArrayList<>();
        List<Character> eventLogGeneratedValueList = Arrays.asList(
                AdherenceCodeEnum.RECEIVED.getCode()
        );
        for (Long id : iamIds) {
            /**
             * delete all previous event adherence data
             * DO NOT DELETE MANUALLY MARKED DOSES (values = 2 or 9)
             **/
            deleteAdherenceLogByIamIdAndAdherenceCodes(eventLogGeneratedValueList, id);
            List<MermLogs> mermLogs = mermLogRepository.getAllByIamIdOrderByClientTimeCustom(id);
            logs.addAll(computeCompleteAdherenceString(mermLogs, id));
        }
        LOGGER.info("Regenerated Successfully for entities: " + iamIds.size());
        return CompletableFuture.completedFuture(logs);
    }

    @Override
    public List<AdhTechLogsData> getTechLogs(Long iamId) {
        List<AdhTechLogsData> adhTechLogs = new ArrayList<>();
        mermLogRepository.getAllByIamIdOrderByClientTimeCustom(iamId)
                .forEach(f -> {
                    adhTechLogs.add(new AdhTechLogsData(f.getClientTime(), f.getImei(), null, null, null));
                });
        return adhTechLogs;
    }

    @Async
    public List<Long> computeCompleteAdherenceString(List<MermLogs> mermLogs, Long id) {
        LOGGER.info("Regenerating Adherence for IAM id: " + id);
        List<AdherenceStringLog> logs = new ArrayList<>();
        Registration registration = registrationRepository.findById(id).get();
        ScheduleHandler handler = scheduleTypeHandlerMap.getHandler(registration.getScheduleTypeId());
        for (MermLogs m : mermLogs) {
            char adherenceCode = AdherenceCodeEnum.RECEIVED.getCode();
            if (handler.shouldTakeDose(id, m.getClientTime())) {
                logs.add(new AdherenceStringLog(id, adherenceCode, Utils.formatDate(m.getClientTime())));
            }
        }
        updateRegistrationAfterRegen(registration, logs);
        LOGGER.info("Regenerating Adherence finished for IAM id: " + id);
        return updateAdherenceLogBulk(logs);
    }

    public List<ImeiMap> getImei(List<Long> iamId, boolean active) {
        List<ImeiMap> imeiMap = new ArrayList<>();
        if(active)
            imeiMap.add(imeiMapRepository.findActiveImeiByIamId(iamId));
        else
            imeiMap.addAll(imeiMapRepository.findAllByIamIdIn(iamId));
        if (Utils.isEmptyOrAllNull(imeiMap)) {
            throw new NotFoundException("IMEI mapping not found!");
        }
        return imeiMap;
    }

    @Override
    public boolean eligibleForUpdate(char before, char after) {
        if (before == AdherenceCodeEnum.RECEIVED.getCode()) return false;
        if ((before == AdherenceCodeEnum.MISSED.getCode() || before == AdherenceCodeEnum.MANUAL.getCode()) && (after == AdherenceCodeEnum.PATIENT_MANUAL.getCode() || after == AdherenceCodeEnum.PATIENT_MISSED.getCode())) return false;
        return true;
    }

    public boolean saveIMEINumbers(ImeiMap imeiMap) {
        LOGGER.info("[saveIMEINumbers] saving imei " + imeiMap.getImei() + " id for iamId: " + imeiMap.getIamId());
        imeiMapRepository.save(imeiMap);
        applicationEventPublisher.publishEvent(new RegenerateAdherenceRequest(Collections.singletonList(imeiMap.getIamId())));
        return true;
    }

    public void deactivateIMEIByIamId(Long iamId, Date date) {
        if (date == null)
            date = Utils.getCurrentDate();
        imeiMapRepository.deallocateImeiByIamIds(iamId, date);
    }

    public boolean isMERMAllocated(String imei) {
        List<ImeiMap> imeiDetails = imeiMapRepository.findActiveIamIdByImei(imei, Utils.getCurrentDate());
        return (!imeiDetails.isEmpty());
    }

    @Override
    public void deleteRegistration(Set<Long> iamId) {
        super.deleteRegistration(iamId);
        imeiMapRepository.deleteAllByIamIdIn(iamId);
    }
}

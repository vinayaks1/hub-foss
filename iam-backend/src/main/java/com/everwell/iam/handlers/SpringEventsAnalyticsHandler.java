package com.everwell.iam.handlers;

import com.everwell.iam.constants.Constants;
import com.everwell.iam.constants.RabbitMQConstants;
import com.everwell.iam.enums.AdherenceCodeEnum;
import com.everwell.iam.enums.MatomoEventCategory;
import com.everwell.iam.models.db.AccessMapping;
import com.everwell.iam.models.db.Registration;
import com.everwell.iam.models.dto.eventstreaming.EventStreamingDto;
import com.everwell.iam.models.dto.eventstreaming.analytics.*;
import com.everwell.iam.repositories.AccessMappingRepository;
import com.everwell.iam.repositories.RegistrationRepository;
import com.everwell.iam.services.ClientEventFlowConfigMap;
import com.everwell.iam.services.ClientService;
import com.everwell.iam.services.RabbitMQPublisherService;
import com.everwell.iam.utils.SentryUtils;
import com.everwell.iam.utils.Utils;
import io.sentry.Sentry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class SpringEventsAnalyticsHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(SpringEventsAnalyticsHandler.class);

    @Autowired
    RabbitMQPublisherService rabbitMQPublisherService;

    @Autowired
    ClientService clientService;

    @Autowired
    AccessMappingRepository accessMappingRepository;

    @Autowired
    RegistrationRepository registrationRepository;

    @Autowired
    private AdhTechHandlerMap adherenceTechnologyHandlerMap;

    @Autowired
    private ClientEventFlowConfigMap clientEventFlowConfigMap;


    @EventListener
    public void trackRegistrationEvent(RegisterEntityTrackingDto registerEntityTrackingDto) {
        LOGGER.info("[trackRegistrationEvent] called - " + registerEntityTrackingDto.toString());
        String currentRegistrationAdhTech = registerEntityTrackingDto.getAdherenceTechName();
        String eventAction = Constants.MatomoEventConstants.REOPEN_WITH_EVENT + currentRegistrationAdhTech;
        String eventCategory = MatomoEventCategory.REOPEN_CASE.name();
        if (registerEntityTrackingDto.isNew()) {
            eventCategory =  MatomoEventCategory.REGISTER_ENTITY.name();
            List<AccessMapping> accessMappingList = accessMappingRepository.getAllByEntityIdAndClientId(registerEntityTrackingDto.getEntityId(), registerEntityTrackingDto.getClientId());
            List<Long> iamIds = accessMappingList.stream().map(AccessMapping::getIamId).collect(Collectors.toList());
            if (iamIds.size() >= 1) { //indicating switch
                List<Registration> registrationList = registrationRepository.findByIdIn(iamIds);
                registrationList
                    .sort(
                        Comparator.comparing(Registration::getCreatedDate)
                            .thenComparing(Registration::getStartDate)
                            .reversed()
                    );
                String adTechNameCurrent = adherenceTechnologyHandlerMap
                    .getHandler(registerEntityTrackingDto.getAdherenceType())
                    .getAdhTechEnumerator()
                    .name();
                String adTechNameOld = adherenceTechnologyHandlerMap
                    .getHandler(registrationList.get(0).getAdTechId())
                    .getAdhTechEnumerator()
                    .name();
                eventAction = Constants.MatomoEventConstants.SWITCH_EVENT + adTechNameOld + "_" + adTechNameCurrent;
            } else {
                eventAction = Constants.MatomoEventConstants.REG_WITH_EVENT + registerEntityTrackingDto.getAdherenceTechName();
            }
        }
        EventStreamingAnalyticsDto eventStreamingAnalyticsDto = new EventStreamingAnalyticsDto(
            registerEntityTrackingDto.getEntityId(), eventAction, eventCategory
        );
        pushToQueue(eventStreamingAnalyticsDto, registerEntityTrackingDto.getClientId());
    }

    @EventListener
    public void trackCloseCaseEvent(CloseCaseTrackingDto closeCaseTrackingDto) {
        LOGGER.info("[trackCloseCaseEvent] called - " + closeCaseTrackingDto.toString());
        Registration registration = registrationRepository.findById(closeCaseTrackingDto.getIamId()).orElse(null);
        if (registration != null) {
            String adTechName = adherenceTechnologyHandlerMap
                .getHandler(registration.getAdTechId())
                .getAdhTechEnumerator()
                .name();
            EventStreamingAnalyticsDto eventStreamingAnalyticsDto = new EventStreamingAnalyticsDto(
                closeCaseTrackingDto.getEntityId(), Constants.MatomoEventConstants.CLOSE_WITH_EVENT + adTechName, MatomoEventCategory.CLOSE_ENTITY.name()
            );
            pushToQueue(eventStreamingAnalyticsDto, closeCaseTrackingDto.getClientId());
        }
    }

    @EventListener
    public void trackRecordAdherence(RecordAdherenceTrackingDto recordAdherenceTrackingDto) {
        LOGGER.info("[trackRecordAdherence] called - " + recordAdherenceTrackingDto.toString());
        AccessMapping accessMapping = accessMappingRepository
            .getByIamId(Arrays.asList(recordAdherenceTrackingDto.getRegistration().getId()))
            .get(0);
        String adTechName = recordAdherenceTrackingDto.getAdherenceTechName();
        recordAdherenceTrackingDto.getDatesToValueMapList()
            .forEach((k, v) -> {
                String eventCategory = MatomoEventCategory.RECORD_ADHERENCE.name();
                String eventAction = v.getTrackingEventName() + Constants.MatomoEventConstants.WITH + adTechName;
                String eventName = accessMapping.getEntityId();
                EventStreamingAnalyticsDto eventStreamingAnalyticsDto = new EventStreamingAnalyticsDto(
                    eventName, eventAction, eventCategory
                );
                pushToQueue(eventStreamingAnalyticsDto, accessMapping.getClientId());
            });
    }

    private void pushToQueue(EventStreamingAnalyticsDto eventStreamingAnalyticsDto, Long clientId) {
        boolean external = clientService.getClientById(clientId).isExternal();
        if(!external) {
            Map<String, Object> headers = new HashMap<>();
            headers.put(RabbitMQConstants.CLIENT_ID, String.valueOf(clientEventFlowConfigMap.getClientConfigMap().get(clientId)));
            rabbitMQPublisherService.send(Utils.asJsonString(eventStreamingAnalyticsDto), "", RabbitMQConstants.EVENTFLOW_EXCHANGE, headers);
        }
    }
}

package com.everwell.iam.handlers.impl;

import com.everwell.iam.enums.AdherenceTech;
import com.everwell.iam.handlers.AdherenceTechHandler;
import com.everwell.iam.models.db.AdherenceStringLog;
import com.everwell.iam.models.db.Registration;
import com.everwell.iam.models.dto.AdhTechLogsData;
import com.everwell.iam.models.http.requests.EntityRequest;
import org.springframework.stereotype.Component;
import com.everwell.iam.enums.AdherenceCodeEnum;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Component
public class NoneHandler extends AdherenceTechHandler {

    @Override
    public void postRegistration(Registration registration, EntityRequest entityRequest, boolean isNew) {
        super.postRegistration(registration, entityRequest, isNew);
    }

    @Override
    public void preDeletion(Registration registration) {

    }

    @Override
    public CompletableFuture<List<Long>> regenerateAdherence(Set<Long> iamIds) {
        return new CompletableFuture<>();
    }

    @Override
    public List<AdhTechLogsData> getTechLogs(Long iamId) {
        Registration registration = registrationRepository.getOne(iamId);
        List<AdherenceStringLog> logs = logRepository.findAllByIamAndStartDate(registration.getStartDate(), iamId);
        List<AdhTechLogsData> adhTechLogs = logs.stream().map(l -> new AdhTechLogsData(l.getRecordDate())).collect(Collectors.toList());
        return adhTechLogs;
    }


    @Override
    public AdherenceTech getAdhTechEnumerator() {
        return AdherenceTech.NONE;
    }

    @Override
    public boolean eligibleForUpdate(char before, char after) {
        if ((before == AdherenceCodeEnum.MISSED.getCode() || before == AdherenceCodeEnum.MANUAL.getCode() || before == AdherenceCodeEnum.RECEIVED.getCode()) && (after == AdherenceCodeEnum.PATIENT_MANUAL.getCode() || after == AdherenceCodeEnum.PATIENT_MISSED.getCode())) return false;
        return true;
    }

}

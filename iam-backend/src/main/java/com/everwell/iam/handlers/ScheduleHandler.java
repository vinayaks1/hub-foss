package com.everwell.iam.handlers;

import com.everwell.iam.enums.AdherenceCodeEnum;
import com.everwell.iam.enums.ScheduleTypeEnum;
import com.everwell.iam.exceptions.ConflictException;
import com.everwell.iam.exceptions.NotFoundException;
import com.everwell.iam.models.db.*;
import com.everwell.iam.models.dto.DoseTimeData;
import com.everwell.iam.models.dto.ScheduleSensitivity;
import com.everwell.iam.repositories.ScheduleMapRepository;
import com.everwell.iam.repositories.ScheduleRepository;
import com.everwell.iam.repositories.ScheduleTimeMapRepository;
import lombok.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.security.InvalidParameterException;
import java.util.*;
import java.util.stream.Collectors;


public abstract class ScheduleHandler {

    @Getter
    @Setter
    protected Long scheduleTypeId;

    @Autowired
    protected ScheduleMapRepository scheduleMapRepository;

    @Autowired
    protected ScheduleRepository scheduleRepository;

    @Autowired
    protected ScheduleTimeMapRepository scheduleTimeMapRepository;


    public abstract ScheduleTypeEnum getScheduleTypeEnumerator();

    public abstract void validateNoOverlapSchedule(Schedule schedule, ScheduleSensitivity sensitivity);

    public abstract String getScheduleValue(String scheduleString);

    protected abstract String getScheduleString(String scheduleValue);

    public abstract boolean shouldTakeDose(Long iamId, Date calledDate);

    public abstract boolean shouldTakeDoseForSchedule(ScheduleMap scheduleMap, Date calledDate);

    public abstract boolean shouldTakeDoseForSchedule(ScheduleMap scheduleMap, Schedule schedule, Date calledDate);

    public abstract boolean shouldTakeDose(List<ScheduleMap> scheduleMaps, Map<Long, Schedule> idScheduleMap, Date calledDate);

    public abstract char computeAdherenceCodeForDay(List<ScheduleMap> scheduleMaps, Map<Long, Schedule> idScheduleMap, Date calledDate);

    public abstract Date getDateToAttribute(List<ScheduleMap> scheduleMaps, Map<Long, Schedule> idScheduleMap, Date calledDate);

    public abstract String generateAdherenceString(HashMap<Integer, AdherenceStringLog> callMap, Date startDate, Date endDate, long iamId, HashMap<Integer, Integer> doseFrequencyMap);

    public abstract boolean shouldFlagTFNRepeatForNND();

    public abstract int getNumCharsInBuffer(Long registrationId, Date registrationStartDate, Date registrationEndDate);

    protected abstract void validateNoOverlapSchedule(ScheduleSensitivity sensitivity, List<String> doseTime);

    protected abstract void saveScheduleAndCreateTimeMap(Long iamId, Date startDate, Long firstScheduleOffset, ScheduleSensitivity sensitivity, List<String> doseTime);

    public abstract void shouldTakeDoseForDates(Set<Date> dates, Long iamId, List<AdherenceStringLog> logs);

    public abstract Character getTransformedValue(Character value, Long recordDays, long iamId, List<AdherenceStringLog> logs, Date startDate, Date date);

    public abstract  List<DoseTimeData> getDoseTimes(Long iamId);

    public abstract boolean isLogsRequired();

    public List<ScheduleMap> findAllScheduleMappings(Long iamId) {
        if(!hasScheduleMapping()) {
            return new ArrayList<>();
        }
        return scheduleMapRepository.findAllByIamId(iamId);
    }

    public List<Schedule> getAllSchedules(List<Long> scheduleIds) {
        if(!hasScheduleMapping()) {
            return new ArrayList<>();
        }
        return scheduleRepository.findAllById(scheduleIds);
    }

    public ScheduleMap getActiveScheduleMapForIam(Long iamId) {
        if(!hasScheduleMapping()) {
            return null;
        }
        return scheduleMapRepository.findByActiveTrueAndIamId(iamId);
    }

    public boolean hasScheduleMapping() {
        if(ScheduleTypeEnum.DAILY.getId().longValue() == scheduleTypeId) {
            return false;
        }
        return true;
    }

    public boolean isDoseTimeSupported() {
        if (ScheduleTypeEnum.MULTI_FREQUENCY.getId().longValue() == scheduleTypeId) {
            return true;
        }
        return false;
    }

    public Schedule findById(Long scheduleId) {
        return scheduleRepository.findById(scheduleId).orElse(null);
    }

    public List<Long> findAllIamWithActiveSchedule(Long clientId, String entityId) {
        if(!hasScheduleMapping()) {
            return new ArrayList<>();
        }
        return scheduleMapRepository.findAllIamIdsWithActiveSchedulesByClientIdAndEntityId(clientId, entityId);
    }

    public ScheduleMap getLastCreatedSchedule(List<ScheduleMap> scheduleMaps) {
        if(!hasScheduleMapping()) {
            return null;
        }
        ScheduleMap lastClosedSchedule;
        List<ScheduleMap> scheduleMapList = scheduleMaps.stream().sorted((o1, o2) -> o2.getCreatedDate().compareTo(o1.getCreatedDate())).collect(Collectors.toList());
        if(!CollectionUtils.isEmpty(scheduleMapList)) {
            lastClosedSchedule = scheduleMapList.get(0);
        } else {
            throw new NotFoundException("no registration schedules found");
        }
        return lastClosedSchedule;
    }

    public ScheduleMap getLastCreatedSchedule(Long iamId) {
        return getLastCreatedSchedule(findAllScheduleMappings(iamId));
    }

    public Schedule findByScheduleValue(String scheduleValue) {
        return scheduleRepository.findByScheduleTypeIdAndValue(scheduleTypeId, scheduleValue);
    }

    public void validateScheduleAndSensitivityOverLap(String scheduleString, ScheduleSensitivity sensitivity) {
        if(!hasScheduleMapping()) {
            return ;
        }
        if(null == sensitivity.getRightBuffer() || null == sensitivity.getLeftBuffer() || sensitivity.getLeftBuffer() < 0 || sensitivity.getRightBuffer() < 0) {
            throw new InvalidParameterException("Invalid schedule sensitivities");
        }
        Schedule schedule = findByScheduleValue(getScheduleValue(scheduleString));
        if(null == schedule) {
            throw  new InvalidParameterException("Invalid schedule string");
        }
        validateNoOverlapSchedule(schedule, sensitivity);
    }

    public void validateScheduleAndSensitivityOverLap(String scheduleString, ScheduleSensitivity sensitivity, List<String> doseTime) {
        if (CollectionUtils.isEmpty(doseTime)) {
            validateScheduleAndSensitivityOverLap(scheduleString, sensitivity);
        } else {
            validateNoOverlapSchedule(sensitivity, doseTime);
        }
    }

    public void save(Long iamId, Date startDate, String scheduleString, Long firstScheduleOffset, ScheduleSensitivity sensitivity) {
        if(!hasScheduleMapping()) {
            return;
        }
        Schedule schedule = findByScheduleValue(getScheduleValue(scheduleString));
        ScheduleMap scheduleMap = new ScheduleMap(iamId, schedule.getId(), startDate, firstScheduleOffset, sensitivity);
        scheduleMapRepository.save(scheduleMap);
    }

    public void save(Long iamId, Date startDate, String scheduleString, Long firstScheduleOffset, ScheduleSensitivity sensitivity, List<String> doseTime) {
        if (null == firstScheduleOffset) {
            firstScheduleOffset = 0L;
        }
        if (CollectionUtils.isEmpty(doseTime)) {
            save(iamId, startDate, scheduleString, firstScheduleOffset, sensitivity);
        } else {
            saveScheduleAndCreateTimeMap(iamId, startDate, firstScheduleOffset, sensitivity, doseTime);
        }
    }

    public void updateStartDate(Long iamId, Date date, Long firstDoseOffset) {
        if(!hasScheduleMapping()) {
            return;
        }
        List<ScheduleMap> scheduleMaps = findAllScheduleMappings(iamId);
        if(scheduleMaps.size() > 1) {
            throw new UnsupportedOperationException("Cannot update start date for schedule");
        }

        ScheduleMap lastCreatedSchedule = getLastCreatedSchedule(scheduleMaps);
        lastCreatedSchedule.setStartDate(date);
        lastCreatedSchedule.setFirstDoseOffset(firstDoseOffset);
        scheduleMapRepository.save(lastCreatedSchedule);
    }

    public void updateEndDateAndActiveStatus(ScheduleMap scheduleMap, Date endDate, boolean active) {
        if(!hasScheduleMapping()) {
            return;
        }
        scheduleMap.setEndDate(endDate);
        scheduleMap.setActive(active);
        scheduleMapRepository.save(scheduleMap);
        if (isDoseTimeSupported()) {
            List<ScheduleTimeMap> scheduleTimeMapList = scheduleTimeMapRepository.findAllByScheduleMapId(scheduleMap.getId());
            scheduleTimeMapList.forEach(s -> s.setActive(active));
            scheduleTimeMapRepository.saveAll(scheduleTimeMapList);
        }
    }

    public void stopSchedule(Long iamId, Date endDate) {
        if(!hasScheduleMapping()) {
            return;
        }
        ScheduleMap activeScheduleMap = getActiveScheduleMapForIam(iamId);
        if(null == activeScheduleMap) {
            throw new ConflictException("Active schedule does not exits");
        }
        updateEndDateAndActiveStatus(activeScheduleMap, endDate, false);
    }

    public void reopenSchedule(Registration registration) {
        if(!hasScheduleMapping()) {
            return;
        }
        ScheduleMap lastClosedScheduleMap = getLastCreatedSchedule(registration.getId());
        updateEndDateAndActiveStatus(lastClosedScheduleMap, null, true);
    }

    public char computeAdherenceCodeForDay(Long iamId, Date date) {
        if(!hasScheduleMapping()) {
            return AdherenceCodeEnum.RECEIVED.getCode();
        }
        List<ScheduleMap> scheduleMapList = findAllScheduleMappings(iamId);
        Map<Long, Schedule> idScheduleMap = getAllSchedules(scheduleMapList.stream().map(ScheduleMap::getScheduleId).collect(Collectors.toList()))
                .stream().collect(Collectors.toMap(Schedule::getId, schedule -> schedule));

        return computeAdherenceCodeForDay(scheduleMapList, idScheduleMap, date);
    }

    public Date getDateToAttribute(Long iamId, Date calledDate) {
        if(!hasScheduleMapping()) {
            return calledDate;
        }
        List<ScheduleMap> scheduleMapList = findAllScheduleMappings(iamId);
        Map<Long, Schedule> idScheduleMap = getAllSchedules(scheduleMapList.stream().map(ScheduleMap::getScheduleId).collect(Collectors.toList()))
                .stream().collect(Collectors.toMap(Schedule::getId, schedule -> schedule));
        return getDateToAttribute(scheduleMapList, idScheduleMap, calledDate);
    }

    public String getActiveScheduleString(Long iamId) {
        ScheduleMap activeMap = getLastCreatedSchedule(iamId);
        if(null == activeMap || null == activeMap.getScheduleId()) {
            return "";
        }
        Schedule schedule = scheduleRepository.findById(activeMap.getScheduleId()).orElse(new Schedule());
        return getScheduleString(schedule.getValue());
    }



}

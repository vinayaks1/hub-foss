package com.everwell.iam.handlers.impl;

import com.everwell.iam.constants.Constants;
import com.everwell.iam.enums.AdherenceCodeEnum;
import com.everwell.iam.enums.AdherenceTech;
import com.everwell.iam.exceptions.InternalServerErrorException;
import com.everwell.iam.handlers.AdherenceTechHandler;
import com.everwell.iam.handlers.ScheduleHandler;
import com.everwell.iam.models.db.*;
import com.everwell.iam.models.dto.AdhTechLogsData;
import com.everwell.iam.models.dto.IamScheduleTypeIdDto;
import com.everwell.iam.models.dto.PhoneEditDto;
import com.everwell.iam.models.dto.TFNRepeatDto;
import com.everwell.iam.models.http.requests.*;
import com.everwell.iam.repositories.CallLogRepository;
import com.everwell.iam.repositories.PhoneMapRepository;
import com.everwell.iam.utils.CacheUtils;
import com.everwell.iam.utils.Utils;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Async;
import org.springframework.util.CollectionUtils;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public abstract class NNDCommonHandler extends AdherenceTechHandler {

    @Autowired
    private CallLogRepository callLogRepository;

    @Autowired
    private PhoneMapRepository phoneMapRepository;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @Getter
    private boolean handleCommonOperation = false;

    private static final Logger LOGGER = LoggerFactory.getLogger(NNDCommonHandler.class);

    int regenerateAdherenceCount = 1;

    @Override
    public void postRegistration(Registration registration, EntityRequest entityRequest, boolean isNew) {
    /*
        regardless of new/reopen case we need to regenerate adherence
        to regenerate adherence:
        1. get all phones numbers for this patient
        2. get all iam ids linked to all those numbers
        3. regenerate adherence for all those iamIds - to cover for shared phones
    */
        super.postRegistration(registration, entityRequest, isNew);
        LOGGER.info("[postRegistration] request received: " + registration + " entity request: " + entityRequest + " new case: "+ isNew);
        NNDCommonEntityRequest nndEntityRequest = (NNDCommonEntityRequest) entityRequest;
        if(isNew) {
            List<PhoneMap> phoneMaps = nndEntityRequest
                    .getPhoneNumbers()
                    .stream()
                    .map(m -> {
                        Date stopDate = null;
                        if (!StringUtils.isEmpty(m.getStopDate()))
                            try {
                                stopDate = Utils.convertStringToDate(m.getStopDate());
                            } catch (ParseException e) {
                                LOGGER.warn("[postRegistration] Could not parse stop date time");
                            }
                        return new PhoneMap(m.getPhoneNumber(), registration.getId(), stopDate);
                    })
                    .collect(Collectors.toList());
            //only phone data for now
            phoneMapRepository.saveAll(phoneMaps);
        } else {
            List<String> phonesLinkedToIAM = new ArrayList<>();
            if (nndEntityRequest.getPhoneNumbers() != null) {
                phonesLinkedToIAM = nndEntityRequest.getPhoneNumbers().stream().map(PhoneRequest.PhoneNumbers::getPhoneNumber).collect(Collectors.toList());
            }
            deactivateOldPhonesAndAddNew(new PhoneEditDto(registration.getId(), phonesLinkedToIAM, entityRequest.getUniqueIdentifier()));
        }
        regenAdherenceForShared(registration, false);
    }

    @Override
    public void preDeletion(Registration registration) {
        LOGGER.info("[preDeletion] request received: " + registration);
        regenAdherenceForShared(registration, true);
    }

    public void emitEventForRegen(List<String> phones) {
        List<String> correctPhones = cleanDummyPhones(phones);
        if (correctPhones.size() > 0) {
            List<Long> iamIds = getIamIdsUsingPhones(correctPhones);
            applicationEventPublisher.publishEvent(new RegenerateAdherenceRequest(iamIds));
        }
    }

    public void regenAdherenceForShared(Registration registration, boolean deactivatePhones) {
        List<String> phones = getPhonesUsingIamId(registration.getId());
        if (deactivatePhones)
            deactivatePhoneNumbersByIamIds(new HashSet<>(Collections.singletonList(registration.getId())), registration.getEndDate());
        emitEventForRegen(phones);
    }

    @Override
    public abstract AdherenceTech getAdhTechEnumerator();

    public CallLogs saveCallLog(IncomingCallRequest incomingCallRequest) {
        Date calledDate = Utils.getCurrentDate();
        if (!StringUtils.isEmpty(incomingCallRequest.getUtcDateTime())) {
            try {
                calledDate = Utils.convertStringToDate(incomingCallRequest.getUtcDateTime());
            } catch (ParseException e) {
                LOGGER.warn("[processIncomingCall] couldn't parse date, defaulting to current time");
            }
        }
        String numberDialed = incomingCallRequest.getNumberDialed();
        String post = incomingCallRequest.getPostBody();
        String caller = incomingCallRequest.getCaller();
        CallLogs callLog = new CallLogs(numberDialed, post, Utils.getCurrentDate(), calledDate, caller);
        callLogRepository.save(callLog);
        return callLog;
    }

    @Async
    public CompletableFuture<List<Long>> processIncomingCall(CallLogs incomingCallLog) {
        Date calledDate = incomingCallLog.getClientTime();
        String caller = incomingCallLog.getCaller();
        CacheUtils.incrementKeyInCache(caller, Constants.PHONE_VALIDITY_EXPIRY);
        List<Long> insertedLogs = new ArrayList<>();
        //do only if called date is today
        List<BigInteger> activeIamIds = phoneMapRepository.findAllActiveIamIdsByPhoneNumbers(caller, calledDate);
        // Check if there is any mapping present in IAM for the incoming caller number
        if(CollectionUtils.isEmpty(activeIamIds)){
            // Check if common operation should be handled by this handler
            if(isHandleCommonOperation()){
                // When there is no mapping present for calling number than publish Spring event for handling the call
                handleCallFromUnregisteredPhoneNumber(caller, incomingCallLog.getNumberDialled());
            }
        } else {
            List<Registration> registrations = registrationRepository.findByIdIn(activeIamIds.stream().map(BigInteger::longValue).collect(Collectors.toList()));
            List<Registration> activeRegistrationsToTakeDose = new ArrayList<>();
            for (Registration iam : registrations) {
                ScheduleHandler handler = scheduleTypeHandlerMap.getHandler(iam.getScheduleTypeId());
                if(handler.shouldTakeDose(iam.getId(), calledDate)) {
                    activeRegistrationsToTakeDose.add(iam);
                }
            }
            List<AdherenceStringLog> logs = computeSingleAdherenceCodeForDay(activeRegistrationsToTakeDose, calledDate);
            Map<Long, TreeMap<Date, AdherenceCodeEnum>> iamIdToAdhLogMap = new HashMap<>();
            // if shared numbers we can have multiple iamIds
            logs.forEach(l -> {
                if (iamIdToAdhLogMap.containsKey(l.getIamId())) {
                    TreeMap<Date, AdherenceCodeEnum> map = iamIdToAdhLogMap.get(l.getIamId());
                    map.put(l.getRecordDate(), AdherenceCodeEnum.getByCode(l.getValue()));
                    iamIdToAdhLogMap.put(l.getIamId(), map);
                } else {
                    TreeMap<Date, AdherenceCodeEnum> map = new TreeMap<>();
                    map.put(l.getRecordDate(), AdherenceCodeEnum.getByCode(l.getValue()));
                    iamIdToAdhLogMap.put(l.getIamId(), map);
                }
            });
            iamIdToAdhLogMap.forEach((iamId, logMap) -> {
                Registration reg = registrationRepository.findById(iamId).get();
                insertedLogs.addAll(updateAdherenceMap(logMap, reg));
            });
        }
        return CompletableFuture.completedFuture(insertedLogs);
    }

    public List<Registration> getRegistrationsForAdherenceTech(List<Registration> registrations){
        int techId = getAdhTechEnumerator().getId();
        return registrations.stream().filter(reg -> reg.getAdTechId() == techId).collect(Collectors.toList());
    }

    @Override
    public CompletableFuture<List<Long>> regenerateAdherence(Set<Long> iamIds) {
        LOGGER.info("[NNDCommonHandler] Regenerate Adherence started for entities: " + iamIds.size());
        List<Long> logs = new ArrayList<>();
        List<Character> callLogGeneratedValueList = getAdherenceCodesToRegenerate();
        List<CompletableFuture<List<Long>>> completableFutureList = new ArrayList<>();
        for (Long id : iamIds) {
            /*
             * delete all previous phone call adherence data
             * DO NOT DELETE MANUALLY MARKED DOSES (values = 2 or 9)
             */
            deleteAdherenceLogByIamIdAndAdherenceCodes(callLogGeneratedValueList, id);
            Registration registrationForIamId = registrationRepository.findById(id).get();
            Date callLogsUpperBoundDate = registrationForIamId.getEndDate() == null ? Utils.getCurrentDate() : registrationForIamId.getEndDate();
            List<CallLogs> callLogs = callLogRepository.getAllByIamIdOrderByClientTimeCustom(id, registrationForIamId.getStartDate(), callLogsUpperBoundDate);
            completableFutureList.add(computeCompleteAdherenceString(callLogs, registrationForIamId));
        }
        CompletableFuture.allOf(completableFutureList.toArray(new CompletableFuture[0])).join();
        LOGGER.info("[NNDCommonHandler] Regenerated Successfully for entities: " + iamIds.size());
        regenerateAdherenceCount = 1;
        completableFutureList.forEach(f -> {
            try {
                logs.addAll(f.get());
            } catch (InterruptedException | ExecutionException e) {
                throw new InternalServerErrorException("Internal Server Error, Try again later!");
            }
        });
        return CompletableFuture.completedFuture(logs);
    }

    public abstract List<Character> getAdherenceCodesToRegenerate();

    @Override
    public List<AdhTechLogsData> getTechLogs(Long iamId) {
        List<AdhTechLogsData> adhTechLogs = new ArrayList<>();
        List<PhoneMap> phones = getPhoneMapUsingIamId(iamId);
        Registration registration = registrationRepository.getOne(iamId);
        phones.forEach(ph -> {
            Date phoneEndDate = ph.getStopDate() == null ? Utils.getCurrentDate() : ph.getStopDate();
            Date registrationEndDate = registration.getEndDate() == null ? Utils.getCurrentDate() : registration.getEndDate();
            callLogRepository.getCallLogsByPhone(ph.getPhoneNumber(), phoneEndDate, registration.getStartDate(), registrationEndDate)
                    .forEach(f -> {
                        adhTechLogs.add(new AdhTechLogsData(f.getClientTime(), null, f.getCaller(), f.getNumberDialled(), null));
                    });
        });
        return adhTechLogs;
    }

    public List<AdherenceStringLog> computeSingleAdherenceCodeForDay(List<Registration> activeRegistrations, Date calledDate) {

        List<BigInteger> activeIamIdsForAllTech = activeRegistrations.stream().map(reg -> BigInteger.valueOf(reg.getId())).collect(Collectors.toList());
        boolean shared = activeIamIdsForAllTech.size() > 1;
        List<Registration> activeRegistrationsForTech = getRegistrationsForAdherenceTech(activeRegistrations);
        List<BigInteger> activeIamIds = activeRegistrationsForTech.stream().map(reg -> BigInteger.valueOf(reg.getId())).collect(Collectors.toList());
        LOGGER.info("[NNDCommonHandler] computeSingleAdherenceCodeForDay for entities: " + activeIamIds);
        List<AdherenceStringLog> logs = new ArrayList<>();
        for (Registration registrationForIamId : activeRegistrationsForTech) {
            Long id = registrationForIamId.getId();
            ScheduleHandler scheduleHandler = scheduleTypeHandlerMap.getHandler(registrationForIamId.getScheduleTypeId());
            char adherenceCode = AdherenceCodeEnum.RECEIVED.getCode();
            Date dateAttributeTo = calledDate;
            List<ScheduleMap> allScheduleMaps = scheduleHandler.findAllScheduleMappings(id);
            Map<Long, Schedule> scheduleIdScheduleMap = scheduleHandler.getAllSchedules(allScheduleMaps.stream().map(ScheduleMap::getScheduleId).collect(Collectors.toList()))
                    .stream().collect(Collectors.toMap(Schedule::getId, schedule -> schedule));
            if(scheduleHandler.shouldTakeDose(allScheduleMaps, scheduleIdScheduleMap, calledDate)) {
                adherenceCode = scheduleHandler.computeAdherenceCodeForDay(allScheduleMaps,scheduleIdScheduleMap, calledDate);
                dateAttributeTo = scheduleHandler.getDateToAttribute(allScheduleMaps, scheduleIdScheduleMap, calledDate);
            }
            adherenceCode = shared ? AdherenceCodeEnum.RECEIVED_UNSURE.getCode() : adherenceCode;
            //if RECEIVED on scheduled day dont bother to add UNSCHEDULED to adhstring log
            if(adherenceCode == AdherenceCodeEnum.RECEIVED_UNSCHEDULED.getCode()){
                long minutes = TimeUnit.MINUTES.convert(dateAttributeTo.getTime() - registrationForIamId.getStartDate().getTime(), TimeUnit.MILLISECONDS);
                int day = (int) Math.floor((minutes / 60f) / 24f);
                Date startDate = DateUtils.addDays(registrationForIamId.getStartDate(), day);
                Date endDate = DateUtils.addDays(startDate, 1);
                List<AdherenceStringLog> adherenceStringLogs = logRepository.findAllByIamIdOrderByRecordDateDescCustom(id, startDate, endDate);
                if(adherenceStringLogs.size() > 0 && adherenceStringLogs.stream().anyMatch(f -> f.getValue().equals(AdherenceCodeEnum.RECEIVED.getCode())))
                    continue;
            }
            TFNRepeatDto tfnRepeatDto = new TFNRepeatDto(logs, registrationForIamId, shared, calledDate, scheduleHandler, allScheduleMaps, scheduleIdScheduleMap);
            Character code = getTFNRepeatAdherenceCode(tfnRepeatDto);
            adherenceCode = code == null ? adherenceCode : code;
            logs.add(new AdherenceStringLog(id, adherenceCode, Utils.formatDate(dateAttributeTo)));
        }
        LOGGER.info("Codes generated for entities " + activeIamIds.size());
        return logs;
    }

    public abstract Character getTFNRepeatAdherenceCode(TFNRepeatDto tfnRepeatDto);

    @Async
    public CompletableFuture<List<Long>> computeCompleteAdherenceString(List<CallLogs> callLogs, Registration registrationForIamId) {
        Long id = registrationForIamId.getId();
        LOGGER.info("[NNDCommonHandler] Regenerating Adherence for IAM id: " + id + " adhTech: " + registrationForIamId.getAdTechId());
        Date registrationStartDate = registrationForIamId.getStartDate();
        if (registrationForIamId.getAdTechId() != getAdhTechEnumerator().getId())
            return CompletableFuture.completedFuture(new ArrayList<>());
        LOGGER.info("[NNDCommonHandler Regenerating Adherence Count No.] : " + regenerateAdherenceCount++);
        Map<Date, AdherenceStringLog> logs = new HashMap<>();
        TreeMap<Integer, List<CallLogs>> calledDateToNumberDialedMap = new TreeMap<>();
        ScheduleHandler scheduleHandler = scheduleTypeHandlerMap.getHandler(registrationForIamId.getScheduleTypeId());
        List<ScheduleMap> allScheduleMaps = scheduleHandler.findAllScheduleMappings(id);
        Map<Long, Schedule> scheduleIdScheduleMap = scheduleHandler.getAllSchedules(allScheduleMaps.stream().map(ScheduleMap::getScheduleId).collect(Collectors.toList()))
                .stream().collect(Collectors.toMap(Schedule::getId, schedule -> schedule));
        for (CallLogs cl : callLogs) {
            Date clAttributeDate = cl.getClientTime();
            if(scheduleHandler.shouldTakeDose(id, clAttributeDate)) {
                clAttributeDate = scheduleHandler.getDateToAttribute(allScheduleMaps, scheduleIdScheduleMap, clAttributeDate);
                long minutes = TimeUnit.MINUTES.convert(clAttributeDate.getTime() - registrationStartDate.getTime(), TimeUnit.MILLISECONDS);
                int day = (int) Math.floor((minutes / 60f) / 24f);
                calledDateToNumberDialedMap.putIfAbsent(day, new ArrayList<>());
                calledDateToNumberDialedMap.get(day).add(cl);
            }
        }
        for (Map.Entry<Integer, List<CallLogs>> entry : calledDateToNumberDialedMap.entrySet()) {
            List<CallLogs> cls = entry.getValue();
            List<String> currentNumbersDialled = cls.stream().map(CallLogs::getNumberDialled).collect(Collectors.toList());
            boolean calledFromUniqueNumber = false;
            List<BigInteger> activeIamsToTakeDose = new ArrayList<>();
            List<IamScheduleTypeIdDto> activeIamScheduleTypeList;
            Date currentDate = DateUtils.addDays(registrationStartDate, entry.getKey());
            for (CallLogs cl : cls) {
                activeIamsToTakeDose = new ArrayList<>();
                Date clientTime = cl.getClientTime();
                List<BigInteger> activeIams = phoneMapRepository.findAllActiveIamIdsByPhoneNumbers(cl.getCaller(), clientTime);
                activeIamScheduleTypeList = registrationRepository.findByIdIn(activeIams.stream().map(BigInteger::longValue).collect(Collectors.toList()))
                        .stream().map(registration -> new IamScheduleTypeIdDto(new BigInteger(String.valueOf(registration.getId().longValue())), new BigInteger(String.valueOf(registration.getScheduleTypeId())))).collect(Collectors.toList());
                for(IamScheduleTypeIdDto iamScheduleTypeIdDto : activeIamScheduleTypeList) {
                    ScheduleHandler handler = scheduleTypeHandlerMap.getHandler(((BigInteger) iamScheduleTypeIdDto.getScheduleTypeId()).longValue());
                    if(handler.shouldTakeDose((iamScheduleTypeIdDto.getIamId()).longValue(), clientTime)) {
                        activeIamsToTakeDose.add(iamScheduleTypeIdDto.getIamId());
                    }
                }
                // calledFromUniqueNumber - use to check if we have already received a call that day,
                // if we have, don't bother checking again for that day since precedence of '4' > '5'
                AdherenceStringLog stringLog = logs.get(currentDate);
                if (null != stringLog) {
                    calledFromUniqueNumber = stringLog.getValue().equals(AdherenceCodeEnum.RECEIVED.getCode());
                }
                if (activeIamsToTakeDose.contains(new BigInteger(id.toString())) && !calledFromUniqueNumber) {
                    boolean shared = activeIamsToTakeDose.size() > 1;
                    char adherenceCode = scheduleHandler.computeAdherenceCodeForDay(allScheduleMaps, scheduleIdScheduleMap, clientTime);
                    adherenceCode = shared ? AdherenceCodeEnum.RECEIVED_UNSURE.getCode() : adherenceCode;
                    clientTime = scheduleHandler.getDateToAttribute(allScheduleMaps, scheduleIdScheduleMap, clientTime);
                    if (null == stringLog) {
                        logs.put(currentDate, new AdherenceStringLog(id, adherenceCode, clientTime));
                    } else {
                        stringLog.setValue(adherenceCode);
                        stringLog.setRecordDate(clientTime);
                    }
                }
            }
        }
        applyTfnFlagging(calledDateToNumberDialedMap, registrationForIamId, logs);
        updateRegistrationAfterRegen(id, new ArrayList<>(logs.values()));
        LOGGER.info("[NNDCommonHandler] Regenerating Adherence finished for IAM id: " + id);
        return CompletableFuture.completedFuture(updateAdherenceLogBulk(new ArrayList<>(logs.values())));
    }

    public abstract void applyTfnFlagging(TreeMap<Integer, List<CallLogs>> calledDateToNumberDialedMap, Registration registration, Map<Date, AdherenceStringLog> logs);

    public void setTFNRepeatValue(AdherenceStringLog stringLog, Date date) {
        stringLog.setValue(stringLog.getValue().equals(AdherenceCodeEnum.RECEIVED.getCode()) ? AdherenceCodeEnum.TFN_REPEAT_RECEIVED.getCode() : AdherenceCodeEnum.TFN_REPEAT_RECEIVED_UNSURE.getCode());
        stringLog.setRecordDate(date);
    }

    @Override
    public abstract boolean eligibleForUpdate(char before, char after);

    public boolean savePhoneNumbers(List<PhoneMap> phoneMap, Long clientId) {
        LOGGER.info("[NNDCommonHandler] Regenerating Adherence initiated for IAM id: " + phoneMap);
        List<String> phones = phoneMap.stream().map(PhoneMap::getPhoneNumber).distinct().collect(Collectors.toList());
        phoneMapRepository.saveAll(phoneMap);
        emitEventForRegen(phones);
        return true;
    }

    public void deactivatePhoneNumbersByIamIds(Set<Long> iamIds){
        deactivatePhoneNumbersByIamIds(iamIds, Utils.getCurrentDate());
    }

    public void deactivatePhoneNumbersByIamIds(Set<Long> iamIds, Date date) {
        phoneMapRepository.deactivatePhonesByIamIds(iamIds, date);
    }

    public void deactivateOldPhonesAndAddNew(PhoneEditDto phones) {
        List<PhoneMap> phoneMap = phoneMapRepository.findAllByIamId(phones.getIamId());
        List<String> newPhones = phones.getNumbers();
        //add primary phone number
        newPhones.add(phones.getPrimaryPhone());
        //select only distinct numbers
        newPhones = newPhones.stream().distinct().collect(Collectors.toList());
        List<String> registeredPhones = phoneMap.stream().map(PhoneMap::getPhoneNumber).collect(Collectors.toList());
        List<String> phonesToActivate = newPhones.stream().distinct().filter(registeredPhones::contains).collect(Collectors.toList());
        List<String> phonesToAdd = Utils.removeAll(registeredPhones, newPhones);
        List<String> phoneToDeactivate = Utils.removeAll(newPhones, registeredPhones);
        LOGGER.info("[deactivateOldPhonesAndAddNew] iamId: " +phones.getIamId() + " phonesToReActivate: " + phonesToActivate + " phoneToDeactivate: " + phoneToDeactivate);
        if (phonesToAdd.size() > 0) {
            phoneMapRepository.saveAll(
                    phonesToAdd
                            .stream()
                            .map(m -> new PhoneMap(m, phones.getIamId()))
                            .collect(Collectors.toList())
            );
        }
        //update unique identifier
        registrationRepository.updateUniqueIdentifier(phones.getPrimaryPhone(), phones.getIamId(), Utils.getCurrentDate());
        if (phoneToDeactivate.size() > 0)
            phoneMapRepository.deactivateOldPhones(phoneToDeactivate, Utils.getCurrentDate(), phones.getIamId());
        //reactive phones
        if(phonesToActivate.size() > 0)
            phoneMapRepository.updateEndDate(phonesToActivate, phones.getIamId(), null);
        // regenerate adherence
        emitEventForRegen(newPhones);
    }

    private List<String> getPhonesUsingIamId(Long iamId) {
        return getPhoneMapUsingIamId(iamId)
                .stream()
                .map(PhoneMap::getPhoneNumber)
                .collect(Collectors.toList());
    }

    private List<PhoneMap> getPhoneMapUsingIamId(Long iamId) {
        return phoneMapRepository.findAllByIamId(iamId);
    }

    private List<PhoneMap> getPhoneMapByIamIdsIn(Set<Long> iamIds) {
        return phoneMapRepository.findAllByIamIdIn(iamIds);
    }

    private List<Long> getIamIdsUsingPhones(List<String> phones) {
        return phoneMapRepository
                .getActiveIamIdsByPhoneNumber(phones)
                .stream()
                .map(BigInteger::longValue)
                .collect(Collectors.toList());
    }

    private List<String> cleanDummyPhones(List<String> phoneNumberList) {
        return phoneNumberList.stream().filter(CacheUtils::hasKey).collect(Collectors.toList());
    }

    @Override
    public List<Long> getIamIdsByAdherenceTechAndUniqueIdentifier(String uniqueIdentifier) {
        return getIamIdsUsingPhones(Collections.singletonList(uniqueIdentifier));
    }

    @Override
    public void deleteRegistration(Set<Long> iamIds) {
        super.deleteRegistration(iamIds);
        List<String> phoneNumbers = getPhoneMapByIamIdsIn(iamIds).stream().map(PhoneMap::getPhoneNumber).collect(Collectors.toList());
        phoneMapRepository.deleteAllByIamIdIn(iamIds);
        List<Long> iamIdsToRegen = getIamIdsUsingPhones(phoneNumbers);
        if (!CollectionUtils.isEmpty(iamIdsToRegen)) {
            applicationEventPublisher.publishEvent(new RegenerateAdherenceRequest(iamIdsToRegen));
        }
    }

    /**
     * Method to publish unregisteredPhoneNumber event to be handled by Spring event handler
     * @param caller - Call Source Number
     * @param numberDialled - Call Target Number
     */
    private void handleCallFromUnregisteredPhoneNumber(String caller, String numberDialled){
        // Generate event request
        ProcessUnregisteredPhoneNumberRequest unregisteredPhoneNumberRequest = new ProcessUnregisteredPhoneNumberRequest(caller, numberDialled);
        // Publish Spring event
        applicationEventPublisher.publishEvent(unregisteredPhoneNumberRequest);
    }
}

package com.everwell.iam.handlers.impl;

import com.everwell.iam.enums.AdherenceCodeEnum;
import com.everwell.iam.enums.AdherenceTech;
import com.everwell.iam.handlers.ScheduleHandler;
import com.everwell.iam.models.db.AdherenceStringLog;
import com.everwell.iam.models.db.CallLogs;
import com.everwell.iam.models.db.Registration;
import com.everwell.iam.models.dto.TFNRepeatDto;
import com.everwell.iam.repositories.CallLogRepository;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
public class NNDHandler extends NNDCommonHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(NNDHandler.class);

    @Autowired
    private CallLogRepository callLogRepository;

    @Override
    public AdherenceTech getAdhTechEnumerator() {
        return AdherenceTech.NNDOTS;
    }

    @Override
    public boolean isHandleCommonOperation (){ return true; }

    @Override
    public Character getTFNRepeatAdherenceCode(TFNRepeatDto tfnRepeatDto) {
        Character adherenceCode = null;
        Long id = tfnRepeatDto.getRegistrationForIamId().getId();
        if(tfnRepeatDto.getScheduleHandler().shouldFlagTFNRepeatForNND()) {
            List<CallLogs> allCallLogsTillDate = callLogRepository.getAllByIamIdOrderByClientTimeLimitByDateCustom(id, tfnRepeatDto.getCalledDate());
            TreeMap<Integer, List<CallLogs>> calledDateToNumberDialedMap = new TreeMap<>();
            //get all records decs by recorded date
            for (CallLogs cl : allCallLogsTillDate) {
                Date clAttributeTo = cl.getClientTime();
                if(tfnRepeatDto.getScheduleHandler().shouldTakeDose(tfnRepeatDto.getAllScheduleMaps(), tfnRepeatDto.getScheduleIdScheduleMap(), clAttributeTo)) {
                    clAttributeTo = tfnRepeatDto.getScheduleHandler().getDateToAttribute(tfnRepeatDto.getAllScheduleMaps(), tfnRepeatDto.getScheduleIdScheduleMap(), clAttributeTo);
                    long minutes = TimeUnit.MINUTES.convert(clAttributeTo.getTime() - tfnRepeatDto.getRegistrationForIamId().getStartDate().getTime(), TimeUnit.MILLISECONDS);
                    int day = (int) Math.floor((minutes / 60f) / 24f);
                    if (calledDateToNumberDialedMap.containsKey(day)) {
                        List<CallLogs> nd = calledDateToNumberDialedMap.get(day);
                        nd.add(cl);
                        calledDateToNumberDialedMap.put(day, nd);
                    } else {
                        List<CallLogs> nd = new ArrayList<>();
                        nd.add(cl);
                        calledDateToNumberDialedMap.put(day, nd);
                    }
                }
            }
            List<String> todayCalls = new ArrayList<>();
            List<String> secondLastDaysNumberDialed = new ArrayList<>();
            List<String> thirdLastDaysNumberDialed = new ArrayList<>();
            int i = 1;
            int mapSize = calledDateToNumberDialedMap.size();
            Integer lastUpdatedValueDate = 0;
            for (Map.Entry<Integer, List<CallLogs>> entry : calledDateToNumberDialedMap.entrySet()) {
                if ((mapSize) == i) {
                    todayCalls = calledDateToNumberDialedMap.get(entry.getKey()).stream().map(CallLogs::getNumberDialled).collect(Collectors.toList());
                }
                if ((mapSize - 1) == i) {
                    lastUpdatedValueDate = entry.getKey();
                    secondLastDaysNumberDialed.addAll(calledDateToNumberDialedMap.get(entry.getKey()).stream().map(CallLogs::getNumberDialled).collect(Collectors.toList()));
                }
                if ((mapSize - 2) == i) {
                    thirdLastDaysNumberDialed.addAll(calledDateToNumberDialedMap.get(entry.getKey()).stream().map(CallLogs::getNumberDialled).collect(Collectors.toList()));
                }
                i++;
            }
            if (calledDateToNumberDialedMap.size() > 2) {
                if (secondLastDaysNumberDialed.containsAll(todayCalls) && thirdLastDaysNumberDialed.containsAll(todayCalls)) {
                    //also flagging the previous day if it was not flagged
                    Calendar getDate = Calendar.getInstance();
                    getDate.setTime(tfnRepeatDto.getRegistrationForIamId().getStartDate());
                    getDate.add(Calendar.DATE, lastUpdatedValueDate);
                    Date startDate = getDate.getTime();
                    getDate.add(Calendar.DATE, 1);
                    Date endDate = getDate.getTime();
                    List<AdherenceStringLog> adherenceStringLogs = logRepository.findAllByIamIdOrderByRecordDateDescCustom(id, startDate, endDate);
                    if (adherenceStringLogs.size() > 0) {
                        boolean callReceived = adherenceStringLogs.stream().anyMatch(f -> f.getValue().equals(AdherenceCodeEnum.RECEIVED.getCode()));
                        char sameTFNCode = AdherenceCodeEnum.TFN_REPEAT_RECEIVED.getCode();
                        if (!callReceived) {
                            sameTFNCode = AdherenceCodeEnum.TFN_REPEAT_RECEIVED_UNSURE.getCode();
                        }
                        tfnRepeatDto.getLogs().add(new AdherenceStringLog(id, sameTFNCode, startDate));
                    }
                    adherenceCode = tfnRepeatDto.shared ? AdherenceCodeEnum.TFN_REPEAT_RECEIVED_UNSURE.getCode() : AdherenceCodeEnum.TFN_REPEAT_RECEIVED.getCode();
                }
            }
        }
        return adherenceCode;
    }

    @Override
    public List<Character> getAdherenceCodesToRegenerate(){
        return Arrays.asList(
                AdherenceCodeEnum.RECEIVED.getCode(),
                AdherenceCodeEnum.RECEIVED_UNSCHEDULED.getCode(),
                AdherenceCodeEnum.RECEIVED_UNSURE.getCode(),
                AdherenceCodeEnum.TFN_REPEAT_RECEIVED_UNSURE.getCode(),
                AdherenceCodeEnum.TFN_REPEAT_RECEIVED.getCode()
        );
    }

    @Override
    public boolean eligibleForUpdate(char before, char after) {
        if (before == AdherenceCodeEnum.RECEIVED_UNSCHEDULED.getCode() && after == AdherenceCodeEnum.RECEIVED.getCode()) return true;
        if (before != AdherenceCodeEnum.RECEIVED.getCode() && after == AdherenceCodeEnum.RECEIVED_UNSCHEDULED.getCode()) return true;
        if (before == AdherenceCodeEnum.RECEIVED_UNSCHEDULED.getCode()) return false;
        if (before == AdherenceCodeEnum.TFN_REPEAT_RECEIVED.getCode()) return false;
        if (before == AdherenceCodeEnum.NO_INFO.getCode() || after == AdherenceCodeEnum.TFN_REPEAT_RECEIVED.getCode()) return true;
        if (before == AdherenceCodeEnum.RECEIVED.getCode() && (after == AdherenceCodeEnum.TFN_REPEAT_RECEIVED_UNSURE.getCode())) return true;
        if (before == AdherenceCodeEnum.RECEIVED_UNSURE.getCode() && (after == AdherenceCodeEnum.RECEIVED.getCode() || after == AdherenceCodeEnum.TFN_REPEAT_RECEIVED_UNSURE.getCode())) return true;
        if ((before == AdherenceCodeEnum.MANUAL.getCode() || before == AdherenceCodeEnum.MISSED.getCode()) && (after == AdherenceCodeEnum.NO_INFO.getCode() || after == AdherenceCodeEnum.RECEIVED.getCode() || after == AdherenceCodeEnum.TFN_REPEAT_RECEIVED_UNSURE.getCode() || after == AdherenceCodeEnum.RECEIVED_UNSURE.getCode())) return true;
        if ((before != AdherenceCodeEnum.MANUAL.getCode() && before != AdherenceCodeEnum.MISSED.getCode() && before != AdherenceCodeEnum.RECEIVED.getCode()) && (after == AdherenceCodeEnum.PATIENT_MANUAL.getCode() || after == AdherenceCodeEnum.PATIENT_MISSED.getCode())) return true;
        if (before == AdherenceCodeEnum.PATIENT_MISSED.getCode() || before == AdherenceCodeEnum.PATIENT_MANUAL.getCode()) return true;
        return false;
    }

    @Override
    public void applyTfnFlagging(TreeMap<Integer, List<CallLogs>> calledDateToNumberDialedMap, Registration registration, Map<Date, AdherenceStringLog> logs) {
        ScheduleHandler scheduleHandler = scheduleTypeHandlerMap.getHandler(registration.getScheduleTypeId());
        int repeated = 0;
        List<String> prevCalls = new ArrayList<>();
        Date prevDate = null;
        for (Map.Entry<Integer, List<CallLogs>> entry : calledDateToNumberDialedMap.entrySet()) {
            List<String> currentNumbersDialled = entry.getValue().stream().map(CallLogs::getNumberDialled).collect(Collectors.toList());
            Date currentDate = DateUtils.addDays(registration.getStartDate(), entry.getKey());
            if (scheduleHandler.shouldFlagTFNRepeatForNND()) {
                repeated = prevCalls.containsAll(currentNumbersDialled) ? repeated + 1 : 0;
                //flag second day
                if (repeated == 2)
                    setTFNRepeatValue(logs.get(prevDate), logs.get(prevDate).getRecordDate());
                if (repeated >= 2)
                    setTFNRepeatValue(logs.get(currentDate), logs.get(currentDate).getRecordDate());
                prevCalls = currentNumbersDialled;
                prevDate = currentDate;
            }
        }
    }
}

package com.everwell.iam.handlers.impl;

import com.everwell.iam.enums.AdherenceCodeEnum;
import com.everwell.iam.enums.ScheduleTypeEnum;
import com.everwell.iam.handlers.ScheduleHandler;
import com.everwell.iam.models.db.AdherenceStringLog;
import com.everwell.iam.models.db.Schedule;
import com.everwell.iam.models.db.ScheduleMap;
import com.everwell.iam.models.dto.DoseTimeData;
import com.everwell.iam.models.dto.ScheduleSensitivity;
import com.everwell.iam.utils.Utils;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.TimeUnit;

@Component
public class DailyScheduleHandler extends ScheduleHandler {

    @Override
    public ScheduleTypeEnum getScheduleTypeEnumerator() {
        return ScheduleTypeEnum.DAILY;
    }

    @Override
    public void validateNoOverlapSchedule(Schedule schedule, ScheduleSensitivity sensitivity) {

    }

    @Override
    public String getScheduleValue(String scheduleString) {
        return null;
    }

    @Override
    public String getScheduleString(String scheduleValue) {
        return "";
    }

    @Override
    public boolean shouldTakeDose(Long iamId, Date calledDate) {
        return true;
    }

    @Override
    public boolean shouldTakeDoseForSchedule(ScheduleMap scheduleMap, Date calledDate) {
        return true;
    }

    @Override
    public boolean shouldTakeDoseForSchedule(ScheduleMap scheduleMap, Schedule schedule, Date calledDate) {
        return true;
    }

    @Override
    public boolean shouldTakeDose(List<ScheduleMap> scheduleMaps, Map<Long, Schedule> idScheduleMap, Date calledDate) {
        return true;
    }

    @Override
    public char computeAdherenceCodeForDay(List<ScheduleMap> scheduleMaps, Map<Long, Schedule> idScheduleMap, Date calledDate) {
        return AdherenceCodeEnum.RECEIVED.getCode();
    }

    @Override
    public Date getDateToAttribute(List<ScheduleMap> scheduleMaps, Map<Long, Schedule> idScheduleMap, Date calledDate) {
        return calledDate;
    }

    @Override
    public String generateAdherenceString(HashMap<Integer, AdherenceStringLog> callMap, Date startDate, Date endDate, long iamId, HashMap<Integer, Integer> doseFrequencyMap) {
        Calendar start = Calendar.getInstance();
        start.setTime(startDate);
        Calendar end = Calendar.getInstance();
        end.setTime(endDate);
        StringBuilder adherenceStringBuilder = new StringBuilder();
        for (Date date = start.getTime(); start.before(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
            long minutes = TimeUnit.MINUTES.convert(date.getTime() - startDate.getTime(), TimeUnit.MILLISECONDS);
            int day = (int) Math.floor((minutes / 60f) / 24f);
            adherenceStringBuilder.append((null == callMap.get(day))?AdherenceCodeEnum.NO_INFO.getCode():callMap.get(day).getValue());
        }
        return adherenceStringBuilder.toString();
    }

    @Override
    public boolean shouldFlagTFNRepeatForNND() {
        return true;
    }

    @Override
    public int getNumCharsInBuffer(Long registrationId, Date registrationStartDate, Date registrationEndDate) {
        Date today = new Date();
        if (registrationEndDate != null && registrationEndDate.before(today)) {
            return 0;
        }
        return 1;
    }

    @Override
    protected void validateNoOverlapSchedule(ScheduleSensitivity sensitivity, List<String> doseTime) {

    }

    @Override
    protected void saveScheduleAndCreateTimeMap(Long iamId, Date startDate, Long firstScheduleOffset, ScheduleSensitivity sensitivity, List<String> doseTime) {

    }

    @Override
    public void shouldTakeDoseForDates(Set<Date> dates, Long iamId, List<AdherenceStringLog> logs ) {
    }

    @Override
    public Character getTransformedValue(Character value, Long recordDays, long iamId, List<AdherenceStringLog> logs, Date startDate, Date date) {
        return value;
    }

    @Override
    public List<DoseTimeData> getDoseTimes(Long iamId) {
        return new ArrayList<>();
    }

    @Override
    public boolean isLogsRequired() {
        return false;
    }
}

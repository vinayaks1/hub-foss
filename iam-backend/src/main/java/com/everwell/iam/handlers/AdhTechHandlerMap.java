package com.everwell.iam.handlers;

import com.everwell.iam.enums.AdherenceTech;
import com.everwell.iam.exceptions.ServiceException;
import com.everwell.iam.models.db.AdherenceTechnology;
import com.everwell.iam.repositories.AdhTechRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AdhTechHandlerMap {

    private Map<Long, AdherenceTechHandler> technologyMap = new HashMap<>();

    private Map<AdherenceTech, AdherenceTechHandler> handlerMap = new HashMap<>();

    @Autowired
    private AdhTechRepository adhTechRepository;

    @Autowired
    public AdhTechHandlerMap(List<AdherenceTechHandler> adherenceTechHandlerList) {
        if(!CollectionUtils.isEmpty(adherenceTechHandlerList)) {
            for(AdherenceTechHandler adherenceTechHandler: adherenceTechHandlerList) {
                handlerMap.put(adherenceTechHandler.getAdhTechEnumerator(), adherenceTechHandler);
            }
        }
    }

    @PostConstruct
    public void initialise() {
        for(AdherenceTechnology tech: adhTechRepository.findAll()) {
            AdherenceTech matchedTech = AdherenceTech.getMatch(tech.getName());
            if(null == matchedTech) {
                throw new ServiceException("Error in initialising Adherence Technology Map");
            }
            AdherenceTechHandler handler = handlerMap.get(matchedTech);
            handler.setAdherenceTechId(tech.getId());
            technologyMap.put(tech.getId(), handler);
        }
    }

    public AdherenceTechHandler getHandler(Long adherenceTechId) {
        return technologyMap.get(adherenceTechId);
    }

    public AdhTechRepository getAdhTechRepository() {
        return adhTechRepository;
    }
}

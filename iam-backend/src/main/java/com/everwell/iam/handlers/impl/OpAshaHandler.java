package com.everwell.iam.handlers.impl;

import com.everwell.iam.enums.AdherenceTech;
import com.everwell.iam.handlers.AdherenceTechHandler;
import com.everwell.iam.models.db.CAccess;
import com.everwell.iam.models.db.Registration;
import com.everwell.iam.models.dto.AdhTechLogsData;
import com.everwell.iam.models.http.requests.EntityRequest;
import com.everwell.iam.repositories.AccessMappingRepository;
import com.everwell.iam.repositories.CAccessRepository;
import com.everwell.iam.repositories.RegistrationRepository;

import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

@Component
public class OpAshaHandler extends AdherenceTechHandler {

    @Autowired
    private CAccessRepository accessRepository;

    @Autowired
    private AccessMappingRepository mappingRepository;

    @Autowired
    private RegistrationRepository registrationRepository;

    private static final String MAPPED_CLIENT_NAME = "Nikshay";

    @Override
    public void postRegistration(Registration registration, EntityRequest entityRequest, boolean isNew) {
        super.postRegistration(registration, entityRequest, isNew);
    }

  @Override
    public void preDeletion(Registration registration) {

    }

    @Override
    public CompletableFuture<List<Long>> regenerateAdherence(Set<Long> iamIds) {
        throw new NotImplementedException("Unimplemented method");
    }

  @Override
  public List<AdhTechLogsData> getTechLogs(Long iamId) {
    return new ArrayList<>();
  }

  @Override
    public AdherenceTech getAdhTechEnumerator() {
        return AdherenceTech.OPASHA;
    }

    public Long getClientId() {
        CAccess mappedClient = accessRepository.findByName(MAPPED_CLIENT_NAME);
        return (null == mappedClient)?null:mappedClient.getId();
    }

}

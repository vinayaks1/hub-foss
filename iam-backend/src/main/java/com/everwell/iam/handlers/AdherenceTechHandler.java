package com.everwell.iam.handlers;

import com.everwell.iam.enums.AdherenceCodeEnum;
import com.everwell.iam.enums.AdherenceTech;
import com.everwell.iam.enums.CacheAdherenceStatusToday;
import com.everwell.iam.enums.DosageTypeEnum;
import com.everwell.iam.exceptions.NotFoundException;
import com.everwell.iam.exceptions.ValidationException;
import com.everwell.iam.models.db.AccessMapping;
import com.everwell.iam.models.db.AdherenceStringLog;
import com.everwell.iam.models.db.Registration;
import com.everwell.iam.models.dto.AdhTechLogsData;
import com.everwell.iam.models.dto.AdherenceData;
import com.everwell.iam.models.dto.eventstreaming.analytics.RecordAdherenceTrackingDto;
import com.everwell.iam.models.dto.eventstreaming.analytics.RegisterEntityTrackingDto;
import com.everwell.iam.models.dto.springevents.AnyAdherenceEvent;
import com.everwell.iam.models.dto.springevents.EpisodeEvent;
import com.everwell.iam.models.http.requests.EntityRequest;
import com.everwell.iam.repositories.AccessMappingRepository;
import com.everwell.iam.repositories.AdhStringLogRepository;
import com.everwell.iam.repositories.RegistrationRepository;
import com.everwell.iam.services.RabbitMQPublisherService;
import com.everwell.iam.utils.Utils;
import lombok.Getter;
import lombok.Setter;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.util.CollectionUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public abstract class AdherenceTechHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdherenceTechHandler.class);

    @Getter
    @Setter
    private Long adherenceTechId;

    @Autowired
    public RabbitMQPublisherService rabbitMQPublisherService;

    @Autowired
    protected AdhStringLogRepository logRepository;

    @Autowired
    private AccessMappingRepository mappingRepository;

    @Autowired
    protected RegistrationRepository registrationRepository;

    @Autowired
    protected ScheduleTypeHandlerMap scheduleTypeHandlerMap;

    @Autowired
    public ApplicationEventPublisher applicationEventPublisher;

    public void postRegistration(Registration registration, EntityRequest entityRequest, boolean isNew) {
        // registration successful, now put this event into Matomo
        applicationEventPublisher.publishEvent(new RegisterEntityTrackingDto(entityRequest.getEntityId(), entityRequest.getClientId(), entityRequest.getAdherenceType().longValue(), isNew, getAdhTechEnumerator().name()));
        // Send registration details to RMQ for episode
        applicationEventPublisher.publishEvent(new EpisodeEvent(registration, entityRequest));
      }

    public void publishLastDosageEventOnRabbitMQ(CacheAdherenceStatusToday type, Registration registration, String dosageType)
    {
        boolean isLastDosageEvent = dosageType.equals(DosageTypeEnum.LAST_DOSAGE.getName());
        boolean isLastMissedDosageEvent = dosageType.equals(DosageTypeEnum.LAST_MISSED_DOSAGE.getName());
        applicationEventPublisher.publishEvent(new AnyAdherenceEvent(type, registration, false, isLastDosageEvent,isLastMissedDosageEvent, false,false));
    }

    public void cacheNullDateEntity(Registration registration)
    {
        applicationEventPublisher.publishEvent(new AnyAdherenceEvent(null,registration,false,false,false,false,true));
    }

    public void cacheNullDateEntity(List<Registration> registrations)
    {
        registrations.forEach(reg -> applicationEventPublisher.publishEvent(new AnyAdherenceEvent(null, reg,false,false,false,false,true)));
    }

    public abstract void preDeletion(Registration registration);

    public abstract AdherenceTech getAdhTechEnumerator();

    private int bufferToAvoidSameUploadAndSystemTimeInMinutes = 1;

    public String generateAdherenceString(Long iamId, List<AdherenceStringLog> adhLogs, Date startDate, Date endDate, Long scheduleTypeId) {
        HashMap<Integer, AdherenceStringLog> map = new HashMap<>();
        HashMap<Integer, Integer> doseFrequencyMap = new HashMap<>();
        adhLogs.forEach(al -> {
            long minutes = TimeUnit.MINUTES.convert(al.getRecordDate().getTime() - startDate.getTime(), TimeUnit.MILLISECONDS);
            int day = (int) Math.floor((minutes / 60f) / 24f);
            if (map.containsKey(day)) {
                doseFrequencyMap.put(day, doseFrequencyMap.get(day) + 1);
                char after = al.getValue();
                char before = map.get(day).getValue();
                if (eligibleForUpdate(before, after)) {
                    map.put(day, al);
                } else if (map.get(day).getUpdatedDate().getTime() < al.getUpdatedDate().getTime()) {
                    map.put(day, al);
                }
            } else {
                doseFrequencyMap.put(day, 1);
                map.put(day, al);
            }
        });
        return generateAdherence(map, startDate, endDate, iamId, scheduleTypeId, doseFrequencyMap);
    }

    protected String generateAdherence(HashMap<Integer, AdherenceStringLog> callMap, Date startDate, Date endDate, long iamId, Long scheduleTypeId, HashMap<Integer, Integer> doseFrequencyMap) {
        ScheduleHandler scheduleHandler = scheduleTypeHandlerMap.getHandler(scheduleTypeId);
        if(null == endDate) {
            endDate = Utils.getCurrentDate();
        }
        return scheduleHandler.generateAdherenceString(callMap, startDate, endDate, iamId, doseFrequencyMap);
    }
    protected boolean eligibleForUpdate(char before, char after) {
        return true;
    }

    public String generateAdherenceString(Long iamId, Date startDate, Date endDate, Long scheduleTypeId) {
        List<AdherenceStringLog> logList = logRepository.findAllByIamAndStartDate(startDate, iamId);
        String adherenceString = "";
        if(startDate.before(Utils.getCurrentDate())) {
            adherenceString = generateAdherenceString(iamId, logList, startDate, endDate, scheduleTypeId);
        }
        return adherenceString;
    }

    public List<AdherenceStringLog> getAdherenceStringLogs(Long iamId, Date startDate) {
        List<AdherenceStringLog> logList = logRepository.findAllByIamAndStartDate(startDate, iamId);
        return logList;
    }

    public AdherenceData getAdherenceDataForIam(Long iamId, Date startDate, Date endDate, Long scheduleTypeId, Date lastDosage,Date lastMissedDosage) {
        String regeneratedString = generateAdherenceString(iamId, startDate, endDate, scheduleTypeId);
        AdherenceData adherenceData = new AdherenceData(regeneratedString, getAdhTechEnumerator().getName());
        Date updatedLastDosage = computeLastDosage(regeneratedString, endDate,false);
        adherenceData.setLastDosage(updatedLastDosage);
        Date updatedLastMissedDosage = computeLastDosage(regeneratedString,endDate,true);
        adherenceData.setLastMissedDosage(updatedLastMissedDosage);
        return adherenceData;
    }

    public Registration getActiveIam(String entityId, Long clientId) {
        if (null == entityId) {
            throw new ValidationException("Invalid entityId");
        }

        if (null == clientId) {
            throw new NotFoundException("No client is mapped");
        }

        AccessMapping mapping = mappingRepository.findActiveAccessMappingByEntityIdAndClientId(entityId, clientId);
        if (null == mapping) {
            throw new NotFoundException("Entity needs to be registered: entityId: " + entityId + " clientId: " + clientId);
        }

        Registration iamRegistration = registrationRepository.findById(mapping.getIamId()).orElse(null);
        if (null == iamRegistration) {
            throw new NotFoundException("Registration is not mapped");
        }

        if (!iamRegistration.getAdTechId().equals(getAdherenceTechId())) {
            throw new ValidationException("Registration is not active for this adherence technology");
        }

        return iamRegistration;
    }

    public List<Long> updateAdherenceLogBulk(Long iamId, Map<Date, AdherenceCodeEnum> dateValueMap) {
        List<AdherenceStringLog> adherenceStringLogList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(dateValueMap)) {
            for (Map.Entry<Date, AdherenceCodeEnum> entry : dateValueMap.entrySet()) {
                AdherenceStringLog adherenceStringLog = new AdherenceStringLog(iamId, entry.getValue().getCode());
                adherenceStringLog.setRecordDate(entry.getKey());
                adherenceStringLogList.add(adherenceStringLog);
            }
        }
        logRepository.saveAll(adherenceStringLogList);
        return adherenceStringLogList.stream().map(AdherenceStringLog::getId).collect(Collectors.toList());
    }

    public List<Long> updateAdherenceLogBulk(List<AdherenceStringLog> logs) {
        if (null == logs) {
            return null;
        }
        logRepository.saveAll(logs);
        return logs.stream().map(AdherenceStringLog::getId).collect(Collectors.toList());
    }

    public void deleteAdherenceLogByIamIdAndAdherenceCodes(List<Character> adherenceCodeList, Long iamId) {
        logRepository.deleteByIamIdAndValueIn(iamId, adherenceCodeList);
    }

    public Registration save(EntityRequest entityRequest) {
        Registration newRegistration = new Registration((long) entityRequest.getAdherenceType(), entityRequest.getScheduleTypeId(), entityRequest.getUniqueIdentifier());
        if (null != entityRequest.getStartDate()) {
            try {
                Date startDate = Utils.convertStringToDate(entityRequest.getStartDate());
                newRegistration.setStartDate(startDate);
            } catch (ParseException exception) {
                LOGGER.warn("[createIam] unable to parse date " + entityRequest.getStartDate());
            }
        }
        registrationRepository.save(newRegistration);
        return newRegistration;
    }

    public List<Long> updateAdherenceMap(TreeMap<Date, AdherenceCodeEnum> datesToValueMapList, Registration registration) {
        List<Long> iamLogIds = new ArrayList<>();
        if (registration != null) {
            ScheduleHandler scheduleHandler = scheduleTypeHandlerMap.getHandler(registration.getScheduleTypeId());
            List<AdherenceStringLog> logs = scheduleHandler.isLogsRequired() ? getAdherenceStringLogs(registration.getId(), registration.getStartDate()) : new ArrayList<>();
            scheduleHandler.shouldTakeDoseForDates(datesToValueMapList.keySet(), registration.getId(), logs);
            Date iamStartDayTime = registration.getStartDate();
            Date endDate = (null == registration.getEndDate()) ? DateUtils.addMinutes(Utils.getCurrentDate(), bufferToAvoidSameUploadAndSystemTimeInMinutes) : registration.getEndDate();
            Date iamEndDayTime = endDate;
            if(null == registration.getEndDate())
                iamEndDayTime = new Date(Math.max(iamEndDayTime.getTime(), datesToValueMapList.lastKey().getTime())+1L); // adding 1L so that this date can be included in subMap(startKey,endKey).
            Map<Date, AdherenceCodeEnum> subMap = datesToValueMapList.subMap(iamStartDayTime,iamEndDayTime);
            if (!CollectionUtils.isEmpty(subMap)) {
                Map<Date, AdherenceCodeEnum> iamLogsToSave = subMap;
                if(null == registration.getEndDate()) {
                    // Allow for future saves to adherence log if active registration, the last dosage would be recalculated when recalculating based on adherence string regeneration
                    iamLogsToSave = datesToValueMapList;
                }
                iamLogIds.addAll(updateAdherenceLogBulk(registration.getId(), iamLogsToSave));
                String adherenceString;
                Date maxDate = registration.getLastDosage();
                Date missedDosage = registration.getLastMissedDosage();
                CacheAdherenceStatusToday type = null;
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                for (Map.Entry<Date, AdherenceCodeEnum> entry : subMap.entrySet()) {
                    adherenceString = updateAdherenceString(registration, entry.getKey(), entry.getValue().getCode());
                    registration.setAdherenceString(adherenceString);
                    if ((null == maxDate || (entry.getKey().after(maxDate))) && AdherenceCodeEnum.getCalledAdherenceEnumList().contains(entry.getValue())) {
                        maxDate = entry.getKey();
                    } else if (entry.getValue().equals(AdherenceCodeEnum.NO_INFO) && maxDate != null && sdf.format(entry.getKey()).equals(sdf.format(maxDate))) {
                        // For no info log, calculate last dosage based on adherence string generated (use eligible update precedence order)
                        maxDate = computeLastDosage(adherenceString, registration.getEndDate(),false);
                    }
                    if ((null == missedDosage || (entry.getKey().after(missedDosage))) && AdherenceCodeEnum.getManuallyMissedEnumList().contains(entry.getValue())) {
                        missedDosage = entry.getKey();
                    } else if (entry.getValue().equals(AdherenceCodeEnum.NO_INFO) && missedDosage != null && sdf.format(entry.getKey()).equals(sdf.format(missedDosage))) {
                        // For no info log, calculate last missed dosage based on adherence string generated (use eligible update precedence order)
                        missedDosage = computeLastDosage(adherenceString, registration.getEndDate(),true);
                    }
                    if(type == null){
                        type = extractAdherenceStatusToday(iamStartDayTime, entry.getKey(), entry.getValue());
                    }
                }
                boolean updateLastDosage = ((null == registration.getLastDosage() && null != maxDate) ||(null == maxDate && registration.getLastDosage() != null) || (null != registration.getLastDosage() && null != maxDate && !registration.getLastDosage().equals(maxDate)));
                boolean updateLastMissedDosage = ((null == registration.getLastMissedDosage() && null != missedDosage) ||(null == missedDosage && registration.getLastMissedDosage() != null) || (null != registration.getLastMissedDosage() && null != missedDosage && !registration.getLastMissedDosage().equals(missedDosage)));
                if (updateLastDosage) registration.setLastDosage(maxDate);
                if(updateLastMissedDosage) registration.setLastMissedDosage(missedDosage);
                registrationRepository.save(registration);
                applicationEventPublisher.publishEvent(new AnyAdherenceEvent(type, registration, false, updateLastDosage,updateLastMissedDosage, false,false));

                //successfully recorded doses, send to matomo for tracking
                applicationEventPublisher.publishEvent(new RecordAdherenceTrackingDto(datesToValueMapList, registration, getAdhTechEnumerator().name()));
            }
         }
        return iamLogIds;
    }

    private CacheAdherenceStatusToday extractAdherenceStatusToday(Date startDate, Date date, AdherenceCodeEnum code) {
        CacheAdherenceStatusToday type = null;
        if (Utils.checkIfSameDayByTime(startDate, Utils.getCurrentDate(), date)) {
            if (AdherenceCodeEnum.getDigitallyConfirmedEnumList().contains(code)) {
                type = CacheAdherenceStatusToday.DIGITAL;
            } else if (AdherenceCodeEnum.getManuallyConfirmedEnumList().contains(code)) {
                type = CacheAdherenceStatusToday.MANUAL;
            } else if (AdherenceCodeEnum.getManuallyMissedEnumList().contains(code)) {
                type = CacheAdherenceStatusToday.MISSED;
            }
            else if (code.equals(AdherenceCodeEnum.NO_INFO)) {
                type = CacheAdherenceStatusToday.NO_INFO;
            }
        }
        return type;
    }

    public Date computeLastDosage(String adherenceString, Date endDate, Boolean isMissedDosage) {
        Date lastDay = (null == endDate) ? Utils.getCurrentDate() : endDate;
        Date lastDosage = null;

        List<AdherenceCodeEnum> enumList = isMissedDosage ? AdherenceCodeEnum.getManuallyMissedEnumList() : AdherenceCodeEnum.getCalledAdherenceEnumList();
        for (int i = adherenceString.length() - 1; i >= 0; i--) {
            if (enumList.contains(AdherenceCodeEnum.getByCode(adherenceString.charAt(i)))) {
                lastDosage = DateUtils.addDays(lastDay, -1 * (adherenceString.length() - 1 - i));
                break;
            }
        }
        return lastDosage;
    }

    protected Date computeLastDosage(List<AdherenceStringLog> stringLogs,Boolean isMissedDosage) {
        Date lastDosage = null;
        stringLogs.sort(Comparator.comparing(AdherenceStringLog::getRecordDate));

        List<AdherenceCodeEnum> enumList = isMissedDosage ? AdherenceCodeEnum.getManuallyMissedEnumList() : AdherenceCodeEnum.getCalledAdherenceEnumList();
        for (int i = stringLogs.size() - 1; i >= 0; i--) {
            if (enumList.contains(AdherenceCodeEnum.getByCode(stringLogs.get(i).getValue()))) {
                lastDosage = stringLogs.get(i).getRecordDate();
                break;
            }
        }
        return lastDosage;
    }


    private String updateAdherenceString(Registration registration, Date date, Character value) {
        String adherenceString = registration.getAdherenceString() == null ? "" : registration.getAdherenceString();
        Date endDate = registration.getEndDate();
        if (null == endDate) {
            endDate = Utils.getCurrentDate();
        }
        Long days = Utils.getDifferenceDays(registration.getStartDate(), endDate);
        Long recordDays = Utils.getDifferenceDays(registration.getStartDate(), date);
        ScheduleHandler scheduleHandler = scheduleTypeHandlerMap.getHandler(registration.getScheduleTypeId());
        List<AdherenceStringLog> logs = scheduleHandler.isLogsRequired() ? getAdherenceStringLogs(registration.getId(), registration.getStartDate()) : new ArrayList<>();
        if (Utils.compareDaysDifference(registration.getStartDate(), endDate, adherenceString.length()) && recordDays <= days) {
            Character currentChar = adherenceString.charAt(recordDays.intValue());
            if (eligibleForUpdate(currentChar, value)) {
                Character transformedValue = scheduleHandler.getTransformedValue(value, recordDays, registration.getId(), logs, registration.getStartDate(), date);
                adherenceString = adherenceString.substring(0, recordDays.intValue()) + transformedValue.toString() + adherenceString.substring(recordDays.intValue() + 1);
            }
        } else {
            // Regenerate adherence
            adherenceString = generateAdherenceString(registration.getId(), registration.getStartDate(), registration.getEndDate(), registration.getScheduleTypeId());
        }
        return adherenceString;
    }

    public void updateRegistrationAfterRegen(Long iamId, List<AdherenceStringLog> logs) {
        Optional<Registration> registration = registrationRepository.findById(iamId);
        if (registration.isPresent()) {
            updateRegistrationAfterRegen(registration.get(), logs);
        }
    }

    public void updateRegistrationAfterRegen(Registration registration, List<AdherenceStringLog> logs) {
        Date lastDosage = null;
        CacheAdherenceStatusToday type = null;
        boolean updateLastDosage = false;
        boolean updateLastMissedDosage = false;
        Date lastMissedDosage = null;
        if (logs != null && logs.size() > 0) {
            lastDosage = computeLastDosage(logs,false);
            lastMissedDosage = computeLastDosage(logs,true);
            updateLastDosage = null == registration.getLastDosage() || (null != lastDosage && lastDosage.after(registration.getLastDosage()));
            updateLastMissedDosage = null == registration.getLastMissedDosage() || (null != lastMissedDosage && lastMissedDosage.after(registration.getLastMissedDosage()));
            type = computeAdherenceStatusToday(registration, logs);
        }
        registration.setLastDosage(lastDosage);
        registration.setLastMissedDosage(lastMissedDosage);
        registrationRepository.save(registration);
        applicationEventPublisher.publishEvent(new AnyAdherenceEvent(type, registration, true, updateLastDosage, updateLastMissedDosage,true,false));
    }

    public CacheAdherenceStatusToday computeAdherenceStatusToday(Registration registration, List<AdherenceStringLog> logs) {
        Date iamStartDayTime = registration.getStartDate();
        CacheAdherenceStatusToday type = null;
        logs.sort(Comparator.comparing(AdherenceStringLog::getRecordDate).reversed());
        for (AdherenceStringLog log : logs) {
            AdherenceCodeEnum todayValue = AdherenceCodeEnum.getByCode(log.getValue());
            type = extractAdherenceStatusToday(iamStartDayTime, log.getRecordDate(), todayValue);
            if(type != null)
                break;
        }
        return type;
    }

    public Long getEntityConsecutiveMissedDoses(String adhString, Date entityStartDate, Date lastDosage, AdherenceData registration, Boolean includeToday) {
        int missedDoses = 0;
        if (StringUtils.isNotBlank(adhString)) {
            Date today = Utils.getCurrentDate();
            if (lastDosage == null) {
                lastDosage = DateUtils.addDays(entityStartDate, -1);
            } else if(lastDosage.after(today)) {
                lastDosage = today;
            }

            // Get substring length since last dose and length of buffer period
            int numCharsInBuffer = 0;
            if (registration.getEndDate() != null) {
                today = registration.getEndDate();
            } else {
                ScheduleHandler scheduleHandler = getScheduleHandler(registration.getScheduleType());
                numCharsInBuffer = scheduleHandler.getNumCharsInBuffer(registration.getIamId(), registration.getStartDate(), registration.getEndDate());
            }

            String searchString = adhString;
            if (includeToday) {
                // Remove string before last dose
                // Adding 1 to diff to take care of edge cases when abs diff between two time instances is less than 24 hours but a new day has started.
                // Adding 1 should not affect the calculation of the final value even if an extra day of adherence string is added.
                Long substringLen = Utils.getDifferenceDays(lastDosage, today) + 1;
                if (adhString.length() - substringLen > 0) {
                    searchString = adhString.substring((int) (adhString.length() - substringLen));
                }
            }
            // Remove buffer period if exists
            if (searchString.length() - numCharsInBuffer >= 0) {
                searchString = searchString.substring(0, searchString.length() - numCharsInBuffer);
            }

            // Get relevant adherence substring and strip blanks
            searchString = searchString.replaceAll(String.valueOf(AdherenceCodeEnum.BLANK.getCode()), "");
            
            // Get length of missed doses at end of string
            Pattern p = Pattern.compile(String.valueOf(AdherenceCodeEnum.NO_INFO.getCode()) +"+$");
            Matcher m = p.matcher(searchString);
            Boolean hasMissedDoses = m.find();
            if (hasMissedDoses) {
            String missedDoseString = m.group();
            missedDoses = missedDoseString.length();
            }
        }
        return Long.valueOf(missedDoses);
    }

    public abstract CompletableFuture<List<Long>> regenerateAdherence(Set<Long> iamIds);

    public abstract List<AdhTechLogsData> getTechLogs(Long iamId);

    protected ScheduleHandler getScheduleHandler(Long scheduleTypeId) {
        return scheduleTypeHandlerMap.getHandler(scheduleTypeId);
    }
    
    public List<Long> getIamIdsByAdherenceTechAndUniqueIdentifier(String uniqueIdentifier) {
        return registrationRepository.getIamIdsFromUniqueIdentifierAndAdTechId(uniqueIdentifier, adherenceTechId);
    }

    public void deleteRegistration(Set<Long> iamIds) {
        registrationRepository.deleteAllByIdIn(iamIds);
        logRepository.deleteAllByIamIds(iamIds);
    }

}

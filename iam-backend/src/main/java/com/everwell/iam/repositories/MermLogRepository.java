package com.everwell.iam.repositories;

import com.everwell.iam.models.db.MermLogs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface MermLogRepository extends JpaRepository<MermLogs, Long> {
  @Query(value = "select ml.* from iam_merm_logs ml inner join iam_imei_map m ON ml.imei = m.imei where m.iam_id = :iamId and ml.client_time >= m.start_date and (m.stop_date is null or m.stop_date >= ml.client_time) ORDER BY ml.client_time", nativeQuery = true)
  List<MermLogs> getAllByIamIdOrderByClientTimeCustom(Long iamId);
}
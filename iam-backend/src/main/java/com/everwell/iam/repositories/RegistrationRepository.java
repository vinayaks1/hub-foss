package com.everwell.iam.repositories;

import com.everwell.iam.models.db.Registration;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Set;



@Repository
public interface RegistrationRepository extends JpaRepository<Registration, Long> {

  @Query("SELECT r.id FROM Registration r where r.startDate < :date AND (r.endDate is null OR :date < r.endDate) AND r.id IN :iamIds")
  List<Long> getAllActiveRegistrationsForGivenDate(Date date, List<Long> iamIds);

  @Query(value = "SELECT r.id FROM iam_registration r where r.adtech_id = :adTechId and (r.adh_string is null or r.adh_string = '') LIMIT :maxLimit", nativeQuery = true)
  List<BigInteger> getByAdTechIdCustom(Long adTechId, Long maxLimit);

  @Query(value = "SELECT r.* from iam_registration r inner join iam_access_mapping a ON r.id = a.iam_id where a.entity_id = :entityId AND r.start_date <= :recordDate AND (r.end_date is null OR :recordDate <= r.end_date)", nativeQuery = true)
  Registration getIamIdForDateAndEntityId(String entityId, Date recordDate);

  @Query(value = "SELECT r.id from iam_registration r where unique_identifier = :uniqueIdentifier AND adtech_id = :adTechId AND end_date is null", nativeQuery = true)
  List<Long> getIamIdsFromUniqueIdentifierAndAdTechId(String uniqueIdentifier, Long adTechId);

  @Query(value="SELECT r.id as iamId, r.start_date as startDate,r.end_date as endDate,r.created_date as createdDate, r.adh_string as adherenceString, r.scheduletype_id as scheduleType FROM iam_registration r where r.id IN :iamIds", nativeQuery = true)
  List<RegistrationShortRepo> getAdherenceDetailsFromIamId(List<Long> iamIds);

  @Modifying
  @Transactional
  @Query("UPDATE Registration r SET r.uniqueIdentifier = :uniqIdentifier, r.updatedDate = :date where r.id = :iamId")
  void updateUniqueIdentifier(String uniqIdentifier, Long iamId, Date date);

  List<Registration> findByIdIn(List<Long> ids);

  Registration findByUniqueIdentifierAndEndDateIsNull(String uniqueIdentifier);

  @Modifying
  @Transactional
  @Query("DELETE from Registration where Id in :iamIds")
  void deleteAllByIdIn(Set<Long> iamIds);

}


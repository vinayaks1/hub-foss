package com.everwell.iam.repositories;

import com.everwell.iam.models.db.AdherenceStringLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Set;

@Repository
public interface AdhStringLogRepository extends JpaRepository<AdherenceStringLog, Long> {

    @Query("select a from AdherenceStringLog a where a.recordDate >= :startDate and a.iamId = :iamId ORDER BY a.updatedDate ASC")
    List<AdherenceStringLog> findAllByIamAndStartDate(Date startDate, Long iamId);

    @Query(value = "select * from iam_adhstring_log a where a.iam_id = :iamId and a.record_date >= :startDate and a.record_date < :endDate and (a.value = '5' OR a.value = '4')", nativeQuery = true)
    List<AdherenceStringLog> findAllByIamIdOrderByRecordDateDescCustom(Long iamId, Date startDate, Date endDate);


    @Modifying
    @Transactional
    @Query(value = "DELETE from AdherenceStringLog where iamId in :iamId")
    void deleteAllByIamIds(Set<Long> iamId);

    @Modifying
    @Transactional
    @Query(value = "DELETE from AdherenceStringLog where iamId = :iamId and value in :valueList")
    void deleteByIamIdAndValueIn(Long iamId, List<Character> valueList);

}

package com.everwell.iam.repositories;

import com.everwell.iam.models.db.AccessMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

public interface AccessMappingRepository extends JpaRepository<AccessMapping, Long> {

    //only one active = TRUE mapping will exist
    @Query(value = "SELECT * FROM iam_access_mapping mapping where mapping.client_id = :clientId and mapping.entity_id = :entityId and mapping.active = TRUE", nativeQuery = true)
    AccessMapping findActiveAccessMappingByEntityIdAndClientId(@Param("entityId") String entityId, @Param("clientId") Long clientId);

    @Query(value = "SELECT mapping.entity_id FROM iam_access_mapping mapping where mapping.client_id = :clientId and mapping.iam_id IN :iamIds and mapping.active = TRUE", nativeQuery = true)
    List<String> getEntityIdByIamIdInAndClientIdAndActiveIsTrue(List<Long> iamIds, Long clientId);

    List<AccessMapping> getByClientIdAndEntityIdInAndActiveIsTrue(Long clientId, List<String> entityId);

    //only one active = TRUE mapping will exist
    @Query(value = "SELECT * FROM iam_access_mapping mapping where mapping.client_id = :clientId and mapping.iam_id = :iamId and mapping.active = TRUE", nativeQuery = true)
    AccessMapping findActiveAccessMappingByIamIdAndClientId(Long iamId, Long clientId);

    List<AccessMapping> findByEntityIdIn(List<String> entityIds);

    List<AccessMapping> getByIamIdInAndClientId(List<Long> iamId, Long clientId);

    List<AccessMapping> getByIamId(List<Long> iamId);

    List<AccessMapping> getAllByEntityId(String entityId);

    List<AccessMapping> getAllByEntityIdAndClientId(String entityId, Long clientId);

    List<AccessMapping> getAllByEntityIdInAndClientId(List<String> entityId, Long clientId);

    @Query(value = "SELECT a.* FROM iam_access_mapping a inner join iam_registration r ON a.iam_id = r.id where a.client_id = :clientId and a.entity_id = :entityId AND (a.active = TRUE OR (:startDate < r.end_date or r.end_date is null))", nativeQuery = true)
    AccessMapping findActiveMappingByEntityIdAndClientIdAndDate(@Param("entityId") String entityId, @Param("clientId") Long clientId, Date startDate);

    @Query(value = "SELECT mapping.* FROM iam_access_mapping mapping where mapping.client_id = :clientId and mapping.iam_id IN ( Select map.iam_id from iam_imei_map map where map.imei = :imei and map.stop_date is null) and mapping.active = TRUE", nativeQuery = true)
    AccessMapping findEntityIdByIMEIAndClientId(@Param("imei") String imei, @Param("clientId") Long clientId);

    @Modifying
    @Transactional
    @Query("DELETE from AccessMapping a where a.iamId in :iamIdList")
    void deleteAllByIamIdInCustom(List<Long> iamIdList);

}

package com.everwell.iam.repositories;

import com.everwell.iam.models.db.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ScheduleRepository extends JpaRepository<Schedule, Long> {

    Schedule findByScheduleTypeIdAndValue(Long scheduleTypeId, String value);

    Schedule findByValue(String value);
}

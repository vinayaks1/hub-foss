package com.everwell.iam.repositories;

import com.everwell.iam.models.db.CAccess;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CAccessRepository extends JpaRepository<CAccess, Long> {

    CAccess findByName(String name);
}

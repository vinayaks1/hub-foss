package com.everwell.iam.repositories;

import com.everwell.iam.models.db.ScheduleMap;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

public interface ScheduleMapRepository extends JpaRepository<ScheduleMap, Long> {

    List<ScheduleMap> findAllByIamId(Long iamId);

    ScheduleMap findByActiveTrueAndIamId(Long iamId);

    @Query(value = "select s.iam_id from iam_schedule_map s where s.active = true and s.iam_id in " +
            "(select a.iam_id from iam_access_mapping a where a.client_id = :clientId and a.entity_id = :entityId) ", nativeQuery = true)
    List<Long> findAllIamIdsWithActiveSchedulesByClientIdAndEntityId(Long clientId, String entityId);

    @Query(value = "select * from iam_schedule_map s where  s.iam_id = :iamId and s.start_date < :date and (s.end_date >= :date or s.end_date is null)", nativeQuery = true)
    ScheduleMap findActiveScheduleMapForDate(Long iamId, Date date);
}

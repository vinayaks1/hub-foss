package com.everwell.iam.repositories;

import com.everwell.iam.models.db.ImeiMap;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Set;

@Repository
public interface ImeiMapRepository extends JpaRepository<ImeiMap, Long> {

  @Query("SELECT m from ImeiMap m where m.imei = :number and m.startDate <= :eventDate and (m.stopDate is null or m.stopDate >= :eventDate)")
  List<ImeiMap> findActiveIamIdByImei(String number, Date eventDate);

  @Query("SELECT m from ImeiMap m where m.iamId IN :iamId and m.stopDate is null")
  ImeiMap findActiveImeiByIamId(List<Long> iamId);

  List<ImeiMap> findAllByIamIdIn(List<Long> iamId);

  @Modifying
  @Transactional
  @Query("UPDATE ImeiMap m SET m.stopDate = :date where m.iamId = :id and m.stopDate is null")
  void deallocateImeiByIamIds(Long id, Date date);

  @Modifying
  @Transactional
  @Query("DELETE from ImeiMap where iamId in :iamIds")
  void deleteAllByIamIdIn(Set<Long> iamIds);

}

package com.everwell.iam.repositories;

import com.everwell.iam.models.db.ScheduleTimeMap;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ScheduleTimeMapRepository extends JpaRepository<ScheduleTimeMap, Long> {
    List<ScheduleTimeMap> findAllByScheduleMapIdAndActiveTrue(Long scheduleMapId);

    List<ScheduleTimeMap> findAllByScheduleMapId(Long scheduleMapId);

    List<ScheduleTimeMap> findAllByScheduleMapIdIn(List<Long> scheduleMapId);
}

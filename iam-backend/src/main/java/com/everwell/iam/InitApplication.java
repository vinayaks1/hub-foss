package com.everwell.iam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.annotation.PostConstruct;
import java.util.TimeZone;


@EnableAutoConfiguration
@ComponentScan(basePackages = { "com.everwell.iam"} )
@Configuration
@EnableAsync
@EnableScheduling
public class InitApplication {
    public static void main(String[] args) {
        SpringApplication.run(InitApplication.class, args);
    }

    //To run in UTC in local
//    @PostConstruct
//    public void init(){
//        // Setting Spring Boot SetTimeZone
//        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
//    }
}

package com.everwell.iam.config;

import com.everwell.iam.constants.RabbitMQConstants;
import com.everwell.iam.exceptions.rmq.RmqExceptionStrategy;
import com.everwell.iam.services.RabbitMQPublisherService;
import com.everwell.iam.services.impl.RabbitMQPublisherServiceImpl;
import org.flywaydb.core.Flyway;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.ConditionalRejectingErrorHandler;
import org.springframework.amqp.rabbit.listener.FatalExceptionStrategy;
import org.springframework.amqp.rabbit.listener.RabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.amqp.SimpleRabbitListenerContainerFactoryConfigurer;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Profile;
import org.springframework.util.ErrorHandler;

@Configuration
public class RabbitMQConfig {

    @Value("${q.iam.process_call}")
    private String processCallQueue;

    @Value("${q.iam.regenerate_entity}")
    private String processRegenerateEntityQueue;

    @Value("${q.iam.register_entity}")
    private String registerEntityQueue;

    @Value("${q.iam.process_merm_event}")
    private String processMermEventQueue;

    @Profile("sender")
    @Bean
    public RabbitMQPublisherService sender() {
        return new RabbitMQPublisherServiceImpl();
    }

    //default prefetch count is of 250
    @Bean
    public RabbitListenerContainerFactory<SimpleMessageListenerContainer> rmqPrefetchCount20(
        SimpleRabbitListenerContainerFactoryConfigurer configurer,
        ConnectionFactory rabbitConnectionFactory
    ) {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        configurer.configure(factory, rabbitConnectionFactory);
        factory.setPrefetchCount(20);
        factory.setErrorHandler(new RmqExceptionStrategy());
        return factory;
    }

    @Bean
    Queue processCallQueue() {
       return new Queue(processCallQueue, true);
    }

    @Bean
    Queue processRegenerateEntityQueue() {
        return new Queue(processRegenerateEntityQueue, true);
    }

    @Bean
    Queue registerEntityQueue() {
       return new Queue(registerEntityQueue, true);
    }

    @Bean
    Queue processMermEventQueue() {
       return new Queue(processMermEventQueue, true);
    }
}

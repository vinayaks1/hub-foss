package com.everwell.iam.config;

import com.everwell.iam.filters.CachingRequestBodyFilter;
import com.everwell.iam.services.impl.ClientAuthProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.authentication.AuthenticationProvider;

import java.util.concurrent.Executor;

@Configuration
public class AppConfig {

    @Bean
    public AuthenticationProvider createClientAuthenticationProvider() {
        return new ClientAuthProvider();
    }

    @Bean
    public ServletContextInitializer sentryServletContextInitializer() {
        return new io.sentry.spring.SentryServletContextInitializer();
    }
    @Bean
    public FilterRegistrationBean someFilterRegistration() {

        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new CachingRequestBodyFilter());
        registration.setName("CachingRequestContentFilter");
        registration.setOrder(1);
        return registration;
    }

    @Bean
    public Executor taskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(4);
        executor.setMaxPoolSize(4);
        executor.setQueueCapacity(500);
        executor.setThreadNamePrefix("iam-");
        executor.initialize();
        return executor;
    }

    @Bean("lowThreadHighQueue")
    public Executor taskExecutorLessPriority() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(2);
        executor.setMaxPoolSize(2);
        executor.setQueueCapacity(500);
        executor.setThreadNamePrefix("iam-less-");
        executor.initialize();
        return executor;
    }

}

package com.everwell.iam.config;

import org.hibernate.cfg.CollectionSecondPass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

import static springfox.documentation.builders.PathSelectors.regex;

@EnableSwagger2
@Profile({"dev","beta","oncall"})
@Configuration
public class SwaggerConfig {

    private final String BASE_PACKAGE = "com.everwell.iam";

    @Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage(BASE_PACKAGE))
                .paths(regex("/.*"))
                .build()
                .apiInfo(getApiInfo());
    }

    private ApiInfo getApiInfo() {
        return new ApiInfo(
            "IAM - Api Documentation",
            "Integrated Adherence Management (IAM) Backend is created to abstract out the entity registration and management of adherence across all Everwell services. Documentation Link: [Adherence](https://sites.google.com/a/everwell.org/engineering/tech-integrations/integrated-adherence-management-iam)",
            "V1",
            "",
            new Contact("Everwell", "https://www.everwell.org/", "contact@everwell.org"),
            "",
            "",
            Collections.emptyList());
    }

}

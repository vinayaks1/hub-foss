package com.everwell.iam.annotations;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.security.access.prepost.PreAuthorize;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@PreAuthorize("hasAuthority('AUTHORIZED')")
@ApiImplicitParams({
        @ApiImplicitParam(name = "X-IAM-Access-Token", value = "Authorization token",
                required = true, dataType = "string", paramType = "header") })
public @interface IsAuthorized {
}

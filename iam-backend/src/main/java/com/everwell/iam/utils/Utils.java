package com.everwell.iam.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URL;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.stream.Collectors;


public class Utils {

    private static final Logger LOGGER = LoggerFactory.getLogger(Utils.class);

    private static ObjectMapper objectMapper = new ObjectMapper();

    public static String getMd5(String input) throws NoSuchAlgorithmException {

            // Static getInstance method is called with hashing MD5
            MessageDigest md = MessageDigest.getInstance("MD5");

            // digest() method is called to calculate message digest
            //  of an input digest() return array of byte
            byte[] messageDigest = md.digest(input.getBytes());

            // Convert byte array into signum representation
            BigInteger no = new BigInteger(1, messageDigest);

            // Convert message digest into hex value
            String hashtext = no.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
    }

    public static <T> T convertStringToObject(String json, Class<T> valueType) throws IOException {
        return objectMapper.readValue(json, valueType);
    }

    public static String getFormattedDate(Date date) {
        return getFormattedDate(date, "yyyy-MM-dd HH:mm:ss");
    }

    public static String getFormattedDate(Date date, String format) {
        DateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(date);
    }

    public static Date formatDate(Date date) {
        return formatDate(date, "yyyy-MM-dd HH:mm:ss");
    }

    public static Date formatDate(Date date, String format) {
        try {
            date = convertStringToDate(getFormattedDate(date, format), format);
        } catch (ParseException e) {
            LOGGER.warn("unable to convert record date " + date);
        }
        return date;
    }

    public static Date getDayStartTime(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        return calendar.getTime();
    }

    public static long getDifferenceDays(Date d1, Date d2) {
        return getDifference(d1, d2, TimeUnit.DAYS);
    }

    public static double getExactDifferenceDays(Date d1, Date d2) {
        double diff = d2.getTime() - d1.getTime();
        return diff / 86400000;
    }

    public static long getDifference(Date d1, Date d2, TimeUnit timeUnit) {
        long diff = d2.getTime() - d1.getTime();
        return timeUnit.convert(diff, TimeUnit.MILLISECONDS);
    }

    public static Date convertStringToDate(String date) throws ParseException {
        return convertStringToDate(date, "yyyy-MM-dd HH:mm:ss");
    }

    public static Date convertStringToDate(String date, String format) throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat(format);
        df.setLenient(false);
        return df.parse(date);
    }

    public static Date getCurrentDate() {
        return Utils.formatDate(new Date());
    }

    public static boolean checkIfSameDay(Date date1, Date date2) {
        if (date1 == null || date2 == null ){
            return false;
        }
        SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
        return fmt.format(date1).equals(fmt.format(date2));
    }

//    here we examine if two dates are on the same day for the client
//    simply checking for the "date part" of dateTime will not work
//    for ex: 21,00:00 (IST) => 20,18:30 (UTC)
//            21,21:30 (IST) => 21,16:30 (UTC) {now IST is same date -> 21, but UTC now has 2 dates -> 20,21)
//    so we have to consider using rolling 24 hour concept and check if two dates are within 24 hours.
    public static boolean checkIfSameDayByTime(Date startDate, Date date1, Date date2) {

        if (null == startDate || null == date1 || null == date2) return false;

        long date1Diff = Utils.getDifferenceDays(startDate, date1);
        long date2Diff = Utils.getDifferenceDays(startDate, date2);
        return date1Diff == date2Diff; //day should be equal
    }

    public static String suffix(String s, int len) {
        return s == null ? null : s.substring(Math.max(0, s.length() - len));
    }

    public static String prefix(String s, int len) {
        return s == null ? null : s.substring(0, Math.max(0, s.length() - len));
    }

    public static <T> List<T> removeAll(List<T> itemsToRemove, List<T> removeFrom) {
        List<T> temp = new ArrayList<>(removeFrom);
        temp.removeAll(itemsToRemove);
        return temp;
    }

    public static Date getCurrentDate(String timeZone) {
        return dateFromUTC(getCurrentDate(), timeZone);
    }

    public static Date dateFromUTC(Date date, String timeZone) {
        TimeZone tz = TimeZone.getTimeZone(timeZone);
        return new Date(date.getTime() + tz.getOffset(date.getTime()));
    }

    public static Date dateToUTC(Date date, String timeZone) {
        TimeZone tz = TimeZone.getTimeZone(timeZone);
        return new Date(date.getTime() - tz.getOffset(date.getTime()));
    }

    public static boolean compareDaysDifference(Date startDate, Date endDate, Integer length) {
        if (null == endDate) {
            endDate = Utils.getCurrentDate();
        }
        long minutes = Utils.getDifference(startDate, endDate, TimeUnit.MINUTES);
        int days = (int) Math.ceil((minutes / 60f) / 24f);
        long stringCount = (null == length) ? 0 : length;
        return (days == stringCount);
    }

    public static <T> List<T> filter(List<T> items, Predicate<T> predicate) {
        return items.stream().filter(predicate).collect(Collectors.toList());
    }

    public static String asJsonString(final Object obj) {
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static String readRequestBody(InputStream inputStream) throws IOException {
        StringWriter writer = null;
        if (inputStream != null) {
            writer = new StringWriter();
            IOUtils.copy(inputStream, writer, "UTF-8");
        }
        return writer != null ? writer.toString() : null;
    }

    public static <T> T jsonToObject (String json, Class<T> klass) throws IOException {
        return objectMapper.readValue(json, klass);
    }

    //for generic classes
    public static <T> T jsonToObject (String json, TypeReference<T> typeReference) throws IOException {
//      Disabling the FAIL_ON_UNKNOWN_PROPERTIES so that in case the input json has extra properties
//      other than the properties in the Class that needs to be converted to, jackson mapper doesn't throw exception
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        return objectMapper.readValue(json, typeReference);
    }

    public static int countChar(String s, char c) {
        int count = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == c) {
                count ++;
            }
        }
        return count;
    }

    public static String nullAsEmpty(String s) {
        return s == null ? "" : s;
    }

    public static Long timeToEntityEndDate(Date startDate, TimeUnit timeUnit) {
        Long daysPassed = Utils.getDifferenceDays(startDate, Utils.getCurrentDate()) + 1;
        Date dayEndTime = DateUtils.addDays(startDate, daysPassed.intValue());
        return Utils.getDifference(Utils.getCurrentDate(), dayEndTime, timeUnit);
    }
    
    public static <T> boolean isEmptyOrAllNull(List<T> list) {
        return CollectionUtils.isEmpty(list) || list.stream().noneMatch(Objects::nonNull);
    }
    public static Long getCurrentTime() {
        return new Date().getTime();
    }
}

package com.everwell.iam.utils;

import io.sentry.Sentry;
import io.sentry.event.EventBuilder;
import io.sentry.event.helper.BasicRemoteAddressResolver;
import io.sentry.event.interfaces.ExceptionInterface;
import io.sentry.event.interfaces.HttpInterface;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;
import java.io.InputStream;

@Component
public class SentryUtils {

    private static String releaseVersion;

    @Value("${iam.sentry.release.version}")
    public void setVersion(String version) {
        this.releaseVersion = version;
    }

    public static EventBuilder eventBuilder(Exception ex, WebRequest request, InputStream inputStream) throws IOException {
        EventBuilder builder = new EventBuilder()
                .withSentryInterface(new ExceptionInterface(ex))
                .withRelease(releaseVersion)
                .withSentryInterface(new HttpInterface(((ServletWebRequest) request).getRequest(), new BasicRemoteAddressResolver(), Utils.readRequestBody(inputStream)));

        return builder;
    }

    public static void captureException(Exception e) {
        EventBuilder builder = new EventBuilder()
                .withSentryInterface(new ExceptionInterface(e))
                .withRelease(releaseVersion);
        Sentry.capture(builder);
    }

    public static EventBuilder eventBuilder(Exception ex) {
        EventBuilder builder = new EventBuilder()
                .withRelease(releaseVersion)
                .withSentryInterface(new ExceptionInterface(ex));
        return builder;
    }

}

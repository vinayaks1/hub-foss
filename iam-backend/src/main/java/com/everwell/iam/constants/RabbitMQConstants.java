package com.everwell.iam.constants;

public class RabbitMQConstants {
    public static String LAST_DOSAGE = "q.iam.last_dosage";
    public static String REGENERATE_ADHERENCE = "q.iam.regenerate_entity";
    public static String EX_DIRECT = "direct";
    public static String DLX_EXCHANGE = "ex.iam.dlx";
    public static String WAIT_EXCHANGE = "ex.iam.wait";
    public static String RETRY_COUNT_HEADER = "x-retry-count";
    public static int RMQ_MAX_RETRY_COUNT = 3;
    public static String HEADERS_EXCHANGE = "ex.iam.headers";
    public static String EVENTFLOW_EXCHANGE = "event-input";
    public static String CLIENT_ID = "CLIENT_ID";
    public static String UNREGISTERED_PHONE_NUMBER = "q.iam.process_unregistered_call";
    public static String CLIENT_ID_CAMEL_CASE = "clientId";
    public static String Q_EPISODE_UPDATE_TRACKER = "q.episode.update_tracker";
}

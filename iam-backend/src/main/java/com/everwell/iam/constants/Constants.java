package com.everwell.iam.constants;

public class Constants {

    public static Integer REFRESH_EXPIRY = 2 * 24 * 60 * 60 * 1000; // 2 days
    public final static Long PHONE_VALIDITY_EXPIRY = 2 * 30 * 24 * 60 * 60L; // 2 months
    public final static String CACHE_USER_PREFIX = "iamentity:";
    public final static String CACHE_ADH_STATUS_TODAY_PREFIX = "adhst:";
    public final static String CACHE_AVG_ADHERENCE_PREFIX = "adhavg:";
    public final static String CACHE_ACCESS_TOKEN_PREFIX = "acctok:";
    public final static String CACHE_NULL_DATE_ENTITY_PREFIX = "nullentity:";
    public static Integer MAX_BULK_ADHERENCE_SIZE = 3000;
    public static Integer MAX_ENTITY_AVG_ADHERENCE_SIZE = 1000;
    public final static String RMQ_HEADER_CLIENT_ID = "client_id";
    public static Integer MAX_BULK_IMEI_SIZE = 3000;
    public final static String MY_MODULE = "iam";
    public final static String MONITORING_METHOD = "monitoringMethod";
    public final static String LAST_DOSAGE = "lastDosage";
    public final static String LAST_MISSED_DOSAGE = "lastMissedDosage";
    public final static String ID = "id";

    public static class MatomoEventConstants {
        public final static String SWITCH_EVENT = "SWITCH_";
        public final static String REG_WITH_EVENT = "REG_WITH_";
        public final static String CLOSE_WITH_EVENT = "CLOSE_WITH_";
        public final static String REOPEN_WITH_EVENT = "REOPEN_WITH_";
        public final static String WITH = "_WITH_";
    }
}

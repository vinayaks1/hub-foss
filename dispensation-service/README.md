# Dispensation Service

Dispensation Service is created to abstract out the patient refills and management of drug inventory across all Everwell Services.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Things to be installed to run Dispensation Service

* Java 11
* IntelliJ IDEA / Eclipse or any editor capable of running Java
* Install Maven
* Add Lombok Plugin

For running pure development environment

* Install Postgres
* Install Redis
* Install Rabbitmq

											
### Installing and Deployment

A step by step series of examples that tell you how to get a development env running

Use the deployment commands for easy startup options. 
The options for env are among dev, oncall, everwell-beta, nikshay-prod and hub-prod.
```
mvn clean compile -P$env package
java -jar target/dispensation-service-0.0.1-SNAPSHOT.jar > console.log&
```
The deployment commands builds a new package of the java project and runs the unit tests.
Once the deployment is done, we can look at the log to ascertain the application has started
```
2022-10-25 17:57:57,853 [main:]          o.s.boot.SpringApplication.logStartupProfileInfo:655 INFO  - The following profiles are active: dev
2022-10-25 17:58:02,730 [main:]          c.e.d.config.CacheConfig.jedisConnectionFactory:51 INFO  - Redis Initialization Complete
2022-10-25 12:28:10,418 [main:]          o.s.boot.StartupInfoLogger.logStarted:61 INFO  - Started DispensationServiceApplication in 14.237 seconds (JVM running for 15.408)
```

### Building, Compilation and Packaging

For individual compiling of the project, we run the maven commands to build and compile

```
mvn clean compile
```

For packaging of the project in an executable format.
This would execute all the compilation build and test commands before packaging
```
mvn clean compile package
```

For individual running of the project, we run the maven commands

```
mvn spring-boot:run
```

## Running the tests

Dispensation Service uses JUnit to run tests

Run the following command to evaluate the entire test suite
```
mvn test
```

Run the following command to evaluate individual unit tests
```
mvn test -Dtest=<testfilename>
```

## Built With

* [Springboot](https://spring.io/projects/spring-boot) - The Java framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [Mockito](https://github.com/mockito/mockito) - Used to generate mocks for testing independently
* [PowerMock](https://github.com/powermock/powermock) - Used to mock static, final and other classes that are not supported by mockito.

## License

This project is licensed under the MIT License

package com.everwell.dispensationservice.binders;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface DispensationEventsBinder {
    String EVENTFLOW_EXCHANGE = "event-input";
    String SHARE_DISPENSATION_EXCHANGE = "share-dispensation";

    @Output(EVENTFLOW_EXCHANGE)
    MessageChannel eventOutput();

    @Output(SHARE_DISPENSATION_EXCHANGE)
    MessageChannel shareDispensationEventOutput();
}

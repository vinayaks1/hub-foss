package com.everwell.dispensationservice.models.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;

@Data
@AllArgsConstructor
public class EpisodeFieldsReponse {
    String module;
    Map<String, Object> fields;
    Long clientId;
}

package com.everwell.dispensationservice.models.db;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.everwell.dispensationservice.elasticsearch.indices.ProductIndex;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "DS_Product")
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String drugCode;

    @NotNull
    @Column
    private String productName;

    @Column
    private String dosage;

    @NotNull
    @Column
    private String dosageForm;

    @Column
    private String manufacturer;

    @Column
    private String composition;

    @Column
    private String category;

    public Product(String productName, String drugCode, String dosage, String dosageForm, String manufacturer, String composition, String category) {
        this.productName = productName;
        this.drugCode = drugCode;
        this.dosage = dosage;
        this.dosageForm = dosageForm;
        this.manufacturer = manufacturer;
        this.composition = composition;
        this.category = category;
    }
    public ProductIndex toProductIndex() {
        return  new ProductIndex(
                String.valueOf(id),
                drugCode,
                productName,
                dosage,
                dosageForm,
                manufacturer,
                composition,
                category);
    }
}
package com.everwell.dispensationservice.models.http.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

@Getter
@AllArgsConstructor
@ToString
public class ReturnDispensationResponse {

    private List<Long> returnDispensationIds;
}

package com.everwell.dispensationservice.models.dto;

import lombok.*;

import java.util.Date;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ProductStockData {

    private Long productId;
    private String batchNumber;
    private Date expiryDate ;
    private List<HierarchyData> hierarchyData;


    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Getter
    @Setter
    public static class HierarchyData{
        private Long hierarchyId;
        private Long quantity;
    }

}


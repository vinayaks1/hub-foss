package com.everwell.dispensationservice.models.http.requests;

import com.everwell.dispensationservice.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class RegisterClientRequest {

    private String name;

    private String password;

    public void validate() {
        if(StringUtils.isEmpty(name) || StringUtils.isEmpty(password)) {
            throw new ValidationException("name and password cannot be null or empty");
        }
    }
}

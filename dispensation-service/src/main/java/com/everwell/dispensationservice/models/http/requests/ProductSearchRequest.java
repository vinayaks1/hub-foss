package com.everwell.dispensationservice.models.http.requests;

import com.everwell.dispensationservice.constants.Constants;
import com.everwell.dispensationservice.elasticsearch.constants.ElasticConstants;
import com.everwell.dispensationservice.elasticsearch.dto.BaseSearchRequest;
import com.everwell.dispensationservice.elasticsearch.dto.MatchSearchDto;
import com.everwell.dispensationservice.elasticsearch.dto.QueryDto;
import com.everwell.dispensationservice.elasticsearch.dto.SupportedSearchMethods;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductSearchRequest extends BaseSearchRequest {

    private List<Long> productIds;

    private List<String> drugCodes;

    private String pattern;

    public void setUpDefaultValues() {
        List<MatchSearchDto> matchSearchDtoList = new ArrayList<>();
        matchSearchDtoList.add(new MatchSearchDto(ElasticConstants.PRODUCT_NAME_FIELD, pattern));
        QueryDto queryDto = new QueryDto(matchSearchDtoList);
        this.setSearch(new SupportedSearchMethods(queryDto));
    }

    public void setUpProductSearchRequestForOther() {
        List<MatchSearchDto> matchSearchDtoList = new ArrayList<>();
        matchSearchDtoList.add(new MatchSearchDto(ElasticConstants.PRODUCT_NAME_FIELD, Constants.OTHER));
        QueryDto queryDto = new QueryDto(matchSearchDtoList);
        this.setSearch(new SupportedSearchMethods(queryDto));
        this.setSize(1);
    }

}

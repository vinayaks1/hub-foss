package com.everwell.dispensationservice.models.db;

import com.everwell.dispensationservice.Utils.Utils;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "DS_Client")
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String password;

    private Date createdDate;

    private Long EventFlowId;

    public Client(String name, String password) {
        this.name = name;
        this.password = password;
        this.createdDate = Utils.getCurrentDate();
    }

    public Client(Long id, String name, String password, Date createdDate) {
        this(name, password);
        this.id = id;
        this.createdDate = createdDate;
        this.EventFlowId = null;
    }

}

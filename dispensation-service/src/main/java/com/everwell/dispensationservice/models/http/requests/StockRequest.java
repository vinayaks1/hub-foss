package com.everwell.dispensationservice.models.http.requests;

import com.everwell.dispensationservice.Utils.Utils;
import com.everwell.dispensationservice.constants.Constants;
import com.everwell.dispensationservice.exceptions.ValidationException;
import lombok.*;
import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class StockRequest {

    private Long hierarchyMappingId;

    private Long productId;

    private String batchNumber;

    private String expiryDate;

    private Long quantity;

    private Long externalTransactionId;

    private Long lastUpdatedBy;

    private String reasonForTransaction;

    public void validate() {
        if (null == hierarchyMappingId){
            throw new ValidationException("hierarchy mapping Id is required");
        }

        if (null == productId){
            throw new ValidationException("product Id is required");
        }

        if(null == externalTransactionId){
            throw new ValidationException("transaction Id is required");
        }

        if (StringUtils.isBlank(batchNumber)) {
            throw new ValidationException("batch number is required");
        }

        if (StringUtils.isBlank(expiryDate)) {
            throw new ValidationException("expiry date  is required");
        }

        if(null == quantity){
            throw new ValidationException("quantity is required");
        }

        if(quantity < 0) {
            throw new ValidationException("quantity can not be less than zero");
        }

        if(null == lastUpdatedBy) {
            throw new ValidationException("last updated by is required");
        }

        try{
            if(null != expiryDate) {
                Utils.convertStringToDate(expiryDate);
            }
        } catch (ParseException ex){
                throw new ValidationException("expiry date format is not valid");
            }
        }
}

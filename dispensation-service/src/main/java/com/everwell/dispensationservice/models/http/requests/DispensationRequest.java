package com.everwell.dispensationservice.models.http.requests;

import com.everwell.dispensationservice.Utils.Utils;
import com.everwell.dispensationservice.constants.Constants;
import com.everwell.dispensationservice.exceptions.ValidationException;
import lombok.*;
import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class DispensationRequest {

    private Long entityId;

    private Long weight;

    private String weightBand;

    private String phase;

    private Long addedBy;

    private Long issuingFacility;

    private String issuedDate;

    private String dosingStartDate;

    private Long drugDispensedForDays;

    private String dateOfPrescription;

    private String notes;

    private List<ProductData> productDataList;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ProductData {
        Long productId;
        Long productConfigId;
        Long unitsIssued;
        String dosingSchedule;
        String refillDate;
        String batchNumber;
        String expiryDate;

        public ProductData(Long productId, Long productConfigId, Long unitsIssued, String dosingSchedule) {
            this.productId = productId;
            this.productConfigId = productConfigId;
            this.unitsIssued = unitsIssued;
            this.dosingSchedule = dosingSchedule;
        }

        public ProductData(Long productId, Long productConfigId, Long unitsIssued, String dosingSchedule, String refillDate) {
            this.productId = productId;
            this.productConfigId = productConfigId;
            this.unitsIssued = unitsIssued;
            this.dosingSchedule = dosingSchedule;
            this.refillDate = refillDate;
        }
    }

    public void validate(DispensationRequest dispensationRequest) {
        if(null == dispensationRequest.getEntityId()) {
            throw new ValidationException("entity id is required");
        }
        if(null == dispensationRequest.getAddedBy()) {
            throw new ValidationException("added by is required");
        }

        boolean allValidProductDeploymentConfigIds = true;
        boolean allValidProductIds = true;

        for(ProductData productData : dispensationRequest.getProductDataList()) {
            if(allValidProductIds && null == productData.getProductId()) {
                allValidProductIds = false;
            }
            if(allValidProductDeploymentConfigIds && null == productData.getProductConfigId()) {
                allValidProductDeploymentConfigIds = false;
            }
        }
        if(!allValidProductIds) {
            throw new ValidationException("product id cannot be null");
        }
        if(!allValidProductDeploymentConfigIds) {
            throw new ValidationException("product deployment config cannot be null");
        }

        boolean validIssueDate = false;
        boolean validPrescriptionDate = false;

        try{
            if(null != dispensationRequest.getIssuedDate()) {
                Utils.convertStringToDateWithSupportedDateFormats(dispensationRequest.getIssuedDate());
                validIssueDate = true;
            }
            if(null != dispensationRequest.getDateOfPrescription()) {
                Utils.convertStringToDateWithSupportedDateFormats(dispensationRequest.getDateOfPrescription());
                validPrescriptionDate = true;
            }

        } catch (ParseException ex) {
            String validationMessage = !validIssueDate ? "issued date format is not valid" : !validPrescriptionDate ? "prescription date format is not valid" : "Something went wrong";
            throw new ValidationException(validationMessage);
        }
    }


}

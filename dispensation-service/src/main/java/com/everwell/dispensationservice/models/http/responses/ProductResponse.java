package com.everwell.dispensationservice.models.http.responses;

import com.everwell.dispensationservice.models.db.Product;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
@AllArgsConstructor
public class ProductResponse {

    private Long productId;

    private String drugCode;

    private String productName;

    private String dosage;

    private String dosageForm;

    private String manufacturer;

    private String composition;

    private String category;

    public ProductResponse(Long productId) {
        this.productId = productId;
    }

    public ProductResponse(Product product) {
        this.productId = product.getId();
        this.drugCode = product.getDrugCode();
        this.productName = product.getProductName();
        this.dosage = product.getDosage();
        this.dosageForm = product.getDosageForm();
        this.manufacturer = product.getManufacturer();
        this.composition = product.getComposition();
    }

}

package com.everwell.dispensationservice.models.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Getter
@Setter
@ToString
@AllArgsConstructor

public class ProductInventoryDto {

    private Long id;

    private Long productId;

    private String batchNumber;

    private Date expiryDate;

    public ProductInventoryDto(Long productId, String batchNumber, Date expiryDate) {
        this.productId = productId;
        this.batchNumber = batchNumber;
        this.expiryDate = expiryDate;
    }
}

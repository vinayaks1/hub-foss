package com.everwell.dispensationservice.models.http.requests;

import com.everwell.dispensationservice.exceptions.ValidationException;
import com.everwell.dispensationservice.models.dto.RangeValueFilter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@ToString
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class SearchDispensationRequest {

    private List<Long> entityIds;

    private List<RangeValueFilter> filters;

    public void validate() {
        if(entityIds.isEmpty()) {
            throw new ValidationException("invalid entity list");
        }
        if(null != filters) {
            filters.forEach(RangeValueFilter::validate);
        }
    }
}

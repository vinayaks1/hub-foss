package com.everwell.dispensationservice.models.http.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
public class DispensationResponse {

    private Long dispensationId;

}

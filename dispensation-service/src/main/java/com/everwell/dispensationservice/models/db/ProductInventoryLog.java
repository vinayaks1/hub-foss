package com.everwell.dispensationservice.models.db;

import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.everwell.dispensationservice.Utils.Utils;
import com.everwell.dispensationservice.enums.CommentType;
import com.everwell.dispensationservice.enums.TransactionType;
import com.everwell.dispensationservice.enums.TransactionIdType;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "ds_product_inventory_log")
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class ProductInventoryLog {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "transaction_type")
    private TransactionType transactionType;

    @NotNull
    @Column(name = "transaction_id")
    private Long transactionId;

    @NotNull
    @Column(name = "transaction_quantity")
    private Long transactionQuantity;

    @Column(name = "comment_type")
    private CommentType commentType;

    @Column(name = "comment")
    private String comment;

    @Column(name = "date_of_action")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @Setter
    private Date dateOfAction;

    @Column(name = "p_inventory_id")
    private Long pInventoryId;

    @Column(name = "product_id")
    private Long productId;

    @Column(name = "updated_by")
    private Long updatedBy;

    @Column(name = "transaction_id_type")
    private TransactionIdType transactionIdType;

    public ProductInventoryLog(TransactionType transactionType, Long transactionId, Long transactionQuantity,
                               CommentType commentType, String comment, Long pInventoryId, Long productId, Long updatedBy, TransactionIdType transactionIdType){
        this.transactionType = transactionType;
        this.transactionId = transactionId;
        this.transactionQuantity = transactionQuantity;
        this.commentType = commentType;
        this.comment = comment;
        this.dateOfAction = Utils.getCurrentDate();
        this.pInventoryId = pInventoryId;
        this.productId = productId;
        this.updatedBy = updatedBy;
        this.transactionIdType = transactionIdType;
    }

    public ProductInventoryLog(Long id, TransactionType transactionType, Long transactionId, Long transactionQuantity,
                               CommentType commentType, String comment, Long pInventoryId, Long productId, Long updatedBy, TransactionIdType transactionIdType) {
        this(transactionType, transactionId, transactionQuantity, commentType, comment, pInventoryId, productId, updatedBy, transactionIdType);
        this.id = id;
    }

}
package com.everwell.dispensationservice.models.http.responses;

import com.everwell.dispensationservice.models.db.Client;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
public class ClientResponse {
    private Long id;
    private String name;
    @Setter
    private String password;
    @Setter
    private String authToken;

    public ClientResponse(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public ClientResponse(Client client) {
        this(client.getId(), client.getName());
    }

    public ClientResponse(Long id, String name, String authToken) {
        this(id, name);
        this.authToken = authToken;
    }
}

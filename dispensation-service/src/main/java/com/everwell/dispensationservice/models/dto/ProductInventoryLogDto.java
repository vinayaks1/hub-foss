package com.everwell.dispensationservice.models.dto;

import com.everwell.dispensationservice.Utils.Utils;
import com.everwell.dispensationservice.enums.CommentType;
import com.everwell.dispensationservice.enums.TransactionType;
import com.everwell.dispensationservice.enums.TransactionIdType;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ProductInventoryLogDto {

    private Long id;

    private TransactionType transactionType;

    private Long transactionId;

    private Long transactionQuantity;

    private CommentType commentType;

    private String comment;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private Date dateOfAction;

    private Long pInventoryId;

    private Long productId;

    private Long updatedBy;

    private TransactionIdType transactionIdType;

    private String batchNumber;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private Date expiryDate;

    public ProductInventoryLogDto(TransactionType transactionType, Long transactionId, Long transactionQuantity,
                               CommentType commentType, String comment, Long pInventoryId, Long productId, Long updatedBy, TransactionIdType transactionIdType){
        this.transactionType = transactionType;
        this.transactionId = transactionId;
        this.transactionQuantity = transactionQuantity;
        this.commentType = commentType;
        this.comment = comment;
        this.dateOfAction = Utils.getCurrentDate();
        this.pInventoryId = pInventoryId;
        this.productId = productId;
        this.updatedBy = updatedBy;
        this.transactionIdType = transactionIdType;
    }


}

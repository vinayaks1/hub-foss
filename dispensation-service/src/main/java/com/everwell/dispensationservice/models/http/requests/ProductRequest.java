package com.everwell.dispensationservice.models.http.requests;

import com.everwell.dispensationservice.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ProductRequest {

    private String name;

    private String dosage;

    private String drugCode;

    private String dosageForm;

    private String manufacturer;

    private String composition;

    private String category;

    public void validate(ProductRequest productRequest) {
        if(StringUtils.isBlank(productRequest.getName())) {
            throw new ValidationException("product name is required");
        }

        if(StringUtils.isBlank(productRequest.getDosageForm())) {
            throw new ValidationException("dosage form is required");
        }

        if(StringUtils.isBlank(productRequest.getDosage())) {
            throw new ValidationException("dosage is required");
        }
    }

}

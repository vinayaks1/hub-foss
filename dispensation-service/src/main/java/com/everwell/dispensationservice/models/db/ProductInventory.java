package com.everwell.dispensationservice.models.db;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "ds_product_inventory")
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ProductInventory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "product_id")
    private Long productId;

    @Column(name = "batch_number")
    @Setter
    private String batchNumber;

    @Column(name = "expiry_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @Setter
    private Date expiryDate;

    public ProductInventory(Long productId, String batchNumber, Date expiryDate ){
        this.productId = productId;
        this.batchNumber = batchNumber;
        this.expiryDate = expiryDate;
    }
}

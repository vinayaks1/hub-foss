package com.everwell.dispensationservice;

import com.everwell.dispensationservice.Utils.SentryUtils;
import io.sentry.Sentry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

@SpringBootApplication
public class DispensationServiceApplication {

	private static Logger LOGGER = LoggerFactory.getLogger(DispensationServiceApplication.class);

	public static void main(String[] args) {
		try{
			SpringApplication.run(DispensationServiceApplication.class, args);
		} catch (Exception ex) {
			Sentry.capture(SentryUtils.eventBuilder(ex));
			LOGGER.error(ex.toString());
		}
	}

	@PostConstruct
	public void init() {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	}

}

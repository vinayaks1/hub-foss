package com.everwell.dispensationservice.handlers;

import com.everwell.dispensationservice.Utils.CacheUtils;
import com.everwell.dispensationservice.Utils.Utils;
import com.everwell.dispensationservice.binders.DispensationEventsBinder;
import com.everwell.dispensationservice.binders.EpisodeFieldsBinder;
import com.everwell.dispensationservice.constants.Constants;
import com.everwell.dispensationservice.enums.event.EventCategoryEnum;
import com.everwell.dispensationservice.enums.event.EventNameEnum;
import com.everwell.dispensationservice.models.db.Client;
import com.everwell.dispensationservice.models.db.Dispensation;
import com.everwell.dispensationservice.models.dto.DispensationData;
import com.everwell.dispensationservice.models.dto.DispensationProductDto;
import com.everwell.dispensationservice.models.dto.EpisodeFieldsReponse;
import com.everwell.dispensationservice.models.dto.eventstreaming.EventStreamingDto;
import com.everwell.dispensationservice.models.dto.eventstreaming.EventDto;
import com.everwell.dispensationservice.repositories.ClientRepository;
import com.everwell.dispensationservice.services.DispensationService;
import com.everwell.dispensationservice.services.ProductService;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;

@Component
@EnableBinding({DispensationEventsBinder.class, EpisodeFieldsBinder.class})
public class SpringEventHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(SpringEventHandler.class);

    @Autowired
    DispensationEventsBinder dispensationEventsBinder;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    DispensationService dispensationService;

    @Autowired
    EpisodeFieldsBinder episodeFieldsBinder;

    @Autowired
    ProductService productService;

    private Map<Long, Object> clientIdTriggerIdMap = new HashMap<>();

    @Setter
    @Value("${abdm.m2.enabled:}")
    boolean abdmM2FlowEnabled;

    @PostConstruct
    public void init() {
        for (Client client : clientRepository.findAll()) {
            clientIdTriggerIdMap.put(client.getId(), client.getEventFlowId());
        }
    }

    @EventListener
    public void trackEvent(EventDto eventDto) {
        String category = eventDto.getEventCategory();
        String name = eventDto.getEventName();
        String action = eventDto.getEventAction();
        Long clientId = eventDto.getClientId();
        dispensationEventsBinder.eventOutput().send(MessageBuilder.withPayload(Utils.asJsonString(new EventDto(category, name, action))).setHeader("CLIENT_ID", clientIdTriggerIdMap.get(clientId)).build());
    }

    @EventListener(
            condition = "#eventDto.eventName eq T(com.everwell.dispensationservice.enums.event.EventNameEnum).ADD_PRODUCT.name " +
                    "or #eventDto.eventName eq T(com.everwell.dispensationservice.enums.event.EventNameEnum).ADD_PRODUCT_BULK.name")
    public void invalidateAllProductIdsCacheEntry(EventDto eventDto) {
        LOGGER.info("[invalidateAllProductIdsCacheEntry] - deleting all product ids cache entry");
        productService.deleteAllProductIdsCacheEntry();
    }

    @EventListener(
            condition = "#eventDto.eventName eq T(com.everwell.dispensationservice.enums.event.EventNameEnum).ADD_DISPENSATION.name " +
                    "or #eventDto.eventName eq T(com.everwell.dispensationservice.enums.event.EventNameEnum).DELETE_DISPENSATION.name " +
                    "or #eventDto.eventName eq T(com.everwell.dispensationservice.enums.event.EventNameEnum).RETURN_ENTITY_DISPENSATIONS.name ")
    public void invalidateEntityDispensationCacheEntry(EventDto eventDto) {
        if (eventDto.getEventKeyValuePairs() != null) {
            LOGGER.info("[invalidateEntityDispensationCacheEntry] - deleting entity cache for value -" + eventDto.getEventKeyValuePairs());
            Long entityId = eventDto.getEventKeyValuePairs().get(EventCategoryEnum.ENTITY.getName());
            if (null == entityId) {
                Long dispensationId = eventDto.getEventKeyValuePairs().get(EventCategoryEnum.DISPENSATION.getName());
                Dispensation dispensation = dispensationService.getDispensation(dispensationId, eventDto.getClientId());
                entityId = dispensation.getEntityId();
            }
            if (null != entityId) {
                CacheUtils.deleteFromCache(String.format(Constants.CACHE_ENTITY_DISPENSATION_KEY, entityId, eventDto.getClientId()));
            }
        }
    }

    @EventListener
    public void updateDataForEpisode (EpisodeFieldsReponse episodeFieldsReponse) {
        EventStreamingDto<EpisodeFieldsReponse> eventStreamingDto = new EventStreamingDto<>("episode-field-tracker", episodeFieldsReponse);
        episodeFieldsBinder
                .episodeTrackerPublisher()
                .send(MessageBuilder.withPayload(Utils.asJsonString(eventStreamingDto)).setHeader("clientId", episodeFieldsReponse.getClientId()).build());
    }

    @EventListener(
            condition = "#eventDto.eventName eq T(com.everwell.dispensationservice.enums.event.EventNameEnum).ADD_DISPENSATION.name")
    public void trackShareDispensationEvent(EventDto eventDto) {
        if(abdmM2FlowEnabled) {
            Long dispensationId = eventDto.getEventKeyValuePairs().get(EventCategoryEnum.DISPENSATION.getName());
            Long clientId = eventDto.getClientId();
            Dispensation dispensation = dispensationService.getDispensation(dispensationId, clientId);
            Map<Long, List<DispensationProductDto>> dispensationProductData  = dispensationService.getDispensationIdToDispensationProductDtoMap(Arrays.asList(dispensationId), true);
            DispensationData dispensationData = new DispensationData(dispensation, dispensationProductData.get(dispensationId));
            EventStreamingDto<DispensationData> eventStreamingDto = new EventStreamingDto<>( EventNameEnum.SHARE_DISPENSATION.getName(), dispensationData);
            dispensationEventsBinder.shareDispensationEventOutput().send(MessageBuilder.withPayload(Utils.asJsonString(eventStreamingDto)).setHeader("X-Client-Id", clientId)
                    .build());
        }
    }

}
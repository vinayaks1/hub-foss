package com.everwell.dispensationservice.exceptions;

import com.everwell.dispensationservice.Utils.SentryUtils;
import com.everwell.dispensationservice.models.Response;
import io.sentry.Sentry;
import io.sentry.event.EventBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(CustomExceptionHandler.class);

    private ResponseEntity<Response> handleException(String message, HttpStatus httpStatus) {
        Response errorResponse = new Response(message);
        return new ResponseEntity<>(errorResponse, httpStatus);
    }

    private ResponseEntity<Object> handleExceptionInternal(Exception ex, String message, HttpHeaders headers, List<String> errors, WebRequest request) {
        Response errorResponse = new Response(false, errors, message);
        return handleExceptionInternal(ex, errorResponse, headers, HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(ConflictException.class)
    public final ResponseEntity<Response> handleConflictExceptions(ConflictException ex, WebRequest request) {
        return handleException(ex.getMessage(), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(NotFoundException.class)
    public final ResponseEntity<Response> handleNotFoundExceptions(NotFoundException ex, WebRequest request) {
        return handleException(ex.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ValidationException.class)
    public final ResponseEntity<Response> handleValidationExceptions(ValidationException ex, WebRequest request) {
        return handleException(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Response> handleAllExceptions(Exception ex, WebRequest request, InputStream inputStream) throws IOException {

        EventBuilder builder = SentryUtils.eventBuilder(ex,request,inputStream);
        Sentry.capture(builder);
        LOGGER.error("[handleAllExceptions] exception occurred for request body " + ex);
        return handleException("Internal Server Error occurred", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Override
    public ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        List<String> errors = new ArrayList<String>();
        for (FieldError error : ex.getBindingResult().getFieldErrors()) {
            errors.add(error.getField() + ": " + error.getDefaultMessage());
        }
        for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
            errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
        }

        return handleExceptionInternal(
                ex, ex.getLocalizedMessage(), headers, errors, request);
    }

}

package com.everwell.dispensationservice.controllers;

import com.everwell.dispensationservice.enums.event.EventActionEnum;
import com.everwell.dispensationservice.enums.event.EventCategoryEnum;
import com.everwell.dispensationservice.enums.event.EventNameEnum;
import com.everwell.dispensationservice.models.Response;
import com.everwell.dispensationservice.models.dto.eventstreaming.EventDto;
import com.everwell.dispensationservice.models.http.requests.RegisterClientRequest;
import com.everwell.dispensationservice.models.http.responses.ClientResponse;
import com.everwell.dispensationservice.services.ClientService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.everwell.dispensationservice.constants.Constants.X_DS_CLIENT_ID;

@RestController
public class ClientController {

    private static Logger LOGGER = LoggerFactory.getLogger(ClientController.class);

    @Autowired
    private ClientService clientService;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @RequestMapping(value = "/v1/client", method = RequestMethod.POST)
    @ApiOperation(
            value = "Register new client",
            notes = "Client will be registered with the given credentials"
    )
    public ResponseEntity<Response<ClientResponse>> registerClient(@RequestBody RegisterClientRequest clientRequest) {
        LOGGER.info("[registerClient] request received : " + clientRequest);
        clientRequest.validate();
        ClientResponse clientResponse = clientService.registerClient(clientRequest);
        Response<ClientResponse> response = new Response<>(clientResponse, "client registered successfully");
        applicationEventPublisher.publishEvent(new EventDto(EventCategoryEnum.CLIENT.getName(), EventNameEnum.ADD_CLIENT.getName(), EventActionEnum.DB_RECORD_SAVE.getName(),clientResponse.getId()));
        LOGGER.info("[registerClient] response generated : " + response);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @ApiOperation(
            value = "Get client details",
            notes = "Generates a new access token and will fetch the client details with the token"
    )
    @RequestMapping(value = "/v1/client", method = RequestMethod.GET)
    public ResponseEntity<Response<ClientResponse>> getClientDetails(@RequestHeader(name = X_DS_CLIENT_ID) Long id) {
        LOGGER.info("[getClientDetails] request received for Id :" + id);
        ClientResponse clientResponse = clientService.getClientWithNewToken(id);
        Response<ClientResponse> response = new Response<>(clientResponse, "client details fetched successfully");
        applicationEventPublisher.publishEvent(new EventDto(EventCategoryEnum.CLIENT.getName(), EventNameEnum.GET_CLIENT.getName(), EventActionEnum.DB_RECORD_FETCH.getName(),clientResponse.getId()));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}

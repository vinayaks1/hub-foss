package com.everwell.dispensationservice.controllers;

import com.everwell.dispensationservice.enums.event.EventActionEnum;
import com.everwell.dispensationservice.enums.event.EventCategoryEnum;
import com.everwell.dispensationservice.enums.event.EventNameEnum;
import com.everwell.dispensationservice.models.Response;
import com.everwell.dispensationservice.models.db.Product;
import com.everwell.dispensationservice.models.dto.eventstreaming.EventDto;
import com.everwell.dispensationservice.models.http.requests.ProductBulkRequest;
import com.everwell.dispensationservice.models.http.requests.ProductRequest;
import com.everwell.dispensationservice.models.http.requests.ProductSearchRequest;
import com.everwell.dispensationservice.models.http.responses.ProductResponse;
import com.everwell.dispensationservice.services.ProductService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.everwell.dispensationservice.constants.Constants.X_DS_CLIENT_ID;

@RestController
public class ProductController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

    @Autowired
    private ProductService productService;


    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @ApiOperation(
            value = "Add product",
            notes = "Used to add new product with details such as Manufacturer, composition, drug code etc"
    )
    @RequestMapping(value = "/v1/product", method = RequestMethod.POST)
    public ResponseEntity<Response<ProductResponse>> addProduct(@RequestHeader(name = X_DS_CLIENT_ID) Long clientId,@RequestBody ProductRequest productRequest) {
        LOGGER.info(" [addProduct] request received: " + productRequest);
        productRequest.validate(productRequest);
        Product product =  productService.addProduct(productRequest);
        Response<ProductResponse> response = new Response<>(new ProductResponse(product.getId()), "product added successfully");
        applicationEventPublisher.publishEvent(new EventDto(EventCategoryEnum.PRODUCT.getName(), EventNameEnum.ADD_PRODUCT.getName(), EventActionEnum.DB_RECORD_SAVE.getName(), clientId));
        LOGGER.info(" [addProduct] product added: " + response);
        return new ResponseEntity<>(response, HttpStatus.OK) ;
    }

    @ApiOperation(
            value = "Add multiple product",
            notes = "Used to add new multiple products with details such as Manufacturer, composition, drug code etc"
    )
    @RequestMapping(value = "/v1/products", method = RequestMethod.POST)
    public  ResponseEntity<Response<List<ProductResponse>>> addProductBulk(@RequestHeader(name = X_DS_CLIENT_ID) Long clientId, @RequestBody ProductBulkRequest productBulkRequest) {
        LOGGER.info(" [addProductBulk] request received: " + productBulkRequest);
        productBulkRequest.validate();
        for(ProductRequest productRequest : productBulkRequest.getProductDetails()){
            productRequest.validate(productRequest);
        }
        List<Product> products =  productService.addProductBulk(productBulkRequest);
        List<ProductResponse> productResponseList = productService.getAllProductDetailsFromProducts(products);
        applicationEventPublisher.publishEvent(new EventDto(EventCategoryEnum.PRODUCT.getName(), EventNameEnum.ADD_PRODUCT_BULK.getName(), EventActionEnum.DB_RECORD_SAVE.getName(), clientId));
        LOGGER.info(" [addProductBulk] product added: " + productResponseList);
        return new ResponseEntity<>(new Response<>(productResponseList, "product list added successfully"), HttpStatus.OK) ;
    }


    @ApiOperation(
            value = "Get product details",
            notes = "Used to get the details about a product"
    )
    @RequestMapping(value = {"/v1/product/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<Response<ProductResponse>> getProductDetails(@RequestHeader(name = X_DS_CLIENT_ID) Long clientId,@PathVariable Long id) {
        LOGGER.info(" [getProductDetails] request received: " + id );
        ProductResponse response = new ProductResponse(productService.getProduct(id));
        applicationEventPublisher.publishEvent(new EventDto(EventCategoryEnum.PRODUCT.getName(), EventNameEnum.GET_PRODUCT.getName(), EventActionEnum.DB_RECORD_FETCH.getName(), clientId));
        LOGGER.info(" [getProductDetails] response generated: " + response );
        return new ResponseEntity<>(new Response<>(response, "product details fetched successfully"), HttpStatus.OK);
    }

    @ApiOperation(
            value = "Get bulk product details",
            notes = "Used to get details of multiple products"
    )
    @RequestMapping(value = "/v1/product/search", method = RequestMethod.POST)
    public ResponseEntity<Response<List<ProductResponse>>> searchProducts(@RequestHeader(name = X_DS_CLIENT_ID) Long clientId,@RequestBody ProductSearchRequest productSearchRequest) {
        LOGGER.info(" [searchProducts] request received, ProductSearchRequest: " + productSearchRequest );
        List<ProductResponse> responseList = productService.getAllProductDetails(productSearchRequest, clientId);
        LOGGER.info(" [searchProducts] response generated, count - " + responseList.size());
        return new ResponseEntity<>(new Response<>(responseList, "product details fetched successfully"), HttpStatus.OK);
    }

    @ApiOperation(
            value = "Get all product details",
            notes = "Used to get all products without any filters"
    )
    @RequestMapping(value = "/v1/products", method = RequestMethod.GET)
    public ResponseEntity<Response<List<ProductResponse>>> getAllProducts(@RequestHeader(name = X_DS_CLIENT_ID) Long clientId) {
        LOGGER.info(" [getAllProducts] request received");
        List<ProductResponse> responseList = productService.getAllProducts();
        applicationEventPublisher.publishEvent(new EventDto(EventCategoryEnum.PRODUCT.getName(), EventNameEnum.GET_ALL_PRODUCT.getName(), EventActionEnum.DB_RECORD_FETCH.getName(), clientId));
        return new ResponseEntity<>(new Response<>(responseList, "all product details fetched successfully"), HttpStatus.OK);
    }

}

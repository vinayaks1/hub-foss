package com.everwell.dispensationservice.controllers;

import com.everwell.dispensationservice.Utils.CacheUtils;
import com.everwell.dispensationservice.Utils.Utils;
import com.everwell.dispensationservice.constants.Constants;
import com.everwell.dispensationservice.enums.event.EventActionEnum;
import com.everwell.dispensationservice.enums.event.EventCategoryEnum;
import com.everwell.dispensationservice.enums.event.EventNameEnum;
import com.everwell.dispensationservice.models.Response;
import com.everwell.dispensationservice.models.db.Dispensation;
import com.everwell.dispensationservice.models.db.ProductInventoryLog;
import com.everwell.dispensationservice.models.db.ProductDispensationMapping;
import com.everwell.dispensationservice.models.dto.CacheMultiSetWithTimeDto;
import com.everwell.dispensationservice.models.dto.DispensationData;
import com.everwell.dispensationservice.models.dto.DispensationProductDto;
import com.everwell.dispensationservice.models.dto.eventstreaming.EventDto;
import com.everwell.dispensationservice.models.http.requests.DispensationRequest;
import com.everwell.dispensationservice.models.http.requests.ReturnDispensationRequest;
import com.everwell.dispensationservice.models.http.requests.SearchDispensationRequest;
import com.everwell.dispensationservice.models.http.responses.DispensationResponse;
import com.everwell.dispensationservice.models.http.responses.EntityDispensationResponse;
import com.everwell.dispensationservice.models.http.responses.ReturnDispensationResponse;
import com.everwell.dispensationservice.services.DispensationService;
import com.everwell.dispensationservice.services.ProductService;
import com.fasterxml.jackson.core.type.TypeReference;
import io.swagger.annotations.ApiOperation;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

import static com.everwell.dispensationservice.constants.Constants.X_DS_CLIENT_ID;

@RestController
public class DispensationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DispensationController.class);

    @Autowired
    private DispensationService dispensationService;

    @Autowired
    private ProductService productService;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;


    @ApiOperation(
            value = "Create new dispensation",
            notes = "New dispensation is created with the product mappings populated for the entity"
    )
    @RequestMapping(value = "/v1/dispensation", method = RequestMethod.POST)
    public ResponseEntity<Response<DispensationResponse>> createDispensation(@RequestBody DispensationRequest dispensationRequest, @RequestHeader(name = X_DS_CLIENT_ID) Long clientId) {
        LOGGER.info("[createDispensation] request received: " + dispensationRequest);
        dispensationRequest.validate(dispensationRequest);
        productService.validateProductIds(
                dispensationRequest
                        .getProductDataList()
                        .stream()
                        .map(DispensationRequest.ProductData::getProductId)
                        .collect(Collectors.toList()));
        Dispensation dispensation =  dispensationService.create(dispensationRequest, clientId);
        List<ProductDispensationMapping> productDispensationMappings = dispensationService.addDispensationProducts(dispensationRequest, dispensation.getId(), dispensation.getIssueDate());
        DispensationResponse response = new DispensationResponse(dispensation.getId());
        Map<String,Long> eventKeyValuePairs = new HashMap<>();
        eventKeyValuePairs.put(EventCategoryEnum.ENTITY.getName(), dispensation.getEntityId());
        eventKeyValuePairs.put(EventCategoryEnum.DISPENSATION.getName(), dispensation.getId());
        applicationEventPublisher.publishEvent(new EventDto(EventCategoryEnum.DISPENSATION.getName(), EventNameEnum.ADD_DISPENSATION.getName(),
                EventActionEnum.DB_RECORD_SAVE.getName(), eventKeyValuePairs, clientId));
        LOGGER.info("[createDispensation] response generated: " + response);
        return new ResponseEntity<>(new Response<>(response, "dispensation created successfully"), HttpStatus.CREATED);
    }

    @ApiOperation(
            value = "Create new dispensation with inventory",
            notes = "New dispensation is created with the product mappings populated for the entity and is added to inventory "
    )
    @RequestMapping(value = "/v2/dispensation", method = RequestMethod.POST)
    public ResponseEntity<Response<DispensationResponse>> createDispensationInventory(@RequestBody DispensationRequest dispensationRequest, @RequestHeader(name = X_DS_CLIENT_ID) Long clientId) {
        LOGGER.info("[createDispensationInventory] request received: " + dispensationRequest);
        dispensationRequest.validate(dispensationRequest);
        productService.validateProductIds(
                dispensationRequest
                        .getProductDataList()
                        .stream()
                        .map(DispensationRequest.ProductData::getProductId)
                        .collect(Collectors.toList()));
        Dispensation dispensation = dispensationService.createDispensationWithInventoryData(dispensationRequest, clientId);
        DispensationResponse response = new DispensationResponse(dispensation.getId());
        Map<String,Long> eventKeyValuePairs = new HashMap<>();
        eventKeyValuePairs.put(EventCategoryEnum.ENTITY.getName(), dispensation.getEntityId());
        eventKeyValuePairs.put(EventCategoryEnum.DISPENSATION.getName(), dispensation.getId());
        applicationEventPublisher.publishEvent(new EventDto(EventCategoryEnum.DISPENSATION.getName(), EventNameEnum.ADD_DISPENSATION.getName(),
                EventActionEnum.DB_RECORD_SAVE.getName(), eventKeyValuePairs, clientId));
        LOGGER.info("[createDispensationInventory] response generated: " + response);
        return new ResponseEntity<>(new Response<>(response, "dispensation created successfully"), HttpStatus.CREATED);
    }

    @SneakyThrows
    @ApiOperation(
        value = "Get dispensation details for entity",
        notes = "Used to get all the dispensation issued with medication details for the entity. RequestParam - includeProductLog is now depreciated. Product Logs are included as default"
    )
    @RequestMapping(value = "/v1/dispensation/entity/{entityId}", method = RequestMethod.GET)
    public ResponseEntity<Response<EntityDispensationResponse>> getDispensationDetailsForEntity(@PathVariable Long entityId, @RequestParam(required = false) boolean includeProductLog, @RequestHeader(name = X_DS_CLIENT_ID) Long clientId) {
        LOGGER.info("[getDispensationDetailsForEntity] request received: " + entityId);
        EntityDispensationResponse response;
        String cacheKey = String.format(String.format(Constants.CACHE_ENTITY_DISPENSATION_KEY, entityId, clientId));
        String cacheResponse = CacheUtils.getFromCache(cacheKey);
        if (null != cacheResponse) {
            response = Utils.jsonToObject(cacheResponse, new TypeReference<EntityDispensationResponse>() {});
        } else {
            response = dispensationService.getAllDispensationDataForEntity(entityId, clientId, true);
            CacheUtils.putIntoCache(cacheKey, Utils.asJsonString(response), Constants.DISPENSATION_ENTITY_VALIDITY);
        }
        applicationEventPublisher.publishEvent(new EventDto(EventCategoryEnum.DISPENSATION.getName(), EventNameEnum.GET_ENTITY_DISPENSATIONS.getName(),
                EventActionEnum.DB_RECORD_FETCH.getName(),clientId));
        LOGGER.info("[getDispensationDetailsForEntity] response generated: " + entityId + " " + response);
        return new ResponseEntity<>(new Response<>(response, "data fetched successfully"), HttpStatus.OK);
    }

    @ApiOperation(
            value = "Get dispensation details",
            notes = "Used to get details about a dispensation. RequestParam - includeProductLog is now depreciated. Product Logs are included as default"
    )
    @RequestMapping(value = "/v1/dispensation/{id}", method = RequestMethod.GET)
    public ResponseEntity<Response<DispensationData>> getDispensationDetails(@PathVariable Long id, @RequestParam(required = false) boolean includeProductLog, @RequestHeader(name = X_DS_CLIENT_ID) Long clientId) {
        LOGGER.info("[getDispensationDetails] request received: " + id);
        Dispensation dispensation = dispensationService.getDispensation(id, clientId);
        Map<Long, List<DispensationProductDto>> dispensationProductData  = dispensationService.getDispensationIdToDispensationProductDtoMap(Arrays.asList(id), true);
        DispensationData dispensationData = new DispensationData(dispensation, dispensationProductData.get(id));
        Response<DispensationData> response = new Response<>(dispensationData, "dispensation data fetched successfully");
        applicationEventPublisher.publishEvent(new EventDto(EventCategoryEnum.DISPENSATION.getName(), EventNameEnum.GET_DISPENSATION.getName(),
                EventActionEnum.DB_RECORD_FETCH.getName(),clientId));
        LOGGER.info("[getDispensationDetails] response generated: " + response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(
            value = "Delete dispensation",
            notes = "Will soft delete a dispensation issued"
    )
    @RequestMapping(value = "/v1/dispensation/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Response<DispensationResponse>> deleteDispensation(@PathVariable Long id, @RequestHeader(name = X_DS_CLIENT_ID) Long clientId) {
        LOGGER.info("[deleteDispensation] request received: " + id);
        Dispensation deletedDispensation = dispensationService.delete(id, clientId);
        DispensationResponse response = new DispensationResponse(deletedDispensation.getId());
        LOGGER.info("[deleteDispensation] response generated: " + response);
        applicationEventPublisher.publishEvent(new EventDto(EventCategoryEnum.DISPENSATION.getName(), EventNameEnum.DELETE_DISPENSATION.getName(),
                EventActionEnum.DB_RECORD_DELETE.getName(), Collections.singletonMap(EventCategoryEnum.ENTITY.getName(), deletedDispensation.getEntityId()),clientId));
        return new ResponseEntity<>(new Response<>(response, "dispensation deleted successfully"), HttpStatus.OK);
    }

    @SneakyThrows
    @ApiOperation(
            value = "Search and get bulk entity dispensation details",
            notes = "Used to get all dispensation and their details of the entities"
    )
    @RequestMapping(value = "/v1/dispensation/entity/search", method = RequestMethod.POST)
    public ResponseEntity<Response<List<EntityDispensationResponse>>> searchMultipleEntityDispensations(@RequestBody SearchDispensationRequest searchDispensationRequest, @RequestHeader(name = X_DS_CLIENT_ID) Long clientId) {
        LOGGER.info("[searchDispensationsForEntities] request received: " + searchDispensationRequest);
        searchDispensationRequest.validate();
        List<EntityDispensationResponse> responses = dispensationService.getAllDispensationDataForEntityBulk(searchDispensationRequest.getEntityIds(), clientId);
        if(null != searchDispensationRequest.getFilters()) {
            responses = dispensationService.filterList(responses, searchDispensationRequest.getFilters());
        }
        applicationEventPublisher.publishEvent(new EventDto(EventCategoryEnum.DISPENSATION.getName(), EventNameEnum.SEARCH_ENTITY_DISPENSATIONS.getName(),
                EventActionEnum.DB_RECORD_FETCH.getName(),clientId));
        return new ResponseEntity<>(new Response<>(responses, "data fetched successfully"), HttpStatus.OK);
    }

    @ApiOperation(
            value = "Return dispensation",
            notes = "Update number of units issued for a dispensation and create a return log"
    )
    @RequestMapping(value = "/v1/dispensation/return", method = RequestMethod.POST)
    public ResponseEntity<Response<ReturnDispensationResponse>> returnDispensation(@RequestBody ReturnDispensationRequest returnDispensationRequest, @RequestHeader(name = X_DS_CLIENT_ID) Long clientId) {
        LOGGER.info("[returnDispensation] request received: " + returnDispensationRequest);
        returnDispensationRequest.validate();
        List<ProductInventoryLog> returnDispensations = dispensationService.createReturnDispensation(returnDispensationRequest, clientId, true);
        List<Long> returnDispensationIds = new ArrayList<>();
        for(ProductInventoryLog log : returnDispensations) {
            returnDispensationIds.add(log.getId());
        }
        ReturnDispensationResponse response = new ReturnDispensationResponse(returnDispensationIds);
        LOGGER.info("[returnDispensation] response generated: " + response);
        applicationEventPublisher.publishEvent(new EventDto(EventCategoryEnum.DISPENSATION.getName(), EventNameEnum.RETURN_ENTITY_DISPENSATIONS.getName(),
                EventActionEnum.DB_RECORD_SAVE.getName(), Collections.singletonMap(EventCategoryEnum.DISPENSATION.getName(), returnDispensationRequest.getDispensationId()), clientId));
        return new ResponseEntity<>(new Response<>(response, "return dispensation created successfully"), HttpStatus.CREATED);
    }

    @ApiOperation(
            value = "Return dispensation with inventory data",
            notes = "Update number of units issued for a dispensation and create a return log and set inventory data"
    )
    @RequestMapping(value = "/v2/dispensation/return", method = RequestMethod.POST)
    public ResponseEntity<Response<ReturnDispensationResponse>> returnDispensationInventory(@RequestBody ReturnDispensationRequest returnDispensationRequest, @RequestHeader(name = X_DS_CLIENT_ID) Long clientId) {
        LOGGER.info("[returnDispensationInventory] request received: " + returnDispensationRequest);
        returnDispensationRequest.validate();
        List<Long> returnDispensationIds = dispensationService.createReturnDispensationWithInventoryData(returnDispensationRequest, clientId);
        ReturnDispensationResponse response = new ReturnDispensationResponse(returnDispensationIds);
        LOGGER.info("[returnDispensationInventory] response generated: " + response);
        applicationEventPublisher.publishEvent(new EventDto(EventCategoryEnum.DISPENSATION.getName(), EventNameEnum.RETURN_ENTITY_DISPENSATIONS.getName(),
                EventActionEnum.DB_RECORD_SAVE.getName(), Collections.singletonMap(EventCategoryEnum.DISPENSATION.getName(), returnDispensationRequest.getDispensationId()), clientId));
        return new ResponseEntity<>(new Response<>(response, "return dispensation created successfully"), HttpStatus.CREATED);
    }

    @SneakyThrows
    @GetMapping(value = "/v1/dispensation/{dispensationId}/entity")
    public ResponseEntity<Response<EntityDispensationResponse>> dispensationEntityData(@PathVariable Long dispensationId, @RequestHeader(name = X_DS_CLIENT_ID) Long clientId) {
        LOGGER.info("[dispensationEntityData] request received: for dispensation id" + dispensationId);
        Dispensation dispensation = dispensationService.getDispensation(dispensationId, clientId);
        return getDispensationDetailsForEntity(dispensation.getEntityId(), true, clientId);
    }
}

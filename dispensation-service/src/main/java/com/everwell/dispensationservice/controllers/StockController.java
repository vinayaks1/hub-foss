package com.everwell.dispensationservice.controllers;

import com.everwell.dispensationservice.enums.event.EventActionEnum;
import com.everwell.dispensationservice.enums.event.EventCategoryEnum;
import com.everwell.dispensationservice.enums.event.EventNameEnum;
import com.everwell.dispensationservice.enums.TransactionType;
import com.everwell.dispensationservice.models.Response;
import com.everwell.dispensationservice.models.dto.ProductStockData;
import com.everwell.dispensationservice.models.dto.eventstreaming.EventDto;
import com.everwell.dispensationservice.models.http.requests.ProductStockSearchRequest;
import com.everwell.dispensationservice.models.http.requests.StockBulkRequest;
import com.everwell.dispensationservice.models.http.requests.StockRequest;
import com.everwell.dispensationservice.services.ProductService;
import com.everwell.dispensationservice.services.StockService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


import static com.everwell.dispensationservice.constants.Constants.*;

@RestController
public class StockController {

    private static final Logger LOGGER = LoggerFactory.getLogger(StockController.class);

    @Autowired
    private StockService stockService;

    @Autowired
    private ProductService productService;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @ApiOperation(
            value = "Credit Stocks",
            notes = "Used to credit the stocks"
    )
    @RequestMapping(value = "/v1/stock", method = RequestMethod.POST)
    public ResponseEntity<Response> stockCredit(@RequestBody StockRequest stockRequest, @RequestHeader(name = X_DS_CLIENT_ID) Long clientId) {
        LOGGER.info("[stockCredit] request received: " + stockRequest);
        stockRequest.validate();
        productService.getProduct(stockRequest.getProductId());
        stockService.setStockData(stockRequest, TransactionType.CREDIT, clientId);
        applicationEventPublisher.publishEvent(new EventDto(EventCategoryEnum.STOCK.getName(), EventNameEnum.STOCK_CREDIT.getName(), EventActionEnum.DB_RECORD_SAVE.getName(), clientId));
        LOGGER.info("[stockCredit] response generated");
        return new ResponseEntity<>(new Response<>(true, "stock credited successfully"), HttpStatus.OK);
    }

    @ApiOperation(
            value = "Debit Stocks",
            notes = "Used to debit the stocks"
    )
    @RequestMapping(value = "/v1/stock", method = RequestMethod.PUT)
    public ResponseEntity<Response> stockDebit(@RequestBody StockRequest stockRequest, @RequestHeader(name = X_DS_CLIENT_ID) Long clientId) {
        LOGGER.info("[stockDebit] request received: " + stockRequest);
        stockRequest.validate();
        stockService.setStockData(stockRequest, TransactionType.DEBIT, clientId);
        applicationEventPublisher.publishEvent(new EventDto(EventCategoryEnum.STOCK.getName(), EventNameEnum.STOCK_DEBIT.getName(), EventActionEnum.DB_RECORD_UPDATE.getName(), clientId));
        LOGGER.info("[stockDebit] response generated");
        return new ResponseEntity<>(new Response<>(true, "stock debited successfully"), HttpStatus.OK);
    }

    @ApiOperation(
            value = "Product stock search",
            notes = "Used to search the stocks of the products by product id and hierarchy id"
    )
    @RequestMapping(value = "/v1/product/stocks/search", method = RequestMethod.POST)
    public  ResponseEntity<Response<List<ProductStockData>>> productStockSearch(@RequestBody ProductStockSearchRequest productStockSearchRequest, @RequestHeader(name = X_DS_CLIENT_ID) Long clientId) {
        LOGGER.info(" [productStockSearch] request received" + productStockSearchRequest);
        productStockSearchRequest.validate();
        List<ProductStockData> productStockDataResponseList = stockService.getProductStockData(productStockSearchRequest);
        applicationEventPublisher.publishEvent(new EventDto(EventCategoryEnum.STOCK.getName(), EventNameEnum.STOCK_SEARCH.getName(), EventActionEnum.DB_RECORD_FETCH.getName(), clientId));
        LOGGER.info(" [productStockSearch] response generated" + productStockDataResponseList);
        return new ResponseEntity<>(new Response<>(productStockDataResponseList, "product stock details fetched successfully"), HttpStatus.OK) ;
    }

    @ApiOperation(
            value = "Update Stocks",
            notes = "Used to update the stocks"
    )
    @RequestMapping(value = "/v1/stocks", method = RequestMethod.PUT)
    public ResponseEntity<Response> updateStock(@RequestBody StockBulkRequest stockBulkRequest, @RequestHeader(name = X_DS_CLIENT_ID) Long clientId) {
        LOGGER.info("[stockUpdate] request received: " + stockBulkRequest);

        for(StockRequest stockRequest : stockBulkRequest.getStockData()) {
            stockRequest.validate();
            stockService.setStockData(stockRequest, TransactionType.UPDATE, clientId);
            applicationEventPublisher.publishEvent(new EventDto(EventCategoryEnum.STOCK.getName(), EventNameEnum.STOCK_UPDATE.getName(), EventActionEnum.DB_RECORD_UPDATE.getName(), clientId));
        }
        LOGGER.info("[stockUpdate] response generated");
        return new ResponseEntity<>(new Response<>(true, "stock updated successfully"), HttpStatus.OK);
    }
}

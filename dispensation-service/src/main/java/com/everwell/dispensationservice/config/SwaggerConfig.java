package com.everwell.dispensationservice.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
@Profile({"dev", "everwell-beta"})
public class SwaggerConfig {

    private final String BASE_PACKAGE = "com.everwell.dispensationservice";
    @Bean
    public Docket Api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage(BASE_PACKAGE))
                .paths(regex("/.*"))
                .build()
                .apiInfo(getApiInfo());
    }

    private ApiInfo getApiInfo() {
        return new ApiInfo(
                "Dispensation Backend - Api Documentation",
                "Dispensation Backend - Standalone service to manage entity's dispensation & refills and also holds Drug specific data.",
                "V1",
                "",
                new Contact("Everwell", "https://www.everwell.org/", "contact@everwell.org"),
                "",
                "",
                Collections.emptyList());
    }
}

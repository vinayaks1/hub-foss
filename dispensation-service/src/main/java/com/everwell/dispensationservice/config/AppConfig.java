package com.everwell.dispensationservice.config;

import com.everwell.dispensationservice.filters.CachingRequestBodyFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Bean
    public FilterRegistrationBean someFilterRegistration() {

        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new CachingRequestBodyFilter());
        registration.setName("CachingRequestContentFilter");
        registration.setOrder(1);
        return registration;
    }
}

package com.everwell.dispensationservice.services.impl;

import com.everwell.dispensationservice.Utils.CacheUtils;
import com.everwell.dispensationservice.Utils.Utils;
import com.everwell.dispensationservice.constants.Constants;
import com.everwell.dispensationservice.enums.*;
import com.everwell.dispensationservice.exceptions.NotFoundException;
import com.everwell.dispensationservice.exceptions.ValidationException;
import com.everwell.dispensationservice.models.db.Dispensation;
import com.everwell.dispensationservice.models.db.Product;
import com.everwell.dispensationservice.models.db.ProductInventoryLog;
import com.everwell.dispensationservice.models.db.ProductDispensationMapping;
import com.everwell.dispensationservice.models.dto.*;
import com.everwell.dispensationservice.models.dto.DispensationProductDto;
import com.everwell.dispensationservice.models.dto.ProductInventoryLogDto;
import com.everwell.dispensationservice.models.dto.RangeValueFilter;
import com.everwell.dispensationservice.models.http.requests.DispensationRequest;
import com.everwell.dispensationservice.models.http.requests.ReturnDispensationRequest;
import com.everwell.dispensationservice.models.http.requests.StockRequest;
import com.everwell.dispensationservice.models.http.responses.EntityDispensationResponse;
import com.everwell.dispensationservice.repositories.*;
import com.everwell.dispensationservice.services.DispensationService;
import com.everwell.dispensationservice.services.StockService;
import com.fasterxml.jackson.core.type.TypeReference;
import org.apache.commons.lang3.StringUtils;
import com.everwell.dispensationservice.services.ProductService;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class DispensationServiceImpl implements DispensationService {

    @Autowired
    private DispensationRepository dispensationRepository;

    @Autowired
    private ProductDispensationMapRepository productDispensationMapRepository;

    @Autowired
    private ProductInventoryLogRepository productInventoryLogRepository;

    @Autowired
    private ProductService productService;

    @Autowired
    private StockService stockService;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    private Date getDispensationRefillDate(Long drugDispensedForDays, Date issuedDate, Date dosingStartDate) {
        Date refillDate = null;
        if(null != drugDispensedForDays) {
            if(null != dosingStartDate) {
                refillDate = DateUtils.addDays(dosingStartDate, drugDispensedForDays.intValue());
            }
            else if (null != issuedDate) {
                refillDate = DateUtils.addDays(issuedDate, drugDispensedForDays.intValue());
            }
        }
        return refillDate;
    }

    private Date getDispensedProductRefillDate(DispensationRequest.ProductData productData, Date issuedDate) {
        Date refillDate = null;
        // Todo:
        //  Refill date calculation based on different dose frequency & schedule
        //  Eventually to be moved to different handlers
        // Consideration here : Freq - 1Dose/perDay
        if(null != issuedDate && null != productData.getUnitsIssued()) {
            refillDate = DateUtils.addDays(issuedDate, productData.getUnitsIssued().intValue());
        }
        return refillDate;
    }

    @Override
    public Dispensation create(DispensationRequest dispensationRequest, Long clientId) {

        Dispensation dispensation = new Dispensation(dispensationRequest.getEntityId(), clientId, dispensationRequest.getAddedBy(),
                dispensationRequest.getWeight(), dispensationRequest.getWeightBand(), dispensationRequest.getPhase(), dispensationRequest.getDrugDispensedForDays(), dispensationRequest.getIssuingFacility(), dispensationRequest.getNotes(), DispensationStatus.ISSUED);
        if(null != dispensationRequest.getIssuedDate()){
            dispensation.setIssueDate(Utils.convertStringToDateOrDefault(dispensationRequest.getIssuedDate()));
        }
        if(null != dispensationRequest.getDateOfPrescription()){
            dispensation.setDateOfPrescription(Utils.convertStringToDateOrDefault(dispensationRequest.getDateOfPrescription()));
        }
        if(null != dispensationRequest.getDosingStartDate()){
            dispensation.setDosingStartDate(Utils.convertStringToDateOrDefault(dispensationRequest.getDosingStartDate()));
        }
        dispensation.setRefillDate(getDispensationRefillDate(dispensation.getDrugDispensedForDays(), dispensation.getIssueDate(), dispensation.getDosingStartDate()));

        dispensationRepository.save(dispensation);
        emitForEpisode(dispensationRequest, clientId);
        return dispensation;
    }

    @Async
    @Override
    public void emitForEpisode(DispensationRequest dispensationRequest, Long clientId) {
        List<Dispensation> dispensations = getDispensationsForEntity(dispensationRequest.getEntityId(), clientId);
        Date entityRefillDate = getRefillDateForEntity(dispensationRequest.getEntityId(), dispensations);
        Map<String, Object> episodeFieldMap = new HashMap<>();
        episodeFieldMap.put(Constants.DISPENSATION_FIELD_LAST_REFILL_DATE, Utils.getFormattedDate(entityRefillDate, "yyyy-MM-dd HH:mm:ss"));
        episodeFieldMap.put(Constants.ID, dispensationRequest.getEntityId());
        applicationEventPublisher.publishEvent(new EpisodeFieldsReponse(Constants.MY_MODULE, episodeFieldMap, clientId));
    }

    @Override
    public Dispensation getDispensation(Long id, Long clientId) {
        Optional<Dispensation> dispensation = dispensationRepository.findByIdAndClientIdAndDeletedIsFalse(id, clientId);
        if(!dispensation.isPresent()) {
            throw new NotFoundException("Dispensation record not found");
        }
        return dispensation.get();
    }

    @Override
    public Dispensation delete(Long id, Long clientId) {
        Dispensation dispensation = getDispensation(id, clientId);
        dispensation.setDeleted(true);
        dispensationRepository.save(dispensation);
        return dispensation;
    }

    @Override
    public void setRefillDate(Dispensation dispensation, Date refillDate) {
        dispensation.setRefillDate(refillDate);
        dispensationRepository.save(dispensation);
    }

    @Override
    public List<Dispensation> getDispensationsForEntity(Long entityId, Long clientId) {
        return dispensationRepository.findAllByEntityIdAndClientIdAndDeletedIsFalse(entityId, clientId);
    }

    @Override
    @Transactional(rollbackFor =  Exception.class)
    public List<DispensationProductDto> getProductDtoForDispensations(Collection<Long> Ids) {
        List<ProductDispensationMapping> productDispensationMappings = productDispensationMapRepository.findAllByDispensationIdIn(Ids);
        return productDispensationMappings.stream()
                .map(prodDisMap -> new DispensationProductDto(prodDisMap.getId(), prodDisMap.getProductId(), prodDisMap.getProductConfigId(), prodDisMap.getDispensationId(),
                        prodDisMap.getNumberOfUnitsIssued(), prodDisMap.getRefillDate(), prodDisMap.getDosingSchedule()))
                .collect(Collectors.toList());
    }

    @Override
    public Map<Long, List<StockQuantityDto>> getStockQuantityDtoMap(Map<Long, List<ProductInventoryLogDto>> logsIdToEntityMap) {
        Map<Long, List<StockQuantityDto>> transactionIdToStockQuantityMap = new HashMap<>();
        for(Map.Entry<Long, List<ProductInventoryLogDto>> transactionIdToLogMap : logsIdToEntityMap.entrySet()) {
            Long transactionId = transactionIdToLogMap.getKey();
            List<ProductInventoryLogDto> productInventoryLogDtoList = transactionIdToLogMap.getValue();
            StockQuantityDto stockQuantityDto;
            Map<Long, StockQuantityDto> pInventoryIdToStockQuantityMap = new HashMap<>();
            for (ProductInventoryLogDto log : productInventoryLogDtoList) {
                if (null != log.getPInventoryId()) {
                    stockQuantityDto = pInventoryIdToStockQuantityMap.get(log.getPInventoryId());
                    if (null == stockQuantityDto) {
                        stockQuantityDto = new StockQuantityDto(log.getBatchNumber(), log.getExpiryDate(), 0L, log.getPInventoryId());
                    }
                    if (log.getTransactionType().equals(TransactionType.ISSUED)) {
                        stockQuantityDto.setQuantity(stockQuantityDto.getQuantity() + log.getTransactionQuantity());
                    } else if (log.getTransactionType().equals(TransactionType.RETURNED)) {
                        stockQuantityDto.setQuantity(stockQuantityDto.getQuantity() - log.getTransactionQuantity());
                    }
                    pInventoryIdToStockQuantityMap.put(log.getPInventoryId(), stockQuantityDto);
                }
            }
            if(pInventoryIdToStockQuantityMap.size() > 0){
                transactionIdToStockQuantityMap.put(transactionId, new ArrayList<>(pInventoryIdToStockQuantityMap.values()));
            }
        }
        return transactionIdToStockQuantityMap;
    }

    @Override
    @Transactional(rollbackFor =  Exception.class)
    public Map<Long, List<DispensationProductDto>> getDispensationIdToDispensationProductDtoMap(Collection<Long> dispensationIds, Boolean includeProductLog) {
        List<DispensationProductDto> dispensationProductDtos = getProductDtoForDispensations(dispensationIds);
        Map<Long, List<DispensationProductDto>> dispensationIdToDispensationProductDtoMap = new HashMap<>();
        if(!dispensationProductDtos.isEmpty()) {
            List<Long> productIds = new ArrayList<>();
            List<Long> dispensationProductMappingIds = new ArrayList<>();
            for(DispensationProductDto dispensationProductDto : dispensationProductDtos) {
                productIds.add(dispensationProductDto.getProductId());
                dispensationProductMappingIds.add(dispensationProductDto.getId());
            }
            Map<Long, Product> productIdToEntityMap = new HashMap<>();
            for(Product product : productService.getProducts(productIds)) {
                productIdToEntityMap.put(product.getId(), product);
            }
            Map<Long, List<ProductInventoryLogDto>> logsIdToEntityMap = getProductInventoryLogMap(dispensationProductMappingIds);
            Map<Long, List<StockQuantityDto>> transactionIdToStockQuantityMap = getStockQuantityDtoMap(logsIdToEntityMap);
            for(DispensationProductDto dispensationProductDto : dispensationProductDtos) {
                dispensationIdToDispensationProductDtoMap.putIfAbsent(dispensationProductDto.getDispensationId(), new ArrayList<>());
                Product currentProduct = productIdToEntityMap.get(dispensationProductDto.getProductId());
                String productName = null;
                String unitOfMeasurement = null;
                if(null != currentProduct) {
                    productName = currentProduct.getProductName();
                    unitOfMeasurement = currentProduct.getDosageForm();
                }
                dispensationProductDto.setProductName(productName);
                dispensationProductDto.setUnitOfMeasurement(unitOfMeasurement);
                if(includeProductLog) {
                    dispensationProductDto.setLogs(logsIdToEntityMap.get(dispensationProductDto.getId()));
                }
                dispensationProductDto.setStockData(transactionIdToStockQuantityMap.get(dispensationProductDto.getId()));
                dispensationIdToDispensationProductDtoMap.get(dispensationProductDto.getDispensationId()).add(dispensationProductDto);
            }
        }
        return dispensationIdToDispensationProductDtoMap;
    }

    @Override
    @Transactional(rollbackFor =  Exception.class)
    public EntityDispensationResponse getAllDispensationDataForEntity(Long entityId, Long clientId, boolean getProductLogs) throws ExecutionException, InterruptedException {
        List<Dispensation> dispensationList = getDispensationsForEntity(entityId, clientId);
        CompletableFuture<Long> totalDispensedDrugs = getTotalDaysDrugsDispensedForEntity(entityId, dispensationList);
        List<Long> dispensationIds = dispensationList
                .stream()
                .map(Dispensation::getId)
                .collect(Collectors.toList());

        Map<Long, List<DispensationProductDto>> dispensationProductDataMap = getDispensationIdToDispensationProductDtoMap(dispensationIds, getProductLogs);
        List<DispensationData> dispensationDataList = new ArrayList<>();
        for(Dispensation dispensation : dispensationList) {
            dispensationDataList.add(new DispensationData(dispensation, dispensationProductDataMap.get(dispensation.getId())));
        }
        Date entityRefillDate = getRefillDateForEntity(entityId, dispensationList);
        return new EntityDispensationResponse(entityId,entityRefillDate, totalDispensedDrugs.get(), dispensationDataList);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public List<EntityDispensationResponse> getAllDispensationDataForEntityBulk(List<Long> entityIds, Long clientId) throws ExecutionException, InterruptedException, IOException {
        List<EntityDispensationResponse> responseList = new ArrayList<>();
        HashSet<String> cacheKeys = new HashSet<>();
        Set<Long> entityIdSet = new HashSet<>();
        for(Long entityId : entityIds) {
            cacheKeys.add(String.format(Constants.CACHE_ENTITY_DISPENSATION_KEY, entityId, clientId));
            entityIdSet.add(entityId);
        }
        List<String> cacheResult = CacheUtils.getFromCacheBulk(cacheKeys);
        if(!CollectionUtils.isEmpty(cacheResult)) {
            for(String cacheResultEntry : cacheResult) {
                if(null != cacheResultEntry) {
                    EntityDispensationResponse cachedEntityResponse = Utils.convertStringToObject(cacheResultEntry, EntityDispensationResponse.class);
                    responseList.add(cachedEntityResponse);
                    entityIdSet.remove(cachedEntityResponse.getEntityId());
                }
            }
        }
        if(!entityIdSet.isEmpty()) {
            List<Dispensation> dispensations = dispensationRepository.findAllByEntityIdInAndClientIdAndDeletedIsFalse(entityIdSet, clientId);
            Map<Long, List<Dispensation>> entityIdToDispensationListMap = new HashMap<>();
            Set<Long> dispensationIds = new HashSet<>();
            Set<Long> entitiesWithDispensation = new HashSet<>();
            for(Dispensation dispensation : dispensations) {
                Long entityId = dispensation.getEntityId();
                entitiesWithDispensation.add(entityId);
                List<Dispensation> entityDispensations = entityIdToDispensationListMap.getOrDefault(entityId, new ArrayList<>());
                entityDispensations.add(dispensation);
                entityIdToDispensationListMap.put(entityId, entityDispensations);
                dispensationIds.add(dispensation.getId());
            }
            Map<Long, List<DispensationProductDto>> dispensationProductDataMap = getDispensationIdToDispensationProductDtoMap(dispensationIds, true);
            Map<Long, List<DispensationData>> entityIdToDispensationDataListMap = new HashMap<>();
            for(Dispensation dispensation : dispensations) {
                List<DispensationData> dispensationDataList = entityIdToDispensationDataListMap.getOrDefault(dispensation.getEntityId(), new ArrayList<>());
                dispensationDataList.add(new DispensationData(dispensation, dispensationProductDataMap.get(dispensation.getId())));
                entityIdToDispensationDataListMap.put(dispensation.getEntityId(), dispensationDataList);
            }
            List<CacheMultiSetWithTimeDto> dataToPutIntoCache = new ArrayList<>();
            for(Map.Entry<Long, List<DispensationData>> entry : entityIdToDispensationDataListMap.entrySet()) {
                Long entityId = entry.getKey();
                List<Dispensation> entityDispensationList = entityIdToDispensationListMap.get(entityId);
                CompletableFuture<Long> totalDispensedDrugs = getTotalDaysDrugsDispensedForEntity(entityId, entityDispensationList);
                Date entityRefillDate = getRefillDateForEntity(entityId, entityDispensationList);
                EntityDispensationResponse dbEntityResponse = new EntityDispensationResponse(entityId, entityRefillDate, totalDispensedDrugs.get(), entry.getValue());
                responseList.add(dbEntityResponse);
                dataToPutIntoCache.add(new CacheMultiSetWithTimeDto(String.format(Constants.CACHE_ENTITY_DISPENSATION_KEY, dbEntityResponse.getEntityId(), clientId), Utils.asJsonString(dbEntityResponse), Constants.DISPENSATION_ENTITY_VALIDITY));
            }
            Set<Long> entityIdsWithoutDispensation = new HashSet<>(entityIdSet);
            entityIdsWithoutDispensation.removeAll(entitiesWithDispensation);
            for(Long entityIdWithoutDispensation : entityIdsWithoutDispensation) {
                EntityDispensationResponse response = new EntityDispensationResponse(entityIdWithoutDispensation, null, Constants.DEFAULT_TOTAL_DISPENSED_DRUGS, Collections.EMPTY_LIST);
                responseList.add(response);
                dataToPutIntoCache.add(new CacheMultiSetWithTimeDto(String.format(Constants.CACHE_ENTITY_DISPENSATION_KEY, response.getEntityId(), clientId), Utils.asJsonString(response), Constants.DISPENSATION_ENTITY_VALIDITY));
            }
            CacheUtils.bulkInsertWithTtl(dataToPutIntoCache);
        }
        return responseList;
    }

    @Override
    public List<ProductDispensationMapping> addDispensationProducts(DispensationRequest dispensationRequest, long dispensationId, Date issuedDate) {
        List<ProductDispensationMapping> productDispensationMappings = new ArrayList<>();
        Set<ProductMappingInventoryDto> productMappingInventoryDtoSet = new HashSet<>(mergeSimilarProductMappings(dispensationRequest.getProductDataList(), issuedDate));
        for(ProductMappingInventoryDto productMappingInventoryDto : productMappingInventoryDtoSet) {
            ProductDispensationMapping productDispensationMapping = new ProductDispensationMapping(productMappingInventoryDto.getProductId(), productMappingInventoryDto.getProductConfigId(),
                    dispensationId, productMappingInventoryDto.getUnitsIssued(), productMappingInventoryDto.getDosingSchedule());
            productDispensationMapping.setRefillDate(productMappingInventoryDto.getRefillDate());
            productDispensationMappings.add(productDispensationMapping);
        }
        productDispensationMapRepository.saveAll(productDispensationMappings);
        return productDispensationMappings;
    }

    @Override
    public CompletableFuture<Long> getTotalDaysDrugsDispensedForEntity(Long entityId, List<Dispensation> dispensations) {
        long totalDaysDrugDispensed = Constants.DEFAULT_TOTAL_DISPENSED_DRUGS;
        TreeMap<Date, Date> drugDispensedDaysRangeMap = dispensations
                .stream()
                .filter(d -> null != d.getIssueDate())
                .filter(d -> null != d.getDrugDispensedForDays())
                .collect(Collectors.toMap(Dispensation::getIssueDate, d -> DateUtils.addDays(d.getIssueDate(), d.getDrugDispensedForDays().intValue() - 1),
                        (endDate1, endDate2) -> endDate2.after(endDate1) ? endDate2 : endDate1, TreeMap::new));
        if (!drugDispensedDaysRangeMap.isEmpty()) {
            Iterator<Map.Entry<Date, Date>> dispensationIterator = drugDispensedDaysRangeMap.entrySet().iterator();
            Map.Entry<Date, Date> iterator = dispensationIterator.next();
            Date minDate = iterator.getKey();
            Date maxDate = iterator.getValue();
            totalDaysDrugDispensed = (Utils.getDifferenceDays(minDate, maxDate) + 1);
            while (dispensationIterator.hasNext()) {
                iterator = dispensationIterator.next();
                Date startDate = iterator.getKey();
                Date endDate = iterator.getValue();
                if (startDate.compareTo(minDate) >= 0 && startDate.compareTo(maxDate) <= 0) {
                    endDate = endDate.compareTo(maxDate) > 0 ? endDate : maxDate;
                    totalDaysDrugDispensed += Utils.getDifferenceDays(maxDate, endDate);
                    maxDate = endDate;
                } else {
                    minDate = startDate;
                    maxDate = endDate;
                    totalDaysDrugDispensed += (Utils.getDifferenceDays(minDate, maxDate) + 1);
                }
            }
        }
        return CompletableFuture.completedFuture(totalDaysDrugDispensed);
    }

    @Override
    public Date getRefillDateForEntity(Long entityId, List<Dispensation> dispensations) {
        Date refillDate = null;
        dispensations = dispensations.stream().filter(dispensation -> null != dispensation.getIssueDate()).collect(Collectors.toList());
        if(!dispensations.isEmpty()) {
            refillDate = dispensations.stream()
                    .min((d1, d2) -> d2.getIssueDate().compareTo(d1.getIssueDate()))
                    .get()
                    .getRefillDate();
        }
        return refillDate;
    }

    @Override
    public List<EntityDispensationResponse> filterList(List<EntityDispensationResponse> responseList, List<RangeValueFilter> filters) {
        List<EntityDispensationResponse>  entityDispensationResponseList = new ArrayList<>();
        HashSet<Long> filteredEntityIds = new HashSet<>();
        for(RangeValueFilter f : filters) {
            HashMap<Long, EntityDispensationResponse> filterListMap =  filterList(responseList, filteredEntityIds, f);
            entityDispensationResponseList.addAll(filterListMap.values());
            filteredEntityIds.addAll(filterListMap.keySet());
        }
        return entityDispensationResponseList;
    }

    private HashMap<Long, EntityDispensationResponse> filterList(List<EntityDispensationResponse> toFilterList, Set<Long> filteredEntityIdList, RangeValueFilter filter) {
        HashMap<Long, EntityDispensationResponse> filteredList = new HashMap<>();
        Stream<EntityDispensationResponse> filterStream = null;
        if (RangeFilterType.NEXT_REFILL_DATE == filter.getType()) {
            Date fromDate = Utils.convertStringToDateOrDefault(filter.getFrom());
            Date toDate = Utils.convertStringToDateOrDefault(filter.getTo());
            filterStream = toFilterList.stream()
                    .filter(entityDispensation -> null != entityDispensation.getRefillDate())
                    .filter(ed -> ((fromDate != null && ed.getRefillDate().compareTo(fromDate) >= 0) || (fromDate == null && toDate != null)) &&
                            (toDate == null || ed.getRefillDate().compareTo(toDate) <= 0));
        }
        if (RangeFilterType.NUMBER_OF_DISPENSATIONS == filter.getType()) {
            filterStream = toFilterList.stream()
                    .filter(entityDispensationResponse -> entityDispensationResponse.getDispensations().size() == filter.getValue());
        }
        filteredList = null != filterStream ? filterStream
                .filter(entityDispensationResponse -> !filteredEntityIdList.contains(entityDispensationResponse.getEntityId()))
                .collect(Collectors.toMap(EntityDispensationResponse::getEntityId, ed -> ed, (ed1, ed2) -> ed1, HashMap::new)) : filteredList;
        return filteredList;
    }

    @Override
    @Transactional(rollbackFor =  Exception.class)
    public List<ProductInventoryLog> createReturnDispensation(ReturnDispensationRequest returnDispensationRequest, Long clientId, Boolean writeInventoryLog) {
        Dispensation dispensation = getDispensation(returnDispensationRequest.getDispensationId(), clientId);
        List<ProductInventoryLog> productInventoryLogs = new ArrayList<>();
        boolean dispensationUpdated = false;
        int count = 0;
        List<ProductDispensationMapping> mappings = productDispensationMapRepository.findAllByDispensationIdIn(Collections.singletonList(returnDispensationRequest.getDispensationId()));
        Map<Long, ProductDispensationMapping> productDispensationMap = mappings
                .stream().collect(Collectors.toMap(ProductDispensationMapping::getId, mapping -> mapping));

        List<Long> transactionIdList = returnDispensationRequest.getReturnDispensationProductDetails().stream().map(ReturnDispensationRequest.ReturnDispensationData::getTransactionId).collect(Collectors.toList());
        List<ProductInventoryLog> productInventoryLogList = productInventoryLogRepository.findAllByTransactionIdIn(transactionIdList);
        Map<String, List<ProductInventoryLog>> productInventoryLogMap = productInventoryLogList
                .stream().collect(Collectors.groupingBy(i->""+i.getTransactionId()+i.getPInventoryId()));
        List<ReturnDispensationRequest.ReturnDispensationData> returnDispensationProductDetailList = returnDispensationRequest.getReturnDispensationProductDetails();
        for (ReturnDispensationRequest.ReturnDispensationData requestMapping : returnDispensationProductDetailList) {
            if (requestMapping != null) {
                Long maximumToBeReturned = 0L;
                ProductDispensationMapping mapping = productDispensationMap.get(requestMapping.getTransactionId());
                if (requestMapping.getPInventoryId() != null) {
                    String key = ""+requestMapping.getTransactionId()+requestMapping.getPInventoryId();
                    List<ProductInventoryLog> filteredProductInventoryLog = productInventoryLogMap.get(key);
                    for (ProductInventoryLog productInventoryLog : filteredProductInventoryLog) {
                        if (productInventoryLog.getTransactionType().equals(TransactionType.ISSUED)) {
                            maximumToBeReturned = maximumToBeReturned + productInventoryLog.getTransactionQuantity();
                        } else if (productInventoryLog.getTransactionType().equals(TransactionType.RETURNED)) {
                            maximumToBeReturned = maximumToBeReturned - productInventoryLog.getTransactionQuantity();
                        }
                    }
                } else {
                    maximumToBeReturned = mapping.getNumberOfUnitsIssued();
                }
                if (maximumToBeReturned < requestMapping.getReturnQuantity()) {
                    throw new ValidationException("Invalid number of units");
                }
                dispensationUpdated = true;
                mapping.setNumberOfUnitsIssued(mapping.getNumberOfUnitsIssued() - requestMapping.getReturnQuantity());
                productDispensationMap.put(requestMapping.getTransactionId(), mapping);
                if(writeInventoryLog) {
                    ProductInventoryLog productInventoryLog = new ProductInventoryLog(TransactionType.RETURNED, mapping.getId(),
                            requestMapping.getReturnQuantity(), CommentType.REASON_FOR_RETURN, requestMapping.getReasonForReturn(), null, mapping.getProductId(), requestMapping.getUpdatedBy(), TransactionIdType.INTERNAL);
                    productInventoryLogs.add(productInventoryLog);
                }
                if(mapping.getNumberOfUnitsIssued() == 0)
                    count++;
            }
        }
        if (dispensationUpdated) {
            mappings = new ArrayList<>(productDispensationMap.values());
            DispensationStatus status = count == mappings.size() ? DispensationStatus.RETURNED : DispensationStatus.PARTIALLY_RETURNED;
            dispensation.setStatus(status);
            dispensationRepository.save(dispensation);
            productDispensationMapRepository.saveAll(mappings);
            productInventoryLogRepository.saveAll(productInventoryLogs);
        }
        return productInventoryLogs;
    }

    @Override
    @Transactional(rollbackFor =  Exception.class)
    public List<Long> createReturnDispensationWithInventoryData(ReturnDispensationRequest returnDispensationRequest, Long clientId) {
        List<ProductInventoryLog> productInventoryLogs = createReturnDispensation(returnDispensationRequest, clientId, false);
        productInventoryLogs.addAll(setReturnDispensationStockData(returnDispensationRequest, clientId));
        return productInventoryLogs
                .stream()
                .map(ProductInventoryLog::getId)
                .collect(Collectors.toList());
    }


    @Override
    public Map<Long, List<ProductInventoryLogDto>> getProductInventoryLogMap(List<Long> productDispensationMappingIds) {
        List<ProductInventoryLog> productInventoryLogList = productInventoryLogRepository.findAllByTransactionIdIn(productDispensationMappingIds);
        return stockService.getProductInventoryLogDto(productInventoryLogList)
                .stream()
                .collect(Collectors.groupingBy(ProductInventoryLogDto::getTransactionId));
    }

    @Override
    public List<ProductInventoryLog> setReturnDispensationStockData(ReturnDispensationRequest returnDispensationRequest, Long clientId) {
        Dispensation dispensation = getDispensation(returnDispensationRequest.getDispensationId(), clientId);
        List<ProductInventoryLog> productInventoryLogsWithBatchNumber = new ArrayList<>();
        List<ProductInventoryLog> productInventoryLogsWithoutBatchNumber = new ArrayList<>();
        List<Long> transactionIds = returnDispensationRequest.getReturnDispensationProductDetails()
                .stream()
                .map(ReturnDispensationRequest.ReturnDispensationData::getTransactionId)
                .collect(Collectors.toList());
        List<ProductDispensationMapping> productDispensationMappings = productDispensationMapRepository.findByIdIn(transactionIds);
        Map<Long, ProductDispensationMapping> productDispensationMappingMap = productDispensationMappings.stream().collect(Collectors.toMap(ProductDispensationMapping::getId, mapping->mapping));
        for(ReturnDispensationRequest.ReturnDispensationData returnDispensationData : returnDispensationRequest.getReturnDispensationProductDetails()){
            Long productId = productDispensationMappingMap.get(returnDispensationData.getTransactionId()).getProductId();
            StockRequest stockRequest = new StockRequest(dispensation.getIssuingFacility(), productId, returnDispensationData.getBatchNumber(), returnDispensationData.getExpiryDate(),
                    returnDispensationData.getReturnQuantity(), returnDispensationData.getTransactionId(), returnDispensationData.getUpdatedBy(), returnDispensationData.getReasonForReturn());
            if(!StringUtils.isBlank(returnDispensationData.getBatchNumber())) {
                productInventoryLogsWithBatchNumber.add(stockService.setStockData(stockRequest, TransactionType.RETURNED, clientId));
            } else {
                ProductInventoryLog productInventoryLog = stockService.setInventoryLogData(stockRequest, TransactionType.RETURNED, null);
                productInventoryLogsWithoutBatchNumber.add(productInventoryLog);
            }
        }
        productInventoryLogsWithBatchNumber.addAll(productInventoryLogsWithoutBatchNumber);
        return productInventoryLogsWithBatchNumber;
    }

    @Override
    @Transactional(rollbackFor =  Exception.class)
    public Dispensation createDispensationWithInventoryData(DispensationRequest dispensationRequest, Long clientId) {
        Dispensation dispensation =  create(dispensationRequest, clientId);
        setAddDispensationStockData(dispensationRequest, clientId, dispensation.getId(), dispensation.getIssueDate());
        return  dispensation;
    }

    @Override
    public Set<ProductMappingInventoryDto> mergeSimilarProductMappings(List<DispensationRequest.ProductData> productDataList, Date issuedDate) {
        Map<String, ProductMappingInventoryDto> productIdProductConfigIdToEntityMap = new HashMap<>();
        for(DispensationRequest.ProductData productData : productDataList) {
            String key = "" + productData.getProductId() + productData.getProductConfigId();
            ProductMappingInventoryDto productMappingInventoryDto = productIdProductConfigIdToEntityMap.get(key);
            if(null == productMappingInventoryDto) {
                productMappingInventoryDto = new ProductMappingInventoryDto(productData.getProductId(), productData.getProductConfigId(), 0L,
                        productData.getDosingSchedule(), null, new HashMap<>(), new HashMap<>());
            }
            if(null != productData.getUnitsIssued()) {
                productMappingInventoryDto.setUnitsIssued(productMappingInventoryDto.getUnitsIssued() + productData.getUnitsIssued());
                DispensationRequest.ProductData requestProductData = new DispensationRequest.ProductData(productData.getProductId(), productData.getProductConfigId(), productMappingInventoryDto.getUnitsIssued(),productMappingInventoryDto.getDosingSchedule());
                productMappingInventoryDto.setRefillDate(getDispensedProductRefillDate(requestProductData, issuedDate));
            }
            if(!StringUtils.isBlank(productData.getBatchNumber())) {
                productMappingInventoryDto.getBatchNumberToQuantity().put(productData.getBatchNumber(), productData.getUnitsIssued());
                productMappingInventoryDto.getBatchNumberToExpiryDateMap().put(productData.getBatchNumber(), productData.getExpiryDate());
            }
            productIdProductConfigIdToEntityMap.put(key, productMappingInventoryDto);
        }
        return new HashSet<>(productIdProductConfigIdToEntityMap.values());
    }

    @Override
    public void setAddDispensationStockData(DispensationRequest dispensationRequest, Long clientId, Long dispensationId, Date issuedDate) {
        Long transactionId ;
        Set<ProductMappingInventoryDto> productMappingInventoryDtoSet = new HashSet<>(mergeSimilarProductMappings(dispensationRequest.getProductDataList(), issuedDate));
        for(ProductMappingInventoryDto productMappingInventoryDto : productMappingInventoryDtoSet) {
            ProductDispensationMapping productDispensationMapping = new ProductDispensationMapping(productMappingInventoryDto.getProductId(), productMappingInventoryDto.getProductConfigId(),
                    dispensationId, productMappingInventoryDto.getUnitsIssued(), productMappingInventoryDto.getDosingSchedule());
            productDispensationMapping.setRefillDate(productMappingInventoryDto.getRefillDate());
            productDispensationMapRepository.save(productDispensationMapping);
            transactionId = productDispensationMapping.getId();
            StockRequest stockRequest = new StockRequest(dispensationRequest.getIssuingFacility(), productMappingInventoryDto.getProductId(), null, null, null, transactionId, dispensationRequest.getAddedBy(), dispensationRequest.getNotes());
            if(!productMappingInventoryDto.getBatchNumberToExpiryDateMap().isEmpty()) {
                for (Map.Entry<String, String > batchNumberToExpiryDateMap : productMappingInventoryDto.getBatchNumberToExpiryDateMap().entrySet()) {
                    Long quantityPerBatch = productMappingInventoryDto.getBatchNumberToQuantity().get(batchNumberToExpiryDateMap.getKey());
                    stockRequest.setQuantity(quantityPerBatch);
                    stockRequest.setBatchNumber(batchNumberToExpiryDateMap.getKey());
                    stockRequest.setExpiryDate(batchNumberToExpiryDateMap.getValue());
                    stockService.setStockData(stockRequest, TransactionType.ISSUED, clientId);
                }
            }else {
                stockRequest.setQuantity(productMappingInventoryDto.getUnitsIssued());
                stockService.setInventoryLogData(stockRequest, TransactionType.ISSUED, null);
            }
        }
    }
}

package com.everwell.dispensationservice.services.impl;

import com.everwell.dispensationservice.Utils.Utils;
import com.everwell.dispensationservice.auth.JwtToken;
import com.everwell.dispensationservice.constants.Constants;
import com.everwell.dispensationservice.models.db.Client;
import com.everwell.dispensationservice.services.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    @Autowired
    private JwtToken jwtTokenUtil;

    @Override
    public String generateToken(Client client) {
        long refreshTime = Utils.getCurrentDate().getTime() + Constants.REFRESH_EXPIRY;
        return jwtTokenUtil.generateToken(client.getId().toString(), client.getName(), new Date(refreshTime));
    }

    @Override
    public Boolean isValidToken(String token, Client client) {
        return jwtTokenUtil.isValidToken(token, client.getId().toString(), client.getName());
    }
}

package com.everwell.dispensationservice.services;

import com.everwell.dispensationservice.enums.TransactionType;
import com.everwell.dispensationservice.models.db.ProductInventory;
import com.everwell.dispensationservice.models.db.ProductInventoryHierarchyMapping;
import com.everwell.dispensationservice.models.db.ProductInventoryLog;
import com.everwell.dispensationservice.models.dto.ProductInventoryDto;
import com.everwell.dispensationservice.models.dto.ProductInventoryHierarchyMappingDto;
import com.everwell.dispensationservice.models.dto.ProductInventoryLogDto;
import com.everwell.dispensationservice.models.dto.ProductStockData;
import com.everwell.dispensationservice.models.http.requests.ProductStockSearchRequest;
import com.everwell.dispensationservice.models.http.requests.StockRequest;

import java.util.List;

public interface StockService {

    ProductInventory getInventoryData(ProductInventoryDto productInventoryDto);

    ProductInventory addInventoryData(ProductInventoryDto productInventoryDto);

    ProductInventoryHierarchyMapping getInventoryHierarchyData(ProductInventoryHierarchyMappingDto productInventoryHierarchyMappingDto);

    ProductInventoryHierarchyMapping addInventoryHierarchyData(ProductInventoryHierarchyMappingDto productInventoryHierarchyMappingDto);

    ProductInventoryHierarchyMapping updateInventoryHierarchyData(ProductInventoryHierarchyMapping productInventoryHierarchyMapping, Long quantity, Long lastUpdatedBy);

    ProductInventoryLog addInventoryLog(ProductInventoryLogDto productInventoryLogDto);

    List<ProductInventory> getAllInventoryDataByProductIds(List<Long> productIds);

    List<ProductInventoryHierarchyMapping> getAllInventoryHierarchyDataByInventoryIdsAndHierarchyIds(List<Long> inventoryIds, List<Long> hierarchyMappingIds);

    Long setInventoryData(ProductInventoryDto productInventoryDto, TransactionType transactionType);

    ProductInventoryHierarchyMapping setInventoryHierarchyData(ProductInventoryHierarchyMappingDto productInventoryHierarchyMappingdto, TransactionType transactionType);

    ProductInventoryLog setInventoryLogData(StockRequest stockRequest, TransactionType transactionType,  Long pInventoryId);

    ProductInventoryLog setStockData(StockRequest stockRequest, TransactionType transactionType, Long clientId);

    List<ProductStockData> getProductStockData(ProductStockSearchRequest productStockSearchRequest);

    List<ProductInventoryLogDto> getProductInventoryLogDto(List<ProductInventoryLog> productInventoryLog);
}

package com.everwell.dispensationservice.Utils;

import io.sentry.event.EventBuilder;
import io.sentry.event.helper.BasicRemoteAddressResolver;
import io.sentry.event.interfaces.ExceptionInterface;
import io.sentry.event.interfaces.HttpInterface;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;
import java.io.InputStream;

public class SentryUtils {

    public static EventBuilder eventBuilder(Exception ex, WebRequest request, InputStream inputStream) throws IOException {
        EventBuilder builder = new EventBuilder()
                .withSentryInterface(new ExceptionInterface(ex))
                .withSentryInterface(new HttpInterface(((ServletWebRequest) request).getRequest(), new BasicRemoteAddressResolver(),
                        Utils.readRequestBody(inputStream)));
        return builder;
    }

    public static EventBuilder eventBuilder(Exception ex) {
        EventBuilder builder = new EventBuilder()
                .withSentryInterface(new ExceptionInterface(ex));
        return builder;
    }

}

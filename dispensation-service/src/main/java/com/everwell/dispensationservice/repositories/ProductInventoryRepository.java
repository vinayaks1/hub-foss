package com.everwell.dispensationservice.repositories;

import com.everwell.dispensationservice.models.db.ProductInventory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface ProductInventoryRepository extends JpaRepository<ProductInventory, Long> {

    ProductInventory findProductInventoryByProductIdAndBatchNumber(Long productId, String batchNumber);

    List<ProductInventory> findAllByProductIdIn(List<Long> productsIds);

    List<ProductInventory> findAllByIdIn(List<Long> ids);
}

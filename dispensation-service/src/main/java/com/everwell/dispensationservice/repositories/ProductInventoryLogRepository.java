package com.everwell.dispensationservice.repositories;

import com.everwell.dispensationservice.models.db.ProductInventoryLog;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductInventoryLogRepository extends JpaRepository<ProductInventoryLog, Long> {

    List<ProductInventoryLog> findAllByTransactionIdIn(List<Long> mappingIds);
}

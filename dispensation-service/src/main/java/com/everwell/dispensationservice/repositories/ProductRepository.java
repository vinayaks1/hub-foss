package com.everwell.dispensationservice.repositories;

import com.everwell.dispensationservice.models.db.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {

    Product findByProductNameAndDrugCodeAndDosageAndDosageFormAndManufacturerAndComposition(String productName, String drugCode, String dosage, String dosageForm, String manufacturer, String composition);

    List<Product> findAllByDrugCodeIn(List<String> drugCodes);
}

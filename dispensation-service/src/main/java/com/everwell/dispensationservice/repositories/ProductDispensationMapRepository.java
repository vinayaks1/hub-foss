package com.everwell.dispensationservice.repositories;

import com.everwell.dispensationservice.models.db.ProductDispensationMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;

import javax.persistence.LockModeType;
import java.util.Collection;
import java.util.List;

public interface ProductDispensationMapRepository extends JpaRepository<ProductDispensationMapping, Long> {
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    List<ProductDispensationMapping> findAllByDispensationIdIn(Collection<Long> ids);

    List<ProductDispensationMapping> findByIdIn(Collection<Long> ids);

}

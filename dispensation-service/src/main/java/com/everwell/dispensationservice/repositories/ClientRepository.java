package com.everwell.dispensationservice.repositories;

import com.everwell.dispensationservice.models.db.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client, Long> {

    Client findClientByName(String name);
}

package com.everwell.dispensationservice.repositories;

import com.everwell.dispensationservice.models.db.ProductInventoryHierarchyMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;

import javax.persistence.LockModeType;
import java.util.List;
import java.util.Optional;

public interface ProductInventoryHierarchyMappingRepository extends JpaRepository<ProductInventoryHierarchyMapping, Long> {
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    ProductInventoryHierarchyMapping findProductInventoryHierarchyMappingByInventoryIdAndHierarchyMappingIdAndClientId(Long inventoryId, Long hierarchyMappingId, Long clientId);

    List<ProductInventoryHierarchyMapping> findAllByInventoryIdInAndHierarchyMappingIdIn(List<Long> inventoryIds, List<Long> hierarchyMappingIds);

    List<ProductInventoryHierarchyMapping> findAllByIdIn(List<Long> ids);
}
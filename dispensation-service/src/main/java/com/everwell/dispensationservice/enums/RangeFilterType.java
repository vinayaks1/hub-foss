package com.everwell.dispensationservice.enums;

public enum RangeFilterType {

    NEXT_REFILL_DATE,
    NUMBER_OF_DISPENSATIONS

}

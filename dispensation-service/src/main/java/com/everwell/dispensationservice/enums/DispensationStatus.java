package com.everwell.dispensationservice.enums;

public enum DispensationStatus {
    ISSUED,
    RETURNED,
    PARTIALLY_RETURNED
}

package com.everwell.dispensationservice.enums.event;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EventActionEnum {
    DB_RECORD_SAVE("DB Record - Save"),
    DB_RECORD_FETCH("DB Record - Fetch"),
    DB_RECORD_DELETE("DB Record - Delete"),
    DB_RECORD_UPDATE("DB Record - Update"),
    ELASTIC_SEARCH_MUST_MATCH_FETCH("Elastic Search must match search with value %s");
    private String name;
}
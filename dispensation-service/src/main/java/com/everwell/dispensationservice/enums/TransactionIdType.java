package com.everwell.dispensationservice.enums;

public enum TransactionIdType {
    INTERNAL,
    EXTERNAL
}

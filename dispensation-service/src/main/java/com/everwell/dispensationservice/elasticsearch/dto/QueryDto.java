package com.everwell.dispensationservice.elasticsearch.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class QueryDto {
    List<MatchSearchDto> matchSearchList;
}

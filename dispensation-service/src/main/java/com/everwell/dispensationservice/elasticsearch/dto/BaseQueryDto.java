package com.everwell.dispensationservice.elasticsearch.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class BaseQueryDto {
    public String searchKey;
    public String searchTerm;
}

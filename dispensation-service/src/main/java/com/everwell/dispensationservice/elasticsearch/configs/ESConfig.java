package com.everwell.dispensationservice.elasticsearch.configs;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.config.AbstractElasticsearchConfiguration;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

@Configuration
@EnableElasticsearchRepositories(basePackages = "com.everwell.dispensationservice.elasticsearch.data")
public class ESConfig extends AbstractElasticsearchConfiguration {

    private static final Logger LOGGER= LoggerFactory.getLogger(AbstractElasticsearchConfiguration.class);

    @Value("${elasticsearch.url}")
    public String elasticSearchUrl;

    @Value("${elasticsearch.username}")
    public String elasticSearchUsername;

    @Value("${elasticsearch.password}")
    public String elasticSearchPassword;

    @Value("${elasticsearch.port}")
    public int elasticSearchPort;

    @Value("${elasticsearch.protocol}")
    public String elasticSearchProtocol;

    @Bean
    @Override
    public RestHighLevelClient elasticsearchClient() {
        final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY,
                new UsernamePasswordCredentials(elasticSearchUsername, elasticSearchPassword));

        RestClientBuilder builder = RestClient.builder(new HttpHost(elasticSearchUrl, elasticSearchPort, elasticSearchProtocol))
                .setHttpClientConfigCallback(httpClientBuilder -> httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider));

        RestHighLevelClient client = new RestHighLevelClient(builder);
        LOGGER.debug("Connected successfully to Elastic Search at: " + elasticSearchUrl);
        return client;
    }
}
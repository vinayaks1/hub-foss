package com.everwell.dispensationservice.elasticsearch.service;

import com.everwell.dispensationservice.elasticsearch.dto.BaseSearchRequest;
import com.everwell.dispensationservice.elasticsearch.dto.QueryDto;
import com.everwell.dispensationservice.elasticsearch.dto.SupportedSearchMethods;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.index.query.*;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

import static com.everwell.dispensationservice.elasticsearch.constants.ElasticConstants.*;

@Component
public class ESQueryBuilder {

    public BoolQueryBuilder build(BaseSearchRequest baseSearchRequest) {
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
        if (null != baseSearchRequest.getSearch()) {
            SupportedSearchMethods search = baseSearchRequest.getSearch();
            // TODO : add support for must_not, filter when required
            if (search.getMust() != null) {
                buildBoolQuery(queryBuilder, search.getMust(), ELASTIC_SEARCH_KEYWORD_MUST);
            }
        }
        return queryBuilder;
    }

    private void buildBoolQuery (BoolQueryBuilder boolQuery, QueryDto queryDto, String queryType) {
        List<AbstractQueryBuilder> queryBuilderList = getQuery(queryDto);
        if (ELASTIC_SEARCH_KEYWORD_MUST.equalsIgnoreCase(queryType)) {
            queryBuilderList.forEach(boolQuery::must);
        }
    }

    private List<AbstractQueryBuilder> getQuery(QueryDto queryDto) {
        List<AbstractQueryBuilder> queryBuilderList = new ArrayList<>();
        // TODO : add support for term, terms, multi-match when required
        if (!CollectionUtils.isEmpty(queryDto.getMatchSearchList())) {
            queryDto.getMatchSearchList().forEach(matchSearchDto -> {
                matchSearchDto.validate();
                Object fuzzinessValue = (null != matchSearchDto.getFuzziness()) ? FUZZINESS_VALUE_TO_OBJECT_MAP.get(matchSearchDto.getFuzziness()) : Fuzziness.AUTO;
                Operator operator = (null != matchSearchDto.getOperator()) ? OPERATOR_VALUE_TO_OBJECT_MAP.get(matchSearchDto.getOperator()) : Operator.OR;
                queryBuilderList.add(QueryBuilders.matchQuery(matchSearchDto.getSearchKey(), matchSearchDto.getSearchTerm())
                        .fuzziness(fuzzinessValue)
                        .fuzzyTranspositions(matchSearchDto.isFuzzyTranspositions())
                        .operator(operator)
                        .prefixLength(matchSearchDto.getPrefixLength()));
            });
        }
        return  queryBuilderList;
    }
}

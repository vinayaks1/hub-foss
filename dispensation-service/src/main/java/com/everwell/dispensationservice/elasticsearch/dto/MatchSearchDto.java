package com.everwell.dispensationservice.elasticsearch.dto;

import com.everwell.dispensationservice.elasticsearch.enums.ElasticSearchValidation;
import com.everwell.dispensationservice.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import static com.everwell.dispensationservice.elasticsearch.constants.ElasticConstants.ELASTIC_QUERY_OPERATORS;


@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class MatchSearchDto extends BaseQueryDto{
    boolean fuzzyTranspositions = true;
    Long fuzziness;
    String operator;
    Integer prefixLength = 0;

    public MatchSearchDto(String searchKey, String searchTerm) {
        super(searchKey, searchTerm);
    }

    public MatchSearchDto(String searchKey, String searchTerm, boolean fuzzyTranspositions, Long fuzziness, String operator, Integer prefixLength) {
        super(searchKey, searchTerm);
        this.fuzzyTranspositions = fuzzyTranspositions;
        this.fuzziness = fuzziness;
        this.operator = operator;
        this.prefixLength = prefixLength;
    }

    public void validate() {
        if (null == searchKey || null == searchTerm) {
            throw new ValidationException(ElasticSearchValidation.INVALID_SEARCH_KEY_AND_SEARCH_TERM_VALUE.getMessage());
        }
        if (null != fuzziness && (fuzziness < 0 || fuzziness > 2)) {
            throw new ValidationException(ElasticSearchValidation.INVALID_FUZZINESS_VALUE.getMessage());
        }
        if (null != operator && !(ELASTIC_QUERY_OPERATORS.contains(operator))) {
            throw new ValidationException(ElasticSearchValidation.INVALID_SEARCH_OPERATOR_VALUE.getMessage());
        }
        if (prefixLength < 0) {
            throw new ValidationException(ElasticSearchValidation.INVALID_PREFIX_LENGTH_VALUE.getMessage());
        }
    }
}

package com.everwell.dispensationservice.elasticsearch.dto;


import com.everwell.dispensationservice.elasticsearch.indices.ProductIndex;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductSearchResult {
    long totalHits;
    List<ProductIndex> productIndexList;
}

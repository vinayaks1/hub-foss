package com.everwell.dispensationservice.elasticsearch.service;

import com.everwell.dispensationservice.elasticsearch.dto.ProductSearchResult;
import com.everwell.dispensationservice.elasticsearch.indices.ProductIndex;
import com.everwell.dispensationservice.models.http.requests.ProductSearchRequest;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;

import java.util.List;

public interface ESProductService {

    void save(ProductIndex productIndex);

    void saveAll(List<ProductIndex> productIndexList);

    ProductSearchResult searchProducts(ProductSearchRequest productSearchRequest);

    ProductSearchResult performSearch(NativeSearchQuery nativeSearchQuery);
}

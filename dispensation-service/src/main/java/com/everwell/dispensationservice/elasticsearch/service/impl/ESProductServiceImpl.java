package com.everwell.dispensationservice.elasticsearch.service.impl;

import com.everwell.dispensationservice.elasticsearch.data.ESProductRepository;
import com.everwell.dispensationservice.elasticsearch.dto.ProductSearchResult;
import com.everwell.dispensationservice.elasticsearch.indices.ProductIndex;
import com.everwell.dispensationservice.elasticsearch.service.ESProductService;
import com.everwell.dispensationservice.elasticsearch.service.ESQueryBuilder;
import com.everwell.dispensationservice.models.http.requests.ProductSearchRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ESProductServiceImpl implements ESProductService {

    @Autowired
    ESProductRepository esProductRepository;

    @Autowired
    ESQueryBuilder esQueryBuilder;

    @Autowired
    private ElasticsearchOperations elasticsearchOperations;

    @Override
    public void save(ProductIndex productIndex) {
        esProductRepository.save(productIndex);
    }

    @Override
    public void saveAll(List<ProductIndex> productIndexList) {
        esProductRepository.saveAll(productIndexList);
    }

    @Override
    public ProductSearchResult searchProducts(ProductSearchRequest productSearchRequest) {
        NativeSearchQueryBuilder searchQueryBuilder = new NativeSearchQueryBuilder()
                .withQuery(esQueryBuilder.build(productSearchRequest))
                .withPageable(PageRequest.of(productSearchRequest.getPage(), productSearchRequest.getSize()));
        return performSearch(searchQueryBuilder.build());
    }

    @Override
    public ProductSearchResult performSearch(NativeSearchQuery nativeSearchQuery) {
        SearchHits<ProductIndex> productIndexSearchHits = elasticsearchOperations.search(nativeSearchQuery, ProductIndex.class);
        List<ProductIndex> productIndexList = productIndexSearchHits.getSearchHits()
                .stream()
                .map(SearchHit::getContent)
                .collect(Collectors.toList());
        return new ProductSearchResult(productIndexSearchHits.getTotalHits(), productIndexList);
    }
}

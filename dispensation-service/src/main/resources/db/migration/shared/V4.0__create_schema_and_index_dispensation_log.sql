create table if not exists ds_product_dispensation_log
(
	id bigserial not null
		constraint ds_dispensation_log_pkey
			primary key,
	type int4 not null,
	product_dispensation_mapping_id bigint not null,
	number_of_units bigint not null,
	comment_type int4,
	comment varchar(255),
	logged_date timestamp not null
);
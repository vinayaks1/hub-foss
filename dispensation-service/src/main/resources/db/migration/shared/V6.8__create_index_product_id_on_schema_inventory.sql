create index if not exists idx_productid
    on ds_product_inventory (product_id);
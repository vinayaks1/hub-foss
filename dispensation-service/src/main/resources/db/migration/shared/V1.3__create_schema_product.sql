create table if not exists ds_product
(
	id bigserial not null
		constraint ds_product_pkey
			primary key,
	composition varchar(255),
	dosage varchar(255),
	dosage_form varchar(255) not null,
	drug_code varchar(255),
	manufacturer varchar(255),
	product_name varchar(255) not null
);
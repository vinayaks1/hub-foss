create index if not exists idx_dispensationid
	on ds_product_dispensation_mapping (dispensation_id);
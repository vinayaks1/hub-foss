create table if not exists ds_client
(
	id bigserial not null
		constraint ds_client_pkey
			primary key,
	created_date timestamp,
	name varchar(255),
	password varchar(255)
);
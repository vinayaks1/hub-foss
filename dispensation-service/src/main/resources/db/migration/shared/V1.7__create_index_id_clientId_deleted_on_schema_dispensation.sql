create index if not exists idx_id_clientid_deleted
	on ds_dispensation (id, client_id, deleted);
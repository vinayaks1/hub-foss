create table if not exists ds_product_dispensation_mapping
(
	id bigserial not null
		constraint ds_product_dispensation_mapping_pkey
			primary key,
	dispensation_id bigint not null,
	dosing_schedule varchar(255),
	number_of_units_issued bigint,
	product_config_id bigint not null,
	product_id bigint not null,
	refill_date timestamp
);
create index if not exists idx_inventoryid_hierarchymappingid_clientid
    on ds_product_inventory_hierarchy_mapping (inventory_id, hierarchy_mapping_id, client_id);
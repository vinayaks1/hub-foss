create index if not exists idx_entityid_clientid_deleted
	on ds_dispensation (entity_id, client_id, deleted);
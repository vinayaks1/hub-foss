package com.everwell.dispensationservice.unit.services;

import com.everwell.dispensationservice.Utils.CacheUtils;
import com.everwell.dispensationservice.binders.EpisodeFieldsBinder;
import com.everwell.dispensationservice.constants.Constants;
import com.everwell.dispensationservice.enums.*;
import com.everwell.dispensationservice.exceptions.ValidationException;
import com.everwell.dispensationservice.models.db.*;
import com.everwell.dispensationservice.models.dto.*;
import com.everwell.dispensationservice.models.dto.DispensationData;
import com.everwell.dispensationservice.models.dto.DispensationProductDto;
import com.everwell.dispensationservice.models.dto.ProductInventoryLogDto;
import com.everwell.dispensationservice.models.dto.RangeValueFilter;
import com.everwell.dispensationservice.models.http.requests.ReturnDispensationRequest;
import com.everwell.dispensationservice.models.http.responses.EntityDispensationResponse;
import com.everwell.dispensationservice.repositories.ProductInventoryLogRepository;
import com.everwell.dispensationservice.repositories.ProductDispensationMapRepository;
import com.everwell.dispensationservice.services.StockService;
import com.everwell.dispensationservice.services.impl.DispensationServiceImpl;
import com.everwell.dispensationservice.repositories.*;
import com.everwell.dispensationservice.services.ProductService;
import com.everwell.dispensationservice.Utils.Utils;
import com.everwell.dispensationservice.exceptions.NotFoundException;
import com.everwell.dispensationservice.models.http.requests.DispensationRequest;
import com.everwell.dispensationservice.unit.BaseTestNoSpring;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.mockito.junit.MockitoJUnit;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.ExecutionException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


public class DispensationServiceTest extends BaseTestNoSpring {

    @Spy
    @InjectMocks
    private DispensationServiceImpl dispensationService;

    @MockBean
    private DispensationRepository dispensationRepository;

    @MockBean
    private ProductDispensationMapRepository productDispensationMapRepository;

    @MockBean
    private ProductInventoryLogRepository productInventoryLogRepository;

    @MockBean
    private StockService stockService;

    @MockBean
    private ProductInventoryHierarchyMappingRepository productInventoryHierarchyMappingRepository;

    @MockBean
    private ProductInventoryRepository productInventoryRepository;

    @MockBean
    private ProductService productService;

    @MockBean
    private ApplicationEventPublisher applicationEventPublisher;

    CacheUtils cacheUtils;

    @Mock
    RedisTemplate<String, Object> redisTemplate;

    @Mock
    private ValueOperations<String, Object> valueOperations;

    @BeforeEach
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(dispensationService, "applicationEventPublisher", applicationEventPublisher);
        cacheUtils = new CacheUtils(redisTemplate);
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
    }

    private Dispensation DISPENSATION;


    private List<Product> products() {
        List<Product> productList = new ArrayList<>();
        productList.add(new Product(1L, "AAA", "ProductA", "dosageA", "dosageFormA", "manufacturerA", "compositionA", "category"));
        productList.add(new Product(2L, "BBB", "ProductB", "dosageB", "dosageFormB", "manufacturerB", "compositionB", "category"));
        productList.add(new Product(3L, "CCC", "ProductC", "dosageC", "dosageFormC", "manufacturerC", "compositionC", "category"));
        productList.add(new Product(4L, "DDD", "ProductD", "dosageD", "dosageFormD", "manufacturerD", "compositionD", "category"));
        productList.add(new Product(5L, "EEE", "ProductE", "dosageE", "dosageFormE", "manufacturerE", "compositionE", "category"));
        productList.add(new Product(6L, "FFF", "ProductF", "dosageF", "dosageFormF", "manufacturerF", "compositionF", "category"));
        productList.add(new Product(7L, "GGG", "ProductG", "dosageG", "dosageFormG", "manufacturerG", "compositionG", "category"));
        return productList;
    }

    private Optional<Dispensation> dispensation_optional_entity() throws ParseException {
        return java.util.Optional.of(new Dispensation(1L,1L, 1L, 23L, 33L, "30-40Kg", "Intensive", 1L, 0L, Utils.convertStringToDate("10-07-2020 00:00:00"), Utils.convertStringToDate("10-07-2020 00:00:00"), Utils.convertStringToDate("13-07-2020 00:00:00"), "notes", DispensationStatus.ISSUED));
    }

    private DispensationRequest dispensation_request_object_without_products() {
        return new DispensationRequest(1L, 33L, "30-40Kg", "Intensive", 23L, 1L, "10-07-2020 00:00:00", "10-07-2020 00:00:00", 0L, "10-07-2020 00:00:00", "", new ArrayList<>());
    }

    private DispensationRequest dispensation_request_object_without_products_upated_date() {
        return new DispensationRequest(1L, 33L, "30-40Kg", "Intensive", 23L, 1L, "2020-07-10 00:00:00", "2020-10-07 18:30:00", 0L, "2020-10-07 18:30:00", "", new ArrayList<>());
    }

    private List<DispensationRequest.ProductData> dispensationProductList () {
        List<DispensationRequest.ProductData> dispensationProductList = new ArrayList<>();
        dispensationProductList.add(new DispensationRequest.ProductData(1L, 1L, 10L, "MTWHFSU", "20-07-2020 00:00:00", "ABC", "20-11-2025 00:00:00"));
        dispensationProductList.add(new DispensationRequest.ProductData(2L, 1L, 10L, "MTWHFSU", "20-07-2020 00:00:00", "ABC", "20-11-2025 00:00:00"));
        dispensationProductList.add(new DispensationRequest.ProductData(3L, 1L, 10L, "MTWHFSU", "20-07-2020 00:00:00", "ABC", "20-11-2025 00:00:00"));
        dispensationProductList.add(new DispensationRequest.ProductData(4L, 1L, 10L, "MTWHFSU", "20-07-2020 00:00:00", "ABC",  "20-11-2025 00:00:00"));
        return dispensationProductList;
    }

    private List<DispensationProductDto> dispensation_product_dto_list() {
        List<DispensationProductDto> dispensationProductDtos = new ArrayList<>();
        dispensationProductDtos.add(new DispensationProductDto(1L, 1L, 3L, 1L, 10L, null, "MTWHFSU"));
        dispensationProductDtos.add(new DispensationProductDto(2L, 2L, 3L, 1L, 10L, null, "MTWHFSU"));
        dispensationProductDtos.add(new DispensationProductDto(3L, 3L, 3L, 1L, 10L, null, "MTWHFSU"));
        return dispensationProductDtos;
    }

    private List<ProductInventoryLogDto> product_inventory_dto_list_with_inventory() {
        List<ProductInventoryLogDto> productInventoryLogDtoList = new ArrayList<>();
        productInventoryLogDtoList.add(new ProductInventoryLogDto(1L, TransactionType.ISSUED, 1L , 10L, CommentType.REASON_FOR_ADD,
                "issued 1 5 units", Utils.getCurrentDate(), 1L, 1L,1L,TransactionIdType.INTERNAL, "ABC123", Utils.convertStringToDateOrDefault("10-11-2025 00:00:00")));
        productInventoryLogDtoList.add(new ProductInventoryLogDto(2L, TransactionType.RETURNED, 1L, 5L, CommentType.REASON_FOR_RETURN,
                "returned 1 5 units", Utils.getCurrentDate(), 1L, 1L,1L,TransactionIdType.INTERNAL, "ABC123", Utils.convertStringToDateOrDefault("10-11-2025 00:00:00")));
        return productInventoryLogDtoList;
    }

    private List<ProductInventoryLogDto> product_inventory_dto_list_without_inventory() {
        List<ProductInventoryLogDto> productInventoryLogDtoList = new ArrayList<>();
        productInventoryLogDtoList.add(new ProductInventoryLogDto(1L, TransactionType.ISSUED, 2L , 10L, CommentType.REASON_FOR_ADD,
                "issued 1 5 units", Utils.getCurrentDate(), null, 1L,1L,TransactionIdType.INTERNAL, null , null));
        productInventoryLogDtoList.add(new ProductInventoryLogDto(2L, TransactionType.RETURNED, 2L, 5L, CommentType.REASON_FOR_RETURN,
                "returned 1 5 units", Utils.getCurrentDate(), null, 1L,1L,TransactionIdType.INTERNAL, null , null));
        return productInventoryLogDtoList;
    }

    @Test
    public void test_get_dispensation() throws ParseException {
        when(dispensationRepository.findByIdAndClientIdAndDeletedIsFalse(any(), any())).thenReturn(dispensation_optional_entity());
        Dispensation dispensation = dispensationService.getDispensation(1L, 1L);
        assertEquals(1L, dispensation.getId().longValue());
    }

    @Test()
    public void test_get_dispensation_throws_not_found_exception() {
        when(dispensationRepository.findById(any())).thenReturn(Optional.empty());
        Assertions.assertThrows(NotFoundException.class, () ->dispensationService.getDispensation(1L, 1L));
    }

    @Test
    public void test_delete_dispensation() throws ParseException {
        when(dispensationRepository.findByIdAndClientIdAndDeletedIsFalse(any(), any())).thenReturn(dispensation_optional_entity());
        Dispensation deletedDispensation = dispensationService.delete(1L, 1L);
        assertTrue(deletedDispensation.isDeleted());
    }

    @Test()
    public void test_delete_dispensation_throws_not_found_exception() {
        when(dispensationRepository.findById(any())).thenReturn(Optional.empty());
        Assertions.assertThrows(NotFoundException.class, () -> dispensationService.delete(1L, 1L));
    }

    @Test
    public void test_create_dispensation() throws ParseException {
        when(dispensationRepository.save(any())).thenAnswer((Answer) invocation -> {
            DISPENSATION = (Dispensation) invocation.getArguments()[0];
            DISPENSATION = new Dispensation(dispensation_optional_entity().get().getId(), DISPENSATION.getEntityId(), DISPENSATION.getClientId(), dispensation_optional_entity().get().getAddedDate(), DISPENSATION.getAddedBy(),
            dispensation_optional_entity().get().getDateOfPrescription(), DISPENSATION.getWeight(), DISPENSATION.getWeightBand(), DISPENSATION.getPhase(), DISPENSATION.getDrugDispensedForDays(), DISPENSATION.getRefillDate(), DISPENSATION.getIssueDate(), DISPENSATION.getDosingStartDate(), DISPENSATION.getIssuingFacility(), false, "", DispensationStatus.ISSUED);
            return DISPENSATION;
        });
        doNothing().when(dispensationService).emitForEpisode(any(), anyLong());
        dispensationService.create(dispensation_request_object_without_products(), dispensation_optional_entity().get().getClientId());
        Dispensation actual = dispensation_optional_entity().get();
        assertEquals(actual.getId(),DISPENSATION.getId());
        assertEquals(actual.getEntityId(), DISPENSATION.getEntityId());
        assertEquals(actual.getIssueDate(), DISPENSATION.getIssueDate());
    }

    @Test
    public void test_create_dispensation_with_updated_date_format() throws ParseException {
        when(dispensationRepository.save(any())).thenAnswer((Answer) invocation -> {
            DISPENSATION = (Dispensation) invocation.getArguments()[0];
            DISPENSATION = new Dispensation(dispensation_optional_entity().get().getId(), DISPENSATION.getEntityId(), DISPENSATION.getClientId(), dispensation_optional_entity().get().getAddedDate(), DISPENSATION.getAddedBy(),
                    dispensation_optional_entity().get().getDateOfPrescription(), DISPENSATION.getWeight(), DISPENSATION.getWeightBand(), DISPENSATION.getPhase(), DISPENSATION.getDrugDispensedForDays(), DISPENSATION.getRefillDate(), DISPENSATION.getIssueDate(), DISPENSATION.getDosingStartDate(), DISPENSATION.getIssuingFacility(), false, "", DispensationStatus.ISSUED);
            return DISPENSATION;
        });
        doNothing().when(dispensationService).emitForEpisode(any(), anyLong());
        dispensationService.create(dispensation_request_object_without_products_upated_date(), dispensation_optional_entity().get().getClientId());
        Dispensation actual = dispensation_optional_entity().get();
        assertEquals(actual.getId(),DISPENSATION.getId());
        assertEquals(actual.getEntityId(), DISPENSATION.getEntityId());
        assertEquals(actual.getIssueDate(), DISPENSATION.getIssueDate());
    }

    @Test
    public void test_validate_dispensation_request_throws_validation_exception_issued_date() {
        DispensationRequest dispensationRequest = new DispensationRequest(1L, 33L, "30-40Kg", "Intensive", 23L, 1L, "10-07-2020 00.00.00", "10-07-2020 00.00.00", 0l, "10-07-2020 00:00:00", "", dispensationProductList());
        Exception ex = assertThrows(ValidationException.class, () -> dispensationRequest.validate(dispensationRequest));

        assertEquals("issued date format is not valid", ex.getMessage());
    }

    @Test
    public void test_validate_dispensation_request_throws_validation_exception_entity_id() {
        DispensationRequest dispensationRequest = new DispensationRequest(null, 33L, "30-40Kg", "Intensive", 23L, 1L, "10-07-2020 00.00.00", "10-07-2020 00:00:00", 0l, "10-07-2020 00:00:00", "", dispensationProductList());
        Exception ex = assertThrows(ValidationException.class, () -> dispensationRequest.validate(dispensationRequest));

        assertEquals("entity id is required", ex.getMessage());
    }

    @Test
    public void test_validate_dispensation_request_addedby_required() {
        List<DispensationRequest.ProductData> productList = dispensationProductList();
        productList.add(new DispensationRequest.ProductData(999L, 999L, 1L, "x"));
        DispensationRequest dispensationRequest = new DispensationRequest(1L,
                33L,
                "30-40Kg",
                "Intensive",
                null,
                1L,
                "10-07-2020 00:00:00",
                "10-07-2020 00:00:00",
                0l,
                "10-07-2020 00:00:00",
                "",
                dispensationProductList());
        Exception ex = assertThrows(ValidationException.class, () -> dispensationRequest.validate(dispensationRequest));

        assertEquals("added by is required", ex.getMessage());
    }

    @Test
    public void test_validate_dispensation_request_throws_validation_exception_prescription_date() {
        DispensationRequest dispensationRequest = new DispensationRequest(1L, 33L, "30-40Kg", "Intensive", 23L, 1L, "10-07-2020 00:00:00", "10-07-2020 00:00:00", 0l, "10-07-2020 00.00.00", "", dispensationProductList());
        Exception ex = assertThrows(ValidationException.class, () -> dispensationRequest.validate(dispensationRequest));

        assertEquals("prescription date format is not valid", ex.getMessage());
    }

    @Test
    public void test_validate_dispensation_request() {
        DispensationRequest dispensationRequest = new DispensationRequest(1L, 33L, "30-40Kg", "Intensive", 23L, 1L, "10-07-2020 00:00:00", "10-07-2020 00:00:00", 0l, "10-07-2020 00:00:00", "", dispensationProductList());
        dispensationRequest.validate(dispensationRequest);
    }

    @Test
    public void test_create_dispensation_valid_refill_and_prescription_date() {
        DispensationRequest request = new DispensationRequest(1L, 30L, "30 - 33 Kgs", "Intensive", 1L, 1L, "01-09-2020 00:00:00", "01-09-2020 00:00:00", 30L, "01-09-2020 00:00:00", "Test Note", new ArrayList<>());
        Dispensation expectedDispensation = new Dispensation(1L, request.getEntityId() , 1L, null, 1L, Utils.convertStringToDateOrDefault(request.getDateOfPrescription()), 30L, request.getWeightBand(), request.getPhase(), request.getDrugDispensedForDays(),
                Utils.convertStringToDateOrDefault("01-10-2020 00:00:00"), Utils.convertStringToDateOrDefault(request.getIssuedDate()), Utils.convertStringToDateOrDefault(request.getDosingStartDate()), request.getIssuingFacility(), false, request.getNotes(), DispensationStatus.ISSUED);
        when(dispensationRepository.save(any())).thenReturn(new Dispensation());
        doNothing().when(dispensationService).emitForEpisode(any(), anyLong());
        Dispensation actualDispensation = dispensationService.create(request, 1L);

        assertEquals(actualDispensation.getRefillDate(),expectedDispensation.getRefillDate());
        assertEquals(actualDispensation.getDateOfPrescription(),expectedDispensation.getDateOfPrescription());
        verify(dispensationRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void test_create_dispensation_invalid_issued_and_prescription_date_gives_null_refill_and_prescription_date() {
        DispensationRequest request = new DispensationRequest(1L, 30L, "30 - 33 Kgs", "Intensive", 1L, 1L, "01-09-2020", "01-09-2020", 30L, "01-09-2020", "Test Note", new ArrayList<>());
        doNothing().when(dispensationService).emitForEpisode(any(), anyLong());
        when(dispensationRepository.save(any())).thenReturn(new Dispensation());
        Dispensation actualDispensation = dispensationService.create(request, 1L);

        assertNull(actualDispensation.getRefillDate());
        assertNull(actualDispensation.getDateOfPrescription());
        verify(dispensationRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void test_create_dispensation_null_drugs_dispensed_for_days_gives_null_refill_date() {
        DispensationRequest request = new DispensationRequest(1L, 30L, "30 - 33 Kgs", "Intensive", 1L, 1L, "01-09-2020", "01-09-2020", null, "01-09-2020", "Test Note", new ArrayList<>());
        when(dispensationRepository.save(any())).thenReturn(new Dispensation());
        doNothing().when(dispensationService).emitForEpisode(any(), anyLong());
        Dispensation actualDispensation = dispensationService.create(request, 1L);

        assertNull(actualDispensation.getRefillDate());
        verify(dispensationRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void test_create_dispensation_with_null_refill_date_valid_dosing_start_date() throws ParseException {
        DispensationRequest request = new DispensationRequest(1L, 30L, "30 - 33 Kgs", "Intensive", 1L, 1L, null, "01-09-2020 00:00:00", 10L, "01-09-2020 00:00:00", "Test Note", new ArrayList<>());
        when(dispensationRepository.save(any())).thenReturn(new Dispensation());
        doNothing().when(dispensationService).emitForEpisode(any(), anyLong());
        Dispensation actualDispensation = dispensationService.create(request, 1L);
        Date expectedRefillDate = Utils.convertStringToDate("11-09-2020 00:00:00");

        assertEquals(actualDispensation.getRefillDate(), expectedRefillDate);
        verify(dispensationRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void test_create_dispensation_with_null_dosing_start_date() throws ParseException {
        DispensationRequest request = new DispensationRequest(1L, 30L, "30 - 33 Kgs", "Intensive", 1L, 1L, "01-09-2020 00:00:00", null, 10L, "01-09-2020 00:00:00", "Test Note", new ArrayList<>());
        doNothing().when(dispensationService).emitForEpisode(any(), anyLong());
        when(dispensationRepository.save(any())).thenReturn(new Dispensation());
        Dispensation actualDispensation = dispensationService.create(request, 1L);
        Date expectedRefillDate = Utils.convertStringToDate("11-09-2020 00:00:00");

        assertEquals(actualDispensation.getRefillDate(), expectedRefillDate);
        verify(dispensationRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void test_get_total_days_drug_dispensed_no_overlap_continous() throws ExecutionException, InterruptedException {
        List<Dispensation> dispensationList = new ArrayList<>();
        dispensationList.add(new Dispensation(1L, 1L, 1L, Utils.convertStringToDateOrDefault("01-01-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""),   31L, "", "Intensive", 31L,  Utils.convertStringToDateOrDefault("31-01-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-01-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-01-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        dispensationList.add(new Dispensation(2L, 1L, 1L, Utils.convertStringToDateOrDefault("01-02-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""),   31L, "", "Intensive", 29L,  Utils.convertStringToDateOrDefault("29-02-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-02-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-02-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        dispensationList.add(new Dispensation(3L, 1L, 1L, Utils.convertStringToDateOrDefault("01-03-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""),   31L, "", "Intensive", 31L,  Utils.convertStringToDateOrDefault("31-03-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-03-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-03-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        dispensationList.add(new Dispensation(4L, 1L, 1L, Utils.convertStringToDateOrDefault("01-04-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""),   31L, "", "Intensive", 30L,  Utils.convertStringToDateOrDefault("30-04-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-04-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-04-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        dispensationList.add(new Dispensation(5L, 1L, 1L, Utils.convertStringToDateOrDefault("01-05-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""),   31L, "", "Intensive", 31L,  Utils.convertStringToDateOrDefault("31-05-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-05-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-05-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        dispensationList.add(new Dispensation(6L, 1L, 1L, Utils.convertStringToDateOrDefault("01-06-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""),   31L, "", "Intensive", 30L,  Utils.convertStringToDateOrDefault("30-06-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-06-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-06-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        dispensationList.add(new Dispensation(7L, 1L, 1L, Utils.convertStringToDateOrDefault("01-07-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""),   31L, "", "Intensive", 31L,  Utils.convertStringToDateOrDefault("31-07-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-07-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-07-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        Long totalDays = dispensationService.getTotalDaysDrugsDispensedForEntity(1L, dispensationList).get();

        assertEquals(213, totalDays);
    }

    @Test
    public void test_get_total_days_drug_dispensed_overlap_with_disjoint() throws ExecutionException, InterruptedException {
        List<Dispensation> dispensationList = new ArrayList<>();
        dispensationList.add(new Dispensation(1L, 1L, 1L, Utils.convertStringToDateOrDefault("01-01-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""),   31L, "", "Intensive", 31L, Utils.convertStringToDateOrDefault("31-01-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-01-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-01-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        dispensationList.add(new Dispensation(2L, 1L, 1L, Utils.convertStringToDateOrDefault("01-02-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""),   31L, "", "Intensive", 11L, Utils.convertStringToDateOrDefault("31-01-2020 00:00:00"), Utils.convertStringToDateOrDefault("21-01-2020 00:00:00"), Utils.convertStringToDateOrDefault("21-01-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        dispensationList.add(new Dispensation(3L, 1L, 1L, Utils.convertStringToDateOrDefault("01-03-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""),   31L, "", "Intensive", 22L, Utils.convertStringToDateOrDefault("20-02-2020 00:00:00"), Utils.convertStringToDateOrDefault("30-01-2020 00:00:00"), Utils.convertStringToDateOrDefault("30-01-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        dispensationList.add(new Dispensation(4L, 1L, 1L, Utils.convertStringToDateOrDefault("01-04-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""),   31L, "", "Intensive", 9L,  Utils.convertStringToDateOrDefault("28-02-2020 00:00:00"), Utils.convertStringToDateOrDefault("20-02-2020 00:00:00"), Utils.convertStringToDateOrDefault("20-02-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        dispensationList.add(new Dispensation(5L, 1L, 1L, Utils.convertStringToDateOrDefault("01-05-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""),   31L, "", "Intensive", 21L, Utils.convertStringToDateOrDefault("21-03-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-03-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-03-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        dispensationList.add(new Dispensation(6L, 1L, 1L, Utils.convertStringToDateOrDefault("01-06-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""),   31L, "", "Intensive", 10L, Utils.convertStringToDateOrDefault("03-06-2020 00:00:00"), Utils.convertStringToDateOrDefault("25-05-2020 00:00:00"), Utils.convertStringToDateOrDefault("25-05-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        dispensationList.add(new Dispensation(7L, 1L, 1L, Utils.convertStringToDateOrDefault("01-07-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""),   31L, "", "Intensive", 30L, Utils.convertStringToDateOrDefault("30-07-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-07-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-07-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        Long totalDays = dispensationService.getTotalDaysDrugsDispensedForEntity(1L, dispensationList).get();

        assertEquals(120, totalDays);
    }

    @Test
    public void test_get_total_days_drug_dispensed_overlap_extremes() throws ExecutionException, InterruptedException {
        List<Dispensation> dispensationList = new ArrayList<>();
        dispensationList.add(new Dispensation(1L, 1L, 1L, Utils.convertStringToDateOrDefault("01-01-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""),   31L, "", "Intensive", 31L, Utils.convertStringToDateOrDefault("31-01-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-01-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-01-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        dispensationList.add(new Dispensation(3L, 1L, 1L, Utils.convertStringToDateOrDefault("01-03-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""),   31L, "", "Intensive", 22L, Utils.convertStringToDateOrDefault("20-02-2020 00:00:00"), Utils.convertStringToDateOrDefault("30-01-2020 00:00:00"), Utils.convertStringToDateOrDefault("30-01-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        dispensationList.add(new Dispensation(4L, 1L, 1L, Utils.convertStringToDateOrDefault("01-04-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""),   31L, "", "Intensive", 11L, Utils.convertStringToDateOrDefault("28-02-2020 00:00:00"), Utils.convertStringToDateOrDefault("18-02-2020 00:00:00"), Utils.convertStringToDateOrDefault("18-02-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        dispensationList.add(new Dispensation(5L, 1L, 1L, Utils.convertStringToDateOrDefault("01-05-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""),   31L, "", "Intensive", 25L, Utils.convertStringToDateOrDefault("21-03-2020 00:00:00"), Utils.convertStringToDateOrDefault("26-02-2020 00:00:00"), Utils.convertStringToDateOrDefault("26-02-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        dispensationList.add(new Dispensation(6L, 1L, 1L, Utils.convertStringToDateOrDefault("01-05-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""),   31L, "", "Intensive", 52L, Utils.convertStringToDateOrDefault("21-04-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-03-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-03-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        dispensationList.add(new Dispensation(7L, 1L, 1L, Utils.convertStringToDateOrDefault("01-06-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""),   31L, "", "Intensive", 78L, Utils.convertStringToDateOrDefault("03-06-2020 00:00:00"), Utils.convertStringToDateOrDefault("18-03-2020 00:00:00"), Utils.convertStringToDateOrDefault("18-03-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        dispensationList.add(new Dispensation(8L, 1L, 1L, Utils.convertStringToDateOrDefault("01-07-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""),   31L, "", "Intensive", 30L, Utils.convertStringToDateOrDefault("30-06-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-06-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-06-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        Long totalDays = dispensationService.getTotalDaysDrugsDispensedForEntity(1L, dispensationList).get();

        assertEquals(182, totalDays);
    }

    @Test
    public void test_get_total_days_drug_dispensed_with_no_issue_date_and_no_of_dispensed_drugs () throws ExecutionException, InterruptedException {
        List<Dispensation> dispensationList = new ArrayList<>();
        dispensationList.add(new Dispensation(1L, 1L, 1L, Utils.convertStringToDateOrDefault("01-01-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""),   31L, "", "Intensive", null, Utils.convertStringToDateOrDefault("31-01-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-01-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-01-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        dispensationList.add(new Dispensation(3L, 1L, 1L, Utils.convertStringToDateOrDefault("01-03-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""),   31L, "", "Intensive", null, Utils.convertStringToDateOrDefault("20-02-2020 00:00:00"), Utils.convertStringToDateOrDefault("30-01-2020 00:00:00"), Utils.convertStringToDateOrDefault("30-01-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        dispensationList.add(new Dispensation(4L, 1L, 1L, Utils.convertStringToDateOrDefault("01-04-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""),   31L, "", "Intensive", null, Utils.convertStringToDateOrDefault("28-02-2020 00:00:00"), Utils.convertStringToDateOrDefault("18-02-2020 00:00:00"), Utils.convertStringToDateOrDefault("18-02-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        dispensationList.add(new Dispensation(5L, 1L, 1L, Utils.convertStringToDateOrDefault("01-05-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""),   31L, "", "Intensive", null, Utils.convertStringToDateOrDefault("21-03-2020 00:00:00"), Utils.convertStringToDateOrDefault("26-02-2020 00:00:00"), Utils.convertStringToDateOrDefault("26-02-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        dispensationList.add(new Dispensation(6L, 1L, 1L, Utils.convertStringToDateOrDefault("01-05-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""),   31L, "", "Intensive", null, Utils.convertStringToDateOrDefault("21-04-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-03-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-03-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        dispensationList.add(new Dispensation(7L, 1L, 1L, Utils.convertStringToDateOrDefault("01-06-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""),   31L, "", "Intensive", null, Utils.convertStringToDateOrDefault("03-06-2020 00:00:00"), Utils.convertStringToDateOrDefault("18-03-2020 00:00:00"), Utils.convertStringToDateOrDefault("18-03-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        dispensationList.add(new Dispensation(8L, 1L, 1L, Utils.convertStringToDateOrDefault("01-07-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""),   31L, "", "Intensive", null, Utils.convertStringToDateOrDefault("30-06-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-06-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-06-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        Long totalDays = dispensationService.getTotalDaysDrugsDispensedForEntity(1L, dispensationList).get();

        assertEquals(0, totalDays);
    }

    @Test
    public void test_get_total_days_drug_dispensed_for_entity_with_one_dispensation () throws ExecutionException, InterruptedException {
        List<Dispensation> dispensationList = new ArrayList<>();
        dispensationList.add(new Dispensation(1L, 1L, 1L, Utils.convertStringToDateOrDefault("01-01-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""),   31L, "", "Intensive", 31L, Utils.convertStringToDateOrDefault("31-01-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-01-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-01-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        Long totalDays = dispensationService.getTotalDaysDrugsDispensedForEntity(1L, dispensationList).get();

        assertEquals(31, totalDays);
    }

    @Test
    public void test_get_refill_date_for_entity_scenario_1 () {
        List<Dispensation> dispensationList = new ArrayList<>();
        dispensationList.add(new Dispensation(1L, 1L, 1L, Utils.convertStringToDateOrDefault("01-01-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""),   31L, "", "Intensive", null, null, Utils.convertStringToDateOrDefault("01-01-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-01-2020 00:00:00"),1L, false, "", DispensationStatus.ISSUED));
        dispensationList.add(new Dispensation(3L, 1L, 1L, Utils.convertStringToDateOrDefault("01-03-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""),   31L, "", "Intensive", null, null, Utils.convertStringToDateOrDefault("30-01-2020 00:00:00"), Utils.convertStringToDateOrDefault("30-01-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        dispensationList.add(new Dispensation(4L, 1L, 1L, Utils.convertStringToDateOrDefault("01-04-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""),   31L, "", "Intensive", null, null, Utils.convertStringToDateOrDefault("18-02-2020 00:00:00"), Utils.convertStringToDateOrDefault("18-02-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        dispensationList.add(new Dispensation(5L, 1L, 1L, Utils.convertStringToDateOrDefault("01-05-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""),   31L, "", "Intensive", null, null, Utils.convertStringToDateOrDefault("26-02-2020 00:00:00"), Utils.convertStringToDateOrDefault("26-02-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        dispensationList.add(new Dispensation(6L, 1L, 1L, Utils.convertStringToDateOrDefault("01-05-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""),   31L, "", "Intensive", null, null, Utils.convertStringToDateOrDefault("01-03-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-03-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        dispensationList.add(new Dispensation(7L, 1L, 1L, Utils.convertStringToDateOrDefault("01-06-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""),   31L, "", "Intensive", null, null, Utils.convertStringToDateOrDefault("18-03-2020 00:00:00"), Utils.convertStringToDateOrDefault("18-03-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        dispensationList.add(new Dispensation(8L, 1L, 1L, Utils.convertStringToDateOrDefault("01-07-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""),   31L, "", "Intensive", null, null, Utils.convertStringToDateOrDefault("01-06-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-06-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        Date refillDate = dispensationService.getRefillDateForEntity(1L, dispensationList);

        assertNull(refillDate);
    }

    @Test
    public void test_get_refill_date_for_entity_no_overlapping_dispensations() {
        List<Dispensation> dispensationList = new ArrayList<>();
        dispensationList.add(new Dispensation(1L, 1L, 1L, Utils.convertStringToDateOrDefault("01-01-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""), 31L, "", "Intensive", 31L, Utils.convertStringToDateOrDefault("31-01-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-01-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-01-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        dispensationList.add(new Dispensation(2L, 1L, 1L, Utils.convertStringToDateOrDefault("01-02-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""), 31L, "", "Intensive", 29L, Utils.convertStringToDateOrDefault("29-02-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-02-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-02-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        dispensationList.add(new Dispensation(3L, 1L, 1L, Utils.convertStringToDateOrDefault("01-03-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""), 31L, "", "Intensive", 31L, Utils.convertStringToDateOrDefault("31-03-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-03-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-03-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        dispensationList.add(new Dispensation(4L, 1L, 1L, Utils.convertStringToDateOrDefault("01-04-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""), 31L, "", "Intensive", 30L, Utils.convertStringToDateOrDefault("30-04-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-04-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-04-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        dispensationList.add(new Dispensation(5L, 1L, 1L, Utils.convertStringToDateOrDefault("01-05-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""), 31L, "", "Intensive", 31L, Utils.convertStringToDateOrDefault("31-05-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-05-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-05-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        dispensationList.add(new Dispensation(6L, 1L, 1L, Utils.convertStringToDateOrDefault("01-06-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""), 31L, "", "Intensive", 30L, Utils.convertStringToDateOrDefault("30-06-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-06-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-06-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        dispensationList.add(new Dispensation(7L, 1L, 1L, Utils.convertStringToDateOrDefault("01-07-2020 00:00:00"), 30L, Utils.convertStringToDateOrDefault(""), 31L, "", "Intensive", 31L, Utils.convertStringToDateOrDefault("31-07-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-07-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-07-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED));
        Date refillDate = dispensationService.getRefillDateForEntity(1L, dispensationList);

        assertEquals(Utils.convertStringToDateOrDefault("31-07-2020 00:00:00"), refillDate);
    }

    @Test
    public void test_filter_list_multiple_cases() throws ParseException {
        List<EntityDispensationResponse> entityDispensationResponseList = new ArrayList<>();
        List<RangeValueFilter> filters = new ArrayList<>();
        List<EntityDispensationResponse> filteredList;
        HashSet<Long> expectedIds;

        entityDispensationResponseList.add(new EntityDispensationResponse( 1L, Utils.convertStringToDate("20-01-2020 00:00:00"), 1L, Collections.emptyList()));
        entityDispensationResponseList.add(new EntityDispensationResponse( 2L, Utils.convertStringToDate("21-01-2020 00:00:00"), 1L, Collections.emptyList()));
        entityDispensationResponseList.add(new EntityDispensationResponse( 3L, Utils.convertStringToDate("22-01-2020 00:00:00"), 1L, Collections.emptyList()));
        entityDispensationResponseList.add(new EntityDispensationResponse( 4L, Utils.convertStringToDate("23-01-2020 00:00:00"), 1L, Collections.emptyList()));
        entityDispensationResponseList.add(new EntityDispensationResponse( 5L, Utils.convertStringToDate("24-01-2020 00:00:00"), 1L, Collections.emptyList()));
        entityDispensationResponseList.add(new EntityDispensationResponse( 6L, Utils.convertStringToDate("23-01-2020 00:00:00"), 1L, Collections.emptyList()));
        entityDispensationResponseList.add(new EntityDispensationResponse( 7L, Utils.convertStringToDate("22-01-2020 00:00:00"), 1L, Collections.emptyList()));
        entityDispensationResponseList.add(new EntityDispensationResponse( 8L, Utils.convertStringToDate("21-01-2020 00:00:00"), 1L, Collections.emptyList()));
        entityDispensationResponseList.add(new EntityDispensationResponse( 9L, Utils.convertStringToDate("24-01-2020 00:00:00"), 1L, Collections.emptyList()));
        entityDispensationResponseList.add(new EntityDispensationResponse(10L, Utils.convertStringToDate("22-01-2020 00:00:00"), 1L, Collections.emptyList()));
        entityDispensationResponseList.add(new EntityDispensationResponse(11L, Utils.convertStringToDate("23-01-2020 00:00:00"), 1L, Collections.emptyList()));
        entityDispensationResponseList.add(new EntityDispensationResponse(12L, Utils.convertStringToDate("25-01-2020 00:00:00"), 1L, Collections.emptyList()));
        entityDispensationResponseList.add(new EntityDispensationResponse(13L, Utils.convertStringToDate("22-01-2020 00:00:00"), 1L, Collections.emptyList()));
        entityDispensationResponseList.add(new EntityDispensationResponse(14L, Utils.convertStringToDateOrDefault(""), 1L, Arrays.asList(new DispensationData(new Dispensation(1L, 1L, 1L, 30L, "weghtBand", "phase", 30L, 1L, "", DispensationStatus.ISSUED), Collections.emptyList()))));
        entityDispensationResponseList.add(new EntityDispensationResponse(15L, Utils.convertStringToDateOrDefault(""), 1L, Arrays.asList(new DispensationData(new Dispensation(1L, 1L, 1L, 30L, "weghtBand", "phase", 30L, 1L, "", DispensationStatus.ISSUED), Collections.emptyList()))));
        entityDispensationResponseList.add(new EntityDispensationResponse(16L, Utils.convertStringToDateOrDefault(""), 1L, Arrays.asList(new DispensationData(new Dispensation(1L, 1L, 1L, 30L, "weghtBand", "phase", 30L, 1L, "", DispensationStatus.ISSUED), Collections.emptyList()))));

        filters.add(new RangeValueFilter(RangeFilterType.NEXT_REFILL_DATE, "22-01-2020 00:00:00", "24-01-2020 00:00:00"));
        filteredList = dispensationService.filterList(entityDispensationResponseList, filters);
        expectedIds = new HashSet<>(Arrays.asList(3L, 4L, 5L, 6L, 7L,9L, 10L, 11L, 13L));
        assertEquals(expectedIds.size(), filteredList.size());
        for (EntityDispensationResponse l : filteredList) {
            assertTrue(expectedIds.contains(l.getEntityId()));
        }

        filters = new ArrayList<>();
        filters.add(new RangeValueFilter(RangeFilterType.NEXT_REFILL_DATE, "", "23-01-2020 00:00:00"));
        filteredList = dispensationService.filterList(entityDispensationResponseList, filters);
        expectedIds = new HashSet<>(Arrays.asList(1L, 2L, 3L, 4L, 6L, 7L, 8L, 10L, 11L, 13L));
        assertEquals(expectedIds.size(), filteredList.size());
        for (EntityDispensationResponse l : filteredList) {
            assertTrue(expectedIds.contains(l.getEntityId()));
        }

        filters = new ArrayList<>();
        filters.add(new RangeValueFilter(RangeFilterType.NEXT_REFILL_DATE, "23-01-2020 00:00:00", ""));
        filteredList = dispensationService.filterList(entityDispensationResponseList, filters);
        expectedIds = new HashSet<>(Arrays.asList(4L,5L,6L,9L,11L,12L));
        assertEquals(expectedIds.size(), filteredList.size());
        for (EntityDispensationResponse l : filteredList) {
            assertTrue(expectedIds.contains(l.getEntityId()));
        }

        filters = new ArrayList<>();
        filters.add(new RangeValueFilter(RangeFilterType.NEXT_REFILL_DATE, "22-01-2020 00:00:00", "22-01-2020 00:00:00"));
        filters.add(new RangeValueFilter(RangeFilterType.NEXT_REFILL_DATE, "23-01-2020 00:00:00", "23-01-2020 00:00:00"));
        filters.add(new RangeValueFilter(RangeFilterType.NEXT_REFILL_DATE, "24-01-2020 00:00:00", "24-01-2020 00:00:00"));
        filteredList = dispensationService.filterList(entityDispensationResponseList, filters);
        expectedIds = new HashSet<>(Arrays.asList(3L, 4L, 5L, 6L, 7L,9L, 10L, 11L, 13L));
        assertEquals(expectedIds.size(), filteredList.size());
        for (EntityDispensationResponse l : filteredList) {
            assertTrue(expectedIds.contains(l.getEntityId()));
        }

        filters = new ArrayList<>();
        filters.add(new RangeValueFilter(RangeFilterType.NEXT_REFILL_DATE, "26-01-2020 00:00:00", ""));
        filteredList = dispensationService.filterList(entityDispensationResponseList, filters);
        expectedIds = new HashSet<>();
        assertEquals(expectedIds.size(), filteredList.size());
        for (EntityDispensationResponse l : filteredList) {
            assertTrue(expectedIds.contains(l.getEntityId()));
        }

        filters = new ArrayList<>();
        filters.add(new RangeValueFilter(RangeFilterType.NUMBER_OF_DISPENSATIONS, 0L));
        filteredList = dispensationService.filterList(entityDispensationResponseList, filters);
        expectedIds = new HashSet<>(Arrays.asList(1L,2L,3L,4L,5L,6L,7L,8L,9L, 10L, 11L, 12L, 13L));
        assertEquals(expectedIds.size(), filteredList.size());
        for (EntityDispensationResponse l : filteredList) {
            assertTrue(expectedIds.contains(l.getEntityId()));
        }

        filters = new ArrayList<>();
        filters.add(new RangeValueFilter(RangeFilterType.NUMBER_OF_DISPENSATIONS, 1L));
        filteredList = dispensationService.filterList(entityDispensationResponseList, filters);
        expectedIds = new HashSet<>(Arrays.asList(14L,15L,16L));
        assertEquals(expectedIds.size(), filteredList.size());
        for (EntityDispensationResponse l : filteredList) {
            assertTrue(expectedIds.contains(l.getEntityId()));
        }

        filters = new ArrayList<>();
        filters.add(new RangeValueFilter(RangeFilterType.NEXT_REFILL_DATE, "23-01-2020 00:00:00", ""));
        filters.add(new RangeValueFilter(RangeFilterType.NUMBER_OF_DISPENSATIONS, 1L));
        filteredList = dispensationService.filterList(entityDispensationResponseList, filters);
        expectedIds = new HashSet<>(Arrays.asList(4L,5L,6L,9L,11L,12L,14L,15L,16L));
        assertEquals(expectedIds.size(), filteredList.size());
        for (EntityDispensationResponse l : filteredList) {
            assertTrue(expectedIds.contains(l.getEntityId()));
        }

        filters = new ArrayList<>();
        filters.add(new RangeValueFilter(RangeFilterType.NEXT_REFILL_DATE, "23-01-2020 00:00:00", ""));
        filters.add(new RangeValueFilter(RangeFilterType.NUMBER_OF_DISPENSATIONS, 0L));
        filteredList = dispensationService.filterList(entityDispensationResponseList, filters);
        expectedIds = new HashSet<>(Arrays.asList(1L,2L,3L,4L,5L,6L,7L,8L,9L,10L,11L,12L,13L));
        assertEquals(expectedIds.size(), filteredList.size());
        for (EntityDispensationResponse l : filteredList) {
            assertTrue(expectedIds.contains(l.getEntityId()));
        }

        filters = new ArrayList<>();
        filters.add(new RangeValueFilter(RangeFilterType.NEXT_REFILL_DATE, "20-01-2020 00:00:00", ""));
        filters.add(new RangeValueFilter(RangeFilterType.NUMBER_OF_DISPENSATIONS, 1L));
        filteredList = dispensationService.filterList(entityDispensationResponseList, filters);
        expectedIds = new HashSet<>(Arrays.asList(1L,2L,3L,4L,5L,6L,7L,8L,9L, 10L, 11L, 12L, 13L,14L,15L,16L));
        assertEquals(expectedIds.size(), filteredList.size());
        for (EntityDispensationResponse l : filteredList) {
            assertTrue(expectedIds.contains(l.getEntityId()));
        }

        filters = new ArrayList<>();
        filters.add(new RangeValueFilter(RangeFilterType.NEXT_REFILL_DATE, "20-01-2020 00:00:00", ""));
        filters.add(new RangeValueFilter(RangeFilterType.NUMBER_OF_DISPENSATIONS, 0L));
        filteredList = dispensationService.filterList(new ArrayList<>(), filters);
        expectedIds = new HashSet<>(Collections.emptyList());
        assertEquals(expectedIds.size(), filteredList.size());
        for (EntityDispensationResponse l : filteredList) {
            assertTrue(expectedIds.contains(l.getEntityId()));
        }

    }

    @Test
    public void test_create_return_dispensation() {
        List<ReturnDispensationRequest.ReturnDispensationData> returnDispensationRequest = new ArrayList<>();
        returnDispensationRequest.add(new ReturnDispensationRequest.ReturnDispensationData(1L, 2L, "Died", 1L));
        ReturnDispensationRequest request = new ReturnDispensationRequest(1L, returnDispensationRequest);

        ProductInventoryLog dispensationLog = new ProductInventoryLog(TransactionType.RETURNED, 1L, 2L, CommentType.REASON_FOR_RETURN, "Died", null, 1L,null, TransactionIdType.INTERNAL);
        List<ProductInventoryLog> expectedProductInventoryLogs = new ArrayList<>();
        expectedProductInventoryLogs.add(dispensationLog);
        ProductDispensationMapping map = new ProductDispensationMapping(1L, 1L, 1L, 1L, 10L, "test");
        List<ProductDispensationMapping> dispensationMappings = new ArrayList<>();
        dispensationMappings.add(map);
        Dispensation dispensation = new Dispensation(1L, 1L, 2L, 4L, "20 pills", "test", 20L, 12L, "Dispensed", DispensationStatus.RETURNED);

        when(productDispensationMapRepository.findAllByDispensationIdIn(anyCollection())).thenReturn(dispensationMappings);
        when(dispensationRepository.findByIdAndClientIdAndDeletedIsFalse(any(), any())).thenReturn(Optional.of(dispensation));
        when(productInventoryLogRepository.findAllByTransactionIdIn(any())).thenReturn(new ArrayList<>());
        List<ProductInventoryLog> actualReturnedDispensation = dispensationService.createReturnDispensation(request, 1L, true);

        assertEquals(actualReturnedDispensation.size(), expectedProductInventoryLogs.size());
        assertEquals(actualReturnedDispensation.get(0).getTransactionQuantity(), expectedProductInventoryLogs.get(0).getTransactionQuantity());
    }

    @Test
    public void test_create_return_dispensation_without_inventory_log() {
        List<ReturnDispensationRequest.ReturnDispensationData> returnDispensationRequest = new ArrayList<>();
        returnDispensationRequest.add(new ReturnDispensationRequest.ReturnDispensationData(1L, 2L, "Died", 1L));
        ReturnDispensationRequest request = new ReturnDispensationRequest(1L, returnDispensationRequest);
        ProductDispensationMapping map = new ProductDispensationMapping(1L, 1L, 1L, 1L, 10L, "test");
        List<ProductDispensationMapping> dispensationMappings = new ArrayList<>();
        dispensationMappings.add(map);
        Dispensation dispensation = new Dispensation(1L, 1L, 2L, 4L, "20 pills", "test", 20L, 12L, "Dispensed", DispensationStatus.RETURNED);
        when(productDispensationMapRepository.findAllByDispensationIdIn(anyCollection())).thenReturn(dispensationMappings);
        when(dispensationRepository.findByIdAndClientIdAndDeletedIsFalse(any(), any())).thenReturn(Optional.of(dispensation));
        when(productInventoryLogRepository.findAllByTransactionIdIn(any())).thenReturn(new ArrayList<>());
        List<ProductInventoryLog> actualReturnedDispensation = dispensationService.createReturnDispensation(request, 1L, false);
        assertEquals(actualReturnedDispensation.size(), 0);
        verify(productDispensationMapRepository, Mockito.times(1)).findAllByDispensationIdIn(any());
        verify(dispensationRepository, Mockito.times(1)).findByIdAndClientIdAndDeletedIsFalse(any(), any());
    }

    @Test
    public void test_create_return_dispensation_with_inventory_data () {
        List<ReturnDispensationRequest.ReturnDispensationData> returnDispensationRequest = new ArrayList<>();
        returnDispensationRequest.add(new ReturnDispensationRequest.ReturnDispensationData(1L, 10L, "Died", 1L, "ABC123", "20-11-2025 00:00:00", 1L));
        ReturnDispensationRequest request = new ReturnDispensationRequest(1L, returnDispensationRequest);

        ProductInventoryLog productInventoryLog = new ProductInventoryLog(TransactionType.RETURNED, 1L, 2L, CommentType.REASON_FOR_RETURN, "Died", null, 1L,null, TransactionIdType.INTERNAL);
        List<ProductInventoryLog> existingProductInventoryLogList = new ArrayList<>();
        existingProductInventoryLogList.add(new ProductInventoryLog(TransactionType.ISSUED, 1L, 20L, CommentType.REASON_FOR_ADD, "NA", 1L, 1L,1L, TransactionIdType.INTERNAL));
        existingProductInventoryLogList.add(new ProductInventoryLog(TransactionType.RETURNED, 1L, 10L, CommentType.REASON_FOR_RETURN, "NA", 1L, 1L,1L, TransactionIdType.INTERNAL));

        ProductDispensationMapping map = new ProductDispensationMapping(1L, 1L, 1L, 1L, 10L, "test");
        List<ProductDispensationMapping> dispensationMappings = new ArrayList<>();
        dispensationMappings.add(map);
        Dispensation dispensation = new Dispensation(1L, 1L, 2L, 4L, "20 pills", "test", 20L, 12L, "Dispensed", DispensationStatus.RETURNED);

        when(productDispensationMapRepository.findAllByDispensationIdIn(anyCollection())).thenReturn(dispensationMappings);
        when(dispensationRepository.findByIdAndClientIdAndDeletedIsFalse(any(), any())).thenReturn(Optional.of(dispensation));
        when(productDispensationMapRepository.findByIdIn(any())).thenReturn(dispensationMappings);
        when(productInventoryLogRepository.findAllByTransactionIdIn(any())).thenReturn(existingProductInventoryLogList);
        when(stockService.setStockData(any(), any(), any())).thenReturn(productInventoryLog);

        List<Long> actualProductInventoryLogIds = dispensationService.createReturnDispensationWithInventoryData(request, 1L);

        assertEquals(actualProductInventoryLogIds.size(), request.getReturnDispensationProductDetails().size());
        verify(productDispensationMapRepository, Mockito.times(1)).findAllByDispensationIdIn(any());
        verify(dispensationRepository, Mockito.times(2)).findByIdAndClientIdAndDeletedIsFalse(any(), any());
        verify(productDispensationMapRepository, Mockito.times(1)).findByIdIn(any());
        verify(stockService, Mockito.times(1)).setStockData(any(), any(), any());
    }

    @Test
    public void test_create_return_dispensation_with_inventory_data_with_quantity_more_than_allowed() {
        List<ReturnDispensationRequest.ReturnDispensationData> returnDispensationRequest = new ArrayList<>();
        returnDispensationRequest.add(new ReturnDispensationRequest.ReturnDispensationData(1L, 100L, "Died", 1L, "ABC123", "20-11-2025 00:00:00", 1L));
        ReturnDispensationRequest request = new ReturnDispensationRequest(1L, returnDispensationRequest);

        List<ProductInventoryLog> existingProductInventoryLogList = new ArrayList<>();
        existingProductInventoryLogList.add(new ProductInventoryLog(TransactionType.ISSUED, 1L, 10L, CommentType.REASON_FOR_ADD, "NA", 1L, 1L,1L, TransactionIdType.INTERNAL));

        ProductDispensationMapping map = new ProductDispensationMapping(1L, 1L, 1L, 1L, 10L, "test");
        List<ProductDispensationMapping> dispensationMappings = new ArrayList<>();
        dispensationMappings.add(map);
        Dispensation dispensation = new Dispensation(1L, 1L, 2L, 4L, "20 pills", "test", 20L, 12L, "Dispensed", DispensationStatus.RETURNED);

        when(productDispensationMapRepository.findAllByDispensationIdIn(anyCollection())).thenReturn(dispensationMappings);
        when(dispensationRepository.findByIdAndClientIdAndDeletedIsFalse(any(), any())).thenReturn(Optional.of(dispensation));
        when(productInventoryLogRepository.findAllByTransactionIdIn(any())).thenReturn(existingProductInventoryLogList);

        Exception ex = assertThrows(ValidationException.class, () -> dispensationService.createReturnDispensationWithInventoryData(request, 1L));
        assertEquals("Invalid number of units", ex.getMessage());
    }


    @Test
    public void test_create_return_dispensation_with_combined_inventory_and_without_inventory_data() {
        List<ReturnDispensationRequest.ReturnDispensationData> returnDispensationRequest = new ArrayList<>();
        returnDispensationRequest.add(new ReturnDispensationRequest.ReturnDispensationData(1L, 10L, "Died", 1L, "ABC123", "20-11-2025 00:00:00", 1L));
        returnDispensationRequest.add(new ReturnDispensationRequest.ReturnDispensationData(2L, 5L, "Died", 1L, null, null, null));
        ReturnDispensationRequest request = new ReturnDispensationRequest(1L, returnDispensationRequest);

        Dispensation dispensation = new Dispensation(1L, 1L, 2L, 4L, "20 pills", "test", 20L, 12L, "Dispensed", DispensationStatus.RETURNED);
        List<ProductDispensationMapping> dispensationMappings = new ArrayList<>();
        dispensationMappings.add(new ProductDispensationMapping(1L, 1L, 1L, 1L, 20L, "test"));
        dispensationMappings.add(new ProductDispensationMapping(2L, 1L, 1L, 1L, 10L, "test"));

        List<ProductInventoryLog> existingProductInventoryLogList = new ArrayList<>();
        existingProductInventoryLogList.add(new ProductInventoryLog(TransactionType.ISSUED, 1L, 20L, CommentType.REASON_FOR_ADD, "NA", 1L, 1L,1L, TransactionIdType.INTERNAL));
        existingProductInventoryLogList.add(new ProductInventoryLog(TransactionType.ISSUED, 2L, 10L, CommentType.REASON_FOR_ADD, "NA", null, 1L,1L, TransactionIdType.INTERNAL));

        when(dispensationRepository.findByIdAndClientIdAndDeletedIsFalse(any(), any())).thenReturn(Optional.of(dispensation));
        when(productDispensationMapRepository.findAllByDispensationIdIn(anyCollection())).thenReturn(dispensationMappings);
        when(productInventoryLogRepository.findAllByTransactionIdIn(any())).thenReturn(existingProductInventoryLogList);

        List<ProductInventoryLog> returnedProductInventoryLog = dispensationService.createReturnDispensation(request, 1L, true);
        assertEquals(request.getReturnDispensationProductDetails().size(), returnedProductInventoryLog.size());
        assertEquals(15L, returnedProductInventoryLog.get(0).getTransactionQuantity()+returnedProductInventoryLog.get(1).getTransactionQuantity());

        verify(productDispensationMapRepository, Mockito.times(1)).findAllByDispensationIdIn(any());
        verify(dispensationRepository, Mockito.times(1)).findByIdAndClientIdAndDeletedIsFalse(any(), any());
        verify(productDispensationMapRepository, Mockito.times(1)).findAllByDispensationIdIn(any());
    }


    @Test
    public void test_create_return_dispensation_with_same_product_different_batches(){
        List<ReturnDispensationRequest.ReturnDispensationData> returnDispensationRequest = new ArrayList<>();
        returnDispensationRequest.add(new ReturnDispensationRequest.ReturnDispensationData(1L, 5L, "Died", 1L, "ABC123", "20-11-2025 00:00:00", 1L));
        returnDispensationRequest.add(new ReturnDispensationRequest.ReturnDispensationData(1L, 5L, "Died", 1L, "ABC456", "10-11-2025 00:00:00", 2L));
        ReturnDispensationRequest request = new ReturnDispensationRequest(1L, returnDispensationRequest);

        Dispensation dispensation = new Dispensation(1L, 1L, 2L, 4L, "20 pills", "test", 20L, 12L, "Dispensed", DispensationStatus.RETURNED);
        List<ProductDispensationMapping> dispensationMappings = new ArrayList<>();
        dispensationMappings.add(new ProductDispensationMapping(1L, 1L, 1L, 1L, 20L, "test"));

        List<ProductInventoryLog> existingProductInventoryLogList = new ArrayList<>();
        existingProductInventoryLogList.add(new ProductInventoryLog(TransactionType.ISSUED, 1L, 10L, CommentType.REASON_FOR_ADD, "NA", 1L, 1L,1L, TransactionIdType.INTERNAL));
        existingProductInventoryLogList.add(new ProductInventoryLog(TransactionType.ISSUED, 1L, 10L, CommentType.REASON_FOR_ADD, "NA", 2L, 1L,1L, TransactionIdType.INTERNAL));

        when(dispensationRepository.findByIdAndClientIdAndDeletedIsFalse(any(), any())).thenReturn(Optional.of(dispensation));
        when(productDispensationMapRepository.findAllByDispensationIdIn(anyCollection())).thenReturn(dispensationMappings);
        when(productInventoryLogRepository.findAllByTransactionIdIn(any())).thenReturn(existingProductInventoryLogList);

        List<ProductInventoryLog> returnedProductInventoryLog = dispensationService.createReturnDispensation(request, 1L, true);
        assertEquals(request.getReturnDispensationProductDetails().size(), returnedProductInventoryLog.size());
        assertEquals(10L, returnedProductInventoryLog.get(0).getTransactionQuantity()+returnedProductInventoryLog.get(1).getTransactionQuantity());

        verify(productDispensationMapRepository, Mockito.times(1)).findAllByDispensationIdIn(any());
        verify(dispensationRepository, Mockito.times(1)).findByIdAndClientIdAndDeletedIsFalse(any(), any());
        verify(productDispensationMapRepository, Mockito.times(1)).findAllByDispensationIdIn(any());
    }

    @Test
    public void test_create_return_dispensation_with_same_product_different_batches_exceeding_max_limit() {
        List<ReturnDispensationRequest.ReturnDispensationData> returnDispensationRequest = new ArrayList<>();
        returnDispensationRequest.add(new ReturnDispensationRequest.ReturnDispensationData(1L, 5L, "Died", 1L, "ABC123", "20-11-2025 00:00:00", 1L));
        returnDispensationRequest.add(new ReturnDispensationRequest.ReturnDispensationData(1L, 11L, "Died", 1L, "ABC456", "10-11-2025 00:00:00", 2L));
        ReturnDispensationRequest request = new ReturnDispensationRequest(1L, returnDispensationRequest);

        Dispensation dispensation = new Dispensation(1L, 1L, 2L, 4L, "20 pills", "test", 20L, 12L, "Dispensed", DispensationStatus.RETURNED);
        List<ProductDispensationMapping> dispensationMappings = new ArrayList<>();
        dispensationMappings.add(new ProductDispensationMapping(1L, 1L, 1L, 1L, 20L, "test"));

        List<ProductInventoryLog> existingProductInventoryLogList = new ArrayList<>();
        existingProductInventoryLogList.add(new ProductInventoryLog(TransactionType.ISSUED, 1L, 10L, CommentType.REASON_FOR_ADD, "NA", 1L, 1L,1L, TransactionIdType.INTERNAL));
        existingProductInventoryLogList.add(new ProductInventoryLog(TransactionType.ISSUED, 1L, 10L, CommentType.REASON_FOR_ADD, "NA", 2L, 1L,1L, TransactionIdType.INTERNAL));

        when(dispensationRepository.findByIdAndClientIdAndDeletedIsFalse(any(), any())).thenReturn(Optional.of(dispensation));
        when(productDispensationMapRepository.findAllByDispensationIdIn(anyCollection())).thenReturn(dispensationMappings);
        when(productInventoryLogRepository.findAllByTransactionIdIn(any())).thenReturn(existingProductInventoryLogList);

        Exception ex = assertThrows(ValidationException.class, () -> dispensationService.createReturnDispensation(request, 1L, true));
        assertEquals("Invalid number of units", ex.getMessage());
    }

    @Test
    public void test_set_return_dispensation_stock_data() {
        List<ReturnDispensationRequest.ReturnDispensationData> returnDispensationRequest = new ArrayList<>();
        returnDispensationRequest.add(new ReturnDispensationRequest.ReturnDispensationData(1L, 2L, "Died", 1L, "ABC123", "20-11-2025 00:00:00", 1L));
        ReturnDispensationRequest request = new ReturnDispensationRequest(1L, returnDispensationRequest);
        Dispensation dispensation = new Dispensation(1L, 1L, 2L, 4L, "20 pills", "test", 20L, 1L, "Dispensed", DispensationStatus.RETURNED);
        ProductDispensationMapping map = new ProductDispensationMapping(1L, 1L, 1L, 1L, 10L, "test");
        List<ProductDispensationMapping> dispensationMappings = new ArrayList<>();
        dispensationMappings.add(map);
        ProductInventoryLog productInventoryLog = new ProductInventoryLog(TransactionType.RETURNED, 1L, 2L, CommentType.REASON_FOR_RETURN, "Died", 1L, 1L,1L, TransactionIdType.INTERNAL);
        List<ProductInventoryLog> expectedProductInventoryLogs = new ArrayList<>();
        expectedProductInventoryLogs.add(productInventoryLog);
        when(dispensationRepository.findByIdAndClientIdAndDeletedIsFalse(any(), any())).thenReturn(Optional.of(dispensation));
        when(productDispensationMapRepository.findByIdIn(any())).thenReturn(dispensationMappings);
        when(stockService.setStockData(any(), any(), any())).thenReturn(productInventoryLog);
        List<ProductInventoryLog> actualReturnedDispensation = dispensationService.setReturnDispensationStockData(request, 1L);
        assertEquals(actualReturnedDispensation.size(), expectedProductInventoryLogs.size());
        verify(dispensationRepository, Mockito.times(1)).findByIdAndClientIdAndDeletedIsFalse(any(), any());
        verify(productDispensationMapRepository, Mockito.times(1)).findByIdIn(any());
        verify(stockService, Mockito.times(1)).setStockData(any(), any(), any());
        verify(productInventoryLogRepository, Mockito.times(0)).saveAll(any());
    }

    @Test
    public void test_set_return_dispensation_stock_data_without_inventory() {
        List<ReturnDispensationRequest.ReturnDispensationData> returnDispensationRequest = new ArrayList<>();
        returnDispensationRequest.add(new ReturnDispensationRequest.ReturnDispensationData(1L, 2L, "Died", 1L, null, null, 1L));
        ReturnDispensationRequest request = new ReturnDispensationRequest(1L, returnDispensationRequest);
        Dispensation dispensation = new Dispensation(1L, 1L, 2L, 4L, "20 pills", "test", 20L, 1L, "Dispensed", DispensationStatus.RETURNED);
        ProductDispensationMapping map = new ProductDispensationMapping(1L, 1L, 1L, 1L, 10L, "test");
        List<ProductDispensationMapping> dispensationMappings = new ArrayList<>();
        dispensationMappings.add(map);
        ProductInventoryLog productInventoryLog = new ProductInventoryLog(TransactionType.RETURNED, 1L, 2L, CommentType.REASON_FOR_RETURN, "Died", null, 1L,1L, TransactionIdType.INTERNAL);
        List<ProductInventoryLog> expectedProductInventoryLogs = new ArrayList<>();
        expectedProductInventoryLogs.add(productInventoryLog);
        when(dispensationRepository.findByIdAndClientIdAndDeletedIsFalse(any(), any())).thenReturn(Optional.of(dispensation));
        when(productDispensationMapRepository.findByIdIn(any())).thenReturn(dispensationMappings);
        when(stockService.setStockData(any(), any(), any())).thenReturn(productInventoryLog);
        when(stockService.setInventoryLogData(any(), any(), any())).thenReturn(productInventoryLog);
        List<ProductInventoryLog> actualReturnedDispensation = dispensationService.setReturnDispensationStockData(request, 1L);
        assertEquals(actualReturnedDispensation.size(), expectedProductInventoryLogs.size());
        verify(dispensationRepository, Mockito.times(1)).findByIdAndClientIdAndDeletedIsFalse(any(), any());
        verify(productDispensationMapRepository, Mockito.times(1)).findByIdIn(any());
        verify(stockService, Mockito.times(0)).setStockData(any(), any(), any());
        verify(stockService, Mockito.times(1)).setInventoryLogData(any(), any(), any());
    }

    @Test
    public void test_validate_return_dispensation_data() {
        ReturnDispensationRequest requestWithNullId = new ReturnDispensationRequest(null, new ArrayList<ReturnDispensationRequest.ReturnDispensationData>());
        Exception exception = assertThrows(ValidationException.class, () -> requestWithNullId.validate());

        assertEquals("dispensation id is required", exception.getMessage());

        ReturnDispensationRequest requestWithEmptyProductData = new ReturnDispensationRequest(1L, new ArrayList<ReturnDispensationRequest.ReturnDispensationData>());
        Exception emptyProductDataException = assertThrows(ValidationException.class, () -> requestWithEmptyProductData.validate());

        assertEquals("return dispensation data is required", emptyProductDataException.getMessage());

        ReturnDispensationRequest requestWithNullProductData = new ReturnDispensationRequest(1L, null);
        Exception nullProductDataException = assertThrows(ValidationException.class, () -> requestWithNullProductData.validate());

        assertEquals("return dispensation data is required", nullProductDataException.getMessage());
    }

    @Test
    public void test_get_dispensation_for_entity() throws ParseException {
        List<Dispensation> dispensationList = new ArrayList<>();
        dispensationList.add(new Dispensation(1L,1L, 1L, 23L, 33L, "30-40Kg", "Intensive", 1L, 0L, Utils.convertStringToDate("10-07-2020 00:00:00"), Utils.convertStringToDate("10-07-2020 00:00:00"), Utils.convertStringToDate("13-07-2020 00:00:00"), "notes", DispensationStatus.ISSUED));
        when(dispensationRepository.findAllByEntityIdAndClientIdAndDeletedIsFalse(any(), any())).thenReturn(dispensationList);
        List<Dispensation> actualDispensationList = dispensationService.getDispensationsForEntity(1L, 1L);
        assertEquals(1L, actualDispensationList.size());
        assertEquals(1L, actualDispensationList.get(0).getEntityId());
        assertEquals(1L, actualDispensationList.get(0).getClientId());
        verify(dispensationRepository, Mockito.times(1)).findAllByEntityIdAndClientIdAndDeletedIsFalse(any(), any());
    }

    @Test
    public void get_product_dto_for_dispensations() {
        List<ProductDispensationMapping> productDispensationMappings = new ArrayList<>();
        productDispensationMappings.add(new ProductDispensationMapping(1L, 1L, 1L, 1L, 10L, "test"));
        when(productDispensationMapRepository.findAllByDispensationIdIn(any())).thenReturn(productDispensationMappings);
        List<DispensationProductDto> dispensationProductDtoList = dispensationService.getProductDtoForDispensations(Collections.singletonList(1L));
        assertEquals(productDispensationMappings.size(), dispensationProductDtoList.size());
        assertEquals(productDispensationMappings.get(0).getDispensationId(), dispensationProductDtoList.get(0).getDispensationId());
        verify(productDispensationMapRepository, Mockito.times(1)).findAllByDispensationIdIn(any());
    }

    @Test
    public void test_get_stock_quantity_dto() {
        Map<Long, List<ProductInventoryLogDto>> transactionIdToLogsMap = new HashMap<>();
        transactionIdToLogsMap.put(1L, product_inventory_dto_list_with_inventory());
        transactionIdToLogsMap.put(2L, product_inventory_dto_list_without_inventory());
        Map<Long, List<StockQuantityDto>> transactionIdToStockQuantityMap = dispensationService.getStockQuantityDtoMap(transactionIdToLogsMap);
        assertEquals(1L, transactionIdToStockQuantityMap.size());
        assertEquals(1L, transactionIdToStockQuantityMap.get(1L).size());
        assertEquals(5L, transactionIdToStockQuantityMap.get(1L).get(0).getQuantity());
    }

    @Test
    public void test_get_dispensation_id_to_dispensation_product_dto_map_empty() {
        List<Long> dispensationIdList = new ArrayList<>();
        dispensationIdList.add(1L);
        List<DispensationProductDto> dispensationProductDtoList = new ArrayList<>();
        doReturn(dispensationProductDtoList).when(dispensationService).getProductDtoForDispensations(any());
        Map<Long, List<DispensationProductDto>> dispensationIdToDispensationProductDtoMap = dispensationService.getDispensationIdToDispensationProductDtoMap(dispensationIdList, false);
        assertEquals(0L, dispensationIdToDispensationProductDtoMap.size());
        verify(dispensationService, Mockito.times(1)).getProductDtoForDispensations(any());
        verify(dispensationService, Mockito.times(0)).getProductInventoryLogMap(any());
        verify(productService, Mockito.times(0)).getProducts(any());
    }

    @Test
    public void test_get_dispensation_id_to_dispensation_product_dto_map_empty_product() {
        List<Product> productList = new ArrayList<>();
        List<Long> dispensationIdList = new ArrayList<>();
        dispensationIdList.add(1L);
        List<DispensationProductDto> dispensationProductDtoList = Collections.singletonList(dispensation_product_dto_list().get(0));
        Map<Long, List<ProductInventoryLogDto>> transactionIdToLogsMap = new HashMap<>();
        transactionIdToLogsMap.put(1L, product_inventory_dto_list_with_inventory());
        doReturn(dispensationProductDtoList).when(dispensationService).getProductDtoForDispensations(any());
        doReturn(transactionIdToLogsMap).when(dispensationService).getProductInventoryLogMap(any());
        when(productService.getProducts(any())).thenReturn(productList);
        Map<Long, List<DispensationProductDto>> dispensationIdToDispensationProductDtoMap = dispensationService.getDispensationIdToDispensationProductDtoMap(dispensationIdList, false);

        assertTrue(dispensationIdToDispensationProductDtoMap.containsKey(1L));
        assertEquals(1L, dispensationIdToDispensationProductDtoMap.size());
        assertNull(dispensationIdToDispensationProductDtoMap.get(1L).get(0).getProductName());
        assertNull(dispensationIdToDispensationProductDtoMap.get(1L).get(0).getProductName());
        assertNull(dispensationIdToDispensationProductDtoMap.get(1L).get(0).getLogs());
        verify(dispensationService, Mockito.times(1)).getProductDtoForDispensations(any());
        verify(dispensationService, Mockito.times(1)).getProductInventoryLogMap(any());
        verify(productService, Mockito.times(1)).getProducts(any());
    }

    @Test
    public void test_get_dispensation_id_to_dispensation_product_dto_map() {
        List<Long> dispensationIdList = new ArrayList<>();
        dispensationIdList.add(1L);
        List<DispensationProductDto> dispensationProductDtoList = Collections.singletonList(dispensation_product_dto_list().get(0));
        Map<Long, List<ProductInventoryLogDto>> transactionIdToLogsMap = new HashMap<>();
        transactionIdToLogsMap.put(1L, product_inventory_dto_list_with_inventory());
        doReturn(dispensationProductDtoList).when(dispensationService).getProductDtoForDispensations(any());
        doReturn(transactionIdToLogsMap).when(dispensationService).getProductInventoryLogMap(any());
        when(productService.getProducts(any())).thenReturn(products());
        Map<Long, List<DispensationProductDto>> dispensationIdToDispensationProductDtoMap = dispensationService.getDispensationIdToDispensationProductDtoMap(dispensationIdList, false);

        assertTrue(dispensationIdToDispensationProductDtoMap.containsKey(1L));
        assertEquals(1L, dispensationIdToDispensationProductDtoMap.size());
        assertNotNull(dispensationIdToDispensationProductDtoMap.get(1L).get(0).getProductName());
        assertNotNull(dispensationIdToDispensationProductDtoMap.get(1L).get(0).getProductName());
        assertNull(dispensationIdToDispensationProductDtoMap.get(1L).get(0).getLogs());
        verify(dispensationService, Mockito.times(1)).getProductDtoForDispensations(any());
        verify(dispensationService, Mockito.times(1)).getProductInventoryLogMap(any());
        verify(productService, Mockito.times(1)).getProducts(any());
    }

    @Test
    public void test_get_dispensation_id_to_dispensation_product_dto_map_with_log() {
        List<Long> dispensationIdList = new ArrayList<>();
        dispensationIdList.add(1L);
        List<DispensationProductDto> dispensationProductDtoList = Collections.singletonList(dispensation_product_dto_list().get(0));
        Map<Long, List<ProductInventoryLogDto>> transactionIdToLogsMap = new HashMap<>();
        transactionIdToLogsMap.put(1L, product_inventory_dto_list_with_inventory());
        doReturn(dispensationProductDtoList).when(dispensationService).getProductDtoForDispensations(any());
        doReturn(transactionIdToLogsMap).when(dispensationService).getProductInventoryLogMap(any());
        when(productService.getProducts(any())).thenReturn(products());
        Map<Long, List<DispensationProductDto>> dispensationIdToDispensationProductDtoMap = dispensationService.getDispensationIdToDispensationProductDtoMap(dispensationIdList, true);

        assertTrue(dispensationIdToDispensationProductDtoMap.containsKey(1L));
        assertEquals(1L, dispensationIdToDispensationProductDtoMap.size());
        assertNotNull(dispensationIdToDispensationProductDtoMap.get(1L).get(0).getProductName());
        assertNotNull(dispensationIdToDispensationProductDtoMap.get(1L).get(0).getProductName());
        assertNotNull(dispensationIdToDispensationProductDtoMap.get(1L).get(0).getLogs());
        assertEquals(2L,dispensationIdToDispensationProductDtoMap.get(1L).get(0).getLogs().size() );
        verify(dispensationService, Mockito.times(1)).getProductDtoForDispensations(any());
        verify(dispensationService, Mockito.times(1)).getProductInventoryLogMap(any());
        verify(productService, Mockito.times(1)).getProducts(any());
    }

    @Test
    public void test_get_all_dispensation_for_entity() throws ParseException, ExecutionException, InterruptedException {
        List<Dispensation> dispensationList = new ArrayList<>();
        dispensationList.add(new Dispensation(1L,1L, 1L, 23L, 33L, "30-40Kg", "Intensive", 1L, 0L, Utils.convertStringToDate("10-07-2020 00:00:00"), Utils.convertStringToDate("10-07-2020 00:00:00"), Utils.convertStringToDate("13-07-2020 00:00:00"), "notes", DispensationStatus.ISSUED));
        DispensationProductDto dispensationProductDto = new DispensationProductDto(1L, 1L, "ProductA", 1L, 1L, "dosageForm", 10L, Utils.convertStringToDate("20-07-2020 00:00:00"), "MTWHFSU", null, null);
        List<DispensationProductDto> dispensationProductDtoList = new ArrayList<>();
        dispensationProductDtoList.add(dispensationProductDto);
        Map<Long, List<DispensationProductDto>> dispensationProductDataMap = new HashMap<>();
        dispensationProductDataMap.put(1L, dispensationProductDtoList);
        doReturn(dispensationList).when(dispensationService).getDispensationsForEntity(any(), any());
        doReturn(dispensationProductDataMap).when(dispensationService).getDispensationIdToDispensationProductDtoMap(any(), any());
        EntityDispensationResponse entityDispensationResponse = dispensationService.getAllDispensationDataForEntity(1L, 1L, true);

        assertEquals(1L, entityDispensationResponse.getEntityId());
        assertEquals(dispensationList.size(), entityDispensationResponse.getDispensations().size());
        assertEquals(1L, entityDispensationResponse.getTotalDispensedDrugs());
        assertNull(entityDispensationResponse.getRefillDate());
        verify(dispensationService, Mockito.times(1)).getDispensationsForEntity(any(), any());
        verify(dispensationService, Mockito.times(1)).getDispensationIdToDispensationProductDtoMap(any(), any());
    }

    @Test
    public void test_get_all_dispensation_data_for_entity_bulk() throws InterruptedException, ExecutionException, IOException, ParseException {
        List<DispensationProductDto> dispensationProductDtoList = new ArrayList<>();
        DispensationProductDto dispensationProductDto = new DispensationProductDto(1L, 1L, "ProductA", 1L, 1L, "dosageForm", 10L, Utils.convertStringToDate("20-07-2020 00:00:00"), "MTWHFSU", null, null);
        dispensationProductDtoList.add(dispensationProductDto);
        Map<Long, List<DispensationProductDto>> dispensationProductDataMap = new HashMap<>();
        dispensationProductDataMap.put(1L, dispensationProductDtoList);
        when(dispensationRepository.findAllByEntityIdInAndClientIdAndDeletedIsFalse(any(), any())).thenReturn(Collections.singletonList(dispensation_optional_entity().get()));
        doReturn(dispensationProductDataMap).when(dispensationService).getDispensationIdToDispensationProductDtoMap(any(), any());
        List<EntityDispensationResponse> entityDispensationResponseList = dispensationService.getAllDispensationDataForEntityBulk(Collections.singletonList(1L), 1L);

        assertEquals(1L, entityDispensationResponseList.get(0).getEntityId());
        assertEquals(1, entityDispensationResponseList.get(0).getDispensations().size());
        assertEquals(1, entityDispensationResponseList.get(0).getTotalDispensedDrugs());
        assertNull(entityDispensationResponseList.get(0).getRefillDate());
        verify(dispensationService, Mockito.times(1)).getDispensationIdToDispensationProductDtoMap(any(), any());
        verify(dispensationRepository, Mockito.times(1)).findAllByEntityIdInAndClientIdAndDeletedIsFalse(any(), any());
    }

    @Test
    public void test_get_all_dispensation_data_for_entity_bulk_with_cache_entry() throws InterruptedException, ExecutionException, IOException, ParseException {
        List<DispensationProductDto> dispensationProductDtoList = new ArrayList<>();
        DispensationProductDto dispensationProductDto = new DispensationProductDto(1L, 1L, "ProductA", 1L, 1L, "dosageForm", 10L, Utils.convertStringToDate("20-07-2020 00:00:00"), "MTWHFSU", null, null);
        dispensationProductDtoList.add(dispensationProductDto);
        Map<Long, List<DispensationProductDto>> dispensationProductDataMap = new HashMap<>();
        dispensationProductDataMap.put(1L, dispensationProductDtoList);
        EntityDispensationResponse response = new EntityDispensationResponse( 1L, Utils.convertStringToDate("20-01-2020 00:00:00"), 1L, Collections.emptyList());
        when(dispensationRepository.findAllByEntityIdInAndClientIdAndDeletedIsFalse(any(), any())).thenReturn(Collections.singletonList(dispensation_optional_entity().get()));
        doReturn(dispensationProductDataMap).when(dispensationService).getDispensationIdToDispensationProductDtoMap(any(), any());
        when(valueOperations.multiGet(any())).thenReturn(Arrays.asList(Utils.asJsonString(response)));
        List<EntityDispensationResponse> entityDispensationResponseList = dispensationService.getAllDispensationDataForEntityBulk(Collections.singletonList(1L), 1L);

        assertEquals(1L, entityDispensationResponseList.get(0).getEntityId());
        assertEquals(0, entityDispensationResponseList.get(0).getDispensations().size());
        assertEquals(1, entityDispensationResponseList.get(0).getTotalDispensedDrugs());
        assertNotNull(entityDispensationResponseList.get(0).getRefillDate());
        verify(dispensationService, Mockito.times(0)).getDispensationIdToDispensationProductDtoMap(any(), any());
        verify(dispensationRepository, Mockito.times(0)).findAllByEntityIdInAndClientIdAndDeletedIsFalse(any(), any());
    }

    @Test
    public void test_add_dispensation_products() throws ParseException {
        Dispensation dispensation = dispensation_optional_entity().get();
        DispensationRequest dispensationRequest = new DispensationRequest(1L, 33L, "30-40Kg", "Intensive", 23L, 1L, "10-07-2020 00:00:00", "10-07-2020 00:00:00", 0l, "10-07-2020 00:00:00", "", dispensationProductList());
        when(productDispensationMapRepository.saveAll(any())).thenReturn(new ArrayList<>());
        List<ProductDispensationMapping> prodDispensationMaps =  dispensationService.addDispensationProducts(dispensationRequest, dispensation.getId(), Utils.convertStringToDate(dispensationRequest.getIssuedDate()));

        assertEquals(dispensationRequest.getProductDataList().size(), prodDispensationMaps.size());
        verify(productDispensationMapRepository, Mockito.times(1)).saveAll(anyCollection());
    }

    @Test
    public void test_add_dispensation_products_with_valid_refill_date() {
        DispensationRequest.ProductData productData = new DispensationRequest.ProductData(1L, 1L, 10L, "Daily");
        List<DispensationRequest.ProductData> productDataList = new ArrayList<>();
        productDataList.add(productData);
        DispensationRequest request = new DispensationRequest(1L, 30L, "30 - 33 Kgs", "Intensive", 1L, 1L, "01-09-2020 00:00:00", "01-09-2020 00:00:00", null, "01-09-2020 00:00:00", "Test Note", productDataList);
        List<ProductDispensationMapping> actualProductMappings = dispensationService.addDispensationProducts(request, 1L, Utils.convertStringToDateOrDefault(request.getIssuedDate()));
        ProductDispensationMapping map = actualProductMappings.get(0);

        assertEquals(Utils.convertStringToDateOrDefault("11-09-2020 00:00:00"), map.getRefillDate());
        verify(productDispensationMapRepository, Mockito.times(1)).saveAll(any());
    }

    @Test
    public void test_add_dispensation_products_null_units_issued_returns_refill_date() {
        DispensationRequest.ProductData productData = new DispensationRequest.ProductData(1L, 1L, null, "Daily");
        List<DispensationRequest.ProductData> productDataList = new ArrayList<>();
        productDataList.add(productData);
        DispensationRequest request = new DispensationRequest(1L, 30L, "30 - 33 Kgs", "Intensive", 1L, 1L, "01-09-2020 00:00:00", "01-09-2020 00:00:00", null, "01-09-2020 00:00:00", "Test Note", productDataList);
        List<ProductDispensationMapping> actualProductMappings = dispensationService.addDispensationProducts(request, 1L, Utils.convertStringToDateOrDefault(request.getIssuedDate()));
        ProductDispensationMapping map = actualProductMappings.get(0);
        assertNull(map.getRefillDate());
        verify(productDispensationMapRepository, Mockito.times(1)).saveAll(any());
    }

    @Test
    public void test_add_dispensation_similar_product_config_data(){
        List<DispensationRequest.ProductData> productDataList = new ArrayList<>();
        productDataList.add(new DispensationRequest.ProductData(1L, 1L, 10L, "Daily"));
        productDataList.add(new DispensationRequest.ProductData(1L, 1L, 10L, "Daily"));
        DispensationRequest request = new DispensationRequest(1L, 30L, "30 - 33 Kgs", "Intensive", 1L, 1L, "01-09-2020 00:00:00", "01-09-2020 00:00:00", null, "01-09-2020 00:00:00", "Test Note", productDataList);
        List<ProductDispensationMapping> actualProductMappings = dispensationService.addDispensationProducts(request, 1L, Utils.convertStringToDateOrDefault(request.getIssuedDate()));
        ProductDispensationMapping map = actualProductMappings.get(0);
        assertEquals(1L, actualProductMappings.size());
        assertEquals(20L, map.getNumberOfUnitsIssued());
        verify(productDispensationMapRepository, Mockito.times(1)).saveAll(any());
    }

    @Test
    public void test_get_product_inventory_log() {
        List<ProductInventoryLog> productInventoryLogs = new ArrayList<>();
        ArrayList<Long> productDispensationMappingIds = new ArrayList<>();
        List<ProductInventoryLogDto> productInventoryLogDtoList = new ArrayList<>();
        productDispensationMappingIds.add(1L);
        ProductInventoryLog log1 = new ProductInventoryLog(TransactionType.ISSUED, 1L, 5L, CommentType.REASON_FOR_ADD, "issued 1 5 units", 1L, 1L,1L,TransactionIdType.INTERNAL);
        productInventoryLogs.add(log1);
        ProductInventoryLogDto productInventoryLogDto1 = new ProductInventoryLogDto(log1.getId(), log1.getTransactionType(), log1.getTransactionId(), log1.getTransactionQuantity(), log1.getCommentType(),
                log1.getComment(), log1.getDateOfAction(),log1.getPInventoryId(), log1.getProductId(), log1.getUpdatedBy(), log1.getTransactionIdType(), "ABC123", Utils.convertStringToDateOrDefault("10-11-2025 00:00:00"));
        productInventoryLogDtoList.add(productInventoryLogDto1);
        when(productInventoryLogRepository.findAllByTransactionIdIn(any())).thenReturn(productInventoryLogs);
        when(stockService.getProductInventoryLogDto(any())).thenReturn(productInventoryLogDtoList);
        Map<Long, List<ProductInventoryLogDto>> productInventoryLogMap = dispensationService.getProductInventoryLogMap(productDispensationMappingIds);

        assertEquals(1, productInventoryLogMap.size());
        assertArrayEquals(productDispensationMappingIds.toArray(), productInventoryLogMap.keySet().toArray());
        verify(productInventoryLogRepository, Mockito.times(1)).findAllByTransactionIdIn(anyList());
        verify(stockService, Mockito.times(1)).getProductInventoryLogDto(any());
    }

    @Test
    public void create_dispensation_with_inventory_data() throws ParseException {
        Dispensation dispensation = dispensation_optional_entity().get();
        List<DispensationRequest.ProductData> dispensationProductList = new ArrayList<>();
        dispensationProductList.add(new DispensationRequest.ProductData(1L, 1L, 10L, "MTWHFSU", "20-07-2020 00:00:00", "ABC", "10-11-2025 00:00:00" ));
        DispensationRequest dispensationRequest = new DispensationRequest(1L, 33L, "30-40Kg", "Intensive", 23L, 1L, "10-07-2020 00:00:00", "10-07-2020 00:00:00", 0L, "10-07-2020 00:00:00", "", dispensationProductList);
        ProductDispensationMapping map = new ProductDispensationMapping(1L, 1L, 1L, 1L, 10L, "MTWHFSU");
        ProductInventoryLog productInventoryLog = new ProductInventoryLog(TransactionType.ISSUED, map.getId(), 10L, CommentType.REASON_FOR_ADD, "Died", 1L, 1L,1L, TransactionIdType.INTERNAL);
        when(productDispensationMapRepository.save(any())).thenReturn(map);
        when(stockService.setStockData(any(), any(), any())).thenReturn(productInventoryLog);
        when(dispensationRepository.save(any())).thenReturn(dispensation);
        doNothing().when(dispensationService).emitForEpisode(any(), anyLong());
        Dispensation actualDispensation = dispensationService.createDispensationWithInventoryData(dispensationRequest, 1L);

        assertEquals(DispensationStatus.ISSUED, actualDispensation.getStatus());
        assertEquals(dispensationRequest.getEntityId(), actualDispensation.getEntityId());
        assertEquals(dispensationRequest.getAddedBy(), actualDispensation.getAddedBy());
        verify(dispensationRepository, Mockito.times(1)).save(any());
        verify(productDispensationMapRepository, Mockito.times(1)).save(any());
        verify(stockService, Mockito.times(1)).setStockData(any(), any(), any());
    }

    @Test
    public void set_add_dispensation_stock_data() {
        List<DispensationRequest.ProductData> dispensationProductList = new ArrayList<>();
        dispensationProductList.add(new DispensationRequest.ProductData(1L, 1L, 10L, "MTWHFSU", "20-07-2020 00:00:00", "ABC", "10-11-2025 00:00:00" ));
        DispensationRequest dispensationRequest = new DispensationRequest(1L, 30L, "30 - 33 Kgs", "Intensive", 1L, 1L, "01-09-2020", "01-09-2020", 30L, "01-09-2020", "Test Note", dispensationProductList);
        ProductDispensationMapping map = new ProductDispensationMapping(1L, 1L, 1L, 1L, 10L, "MTWHFSU");
        ProductInventoryLog productInventoryLog = new ProductInventoryLog(TransactionType.ISSUED, map.getId(), 10L, CommentType.REASON_FOR_ADD, "Died", 1L, 1L,1L, TransactionIdType.INTERNAL);
        when(productDispensationMapRepository.save(any())).thenReturn(map);
        when(stockService.setStockData(any(), any(), any())).thenReturn(productInventoryLog);
        dispensationService.setAddDispensationStockData(dispensationRequest, 1L, 1L, Utils.convertStringToDateOrDefault("01-09-2020" ));

        verify(productDispensationMapRepository, Mockito.times(1)).save(any());
        verify(stockService, Mockito.times(1)).setStockData(any(), any(), any());
        verify(stockService, Mockito.times(0)).setInventoryLogData(any(), any(), any());
    }

    @Test
    public void set_add_dispensation_stock_data_without_inventory() {
        List<DispensationRequest.ProductData> dispensationProductList = new ArrayList<>();
        dispensationProductList.add(new DispensationRequest.ProductData(1L, 1L, 10L, "MTWHFSU", "20-07-2020 00:00:00"));
        DispensationRequest dispensationRequest = new DispensationRequest(1L, 30L, "30 - 33 Kgs", "Intensive", 1L, 1L, "01-09-2020", "01-09-2020", 30L, "01-09-2020", "Test Note", dispensationProductList);
        ProductDispensationMapping map = new ProductDispensationMapping(1L, 1L, 1L, 1L, 10L, "MTWHFSU");
        ProductInventoryLog productInventoryLog = new ProductInventoryLog(TransactionType.ISSUED, map.getId(), 10L, CommentType.REASON_FOR_ADD, "Died", null, 1L,1L, TransactionIdType.INTERNAL);
        when(productDispensationMapRepository.save(any())).thenReturn(map);
        when(stockService.setInventoryLogData(any(), any(), any())).thenReturn(productInventoryLog);
        dispensationService.setAddDispensationStockData(dispensationRequest, 1L, 1L, Utils.convertStringToDateOrDefault("01-09-2020" ));

        verify(productDispensationMapRepository, Mockito.times(1)).save(any());
        verify(stockService, Mockito.times(0)).setStockData(any(), any(), any());
        verify(stockService, Mockito.times(1)).setInventoryLogData(any(), any(), any());
    }

    @Test
    public void test_set_refill_date() {
        Dispensation dispensation = new Dispensation(1L, 1L, 2L, 4L, "20 pills", "test", 20L, 12L, "Dispensed", DispensationStatus.ISSUED);
        Date refillDate = Utils.getCurrentDate();
        dispensationService.setRefillDate(dispensation, refillDate);
        verify(dispensationRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void emitForEpisodeTest() {
        DispensationRequest dispensationRequest = new DispensationRequest();
        dispensationRequest.setEntityId(1L);
        Date date = new Date();
        doReturn(new ArrayList<>()).when(dispensationService).getDispensationsForEntity(any(), eq(1L));
        doReturn(date).when(dispensationService).getRefillDateForEntity(any(), anyList());
        doNothing().when(applicationEventPublisher).publishEvent(mock(EpisodeFieldsReponse.class));
        Map<String, Object> episodeFieldMap = new HashMap<>();
        episodeFieldMap.put(Constants.ID, 1L);
        episodeFieldMap.put(Constants.DISPENSATION_FIELD_LAST_REFILL_DATE, Utils.getFormattedDate(date, "yyyy-MM-dd HH:mm:ss"));
        EpisodeFieldsReponse episodeFieldsReponse = new EpisodeFieldsReponse("dispensation", episodeFieldMap, 1L);
        dispensationService.emitForEpisode(dispensationRequest, 1L);
        Mockito.verify(applicationEventPublisher, Mockito.times(1)).publishEvent(eq(episodeFieldsReponse));
    }

}

package com.everwell.dispensationservice.unit.handlers;

import com.everwell.dispensationservice.Utils.CacheUtils;
import com.everwell.dispensationservice.Utils.Utils;
import com.everwell.dispensationservice.binders.DispensationEventsBinder;
import com.everwell.dispensationservice.binders.EpisodeFieldsBinder;
import com.everwell.dispensationservice.constants.Constants;
import com.everwell.dispensationservice.enums.DispensationStatus;
import com.everwell.dispensationservice.enums.event.EventActionEnum;
import com.everwell.dispensationservice.enums.event.EventCategoryEnum;
import com.everwell.dispensationservice.enums.event.EventNameEnum;
import com.everwell.dispensationservice.handlers.SpringEventHandler;
import com.everwell.dispensationservice.models.db.Client;
import com.everwell.dispensationservice.models.db.Dispensation;
import com.everwell.dispensationservice.models.db.Product;
import com.everwell.dispensationservice.models.dto.DispensationProductDto;
import com.everwell.dispensationservice.models.dto.EpisodeFieldsReponse;
import com.everwell.dispensationservice.models.dto.eventstreaming.EventDto;
import com.everwell.dispensationservice.repositories.ClientRepository;
import com.everwell.dispensationservice.services.DispensationService;
import com.everwell.dispensationservice.services.ProductService;
import com.everwell.dispensationservice.unit.BaseTestNoSpring;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;

import java.io.IOException;
import java.text.ParseException;
import java.util.*;


import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class SpringEventHandlerTest extends BaseTestNoSpring {

    @Spy
    @InjectMocks
    SpringEventHandler springEventHandler;

    @Mock
    DispensationEventsBinder dispensationEventsBinder;

    @Mock
    ClientRepository clientRepository;

    @Mock
    MessageChannel eventOutput;

    @Mock
    MessageChannel eventOutputEpisode;

    @Mock
    MessageChannel shareDispensationEventOutput;

    @Mock
    private DispensationService dispensationService;

    @Mock
    EpisodeFieldsBinder episodeFieldsBinder;

    @Mock
    private ProductService productService;

    CacheUtils cacheUtils;

    @Mock
    RedisTemplate<String, Object> redisTemplate;

    @Mock
    private ValueOperations<String, Object> valueOperations;

    @BeforeEach
    public void initMocks() {
        when(dispensationEventsBinder.eventOutput()).thenReturn(eventOutput);
        when(dispensationEventsBinder.shareDispensationEventOutput()).thenReturn(shareDispensationEventOutput);
        when(episodeFieldsBinder.episodeTrackerPublisher()).thenReturn(eventOutputEpisode);

        cacheUtils = new CacheUtils(redisTemplate);
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
        springEventHandler.setAbdmM2FlowEnabled(true);
        when(clientRepository.findAll()).thenReturn(Collections.singletonList(new Client(29L,"test", "test", null )));
        springEventHandler.init();
    }

    private Optional<Dispensation> dispensation_optional_entity() throws ParseException {
        return java.util.Optional.of(new Dispensation(1L,1L,1L,23L, 33L, "30-33Kg", "Intensive", 0L,1L, Utils.convertStringToDate("10-07-2020 00:00:00"), Utils.convertStringToDate("10-07-2020 00:00:00"), Utils.convertStringToDate("13-07-2020 00:00:00"), "notes", DispensationStatus.ISSUED));
    }

    private List<DispensationProductDto> dispensation_product_list() {
        List<DispensationProductDto> dispensationProductDtos = new ArrayList<>();
        dispensationProductDtos.add(new DispensationProductDto(1L, 1L, 3L, 1L, 10L, null, "MTWHFSU"));
        return dispensationProductDtos;
    }

    private List<Product> product_list() {
        List<Product> productList = new ArrayList<>();
        productList.add(new Product(1L, "dc1", "Product1", "dose1", "df1", "manufacturer1", "composition1", "category"));
        return productList;
    }

    @Test
    public void trackEventTest() throws IOException {
        EventDto eventDto = new EventDto("category","name","action",1L);
        springEventHandler.trackEvent(eventDto);
        Mockito.verify(eventOutput,Mockito.times(1))
                .send(any(Message.class));
    }

    @Test
    public void trackShareDispensationEvent() throws ParseException {
        Long entityId = 1L;
        Dispensation dispensation = dispensation_optional_entity().get();
        List<Long> productIdList = new ArrayList<>();
        productIdList.add(1L);
        Map<Long, List<DispensationProductDto>> dispensationProductDataMap = new HashMap<>();
        dispensationProductDataMap.put(1L, dispensation_product_list());
        when(dispensationService.getDispensation(1L, 1L)).thenReturn(dispensation);
        when(productService.getProducts(productIdList)).thenReturn(product_list());
        doReturn(dispensationProductDataMap).when(dispensationService).getDispensationIdToDispensationProductDtoMap(Collections.singletonList(1L), true);
        springEventHandler.trackShareDispensationEvent(new EventDto(EventCategoryEnum.DISPENSATION.getName(), EventNameEnum.ADD_DISPENSATION.getName(), EventActionEnum.DB_RECORD_SAVE.getName(), Collections.singletonMap(EventCategoryEnum.DISPENSATION.getName(), entityId), 1L));
        Mockito.verify(shareDispensationEventOutput,Mockito.times(1))
                .send(any(Message.class));
    }

    @Test
    public void updateDataForEpisode() {
        Map<String, Object> episodeFieldMap = new HashMap<>();
        episodeFieldMap.put(Constants.DISPENSATION_FIELD_LAST_REFILL_DATE, Utils.getFormattedDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
        EpisodeFieldsReponse episodeFieldsReponse = new EpisodeFieldsReponse("dispensation", episodeFieldMap, 1L);
        springEventHandler.updateDataForEpisode(episodeFieldsReponse);
        Mockito.verify(eventOutputEpisode, Mockito.times(1))
                .send(any(Message.class));
    }

    @Test
    public void test_invalidateAllProductIdsCacheEntry() {
        EventDto eventDto = new EventDto("category","Add product","action",1L);
        springEventHandler.invalidateAllProductIdsCacheEntry(eventDto);
        Mockito.verify(productService,Mockito.times(1))
                .deleteAllProductIdsCacheEntry();
    }

    @Test
    public void test_invalidateEntityDispensationCacheEntry() {
        Long entityId = 1L;
        springEventHandler.invalidateEntityDispensationCacheEntry(new EventDto(EventCategoryEnum.DISPENSATION.getName(), EventNameEnum.ADD_DISPENSATION.getName(), EventActionEnum.DB_RECORD_SAVE.getName(), Collections.singletonMap(EventCategoryEnum.ENTITY.getName(), entityId), 1L));
        Mockito.verify(redisTemplate, Mockito.times(1)).delete(eq(String.format(Constants.CACHE_ENTITY_DISPENSATION_KEY, entityId, 1L)));
    }

    @Test
    public void test_invalidateEntityDispensationCacheEntry_with_dispensationEventKeyValuePair() throws ParseException {
        Long entityId = 1L;
        Dispensation dispensation = dispensation_optional_entity().get();
        when(dispensationService.getDispensation(1L, 1L)).thenReturn(dispensation);
        springEventHandler.invalidateEntityDispensationCacheEntry(new EventDto(EventCategoryEnum.DISPENSATION.getName(), EventNameEnum.ADD_DISPENSATION.getName(), EventActionEnum.DB_RECORD_SAVE.getName(), Collections.singletonMap(EventCategoryEnum.DISPENSATION.getName(), entityId), 1L));
        Mockito.verify(redisTemplate, Mockito.times(1)).delete(eq(String.format(Constants.CACHE_ENTITY_DISPENSATION_KEY, entityId, 1L)));
    }

}

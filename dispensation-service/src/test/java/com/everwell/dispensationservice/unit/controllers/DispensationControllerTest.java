package com.everwell.dispensationservice.unit.controllers;

import com.everwell.dispensationservice.Utils.CacheUtils;
import com.everwell.dispensationservice.constants.Constants;
import com.everwell.dispensationservice.controllers.DispensationController;
import com.everwell.dispensationservice.enums.*;
import com.everwell.dispensationservice.exceptions.CustomExceptionHandler;
import com.everwell.dispensationservice.models.db.ProductDispensationMapping;
import com.everwell.dispensationservice.models.db.ProductInventoryLog;
import com.everwell.dispensationservice.models.dto.DispensationData;
import com.everwell.dispensationservice.models.dto.DispensationProductDto;
import com.everwell.dispensationservice.models.dto.RangeValueFilter;
import com.everwell.dispensationservice.models.http.requests.DispensationRequest;
import com.everwell.dispensationservice.models.http.requests.SearchDispensationRequest;
import com.everwell.dispensationservice.models.http.requests.ReturnDispensationRequest;
import com.everwell.dispensationservice.models.http.responses.EntityDispensationResponse;
import com.everwell.dispensationservice.repositories.DispensationRepository;
import com.everwell.dispensationservice.repositories.ProductDispensationMapRepository;
import com.everwell.dispensationservice.services.ProductService;
import com.everwell.dispensationservice.unit.BaseTest;
import com.everwell.dispensationservice.Utils.Utils;
import com.everwell.dispensationservice.models.db.Dispensation;
import com.everwell.dispensationservice.services.DispensationService;
import com.everwell.dispensationservice.unit.BaseTestNoSpring;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

import static com.everwell.dispensationservice.Utils.Utils.asJsonString;
import static com.everwell.dispensationservice.constants.Constants.X_DS_CLIENT_ID;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class DispensationControllerTest extends BaseTestNoSpring {

    private MockMvc mockMvc;

    @Spy
    @InjectMocks
    private DispensationController dispensationController;

    @Mock
    private DispensationService dispensationService;

    @Mock
    private ProductService productService;

    @MockBean
    private DispensationRepository dispensationRepository;

    @MockBean
    private ProductDispensationMapRepository productDispensationMapRepository;

    @Mock
    ApplicationEventPublisher applicationEventPublisher;

    CacheUtils cacheUtils;

    @Mock
    RedisTemplate<String, Object> redisTemplate;

    @Mock
    private ValueOperations<String, Object> valueOperations;


    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(dispensationController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
        cacheUtils = new CacheUtils(redisTemplate);
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
    }

    private Optional<Dispensation> dispensation_optional_entity() throws ParseException {
        return java.util.Optional.of(new Dispensation(1L,1L,1L,23L, 33L, "30-33Kg", "Intensive", 0L,1L, Utils.convertStringToDate("10-07-2020 00:00:00"), Utils.convertStringToDate("10-07-2020 00:00:00"), Utils.convertStringToDate("13-07-2020 00:00:00"), "notes", DispensationStatus.ISSUED));
    }

    private List<EntityDispensationResponse> entity_dispensation_response_list() {
        List<EntityDispensationResponse> entityDispensationResponseList = new ArrayList<>();
        entityDispensationResponseList.add(new EntityDispensationResponse(1L, Utils.convertStringToDateOrDefault("01-10-2020 00:00:00"), 30L, Collections.singletonList(new DispensationData(new Dispensation(1L, 1L, 1L, null, 1L, Utils.convertStringToDateOrDefault("01-09-2020 00:00:00"), 30L, "30 - 33 Kgs", "Intensive", 30L,
                Utils.convertStringToDateOrDefault("01-10-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-09-2020 00:00:00"), Utils.convertStringToDateOrDefault("01-09-2020 00:00:00"), 1L, false, "", DispensationStatus.ISSUED), Collections.emptyList()))));
        return entityDispensationResponseList;
    }

    private List<DispensationRequest.ProductData> dispensationProductList () {
        List<DispensationRequest.ProductData> dispensationProductList = new ArrayList<>();
        dispensationProductList.add(new DispensationRequest.ProductData(1L, 1L, 10L, "MTWHFSU", "20-07-2020 00:00:00", "ABC", "20-11-2025 00:00:00"));
        dispensationProductList.add(new DispensationRequest.ProductData(2L, 1L, 10L, "MTWHFSU", "20-07-2020 00:00:00", "ABC", "20-11-2025 00:00:00"));
        dispensationProductList.add(new DispensationRequest.ProductData(3L, 1L, 10L, "MTWHFSU", "20-07-2020 00:00:00", "ABC", "20-11-2025 00:00:00"));
        dispensationProductList.add(new DispensationRequest.ProductData(4L, 1L, 10L, "MTWHFSU", "20-07-2020 00:00:00", "ABC", "20-11-2025 00:00:00"));
        return dispensationProductList;
    }

    private List<DispensationProductDto> dispensation_product_list() {
        List<DispensationProductDto> dispensationProductDtos = new ArrayList<>();
        dispensationProductDtos.add(new DispensationProductDto(1L, 1L, 3L, 1L, 10L, null, "MTWHFSU"));
        dispensationProductDtos.add(new DispensationProductDto(2L, 2L, 3L, 1L, 10L, null, "MTWHFSU"));
        dispensationProductDtos.add(new DispensationProductDto(3L, 3L, 3L, 1L, 10L, null, "MTWHFSU"));
        dispensationProductDtos.add(new DispensationProductDto(4L, 4L, 3L, 1L, 10L, null, "MTWHFSU"));
        dispensationProductDtos.add(new DispensationProductDto(5L, 5L, 3L, 1L, 10L, null, "MTWHFSU"));
        dispensationProductDtos.add(new DispensationProductDto(6L, 6L, 3L, 1L, 10L, null, "MTWHFSU"));
        return dispensationProductDtos;
    }

    private List<ProductDispensationMapping> getProductDispensationMappings(Long dispensationId, List<DispensationRequest.ProductData> dispensationProductDataList) {
        return dispensationProductDataList
                .stream()
                .map(dispProduct-> {
                    ProductDispensationMapping map = new ProductDispensationMapping(1L, dispProduct.getProductId(), dispProduct.getProductConfigId(), dispensationId, dispProduct.getUnitsIssued(), dispProduct.getDosingSchedule());
                    Date refillDate = Utils.getCurrentDate();
                    try{
                        refillDate = Utils.convertStringToDate(dispProduct.getRefillDate());
                    } catch (ParseException ignored) {

                    }
                    map.setRefillDate(refillDate);
                    return map;
                })
                .collect(Collectors.toList());
    }

    @Test
    public void test_create_dispensation() throws Exception {
        long clientId = 1L;
        Dispensation dispensation = dispensation_optional_entity().get();
        DispensationRequest dispensationRequest = new DispensationRequest(1L, 33L, "30-40Kg", "Intensive", 23L, 1L, "10-07-2020 00:00:00", "10-07-2020 00:00:00", 0L, "10-07-2020 00:00:00", "", dispensationProductList());
        doNothing().when(productService).validateProductIds(any());
        when(dispensationService.create(any(), any())).thenReturn(dispensation);
        List<ProductDispensationMapping> prodDispMaps = getProductDispensationMappings(dispensation.getId(), dispensationProductList());
        when(dispensationService.addDispensationProducts(any(), anyLong(), any())).thenReturn(prodDispMaps);
        mockMvc
                .perform(post("/v1/dispensation")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(dispensationRequest))
                        .header(X_DS_CLIENT_ID, Long.toString(clientId)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.message").value("dispensation created successfully"));
    }

    @Test
    public void test_create_dispensation_inventory() throws Exception {
        long clientId = 1L;
        Dispensation dispensation = dispensation_optional_entity().get();
        DispensationRequest dispensationRequest = new DispensationRequest(123L, 33L, "30-40Kg", "Intensive", 23L, 1L, "10-07-2020 00:00:00", "10-07-2020 00:00:00", 0L, "10-07-2020 00:00:00", "", dispensationProductList());
        doNothing().when(productService).validateProductIds(any());
        when(dispensationService.createDispensationWithInventoryData(any(), any())).thenReturn(dispensation);
        mockMvc
                .perform(post("/v2/dispensation")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(dispensationRequest))
                        .header(X_DS_CLIENT_ID, Long.toString(clientId)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.message").value("dispensation created successfully"));
    }

    @Test
    public void test_create_dispensation_inventory_null_entity_id_throws_validation_exception() throws Exception {
        long clientId = 1L;
        Dispensation dispensation = dispensation_optional_entity().get();
        DispensationRequest dispensationRequest = new DispensationRequest(null, 33L, "30-40Kg", "Intensive", 23L, 1L, "10-07-2020 00:00:00", "10-07-2020 00:00:00", 0L, "10-07-2020 00:00:00", "", dispensationProductList());
        doNothing().when(productService).validateProductIds(any());
        when(dispensationService.createDispensationWithInventoryData(any(), any())).thenReturn(dispensation);
        mockMvc
                .perform(post("/v2/dispensation")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(dispensationRequest))
                        .header(X_DS_CLIENT_ID, Long.toString(clientId)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("entity id is required"));
    }

    @Test
    public void test_create_dispensation_inventory_null_added_by_throws_validation_exception() throws Exception {
        long clientId = 1L;
        Dispensation dispensation = dispensation_optional_entity().get();
        DispensationRequest dispensationRequest = new DispensationRequest(1L, 33L, "30-40Kg", "Intensive", null, 1L, "10-07-2020 00:00:00", "10-07-2020 00:00:00", 0L, "10-07-2020 00:00:00", "", dispensationProductList());
        doNothing().when(productService).validateProductIds(any());
        when(dispensationService.createDispensationWithInventoryData(any(), any())).thenReturn(dispensation);
        mockMvc
                .perform(post("/v2/dispensation")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(dispensationRequest))
                        .header(X_DS_CLIENT_ID, Long.toString(clientId)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("added by is required"));
    }

    @Test
    public void test_create_dispensation_inventory_null_product_id_throws_validation_exception() throws Exception {
        long clientId = 1L;
        Dispensation dispensation = dispensation_optional_entity().get();
        List<DispensationRequest.ProductData>  dispensationProductList = dispensationProductList();
        dispensationProductList.add(new DispensationRequest.ProductData(null, 1L, 10L, "MTWHFSU", "20-07-2020 00:00:00", "ABC", "10-07-2025 00:00:00" ));
        DispensationRequest dispensationRequest = new DispensationRequest(1L, 33L, "30-40Kg", "Intensive", 23L, 1L, "10-07-2020 00:00:00", "10-07-2020 00:00:00", 0L, "10-07-2020 00:00:00", "", dispensationProductList);
        doNothing().when(productService).validateProductIds(any());
        when(dispensationService.createDispensationWithInventoryData(any(), any())).thenReturn(dispensation);
        mockMvc
                .perform(post("/v2/dispensation")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(dispensationRequest))
                        .header(X_DS_CLIENT_ID, Long.toString(clientId)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("product id cannot be null"));
    }

    @Test
    public void test_create_dispensation_inventory_null_product_config_id_throws_validation_exception() throws Exception {
        long clientId = 1L;
        Dispensation dispensation = dispensation_optional_entity().get();
        List<DispensationRequest.ProductData>  dispensationProductList = dispensationProductList();
        dispensationProductList.add(new DispensationRequest.ProductData(1L, null, 10L, "MTWHFSU", "20-07-2020 00:00:00", "ABC", "10-07-2025 00:00:00" ));
        DispensationRequest dispensationRequest = new DispensationRequest(1L, 33L, "30-40Kg", "Intensive", 23L, 1L, "10-07-2020 00:00:00", "10-07-2020 00:00:00", 0L, "10-07-2020 00:00:00", "", dispensationProductList);
        doNothing().when(productService).validateProductIds(any());
        when(dispensationService.createDispensationWithInventoryData(any(), any())).thenReturn(dispensation);
        mockMvc
                .perform(post("/v2/dispensation")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(dispensationRequest))
                        .header(X_DS_CLIENT_ID, Long.toString(clientId)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("product deployment config cannot be null"));
    }

    @Test
    public void test_create_dispensation_inventory_invalid_issue_date_format_throws_validation_exception() throws Exception {
        long clientId = 1L;
        Dispensation dispensation = dispensation_optional_entity().get();
        DispensationRequest dispensationRequest = new DispensationRequest(1L, 33L, "30-40Kg", "Intensive", 23L, 1L, "10-07---2020 00:00:00", "10-07-2020 00:00:00", 0L, "10-07-2020 00:00:00", "", dispensationProductList());
        doNothing().when(productService).validateProductIds(any());
        when(dispensationService.createDispensationWithInventoryData(any(), any())).thenReturn(dispensation);
        mockMvc
                .perform(post("/v2/dispensation")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(dispensationRequest))
                        .header(X_DS_CLIENT_ID, Long.toString(clientId)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("issued date format is not valid"));
    }

    @Test
    public void test_create_dispensation_inventory_invalid_prescription_date_format_throws_validation_exception() throws Exception {
        long clientId = 1L;
        Dispensation dispensation = dispensation_optional_entity().get();
        DispensationRequest dispensationRequest = new DispensationRequest(1L, 33L, "30-40Kg", "Intensive", 23L, 1L, "10-07-2020 00:00:00", "10-07-2020 00:00:00", 0L, "10-07----2020 00:00:00", "", dispensationProductList());
        doNothing().when(productService).validateProductIds(any());
        when(dispensationService.createDispensationWithInventoryData(any(), any())).thenReturn(dispensation);
        mockMvc
                .perform(post("/v2/dispensation")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(dispensationRequest))
                        .header(X_DS_CLIENT_ID, Long.toString(clientId)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("prescription date format is not valid"));
    }

    @Test
    public void test_get_dispensation_details_for_entity() throws Exception {
        long clientId = 1L;
        Long entityId = 1L;
        EntityDispensationResponse entityDispensationResponse = entity_dispensation_response_list().get(0);
        when(dispensationService.getAllDispensationDataForEntity(anyLong(), anyLong(), anyBoolean())).thenReturn(entityDispensationResponse);
        mockMvc
                .perform(get("/v1/dispensation/entity/{entityId}", entityId)
                        .header(X_DS_CLIENT_ID, Long.toString(clientId)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message").value("data fetched successfully"));
    }

    @Test
    public void test_get_dispensation_details_for_entity_cached_response() throws Exception {
        long clientId = 1L;
        Long entityId = 1L;
        EntityDispensationResponse entityDispensationResponse = entity_dispensation_response_list().get(0);
        when(valueOperations.get(String.format(Constants.CACHE_ENTITY_DISPENSATION_KEY, entityId, clientId))).thenReturn((Utils.asJsonString(entityDispensationResponse)));
        mockMvc
                .perform(get("/v1/dispensation/entity/{entityId}", entityId)
                        .header(X_DS_CLIENT_ID, Long.toString(clientId)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message").value("data fetched successfully"));
    }

    @Test
    public void test_get_dispensation() throws Exception {
        Dispensation dispensation = dispensation_optional_entity().get();
        Map<Long, List<DispensationProductDto>> dispensationIdToDispensationProductDtoMap = new HashMap<>();
        dispensationIdToDispensationProductDtoMap.put(dispensation.getId(), dispensation_product_list());
        when(dispensationService.getDispensation(any(), any())).thenReturn(dispensation);
        when(dispensationService.getDispensationIdToDispensationProductDtoMap(any(), any())).thenReturn(dispensationIdToDispensationProductDtoMap);
        mockMvc
                .perform(get("/v1/dispensation/{id}", 1L)
                        .header(X_DS_CLIENT_ID, dispensation.getClientId().toString()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message").value("dispensation data fetched successfully"));
    }

    @Test
    public void test_delete_dispensation() throws Exception {
        Dispensation dispensation = dispensation_optional_entity().get();
        when(dispensationService.delete(any(), any())).thenReturn(dispensation);
        mockMvc
                .perform(delete("/v1/dispensation/{id}", dispensation.getId())
                        .header(X_DS_CLIENT_ID, dispensation.getClientId().toString()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message").value("dispensation deleted successfully"));
    }

    @Test
    public void test_search_multiple_entity_dispensations () throws Exception {
        SearchDispensationRequest request = new SearchDispensationRequest(Collections.singletonList(1L), Collections.singletonList(new RangeValueFilter(RangeFilterType.NEXT_REFILL_DATE, "01-08-2020 00:00:00", "01-10-2020 00:00:00")));
        EntityDispensationResponse entityDispensationResponse = entity_dispensation_response_list().get(0);
        when(dispensationService.getAllDispensationDataForEntity(anyLong(), anyLong(), anyBoolean())).thenReturn(entityDispensationResponse);
        mockMvc
                .perform(post("/v1/dispensation/entity/search")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(request))
                        .header(X_DS_CLIENT_ID, Long.toString(1L)))
                .andExpect(jsonPath("$.message").value("data fetched successfully"));
    }

    @Test
    public void test_search_multiple_entity_dispensations_with_cache_entries () throws Exception {
        SearchDispensationRequest request = new SearchDispensationRequest(Collections.singletonList(1L), Collections.singletonList(new RangeValueFilter(RangeFilterType.NEXT_REFILL_DATE, "01-08-2020 00:00:00", "01-10-2020 00:00:00")));
        EntityDispensationResponse entityDispensationResponse = entity_dispensation_response_list().get(0);
        when(dispensationService.getAllDispensationDataForEntity(anyLong(), anyLong(), anyBoolean())).thenReturn(entityDispensationResponse);
        when(valueOperations.multiGet(any())).thenReturn(Arrays.asList(Utils.asJsonString(entityDispensationResponse)));
        mockMvc
                .perform(post("/v1/dispensation/entity/search")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(request))
                        .header(X_DS_CLIENT_ID, Long.toString(1L)))
                .andExpect(jsonPath("$.message").value("data fetched successfully"));
    }

    @Test
    public void test_search_multiple_entity_dispensations_empty_entity_idList_throws_validation_exception () throws Exception {
        SearchDispensationRequest request = new SearchDispensationRequest(Collections.emptyList(), Arrays.asList(new RangeValueFilter(RangeFilterType.NEXT_REFILL_DATE, "01-08-2020 00:00:00", "01-10-2020 00:00:00")));
        EntityDispensationResponse entityDispensationResponse = entity_dispensation_response_list().get(0);
        when(dispensationService.getAllDispensationDataForEntity(anyLong(), anyLong(), anyBoolean())).thenReturn(entityDispensationResponse);
        mockMvc
                .perform(post("/v1/dispensation/entity/search")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(request))
                        .header(X_DS_CLIENT_ID, Long.toString(1L)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("invalid entity list"));
    }

    @Test
    public void test_search_multiple_entity_dispensations_invalid_from_date_throws_validation_exception () throws Exception {
        SearchDispensationRequest request = new SearchDispensationRequest(Arrays.asList(1L,2L), Arrays.asList(new RangeValueFilter(RangeFilterType.NEXT_REFILL_DATE, "01-08-2020", "01-10-2020 00:00:00")));
        EntityDispensationResponse entityDispensationResponse = entity_dispensation_response_list().get(0);
        when(dispensationService.getAllDispensationDataForEntity(anyLong(), anyLong(), anyBoolean())).thenReturn(entityDispensationResponse);
        mockMvc
                .perform(post("/v1/dispensation/entity/search")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(request))
                        .header(X_DS_CLIENT_ID, Long.toString(1L)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("invalid from date format"));
    }

    @Test
    public void test_search_multiple_entity_dispensations_invalid_to_date_throws_validation_exception () throws Exception {
        SearchDispensationRequest request = new SearchDispensationRequest(Arrays.asList(1L,2L), Arrays.asList(new RangeValueFilter(RangeFilterType.NEXT_REFILL_DATE, "01-08-2020 00:00:00", "01-10-2020")));
        EntityDispensationResponse entityDispensationResponse = entity_dispensation_response_list().get(0);
        when(dispensationService.getAllDispensationDataForEntity(anyLong(), anyLong(), anyBoolean())).thenReturn(entityDispensationResponse);
        mockMvc
                .perform(post("/v1/dispensation/entity/search")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(request))
                        .header(X_DS_CLIENT_ID, Long.toString(1L)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("invalid to date format"));
    }

    @Test
    public void test_return_dispensation_with_null_dispensationId () throws Exception {
        ReturnDispensationRequest request = new ReturnDispensationRequest(null, new ArrayList<>());
        ProductDispensationMapping map = new ProductDispensationMapping(1L, 1L, 1L, 1L, 10L, "test");
        List<ProductDispensationMapping> dispensationMappings = new ArrayList<>();
        dispensationMappings.add(map);
        Dispensation dispensation = new Dispensation(1L, 1L, 2L, 4L, "20 pills", "test", 20L, 12L, "Dispensed", DispensationStatus.ISSUED);
        when(productDispensationMapRepository.findAllByDispensationIdIn(anyCollection())).thenReturn(dispensationMappings);
        when(dispensationRepository.getOne(any())).thenReturn(dispensation);
        mockMvc
                .perform(post("/v1/dispensation/return")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(request))
                        .header(X_DS_CLIENT_ID, Long.toString(1L)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("dispensation id is required"));
    }

    @Test
    public void test_return_dispensation_empty_dispensation_data () throws Exception {
        ReturnDispensationRequest request = new ReturnDispensationRequest(1L, new ArrayList<ReturnDispensationRequest.ReturnDispensationData>());
        ProductInventoryLog dispensationLog = new ProductInventoryLog(TransactionType.RETURNED, 1L, 2L, CommentType.REASON_FOR_RETURN, "Died", null, 1L,1L,TransactionIdType.INTERNAL);
        List<ProductInventoryLog> expectedProductInventoryLogs = new ArrayList<>();
        expectedProductInventoryLogs.add(dispensationLog);
        ProductDispensationMapping map = new ProductDispensationMapping(1L, 1L, 1L, 1L, 10L, "test");
        List<ProductDispensationMapping> dispensationMappings = new ArrayList<>();
        dispensationMappings.add(map);
        Dispensation dispensation = new Dispensation(1L, 1L, 2L, 4L, "20 pills", "test", 20L, 12L, "Dispensed", DispensationStatus.ISSUED);
        when(productDispensationMapRepository.findAllByDispensationIdIn(anyCollection())).thenReturn(dispensationMappings);
        when(dispensationRepository.getOne(any())).thenReturn(dispensation);
        when(dispensationService.createReturnDispensation(any(), any(), any())).thenReturn(expectedProductInventoryLogs);
        mockMvc
                .perform(post("/v1/dispensation/return")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(request))
                        .header(X_DS_CLIENT_ID, Long.toString(1L)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("return dispensation data is required"));
    }

    @Test
    public void test_return_dispensation_success () throws Exception {
        List<ReturnDispensationRequest.ReturnDispensationData> dispensationDataList = new ArrayList<>();
        ProductInventoryLog dispensationLog = new ProductInventoryLog(TransactionType.RETURNED, 1L, 2L, CommentType.REASON_FOR_RETURN, "Died", null, 1L,1L,TransactionIdType.INTERNAL);
        List<ProductInventoryLog> expectedProductInventoryLogs = new ArrayList<>();
        expectedProductInventoryLogs.add(dispensationLog);
        ReturnDispensationRequest.ReturnDispensationData dispensationData = new ReturnDispensationRequest.ReturnDispensationData(1L, 2L, "Wrong dispensation", 1L);
        dispensationDataList.add(dispensationData);
        ReturnDispensationRequest request = new ReturnDispensationRequest(1L, dispensationDataList);
        ProductDispensationMapping map = new ProductDispensationMapping(1L, 1L, 1L, 1L, 10L, "test");
        List<ProductDispensationMapping> dispensationMappings = new ArrayList<>();
        dispensationMappings.add(map);
        Dispensation dispensation = new Dispensation(1L, 1L, 2L, 4L, "20 pills", "test", 20L, 12L, "Dispensed", DispensationStatus.ISSUED);
        when(productDispensationMapRepository.findAllByDispensationIdIn(anyCollection())).thenReturn(dispensationMappings);
        when(dispensationRepository.getOne(any())).thenReturn(dispensation);
        when(dispensationService.createReturnDispensation(any(), any(), any())).thenReturn(expectedProductInventoryLogs);
        mockMvc
                .perform(post("/v1/dispensation/return")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(request))
                        .header(X_DS_CLIENT_ID, Long.toString(1L)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.message").value("return dispensation created successfully"));
    }

    @Test
    public void test_return_dispensation_inventory () throws Exception {
        List<ReturnDispensationRequest.ReturnDispensationData> dispensationDataList = new ArrayList<>();
        ReturnDispensationRequest.ReturnDispensationData dispensationData = new ReturnDispensationRequest.ReturnDispensationData(1L, 2L, "Wrong dispensation", 1L, "ABC123", "01-09-2025 00:00:00", 1L);
        dispensationDataList.add(dispensationData);
        ReturnDispensationRequest request = new ReturnDispensationRequest(1L, dispensationDataList);

        ProductInventoryLog dispensationLog = new ProductInventoryLog(TransactionType.RETURNED, 1L, 2L, CommentType.REASON_FOR_RETURN, "Died", 1L, 1L,1L,TransactionIdType.INTERNAL);
        List<ProductInventoryLog> expectedProductInventoryLogs = new ArrayList<>();
        expectedProductInventoryLogs.add(dispensationLog);
        List<Long> expectedProductInventoryLogIds = expectedProductInventoryLogs
                .stream()
                .map(ProductInventoryLog::getId)
                .collect(Collectors.toList());
        ProductDispensationMapping map = new ProductDispensationMapping(1L, 1L, 1L, 1L, 10L, "test");
        List<ProductDispensationMapping> dispensationMappings = new ArrayList<>();
        dispensationMappings.add(map);
        Dispensation dispensation = new Dispensation(1L, 1L, 2L, 4L, "20 pills", "test", 20L, 12L, "Dispensed", DispensationStatus.ISSUED);
        when(productDispensationMapRepository.findAllByDispensationIdIn(anyCollection())).thenReturn(dispensationMappings);
        when(dispensationRepository.getOne(any())).thenReturn(dispensation);
        when(dispensationService.createReturnDispensationWithInventoryData(any(), any())).thenReturn(expectedProductInventoryLogIds);
        mockMvc
                .perform(post("/v2/dispensation/return")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(request))
                        .header(X_DS_CLIENT_ID, Long.toString(1L)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.message").value("return dispensation created successfully"));
    }

    @Test
    public void test_return_dispensation_inventory_null_transaction_id () throws Exception {
        List<ReturnDispensationRequest.ReturnDispensationData> dispensationDataList = new ArrayList<>();
        ReturnDispensationRequest.ReturnDispensationData dispensationData = new ReturnDispensationRequest.ReturnDispensationData(null, 2L, "Wrong dispensation", 1L, "ABC123", "01-09-2025 00:00:00", 1L);
        dispensationDataList.add(dispensationData);
        ReturnDispensationRequest request = new ReturnDispensationRequest(1L, dispensationDataList);

        ProductInventoryLog dispensationLog = new ProductInventoryLog(TransactionType.RETURNED, 1L, 2L, CommentType.REASON_FOR_RETURN, "Died", 1L, 1L,1L,TransactionIdType.INTERNAL);
        List<ProductInventoryLog> expectedProductInventoryLogs = new ArrayList<>();
        expectedProductInventoryLogs.add(dispensationLog);
        List<Long> expectedProductInventoryLogIds = expectedProductInventoryLogs
                .stream()
                .map(ProductInventoryLog::getId)
                .collect(Collectors.toList());
        ProductDispensationMapping map = new ProductDispensationMapping(1L, 1L, 1L, 1L, 10L, "test");
        List<ProductDispensationMapping> dispensationMappings = new ArrayList<>();
        dispensationMappings.add(map);
        Dispensation dispensation = new Dispensation(1L, 1L, 2L, 4L, "20 pills", "test", 20L, 12L, "Dispensed", DispensationStatus.ISSUED);
        when(productDispensationMapRepository.findAllByDispensationIdIn(anyCollection())).thenReturn(dispensationMappings);
        when(dispensationRepository.getOne(any())).thenReturn(dispensation);
        when(dispensationService.createReturnDispensationWithInventoryData(any(), any())).thenReturn(expectedProductInventoryLogIds);
        mockMvc
                .perform(post("/v2/dispensation/return")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(request))
                        .header(X_DS_CLIENT_ID, Long.toString(1L)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("transaction id is required"));
    }

    @Test
    public void test_return_dispensation_inventory_null_number_of_units () throws Exception {
        List<ReturnDispensationRequest.ReturnDispensationData> dispensationDataList = new ArrayList<>();
        ReturnDispensationRequest.ReturnDispensationData dispensationData = new ReturnDispensationRequest.ReturnDispensationData(1L, null, "Wrong dispensation", 1L, "ABC123", "01-09-2025 00:00:00", 1L);
        dispensationDataList.add(dispensationData);
        ReturnDispensationRequest request = new ReturnDispensationRequest(1L, dispensationDataList);

        ProductInventoryLog dispensationLog = new ProductInventoryLog(TransactionType.RETURNED, 1L, 2L, CommentType.REASON_FOR_RETURN, "Died", 1L, 1L,1L,TransactionIdType.INTERNAL);
        List<ProductInventoryLog> expectedProductInventoryLogs = new ArrayList<>();
        expectedProductInventoryLogs.add(dispensationLog);
        List<Long> expectedProductInventoryLogIds = expectedProductInventoryLogs
                .stream()
                .map(ProductInventoryLog::getId)
                .collect(Collectors.toList());
        ProductDispensationMapping map = new ProductDispensationMapping(1L, 1L, 1L, 1L, 10L, "test");
        List<ProductDispensationMapping> dispensationMappings = new ArrayList<>();
        dispensationMappings.add(map);
        Dispensation dispensation = new Dispensation(1L, 1L, 2L, 4L, "20 pills", "test", 20L, 12L, "Dispensed", DispensationStatus.ISSUED);
        when(productDispensationMapRepository.findAllByDispensationIdIn(anyCollection())).thenReturn(dispensationMappings);
        when(dispensationRepository.getOne(any())).thenReturn(dispensation);
        when(dispensationService.createReturnDispensationWithInventoryData(any(), any())).thenReturn(expectedProductInventoryLogIds);
        mockMvc
                .perform(post("/v2/dispensation/return")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(request))
                        .header(X_DS_CLIENT_ID, Long.toString(1L)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("number of units is required"));
    }

    @Test
    public void test_return_dispensation_inventory_null_reason_for_return () throws Exception {
        List<ReturnDispensationRequest.ReturnDispensationData> dispensationDataList = new ArrayList<>();
        ReturnDispensationRequest.ReturnDispensationData dispensationData = new ReturnDispensationRequest.ReturnDispensationData(1L, 2L, null, 1L, "ABC123", "01-09-2025 00:00:00", 1L);
        dispensationDataList.add(dispensationData);
        ReturnDispensationRequest request = new ReturnDispensationRequest(1L, dispensationDataList);

        ProductInventoryLog dispensationLog = new ProductInventoryLog(TransactionType.RETURNED, 1L, 2L, CommentType.REASON_FOR_RETURN, "Died", 1L, 1L,1L,TransactionIdType.INTERNAL);
        List<ProductInventoryLog> expectedProductInventoryLogs = new ArrayList<>();
        expectedProductInventoryLogs.add(dispensationLog);
        List<Long> expectedProductInventoryLogIds = expectedProductInventoryLogs
                .stream()
                .map(ProductInventoryLog::getId)
                .collect(Collectors.toList());
        ProductDispensationMapping map = new ProductDispensationMapping(1L, 1L, 1L, 1L, 10L, "test");
        List<ProductDispensationMapping> dispensationMappings = new ArrayList<>();
        dispensationMappings.add(map);
        Dispensation dispensation = new Dispensation(1L, 1L, 2L, 4L, "20 pills", "test", 20L, 12L, "Dispensed", DispensationStatus.ISSUED);
        when(productDispensationMapRepository.findAllByDispensationIdIn(anyCollection())).thenReturn(dispensationMappings);
        when(dispensationRepository.getOne(any())).thenReturn(dispensation);
        when(dispensationService.createReturnDispensationWithInventoryData(any(), any())).thenReturn(expectedProductInventoryLogIds);
        mockMvc
                .perform(post("/v2/dispensation/return")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(request))
                        .header(X_DS_CLIENT_ID, Long.toString(1L)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("reason for return is required"));
    }

    @Test
    public void test_return_dispensation_inventory_invalid_expiry_date_format () throws Exception {
        List<ReturnDispensationRequest.ReturnDispensationData> dispensationDataList = new ArrayList<>();
        ReturnDispensationRequest.ReturnDispensationData dispensationData = new ReturnDispensationRequest.ReturnDispensationData(1L, 2L, "Wrong dispensation", 1L, "ABC123", "01-09-2025 00-00-00", 1L);
        dispensationDataList.add(dispensationData);
        ReturnDispensationRequest request = new ReturnDispensationRequest(1L, dispensationDataList);

        ProductInventoryLog dispensationLog = new ProductInventoryLog(TransactionType.RETURNED, 1L, 2L, CommentType.REASON_FOR_RETURN, "Died", 1L, 1L,1L,TransactionIdType.INTERNAL);
        List<ProductInventoryLog> expectedProductInventoryLogs = new ArrayList<>();
        expectedProductInventoryLogs.add(dispensationLog);
        List<Long> expectedProductInventoryLogIds = expectedProductInventoryLogs
                .stream()
                .map(ProductInventoryLog::getId)
                .collect(Collectors.toList());
        ProductDispensationMapping map = new ProductDispensationMapping(1L, 1L, 1L, 1L, 10L, "test");
        List<ProductDispensationMapping> dispensationMappings = new ArrayList<>();
        dispensationMappings.add(map);
        Dispensation dispensation = new Dispensation(1L, 1L, 2L, 4L, "20 pills", "test", 20L, 12L, "Dispensed", DispensationStatus.ISSUED);
        when(productDispensationMapRepository.findAllByDispensationIdIn(anyCollection())).thenReturn(dispensationMappings);
        when(dispensationRepository.getOne(any())).thenReturn(dispensation);
        when(dispensationService.createReturnDispensationWithInventoryData(any(), any())).thenReturn(expectedProductInventoryLogIds);
        mockMvc
                .perform(post("/v2/dispensation/return")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(request))
                        .header(X_DS_CLIENT_ID, Long.toString(1L)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("expiry date format is not valid"));
    }

    @Test
    public void testDispensationEntityData() throws Exception {
        Dispensation dispensation = new Dispensation();
        dispensation.setEntityId(12345L);
        EntityDispensationResponse entityDispensationResponse = new EntityDispensationResponse(12345L, null, 2L, new ArrayList<>());
        when(dispensationService.getDispensation(eq(123L), eq(29L))).thenReturn(dispensation);
        when(dispensationService.getAllDispensationDataForEntity(eq(12345L), eq(29L), eq(true))).thenReturn(entityDispensationResponse);

        mockMvc
                .perform(get("/v1/dispensation/123/entity")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(X_DS_CLIENT_ID, Long.toString(29L)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.entityId").value(12345L));
    }

}

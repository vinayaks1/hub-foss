package com.everwell.dispensationservice.unit.controllers;

import com.everwell.dispensationservice.controllers.ClientController;
import com.everwell.dispensationservice.exceptions.CustomExceptionHandler;
import com.everwell.dispensationservice.models.http.requests.RegisterClientRequest;
import com.everwell.dispensationservice.models.http.responses.ClientResponse;
import com.everwell.dispensationservice.services.ClientService;
import com.everwell.dispensationservice.unit.BaseTestNoSpring;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static com.everwell.dispensationservice.Utils.Utils.asJsonString;
import static com.everwell.dispensationservice.constants.Constants.X_DS_CLIENT_ID;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ClientControllerTest extends BaseTestNoSpring {

    private MockMvc mockMvc;

    @Spy
    @InjectMocks
    private ClientController clientController;

    @Mock
    private ClientService clientService;

    @Mock
    private ApplicationEventPublisher applicationEventPublisher;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(clientController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void test_register_client_success() throws Exception {
        RegisterClientRequest clientRequest = new RegisterClientRequest("TestClient", "Test_Client");
        ClientResponse clientResponse = new ClientResponse(1L, "TestClient");
        when(clientService.registerClient(any())).thenReturn(clientResponse);
        mockMvc
                .perform(post("/v1/client")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(clientRequest)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.message").value("client registered successfully"))
                .andExpect(jsonPath("$.data.id").value(clientResponse.getId()))
                .andExpect(jsonPath("$.data.name").value(clientResponse.getName()));
    }

    @Test
    public void test_register_client_throws_validation_bad_request_exception() throws Exception {
        RegisterClientRequest clientRequest = new RegisterClientRequest("", "");
        ClientResponse clientResponse = new ClientResponse(1L, "TestClient");
        when(clientService.registerClient(any())).thenReturn(clientResponse);
        mockMvc
                .perform(post("/v1/client")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(clientRequest)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("name and password cannot be null or empty"));
    }

    @Test
    public void test_get_client_success() throws Exception {
        long clientId = 1L;
        ClientResponse clientResponse = new ClientResponse(1L, "TestClient", "authToken");
        when(clientService.getClientWithNewToken(any())).thenReturn(clientResponse);
        mockMvc
                .perform(get("/v1/client")
                        .header(X_DS_CLIENT_ID, Long.toString(clientId)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message").value("client details fetched successfully"))
                .andExpect(jsonPath("$.data.id").value(clientResponse.getId()))
                .andExpect(jsonPath("$.data.name").value(clientResponse.getName()));
    }
}

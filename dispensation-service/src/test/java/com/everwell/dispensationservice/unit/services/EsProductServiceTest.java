package com.everwell.dispensationservice.unit.services;

import com.everwell.dispensationservice.elasticsearch.data.ESProductRepository;
import com.everwell.dispensationservice.elasticsearch.dto.ProductSearchResult;
import com.everwell.dispensationservice.elasticsearch.indices.ProductIndex;
import com.everwell.dispensationservice.elasticsearch.service.ESQueryBuilder;
import com.everwell.dispensationservice.elasticsearch.service.impl.ESProductServiceImpl;
import com.everwell.dispensationservice.models.http.requests.ProductSearchRequest;
import com.everwell.dispensationservice.unit.BaseTestNoSpring;
import org.elasticsearch.index.query.QueryBuilders;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHitsImpl;
import org.springframework.data.elasticsearch.core.TotalHitsRelation;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class EsProductServiceTest extends BaseTestNoSpring {
    @Spy
    @InjectMocks
    ESProductServiceImpl esProductService;

    @Mock
    ESProductRepository esProductRepository;

    @Mock
    ESQueryBuilder esQueryBuilder;

    @Mock
    ElasticsearchOperations elasticsearchOperations;


    private ProductIndex getProductIndex() {
        return new ProductIndex("1", "ET", "vitamin", "pill", "pill", "vendor A", "test", "category");
    }

    private ProductSearchResult getProductSearchResult() {
        return new ProductSearchResult(1L, Collections.singletonList(getProductIndex()));
    }

    @Test
    public void saveTest() {
        ProductIndex productIndex = getProductIndex();
        esProductService.save(productIndex);
        verify(esProductRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void saveAllTest() {
        ProductIndex productIndex = getProductIndex();
        esProductService.saveAll(Collections.singletonList(productIndex));
        verify(esProductRepository, Mockito.times(1)).saveAll(any());
    }

    @Test
    public void searchProductsTest() {
        ProductSearchRequest productSearchRequest = new ProductSearchRequest(null, null, "test");
        ProductSearchResult productSearchResult = getProductSearchResult();
        doReturn(QueryBuilders.boolQuery()).when(esQueryBuilder).build(any());
        doReturn(productSearchResult).when(esProductService).performSearch(any());

        ProductSearchResult productSearchResultResponse = esProductService.searchProducts(productSearchRequest);
        assertEquals(productSearchResult.getProductIndexList().size(), productSearchResultResponse.getProductIndexList().size());
        assertEquals(productSearchResult.getTotalHits(), productSearchResultResponse.getTotalHits());
        verify(esQueryBuilder, Mockito.times(1)).build(any());
        verify(esProductService, Mockito.times(1)).performSearch(any());
    }

    @Test
    public void performSearchTest() {
        List<SearchHit<ProductIndex>> searchHits = new ArrayList<>();
        searchHits.add(new SearchHit<>(
                "1", "1", "", 1f, new Object[1], new HashMap<>(),
               getProductIndex()
        ));
        NativeSearchQuery nativeSearchQuery = new NativeSearchQueryBuilder().build();
        when(elasticsearchOperations.search(eq(nativeSearchQuery), eq(ProductIndex.class))).thenReturn(
                new SearchHitsImpl<>(1, TotalHitsRelation.EQUAL_TO, 12f, "12", searchHits, null, null)
        );

        ProductSearchResult productSearchResult = esProductService.performSearch(nativeSearchQuery);

        assertEquals(1L, productSearchResult.getTotalHits());
        assertEquals(1L, productSearchResult.getProductIndexList().size());
        verify(elasticsearchOperations, Mockito.times(1)).search(eq(nativeSearchQuery), eq(ProductIndex.class));
    }

}

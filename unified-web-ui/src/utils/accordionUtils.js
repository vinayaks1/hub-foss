export const accordionState = Object.freeze({
  Collapsed: 0,
  Expanded: 1
})

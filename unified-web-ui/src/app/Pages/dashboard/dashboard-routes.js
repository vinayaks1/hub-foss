import { tasklistChildren } from '../../../constants/index.js'
const ReviewVot = () => import('./ReviewVot/ReviewVot.vue')
const Overview = () => import('./Overview/Overview.vue')
const Dashboard = () => import('./Dashboard.vue')
const Demo = () => import('@/app/Pages/dashboard/patient/Demo.vue')
const Patient = () => import('./patient/Patient.vue')
const Adherence = () => import('./patient/Tabs/Adherence/App.vue')
const SupportActions = () => import('./patient/components/SupportActions.vue')
const UnifiedPatient = () => import('./UnifiedPatient/UnifiedPatient.vue')
const Registration = () => import('./Registration/Registration.vue')
const Notes = () => import('./patient/components/Notes.vue')
const CallLogs = () => import('./patient/components/CallLogs.vue')
const MERM = () => import('./patient/components/MERM.vue')
const Evrimed = () => import('./components/Evrimed.vue')
const Dispensation = () => import('./patient/components/Dispensation.vue')
const StaffMapping = () => import('./patient/components/StaffMapping.vue')
const CloseCase = () => import('./patient/components/CloseCase.vue')
const Comorbidity = () => import('./patient/components/Comorbidity.vue')
const TreatmentCenterTab = () => import('./patient/components/TreatmentCenterTab.vue')
const Engagement = () => import('./patient/components/Engagement.vue')
const DeletePatient = () => import('./patient/components/DeletePatient.vue')
const BasicDetails = () => import('./patient/components/BasicDetails.vue')
const Tasklists = () => import('./tasklist/Tasklists.vue')
const StaffDetails = () => import('./StaffDetails/StaffDetails.vue')
const AddStaffDetails = () => import('./StaffDetails/AddStaffDetails.vue')
const HierachyManagement = () => import('./HierarchyManagement/HierarchyManagement.vue')
const HierarchyDetails = () => import('@/app/Pages/dashboard/HierarchyManagement/components/HierarchyDetails.vue')
const LoginsManagement = () => import('@/app/Pages/dashboard/HierarchyManagement/components/LoginsManagement.vue')
const Communications = () => import('@/app/Pages/dashboard/HierarchyManagement/components/Communications.vue')
const Reports = () => import('./Reports/Reports.vue')
const RegimenTemplate = () => import('./RegimenTemplate/RegimenTemplate.vue')

const dashboardRoutes = [
  {
    path: '/dashboard',
    name: 'dashboard',
    component: Dashboard,
    children: [
      {
        path: 'reviewvot',
        name: 'Review VOT',
        component: ReviewVot
      },
      {
        path: 'demo',
        name: 'demo',
        component: Demo
      },
      {
        path: 'overview',
        name: 'Overview',
        component: Overview
      },
      {
        path: 'UnifiedPatient',
        name: 'UnifiedPatient',
        component: UnifiedPatient
      },
      {
        path: 'evrimed',
        name: 'Evrimed',
        component: Evrimed
      },
      {
        path: 'patient/:patientId',
        name: 'patient',
        component: Patient,
        props: true,
        children: [
          {
            path: 'ADHERENCE',
            name: 'adherence',
            component: Adherence
          },
          {
            path: 'SUPPORT_ACTIONS',
            name: 'support_actions',
            component: SupportActions
          },
          {
            path: 'STAFF_MAPPING',
            name: 'staff_mapping',
            component: StaffMapping
          },
          {
            path: 'TAGS_AND_NOTES',
            name: 'tags_and_notes',
            component: Notes,
            props: true
          },
          {
            path: 'NOTES',
            name: 'notes',
            component: Notes,
            props: true
          },
          {
            path: 'CALL_LOGS',
            name: 'call_logs',
            component: CallLogs,
            props: true
          },
          {
            path: 'MERM',
            name: 'merm',
            component: MERM,
            props: true
          },
          {
            path: 'CLOSE_CASE',
            name: 'close_case',
            component: CloseCase,
            props: true
          },
          {
            path: 'DISPENSATION',
            name: 'dispensation',
            component: Dispensation
          },
          {
            path: 'BASIC_DETAILS',
            name: 'basic_details',
            component: BasicDetails
          },
          {
            path: 'COMORBIDITY',
            name: 'comorbidity',
            component: Comorbidity
          },
          {
            path: 'TREATMENT_CENTERS',
            name: 'treatment_centers',
            component: TreatmentCenterTab
          },
          {
            path: 'ENGAGEMENT',
            name: 'engagement',
            component: Engagement,
            props: true
          },
          {
            path: 'DELETE_PATIENT',
            name: 'delete_patient',
            component: DeletePatient
          },
          {
            path: 'VOT_REVIEW',
            name: 'vot_review',
            component: ReviewVot,
            props: true
          }
        ]
      },
      {
        path: 'enrollment',
        name: 'enrollment',
        component: Registration
      },
      {
        path: 'tasklists',
        name: 'tasklists',
        component: Tasklists,
        children: tasklistChildren
      },
      {
        path: 'StaffDetails',
        name: 'StaffDetails',
        component: StaffDetails
      },
      {
        path: 'StaffDetails/:hierarchyId',
        name: 'StaffDetails',
        component: StaffDetails
      },
      {
        path: 'AddStaff/:hierarchyId',
        name: 'AddStaff',
        component: AddStaffDetails
      },
      {
        path: 'UpdateStaff/:staffId',
        name: 'UpdateStaff',
        component: AddStaffDetails
      },
      {
        path: 'reports',
        name: 'reports',
        component: Reports
      },
      {
        path: 'hierarchymanagement',
        name: 'hierarchymanagement',
        component: HierachyManagement,
        props: true,
        children: [
          {
            path: 'hierarchy_details/:hierarchyId',
            name: 'hierarchy_details',
            component: HierarchyDetails
          },
          {
            path: 'logins/:hierarchyId',
            name: 'logins',
            component: LoginsManagement
          },
          {
            path: 'communication/:hierarchyId',
            name: 'communication',
            component: Communications
          }
        ]
      },
      {
        path: 'dispensation/template',
        name: 'RegimenTemplate',
        component: RegimenTemplate
      }
    ]
  }
]

export default dashboardRoutes

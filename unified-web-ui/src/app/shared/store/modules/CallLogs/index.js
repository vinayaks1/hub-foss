import { ApiServerClient, errorCallback } from '../../Api'
export default {
  namespaced: true,
  actions: {
    async getPatientCallLogs ({ commit, state }, patientId) {
      const url = 'api/Patients/GetPatientCallLogs?id=' + patientId
      const response = await ApiServerClient.get(url, null, errorCallback)
      if (response.success) {
        return response.data
      } else {
        // toastError('Unable to load  Details')
      }
    }
  }
}

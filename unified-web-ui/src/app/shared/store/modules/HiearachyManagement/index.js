import { ApiServerClient, errorCallback } from '../../Api'
import { defaultToast, ToastType } from '@/utils/toastUtils'
import i18n from '@/plugins/i18n'
export default {
  namespaced: true,
  state: {
    treeRef: null,
    selectedHierarchyNode: null,
    allHierarchyResponse: [],
    hierarchyDetails: null,
    hierarchyBasicDetails: null,
    parentMonitoringMethods: {},
    availableMonitoringMethods: {},
    availableMonitoringMethodsAsOptions: [],
    selectedMonitoringMethods: [],
    basicDetailsEditing: false,
    addNewHierarchConfig: {},
    loginDetails: [],
    typeOfPatientsOptions: [],
    accessTypeOptions: [],
    selectedHierarchyCommunicationConfig: {},
    selectedLanguages: [],
    showLoader: false,
    selectedHierarchyId: null
  },
  mutations: {
    UPDATE_PASSWORD_RESPONSE (state, payload) {

    },
    TOGGLE_LOADER (state) {
      state.showLoader = !state.showLoader
    },
    UPDATE_COMMUNICATION_FOR_SELECTED_HIERARCHY (state, payload) {
    },
    COMMUNICATION_CONFIG_FOR_SELECTED_HIERARCHY (state, payload) {
      state.selectedHierarchyCommunicationConfig = payload
      state.selectedLanguages = payload.selectedLanguages
    },
    CREATE_NEW_LOGIN_RESPONSE (state, payload) {
    },
    GET_NEW_LOGIN_CONFIG (state, payload) {
      state.typeOfPatientsOptions = payload.responseTypeOfPatients
      state.accessTypeOptions = payload.responseAccessType
    },
    ADD_NEW_HIERARCHY (state, payload) {
    },
    ADD_HEW_HIERARCHY_CONFIG (state, payload) {
      state.addNewHierarchConfig = { ...payload }
    },
    UPDATE_TREE_REF (state, payload) {
      state.treeRef = payload
    },
    SELECT_NODE (state, payload) {
      state.selectedHierarchyNode = payload
      state.basicDetailsEditing = false
    },
    TOGGLE_BASIC_DETAILS_EDIT (state) {
      state.basicDetailsEditing = !state.basicDetailsEditing
    },
    GET_ALL_HIERARCHIES (state, payload) {
      state.allHierarchyResponse = payload
    },
    GET_HIERARCHY_DETAILS (state, payload) {
      state.hierarchyDetails = payload
      state.hierarchyBasicDetails = JSON.parse(payload.basicDetails)
      state.loginDetails = payload.loginDetails
      state.selectedHierarchyId = state.hierarchyBasicDetails.id
    },
    GET_PARENT_MONITORING_METHODS (state, payload) {
      state.parentMonitoringMethods = (payload !== null ? JSON.parse(payload) : {})
    },
    UPDATE_BASIC_DETAILS (state, payload) {
      state.hierarchyBasicDetails.name = payload.data.name
      state.hierarchyBasicDetails.code = payload.data.code
      state.hierarchyBasicDetails.hasMERM = payload.data.hasMERM
      state.hierarchyBasicDetails.hasVOT = payload.data.hasVOT
      state.hierarchyBasicDetails.has99DOTS = payload.data.has99DOTS
      state.hierarchyBasicDetails.has99DOTSLite = payload.data.has99DOTSLite
      state.hierarchyBasicDetails.hasNONE = payload.data.hasNONE
      state.basicDetailsEditing = false
    },
    UPDATE_AVAILABLE_MONITORING_METHODS_AND_SELETED_METHODS (state, hasParent) {
      const moniotoringMethods = {
        hasMERM: state.hierarchyBasicDetails.hasMERM === 'true',
        hasVOT: state.hierarchyBasicDetails.hasVOT === 'true',
        has99DOTS: state.hierarchyBasicDetails.has99DOTS === 'true',
        has99DOTSLite: state.hierarchyBasicDetails.has99DOTSLite === 'true',
        hasNONE: state.hierarchyBasicDetails.hasNONE === 'true'
      }
      if (hasParent) {
        Object.entries(state.parentMonitoringMethods).forEach((item) => {
          const key = item[0]
          const value = item[1]
          if (value) {
            if (!(key in moniotoringMethods)) {
              moniotoringMethods[key] = false
            }
          } else {
            if ((key in moniotoringMethods)) {
              delete moniotoringMethods[key]
            }
          }
        })
      }
      state.selectedMonitoringMethods = []
      state.availableMonitoringMethodsAsOptions = []
      const selectedMonitoringMethods = []
      const labelsMap = {
        hasMERM: 'allow_MERM_registration',
        hasVOT: 'allow_VOT',
        has99DOTS: 'allow_99dots',
        has99DOTSLite: 'allow_99dotsLite',
        hasNONE: 'allow_follow_up_without_tech'
      }
      for (const [key, value] of Object.entries(moniotoringMethods)) {
        state.availableMonitoringMethodsAsOptions.push({ Key: key, Value: labelsMap[key] })
        if (value) {
          selectedMonitoringMethods.push(key)
        }
      }
      state.selectedMonitoringMethods = [...selectedMonitoringMethods]
    }
  },
  actions: {
    async updateTreeRef ({ dispatch, commit }, treeRef) {
      commit('UPDATE_TREE_REF', treeRef)
    },
    async getAllHierarchies ({ dispatch, commit }) {
      try {
        const url = 'api/Hierarchy/GetHierarchies'
        const response = await ApiServerClient.get(url, {}, errorCallback)
        commit('GET_ALL_HIERARCHIES', response)
        return response
      } catch (ex) {
        if (ex.code === 429) {
          await dispatch('getAllHierarchies')
        }
      }
    },
    async getHierarchyDetails ({ dispatch, commit }, { hierarchyId }) {
      try {
        const url = 'api/Admin/HierarchyDetailsJSON/' + hierarchyId
        const response = await ApiServerClient.get(url, {}, errorCallback)
        // eslint-disable-next-line eqeqeq
        if (response.success) {
          commit('GET_HIERARCHY_DETAILS', response.result)
        } else {
          commit('GET_HIERARCHY_DETAILS', null)
          errorCallback('' + response.success)
        }
      } catch (ex) {
        if (ex.code === 429) {
          await dispatch('getHierarchyDetails')
        }
      }
    },
    async getParentMonitoringMethods ({ dispatch, commit }, { params }) {
      try {
        const url = 'api/Admin/ParentMonitoringMethods'
        if (params.parentId !== null) {
          const response = await ApiServerClient.get(url, params, errorCallback)
          // eslint-disable-next-line eqeqeq
          if (response.success) {
            commit('GET_PARENT_MONITORING_METHODS', response.result)
            commit('UPDATE_AVAILABLE_MONITORING_METHODS_AND_SELETED_METHODS', true)
          } else {
            errorCallback('ERROR: Parent Monitoring Methods API responded with status ' + response.success)
            commit('GET_PARENT_MONITORING_METHODS', null)
            commit('UPDATE_AVAILABLE_MONITORING_METHODS_AND_SELETED_METHODS', false)
          }
        } else {
          commit('GET_PARENT_MONITORING_METHODS', null)
          commit('UPDATE_AVAILABLE_MONITORING_METHODS_AND_SELETED_METHODS', false)
        }
      } catch (ex) {
        if (ex.code === 429) {
          await dispatch('getParentMonitoringMethods')
        }
      }
    },
    async updateBasicDetails ({ dispatch, commit }, { data }) {
      try {
        const url = 'api/Hierarchy/UpdateHierarchy'
        const response = await ApiServerClient.put(url, data, errorCallback)
        if (response && response.Success) {
          commit('UPDATE_BASIC_DETAILS', { result: response.result, data: data })
          await dispatch('getAllHierarchies')
          defaultToast(ToastType.Success, i18n.t('update_was_successful'))
        } else if (response !== undefined && response !== null && response.message !== undefined) {
          errorCallback(response.message)
        }
        // eslint-disable-next-line eqeqeq
      } catch (ex) {
        if (ex.code === 429) {
          await dispatch('updateBasicDetails')
        }
      }
    },
    toggleBasicDetailsEdit ({ dispatch, commit }) {
      commit('TOGGLE_BASIC_DETAILS_EDIT')
    },
    selectNode ({ dispatch, commit }, { node }) {
      commit('SELECT_NODE', node)
    },
    async getAddNewHierarchyConfig ({ dispatch, commit }, { params }) {
      try {
        const urlForType = 'api/Admin/HierarchyConfigValues'
        const responseForType = await ApiServerClient.get(urlForType, params, errorCallback)
        const urlForParentMonitoringMethods = 'api/Admin/ParentMonitoringMethods'
        const responseForParentMonitoringMethods = await ApiServerClient.get(urlForParentMonitoringMethods, { parentId: params.id }, errorCallback)
        commit('ADD_HEW_HIERARCHY_CONFIG', { responseForParentMonitoringMethods: JSON.parse(responseForParentMonitoringMethods.result), responseForType: responseForType })
      } catch (ex) {
        if (ex.code === 429) {
          await dispatch('getAddNewHierarchyConfig')
        }
      }
    },
    async addNewHierarchy ({ dispatch, commit }, { data }) {
      try {
        const url = 'api/Hierarchy/CreateHierarchy'
        const response = await ApiServerClient.post(url, data, errorCallback)
        if (response.success) {
          commit('ADD_NEW_HIERARCHY', response)
          defaultToast(ToastType.Success, i18n.t('add_hierarchy_success'))
          await dispatch('getAllHierarchies')
        } else {
          errorCallback(response.Error.Message)
        }
      } catch (ex) {
        if (ex.code === 429) {
          await dispatch('addNewHierarchy')
        }
      }
    },
    async getNewLoginConfig ({ dispatch, commit }, { params }) {
      try {
        const url = 'api/Admin/HierarchyConfigValues'
        const configTypeOfPatients = { configName: 'TypeOfPatient' }
        const configAccessType = { configName: 'AccessType' }
        const responseTypeOfPatients = await ApiServerClient.get(url, { ...params, ...configTypeOfPatients }, errorCallback)
        const responseAccessType = await ApiServerClient.get(url, { ...params, ...configAccessType }, errorCallback)
        commit('GET_NEW_LOGIN_CONFIG', { responseTypeOfPatients: responseTypeOfPatients, responseAccessType: responseAccessType })
      } catch (ex) {
        if (ex.code === 429) {
          await dispatch('getNewLoginConfig')
        }
      }
    },
    async createNewLoginInHierarchy ({ dispatch, commit }, { data }) {
      try {
        const url = 'Api/Account/CreateLogin/'
        const response = await ApiServerClient.post(url, data, errorCallback)
        if (response.success) {
          commit('CREATE_NEW_LOGIN_RESPONSE', response)
          defaultToast(ToastType.Success, i18n.t('create_login_success'))
          return response
        } else {
          errorCallback(response.result)
        }
      } catch (ex) {
        if (ex.code === 429) {
          await dispatch('createNewLogin')
        }
      }
    },
    async updatePasswordForLogin ({ dispatch, commit }, { data }) {
      try {
        const url = 'Api/Account/ResetPasswordByAdmin'
        const response = await ApiServerClient.post(url, data, errorCallback)
        if (response.Success) {
          commit('UPDATE_PASSWORD_RESPONSE', response)
          defaultToast(ToastType.Success, i18n.t('update_was_successful'))
          return response
        } else {
          errorCallback(response.Error.Message)
        }
      } catch (ex) {
        if (ex.code === 429) {
          await dispatch('updatePasswordForLogin')
        }
      }
    },
    async getCommunicationConfigForHierarchy ({ dispatch, commit }, { params }) {
      try {
        const url = 'api/Admin/CommunicationConfig'
        const response = await ApiServerClient.get(url, params, errorCallback)
        commit('COMMUNICATION_CONFIG_FOR_SELECTED_HIERARCHY', response.data)
      } catch (ex) {
        if (ex.code === 429) {
          await dispatch('getCommunicationConfigForHierarchy')
        }
      }
    },
    async updateCommunicationForHierarchy ({ dispatch, commit }, { data }) {
      try {
        const url = 'api/Admin/UpdateCommunicationConfig'
        const response = await ApiServerClient.post(url, data, errorCallback)
        if (response.success) {
          commit('UPDATE_COMMUNICATION_FOR_SELECTED_HIERARCHY', response.data)
          defaultToast(ToastType.Success, i18n.t('update_was_successful'))
        } else {
          errorCallback(response.Error)
        }
      } catch (ex) {
        if (ex.code === 429) {
          await dispatch('updateCommunicationForHierarchy')
        }
      }
    },
    toggleLoader ({ dispatch, commit }) {
      commit('TOGGLE_LOADER')
    }
  }
}

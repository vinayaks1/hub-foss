import { ApiServerClient, errorCallback } from '../../Api'
import { RegistryServerClient, registryErrorCallback } from '../../../store/RegistryApi.js'
import { toastError } from '@/utils/utils'

export default {
  namespaced: true,
  state: {
    isSliderEnabled: null,
    isStatsEnabled: null,
    isChartsEnabled: null,
    stats: [],
    charts: [],
    sliderImages: [],
    typesOfStats: []
  },
  mutations: {
    SET_STATS (state, data) {
      state.stats = data
    },
    SET_CHARTS_TO_BE_DISPLAYED (state, data) {
      state.charts = data
    },
    SET_SLIDER_IMAGES (state, data) {
      state.sliderImages = data.sort()
    },
    UPDATE_OVERVIEW_CONFIG (state, data) {
      state.isSliderEnabled = data.overviewConfig.slider
      state.isStatsEnabled = data.overviewConfig.overallStats
      state.isChartsEnabled = data.overviewConfig.charts
      state.typesOfStats = data.overviewConfigTypes.typesOfStats
    }
  },
  actions: {
    async updateOverviewConfig ({ commit }) {
      var url = '/v1/overview/config'
      try {
        const response = await RegistryServerClient.get(url, null, registryErrorCallback)
        if (response.success) {
          return commit('UPDATE_OVERVIEW_CONFIG', response.data)
        } else {
          // eslint-disable-next-line no-undef
          toastError('Unable to load Overview config')
        }
      } catch (e) {
        registryErrorCallback(e)
      }
    },
    async loadStats ({ commit, state }) {
      if (state.isStatsEnabled) {
        var url = '/api/Overview/OverallStats'
        try {
          const response = await ApiServerClient.get(url, null, errorCallback)
          if (response.success) {
            return commit('SET_STATS', response.data)
          } else {
            // eslint-disable-next-line no-undef
            toastError('Unable to load Stats')
          }
        } catch (e) {
          errorCallback(e)
        }
      }
    },
    async loadOverviewStats ({ commit, state }, typeOfStat) {
      var url = '/api/Overview/' + typeOfStat
      try {
        const response = await ApiServerClient.get(url, null, errorCallback)
        if (response.success) {
          return response.data
        } else {
          // eslint-disable-next-line no-undef
          toastError('Unable to load Stats')
        }
      } catch (e) {
        errorCallback(e)
      }
    },
    async loadChart ({ commit, state }, url) {
      if (state.isChartsEnabled) {
        try {
          const response = await ApiServerClient.get(url, null, errorCallback)
          if (response.success) {
            return response.data
          } else {
            // eslint-disable-next-line no-undef
            toastError('Unable to load Chart Data')
          }
        } catch (e) {
          errorCallback(e)
        }
      }
    },
    async loadChartsToBeDisplayed ({ commit, state }) {
      if (state.isChartsEnabled) {
        const url = '/api/Overview/ChartConfig'
        try {
          const response = await ApiServerClient.get(url, null, errorCallback)
          if (response.success) {
            commit('SET_CHARTS_TO_BE_DISPLAYED', response.data)
          } else {
            // eslint-disable-next-line no-undef
            toastError('Unable to load Charts')
          }
        } catch (e) {
          errorCallback(e)
        }
      }
    },
    async loadSliderImages ({ commit, state }) {
      if (state.isSliderEnabled) {
        const url = '/api/Overview/SliderImages'
        try {
          const response = await ApiServerClient.get(url, null, errorCallback)
          if (response.success) {
            return commit('SET_SLIDER_IMAGES', response.data)
          } else {
            // eslint-disable-next-line no-undef
            toastError('Unable to load Slider images')
          }
        } catch (e) {
          errorCallback(e)
        }
      }
    }
  }
}

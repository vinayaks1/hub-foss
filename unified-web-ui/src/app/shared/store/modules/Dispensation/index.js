import { ApiServerClient, errorCallback } from '../../Api'
import { defaultToast, ToastType } from '../../../../../utils/toastUtils'

export default {
  namespaced: true,
  actions: {
    async getDispensationTabPermission ({ commit, state }, patientId) {
      const url = '/api/Patients/FormParentConfig?formName=DispensationList&idName=PATIENT&id=' + patientId
      const response = await ApiServerClient.get(url, null, errorCallback)
      if (response.Success) {
        return response
      } else {
        defaultToast(ToastType.Error, 'Unable to load  Details')
      }
    },
    async removeDispensation ({ commit, state }, dispensationId) {
      const url = '/API/Dispensation/RemoveDispensation?dispensationId=' + dispensationId
      const response = await ApiServerClient.get(url, null, errorCallback)
      if (response.Success) {
        defaultToast(ToastType.Success, 'Deleted Successfully')
      } else {
        defaultToast(ToastType.Error, 'Unable to Delete')
      }
    },
    async patientPreviousData ({ commit, state }, requestData) {
      const url = '/api/Dispensation/PatientPreviousDispensationData?patientId=' + requestData.patientId
      const response = await ApiServerClient.post(url, requestData.data, errorCallback)
      if (response.Success) {
        return response
      } else {
        defaultToast(ToastType.Error, 'Unable to load  Previous Dispensation')
      }
    }
  }
}

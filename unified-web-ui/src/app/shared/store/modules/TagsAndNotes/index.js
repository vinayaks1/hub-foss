import { ApiServerClient, errorCallback } from '../../Api'

export default {
  namespaced: true,
  actions: {
    async getPatientNotes ({ commit, state }, patientId) {
      const url = 'api/Patients/GetPatientNotes?patientId=' + patientId
      const response = await ApiServerClient.get(url, null, errorCallback)
      if (response.success) {
        return response
      } else {
        // toastError('Unable to load  Details')
      }
    },

    async addTagsAndNote ({ commit, state }, inputData) {
      const url = 'api/Patients/AddTagsAndNote'
      const response = await ApiServerClient.post(url, { patientId: inputData.patientId, patientTags: inputData.patientTags, comment: inputData.comment }, errorCallback)
      if (response.success) {
        return true
      }
    }
  }
}

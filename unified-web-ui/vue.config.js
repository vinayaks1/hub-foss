// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
// const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin')
module.exports = {
  // chainWebpack: config => {
  //   config.plugin('VuetifyLoaderPlugin').use(new VuetifyLoaderPlugin()).tap(args => [{
  //     match (originalTag, { kebabTag, camelTag, path, component }) {
  //       if (kebabTag.startsWith('core-')) {
  //         return [camelTag, `import ${camelTag} from '@/components/core/${camelTag.substring(4)}.vue'`]
  //       }
  //     }
  //   }])
  // },
  configureWebpack: {
    // plugins: [new BundleAnalyzerPlugin()],
    // No need for splitting
    optimization: {
      splitChunks: {
        automaticNameDelimiter: '-'
      }
    }
  },

  css: {
    loaderOptions: {
      sass: {
        implementation: require('sass'),
        additionalData: `@import "@/scss/_${process.env.VUE_APP_THEME}variables.scss"`
      },
      scss: {
        additionalData: `@import "@/scss/_${process.env.VUE_APP_THEME}variables.scss";`
      }
    }
  },
  devServer: {
    // proxy: {
    //   '/nikshay': {
    //     target: process.env.VUE_APP_BASE_URL,
    //     pathRewrite: {
    //       '^/nikshay': ''
    //     },
    //     logLevel: 'debug',
    //     secure: false,
    //     changeOrigin: true
    //   },
    //   '/hub': {
    //     target: process.env.VUE_APP_BASE_URL,
    //     pathRewrite: {
    //       '^/hub': ''
    //     },
    //     logLevel: 'debug',
    //     secure: false,
    //     changeOrigin: true
    //   }
    // }
  },
  pwa: {
    name: 'Everwell-Unified-Frontend',
    workboxPluginMode: 'GenerateSW',
    workboxOptions: {
      skipWaiting: true
    },
    icons: [
      {
        src: process.env.VUE_ICON_PATH + '/icon-192.png',
        type: 'image/png',
        sizes: '192x192'
      },
      {
        src: process.env.VUE_ICON_PATH + '/icon-512.png',
        type: 'image/png',
        sizes: '512x512'
      }
    ]
  },

  transpileDependencies: [
    'vuetify'
  ]
}

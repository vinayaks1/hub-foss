import { shallowMount, createLocalVue, mount } from '@vue/test-utils'
import DatePicker from '@/app/shared/components/DatePicker.vue'
import Vue from 'vue'
import Vuex from 'vuex'
import FormStore from '@/app/shared/store/modules/form'
import flushPromises from 'flush-promises'
import Form from '@/app/shared/components/Form.vue'

const localVue = createLocalVue()
localVue.use(Vuex)
window.EventBus = new Vue()
const getStore = () => {
  return new Vuex.Store({
  })
}

const getStoreWithActions = (actions) => {
  const formattedActions = {
    ...FormStore.actions,
    ...actions
  }

  return new Vuex.Store({
    modules: {
      Form: {
        state: FormStore.state,
        actions: formattedActions,
        mutations: FormStore.mutations,
        namespaced: true
      }
    }
  })
}

const formWithOneSelect = {
  "Success": true,
  "Data": {
      "Form": {
          "Name": "SingleInputForm1",
          "Title": "_dummy_input_form",
          "Parts": [
              {
                  "FormName": "SingleInputForm1",
                  "Name": "input_form_01",
                  "Title": "",
                  "TitleKey": "input_form_01",
                  "Order": 1,
                  "IsVisible": true,
                  "Id": 119,
                  "Type": "vertical-form-part",
                  "Rows": null,
                  "Columns": null,
                  "RowDataName": "input_form_01",
                  "AllowRowOpen": false,
                  "AllowRowDelete": false,
                  "ListItemTitle": null,
                  "ItemDescriptionField": null,
                  "IsRepeatable": false,
                  "RecordId": null
              }
          ],
          "PartOptions": null,
          "Fields": [
              {
                  "Value": null,
                  "PartName": "input_form_01",
                  "Placeholder": null,
                  "Component": "app-datepicker",
                  "IsDisabled": false,
                  "IsVisible": true,
                  "LabelKey": "Validate_input__Required_01",
                  "Label": null,
                  "Name": "Validate_input__Required_01",
                  "IsHierarchySelector": false,
                  "IsRequired": false,
                  "RemoteUrl": null,
                  "Type": null,
                  "AddOn": null,
                  "Options": null,
                  "HierarchyOptions": null,
                  "OptionsWithKeyValue": null,
                  "AdditionalInfo": null,
                  "DefaultVisibilty": false,
                  "Order": 0,
                  "OptionsWithLabel": null,
                  "Validations": {
                      "Or": null,
                      "And": [
                      ]
                  },
                  "ResponseDataPath": null,
                  "OptionDisplayKeys": null,
                  "OptionValueKey": null,
                  "LoadImmediately": false,
                  "HierarchySelectionConfigs": null,
                  "DisabledDateConfig": null,
                  "RemoteUpdateConfig": null,
                  "RowNumber": 16,
                  "ColumnNumber": null,
                  "Id": 11407,
                  "ParentType": "PART",
                  "ParentId": 119,
                  "FieldOptions": null,
                  "ValidationList": "Required",
                  "DefaultValue": null,
                  "Key": "input_form_01Validate_input__Required_01",
                  "HasInfo": false,
                  "InfoText": null,
                  "Config": null,
                  "ColumnWidth": 6,
                  "HasToggleButton": false
              }
          ],
          "Triggers": null,
          "TriggerConfigs": null,
          "ValueDependencies": [],
          "FilterDependencies": [],
          "VisibilityDependencies": [],
          "DateConstraintDependencies": [],
          "IsRequiredDependencies": [],
          "RemoteUpdateConfigs": [],
          "ValuePropertyDependencies": [],
          "CompoundValueDependencies": null,
          "SaveEndpoint": "/api/patients",
          "SaveText": null,
          "SaveTextKey": "_submit"
      },
      "ExistingData": {}
  },
  "Error": null
} 

const getNewForm = () => {
  return JSON.parse(JSON.stringify(formWithOneSelect))
}
const dummyValidation = {
  "Type": "Dummy",
  "Max": 0,
  "Min": 0,
  "IsBackendValidation": false,
  "ValidationUrl": null,
  "ShowServerError": false,
  "ErrorTargetField": null,
  "ValidationParams": null,
  "ErrorMessage": "Dummy validation message",
  "ErrorMessageKey": "error_dummy",
  "RequiredOnlyWhen": null,
  "Regex": null
}
const getDummyValidation = () => {
  return [JSON.parse(JSON.stringify(dummyValidation))]
}

describe('DatePicker.vue', () => {
  it('renders label when passed', () => {
    const label = 'new label'
    const wrapper = shallowMount(DatePicker, {
      store: getStore(),
      localVue,
      propsData: { label },
      mocks: {
        $t: (key) => key
      }
    })
    const labels = wrapper.findAll('label')
    expect(labels.length.toString()).toMatch('1')
  })

  it('name to be set', () => {
    const name = 'Name'
    const wrapper = shallowMount(DatePicker, {
      store: getStore(),
      localVue,
      propsData: { name },
      mocks: {
        $t: (key) => key
      }
    })
    expect(wrapper.find('.input').element.name).toMatch(name)
  })

  it('placeholder to be set', () => {
    const placeholder = 'PlaceHolder'
    const wrapper = shallowMount(DatePicker, {
      store: getStore(),
      localVue,
      propsData: { placeholder },
      mocks: {
        $t: (key) => key
      }
    })
    expect(wrapper.find('.input').element.placeholder).toMatch(placeholder)
  })

  it('Default Placeholder', () => {
    const wrapper = shallowMount(DatePicker, {
      store: getStore(),
      localVue,
      propsData: { },
      mocks: {
        $t: (key) => key
      }
    })
    expect(wrapper.find('.input').element.placeholder).toMatch('Select Date')
  })

  it('calendar closed by default', async () => {
    const wrapper = shallowMount(DatePicker, {
      store: getStore(),
      localVue,
      propsData: { },
      mocks: {
        $t: (key) => key
      }
    })

    expect(wrapper.find('.calendar-container > div').classes().includes('show')).toBe(false)
  })

  it('calendar open on click', async () => {
    const wrapper = shallowMount(DatePicker, {
      store: getStore(),
      localVue,
      propsData: { },
      mocks: {
        $t: (key) => key
      }
    })

    await wrapper.find('.input').trigger('click')

    expect(wrapper.find('.calendar-container > div').classes().includes('show')).toBe(true)
  })

  it('calendar closed on click again', async () => {
    const wrapper = shallowMount(DatePicker, {
      store: getStore(),
      localVue,
      propsData: { },
      mocks: {
        $t: (key) => key
      }
    })

    await wrapper.find('.input').trigger('click')
    await wrapper.find('.input').trigger('click')

    expect(wrapper.find('.calendar-container > div').classes().includes('show')).toBe(false)
  })

  it('calendar not closed on clicking inside the calendar', async () => {
    const wrapper = shallowMount(DatePicker, {
      store: getStore(),
      localVue,
      propsData: { },
      mocks: {
        $t: (key) => key
      }
    })

    await wrapper.find('.input').trigger('click')
    await wrapper.find('.calendar-container').trigger('click')

    expect(wrapper.find('.calendar-container > div').classes().includes('show')).toBe(true)
  })

  it('calendar closed on clicking outside', async () => {
    const wrapper = shallowMount(DatePicker, {
      store: getStore(),
      localVue,
      propsData: { name: 'date' },
      mocks: {
        $t: (key) => key
      }
    })

    await wrapper.find('.input').trigger('click')
    await document.querySelector('body').click()

    expect(wrapper.find('.calendar-container > div').classes().includes('show')).toBe(false)
  })

  it('display current month by default in calendar', async () => {
    const wrapper = shallowMount(DatePicker, {
      store: getStore(),
      localVue,
      propsData: { },
      mocks: {
        $t: (key) => key
      }
    })

    await wrapper.find('.input').trigger('click')

    const date = new Date()

    expect(wrapper.findAll('.month-year-selector span').at(0).text()).toBe(date.toLocaleDateString('en-GB', { month: 'short' }).substring(0, 3))
    expect(parseInt(wrapper.findAll('.month-year-selector span').at(1).text())).toBe(date.getFullYear())
  })

  it('year selector being shown', async () => {
    const wrapper = shallowMount(DatePicker, {
      store: getStore(),
      localVue,
      propsData: { },
      mocks: {
        $t: (key) => key
      }
    })

    await wrapper.find('.input').trigger('click')
    await wrapper.find('.month-year-selector').trigger('click')

    expect(wrapper.find('.year-selector').exists()).toBe(true)
  })

  it('year selector being shown when value is passed', async () => {
    const wrapper = shallowMount(DatePicker, {
      store: getStore(),
      localVue,
      propsData: { value: new Date(1947, 7, 15).getTime() },
      mocks: {
        $t: (key) => key
      }
    })

    await wrapper.find('.input').trigger('click')
    await wrapper.find('.month-year-selector').trigger('click')

    expect(wrapper.find('.year-selector').exists()).toBe(true)
    expect(wrapper.findAll('.year-selector .year').at(11).text()).toBe('1947')
  })

  it('month selector being shown', async () => {
    const wrapper = shallowMount(DatePicker, {
      store: getStore(),
      localVue,
      propsData: { },
      mocks: {
        $t: (key) => key
      }
    })

    await wrapper.find('.input').trigger('click')
    await wrapper.find('.month-year-selector').trigger('click')
    await wrapper.findAll('.year-selector .year').at(0).trigger('click')

    expect(wrapper.find('.month-selector').exists()).toBe(true)
  })

  it('next click in date selector', async () => {
    const wrapper = shallowMount(DatePicker, {
      store: getStore(),
      localVue,
      propsData: { },
      mocks: {
        $t: (key) => key
      }
    })

    await wrapper.find('.input').trigger('click')

    const date = new Date()
    const nextMonthDate = new Date(date.getFullYear(), ((date.getMonth() + 1) % 12), 1)

    await wrapper.findAll('.datepicker-nav-icons').at(1).trigger('click')

    expect(wrapper.findAll('.month-year-selector span').at(0).text()).toBe(nextMonthDate.toLocaleDateString('en-GB', { month: 'short' }).substring(0, 3))
  })

  it('previous click in date selector', async () => {
    const wrapper = shallowMount(DatePicker, {
      store: getStore(),
      localVue,
      propsData: { },
      mocks: {
        $t: (key) => key
      }
    })

    await wrapper.find('.input').trigger('click')

    const date = new Date()
    const previousMonthDate = new Date(date.getFullYear(), ((date.getMonth() + 12 - 1) % 12), 1)

    await wrapper.findAll('.datepicker-nav-icons').at(0).trigger('click')

    expect(wrapper.findAll('.month-year-selector span').at(0).text()).toBe(previousMonthDate.toLocaleDateString('en-GB', { month: 'short' }).substring(0, 3))
  })

  it('handling year change for next click in date selector', async () => {
    const date = new Date(2020, 11, 1)
    const wrapper = shallowMount(DatePicker, {
      store: getStore(),
      localVue,
      propsData: { value: date.getTime() },
      mocks: {
        $t: (key) => key
      }
    })

    await wrapper.find('.input').trigger('click')

    await wrapper.findAll('.datepicker-nav-icons').at(1).trigger('click')

    expect(parseInt(wrapper.findAll('.month-year-selector span').at(1).text())).toBe(date.getFullYear() + 1)
    expect(wrapper.findAll('.month-year-selector span').at(0).text()).toBe('Jan')
  })

  it('handling year change for previous click in date selector', async () => {
    const date = new Date(2020, 0, 1)
    const wrapper = shallowMount(DatePicker, {
      store: getStore(),
      localVue,
      propsData: { value: date.getTime() },
      mocks: {
        $t: (key) => key
      }
    })

    await wrapper.find('.input').trigger('click')

    await wrapper.findAll('.datepicker-nav-icons').at(0).trigger('click')

    expect(parseInt(wrapper.findAll('.month-year-selector span').at(1).text())).toBe(date.getFullYear() - 1)
    expect(wrapper.findAll('.month-year-selector span').at(0).text()).toBe('Dec')
  })

  it('next click in year selector', async () => {
    const wrapper = shallowMount(DatePicker, {
      store: getStore(),
      localVue,
      propsData: { },
      mocks: {
        $t: (key) => key
      }
    })

    await wrapper.find('.input').trigger('click')
    await wrapper.find('.month-year-selector').trigger('click')

    const year1 = parseInt(wrapper.findAll('.year-selector .year').at(0).text())

    await wrapper.findAll('.datepicker-nav-icons').at(1).trigger('click')

    const year2 = parseInt(wrapper.findAll('.year-selector .year').at(0).text())

    expect(year2 - year1).toBe(15)
  })

  it('previous click in year selector', async () => {
    const wrapper = shallowMount(DatePicker, {
      store: getStore(),
      localVue,
      propsData: { },
      mocks: {
        $t: (key) => key
      }
    })

    await wrapper.find('.input').trigger('click')
    await wrapper.find('.month-year-selector').trigger('click')

    const year1 = parseInt(wrapper.findAll('.year-selector .year').at(0).text())

    await wrapper.findAll('.datepicker-nav-icons').at(0).trigger('click')

    const year2 = parseInt(wrapper.findAll('.year-selector .year').at(0).text())

    expect(year1 - year2).toBe(15)
  })

  it('date select functionality', async () => {
    const wrapper = shallowMount(DatePicker, {
      store: getStore(),
      localVue,
      propsData: { },
      mocks: {
        $t: (key) => key
      }
    })

    await wrapper.find('.input').trigger('click')

    const date = new Date()

    const expectedDateElement = wrapper.findAll('.date').at(20)
    await expectedDateElement.trigger('click')

    expect(wrapper.find('.input').element.value).toBe(expectedDateElement.text() + ' ' + date.toLocaleDateString('en-GB', { month: 'short' }).substring(0, 3) + ' ' + date.getFullYear())
  })

  it('month select functionality', async () => {
    const wrapper = shallowMount(DatePicker, {
      store: getStore(),
      localVue,
      propsData: { },
      mocks: {
        $t: (key) => key
      }
    })

    await wrapper.find('.input').trigger('click')
    await wrapper.find('.month-year-selector').trigger('click')
    const year = wrapper.findAll('.year-selector .year').at(0)
    await year.trigger('click')
    const month = wrapper.findAll('.month-selector .month').at(0)
    await month.trigger('click')

    expect(wrapper.findAll('.month-year-selector span').at(1).text()).toBe(year.text())
    expect(wrapper.findAll('.month-year-selector span').at(0).text()).toBe(month.text())
    expect(wrapper.find('.selected-month').text()).toBe(month.text())
    expect(wrapper.find('.month-selector').exists()).toBe(false)
  })

  it('passed date is displayed', async () => {
    const wrapper = shallowMount(DatePicker, {
      store: getStore(),
      localVue,
      propsData: { value: (new Date(2021, 0, 1)).getTime() },
      mocks: {
        $t: (key) => key
      }
    })

    expect(wrapper.find('.input').element.value).toBe('1 Jan 2021')
  })

  it('active date being highlighted', async () => {
    const wrapper = shallowMount(DatePicker, {
      store: getStore(),
      localVue,
      propsData: { value: (new Date(2021, 0, 1)).getTime() },
      mocks: {
        $t: (key) => key
      }
    })

    await wrapper.find('.input').trigger('click')

    expect(wrapper.findAll('.date-selector .date').at(5).classes().includes('active')).toBe(true)
  })

  it('active year being highlighted', async () => {
    const wrapper = shallowMount(DatePicker, {
      store: getStore(),
      localVue,
      propsData: { value: (new Date(2021, 0, 1)).getTime() },
      mocks: {
        $t: (key) => key
      }
    })

    await wrapper.find('.input').trigger('click')
    await wrapper.find('.month-year-selector').trigger('click')

    expect(wrapper.findAll('.year-selector .year').at(10).classes().includes('active')).toBe(true)
  })

  it('active month being highlighted', async () => {
    const wrapper = shallowMount(DatePicker, {
      store: getStore(),
      localVue,
      propsData: { value: (new Date(2021, 0, 1)).getTime() },
      mocks: {
        $t: (key) => key
      }
    })

    await wrapper.find('.input').trigger('click')
    await wrapper.find('.month-year-selector').trigger('click')
    await wrapper.findAll('.year-selector .year').at(10).trigger('click')

    expect(wrapper.findAll('.month-selector .month').at(0).classes().includes('active')).toBe(true)
  })

  it('props being changed', async () => {
    const wrapper = shallowMount(DatePicker, {
      store: getStore(),
      localVue,
      propsData: { value: (new Date(2021, 0, 1)).getTime() },
      mocks: {
        $t: (key) => key
      }
    })

    await wrapper.setProps({
      value: (new Date(2020, 0, 1)).getTime()
    })

    expect(wrapper.find('.input').element.value).toBe('1 Jan 2020')
  })

  it('checks disbaled functionality', async () => {
    const wrapper = shallowMount(DatePicker, {
      store: getStore(),
      localVue,
      propsData: { value: (new Date(2021, 0, 1)).getTime(), isDisabled: true },
      mocks: {
        $t: (key) => key
      }
    })
    expect(wrapper.find('input').element.disabled).toBe(true)
  })
})

describe('DatePicker.vue', () => {
  it('check if validation is invoked on value change and error message displayed', async () => {
    const label = 'new_label'
    const formData = getNewForm()
    const dummyValidation = getDummyValidation()
    formData['Data']['Form']['Fields'][0]['Label'] = label
    formData['Data']['Form']['Fields'][0]['Validations']['And'] = dummyValidation
    const negativeDummy = (_) => false
    const wrapper = mount(Form, {
      store: getStoreWithActions({
        getFormData: async () => (
          Promise.resolve(
            formData
          )
        )
      }),
      localVue,
      propsData: { isEditing: true, name: 'DummyForm' },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises() 
    await wrapper.findComponent(DatePicker).setData({validationMapper: {Dummy:negativeDummy}})
    await wrapper.findComponent(DatePicker).setData({ localValue :{year :2021, month: 4, date:1} })
    await wrapper.findComponent(DatePicker).vm.$emit('input',Date('2021-05-01T00:00:00'))
    await wrapper.findComponent(DatePicker).vm.$emit('change',Date('2021-05-01T00:00:00'))
    await wrapper.findComponent(DatePicker).vm.$nextTick()
    expect(wrapper.findComponent(DatePicker).find('p.error-message').text()).toMatch(dummyValidation[0]['ErrorMessage'])
  })
})

describe('DatePicker.vue', () => {
  it('check if validation is invoked on value change and error message not displayed if validation return true', async () => {
    const label = 'new_label'
    const formData = getNewForm()
    const dummyValidation = getDummyValidation()
    formData['Data']['Form']['Fields'][0]['Label'] = label
    formData['Data']['Form']['Fields'][0]['Validations']['And'] = dummyValidation
    const postiveDummy = (_) => true
    const wrapper = mount(Form, {
      store: getStoreWithActions ({
        getFormData: async () => (
          Promise.resolve(
            formData
          )
        )
      }),
      localVue,
      propsData: { isEditing: false, name: 'DummyForm' },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises() 
    await wrapper.findComponent(DatePicker).setData({validationMapper: {Dummy:postiveDummy}})
    await wrapper.findComponent(DatePicker).setData({ localValue :{year :2021, month: 4, date:1} })
    await wrapper.findComponent(DatePicker).vm.$emit('input',Date('2021-05-01T00:00:00'))
    await wrapper.findComponent(DatePicker).vm.$emit('change',Date('2021-05-01T00:00:00'))
    await wrapper.findComponent(DatePicker).vm.$nextTick()
    expect(wrapper.findComponent(DatePicker).find('p.error-message').exists()).toBeFalsy()
  })
})
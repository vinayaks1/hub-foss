const testForm = {
    "Success": true,
    "Data": {
        "Form": {
            "Name": "Registration",
            "Title": "_add_patient",
            "Parts": [
                {
                    "FormName": "Registration",
                    "Name": "Residence",
                    "Title": "",
                    "TitleKey": "treatment_center",
                    "Order": 3,
                    "IsVisible": true,
                    "Id": 59,
                    "Type": "vertical-form-part",
                    "Rows": null,
                    "Columns": null,
                    "RowDataName": "Residence",
                    "AllowRowOpen": false,
                    "AllowRowDelete": false,
                    "ListItemTitle": null,
                    "ItemDescriptionField": null,
                    "IsRepeatable": false,
                    "RecordId": null
                },
                {
                    "FormName": "Registration",
                    "Name": "TreatmentDetails",
                    "Title": "",
                    "TitleKey": "Treatment Details",
                    "Order": 4,
                    "IsVisible": true,
                    "Id": 60,
                    "Type": "vertical-form-part",
                    "Rows": null,
                    "Columns": null,
                    "RowDataName": "TreatmentDetails",
                    "AllowRowOpen": false,
                    "AllowRowDelete": false,
                    "ListItemTitle": null,
                    "ItemDescriptionField": null,
                    "IsRepeatable": false,
                    "RecordId": null
                }
            ],
            "PartOptions": null,
            "Fields": [
                {
                    "Value": null,
                    "PartName": "Residence",
                    "Placeholder": null,
                    "Component": "app-hierarchy-selection-field",
                    "IsDisabled": false,
                    "IsVisible": true,
                    "LabelKey": "HierarchyMapping_Residence",
                    "Label": "Hierarchy Mapping Residence",
                    "Name": "HierarchyMapping_Residence",
                    "IsHierarchySelector": false,
                    "IsRequired": true,
                    "RemoteUrl": null,
                    "Type": null,
                    "AddOn": null,
                    "Options": null,
                    "HierarchyOptions": null,
                    "OptionsWithKeyValue": null,
                    "AdditionalInfo": null,
                    "DefaultVisibilty": false,
                    "Order": 0,
                    "OptionsWithLabel": null,
                    "Validations": {
                        "Or": null,
                        "And": [
                            {
                                "Type": "Required",
                                "Max": 0,
                                "Min": 0,
                                "IsBackendValidation": false,
                                "ValidationUrl": null,
                                "ShowServerError": false,
                                "ErrorTargetField": null,
                                "ValidationParams": null,
                                "ErrorMessage": "This field is required",
                                "ErrorMessageKey": "error_field_required",
                                "RequiredOnlyWhen": null,
                                "Regex": null
                            }
                        ]
                    },
                    "ResponseDataPath": null,
                    "OptionDisplayKeys": null,
                    "OptionValueKey": null,
                    "LoadImmediately": false,
                    "HierarchySelectionConfigs": [
                        {
                            "Types": [
                                "STATE"
                            ],
                            "Type": null,
                            "Order": 1,
                            "Level": 2,
                            "OptionDisplayKeys": [
                                "Name",
                                "Id",
                                "Code"
                            ],
                            "Placeholder": null,
                            "Label": "Select STATE",
                            "Options": [],
                            "RemoteUrl": "/api/Hierarchy/GetHierarchiesByTypeParent"
                        },
                        {
                            "Types": [
                                "DISTRICT"
                            ],
                            "Type": null,
                            "Order": 2,
                            "Level": 3,
                            "OptionDisplayKeys": [
                                "Name",
                                "Id",
                                "Code"
                            ],
                            "Placeholder": null,
                            "Label": "Select DISTRICT",
                            "Options": [],
                            "RemoteUrl": "/api/Hierarchy/GetHierarchiesByTypeParent"
                        },
                        {
                            "Types": [
                                "TU"
                            ],
                            "Type": null,
                            "Order": 3,
                            "Level": 4,
                            "OptionDisplayKeys": [
                                "Name",
                                "Id",
                                "Code"
                            ],
                            "Placeholder": null,
                            "Label": "Select TU",
                            "Options": [],
                            "RemoteUrl": "/api/Hierarchy/GetHierarchiesByTypeParent"
                        }
                    ],
                    "DisabledDateConfig": null,
                    "RemoteUpdateConfig": null,
                    "RowNumber": 1,
                    "ColumnNumber": null,
                    "Id": 3034,
                    "ParentType": "PART",
                    "ParentId": 59,
                    "FieldOptions": null,
                    "ValidationList": null,
                    "DefaultValue": null,
                    "Key": "ResidenceHierarchyMapping_Residence",
                    "HasInfo": false,
                    "InfoText": null,
                    "Config": null,
                    "ColumnWidth": 6,
                    "HasToggleButton": false
                },
                {
                    "Value": null,
                    "PartName": "TreatmentDetails",
                    "Placeholder": null,
                    "Component": "app-select",
                    "IsDisabled": false,
                    "IsVisible": true,
                    "LabelKey": "adherence_technology",
                    "Label": "Adherence Technology",
                    "Name": "MonitoringMethod",
                    "IsHierarchySelector": false,
                    "IsRequired": true,
                    "RemoteUrl": null,
                    "Type": null,
                    "AddOn": null,
                    "Options": null,
                    "HierarchyOptions": null,
                    "OptionsWithKeyValue": [
                        {
                            "Value": "evriMED(MERM)",
                            "Key": "MERM",
                            "AdherenceTechOptions": "MERM"
                        },
                        {
                            "Value": "VOT",
                            "Key": "VOT",
                            "AdherenceTechOptions": "VOT"
                        },
                        {
                            "Value": "Envelopes - 99DOTS",
                            "Key": "99DOTS",
                            "AdherenceTechOptions": "99DOTS"
                        },
                        {
                            "Value": "Stickers/Labels - 99DOTS Lite",
                            "Key": "99DOTSLite",
                            "AdherenceTechOptions": "99DOTSLite"
                        },
                        {
                            "Value": "Followed up Without Technology",
                            "Key": "NONE",
                            "AdherenceTechOptions": "NONE"
                        }
                    ],
                    "AdditionalInfo": null,
                    "DefaultVisibilty": false,
                    "Order": 0,
                    "OptionsWithLabel": null,
                    "Validations": {
                        "Or": null,
                        "And": [
                            {
                                "Type": "Required",
                                "Max": 0,
                                "Min": 0,
                                "IsBackendValidation": false,
                                "ValidationUrl": null,
                                "ShowServerError": false,
                                "ErrorTargetField": null,
                                "ValidationParams": null,
                                "ErrorMessage": "This field is required",
                                "ErrorMessageKey": "error_field_required",
                                "RequiredOnlyWhen": null,
                                "Regex": null
                            }
                        ]
                    },
                    "ResponseDataPath": null,
                    "OptionDisplayKeys": null,
                    "OptionValueKey": null,
                    "LoadImmediately": false,
                    "HierarchySelectionConfigs": null,
                    "DisabledDateConfig": null,
                    "RemoteUpdateConfig": null,
                    "RowNumber": 6,
                    "ColumnNumber": null,
                    "Id": 11133,
                    "ParentType": "PART",
                    "ParentId": 60,
                    "FieldOptions": "[{\"Value\" : \"evriMED(MERM)\", \"Key\" : \"MERM\", \"AdherenceTechOptions\": \"MERM\"}, {\"Value\":\"_VOT\", \"Key\":\"VOT\", \"AdherenceTechOptions\":\"VOT\"}, {\"Value\":\"_99dots\", \"Key\":\"99DOTS\", \"AdherenceTechOptions\": \"99DOTS\"}, {\"Value\":\"_99dotsLite\", \"Key\":\"99DOTSLite\", \"AdherenceTechOptions\": \"99DOTSLite\"}, {\"Value\":\"FollowedWithoutTechnology\", \"Key\":\"NONE\", \"AdherenceTechOptions\": \"NONE\"}]",
                    "ValidationList": null,
                    "DefaultValue": null,
                    "Key": "TreatmentDetailsMonitoringMethod",
                    "HasInfo": false,
                    "InfoText": null,
                    "Config": null,
                    "ColumnWidth": 6,
                    "HasToggleButton": false
                }
            ],
            "Triggers": null,
            "TriggerConfigs": null,
            "ValueDependencies": [],
            "FilterDependencies": [
                {
                    "Lookups": [
                        {
                            "FormPart": "Residence",
                            "Field": "HierarchyMapping_Residence",
                            "ExpectedValue": null,
                            "ExpectedValues": null,
                            "IsObject": false,
                            "ObjectKeyAndValues": null,
                            "ObjectKeyAndValue": null,
                            "AttributeName": "AdherenceTechOptions",
                            "ConstraintName": null,
                            "RestrictedValue": null,
                            "ComparisonOperator": "EQ",
                            "AnyObjectKeyValue": false,
                            "FilteredKeys": null,
                            "LookupFormPart": "Residence",
                            "LookupField": "HierarchyMapping_Residence"
                        }
                    ],
                    "FormPart": "TreatmentDetails",
                    "Field": "MonitoringMethod",
                    "FormPartId": null,
                    "FieldId": null,
                    "Type": "FILTER",
                    "IsForm": false
                }
            ],
            "VisibilityDependencies": [],
            "DateConstraintDependencies": [],
            "IsRequiredDependencies": [],
            "RemoteUpdateConfigs": [] ,
            "ValuePropertyDependencies": [],
            "CompoundValueDependencies": null,
            "SaveEndpoint": "/api/Patients/Add?isPatientDetailsRequired=false",
            "SaveText": "Add Patient",
            "SaveTextKey": "_add_patient"
        },
        "ExistingData": {}
    },
    "Error": null
}

export default testForm
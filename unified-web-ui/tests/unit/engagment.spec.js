import { mount, createLocalVue } from '@vue/test-utils'
import { Map, List } from 'immutable'
import Form from '@/app/shared/components/Form.vue'
import Button from '@/app/shared/components/Button.vue'
import flushPromises from 'flush-promises'
import Engagment from '../../src/app/Pages/dashboard/patient/components/Engagement'
import Vuex from 'vuex'
import Vue from 'vue'

const localVue = createLocalVue()

localVue.use(Vuex)
window.EventBus = new Vue()

describe('Engagment.vue', () => {
  let store
  let actions
  beforeEach(() => {
    actions = {
      getFormData: jest.fn(),
      setIsEditing: jest.fn(),
      loadForm: jest.fn(),
      startSaving: jest.fn(),
      save: jest.fn(),
      endSaving: jest.fn()
    }
    store = new Vuex.Store({
      modules: {
        Form: {
          namespaced: true,
          state: {
            isSaving: false,
            isLoading: false,
            isEditing: true,
            hasFormError: false,
            adherenceTechnology: { MERM: false, VOT: false },
            existingData: null,
            existingVModel: {},
            form: {},
            title: '',
            remoteUpdateConfigsMappedByField: Map(),
            remoteUpdateConfigs: [],
            hierarchyConfigs: Map(),
            valueDependencies: List(),
            formPartsMappedByName: Map(),
            fieldsMappedByFormPartName: Map(),
            fieldsMappedByNameOriginal: Map(),
            fieldsMappedByName: null,
            allFields: null,
            visibilityDependencies: List(),
            isRequiredDependencies: List(),
            filterDependencies: List(),
            visibilityDependenciesMappedByFormPartName: null,
            dateConstraintDependency: List(),
            formFieldsByFormPartName: {},
            saveEndpoint: '',
            saveText: '',
            saveUrlParams: '',
            getFormUrlParams: '',
            replaceExistingDataFromServer: true,
            recaptchaVerified: false,
            otp: '',
            editAccess: false,
            valuePropertyDependencies: List()
          },
          actions
        }
      }
    })
  })

  it('render editdetails and form component', async () => {
    store.state.Form.isLoading = true
    const wrapper = mount(Engagment, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })

    await flushPromises()
    await wrapper.setData({ formActive: true, editEngagement: true })
    expect(wrapper.find('#engagement').exists()).toBe(true)
    await wrapper.findAllComponents(Button).at(0).trigger('click')
    expect(wrapper.find('.submit').exists()).toBe(false)
  })

  it('loading to be true', async () => {
    store.state.Form.isLoading = true
    const wrapper = mount(Engagment, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })

    await flushPromises()
    await wrapper.setData({ formActive: false, editEngagement: true })
    expect(wrapper.find('#engagement').exists()).toBe(true)
    await wrapper.findAllComponents(Button).at(0).trigger('click')
    expect(wrapper.find('.submit').exists()).toBe(false)
  })

  it('Loading to be false', async () => {
    store.state.Form.isLoading = false
    const wrapper = mount(Engagment, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })

    await flushPromises()
    await wrapper.setData({ formActive: false, editEngagement: true })
    expect(wrapper.find('#engagement').exists()).toBe(true)
    await wrapper.findAllComponents(Button).at(0).trigger('click')
    expect(wrapper.find('.submit').exists()).toBe(true)
  })

  it('cancel and form component', async () => {
    store.state.Form.isLoading = false
    const wrapper = mount(Engagment, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })

    await flushPromises()
    await wrapper.setData({ formActive: true, editEngagement: true })
    expect(wrapper.find('#engagement').exists()).toBe(true)
    await wrapper.findAllComponents(Button).at(0).trigger('click')
    expect(wrapper.find('.submit').exists()).toBe(false)
  })

  it('cancel button with loading as true', async () => {
    store.state.Form.isLoading = true
    const wrapper = mount(Engagment, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    await wrapper.setData({ formActive: false, editEngagement: true })
    expect(wrapper.find('#engagement').exists()).toBe(true)
    await wrapper.findAllComponents(Button).at(0).trigger('click')
    expect(wrapper.find('#engagement').find('#Engagement').exists()).toBe(true)
  })

  it('press submit', async () => {
    store.state.Form.isLoading = false
    const wrapper = mount(Engagment, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    await wrapper.setData({ formActive: true, editEngagement: true })
    expect(wrapper.findComponent(Form).exists()).toBe(true)
    wrapper.vm.back()
    await flushPromises()
    expect(wrapper.find('.secondary').exists()).toBe(false)
  })
})

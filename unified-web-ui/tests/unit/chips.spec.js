import {
  mount
} from '@vue/test-utils'
import Chips from '@/app/shared/components/Chips.vue'

const testData = {
  label: 'Enabled',
  color: 'white',
  showButton: true
}

describe('Chips unit test', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(Chips, {
      mocks: {
        $t: (key) => key
      }
    })
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  test('Rendering props', () => {
    const wrapper = mount(Chips, {
      propsData: {
        ...testData
      },
      mocks: {
        $t: (key) => key
      }
    })
    expect(wrapper.props().label).toBe('Enabled')
    expect(wrapper.props().color).toBe('white')
    expect(wrapper.props().showButton).toBe(true)
  })

  test('Calls showChip', () => {
    const showChip = jest.fn()
    const wrapper = mount(Chips, {
      propsData: {
        ...testData
      },
      mocks: {
        $t: (key) => key
      }
    })
    wrapper.setMethods({
      showChip: showChip
    })
    wrapper.find('button').trigger('showChip')
    /* eslint-disable */
    expect(showChip).toHaveBeenCalled
    /* eslint-enable */
  })
})

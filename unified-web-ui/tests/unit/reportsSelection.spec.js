import { mount, createLocalVue } from '@vue/test-utils'
import flushPromises from 'flush-promises'
import ReportsSelection from '../../src/app/Pages/dashboard/Reports/components/ReportSelection'
import Vuex from 'vuex'
import Vue from 'vue'

const localVue = createLocalVue()

localVue.use(Vuex)
window.EventBus = new Vue()
const $t = () => {}

describe('ReportsSelection.vue', () => {
  let store
  let actions
  beforeEach(() => {
    actions = {
      setActiveReport: jest.fn()
    }
    store = new Vuex.Store({
      modules: {
        Reports: {
          namespaced: true,
            state: {
            isFormActive: false,
            activeReportTitle: {},
          },
          actions
        }
      }
    })
  })
  
  it('render reports selection', async () => {
    const wrapper = mount(ReportsSelection, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    expect(wrapper.find('.card').exists()).toBe(true)
  })
  
})

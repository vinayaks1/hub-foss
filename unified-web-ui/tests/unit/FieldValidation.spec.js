import {
    isPhoneNumber,
    isPhoneNumberNotStartingWithZero,
    isPhoneNumberStartingWithZero,
    isPhoneNumberBetweenSevenAndEleven,
    isPhoneNumberLengthEleven,
    isFourAlphaNumericCharacters,
    makeBackendRequest,
    isOnlyAlphabetAndSpaces,
    isEmailId,
    validRequiredInput,
    isValidLength,
    isOnlyNumber,
    isTBNumberFormat,
    isTBNumberFormatForMYA,
    // isValueSameAs,
    isThreeDigitNumber,
    isAgeBetween18and99,
    // isPhoneNumberLength9without0or10with0,
    isAlphaNumeric,
    isPhoneNumberMustStartWithZero,
    isPhoneNumberLengthSevenToNine,
    isPhoneNumberLengthNine,
    isPhoneNumberLengthNinePlus,
    isPhoneNumberLengthTen,
    isPhoneNumberMaxLengthEleven,
    isBetweenZeroAndHundred,
    isBetweenTenAndHundred,
    isNumberLengthFive,
    isTBNumberFormatForUKRandUGAVOT,
    isMinLengthGreaterThan2,
    isGreaterThanZero
  } from '@/utils/FieldValidation'

const testData = [
    // isPhoneNumber tests
    {test_data: ['123456789'], test_function: isPhoneNumber , assertion: true },
    {test_data: ['023456789'], test_function: isPhoneNumber , assertion: false },    
    {test_data: ['1234567899'], test_function: isPhoneNumber , assertion: false },
    {test_data: ['12345678'], test_function: isPhoneNumber , assertion: false },
    // isPhoneNumberNotStartingWithZero
    {test_data: ['1234567801'], test_function: isPhoneNumberNotStartingWithZero , assertion: true },
    {test_data: ['0234567801'], test_function: isPhoneNumberNotStartingWithZero , assertion: false },
    // isPhoneNumberStartingWithZero
    {test_data: ['0112345678'], test_function: isPhoneNumberStartingWithZero , assertion: true },
    // isPhoneNumberBetweenSevenAndEleven
    {test_data: ['912345'], test_function: isPhoneNumberBetweenSevenAndEleven , assertion: false },
    {test_data: ['9123451'], test_function: isPhoneNumberBetweenSevenAndEleven , assertion: true },
    {test_data: ['91234512345'], test_function: isPhoneNumberBetweenSevenAndEleven , assertion: true },
    {test_data: ['912345123451'], test_function: isPhoneNumberBetweenSevenAndEleven , assertion: false },
    // isPhoneNumberLengthEleven
    {test_data: ['12345678901'], test_function: isPhoneNumberLengthEleven , assertion: true },
    {test_data: ['1234567890'], test_function: isPhoneNumberLengthEleven , assertion: false },
    {test_data: ['123456789011'], test_function: isPhoneNumberLengthEleven , assertion: false },
    {test_data: ['02345678901'], test_function: isPhoneNumberLengthEleven , assertion: false },
    // isFourAlphaNumericCharacters
    {test_data: ['12abc'], test_function: isFourAlphaNumericCharacters , assertion: false },
    {test_data: ['12ab'], test_function: isFourAlphaNumericCharacters , assertion: true },
    {test_data: ['12a@'], test_function: isFourAlphaNumericCharacters , assertion: false },
    // isOnlyAlphabetAndSpaces
    {test_data: ['abc abc'], test_function: isOnlyAlphabetAndSpaces , assertion: true },
    {test_data: ['1 abc'], test_function: isOnlyAlphabetAndSpaces , assertion: false },
    // isEmailId
    {test_data: ['abc@abc.com'], test_function: isEmailId , assertion: true },
    {test_data: ['abc@.abc.com'], test_function: isEmailId , assertion: false },
    {test_data: ['abc.abc.com'], test_function: isEmailId , assertion: false },
    // validRequiredInput
    {test_data: [null], test_function: validRequiredInput , assertion: false },
    {test_data: [''], test_function: validRequiredInput , assertion: false },
    {test_data: [{}], test_function: validRequiredInput , assertion: false },
    {test_data: [[]], test_function: validRequiredInput , assertion: false },
    {test_data: ['abc'], test_function: validRequiredInput , assertion: true },
    {test_data: [{a:''}], test_function: validRequiredInput , assertion: true },
    {test_data: [['a']], test_function: validRequiredInput , assertion: true },
    // isValidLength
    {test_data: ['abc',3,1], test_function: isValidLength , assertion: true },
    {test_data: ['abc',5,4], test_function: isValidLength , assertion: false },
    {test_data: ['abcefg',5,1], test_function: isValidLength , assertion: false },
    // isOnlyNumber
    {test_data: ['1234'], test_function: isOnlyNumber , assertion: true },
    {test_data: ['123a'], test_function: isOnlyNumber , assertion: false },
    // isTBNumberFormat
    {test_data: ['012345/KK/1234/0987'], test_function: isTBNumberFormat , assertion: true },
    {test_data: ['012345/KK/0234/0987'], test_function: isTBNumberFormat , assertion: false },
    // isTBNumberFormatForMYA
    {test_data: ['0123-1234'], test_function: isTBNumberFormatForMYA , assertion: true },
    {test_data: ['0123/1234'], test_function: isTBNumberFormatForMYA , assertion: false },
    // isThreeDigitNumber
    {test_data: ['12'], test_function: isThreeDigitNumber , assertion: true },
    {test_data: ['123'], test_function: isThreeDigitNumber , assertion: true },
    {test_data: ['1234'], test_function: isThreeDigitNumber , assertion: false },
    // isAgeBetween18and99
    {test_data: ['12'], test_function: isAgeBetween18and99 , assertion: false },
    {test_data: ['22'], test_function: isAgeBetween18and99 , assertion: true },
    {test_data: ['100'], test_function: isAgeBetween18and99 , assertion: false },
    // isPhoneNumberLength9without0or10with0
    // {test_data: ['123456789'], test_function: isPhoneNumberLength9without0or10with0 , assertion: true },
    // {test_data: ['0123456789'], test_function: isPhoneNumberLength9without0or10with0 , assertion: true },
    // {test_data: ['1234567890'], test_function: isPhoneNumberLength9without0or10with0 , assertion: false },
    // {test_data: ['12345678'], test_function: isPhoneNumberLength9without0or10with0 , assertion: false },
    // isAlphaNumeric
    {test_data: ['abc123. 123abc'], test_function: isAlphaNumeric , assertion: true },
    {test_data: ['abc123*'], test_function: isAlphaNumeric , assertion: false },
    // isPhoneNumberMustStartWithZero
    {test_data: ['0123456789'], test_function: isPhoneNumberMustStartWithZero , assertion: true },
    {test_data: ['1123456789'], test_function: isPhoneNumberMustStartWithZero , assertion: false },
    // isPhoneNumberLengthSevenToNine
    {test_data: ['1234567'], test_function: isPhoneNumberLengthSevenToNine , assertion: true },
    {test_data: ['123456789'], test_function: isPhoneNumberLengthSevenToNine , assertion: true },
    {test_data: ['1234567890'], test_function: isPhoneNumberLengthSevenToNine , assertion: false },
    {test_data: ['123456'], test_function: isPhoneNumberLengthSevenToNine , assertion: false },
    // isPhoneNumberLengthNine
    {test_data: ['123456789'], test_function: isPhoneNumberLengthNine , assertion: true },
    {test_data: ['12345678'], test_function: isPhoneNumberLengthNine , assertion: false },
    {test_data: ['1234567890'], test_function: isPhoneNumberLengthNine , assertion: false },
    {test_data: ['023456789'], test_function: isPhoneNumberLengthNine , assertion: false },
    // isPhoneNumberLengthNinePlus
    {test_data: ['123456789'], test_function: isPhoneNumberLengthNinePlus , assertion: true },
    {test_data: ['12345678'], test_function: isPhoneNumberLengthNinePlus , assertion: false },
    {test_data: ['1234567890'], test_function: isPhoneNumberLengthNinePlus , assertion: true },
    {test_data: ['023456789'], test_function: isPhoneNumberLengthNinePlus , assertion: false },
    // isPhoneNumberLengthTen
    {test_data: ['1234567890'], test_function: isPhoneNumberLengthTen , assertion: true },
    {test_data: ['123456789'], test_function: isPhoneNumberLengthTen , assertion: false },
    {test_data: ['0123456789'], test_function: isPhoneNumberLengthTen , assertion: false },
    // isPhoneNumberMaxLengthEleven
    {test_data: ['1'], test_function: isPhoneNumberMaxLengthEleven , assertion: true },
    {test_data: ['12345678901'], test_function: isPhoneNumberMaxLengthEleven , assertion: true },
    {test_data: ['02345678901'], test_function: isPhoneNumberMaxLengthEleven , assertion: false },
    {test_data: ['123456789012'], test_function: isPhoneNumberMaxLengthEleven , assertion: false },
    // isBetweenZeroAndHundred
    {test_data: ['1'], test_function: isBetweenZeroAndHundred , assertion: true },
    {test_data: ['99'], test_function: isBetweenZeroAndHundred , assertion: true },
    {test_data: ['100'], test_function: isBetweenZeroAndHundred , assertion: false },
    {test_data: ['0'], test_function: isBetweenZeroAndHundred , assertion: false },
    // isBetweenTenAndHundred
    {test_data: ['10'], test_function: isBetweenTenAndHundred , assertion: false },
    {test_data: ['11'], test_function: isBetweenTenAndHundred , assertion: true },
    {test_data: ['99'], test_function: isBetweenTenAndHundred , assertion: true },
    {test_data: ['100'], test_function: isBetweenTenAndHundred , assertion: false },
    // isNumberLengthFive
    {test_data: ['01234'], test_function: isNumberLengthFive , assertion: true },
    {test_data: ['12345'], test_function: isNumberLengthFive , assertion: true },
    {test_data: ['123456'], test_function: isNumberLengthFive , assertion: false },
    {test_data: ['1234'], test_function: isNumberLengthFive , assertion: false },
    // isTBNumberFormatForUKRandUGAVOT
    {test_data: ['123456-9'], test_function: isTBNumberFormatForUKRandUGAVOT , assertion: true },
    {test_data: ['123456-90'], test_function: isTBNumberFormatForUKRandUGAVOT , assertion: false },
    {test_data: ['12345v-9'], test_function: isTBNumberFormatForUKRandUGAVOT , assertion: false },
    // isMinLengthGreaterThan2
    {test_data: ['12'], test_function: isMinLengthGreaterThan2 , assertion: false },
    {test_data: ['123'], test_function: isMinLengthGreaterThan2 , assertion: true },
    // isGreaterThanZero
    {test_data: ['1'], test_function: isGreaterThanZero , assertion: true },
    {test_data: ['0'], test_function: isGreaterThanZero , assertion: false }
] 

jest.mock("../../src/app/shared/store/Api", () => ({
    ApiServerClient: {get: jest.fn(() => {
        return Promise.resolve(
        {Success:true}
      )}
      )
  }}));

describe('FieldValidaiton unit test', () => {
    test('it validates list of validations across different inputs', () => {
        for(let i = 0;i<testData.length;i++){
            let {test_data,test_function,assertion} = testData[i]
            try{
                expect(test_function(...test_data)).toBe(assertion)
            }
            catch(Exception){
                console.log(test_function.name+" with args "+test_data+" failed")
            }    
        }
    })
    test('it validates makeBackendRequest', async () => {
        expect(await makeBackendRequest('url','val1','val2')).toBe(true)
    })
})   
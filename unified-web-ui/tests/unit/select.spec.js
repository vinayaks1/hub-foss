import moxios from 'moxios'
import api from '../../src/app/shared/store/Api.js'

import { mount, shallowMount, createLocalVue } from '@vue/test-utils'
import FormStore from '@/app/shared/store/modules/form'
import flushPromises from 'flush-promises'
import Vue from 'vue'
import Form from '@/app/shared/components/Form.vue'
import Select from '@/app/shared/components/Select.vue'
import Vuex from 'vuex'

const getLocalVue = () => {
  const localVue = createLocalVue()
  localVue.use(Vuex)
  return localVue
}
const localVue = createLocalVue()

localVue.use(Vuex)
const getStore = () => {
  return new Vuex.Store({
  })
}

const getStoreWithActions = (actions) => {
  const formattedActions = {
    ...FormStore.actions,
    ...actions
  }

  return new Vuex.Store({
    modules: {
      Form: {
        state: FormStore.state,
        actions: formattedActions,
        mutations: FormStore.mutations,
        namespaced: true
      }
    }
  })
}

const formWithOneSelect = {
  Success: true,
  Data: {
    Form: {
      Name: 'SingleInputForm1',
      Title: '_dummy_input_form',
      Parts: [
        {
          FormName: 'SingleInputForm1',
          Name: 'input_form_01',
          Title: '',
          TitleKey: 'input_form_01',
          Order: 1,
          IsVisible: true,
          Id: 119,
          Type: 'vertical-form-part',
          Rows: null,
          Columns: null,
          RowDataName: 'input_form_01',
          AllowRowOpen: false,
          AllowRowDelete: false,
          ListItemTitle: null,
          ItemDescriptionField: null,
          IsRepeatable: false,
          RecordId: null
        }
      ],
      PartOptions: null,
      Fields: [
        {
          Value: null,
          PartName: 'input_form_01',
          Placeholder: null,
          Component: 'app-select',
          IsDisabled: false,
          IsVisible: true,
          LabelKey: 'Validate_input__Required_01',
          Label: null,
          Name: 'Validate_input__Required_01',
          IsHierarchySelector: false,
          IsRequired: false,
          RemoteUrl: null,
          Type: null,
          AddOn: null,
          Options: null,
          HierarchyOptions: null,
          OptionsWithKeyValue: [
            {
              Value: 'Val1',
              Key: 'Val1'
            },
            {
              Value: 'Val2',
              Key: 'Val2'
            }
          ],
          AdditionalInfo: null,
          DefaultVisibilty: false,
          Order: 0,
          OptionsWithLabel: null,
          Validations: {
            Or: null,
            And: [
            ]
          },
          ResponseDataPath: null,
          OptionDisplayKeys: null,
          OptionValueKey: null,
          LoadImmediately: false,
          HierarchySelectionConfigs: null,
          DisabledDateConfig: null,
          RemoteUpdateConfig: null,
          RowNumber: 16,
          ColumnNumber: null,
          Id: 11407,
          ParentType: 'PART',
          ParentId: 119,
          FieldOptions: '[{"Value" : "Val1", "Key" : "Val1"}, {"Value":"Val2", "Key":"Val2"}]',
          ValidationList: 'Required',
          DefaultValue: null,
          Key: 'input_form_01Validate_input__Required_01',
          HasInfo: false,
          InfoText: null,
          Config: null,
          ColumnWidth: 6,
          HasToggleButton: false
        }
      ],
      Triggers: null,
      TriggerConfigs: null,
      ValueDependencies: [],
      FilterDependencies: [],
      VisibilityDependencies: [],
      DateConstraintDependencies: [],
      IsRequiredDependencies: [],
      RemoteUpdateConfigs: [],
      ValuePropertyDependencies: [],
      CompoundValueDependencies: null,
      SaveEndpoint: '/api/patients',
      SaveText: null,
      SaveTextKey: '_submit'
    },
    ExistingData: {}
  },
  Error: null
}

const getNewForm = () => {
  return JSON.parse(JSON.stringify(formWithOneSelect))
}
const dummyValidation = {
  Type: 'Dummy',
  Max: 0,
  Min: 0,
  IsBackendValidation: false,
  ValidationUrl: null,
  ShowServerError: false,
  ErrorTargetField: null,
  ValidationParams: null,
  ErrorMessage: 'Dummy validation message',
  ErrorMessageKey: 'error_dummy',
  RequiredOnlyWhen: null,
  Regex: null
}

const getDummyValidation = () => {
  return [JSON.parse(JSON.stringify(dummyValidation))]
}

describe('Select.vue', () => {
  it('renders props.label when passed', async () => {
    const label = 'Label'
    const wrapper = shallowMount(Select, {
      store: getStore(),
      localVue,
      propsData: { label },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    expect(wrapper.text()).toMatch(label)
  })
})

describe('Select.vue', () => {
  it('error message to be displayed', async () => {
    const errorMessage = 'Error'
    const wrapper = shallowMount(Select, {
      store: getStore(),
      localVue,
      propsData: { hasError: true, errorMessage },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    expect(wrapper.find('.error-container').exists()).toBe(true)
    expect(wrapper.find('p.error-message').text()).toMatch(errorMessage)
  })
})

describe('Select.vue', () => {
  it('error turned off by default', async () => {
    const wrapper = shallowMount(Select, {
      store: getStore(),
      localVue,
      propsData: { },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    expect(wrapper.find('.error-container').exists()).toBe(false)
  })
})

describe('Select.vue', () => {
  it('disabled functionality', async () => {
    const wrapper = shallowMount(Select, {
      store: getStore(),
      localVue,
      propsData: { isDisabled: true },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    expect(wrapper.find('input').element.disabled).toBe(true)
  })
})

describe('Select.vue', () => {
  it('disabled turned off by default', async () => {
    const wrapper = shallowMount(Select, {
      store: getStore(),
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    expect(wrapper.find('input').element.disabled).toBe(false)
  })
})

describe('Select.vue', () => {
  it('name to be set', async () => {
    const name = 'Name'
    const wrapper = shallowMount(Select, {
      store: getStore(),
      localVue,
      propsData: { name },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    expect(wrapper.find('input').element.name).toMatch(name)
  })
})

describe('Select.vue', () => {
  it('placeholder to be set', async () => {
    const placeholder = 'Default option'
    const wrapper = shallowMount(Select, {
      store: getStore(),
      localVue,
      propsData: { placeholderText: placeholder },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    expect(wrapper.find('input').element.placeholder).toMatch(placeholder)
  })
})

describe('Select.vue', () => {
  it('Default Placeholder', async () => {
    const wrapper = shallowMount(Select, {
      store: getStore(),
      localVue,
      propsData: { },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    expect(wrapper.find('input').element.placeholder).toMatch('Default option')
  })
})

describe('Select.vue', () => {
  it('Options to be set', async () => {
    const options = [
      {
        Key: 'Value 1',
        Value: 'Value 1'
      },
      {
        Key: 'Value 2',
        Value: 'value 2'
      }
    ]
    const wrapper = shallowMount(Select, {
      store: getStore(),
      localVue,
      propsData: { allOptions: options },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    expect(wrapper.findAll('.dropdown ul li').length).toBe(options.length)
    expect(wrapper.findAll('.dropdown ul li').at(0).text()).toBe(options[0].Value)
    expect(wrapper.findAll('.dropdown ul li').at(1).text()).toBe(options[1].Value)
  })
})

describe('Select.vue', () => {
  it('Passing value for single select', async () => {
    const options = [
      {
        Key: 'Value 1',
        Value: 'Value 1'
      },
      {
        Key: 'Value 2',
        Value: 'Value 2'
      }
    ]
    const value = {
      Key: 'Value 1',
      Value: 'Value 1'
    }
    const wrapper = shallowMount(Select, {
      store: getStore(),
      localVue,
      propsData: { value, allOptions: options },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    expect(wrapper.find('input').element.value).toMatch('Value 1')
  })
})

describe('Select.vue', () => {
  it('Passing very long string value for single select', async () => {
    const options = [
      {
        Key: 'Value 1',
        Value: 'Very very very very very very very very long'
      },
      {
        Key: 'Value 2',
        Value: 'Value 2'
      }
    ]
    const value = {
      Key: 'Value 1',
      Value: 'Very very very very very very very very long'
    }
    const wrapper = shallowMount(Select, {
      store: getStore(),
      localVue,
      propsData: { value, allOptions: options },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    expect(wrapper.find('input').element.value).toMatch('Very very very very ...')
  })
})

describe('Select.vue', () => {
  it('Passing one very long value for multi select', async () => {
    const options = [
      {
        Key: 'Value 1',
        Value: 'Very very very very very very very very long'
      }
    ]
    const value = [{
      Key: 'Value 1',
      Value: 'Very very very very very very very very long'
    }]
    const wrapper = shallowMount(Select, {
      store: getStore(),
      localVue,
      propsData: { value, isMultiSelect: true, allOptions: options },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    expect(wrapper.find('input').element.value).toMatch('Very very very very very...')
  })
})

describe('Select.vue', () => {
  it('Passing one very long value and one more value for multi select', async () => {
    const options = [
      {
        Key: 'Value 1',
        Value: 'Very very very very very very very very long'
      },
      {
        Key: 'Value 2',
        Value: 'Value 2'
      }
    ]
    const value = [{
      Key: 'Value 1',
      Value: 'Very very very very very very very very long'
    },
    {
      Key: 'Value 2',
      Value: 'Value 2'
    }]
    const wrapper = shallowMount(Select, {
      store: getStore(),
      localVue,
      propsData: { value, isMultiSelect: true, allOptions: options },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    expect(wrapper.find('input').element.value).toMatch('Very very v... + 1 more')
  })
})

describe('Select.vue', () => {
  it('Passing value for multi select', async () => {
    const options = [
      {
        Key: 'Value 1',
        Value: 'Value 1'
      },
      {
        Key: 'Value 2',
        Value: 'Value 2'
      }
    ]
    const value = [{
      Key: 'Value 1',
      Value: 'Value 1'
    },
    {
      Key: 'Value 2',
      Value: 'Value 2'
    }]
    const wrapper = shallowMount(Select, {
      store: getStore(),
      localVue,
      propsData: { value, isMultiSelect: true, allOptions: options },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    expect(wrapper.find('input').element.value).toMatch('Value 1,Value 2')
  })
})

describe('Select.vue', () => {
  it('Passing value for multi select and respective checkboxes being checked and select all checkbox not checked ', async () => {
    const options = [
      {
        Key: 'Value 1',
        Value: 'Value 1'
      },
      {
        Key: 'Value 2',
        Value: 'Value 2'
      }
    ]
    const value = [{
      Key: 'Value 2',
      Value: 'Value 2'
    }]
    const wrapper = shallowMount(Select, {
      store: getStore(),
      localVue,
      propsData: { allOptions: options, value, isMultiSelect: true },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    expect(wrapper.findAll('.dropdown ul li input[type="checkbox"]').at(0).element.checked).toBe(false)
    expect((wrapper.findAll('.dropdown ul li input[type="checkbox"]').at(1)).element.checked).toBe(true)
    expect(
      wrapper.findAll('.dropdown ul li input[type="checkbox"]').at(2).element
        .checked
    ).toBe(false)
  })
})

describe('Select.vue', () => {
  it('Passing many values for multi select', async () => {
    const options = [
      {
        Key: 'Value 1',
        Value: 'Value 1'
      },
      {
        Key: 'Value 2',
        Value: 'Value 2'
      },
      {
        Key: 'Value 3',
        Value: 'Value 3'
      },
      {
        Key: 'Value 4',
        Value: 'Value 4'
      },
      {
        Key: 'Value 5',
        Value: 'Value 5'
      }
    ]
    const value = [{
      Key: 'Value 1',
      Value: 'Value 1'
    },
    {
      Key: 'Value 2',
      Value: 'Value 2'
    },
    {
      Key: 'Value 3',
      Value: 'Value 3'
    },
    {
      Key: 'Value 4',
      Value: 'Value 4'
    },
    {
      Key: 'Value 5',
      Value: 'Value 5'
    }]
    const wrapper = shallowMount(Select, {
      store: getStore(),
      localVue,
      propsData: { value: value, isMultiSelect: true, allOptions: options },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    expect(wrapper.findAll('.dropdown ul li input[type="checkbox"]').at(0).element.checked).toBe(true)
    expect(wrapper.findAll('.dropdown ul li input[type="checkbox"]').at(1).element.checked).toBe(true)
    expect(wrapper.find('input').element.value).toBe('Value 1,Value 2 + 3 more')
    expect(
      wrapper.find('[data-testid="select-all-input"] input[type="checkbox"]')
        .element.checked
    ).toBe(true)
  })
})

describe('Select.vue', () => {
  it('All options are selected when select-all checkbox is clicked', async () => {
    const options = [
      {
        Key: 'Value 1',
        Value: 'Value 1'
      },
      {
        Key: 'Value 2',
        Value: 'Value 2'
      },
      {
        Key: 'Value 3',
        Value: 'Value 3'
      }
    ]
    const value = [{
      Key: 'Value 1',
      Value: 'Value 1'
    }
    ]
    const wrapper = shallowMount(Select, {
      store: getStore(),
      localVue,
      propsData: { value: value, isMultiSelect: true, allOptions: options },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()

    await wrapper.find(
      '[data-testid="select-all-input"]'
    ).trigger('click')

    expect(wrapper.findAll('.dropdown ul li input[type="checkbox"]').at(0).element.checked).toBe(true)
    expect(wrapper.findAll('.dropdown ul li input[type="checkbox"]').at(1).element.checked).toBe(true)
    expect(wrapper.findAll('.dropdown ul li input[type="checkbox"]').at(2).element
      .checked).toBe(true)
    expect(wrapper.find('input').element.value).toBe('Value 1,Value 2 + 1 more')
    expect(
      wrapper.find('[data-testid="select-all-input"] input[type="checkbox"]')
        .element.checked
    ).toBe(true)
  })
})

describe('Select.vue', () => {
  it('All options are cleared when select-all checkbox is clicked twice', async () => {
    const options = [
      {
        Key: 'Value 1',
        Value: 'Value 1'
      },
      {
        Key: 'Value 2',
        Value: 'Value 2'
      },
      {
        Key: 'Value 3',
        Value: 'Value 3'
      }
    ]
    const value = [
      {
        Key: 'Value 1',
        Value: 'Value 1'
      }
    ]
    const wrapper = shallowMount(Select, {
      store: getStore(),
      localVue,
      propsData: { value: value, isMultiSelect: true, allOptions: options },
      mocks: {
        $t: key => key
      }
    })
    await flushPromises()

    // All options will be selected
    await wrapper.find('[data-testid="select-all-input"]').trigger('click')

    // All Options should be cleared
    await wrapper.find('[data-testid="select-all-input"]').trigger('click')

    expect(
      wrapper.findAll('.dropdown ul li input[type="checkbox"]').at(0)
        .element.checked
    ).toBe(false)
    expect(
      wrapper.findAll('.dropdown ul li input[type="checkbox"]').at(1)
        .element.checked
    ).toBe(false)
    expect(
      wrapper.findAll('.dropdown ul li input[type="checkbox"]').at(2)
        .element.checked
    ).toBe(false)
    expect(
      wrapper.find(
        '[data-testid="select-all-input"] input[type="checkbox"]'
      ).element.checked
    ).toBe(false)
  })
})

describe('Select.vue', () => {
  it('All options are cleared when clear-selection button is clicked', async () => {
    const options = [
      {
        Key: 'Value 1',
        Value: 'Value 1'
      },
      {
        Key: 'Value 2',
        Value: 'Value 2'
      },
      {
        Key: 'Value 3',
        Value: 'Value 3'
      }
    ]
    const value = [
      {
        Key: 'Value 1',
        Value: 'Value 1'
      },
      {
        Key: 'Value 2',
        Value: 'Value 2'
      },
      {
        Key: 'Value 3',
        Value: 'Value 3'
      }
    ]
    const wrapper = shallowMount(Select, {
      store: getStore(),
      localVue,
      propsData: { value: value, isMultiSelect: true, allOptions: options },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()

    await wrapper.find(
      '[data-testid="clear-selection"]'
    ).trigger('click')

    expect(wrapper.findAll('.dropdown ul li input[type="checkbox"]').at(0).element.checked).toBe(false)
    expect(wrapper.findAll('.dropdown ul li input[type="checkbox"]').at(1).element.checked).toBe(false)
    expect(wrapper.findAll('.dropdown ul li input[type="checkbox"]').at(2).element
      .checked).toBe(false)
    expect(
      wrapper.find('[data-testid="select-all-input"] input[type="checkbox"]')
        .element.checked
    ).toBe(false)
  })
})

describe('Select.vue', () => {
  it('Dropdown closed by default', async () => {
    const wrapper = shallowMount(Select, {
      store: getStore(),
      localVue,
      propsData: { },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    expect((wrapper.find('.dropdown>div').classes()).includes('show')).toBe(false)
  })
})

describe('Select.vue', () => {
  it('Dropdown open', async () => {
    const wrapper = shallowMount(Select, {
      store: getStore(),
      localVue,
      propsData: { },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    await wrapper.find('input').trigger('click')

    expect((wrapper.find('.dropdown>div').classes()).includes('show')).toBe(true)
  })
})

describe('Select.vue', () => {
  it('Displays no option when searched for value that is not present', async () => {
    const options = [
      {
        Key: 'Value 1',
        Value: 'Value 1'
      },
      {
        Key: 'Value 2',
        Value: 'Value 2'
      },
      {
        Key: 'Value 3',
        Value: 'Value 2'
      },
      {
        Key: 'Value 4',
        Value: 'Value 2'
      },
      {
        Key: 'Value 5',
        Value: 'Value 2'
      },
      {
        Key: 'Value 6',
        Value: 'Value 2'
      },
      {
        Key: 'Value 7',
        Value: 'Value 2'
      },
      {
        Key: 'Value 8',
        Value: 'Value 2'
      },
      {
        Key: 'Value 9',
        Value: 'Value 2'
      },
      {
        Key: 'Value 10',
        Value: 'Value 2'
      },
      {
        Key: 'Value 11',
        Value: 'Value 2'
      },
      {
        Key: 'Value 12',
        Value: 'Value 2'
      }
    ]
    const wrapper = shallowMount(Select, {
      store: getStore(),
      localVue,
      propsData: { allOptions: options },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    await wrapper.find('input').trigger('click')
    expect(wrapper.findAll('.dropdown ul li').length).toBe(options.length) // displaying all values more than 10
    const textInput = wrapper.find('.select-search-input')
    await textInput.setValue('sample search')
    expect(wrapper.findAll('.dropdown ul li').length).toBe(0)
  })
})

describe('Select.vue', () => {
  it('Dropdown being closed', async () => {
    const wrapper = shallowMount(Select, {
      store: getStore(),
      localVue,
      propsData: { },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    await wrapper.find('input').trigger('click')
    await wrapper.find('input').trigger('click')

    expect((wrapper.find('.dropdown>div').classes()).includes('show')).toBe(false)
  })
})

describe('Select.vue', () => {
  it('Dropdown being closed when clicked outside of the dropdown', async () => {
    const wrapper = shallowMount(Select, {
      store: getStore(),
      localVue,
      propsData: { name: 'Select' },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    await wrapper.find('input').trigger('click')
    await document.querySelector('body').click()
    expect((wrapper.find('.dropdown>div').classes()).includes('show')).toBe(false)
  })
})

describe('Select.vue', () => {
  it('Option selected', async () => {
    const options = [
      {
        Key: 'Value 1',
        Value: 'Value 1'
      },
      {
        Key: 'Value 2',
        Value: 'Value 2'
      }
    ]
    const wrapper = shallowMount(Select, {
      store: getStore(),
      localVue,
      propsData: { allOptions: options },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    await wrapper.find('input').trigger('click')

    await wrapper.findAll('.dropdown ul li').at(1).trigger('click')
    expect(wrapper.find('input').element.value).toBe('Value 2')
  })
})

describe('Select.vue', () => {
  it('Option selected in Multiselect', async () => {
    const options = [
      {
        Key: 'Value 1',
        Value: 'Value 1'
      },
      {
        Key: 'Value 2',
        Value: 'Value 2'
      }
    ]

    const wrapper = shallowMount(Select, {
      store: getStore(),
      localVue,
      propsData: { allOptions: options, isMultiSelect: true },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    await wrapper.find('input').trigger('click')

    await wrapper.findAll('.dropdown ul li').at(1).trigger('click')
    expect(wrapper.find('input').element.value).toBe('Value 2')
    expect(wrapper.findAll('.dropdown ul li input[type="checkbox"]').at(1).element.checked).toBe(true)
  })
})

describe('Select.vue', () => {
  it('Option deselected in Multiselect', async () => {
    const options = [
      {
        Key: 'Key1',
        Value: 'Value 1'
      },
      {
        Key: 'Key2',
        Value: 'Value 2'
      }
    ]

    const wrapper = shallowMount(Select, {
      store: getStoreWithActions({
        getFormData: async () => (
          Promise.resolve(
            formData
          )
        )
      }),
      localVue,
      propsData: {
        allOptions: options,
        isMultiSelect: true,
        value: [
          {
            Key: 'Key1',
            Value: 'Value 1'
          },
          {
            Key: 'Key2',
            Value: 'Value 2'
          }]
      },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    await wrapper.find('input').trigger('click')
    await wrapper.findAll('.dropdown ul li').at(1).trigger('click')
    expect(wrapper.find('input').element.value).toBe('Value 1')
    expect(wrapper.findAll('.dropdown ul li input[type="checkbox"]').at(0).element.checked).toBe(true)
    expect(wrapper.findAll('.dropdown ul li input[type="checkbox"]').at(1).element.checked).toBe(false)
  })
})

// describe('Select.vue with remoteURL', () => {
//   beforeEach(() => {
//     moxios.install()
//   })

//   it('Options getting populated from remoteURL', async () => {
//     moxios.install(api)
//     var sampleResponse = { Success: true, Data: [{ id: '1', value: 'Option 1' }, { id: '2', value: 'Option 2' }, { id: '3', value: 'Option 3' }, { id: '4', value: 'Option 4' }, { id: '5', value: 'Option 5' }, { id: '6', value: 'Option 6' }, { id: '7', value: 'Option 7' }, { id: '8', value: 'Option 8' }, { id: '9', value: 'Option 4' }, { id: '10', value: 'Option 5' }, { id: '11', value: 'Option 6' }, { id: '12', value: 'Option 7' }, { id: '13', value: 'Option 8' }], Error: null }

//     const wrapper = shallowMount(Select, {
//       propsData: {
//         name: 'Select',
//         allOptions: [],
//         remoteUrl: '/UnifiedHome/SampleRemoteCall'
//       },
//       store:getStore(),
//       localVue,
//       mocks: {
//         $t: (key) => key
//       }
//     })
//     moxios.wait(() => {
//         const request =  moxios.requests.mostRecent()
//         request.respondWith({
//           status: 200,
//           response: sampleResponse
//         }).then(function () {
//           expect(wrapper.findAll('.dropdown ul li').length).toBe(sampleResponse.Data.length)
//         })
//       },1000)
//     })
// })
describe('Select.vue', () => {
  it('check if validation is invoked on value change and error message not displayed if validation return true', async () => {
    const label = 'new_label'
    const formData = getNewForm()
    const dummyValidation = getDummyValidation()
    formData.Data.Form.Fields[0].Label = label
    formData.Data.Form.Fields[0].Validations.And = dummyValidation
    const postiveDummy = (_) => true
    const wrapper = mount(Form, {
      store: getStoreWithActions({
        getFormData: async () => (
          Promise.resolve(
            formData
          )
        )
      }),
      localVue,
      propsData: { isEditing: true, name: 'DummyForm' },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    await wrapper.findComponent(Select).setData({ validationMapper: { Dummy: postiveDummy } })
    await wrapper.findComponent(Select).find('input').trigger('click')
    await wrapper.findComponent(Select).findAll('.dropdown ul li').at(1).trigger('click')
    expect(wrapper.findComponent(Select).find('p.error-message').exists()).toBeFalsy()
  })
})

describe('Select.vue', () => {
  it('check if validation is invoked on value change and error message displayed', async () => {
    const label = 'new_label'
    const formData = getNewForm()
    const dummyValidation = getDummyValidation()
    formData.Data.Form.Fields[0].Label = label
    formData.Data.Form.Fields[0].Validations.And = dummyValidation
    const negativeDummy = (_) => false
    const wrapper = mount(Form, {
      store: getStoreWithActions({
        getFormData: async () => (
          Promise.resolve(
            formData
          )
        )
      }),
      localVue,
      propsData: { isEditing: true, name: 'DummyForm' },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    await wrapper.findComponent(Select).setData({ validationMapper: { Dummy: negativeDummy } })
    await wrapper.findComponent(Select).find('input').trigger('click')
    await wrapper.findComponent(Select).findAll('.dropdown ul li').at(1).trigger('click')
    // await wrapper.findComponent(Select).vm.$emit('input','a')
    // await wrapper.vm.$nextTick()
    expect(wrapper.findComponent(Select).find('p.error-message').text()).toMatch(dummyValidation[0].ErrorMessage)
  })
})

describe('Select.vue with remoteURL', () => {
  beforeEach(() => {
    moxios.install()
  })

  it('Options getting populated from remoteURL', async () => {
    moxios.install(api)
    var sampleResponse = { Success: true, Data: [{ id: '1', value: 'Option 1' }, { id: '2', value: 'Option 2' }, { id: '3', value: 'Option 3' }, { id: '4', value: 'Option 4' }, { id: '5', value: 'Option 5' }, { id: '6', value: 'Option 6' }, { id: '7', value: 'Option 7' }, { id: '8', value: 'Option 8' }, { id: '9', value: 'Option 4' }, { id: '10', value: 'Option 5' }, { id: '11', value: 'Option 6' }, { id: '12', value: 'Option 7' }, { id: '13', value: 'Option 8' }], Error: null }

    const wrapper = shallowMount(Select, {
      propsData: {
        name: 'Select',
        allOptions: [],
        remoteUrl: '/UnifiedHome/SampleRemoteCall'
      },
      store: getStore(),
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 200,
        response: sampleResponse
      }).then(function () {
        expect(wrapper.findAll('.dropdown ul li').length).toBe(sampleResponse.Data.length)
      })
    })
  })
})

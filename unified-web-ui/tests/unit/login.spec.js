import {
  createLocalVue,
  mount, shallowMount
} from '@vue/test-utils'
import Login from '@/app/Pages/home/components/Login.vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('Login page unit test', () => {
  let store
  let actions

  beforeEach(() => {
    actions = {
      setUserAuthenticated: jest.fn(),
      ssoLogout: jest.fn(),
      eraseCorruptedUserData: jest.fn()
    }
    store = new Vuex.Store({
      modules : {
        Login : {
          actions,
          state : {
            corruptedUserData : true
          }
        }
      }
    })
  })

  it('user authentication set correctly', () => {
    const wrapper = mount(Login, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    actions.setUserAuthenticated(true)
    expect(wrapper.isUserAuthenticated).toBeTruthy
  })

  it('invalid user log out correctly', () => {
    const wrapper = mount(Login, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    actions.ssoLogout()
    expect(wrapper.corruptedUserData).toBeFalsy
  })
})

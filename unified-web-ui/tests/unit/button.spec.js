import { shallowMount } from '@vue/test-utils'
import Button from '@/app/shared/components/Button.vue'

describe('Button.vue', () => {
  it('renders props.label when passed', () => {
    const label = 'Click me!'
    const wrapper = shallowMount(Button, {
      propsData: { label },
      mocks: {
        $t: (key) => key
      }
    })
    expect(wrapper.text()).toMatch(label)
  })

  it('emitts event when the button is clicked', async () => {
    const label = 'Click me!'
    const wrapper = shallowMount(Button, {
      propsData: { label },
      mocks: {
        $t: (key) => key
      }
    })
    await wrapper.find('button').trigger('click')
    expect(wrapper.emitted().click).toBeTruthy()
  })
})

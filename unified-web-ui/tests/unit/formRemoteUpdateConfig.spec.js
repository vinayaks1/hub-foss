import { mount, createLocalVue } from '@vue/test-utils'
import Form from '@/app/shared/components/Form.vue'
import Vuex from 'vuex'
import FormStore from '@/app/shared/store/modules/form'
import flushPromises from 'flush-promises'
import Vue from 'vue'
import testForm from './formRemoteUpdateConfigDataTest'
import Select from '@/app/shared/components/Select'

const localVue = createLocalVue()

localVue.use(Vuex)

jest.mock('../../src/utils/toastUtils', () => {
  return {
    __esModule: true,
    defaultToast: jest.fn(),
    ToastType: {
      Success: 'Success',
      Error: 'Error',
      Warning: 'Warning',
      Neutal: 'Neutal'
    }
  }
})
jest.useFakeTimers()
const methodActions = {
  getRemoteData: jest.fn()
}
let formattedActions
const getStore = (actions) => {
  formattedActions = {
    ...FormStore.actions,
    ...actions,
    ...methodActions
  }
  return new Vuex.Store({
    modules: {
      Form: {
        state: FormStore.state,
        actions: formattedActions,
        mutations: FormStore.mutations,
        namespaced: true
      }
    }
  })
}

describe('Form.vue', () => {
  afterEach(() => {
    jest.clearAllMocks()
  })
  var sampleResponse = { Success: true, Data: [{ Key: 'Key1', Value: 'Option 1' }, { Key: 'Key2', value: 'Option 2' }, { Key: 'Key3', value: 'Option 3' }], Error: null }
  it('testing remote update config for fields', async () => {
    const wrapper = mount(Form, {
      store: getStore({
        getFormData: async () => (
          Promise.resolve(
            testForm
          )
        )
      }),
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    formattedActions.getRemoteData.mockReturnValue(sampleResponse.Data)
    expect(wrapper.findAllComponents(Select).length).toBe(3)
    const selectComponentOne = wrapper.findAllComponents(Select).at(0)
    const selectComponentTwo = wrapper.findAllComponents(Select).at(1)
    expect(selectComponentOne.findAll('ul li').length).toBe(2)
    expect(selectComponentTwo.findAll('ul li').length).toBe(0)
    await selectComponentOne.findAll('ul li').at(0).trigger('click')
    await wrapper.vm.$nextTick()
    jest.runTimersToTime(300)
    await wrapper.vm.$nextTick()
    await wrapper.vm.$nextTick()
    expect(formattedActions.getRemoteData.mock.calls).toHaveLength(1)
  })

  it('testing remote update config for fields with multiple select values ', async () => {
    const wrapper = mount(Form, {
      store: getStore({
        getFormData: async () => (
          Promise.resolve(
            testForm
          )
        )
      }),
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    formattedActions.getRemoteData.mockReturnValue(sampleResponse.Data)
    expect(wrapper.findAllComponents(Select).length).toBe(3)
    const selectComponentMultiSelect = wrapper.findAllComponents(Select).at(2)
    expect(selectComponentMultiSelect.findAll('ul li').length).toBe(5)
    await selectComponentMultiSelect.findAll('.dropdown ul li').at(0).trigger('click')
    await selectComponentMultiSelect.findAll('.dropdown ul li').at(1).trigger('click')
    expect(selectComponentMultiSelect.find('input').element.value).toBe('Value 1,Value 2')
    expect(selectComponentMultiSelect.findAll('.dropdown ul li input[type="checkbox"]').at(0).element.checked).toBe(true)
    expect(selectComponentMultiSelect.findAll('.dropdown ul li input[type="checkbox"]').at(1).element.checked).toBe(true)
    jest.runTimersToTime(300)
    expect(formattedActions.getRemoteData.mock.calls).toHaveLength(2)
  })
})

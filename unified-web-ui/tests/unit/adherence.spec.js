import { shallowMount, createLocalVue } from '@vue/test-utils'
import App from '../../src/app/Pages/dashboard/patient/Tabs/Adherence/App.vue'
import GregorianCalendar from '../../src/app/Pages/dashboard/patient/Tabs/Adherence/GregorianCalendar.vue'
import Vuex from 'vuex'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import CalendarDay from '../../src/app/Pages/dashboard/patient/Tabs/Adherence/CalendarDay.vue'
import CalendarLegend from '../../src/app/Pages/dashboard/patient/Tabs/Adherence/CalendarLegend.vue'
import CalendarDaySpeechBubbleContent from '../../src/app/Pages/dashboard/patient/Tabs/Adherence/CalendayDaySpeechBubbleContent.vue'
import MultiCalendarMonthView from '../../src/app/Pages/dashboard/patient/Tabs/Adherence/MultiCalendarMonthView.vue'
import SpeechBubble from '../../src/app/Pages/dashboard/patient/Tabs/Adherence/SpeechBubble.vue'
import VueRouter from 'vue-router'
import Patient from '../../src/app/Pages/dashboard/patient/Patient.vue'
import Dashboard from '../../src/app/Pages/dashboard/Dashboard.vue'
import Modal from '@/app/shared/components/Modal'

describe('App.vue', () => {
  it('main app to check child component visibility', () => {
    const wrapper = shallowMount(App, {
      mocks: {
        $t: (key) => key
      }
    })
    expect(wrapper.findComponent(GregorianCalendar).isVisible()).toBe(true)
  })
})

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)
localVue.component('font-awesome-icon', FontAwesomeIcon)

describe('CalendarDay.vue', () => {
  let getters
  let store

  const codeToConfigMap = {
    2: {
      code: '2',
      codeName: 'MISSED',
      design: {
        color: '#990000',
        icon: null,
        iconColor: null
      },
      legendText: 'Manually Reported Missed Dose'
    },
    A: {
      code: 'B',
      codeName: 'TFN_REPEAT_RECEIVED_UNSURE',
      design: {
        color: '#FFFF00',
        icon: 'exclamation',
        iconColor: '#000000'
      },
      legendText: 'Digitally Reported (Using Same Toll Free Number & Shared Phone Number)'
    }
  }

  beforeEach(() => {
    getters = {
      getCodeToConfigMap: jest.fn()
    }
    store = new Vuex.Store({
      modules: {
        Adherence: {
          namespaced: true,
          getters
        }
      }
    })
  })

  const monthData = {
    monthName: 'JANUARY',
    monthNumber: 1,
    daysInMonth: 31,
    year: 2022,
    adherenceDataForCalendar: [
      {
        code: '2',
        dayName: 'WEDNESDAY',
        tagsForDay: []
      },
      {
        code: 'A',
        dayName: 'THURSDAY',
        tagsForDay: ['No_Coverage']
      }
    ]
  }

  it('basic classes check', () => {
    getters.getCodeToConfigMap.mockReturnValue(codeToConfigMap)
    const wrapper = shallowMount(CalendarDay, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      },
      propsData: {
        dayData: monthData.adherenceDataForCalendar[0],
        day: 10,
        monthData,
        firstDay: true,
        endDay: true
      }
    })
    expect(wrapper.find('.day-container').exists()).toBe(true)
    expect(wrapper.find('.day-icon').exists()).toBe(false)
    expect(wrapper.find('.start-gradient').exists()).toBe(true)
    expect(wrapper.find('.end-gradient').exists()).toBe(true)
  })

  it('mouse actions check', () => {
    const testId = 'main-page'
    const newDiv = document.createElement('div')
    newDiv.setAttribute('id', testId)
    document.body.appendChild(newDiv)
    getters.getCodeToConfigMap.mockReturnValue(codeToConfigMap)
    const wrapper = shallowMount(CalendarDay, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      },
      propsData: {
        dayData: monthData.adherenceDataForCalendar[0],
        day: 10,
        monthData
      }
    })
    wrapper.find('.day-container').trigger('click')
    wrapper.find('.day-container').trigger('mouseenter')
    wrapper.find('.day-container').trigger('mouseleave')
    expect(wrapper.emitted('day-click')).toBeTruthy()
    expect(wrapper.emitted('day-mouseover')).toBeTruthy()
    expect(wrapper.emitted('day-mouseleave')).toBeTruthy()
  })

  it('visibility checks', () => {
    getters.getCodeToConfigMap.mockReturnValue(codeToConfigMap)
    const wrapper = shallowMount(CalendarDay, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      },
      propsData: {
        dayData: monthData.adherenceDataForCalendar[1],
        day: 10,
        monthData
      }
    })
    expect(wrapper.find('.day-icon').exists()).toBe(true)
  })
})

describe('CalendarLegend.vue', () => {
  let getters
  let store
  let state
  let globalGetters

  const adherenceData = {
    lastDosage: null,
    technologyDoses: 0,
    manualDoses: 0,
    totalDoses: 22,
    adherenceMonthWiseData: [
      {
        monthName: 'FEBRUARY',
        monthNumber: 2,
        daysInMonth: 28,
        year: 2022,
        adherenceDataForCalendar: [
          {
            code: '6',
            dayName: 'TUESDAY',
            tagsForDay: []
          },
          {
            code: '6',
            dayName: 'WEDNESDAY',
            tagsForDay: []
          }
        ]
      }
    ]
  }

  const codeConfig = [
    {
      code: '5',
      codeName: 'RECEIVED_UNSURE',
      design: {
        color: '#ffff00',
        icon: null,
        iconColor: null
      },
      legendText: 'Digitally Reported (From Shared Phone Number)'
    },
    {
      code: 'B',
      codeName: 'TFN_REPEAT_RECEIVED_UNSURE',
      design: {
        color: '#FFFF00',
        icon: 'exclamation',
        iconColor: '#000000'
      },
      legendText: 'Digitally Reported (Using Same Toll Free Number & Shared Phone Number)'
    },
    {
      code: '6',
      codeName: 'NO_INFO',
      design: {
        color: '#ff7878',
        icon: null,
        iconColor: null
      },
      legendText: 'Unreported Dose'
    }
  ]

  var codeConfigMap = {
    5: codeConfig[0],
    B: codeConfig[1],
    6: codeConfig[2]
  }

  var accessibilityCodeMap = {
    1: '#009E73',
    2: '#D55E00',
    3: '#0072B2',
    4: '#0072B2',
    5: '#F0E442',
    6: '#CC79A7',
    7: 'transparent',
    8: '#009E73',
    9: '#56B4E9',
    A: '#0072B2',
    B: '#F0E442',
    C: '#E69F00',
    D: '#56B4E9',
    E: '#D55E00'
  }

  beforeEach(() => {
    getters = {
      adherenceData: jest.fn(),
      adherenceCodeConfig: jest.fn(),
      accessibilityCodeToColorMap: jest.fn(),
      getCodeToConfigMap: jest.fn()
    }
    globalGetters = {
      accessibilityModeState: jest.fn()
    }
    state = {
      patientData: {
        MonitoringMethod: '99DOTS'
      }
    }
    store = new Vuex.Store({
      modules: {
        Adherence: {
          namespaced: true,
          getters
        },
        Patient: {
          namespaced: true,
          state
        }
      },
      getters: globalGetters
    })
  })

  it('basic classes checks', () => {
    getters.adherenceData.mockReturnValue(adherenceData)
    getters.adherenceCodeConfig.mockReturnValue(codeConfig)
    getters.accessibilityCodeToColorMap.mockReturnValue(accessibilityCodeMap)
    globalGetters.accessibilityModeState.mockReturnValue(true)
    const wrapper = shallowMount(CalendarLegend, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    expect(wrapper.find('.calendar-legend').exists()).toBe(true)
    expect(wrapper.find('.legend-info').exists()).toBe(true)
    expect(wrapper.find('.legend-color').exists()).toBe(true)
  })

  it('legned show count true', () => {
    getters.adherenceData.mockReturnValue(adherenceData)
    getters.adherenceCodeConfig.mockReturnValue(codeConfig)
    getters.accessibilityCodeToColorMap.mockReturnValue(accessibilityCodeMap)
    globalGetters.accessibilityModeState.mockReturnValue(true)
    const wrapper = shallowMount(CalendarLegend, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      },
      propsData: {
        showCounts: true
      }
    })
    expect(wrapper.find('.code-count').exists()).toBe(true)
  })

  it('MERM MonitoringMethod Test', () => {
    state.patientData.MonitoringMethod = 'MERM'
    getters.adherenceData.mockReturnValue(adherenceData)
    getters.adherenceCodeConfig.mockReturnValue(codeConfig)
    getters.accessibilityCodeToColorMap.mockReturnValue(accessibilityCodeMap)
    globalGetters.accessibilityModeState.mockReturnValue(true)
    const wrapper = shallowMount(CalendarLegend, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      },
      propsData: {
        showCounts: true
      }
    })
    expect(wrapper.find('.code-count').exists()).toBe(true)
  })

  it('Patient Data Null', () => {
    state.patientData = null
    getters.adherenceData.mockReturnValue(adherenceData)
    getters.adherenceCodeConfig.mockReturnValue(codeConfig)
    getters.accessibilityCodeToColorMap.mockReturnValue(accessibilityCodeMap)
    globalGetters.accessibilityModeState.mockReturnValue(true)
    const wrapper = shallowMount(CalendarLegend, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      },
      propsData: {
        showCounts: false
      }
    })
    expect(wrapper.find('.code-count').exists()).toBe(false)
  })

  it('VOT MonitoringMethod Tests', () => {
    state.patientData.MonitoringMethod = 'VOT'
    getters.adherenceData.mockReturnValue(adherenceData)
    getters.adherenceCodeConfig.mockReturnValue(codeConfig)
    getters.accessibilityCodeToColorMap.mockReturnValue(accessibilityCodeMap)
    globalGetters.accessibilityModeState.mockReturnValue(true)
    const wrapper = shallowMount(CalendarLegend, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      },
      propsData: {
        showCounts: true
      }
    })
    expect(wrapper.find('.code-count').exists()).toBe(true)
  })

  it('99DOTSLITE MonitoringMethod Test', () => {
    state.patientData.MonitoringMethod = '99DOTSLITE'
    getters.adherenceData.mockReturnValue(adherenceData)
    getters.adherenceCodeConfig.mockReturnValue(codeConfig)
    getters.accessibilityCodeToColorMap.mockReturnValue(accessibilityCodeMap)
    getters.getCodeToConfigMap.mockReturnValue(codeConfigMap)
    globalGetters.accessibilityModeState.mockReturnValue(false)
    const wrapper = shallowMount(CalendarLegend, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      },
      propsData: {
        showCounts: true
      }
    })
    expect(wrapper.find('.code-count').exists()).toBe(true)
  })

  it('adherence data 0 length', () => {
    state.patientData.MonitoringMethod = '99DOTSLITE'
    getters.adherenceData.mockReturnValue({})
    getters.adherenceCodeConfig.mockReturnValue(codeConfig)
    getters.accessibilityCodeToColorMap.mockReturnValue(accessibilityCodeMap)
    globalGetters.accessibilityModeState.mockReturnValue(true)
    const wrapper = shallowMount(CalendarLegend, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      },
      propsData: {
        showCounts: true
      }
    })
    expect(wrapper.find('.code-count').exists()).toBe(true)
  })
})

describe('CalendarDaySpeechBubbleContent.vue', () => {
  let getters
  let store
  let globalGetters

  const codeToConfigMap = {
    2: {
      code: '2',
      codeName: 'MISSED',
      design: {
        color: '#990000',
        icon: null,
        iconColor: null
      },
      legendText: 'Manually Reported Missed Dose'
    },
    A: {
      code: 'B',
      codeName: 'TFN_REPEAT_RECEIVED_UNSURE',
      design: {
        color: '#FFFF00',
        icon: 'exclamation',
        iconColor: '#000000'
      },
      legendText: 'Digitally Reported (Using Same Toll Free Number & Shared Phone Number)'
    }
  }

  var accessibilityCodeMap = {
    1: '#009E73',
    2: '#D55E00',
    3: '#0072B2',
    4: '#0072B2',
    5: '#F0E442',
    6: '#CC79A7',
    7: 'transparent',
    8: '#009E73',
    9: '#56B4E9',
    A: '#0072B2',
    B: '#F0E442',
    C: '#E69F00',
    D: '#56B4E9',
    E: '#D55E00'
  }

  beforeEach(() => {
    getters = {
      getCodeToConfigMap: jest.fn(),
      accessibilityCodeToColorMap: jest.fn()
    }
    globalGetters = {
      accessibilityModeState: jest.fn()
    }
    store = new Vuex.Store({
      modules: {
        Adherence: {
          namespaced: true,
          getters
        }
      },
      getters: globalGetters
    })
  })

  it('basic classes checks', () => {
    getters.getCodeToConfigMap.mockReturnValue(codeToConfigMap)
    getters.accessibilityCodeToColorMap.mockReturnValue(accessibilityCodeMap)
    globalGetters.accessibilityModeState.mockReturnValue(true)
    const wrapper = shallowMount(CalendarDaySpeechBubbleContent, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      },
      propsData: {
        doseType: '2',
        heading: 'Speech Bubble!',
        items: []
      }
    })
    expect(wrapper.find('.calendar-day-sb-header b').text()).toMatch('Speech Bubble!')
    expect(wrapper.find('.calendar-day-sb-header').exists()).toBe(true)
    expect(wrapper.find('.dose-color').exists()).toBe(true)
    expect(wrapper.find('.calendar-day-sb-content').exists()).toBe(false)
    expect(wrapper.find('.list-group').exists()).toBe(false)
    expect(wrapper.find('.list-group-item').exists()).toBe(false)
  })

  it('visibility classes checks', () => {
    getters.getCodeToConfigMap.mockReturnValue(codeToConfigMap)
    const wrapper = shallowMount(CalendarDaySpeechBubbleContent, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      },
      propsData: {
        doseType: '2',
        heading: 'Speech Bubble!',
        items: ['A']
      }
    })
    expect(wrapper.find('.calendar-day-sb-header').exists()).toBe(true)
    expect(wrapper.find('.dose-color').exists()).toBe(true)
    expect(wrapper.find('.calendar-day-sb-content').exists()).toBe(true)
    expect(wrapper.find('.list-group').exists()).toBe(true)
    expect(wrapper.find('.list-group-item').exists()).toBe(true)
  })
})

describe('GregorianCalendar.vue', () => {
  let getters
  let store
  let actions
  let state

  const monthData = {
    monthName: 'JANUARY',
    monthNumber: 1,
    daysInMonth: 31,
    year: 2022,
    adherenceDataForCalendar: [
      {
        code: '2',
        dayName: 'WEDNESDAY',
        tagsForDay: []
      },
      {
        code: 'A',
        dayName: 'THURSDAY',
        tagsForDay: ['No_Coverage']
      }
    ]
  }

  const adherenceData = {
    lastDosage: null,
    technologyDoses: 0,
    manualDoses: 0,
    totalDoses: 22,
    adherenceMonthWiseData: [
      {
        monthName: 'FEBRUARY',
        monthNumber: 2,
        daysInMonth: 28,
        year: 2022,
        adherenceDataForCalendar: [
          {
            code: '6',
            dayName: 'TUESDAY',
            tagsForDay: []
          },
          {
            code: '6',
            dayName: 'WEDNESDAY',
            tagsForDay: []
          }
        ]
      }
    ]
  }

  const codeConfig = [
    {
      code: '5',
      codeName: 'RECEIVED_UNSURE',
      design: {
        color: '#ffff00',
        icon: null,
        iconColor: null
      },
      legendText: 'Digitally Reported (From Shared Phone Number)'
    },
    {
      code: 'B',
      codeName: 'TFN_REPEAT_RECEIVED_UNSURE',
      design: {
        color: '#FFFF00',
        icon: 'exclamation',
        iconColor: '#000000'
      },
      legendText: 'Digitally Reported (Using Same Toll Free Number & Shared Phone Number)'
    },
    {
      code: '6',
      codeName: 'NO_INFO',
      design: {
        color: '#ff7878',
        icon: null,
        iconColor: null
      },
      legendText: 'Unreported Dose'
    }
  ]

  const tagsConfig = [
    {
      tagName: 'Registration_through_Screening_Tool',
      tagDisplayName: 'Registration through Screening Tool',
      tagGroup: 'Externally_Enrolled'
    },
    {
      tagName: 'Wrong_number',
      tagDisplayName: 'Wrong number',
      tagGroup: 'Phone_issues'
    },
    {
      tagName: 'End_Date_Passed',
      tagDisplayName: 'End date passed',
      tagGroup: 'Automatic_Tags'
    }
  ]

  const router = new VueRouter({
    routes: [
      {
        path: '/dashboard',
        name: 'dashboard',
        component: Dashboard,
        children: [
          {
            path: 'patient/:patientId',
            name: 'patient',
            component: Patient
          }
        ]
      }
    ]
  })

  beforeEach(() => {
    getters = {
      adherenceData: jest.fn(),
      adherenceCodeConfig: jest.fn(),
      tagsConfig: jest.fn()
    }
    actions = {
      getPatientMonthwiseAdherence: jest.fn(),
      markDoses: jest.fn(),
      addTags: jest.fn(),
      removeTags: jest.fn(),
      getTagsConfig: jest.fn()
    }
    state = {
      patientData: {
        Stage: 'ON_TREATMENT',
        EndDate: null
      }
    }
    store = new Vuex.Store({
      modules: {
        Adherence: {
          namespaced: true,
          getters,
          actions
        },
        Patient: {
          namespaced: true,
          state
        }
      }
    })
  })

  // push to router so that .vue page does not throw NPE for $route.params
  router.push('/dashboard/patient/12345')

  it('basic classes checks', () => {
    getters.adherenceData.mockReturnValue(adherenceData)
    getters.adherenceCodeConfig.mockReturnValue(codeConfig)
    getters.tagsConfig.mockReturnValue(tagsConfig)
    state.patientData.EndDate = '2020-01-01T18:30:00'
    const wrapper = shallowMount(GregorianCalendar, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      },
      router
    })

    expect(wrapper.find('.info_box').exists()).toBe(true)
    expect(wrapper.find('.action_buttons').exists()).toBe(true)
    expect(wrapper.find('.action-positive-btns').exists()).toBe(true)
    expect(wrapper.find('.alter-tags').exists()).toBe(true)
    expect(wrapper.find('.greg_table').exists()).toBe(true)
    expect(wrapper.findComponent(CalendarLegend).exists()).toBe(false)
    expect(wrapper.findComponent(SpeechBubble).exists()).toBe(false)
  })

  it('legend visible on toggle', () => {
    getters.adherenceData.mockReturnValue(adherenceData)
    getters.adherenceCodeConfig.mockReturnValue(codeConfig)
    getters.tagsConfig.mockReturnValue(tagsConfig)
    state.patientData.EndDate = '2020-01-01T18:30:00'
    const wrapper = shallowMount(GregorianCalendar, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      },
      data () {
        return {
          showLegend: true
        }
      },
      router
    })

    expect(wrapper.findComponent(CalendarLegend).exists()).toBe(true)
  })

  it('positiveAction false check', () => {
    getters.adherenceData.mockReturnValue(adherenceData)
    getters.adherenceCodeConfig.mockReturnValue(codeConfig)
    getters.tagsConfig.mockReturnValue(tagsConfig)
    const wrapper = shallowMount(GregorianCalendar, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      },
      router,
      data () {
        return {
          positiveAction: false
        }
      }
    })
    expect(wrapper.find('.action-positive-btns').exists()).toBe(false)
    expect(wrapper.find('.manual-marking-confirm-canel').exists()).toBe(true)
  })

  it('calendar-day click code 6', () => {
    getters.adherenceData.mockReturnValue(adherenceData)
    getters.adherenceCodeConfig.mockReturnValue(codeConfig)
    getters.tagsConfig.mockReturnValue(tagsConfig)
    const wrapper = shallowMount(GregorianCalendar, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      },
      router,
      data () {
        return {
          positiveAction: false
        }
      }
    })
    wrapper.vm.performDayClickAction(1,
      {
        code: '6',
        dayName: 'TUESDAY',
        tagsForDay: []
      },
      adherenceData.adherenceMonthWiseData[0].adherenceDataForCalendar
    )
    expect('MARK_MANUAL_DOSE' in wrapper.vm.onHoverComponentList).toBe(true)
    expect('MARK_MISSED_DOSE' in wrapper.vm.onHoverComponentList).toBe(true)
    expect('ADD_TAGS' in wrapper.vm.onHoverComponentList).toBe(true)
    expect('REMOVE_TAGS' in wrapper.vm.onHoverComponentList).toBe(true)
    expect(wrapper.find('.action-positive-btns').exists()).toBe(false)
    expect(wrapper.find('.manual-marking-confirm-canel').exists()).toBe(true)
  })

  it('calendar-day click code 2', () => {
    getters.adherenceData.mockReturnValue(adherenceData)
    getters.adherenceCodeConfig.mockReturnValue(codeConfig)
    getters.tagsConfig.mockReturnValue(tagsConfig)
    const wrapper = shallowMount(GregorianCalendar, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      },
      router,
      data () {
        return {
          positiveAction: false
        }
      }
    })
    wrapper.vm.performDayClickAction(1,
      {
        code: '2',
        dayName: 'TUESDAY',
        tagsForDay: []
      },
      adherenceData.adherenceMonthWiseData[0].adherenceDataForCalendar
    )
    expect('REMOVE_MISSED_DOSE' in wrapper.vm.onHoverComponentList).toBe(true)
    expect('ADD_TAGS' in wrapper.vm.onHoverComponentList).toBe(true)
    expect('REMOVE_TAGS' in wrapper.vm.onHoverComponentList).toBe(true)
    expect(wrapper.find('.action-positive-btns').exists()).toBe(false)
    expect(wrapper.find('.manual-marking-confirm-canel').exists()).toBe(true)
  })

  it('calendar-day click toggle', () => {
    getters.adherenceData.mockReturnValue(adherenceData)
    getters.adherenceCodeConfig.mockReturnValue(codeConfig)
    getters.tagsConfig.mockReturnValue(tagsConfig)
    const wrapper = shallowMount(GregorianCalendar, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      },
      router,
      data () {
        return {
          positiveAction: false
        }
      }
    })
    wrapper.vm.performDayClickAction(1,
      {
        code: '9',
        dayName: 'TUESDAY',
        tagsForDay: []
      },
      adherenceData.adherenceMonthWiseData[0].adherenceDataForCalendar
    )
    expect('REMOVE_MANUAL_DOSE' in wrapper.vm.onHoverComponentList).toBe(true)
    expect('ADD_TAGS' in wrapper.vm.onHoverComponentList).toBe(true)
    expect('REMOVE_TAGS' in wrapper.vm.onHoverComponentList).toBe(true)
    expect(wrapper.find('.action-positive-btns').exists()).toBe(false)
    expect(wrapper.find('.manual-marking-confirm-canel').exists()).toBe(true)
  })

  it('calendar-day click code 9', async () => {
    getters.adherenceData.mockReturnValue(adherenceData)
    getters.adherenceCodeConfig.mockReturnValue(codeConfig)
    getters.tagsConfig.mockReturnValue(tagsConfig)
    const wrapper = shallowMount(GregorianCalendar, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      },
      router,
      data () {
        return {
          positiveAction: false
        }
      }
    })
    await wrapper.vm.performDayClickAction(1, { code: '9', dayName: 'TUESDAY', tagsForDay: [] }, adherenceData.adherenceMonthWiseData[0].adherenceDataForCalendar)
    await wrapper.vm.performDayClickAction(1, { code: '9', dayName: 'TUESDAY', tagsForDay: [] }, adherenceData.adherenceMonthWiseData[0].adherenceDataForCalendar)
    expect(wrapper.find('.action-positive-btns').exists()).toBe(true)
    expect(wrapper.find('.manual-marking-confirm-canel').exists()).toBe(false)
    expect(wrapper.vm.doseMarkingByBtn).toBe(false)
    expect(wrapper.vm.positiveAction).toBe(true)
    expect(wrapper.vm.codesSelected).toHaveLength(0)
    expect(wrapper.vm.unknwonDateSelection).toBe(false)
    expect(wrapper.vm.highlightCodes).toHaveLength(0)
    expect(wrapper.vm.tagsSelection).toBe(false)
    expect(wrapper.vm.currentDoseMarkingState).toMatch('')
    expect(wrapper.vm.disableModalConfirmBtn).toBe(false)
    expect(wrapper.vm.selectedTags).toHaveLength(0)
  })

  it('speech bubble mouse enter', () => {
    getters.adherenceData.mockReturnValue(adherenceData)
    getters.adherenceCodeConfig.mockReturnValue(codeConfig)
    getters.tagsConfig.mockReturnValue(tagsConfig)
    const wrapper = shallowMount(GregorianCalendar, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      },
      router,
      data () {
        return {
          speechBubbleHeading: 'Hello',
        }
      }
    })
    wrapper.vm.performDayMouseoverAction(null, {
      pointX: 10,
      pointY: 10
    }, 12,
    { code: '9', dayName: 'TUESDAY', tagsForDay: [] }, monthData)
    setTimeout(() => {
      expect(wrapper.findComponent(CalendarDaySpeechBubbleContent).isVisible())
    }, 1000)
  })

  it('speech bubble mouse leave', async () => {
    getters.adherenceData.mockReturnValue(adherenceData)
    getters.adherenceCodeConfig.mockReturnValue(codeConfig)
    getters.tagsConfig.mockReturnValue(tagsConfig)
    const wrapper = shallowMount(GregorianCalendar, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      },
      router,
      data () {
        return {
          showSpeechBubble: true,
          leftCoordinates: 0,
          topCoordinates: 0,
          speechBubbleHovering: false,
          lastPoints: {
            X: 0, Y: 0
          },
          speechBubbleItems: ['A', 'B', 'C'],
          speechBubbleHeading: 'Hello',
          speechBubbleDoseType: 'How are you',
          speechBubbleHeight: 100
        }
      }
    })
    await wrapper.vm.performDayMouseLeaveAction()
    expect(wrapper.find('.speech-bubble-greg-cal').exists()).toBe(false)
  })

  it('speech bubble mouse leave', async () => {
    getters.adherenceData.mockReturnValue(adherenceData)
    getters.adherenceCodeConfig.mockReturnValue(codeConfig)
    getters.tagsConfig.mockReturnValue(tagsConfig)
    const wrapper = shallowMount(GregorianCalendar, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      },
      router,
      data () {
        return {
          showSpeechBubble: false,
          leftCoordinates: 0,
          topCoordinates: 0,
          speechBubbleHovering: false,
          lastPoints: {
            X: 0, Y: 0
          },
          speechBubbleItems: ['A', 'B', 'C'],
          speechBubbleHeading: 'Hello',
          speechBubbleDoseType: 'How are you',
          speechBubbleHeight: 100
        }
      }
    })
    wrapper.vm.showSpeechBubble = true
    wrapper.vm.speechBubbleHeight = 19
    await wrapper.vm.$nextTick()
    expect(wrapper.find('.speech-bubble-greg-cal').exists()).toBe(true)
  })

  it('maxDaysInGivenMonths test', () => {
    getters.adherenceData.mockReturnValue(adherenceData)
    getters.adherenceCodeConfig.mockReturnValue(codeConfig)
    getters.tagsConfig.mockReturnValue(tagsConfig)
    const wrapper = shallowMount(GregorianCalendar, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      },
      router
    })
    var max = wrapper.vm.maxDaysInGivenMonths.toString()
    expect(max).toMatch('28')
  })

  it('showModal true test', () => {
    getters.adherenceData.mockReturnValue(adherenceData)
    getters.adherenceCodeConfig.mockReturnValue(codeConfig)
    getters.tagsConfig.mockReturnValue(tagsConfig)
    const wrapper = shallowMount(GregorianCalendar, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      },
      router,
      data () {
        return {
          showModal: true
        }
      }
    })
    expect(wrapper.findComponent(Modal).exists()).toBe(true)
  })

  it('modalClickConfirm MARK_MANUAL_DOSE test', async () => {
    getters.adherenceData.mockReturnValue(adherenceData)
    getters.adherenceCodeConfig.mockReturnValue(codeConfig)
    getters.tagsConfig.mockReturnValue(tagsConfig)
    const wrapper = shallowMount(GregorianCalendar, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      },
      router,
      data () {
        return {
          showModal: true,
          currentDoseMarkingState: 'MARK_MANUAL_DOSE'
        }
      }
    })
    await wrapper.vm.modalClickConfirm()
    expect(actions.markDoses).toBeCalled()
  })

  it('modalClickConfirm MARK_MISSED_DOSE test', async () => {
    getters.adherenceData.mockReturnValue(adherenceData)
    getters.adherenceCodeConfig.mockReturnValue(codeConfig)
    getters.tagsConfig.mockReturnValue(tagsConfig)
    const wrapper = shallowMount(GregorianCalendar, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      },
      router,
      data () {
        return {
          showModal: true,
          currentDoseMarkingState: 'MARK_MISSED_DOSE'
        }
      }
    })
    await wrapper.vm.modalClickConfirm()
    expect(actions.markDoses).toBeCalled()
  })

  it('modalClickConfirm ADD_TAGS test', async () => {
    getters.adherenceData.mockReturnValue(adherenceData)
    getters.adherenceCodeConfig.mockReturnValue(codeConfig)
    getters.tagsConfig.mockReturnValue(tagsConfig)
    const wrapper = shallowMount(GregorianCalendar, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      },
      router,
      data () {
        return {
          showModal: true,
          currentDoseMarkingState: 'ADD_TAGS'
        }
      }
    })
    await wrapper.vm.modalClickConfirm()
    expect(actions.addTags).toBeCalled()
  })

  it('modalClickConfirm REMOVE_TAGS test', async () => {
    getters.adherenceData.mockReturnValue(adherenceData)
    getters.adherenceCodeConfig.mockReturnValue(codeConfig)
    getters.tagsConfig.mockReturnValue(tagsConfig)
    const wrapper = shallowMount(GregorianCalendar, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      },
      router,
      data () {
        return {
          showModal: true,
          currentDoseMarkingState: 'REMOVE_TAGS'
        }
      }
    })
    await wrapper.vm.modalClickConfirm()
    expect(actions.removeTags).toBeCalled()
  })

  it('addRemoveTags test', async () => {
    getters.adherenceData.mockReturnValue(adherenceData)
    getters.adherenceCodeConfig.mockReturnValue(codeConfig)
    getters.tagsConfig.mockReturnValue(tagsConfig)
    const wrapper = shallowMount(GregorianCalendar, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      },
      router,
      data () {
        return {
          showModal: true,
          currentDoseMarkingState: 'REMOVE_TAGS',
          confirmBtnText: ''
        }
      }
    })
    await wrapper.vm.addRemoveTags(1, 'ADD_TAGS')
    expect(wrapper.vm.currentDoseMarkingState).toMatch('ADD_TAGS')
  })

  it('selectOnHoverAction showmodal test', async () => {
    getters.adherenceData.mockReturnValue(adherenceData)
    getters.adherenceCodeConfig.mockReturnValue(codeConfig)
    getters.tagsConfig.mockReturnValue(tagsConfig)
    const wrapper = shallowMount(GregorianCalendar, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      },
      router,
      data () {
        return {
          showModal: false,
          currentDoseMarkingState: 'REMOVE_TAGS'
        }
      }
    })
    await wrapper.vm.selectOnHoverAction(1, 'ADD_TAGS')
    expect(wrapper.vm.showModal).toBe(true)
  })

  it('performLegenedMouseClick test', async () => {
    getters.adherenceData.mockReturnValue(adherenceData)
    getters.adherenceCodeConfig.mockReturnValue(codeConfig)
    getters.tagsConfig.mockReturnValue(tagsConfig)
    const wrapper = shallowMount(GregorianCalendar, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      },
      router,
      data () {
        return {
          unknwonDateSelection: false,
          doseMarkingByBtn: false,
          highlightCodes: ''
        }
      }
    })
    await wrapper.vm.performLegenedMouseClick('6')
    expect(wrapper.vm.highlightCodes).toMatch('6')
  })

  it('performDayMouseoverCtrlAction test', async () => {
    getters.adherenceData.mockReturnValue(adherenceData)
    getters.adherenceCodeConfig.mockReturnValue(codeConfig)
    getters.tagsConfig.mockReturnValue(tagsConfig)
    const wrapper = shallowMount(GregorianCalendar, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      },
      router,
      data () {
        return {
          unknwonDateSelection: false,
          doseMarkingByBtn: false
        }
      }
    })
    await wrapper.vm.performDayMouseoverCtrlAction(12, { code: '9', dayName: 'TUESDAY', tagsForDay: [] }, monthData)
    expect('REMOVE_MANUAL_DOSE' in wrapper.vm.onHoverComponentList).toBe(true)
  })

  it('modalClickConfirm REMOVE_TAGS test', async () => {
    getters.adherenceData.mockReturnValue(adherenceData)
    getters.adherenceCodeConfig.mockReturnValue(codeConfig)
    getters.tagsConfig.mockReturnValue(tagsConfig)
    const wrapper = shallowMount(GregorianCalendar, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      },
      router,
      data () {
        return {
          codesSelected: Array(61).fill('2')
        }
      }
    })
    await wrapper.vm.performDayClickAction(12, { code: '9', dayName: 'TUESDAY', tagsForDay: [] }, monthData)
  })
})

describe('MultiCalendarMonthView.vue', () => {
  const adherenceDataMonthly = { success: true, data: [{ lastDosage: null, technologyDoses: 0, manualDoses: 2, totalDoses: 49, adherenceMonthWiseData: [{ monthName: 'JANUARY', monthNumber: 1, daysInMonth: 31, year: 2022, adherenceDataForCalendar: [{ code: '7', dayName: 'SATURDAY', tagsForDay: [] }, { code: '7', dayName: 'SUNDAY', tagsForDay: [] }] }, { monthName: 'FEBRUARY', monthNumber: 2, daysInMonth: 28, year: 2022, adherenceDataForCalendar: [{ code: '6', dayName: 'TUESDAY', tagsForDay: [] }, { code: '6', dayName: 'WEDNESDAY', tagsForDay: [] }] }] }, { lastDosage: null, technologyDoses: 0, manualDoses: 0, totalDoses: 7, adherenceMonthWiseData: [{ monthName: 'FEBRUARY', monthNumber: 2, daysInMonth: 28, year: 2022, adherenceDataForCalendar: [{ code: '7', dayName: 'TUESDAY', tagsForDay: [] }, { code: '7', dayName: 'WEDNESDAY', tagsForDay: [] }] }] }, { lastDosage: null, technologyDoses: 0, manualDoses: 0, totalDoses: 21, adherenceMonthWiseData: [{ monthName: 'FEBRUARY', monthNumber: 2, daysInMonth: 28, year: 2022, adherenceDataForCalendar: [{ code: '7', dayName: 'TUESDAY', tagsForDay: [] }, { code: '6', dayName: 'TUESDAY', tagsForDay: [] }] }] }] }
  const uppData = { TotalPatients: 4, TodayInfo: { month: 1, year: 2021, dayNumber: 5, dayName: 'TUE', monthName: 'March', daysInMonth: 31 }, EarliestEnrollment: '2019-05-01T00:00:00', Columns: { Id: 'patient_id_web', PatientName: 'patient_name', MonitoringMethod: 'adherence_technology', DigitalDoses: 'digitally_confirmed_adherence', DigitalManual: 'digital_manual_adherence', Phone: 'primary_phone_number', CurrentTags: 'current_tags', TBTreatmentStartDate: 'treatment_start_date', MissedDoses: 'doses_not_reported', Facility: 'Facility', EnrollmentDate: 'dat_start_date', ExternalId1: 'medical_record_number', EndDate: 'treatment_end_date', TotalTreatmentDays: 'total_treatment_days', PatientType: '_patient_type', LastDosage: 'last_dat_event', DeviceIMEI: 'device_imei_merm', BatteryLevel: 'battery_level_merm' }, Patients: [{ Id: 312579, PatientName: 'vot-test ', ExternalId1: null, Facility: 'DEMO_TU', MonitoringMethod: 'VOT', Phone: '9876543210', EnrollmentDate: '3/24/2022', TBTreatmentStartDate: '3/24/2022', EndDate: '9/8/2022', TotalTreatmentDays: 168, MissedDoses: 176, DigitalDoses: '0%', DigitalManual: '0%', LastDosage: '3/24/2022', CurrentTags: 'New_Enrollment', TreatmentOutcome: null, AdherenceString: '6666666666', C_PatientTag: 'New_Enrollment', DeviceIMEI: '0', BatteryLevel: '0 %', MonthWiseAllAdherenceData: adherenceDataMonthly.data[0], AdherenceResults: [{ Item1: 'no_info ', Item2: 'Monday March 3/28/2022 12:00:00 AM, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Tuesday March 3/29/2022 12:00:00 AM, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Wednesday March 3/30/2022 12:00:00 AM, 2022: No tags added' }, { Item1: 'no_info ', Item2: 'Thursday March 3/31/2022 12:00:00 AM, 2022: No tags added' }], Clickable: true, ClickableLink: 'Patient/312579' }] }

  let getters
  let store
  let actions
  let actionsAdherence
  let state

  beforeEach(() => {
    getters = {
      uppPatientData: jest.fn(),
      todaysInfo: jest.fn()
    }
    actions = {
      getPatients: jest.fn(),
      setCurrentPage: jest.fn()
    }
    actionsAdherence = {
      getAdherenceCodeConfig: jest.fn()
    }
    state = {
      numberPages: 10,
      selectedPage: 2,
      isPatientsLoading: false,
      totalPatients: 1000,
      pageSize: 100
    }
    store = new Vuex.Store({
      modules: {
        Adherence: {
          namespaced: true,
          actions: actionsAdherence
        },
        UnifiedPatient: {
          namespaced: true,
          getters,
          actions,
          state
        }
      }
    })
  })

  it('basic classes test', () => {
    actionsAdherence.getAdherenceCodeConfig.mockReturnValue(adherenceDataMonthly)
    getters.uppPatientData.mockReturnValue(uppData.Patients)
    getters.todaysInfo.mockReturnValue(uppData.TodayInfo)
    const wrapper = shallowMount(MultiCalendarMonthView, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    expect(wrapper.find('.multi-cal-header').exists()).toBe(true)
    expect(wrapper.find('.upp-cal-container').exists()).toBe(true)
  })

  it('Page turner test', () => {
    actionsAdherence.getAdherenceCodeConfig.mockReturnValue(adherenceDataMonthly)
    getters.uppPatientData.mockReturnValue(uppData.Patients)
    getters.todaysInfo.mockReturnValue(uppData.TodayInfo)
    const wrapper = shallowMount(MultiCalendarMonthView, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    wrapper.vm.changePage(10)
    expect(wrapper.vm.availableMonths.length).toEqual(0)
  })

  it('Legend click test', () => {
    actionsAdherence.getAdherenceCodeConfig.mockReturnValue(adherenceDataMonthly)
    getters.uppPatientData.mockReturnValue(uppData.Patients)
    getters.todaysInfo.mockReturnValue(uppData.TodayInfo)
    const wrapper = shallowMount(MultiCalendarMonthView, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    wrapper.vm.legendBtnClickAction()
    expect(wrapper.vm.showLegend).toBe(true)
  })

  it('Legend click test', () => {
    actionsAdherence.getAdherenceCodeConfig.mockReturnValue(adherenceDataMonthly)
    getters.uppPatientData.mockReturnValue(uppData.Patients)
    getters.todaysInfo.mockReturnValue(uppData.TodayInfo)
    const wrapper = shallowMount(MultiCalendarMonthView, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      },
      data () {
        return {
          dayInfo: {
            month: 'Jan'
          }
        }
      }
    })
    wrapper.vm.todayBtnClickAction()
    expect(wrapper.vm.currMonth.month).toEqual('Jan')
  })
})

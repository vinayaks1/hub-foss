
import {
  mount
} from '@vue/test-utils'
import ProgressBar from '@/app/shared/components/ProgressBar.vue'
const testData = {
  progress: 50,
  isRadial: false
}
describe('ProgressBar unit test', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(ProgressBar, {
      propsData: {
        ...testData
      },
      mocks: {
        $t: (key) => key
      }
    })
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  test('Rendering props', () => {
    const wrapper = mount(ProgressBar, {
      propsData: {
        ...testData
      },
      mocks: {
        $t: (key) => key
      }
    })
    expect(wrapper.props().progress).toBe(50)
    expect(wrapper.props().isRadial).toBe(false)
  })

  test('Computed Property', () => {
    const wrapper = mount(ProgressBar, {
      propsData: {
        ...testData
      },
      mocks: {
        $t: (key) => key
      }
    })
    const vm = wrapper.vm
    expect(vm.getOffset).toBe(220)
  })
})

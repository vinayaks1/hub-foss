import { mount, shallowMount, createLocalVue } from '@vue/test-utils'
import Form from '@/app/shared/components/Form.vue'
import Vuex from 'vuex'
import FormStore from '@/app/shared/store/modules/form'
import HierarchySelectionFieldStore from '@/app/shared/store/modules/HierarchySelectionField'
import flushPromises from 'flush-promises'
import Vue from 'vue'
import testForm from './form-filter-dependency-data-test'
import Select from '@/app/shared/components/Select'
const localVue = createLocalVue()

localVue.use(Vuex)
window.EventBus = new Vue()

const filteredFilters = [{
  Label: 'Select STATE',
  Level: 0,
  Order: 1,
  Placeholder: null,
  RemoteUrl: '/api/Hierarchy/GetHierarchiesByTypeParent',
  Types: ['STATE'],
  OptionDisplayKeys: ['Name', 'Id', 'Code'],
  Options: [
    {
      Has99DOTS: true,
      Has99DOTSLite: true,
      HasChildren: false,
      HasMERM: true,
      HasNONE: true,
      HasVOT: true,
      Id: 1,
      Level: 0,
      Name: 'DemoSt1',
      Code: '23124',
      Order: 2,
      ParentId: 0,
      Type: 'STATE',
      AdherenceTechOptions: ['99DOTS', '99DOTSLite', 'MERM', 'VOT', 'NONE']
    }
  ]
},
{
  Label: 'Select DISTRICT',
  Level: 1,
  Order: 2,
  Placeholder: null,
  RemoteUrl: '/api/Hierarchy/GetHierarchiesByTypeParent',
  Types: ['DISTRICT'],
  OptionDisplayKeys: ['Name', 'Id', 'Code'],
  Options: [
    {
      Has99DOTS: true,
      Has99DOTSLite: true,
      HasChildren: false,
      HasMERM: true,
      HasNONE: true,
      HasVOT: true,
      Id: 2,
      Level: 1,
      Name: 'DemoDist1',
      Code: '23124',
      Order: 3,
      ParentId: 1,
      Type: 'DISTRICT',
      AdherenceTechOptions: ['99DOTS', '99DOTSLite', 'MERM', 'VOT', 'NONE']
    }
  ]
},
{
  Label: 'Select TU',
  Level: 2,
  Order: 3,
  Placeholder: null,
  RemoteUrl: '/api/Hierarchy/GetHierarchiesByTypeParent',
  Types: ['TU'],
  OptionDisplayKeys: ['Name', 'Id', 'Code'],
  Options: [
    {
      Has99DOTS: true,
      Has99DOTSLite: true,
      HasChildren: false,
      HasMERM: true,
      HasNONE: true,
      HasVOT: true,
      Id: 3,
      Level: 2,
      Name: 'DemoTU1',
      Code: '23124',
      Order: 4,
      ParentId: 2,
      Type: 'TU',
      AdherenceTechOptions: ['MERM']
    },
    {
      Has99DOTS: true,
      Has99DOTSLite: true,
      HasChildren: false,
      HasMERM: true,
      HasNONE: true,
      HasVOT: true,
      Id: 4,
      Level: 2,
      Name: 'DemoTU1',
      Code: '5',
      Order: 4,
      ParentId: 2,
      Type: 'TU',
      AdherenceTechOptions: ['99DOTS']
    },
    {
      Has99DOTS: true,
      Has99DOTSLite: true,
      HasChildren: false,
      HasMERM: true,
      HasNONE: true,
      HasVOT: true,
      Id: 4,
      Level: 2,
      Name: 'DemoTU1',
      Code: '5',
      Order: 4,
      ParentId: 2,
      Type: 'TU',
      AdherenceTechOptions: ['99DOTS','MERM']
    }
  ]
}]

jest.mock('../../src/utils/toastUtils', () => {
  return {
    __esModule: true,
    defaultToast: jest.fn(),
    ToastType: {
      Success: 'Success',
      Error: 'Error',
      Warning: 'Warning',
      Neutal: 'Neutal'
    }
  }
})

const getStore = (actions) => {
  const formattedActions = {
    ...FormStore.actions,
    ...actions
  }
  return new Vuex.Store({
    modules: {
      Form: {
        state: FormStore.state,
        actions: formattedActions,
        mutations: FormStore.mutations,
        namespaced: true
      },
      HierarchySelectionField: {
        actions: HierarchySelectionFieldStore.actions,
        state: HierarchySelectionFieldStore.state,
        mutations: HierarchySelectionFieldStore.mutations,
        namespaced: true
      }
    }
  })
}
describe('Form.vue', () => {
  it('checks if dependent select field options changes as per given options in TU selected field in hierarchy selector', async () => {
    const wrapper = mount(Form, { 
      store: getStore({
        getFormData: async () => (
          Promise.resolve(
            testForm
          )
        )
      }),
      localVue,
      propsData: { isEditing: true, name: 'Registration' },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    expect(wrapper.findComponent(Select).findAll('.dropdown ul li').length).toBe(5)
    wrapper.setData({input:{Residence:{HierarchyMapping_Residence: filteredFilters[2].Options[1] } }})
    wrapper.vm.applyFilterDependencies()
    await wrapper.vm.$nextTick();
    expect(wrapper.findComponent(Select).findAll('.dropdown ul li').length).toBe(1)
    expect(wrapper.findComponent(Select).findAll('.dropdown ul li').at(0).text()).toBe('Envelopes - 99DOTS')
    wrapper.setData({input:{Residence:{HierarchyMapping_Residence: filteredFilters[2].Options[0] } }})
    wrapper.vm.applyFilterDependencies()
    await wrapper.vm.$nextTick();
    expect(wrapper.findComponent(Select).findAll('.dropdown ul li').length).toBe(1)
    expect(wrapper.findComponent(Select).findAll('.dropdown ul li').at(0).text()).toBe('evriMED(MERM)')
    wrapper.setData({input:{Residence:{HierarchyMapping_Residence: filteredFilters[2].Options[2] } }})
    wrapper.vm.applyFilterDependencies()
    await wrapper.vm.$nextTick();
    expect(wrapper.findComponent(Select).findAll('.dropdown ul li').length).toBe(2)
    expect(['Envelopes - 99DOTS', 'evriMED(MERM)'] ).toContain(wrapper.findComponent(Select).findAll('.dropdown ul li').at(0).text())
    expect(['Envelopes - 99DOTS', 'evriMED(MERM)']).toContain(wrapper.findComponent(Select).findAll('.dropdown ul li').at(1).text())
  })
})



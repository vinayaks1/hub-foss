const testForm = {
    "Success": true,
    "Data": {
        "Form": {
            "Name": "Registration",
            "Title": "_add_patient",
            "Parts": [
                {
                    "FormName": "Registration",
                    "Name": "BasicDetails",
                    "Title": "",
                    "TitleKey": "_basic_details",
                    "Order": 1,
                    "IsVisible": true,
                    "Id": 57,
                    "Type": "vertical-form-part",
                    "Rows": null,
                    "Columns": null,
                    "RowDataName": "BasicDetails",
                    "AllowRowOpen": false,
                    "AllowRowDelete": false,
                    "ListItemTitle": null,
                    "ItemDescriptionField": null,
                    "IsRepeatable": false,
                    "RecordId": null
                },
                {
                    "FormName": "Registration",
                    "Name": "ContactDetails",
                    "Title": "",
                    "TitleKey": "contact_details",
                    "Order": 2,
                    "IsVisible": true,
                    "Id": 58,
                    "Type": "vertical-form-part",
                    "Rows": null,
                    "Columns": null,
                    "RowDataName": "ContactDetails",
                    "AllowRowOpen": false,
                    "AllowRowDelete": false,
                    "ListItemTitle": null,
                    "ItemDescriptionField": null,
                    "IsRepeatable": false,
                    "RecordId": null
                },
                {
                    "FormName": "Registration",
                    "Name": "Residence",
                    "Title": "",
                    "TitleKey": "treatment_center",
                    "Order": 3,
                    "IsVisible": true,
                    "Id": 59,
                    "Type": "vertical-form-part",
                    "Rows": null,
                    "Columns": null,
                    "RowDataName": "Residence",
                    "AllowRowOpen": false,
                    "AllowRowDelete": false,
                    "ListItemTitle": null,
                    "ItemDescriptionField": null,
                    "IsRepeatable": false,
                    "RecordId": null
                },
                {
                    "FormName": "Registration",
                    "Name": "TreatmentDetails",
                    "Title": "",
                    "TitleKey": "Treatment Details",
                    "Order": 4,
                    "IsVisible": true,
                    "Id": 60,
                    "Type": "vertical-form-part",
                    "Rows": null,
                    "Columns": null,
                    "RowDataName": "TreatmentDetails",
                    "AllowRowOpen": false,
                    "AllowRowDelete": false,
                    "ListItemTitle": null,
                    "ItemDescriptionField": null,
                    "IsRepeatable": false,
                    "RecordId": null
                }
            ],
            "PartOptions": null,
            "Fields": [
                {
                    "Value": null,
                    "PartName": "BasicDetails",
                    "Placeholder": null,
                    "Component": "app-input-field",
                    "ComponentVariants": null,
                    "IsDisabled": false,
                    "IsVisible": true,
                    "LabelKey": "_first_name",
                    "Label": "First Name",
                    "Name": "FirstName",
                    "IsHierarchySelector": false,
                    "IsRequired": true,
                    "RemoteUrl": null,
                    "Type": null,
                    "AddOn": null,
                    "Options": null,
                    "HierarchyOptions": null,
                    "OptionsWithKeyValue": null,
                    "AdditionalInfo": null,
                    "DefaultVisibilty": false,
                    "Order": 0,
                    "OptionsWithLabel": null,
                    "Validations": {
                        "Or": null,
                        "And": [
                            {
                                "Type": "Required",
                                "Max": 0,
                                "Min": 0,
                                "IsBackendValidation": false,
                                "ValidationUrl": null,
                                "ShowServerError": false,
                                "ErrorTargetField": null,
                                "ValidationParams": null,
                                "ErrorMessage": "This field is required",
                                "ErrorMessageKey": "error_field_required",
                                "RequiredOnlyWhen": null,
                                "Regex": null
                            },
                            {
                                "Type": "AlphaNumeric",
                                "Max": 0,
                                "Min": 0,
                                "IsBackendValidation": false,
                                "ValidationUrl": null,
                                "ShowServerError": false,
                                "ErrorTargetField": null,
                                "ValidationParams": null,
                                "ErrorMessage": "Only alphabet, numbers and spaces are allowed",
                                "ErrorMessageKey": "AlphaNumeric",
                                "RequiredOnlyWhen": null,
                                "Regex": "[\\p{L}\\p{N}\\s*]+"
                            }
                        ]
                    },
                    "ResponseDataPath": null,
                    "OptionDisplayKeys": null,
                    "OptionValueKey": null,
                    "LoadImmediately": false,
                    "HierarchySelectionConfigs": null,
                    "DisabledDateConfig": null,
                    "RemoteUpdateConfig": null,
                    "RowNumber": 1,
                    "ColumnNumber": null,
                    "Id": 11114,
                    "ParentType": "PART",
                    "ParentId": 57,
                    "FieldOptions": null,
                    "ValidationList": "AlphaNumeric",
                    "DefaultValue": null,
                    "Key": "BasicDetailsFirstName",
                    "HasInfo": false,
                    "InfoText": null,
                    "Config": null,
                    "ColumnWidth": 6,
                    "HasToggleButton": false
                },
                {
                    "Value": null,
                    "PartName": "BasicDetails",
                    "Placeholder": null,
                    "Component": "app-input-field",
                    "ComponentVariants": null,
                    "IsDisabled": false,
                    "IsVisible": true,
                    "LabelKey": "_surname",
                    "Label": "Surname",
                    "Name": "LastName",
                    "IsHierarchySelector": false,
                    "IsRequired": false,
                    "RemoteUrl": null,
                    "Type": null,
                    "AddOn": null,
                    "Options": null,
                    "HierarchyOptions": null,
                    "OptionsWithKeyValue": null,
                    "AdditionalInfo": null,
                    "DefaultVisibilty": false,
                    "Order": 0,
                    "OptionsWithLabel": null,
                    "Validations": {
                        "Or": null,
                        "And": [
                            {
                                "Type": "AlphaNumeric",
                                "Max": 0,
                                "Min": 0,
                                "IsBackendValidation": false,
                                "ValidationUrl": null,
                                "ShowServerError": false,
                                "ErrorTargetField": null,
                                "ValidationParams": null,
                                "ErrorMessage": "Only alphabet, numbers and spaces are allowed",
                                "ErrorMessageKey": "AlphaNumeric",
                                "RequiredOnlyWhen": null,
                                "Regex": "[\\p{L}\\p{N}\\s*]+"
                            }
                        ]
                    },
                    "ResponseDataPath": null,
                    "OptionDisplayKeys": null,
                    "OptionValueKey": null,
                    "LoadImmediately": false,
                    "HierarchySelectionConfigs": null,
                    "DisabledDateConfig": null,
                    "RemoteUpdateConfig": null,
                    "RowNumber": 2,
                    "ColumnNumber": null,
                    "Id": 11115,
                    "ParentType": "PART",
                    "ParentId": 57,
                    "FieldOptions": null,
                    "ValidationList": "AlphaNumeric",
                    "DefaultValue": null,
                    "Key": "BasicDetailsLastName",
                    "HasInfo": false,
                    "InfoText": null,
                    "Config": null,
                    "ColumnWidth": 6,
                    "HasToggleButton": false
                },
                {
                    "Value": null,
                    "PartName": "BasicDetails",
                    "Placeholder": null,
                    "Component": "app-input-field",
                    "ComponentVariants": null,
                    "IsDisabled": false,
                    "IsVisible": true,
                    "LabelKey": "_address",
                    "Label": "Address",
                    "Name": "Address",
                    "IsHierarchySelector": false,
                    "IsRequired": false,
                    "RemoteUrl": null,
                    "Type": null,
                    "AddOn": null,
                    "Options": null,
                    "HierarchyOptions": null,
                    "OptionsWithKeyValue": null,
                    "AdditionalInfo": null,
                    "DefaultVisibilty": false,
                    "Order": 0,
                    "OptionsWithLabel": null,
                    "Validations": {
                        "Or": null,
                        "And": []
                    },
                    "ResponseDataPath": null,
                    "OptionDisplayKeys": null,
                    "OptionValueKey": null,
                    "LoadImmediately": false,
                    "HierarchySelectionConfigs": null,
                    "DisabledDateConfig": null,
                    "RemoteUpdateConfig": null,
                    "RowNumber": 3,
                    "ColumnNumber": null,
                    "Id": 11116,
                    "ParentType": "PART",
                    "ParentId": 57,
                    "FieldOptions": null,
                    "ValidationList": null,
                    "DefaultValue": null,
                    "Key": "BasicDetailsAddress",
                    "HasInfo": false,
                    "InfoText": null,
                    "Config": null,
                    "ColumnWidth": 6,
                    "HasToggleButton": false
                },
                {
                    "Value": null,
                    "PartName": "BasicDetails",
                    "Placeholder": null,
                    "Component": "app-input-field",
                    "ComponentVariants": null,
                    "IsDisabled": false,
                    "IsVisible": true,
                    "LabelKey": "_age",
                    "Label": "Age",
                    "Name": "Age",
                    "IsHierarchySelector": false,
                    "IsRequired": false,
                    "RemoteUrl": null,
                    "Type": null,
                    "AddOn": null,
                    "Options": null,
                    "HierarchyOptions": null,
                    "OptionsWithKeyValue": null,
                    "AdditionalInfo": null,
                    "DefaultVisibilty": false,
                    "Order": 0,
                    "OptionsWithLabel": null,
                    "Validations": {
                        "Or": null,
                        "And": [
                            {
                                "Type": "OnlyNumbersAllowed",
                                "Max": 100,
                                "Min": 0,
                                "IsBackendValidation": false,
                                "ValidationUrl": null,
                                "ShowServerError": false,
                                "ErrorTargetField": null,
                                "ValidationParams": null,
                                "ErrorMessage": "Age must be a valid number and less than 99",
                                "ErrorMessageKey": "AgeError",
                                "RequiredOnlyWhen": null,
                                "Regex": "[0-9]{1,2}$"
                            }
                        ]
                    },
                    "ResponseDataPath": null,
                    "OptionDisplayKeys": null,
                    "OptionValueKey": null,
                    "LoadImmediately": false,
                    "HierarchySelectionConfigs": null,
                    "DisabledDateConfig": null,
                    "RemoteUpdateConfig": null,
                    "RowNumber": 4,
                    "ColumnNumber": null,
                    "Id": 11117,
                    "ParentType": "PART",
                    "ParentId": 57,
                    "FieldOptions": null,
                    "ValidationList": "Age",
                    "DefaultValue": null,
                    "Key": "BasicDetailsAge",
                    "HasInfo": false,
                    "InfoText": null,
                    "Config": null,
                    "ColumnWidth": 6,
                    "HasToggleButton": false
                },
                {
                    "Value": null,
                    "PartName": "BasicDetails",
                    "Placeholder": null,
                    "Component": "app-select",
                    "ComponentVariants": null,
                    "IsDisabled": false,
                    "IsVisible": true,
                    "LabelKey": "_gender",
                    "Label": "Gender",
                    "Name": "Gender",
                    "IsHierarchySelector": false,
                    "IsRequired": false,
                    "RemoteUrl": null,
                    "Type": null,
                    "AddOn": null,
                    "Options": null,
                    "HierarchyOptions": null,
                    "OptionsWithKeyValue": [
                        {
                            "Value": "Male",
                            "Key": "Male"
                        },
                        {
                            "Value": "Female",
                            "Key": "Female"
                        },
                        {
                            "Value": "Unknown",
                            "Key": "Unknown"
                        }
                    ],
                    "AdditionalInfo": null,
                    "DefaultVisibilty": false,
                    "Order": 0,
                    "OptionsWithLabel": null,
                    "Validations": {
                        "Or": null,
                        "And": []
                    },
                    "ResponseDataPath": null,
                    "OptionDisplayKeys": null,
                    "OptionValueKey": null,
                    "LoadImmediately": false,
                    "HierarchySelectionConfigs": null,
                    "DisabledDateConfig": null,
                    "RemoteUpdateConfig": null,
                    "RowNumber": 5,
                    "ColumnNumber": null,
                    "Id": 11118,
                    "ParentType": "PART",
                    "ParentId": 57,
                    "FieldOptions": "[{\"Value\" : \"_male\", \"Key\" : \"Male\"}, {\"Value\":\"_female\", \"Key\":\"Female\"}, {\"Value\":\"_unknown\", \"Key\":\"Unknown\"}]",
                    "ValidationList": null,
                    "DefaultValue": null,
                    "Key": "BasicDetailsGender",
                    "HasInfo": false,
                    "InfoText": null,
                    "Config": null,
                    "ColumnWidth": 6,
                    "HasToggleButton": false
                },
                {
                    "Value": null,
                    "PartName": "ContactDetails",
                    "Placeholder": null,
                    "Component": "app-input-field-group",
                    "ComponentVariants": null,
                    "IsDisabled": false,
                    "IsVisible": true,
                    "LabelKey": "primary_phone",
                    "Label": "Primary Phone",
                    "Name": "PrimaryPhone",
                    "IsHierarchySelector": false,
                    "IsRequired": true,
                    "RemoteUrl": null,
                    "Type": null,
                    "AddOn": "+91",
                    "Options": null,
                    "HierarchyOptions": null,
                    "OptionsWithKeyValue": null,
                    "AdditionalInfo": null,
                    "DefaultVisibilty": false,
                    "Order": 0,
                    "OptionsWithLabel": null,
                    "Validations": {
                        "Or": null,
                        "And": [
                            {
                                "Type": "Required",
                                "Max": 0,
                                "Min": 0,
                                "IsBackendValidation": false,
                                "ValidationUrl": null,
                                "ShowServerError": false,
                                "ErrorTargetField": null,
                                "ValidationParams": null,
                                "ErrorMessage": "This field is required",
                                "ErrorMessageKey": "error_field_required",
                                "RequiredOnlyWhen": null,
                                "Regex": null
                            },
                            {
                                "Type": "PhoneNumberNotStartingWithZero",
                                "Max": 0,
                                "Min": 0,
                                "IsBackendValidation": false,
                                "ValidationUrl": null,
                                "ShowServerError": false,
                                "ErrorTargetField": null,
                                "ValidationParams": null,
                                "ErrorMessage": "Please enter a valid phone number",
                                "ErrorMessageKey": "error_phone_number",
                                "RequiredOnlyWhen": null,
                                "Regex": "[1-9][0-9]{9}"
                            },
                            {
                                "Type": "PhoneNumberMaxLengthEleven",
                                "Max": 0,
                                "Min": 0,
                                "IsBackendValidation": false,
                                "ValidationUrl": null,
                                "ShowServerError": false,
                                "ErrorTargetField": null,
                                "ValidationParams": null,
                                "ErrorMessage": "Length of Phone number cannot exceed 11",
                                "ErrorMessageKey": "PhoneNumberMaxLengthEleven",
                                "RequiredOnlyWhen": null,
                                "Regex": "^[1-9][0-9]{0,10}$"
                            }
                        ]
                    },
                    "ResponseDataPath": null,
                    "OptionDisplayKeys": null,
                    "OptionValueKey": null,
                    "LoadImmediately": false,
                    "HierarchySelectionConfigs": null,
                    "DisabledDateConfig": null,
                    "RemoteUpdateConfig": null,
                    "RowNumber": 1,
                    "ColumnNumber": null,
                    "Id": 11120,
                    "ParentType": "PART",
                    "ParentId": 58,
                    "FieldOptions": null,
                    "ValidationList": "PhoneNumberNotStartingWithZero,PhoneNumberMaxLengthEleven",
                    "DefaultValue": null,
                    "Key": "ContactDetailsPrimaryPhone",
                    "HasInfo": false,
                    "InfoText": null,
                    "Config": null,
                    "ColumnWidth": 6,
                    "HasToggleButton": false
                },
                {
                    "Value": null,
                    "PartName": "ContactDetails",
                    "Placeholder": null,
                    "Component": "app-select",
                    "ComponentVariants": null,
                    "IsDisabled": false,
                    "IsVisible": true,
                    "LabelKey": "_owner",
                    "Label": "Owner",
                    "Name": "PrimaryPhoneOwner",
                    "IsHierarchySelector": false,
                    "IsRequired": false,
                    "RemoteUrl": null,
                    "Type": null,
                    "AddOn": null,
                    "Options": null,
                    "HierarchyOptions": null,
                    "OptionsWithKeyValue": [
                        {
                            "Value": "Patient",
                            "Key": "PATIENT"
                        },
                        {
                            "Value": "Family",
                            "Key": "FAMILY"
                        },
                        {
                            "Value": "Friend",
                            "Key": "FRIEND"
                        },
                        {
                            "Value": "Treatment Supporter",
                            "Key": "TREATMENT_SUPPORTER"
                        },
                        {
                            "Value": "Other",
                            "Key": "OTHER"
                        }
                    ],
                    "AdditionalInfo": null,
                    "DefaultVisibilty": false,
                    "Order": 0,
                    "OptionsWithLabel": null,
                    "Validations": {
                        "Or": null,
                        "And": [
                            {
                                "Type": "OnlyAlphabetAndSpaceAllowed",
                                "Max": 0,
                                "Min": 0,
                                "IsBackendValidation": false,
                                "ValidationUrl": null,
                                "ShowServerError": false,
                                "ErrorTargetField": null,
                                "ValidationParams": null,
                                "ErrorMessage": "Only alphabet and spaces are allowed",
                                "ErrorMessageKey": "OnlyAlphabetAndSpaceAllowed",
                                "RequiredOnlyWhen": null,
                                "Regex": "[\\p{L}\\s*]+"
                            }
                        ]
                    },
                    "ResponseDataPath": null,
                    "OptionDisplayKeys": null,
                    "OptionValueKey": null,
                    "LoadImmediately": false,
                    "HierarchySelectionConfigs": null,
                    "DisabledDateConfig": null,
                    "RemoteUpdateConfig": null,
                    "RowNumber": 2,
                    "ColumnNumber": null,
                    "Id": 11121,
                    "ParentType": "PART",
                    "ParentId": 58,
                    "FieldOptions": "[{\"Value\" : \"_patient\", \"Key\" : \"PATIENT\"}, {\"Value\":\"_family\", \"Key\":\"FAMILY\"}, {\"Value\": \"Friend\", \"Key\": \"FRIEND\"}, {\"Value\": \"TreatmentSupporter\", \"Key\": \"TREATMENT_SUPPORTER\"}, {\"Value\": \"_other\", \"Key\": \"OTHER\"}]",
                    "ValidationList": "OnlyAlphabetAndSpaceAllowed",
                    "DefaultValue": null,
                    "Key": "ContactDetailsPrimaryPhoneOwner",
                    "HasInfo": false,
                    "InfoText": null,
                    "Config": null,
                    "ColumnWidth": 6,
                    "HasToggleButton": false
                },
                {
                    "Value": null,
                    "PartName": "ContactDetails",
                    "Placeholder": null,
                    "Component": "app-input-field-group",
                    "ComponentVariants": null,
                    "IsDisabled": false,
                    "IsVisible": true,
                    "LabelKey": "_phone_1",
                    "Label": "Phone 2",
                    "Name": "SecondaryPhone1",
                    "IsHierarchySelector": false,
                    "IsRequired": false,
                    "RemoteUrl": null,
                    "Type": null,
                    "AddOn": "+91",
                    "Options": null,
                    "HierarchyOptions": null,
                    "OptionsWithKeyValue": null,
                    "AdditionalInfo": null,
                    "DefaultVisibilty": false,
                    "Order": 0,
                    "OptionsWithLabel": null,
                    "Validations": {
                        "Or": null,
                        "And": [
                            {
                                "Type": "PhoneNumberNotStartingWithZero",
                                "Max": 0,
                                "Min": 0,
                                "IsBackendValidation": false,
                                "ValidationUrl": null,
                                "ShowServerError": false,
                                "ErrorTargetField": null,
                                "ValidationParams": null,
                                "ErrorMessage": "Please enter a valid phone number",
                                "ErrorMessageKey": "error_phone_number",
                                "RequiredOnlyWhen": null,
                                "Regex": "[1-9][0-9]{9}"
                            },
                            {
                                "Type": "PhoneNumberMaxLengthEleven",
                                "Max": 0,
                                "Min": 0,
                                "IsBackendValidation": false,
                                "ValidationUrl": null,
                                "ShowServerError": false,
                                "ErrorTargetField": null,
                                "ValidationParams": null,
                                "ErrorMessage": "Length of Phone number cannot exceed 11",
                                "ErrorMessageKey": "PhoneNumberMaxLengthEleven",
                                "RequiredOnlyWhen": null,
                                "Regex": "^[1-9][0-9]{0,10}$"
                            }
                        ]
                    },
                    "ResponseDataPath": null,
                    "OptionDisplayKeys": null,
                    "OptionValueKey": null,
                    "LoadImmediately": false,
                    "HierarchySelectionConfigs": null,
                    "DisabledDateConfig": null,
                    "RemoteUpdateConfig": null,
                    "RowNumber": 3,
                    "ColumnNumber": null,
                    "Id": 11122,
                    "ParentType": "PART",
                    "ParentId": 58,
                    "FieldOptions": null,
                    "ValidationList": "PhoneNumberNotStartingWithZero,PhoneNumberMaxLengthEleven",
                    "DefaultValue": null,
                    "Key": "ContactDetailsSecondaryPhone1",
                    "HasInfo": false,
                    "InfoText": null,
                    "Config": null,
                    "ColumnWidth": 6,
                    "HasToggleButton": false
                },
                {
                    "Value": null,
                    "PartName": "ContactDetails",
                    "Placeholder": null,
                    "Component": "app-select",
                    "ComponentVariants": null,
                    "IsDisabled": false,
                    "IsVisible": true,
                    "LabelKey": "_owner",
                    "Label": "Owner",
                    "Name": "SecondaryPhone1Owner",
                    "IsHierarchySelector": false,
                    "IsRequired": false,
                    "RemoteUrl": null,
                    "Type": null,
                    "AddOn": null,
                    "Options": null,
                    "HierarchyOptions": null,
                    "OptionsWithKeyValue": [
                        {
                            "Value": "Patient",
                            "Key": "PATIENT"
                        },
                        {
                            "Value": "Family",
                            "Key": "FAMILY"
                        },
                        {
                            "Value": "Friend",
                            "Key": "FRIEND"
                        },
                        {
                            "Value": "Treatment Supporter",
                            "Key": "TREATMENT_SUPPORTER"
                        },
                        {
                            "Value": "Other",
                            "Key": "OTHER"
                        }
                    ],
                    "AdditionalInfo": null,
                    "DefaultVisibilty": false,
                    "Order": 0,
                    "OptionsWithLabel": null,
                    "Validations": {
                        "Or": null,
                        "And": [
                            {
                                "Type": "OnlyAlphabetAndSpaceAllowed",
                                "Max": 0,
                                "Min": 0,
                                "IsBackendValidation": false,
                                "ValidationUrl": null,
                                "ShowServerError": false,
                                "ErrorTargetField": null,
                                "ValidationParams": null,
                                "ErrorMessage": "Only alphabet and spaces are allowed",
                                "ErrorMessageKey": "OnlyAlphabetAndSpaceAllowed",
                                "RequiredOnlyWhen": null,
                                "Regex": "[\\p{L}\\s*]+"
                            }
                        ]
                    },
                    "ResponseDataPath": null,
                    "OptionDisplayKeys": null,
                    "OptionValueKey": null,
                    "LoadImmediately": false,
                    "HierarchySelectionConfigs": null,
                    "DisabledDateConfig": null,
                    "RemoteUpdateConfig": null,
                    "RowNumber": 4,
                    "ColumnNumber": null,
                    "Id": 11123,
                    "ParentType": "PART",
                    "ParentId": 58,
                    "FieldOptions": "[{\"Value\" : \"_patient\", \"Key\" : \"PATIENT\"}, {\"Value\":\"_family\", \"Key\":\"FAMILY\"}, {\"Value\": \"Friend\", \"Key\": \"FRIEND\"}, {\"Value\": \"TreatmentSupporter\", \"Key\": \"TREATMENT_SUPPORTER\"}, {\"Value\": \"_other\", \"Key\": \"OTHER\"}]",
                    "ValidationList": "OnlyAlphabetAndSpaceAllowed",
                    "DefaultValue": null,
                    "Key": "ContactDetailsSecondaryPhone1Owner",
                    "HasInfo": false,
                    "InfoText": null,
                    "Config": null,
                    "ColumnWidth": 6,
                    "HasToggleButton": false
                },
                {
                    "Value": null,
                    "PartName": "ContactDetails",
                    "Placeholder": null,
                    "Component": "app-input-field-group",
                    "ComponentVariants": null,
                    "IsDisabled": false,
                    "IsVisible": true,
                    "LabelKey": "_phone_3",
                    "Label": "Phone 3",
                    "Name": "SecondaryPhone2",
                    "IsHierarchySelector": false,
                    "IsRequired": false,
                    "RemoteUrl": null,
                    "Type": null,
                    "AddOn": "+91",
                    "Options": null,
                    "HierarchyOptions": null,
                    "OptionsWithKeyValue": null,
                    "AdditionalInfo": null,
                    "DefaultVisibilty": false,
                    "Order": 0,
                    "OptionsWithLabel": null,
                    "Validations": {
                        "Or": null,
                        "And": [
                            {
                                "Type": "PhoneNumberNotStartingWithZero",
                                "Max": 0,
                                "Min": 0,
                                "IsBackendValidation": false,
                                "ValidationUrl": null,
                                "ShowServerError": false,
                                "ErrorTargetField": null,
                                "ValidationParams": null,
                                "ErrorMessage": "Please enter a valid phone number",
                                "ErrorMessageKey": "error_phone_number",
                                "RequiredOnlyWhen": null,
                                "Regex": "[1-9][0-9]{9}"
                            },
                            {
                                "Type": "PhoneNumberMaxLengthEleven",
                                "Max": 0,
                                "Min": 0,
                                "IsBackendValidation": false,
                                "ValidationUrl": null,
                                "ShowServerError": false,
                                "ErrorTargetField": null,
                                "ValidationParams": null,
                                "ErrorMessage": "Length of Phone number cannot exceed 11",
                                "ErrorMessageKey": "PhoneNumberMaxLengthEleven",
                                "RequiredOnlyWhen": null,
                                "Regex": "^[1-9][0-9]{0,10}$"
                            }
                        ]
                    },
                    "ResponseDataPath": null,
                    "OptionDisplayKeys": null,
                    "OptionValueKey": null,
                    "LoadImmediately": false,
                    "HierarchySelectionConfigs": null,
                    "DisabledDateConfig": null,
                    "RemoteUpdateConfig": null,
                    "RowNumber": 5,
                    "ColumnNumber": null,
                    "Id": 11124,
                    "ParentType": "PART",
                    "ParentId": 58,
                    "FieldOptions": null,
                    "ValidationList": "PhoneNumberNotStartingWithZero,PhoneNumberMaxLengthEleven",
                    "DefaultValue": null,
                    "Key": "ContactDetailsSecondaryPhone2",
                    "HasInfo": false,
                    "InfoText": null,
                    "Config": null,
                    "ColumnWidth": 6,
                    "HasToggleButton": false
                },
                {
                    "Value": null,
                    "PartName": "ContactDetails",
                    "Placeholder": null,
                    "Component": "app-select",
                    "ComponentVariants": null,
                    "IsDisabled": false,
                    "IsVisible": true,
                    "LabelKey": "_owner",
                    "Label": "Owner",
                    "Name": "SecondaryPhone2Owner",
                    "IsHierarchySelector": false,
                    "IsRequired": false,
                    "RemoteUrl": null,
                    "Type": null,
                    "AddOn": null,
                    "Options": null,
                    "HierarchyOptions": null,
                    "OptionsWithKeyValue": [
                        {
                            "Value": "Patient",
                            "Key": "PATIENT"
                        },
                        {
                            "Value": "Family",
                            "Key": "FAMILY"
                        },
                        {
                            "Value": "Friend",
                            "Key": "FRIEND"
                        },
                        {
                            "Value": "Treatment Supporter",
                            "Key": "TREATMENT_SUPPORTER"
                        },
                        {
                            "Value": "Other",
                            "Key": "OTHER"
                        }
                    ],
                    "AdditionalInfo": null,
                    "DefaultVisibilty": false,
                    "Order": 0,
                    "OptionsWithLabel": null,
                    "Validations": {
                        "Or": null,
                        "And": [
                            {
                                "Type": "OnlyAlphabetAndSpaceAllowed",
                                "Max": 0,
                                "Min": 0,
                                "IsBackendValidation": false,
                                "ValidationUrl": null,
                                "ShowServerError": false,
                                "ErrorTargetField": null,
                                "ValidationParams": null,
                                "ErrorMessage": "Only alphabet and spaces are allowed",
                                "ErrorMessageKey": "OnlyAlphabetAndSpaceAllowed",
                                "RequiredOnlyWhen": null,
                                "Regex": "[\\p{L}\\s*]+"
                            }
                        ]
                    },
                    "ResponseDataPath": null,
                    "OptionDisplayKeys": null,
                    "OptionValueKey": null,
                    "LoadImmediately": false,
                    "HierarchySelectionConfigs": null,
                    "DisabledDateConfig": null,
                    "RemoteUpdateConfig": null,
                    "RowNumber": 6,
                    "ColumnNumber": null,
                    "Id": 11125,
                    "ParentType": "PART",
                    "ParentId": 58,
                    "FieldOptions": "[{\"Value\" : \"_patient\", \"Key\" : \"PATIENT\"}, {\"Value\":\"_family\", \"Key\":\"FAMILY\"}, {\"Value\": \"Friend\", \"Key\": \"FRIEND\"}, {\"Value\": \"TreatmentSupporter\", \"Key\": \"TREATMENT_SUPPORTER\"}, {\"Value\": \"_other\", \"Key\": \"OTHER\"}]",
                    "ValidationList": "OnlyAlphabetAndSpaceAllowed",
                    "DefaultValue": null,
                    "Key": "ContactDetailsSecondaryPhone2Owner",
                    "HasInfo": false,
                    "InfoText": null,
                    "Config": null,
                    "ColumnWidth": 6,
                    "HasToggleButton": false
                },
                {
                    "Value": null,
                    "PartName": "ContactDetails",
                    "Placeholder": null,
                    "Component": "app-input-field-group",
                    "ComponentVariants": null,
                    "IsDisabled": false,
                    "IsVisible": true,
                    "LabelKey": "_phone_4",
                    "Label": "Phone 4",
                    "Name": "SecondaryPhone3",
                    "IsHierarchySelector": false,
                    "IsRequired": false,
                    "RemoteUrl": null,
                    "Type": null,
                    "AddOn": "+91",
                    "Options": null,
                    "HierarchyOptions": null,
                    "OptionsWithKeyValue": null,
                    "AdditionalInfo": null,
                    "DefaultVisibilty": false,
                    "Order": 0,
                    "OptionsWithLabel": null,
                    "Validations": {
                        "Or": null,
                        "And": [
                            {
                                "Type": "PhoneNumberNotStartingWithZero",
                                "Max": 0,
                                "Min": 0,
                                "IsBackendValidation": false,
                                "ValidationUrl": null,
                                "ShowServerError": false,
                                "ErrorTargetField": null,
                                "ValidationParams": null,
                                "ErrorMessage": "Please enter a valid phone number",
                                "ErrorMessageKey": "error_phone_number",
                                "RequiredOnlyWhen": null,
                                "Regex": "[1-9][0-9]{9}"
                            },
                            {
                                "Type": "PhoneNumberMaxLengthEleven",
                                "Max": 0,
                                "Min": 0,
                                "IsBackendValidation": false,
                                "ValidationUrl": null,
                                "ShowServerError": false,
                                "ErrorTargetField": null,
                                "ValidationParams": null,
                                "ErrorMessage": "Length of Phone number cannot exceed 11",
                                "ErrorMessageKey": "PhoneNumberMaxLengthEleven",
                                "RequiredOnlyWhen": null,
                                "Regex": "^[1-9][0-9]{0,10}$"
                            }
                        ]
                    },
                    "ResponseDataPath": null,
                    "OptionDisplayKeys": null,
                    "OptionValueKey": null,
                    "LoadImmediately": false,
                    "HierarchySelectionConfigs": null,
                    "DisabledDateConfig": null,
                    "RemoteUpdateConfig": null,
                    "RowNumber": 7,
                    "ColumnNumber": null,
                    "Id": 11126,
                    "ParentType": "PART",
                    "ParentId": 58,
                    "FieldOptions": null,
                    "ValidationList": "PhoneNumberNotStartingWithZero,PhoneNumberMaxLengthEleven",
                    "DefaultValue": null,
                    "Key": "ContactDetailsSecondaryPhone3",
                    "HasInfo": false,
                    "InfoText": null,
                    "Config": null,
                    "ColumnWidth": 6,
                    "HasToggleButton": false
                },
                {
                    "Value": null,
                    "PartName": "ContactDetails",
                    "Placeholder": null,
                    "Component": "app-select",
                    "ComponentVariants": null,
                    "IsDisabled": false,
                    "IsVisible": true,
                    "LabelKey": "_owner",
                    "Label": "Owner",
                    "Name": "SecondaryPhone3Owner",
                    "IsHierarchySelector": false,
                    "IsRequired": false,
                    "RemoteUrl": null,
                    "Type": null,
                    "AddOn": null,
                    "Options": null,
                    "HierarchyOptions": null,
                    "OptionsWithKeyValue": [
                        {
                            "Value": "Patient",
                            "Key": "PATIENT"
                        },
                        {
                            "Value": "Family",
                            "Key": "FAMILY"
                        },
                        {
                            "Value": "Friend",
                            "Key": "FRIEND"
                        },
                        {
                            "Value": "Treatment Supporter",
                            "Key": "TREATMENT_SUPPORTER"
                        },
                        {
                            "Value": "Other",
                            "Key": "OTHER"
                        }
                    ],
                    "AdditionalInfo": null,
                    "DefaultVisibilty": false,
                    "Order": 0,
                    "OptionsWithLabel": null,
                    "Validations": {
                        "Or": null,
                        "And": [
                            {
                                "Type": "OnlyAlphabetAndSpaceAllowed",
                                "Max": 0,
                                "Min": 0,
                                "IsBackendValidation": false,
                                "ValidationUrl": null,
                                "ShowServerError": false,
                                "ErrorTargetField": null,
                                "ValidationParams": null,
                                "ErrorMessage": "Only alphabet and spaces are allowed",
                                "ErrorMessageKey": "OnlyAlphabetAndSpaceAllowed",
                                "RequiredOnlyWhen": null,
                                "Regex": "[\\p{L}\\s*]+"
                            }
                        ]
                    },
                    "ResponseDataPath": null,
                    "OptionDisplayKeys": null,
                    "OptionValueKey": null,
                    "LoadImmediately": false,
                    "HierarchySelectionConfigs": null,
                    "DisabledDateConfig": null,
                    "RemoteUpdateConfig": null,
                    "RowNumber": 8,
                    "ColumnNumber": null,
                    "Id": 11127,
                    "ParentType": "PART",
                    "ParentId": 58,
                    "FieldOptions": "[{\"Value\" : \"_patient\", \"Key\" : \"PATIENT\"}, {\"Value\":\"_family\", \"Key\":\"FAMILY\"}, {\"Value\": \"Friend\", \"Key\": \"FRIEND\"}, {\"Value\": \"TreatmentSupporter\", \"Key\": \"TREATMENT_SUPPORTER\"}, {\"Value\": \"_other\", \"Key\": \"OTHER\"}]",
                    "ValidationList": "OnlyAlphabetAndSpaceAllowed",
                    "DefaultValue": null,
                    "Key": "ContactDetailsSecondaryPhone3Owner",
                    "HasInfo": false,
                    "InfoText": null,
                    "Config": null,
                    "ColumnWidth": 6,
                    "HasToggleButton": false
                },
                {
                    "Value": null,
                    "PartName": "Residence",
                    "Placeholder": null,
                    "Component": "app-hierarchy-selection-field",
                    "ComponentVariants": null,
                    "IsDisabled": false,
                    "IsVisible": true,
                    "LabelKey": "HierarchyMapping_Residence",
                    "Label": "Hierarchy Mapping Residence",
                    "Name": "HierarchyMapping_Residence",
                    "IsHierarchySelector": false,
                    "IsRequired": true,
                    "RemoteUrl": null,
                    "Type": null,
                    "AddOn": null,
                    "Options": null,
                    "HierarchyOptions": null,
                    "OptionsWithKeyValue": null,
                    "AdditionalInfo": null,
                    "DefaultVisibilty": false,
                    "Order": 0,
                    "OptionsWithLabel": null,
                    "Validations": {
                        "Or": null,
                        "And": [
                            {
                                "Type": "Required",
                                "Max": 0,
                                "Min": 0,
                                "IsBackendValidation": false,
                                "ValidationUrl": null,
                                "ShowServerError": false,
                                "ErrorTargetField": null,
                                "ValidationParams": null,
                                "ErrorMessage": "This field is required",
                                "ErrorMessageKey": "error_field_required",
                                "RequiredOnlyWhen": null,
                                "Regex": null
                            }
                        ]
                    },
                    "ResponseDataPath": null,
                    "OptionDisplayKeys": null,
                    "OptionValueKey": null,
                    "LoadImmediately": false,
                    "HierarchySelectionConfigs": [
                        {
                            "Types": [
                                "STATE"
                            ],
                            "Type": null,
                            "Order": 1,
                            "Level": 2,
                            "OptionDisplayKeys": [
                                "Name",
                                "Id",
                                "Code"
                            ],
                            "Placeholder": null,
                            "Label": "Select STATE",
                            "Options": [],
                            "RemoteUrl": "/api/Hierarchy/GetHierarchiesByTypeParent"
                        },
                        {
                            "Types": [
                                "DISTRICT"
                            ],
                            "Type": null,
                            "Order": 2,
                            "Level": 3,
                            "OptionDisplayKeys": [
                                "Name",
                                "Id",
                                "Code"
                            ],
                            "Placeholder": null,
                            "Label": "Select DISTRICT",
                            "Options": [],
                            "RemoteUrl": "/api/Hierarchy/GetHierarchiesByTypeParent"
                        },
                        {
                            "Types": [
                                "TU"
                            ],
                            "Type": null,
                            "Order": 3,
                            "Level": 4,
                            "OptionDisplayKeys": [
                                "Name",
                                "Id",
                                "Code"
                            ],
                            "Placeholder": null,
                            "Label": "Select TU",
                            "Options": [],
                            "RemoteUrl": "/api/Hierarchy/GetHierarchiesByTypeParent"
                        }
                    ],
                    "DisabledDateConfig": null,
                    "RemoteUpdateConfig": null,
                    "RowNumber": 1,
                    "ColumnNumber": null,
                    "Id": 3034,
                    "ParentType": "PART",
                    "ParentId": 59,
                    "FieldOptions": null,
                    "ValidationList": null,
                    "DefaultValue": null,
                    "Key": "ResidenceHierarchyMapping_Residence",
                    "HasInfo": false,
                    "InfoText": null,
                    "Config": null,
                    "ColumnWidth": 6,
                    "HasToggleButton": false
                },
                {
                    "Value": null,
                    "PartName": "TreatmentDetails",
                    "Placeholder": null,
                    "Component": "app-select",
                    "ComponentVariants": null,
                    "IsDisabled": false,
                    "IsVisible": true,
                    "LabelKey": "_weight_band",
                    "Label": "Weight Band",
                    "Name": "WeightBand",
                    "IsHierarchySelector": false,
                    "IsRequired": true,
                    "RemoteUrl": null,
                    "Type": null,
                    "AddOn": null,
                    "Options": null,
                    "HierarchyOptions": null,
                    "OptionsWithKeyValue": [
                        {
                            "Value": "2 pills per day",
                            "Key": "2"
                        },
                        {
                            "Value": "3 pills per day",
                            "Key": "3"
                        },
                        {
                            "Value": "4 pills per day",
                            "Key": "4"
                        },
                        {
                            "Value": "5 pills per day",
                            "Key": "5"
                        }
                    ],
                    "AdditionalInfo": null,
                    "DefaultVisibilty": false,
                    "Order": 0,
                    "OptionsWithLabel": null,
                    "Validations": {
                        "Or": null,
                        "And": [
                            {
                                "Type": "Required",
                                "Max": 0,
                                "Min": 0,
                                "IsBackendValidation": false,
                                "ValidationUrl": null,
                                "ShowServerError": false,
                                "ErrorTargetField": null,
                                "ValidationParams": null,
                                "ErrorMessage": "This field is required",
                                "ErrorMessageKey": "error_field_required",
                                "RequiredOnlyWhen": null,
                                "Regex": null
                            }
                        ]
                    },
                    "ResponseDataPath": null,
                    "OptionDisplayKeys": null,
                    "OptionValueKey": null,
                    "LoadImmediately": false,
                    "HierarchySelectionConfigs": null,
                    "DisabledDateConfig": null,
                    "RemoteUpdateConfig": null,
                    "RowNumber": 4,
                    "ColumnNumber": null,
                    "Id": 11131,
                    "ParentType": "PART",
                    "ParentId": 60,
                    "FieldOptions": "[{\"Value\" : \"2 pills per day\", \"Key\" : \"2\"}, {\"Value\":\"3 pills per day\", \"Key\":\"3\"}, {\"Value\":\"4 pills per day\", \"Key\":\"4\"}, {\"Value\":\"5 pills per day\", \"Key\":\"5\"}]",
                    "ValidationList": null,
                    "DefaultValue": null,
                    "Key": "TreatmentDetailsWeightBand",
                    "HasInfo": false,
                    "InfoText": null,
                    "Config": null,
                    "ColumnWidth": 6,
                    "HasToggleButton": false
                },
                {
                    "Value": null,
                    "PartName": "TreatmentDetails",
                    "Placeholder": null,
                    "Component": "app-select",
                    "ComponentVariants": null,
                    "IsDisabled": false,
                    "IsVisible": true,
                    "LabelKey": "NumberOfPills",
                    "Label": "Number Of Pills",
                    "Name": "NumberOfPills",
                    "IsHierarchySelector": false,
                    "IsRequired": true,
                    "RemoteUrl": null,
                    "Type": null,
                    "AddOn": null,
                    "Options": null,
                    "HierarchyOptions": null,
                    "OptionsWithKeyValue": [
                        {
                            "Value": "1",
                            "Key": "1"
                        },
                        {
                            "Value": "2",
                            "Key": "2"
                        },
                        {
                            "Value": "3",
                            "Key": "3"
                        },
                        {
                            "Value": "4",
                            "Key": "4"
                        },
                        {
                            "Value": "5",
                            "Key": "5"
                        },
                        {
                            "Value": "6",
                            "Key": "6"
                        },
                        {
                            "Value": "7",
                            "Key": "7"
                        },
                        {
                            "Value": "8",
                            "Key": "8"
                        },
                        {
                            "Value": "9",
                            "Key": "9"
                        },
                        {
                            "Value": "10",
                            "Key": "10"
                        },
                        {
                            "Value": "11",
                            "Key": "11"
                        },
                        {
                            "Value": "12",
                            "Key": "12"
                        },
                        {
                            "Value": "13",
                            "Key": "13"
                        },
                        {
                            "Value": "14",
                            "Key": "14"
                        },
                        {
                            "Value": "15",
                            "Key": "15"
                        },
                        {
                            "Value": "16",
                            "Key": "16"
                        },
                        {
                            "Value": "17",
                            "Key": "17"
                        },
                        {
                            "Value": "18",
                            "Key": "18"
                        },
                        {
                            "Value": "19",
                            "Key": "19"
                        },
                        {
                            "Value": "20",
                            "Key": "20"
                        }
                    ],
                    "AdditionalInfo": null,
                    "DefaultVisibilty": false,
                    "Order": 0,
                    "OptionsWithLabel": null,
                    "Validations": {
                        "Or": null,
                        "And": [
                            {
                                "Type": "Required",
                                "Max": 0,
                                "Min": 0,
                                "IsBackendValidation": false,
                                "ValidationUrl": null,
                                "ShowServerError": false,
                                "ErrorTargetField": null,
                                "ValidationParams": null,
                                "ErrorMessage": "This field is required",
                                "ErrorMessageKey": "error_field_required",
                                "RequiredOnlyWhen": null,
                                "Regex": null
                            }
                        ]
                    },
                    "ResponseDataPath": null,
                    "OptionDisplayKeys": null,
                    "OptionValueKey": null,
                    "LoadImmediately": false,
                    "HierarchySelectionConfigs": null,
                    "DisabledDateConfig": null,
                    "RemoteUpdateConfig": null,
                    "RowNumber": 5,
                    "ColumnNumber": null,
                    "Id": 11132,
                    "ParentType": "PART",
                    "ParentId": 60,
                    "FieldOptions": "[{\"Value\" : \"1\", \"Key\" : \"1\"}, {\"Value\":\"2\", \"Key\":\"2\"}, {\"Value\" : \"3\", \"Key\" : \"3\"}, {\"Value\" : \"4\", \"Key\" : \"4\"}, {\"Value\" : \"5\", \"Key\" : \"5\"}, {\"Value\" : \"6\", \"Key\" : \"6\"}, {\"Value\" : \"7\", \"Key\" : \"7\"}, {\"Value\" : \"8\", \"Key\" : \"8\"}, {\"Value\" : \"9\", \"Key\" : \"9\"}, {\"Value\" : \"10\", \"Key\" : \"10\"}, {\"Value\" : \"11\", \"Key\" : \"11\"}, {\"Value\" : \"12\", \"Key\" : \"12\"}, {\"Value\" : \"13\", \"Key\" : \"13\"}, {\"Value\" : \"14\", \"Key\" : \"14\"}, {\"Value\" : \"15\", \"Key\" : \"15\"}, {\"Value\" : \"16\", \"Key\" : \"16\"}, {\"Value\" : \"17\", \"Key\" : \"17\"}, {\"Value\" : \"18\", \"Key\" : \"18\"}, {\"Value\" : \"19\", \"Key\" : \"19\"}, {\"Value\" : \"20\", \"Key\" : \"20\"}]",
                    "ValidationList": null,
                    "DefaultValue": null,
                    "Key": "TreatmentDetailsNumberOfPills",
                    "HasInfo": false,
                    "InfoText": null,
                    "Config": null,
                    "ColumnWidth": 6,
                    "HasToggleButton": false
                },
                {
                    "Value": null,
                    "PartName": "TreatmentDetails",
                    "Placeholder": null,
                    "Component": "app-select",
                    "ComponentVariants": null,
                    "IsDisabled": false,
                    "IsVisible": true,
                    "LabelKey": "_tb_category",
                    "Label": "TB Category",
                    "Name": "TBCategory",
                    "IsHierarchySelector": false,
                    "IsRequired": true,
                    "RemoteUrl": null,
                    "Type": null,
                    "AddOn": null,
                    "Options": null,
                    "HierarchyOptions": null,
                    "OptionsWithKeyValue": [
                        {
                            "Value": "DS-TB",
                            "Key": "DS-TB"
                        },
                        {
                            "Value": "DR-TB",
                            "Key": "DR-TB"
                        }
                    ],
                    "AdditionalInfo": null,
                    "DefaultVisibilty": false,
                    "Order": 0,
                    "OptionsWithLabel": null,
                    "Validations": {
                        "Or": null,
                        "And": [
                            {
                                "Type": "Required",
                                "Max": 0,
                                "Min": 0,
                                "IsBackendValidation": false,
                                "ValidationUrl": null,
                                "ShowServerError": false,
                                "ErrorTargetField": null,
                                "ValidationParams": null,
                                "ErrorMessage": "This field is required",
                                "ErrorMessageKey": "error_field_required",
                                "RequiredOnlyWhen": null,
                                "Regex": null
                            }
                        ]
                    },
                    "ResponseDataPath": null,
                    "OptionDisplayKeys": null,
                    "OptionValueKey": null,
                    "LoadImmediately": false,
                    "HierarchySelectionConfigs": null,
                    "DisabledDateConfig": null,
                    "RemoteUpdateConfig": null,
                    "RowNumber": 3,
                    "ColumnNumber": null,
                    "Id": 11130,
                    "ParentType": "PART",
                    "ParentId": 60,
                    "FieldOptions": "[{\"Value\" : \"_ds_tb\", \"Key\" : \"DS-TB\"}, {\"Value\":\"_dr_tb\", \"Key\":\"DR-TB\"}]",
                    "ValidationList": null,
                    "DefaultValue": null,
                    "Key": "TreatmentDetailsTBCategory",
                    "HasInfo": false,
                    "InfoText": null,
                    "Config": null,
                    "ColumnWidth": 6,
                    "HasToggleButton": false
                },
                {
                    "Value": null,
                    "PartName": "TreatmentDetails",
                    "Placeholder": null,
                    "Component": "app-input-field",
                    "ComponentVariants": null,
                    "IsDisabled": false,
                    "IsVisible": true,
                    "LabelKey": "_tb_number",
                    "Label": "TB Number",
                    "Name": "TBNumber",
                    "IsHierarchySelector": false,
                    "IsRequired": false,
                    "RemoteUrl": null,
                    "Type": null,
                    "AddOn": null,
                    "Options": null,
                    "HierarchyOptions": null,
                    "OptionsWithKeyValue": null,
                    "AdditionalInfo": null,
                    "DefaultVisibilty": false,
                    "Order": 0,
                    "OptionsWithLabel": null,
                    "Validations": {
                        "Or": null,
                        "And": []
                    },
                    "ResponseDataPath": null,
                    "OptionDisplayKeys": null,
                    "OptionValueKey": null,
                    "LoadImmediately": false,
                    "HierarchySelectionConfigs": null,
                    "DisabledDateConfig": null,
                    "RemoteUpdateConfig": null,
                    "RowNumber": 17,
                    "ColumnNumber": null,
                    "Id": 11144,
                    "ParentType": "PART",
                    "ParentId": 60,
                    "FieldOptions": null,
                    "ValidationList": null,
                    "DefaultValue": null,
                    "Key": "TreatmentDetailsTBNumber",
                    "HasInfo": false,
                    "InfoText": null,
                    "Config": null,
                    "ColumnWidth": 6,
                    "HasToggleButton": false
                },
                {
                    "Value": null,
                    "PartName": "TreatmentDetails",
                    "Placeholder": null,
                    "Component": "app-datepicker",
                    "ComponentVariants": null,
                    "IsDisabled": false,
                    "IsVisible": true,
                    "LabelKey": "TBTreatmentStartDate",
                    "Label": "Date on which patient started treatment (Can be same or before adherence technology start date)",
                    "Name": "TBTreatmentStartDate",
                    "IsHierarchySelector": false,
                    "IsRequired": true,
                    "RemoteUrl": null,
                    "Type": null,
                    "AddOn": null,
                    "Options": null,
                    "HierarchyOptions": null,
                    "OptionsWithKeyValue": null,
                    "AdditionalInfo": null,
                    "DefaultVisibilty": false,
                    "Order": 0,
                    "OptionsWithLabel": null,
                    "Validations": {
                        "Or": null,
                        "And": [
                            {
                                "Type": "Required",
                                "Max": 0,
                                "Min": 0,
                                "IsBackendValidation": false,
                                "ValidationUrl": null,
                                "ShowServerError": false,
                                "ErrorTargetField": null,
                                "ValidationParams": null,
                                "ErrorMessage": "This field is required",
                                "ErrorMessageKey": "error_field_required",
                                "RequiredOnlyWhen": null,
                                "Regex": null
                            }
                        ]
                    },
                    "ResponseDataPath": null,
                    "OptionDisplayKeys": null,
                    "OptionValueKey": null,
                    "LoadImmediately": false,
                    "HierarchySelectionConfigs": null,
                    "DisabledDateConfig": {
                        "To": "0001-01-01T00:00:00",
                        "From": "2022-03-27T12:20:42.0573645",
                        "Days": null,
                        "DaysOfMonth": null,
                        "Dates": null,
                        "Ranges": null,
                        "AddDaysFromToday": 0,
                        "SubtractDaysFromToday": null
                    },
                    "RemoteUpdateConfig": null,
                    "RowNumber": 1,
                    "ColumnNumber": null,
                    "Id": 11128,
                    "ParentType": "PART",
                    "ParentId": 60,
                    "FieldOptions": null,
                    "ValidationList": null,
                    "DefaultValue": null,
                    "Key": "TreatmentDetailsTBTreatmentStartDate",
                    "HasInfo": false,
                    "InfoText": null,
                    "Config": null,
                    "ColumnWidth": 6,
                    "HasToggleButton": false
                },
                {
                    "Value": null,
                    "PartName": "TreatmentDetails",
                    "Placeholder": null,
                    "Component": "app-datepicker",
                    "ComponentVariants": null,
                    "IsDisabled": false,
                    "IsVisible": true,
                    "LabelKey": "EnrollmentDate",
                    "Label": "Date on which patient started using adherence technology",
                    "Name": "EnrollmentDate",
                    "IsHierarchySelector": false,
                    "IsRequired": true,
                    "RemoteUrl": null,
                    "Type": null,
                    "AddOn": null,
                    "Options": null,
                    "HierarchyOptions": null,
                    "OptionsWithKeyValue": null,
                    "AdditionalInfo": null,
                    "DefaultVisibilty": false,
                    "Order": 0,
                    "OptionsWithLabel": null,
                    "Validations": {
                        "Or": null,
                        "And": [
                            {
                                "Type": "Required",
                                "Max": 0,
                                "Min": 0,
                                "IsBackendValidation": false,
                                "ValidationUrl": null,
                                "ShowServerError": false,
                                "ErrorTargetField": null,
                                "ValidationParams": null,
                                "ErrorMessage": "This field is required",
                                "ErrorMessageKey": "error_field_required",
                                "RequiredOnlyWhen": null,
                                "Regex": null
                            }
                        ]
                    },
                    "ResponseDataPath": null,
                    "OptionDisplayKeys": null,
                    "OptionValueKey": null,
                    "LoadImmediately": false,
                    "HierarchySelectionConfigs": null,
                    "DisabledDateConfig": {
                        "To": "2021-09-28T12:20:42.0573645",
                        "From": "2022-03-27T12:20:42.0573645",
                        "Days": null,
                        "DaysOfMonth": null,
                        "Dates": null,
                        "Ranges": null,
                        "AddDaysFromToday": 0,
                        "SubtractDaysFromToday": 180
                    },
                    "RemoteUpdateConfig": null,
                    "RowNumber": 2,
                    "ColumnNumber": null,
                    "Id": 11129,
                    "ParentType": "PART",
                    "ParentId": 60,
                    "FieldOptions": null,
                    "ValidationList": null,
                    "DefaultValue": null,
                    "Key": "TreatmentDetailsEnrollmentDate",
                    "HasInfo": false,
                    "InfoText": null,
                    "Config": null,
                    "ColumnWidth": 6,
                    "HasToggleButton": false
                },
                {
                    "Value": null,
                    "PartName": "TreatmentDetails",
                    "Placeholder": null,
                    "Component": "app-input-field",
                    "ComponentVariants": null,
                    "IsDisabled": false,
                    "IsVisible": true,
                    "LabelKey": "TreatmentLength",
                    "Label": "Total number of days of treatment",
                    "Name": "TreatmentLength",
                    "IsHierarchySelector": false,
                    "IsRequired": true,
                    "RemoteUrl": null,
                    "Type": null,
                    "AddOn": null,
                    "Options": null,
                    "HierarchyOptions": null,
                    "OptionsWithKeyValue": null,
                    "AdditionalInfo": null,
                    "DefaultVisibilty": false,
                    "Order": 0,
                    "OptionsWithLabel": null,
                    "Validations": {
                        "Or": null,
                        "And": [
                            {
                                "Type": "Required",
                                "Max": 0,
                                "Min": 0,
                                "IsBackendValidation": false,
                                "ValidationUrl": null,
                                "ShowServerError": false,
                                "ErrorTargetField": null,
                                "ValidationParams": null,
                                "ErrorMessage": "This field is required",
                                "ErrorMessageKey": "error_field_required",
                                "RequiredOnlyWhen": null,
                                "Regex": null
                            }
                        ]
                    },
                    "ResponseDataPath": null,
                    "OptionDisplayKeys": null,
                    "OptionValueKey": null,
                    "LoadImmediately": false,
                    "HierarchySelectionConfigs": null,
                    "DisabledDateConfig": null,
                    "RemoteUpdateConfig": null,
                    "RowNumber": 20,
                    "ColumnNumber": null,
                    "Id": 11147,
                    "ParentType": "PART",
                    "ParentId": 60,
                    "FieldOptions": null,
                    "ValidationList": null,
                    "DefaultValue": "168",
                    "Key": "TreatmentDetailsTreatmentLength",
                    "HasInfo": false,
                    "InfoText": null,
                    "Config": null,
                    "ColumnWidth": 6,
                    "HasToggleButton": false
                },
                {
                    "Value": null,
                    "PartName": "TreatmentDetails",
                    "Placeholder": null,
                    "Component": "app-select",
                    "ComponentVariants": null,
                    "IsDisabled": false,
                    "IsVisible": true,
                    "LabelKey": "adherence_technology",
                    "Label": "Adherence Technology",
                    "Name": "MonitoringMethod",
                    "IsHierarchySelector": false,
                    "IsRequired": true,
                    "RemoteUrl": null,
                    "Type": null,
                    "AddOn": null,
                    "Options": null,
                    "HierarchyOptions": null,
                    "OptionsWithKeyValue": [
                        {
                            "Value": "evriMED(MERM)",
                            "Key": "MERM",
                            "AdherenceTechOptions": "MERM"
                        },
                        {
                            "Value": "VOT",
                            "Key": "VOT",
                            "AdherenceTechOptions": "VOT"
                        },
                        {
                            "Value": "Envelopes - 99DOTS",
                            "Key": "99DOTS",
                            "AdherenceTechOptions": "99DOTS"
                        },
                        {
                            "Value": "Stickers/Labels - 99DOTS Lite",
                            "Key": "99DOTSLite",
                            "AdherenceTechOptions": "99DOTSLite"
                        },
                        {
                            "Value": "Followed up Without Technology",
                            "Key": "NONE",
                            "AdherenceTechOptions": "NONE"
                        }
                    ],
                    "AdditionalInfo": null,
                    "DefaultVisibilty": false,
                    "Order": 0,
                    "OptionsWithLabel": null,
                    "Validations": {
                        "Or": null,
                        "And": [
                            {
                                "Type": "Required",
                                "Max": 0,
                                "Min": 0,
                                "IsBackendValidation": false,
                                "ValidationUrl": null,
                                "ShowServerError": false,
                                "ErrorTargetField": null,
                                "ValidationParams": null,
                                "ErrorMessage": "This field is required",
                                "ErrorMessageKey": "error_field_required",
                                "RequiredOnlyWhen": null,
                                "Regex": null
                            }
                        ]
                    },
                    "ResponseDataPath": null,
                    "OptionDisplayKeys": null,
                    "OptionValueKey": null,
                    "LoadImmediately": false,
                    "HierarchySelectionConfigs": null,
                    "DisabledDateConfig": null,
                    "RemoteUpdateConfig": null,
                    "RowNumber": 6,
                    "ColumnNumber": null,
                    "Id": 11133,
                    "ParentType": "PART",
                    "ParentId": 60,
                    "FieldOptions": "[{\"Value\" : \"evriMED(MERM)\", \"Key\" : \"MERM\", \"AdherenceTechOptions\": \"MERM\"}, {\"Value\":\"_VOT\", \"Key\":\"VOT\", \"AdherenceTechOptions\":\"VOT\"}, {\"Value\":\"_99dots\", \"Key\":\"99DOTS\", \"AdherenceTechOptions\": \"99DOTS\"}, {\"Value\":\"_99dotsLite\", \"Key\":\"99DOTSLite\", \"AdherenceTechOptions\": \"99DOTSLite\"}, {\"Value\":\"FollowedWithoutTechnology\", \"Key\":\"NONE\", \"AdherenceTechOptions\": \"NONE\"}]",
                    "ValidationList": null,
                    "DefaultValue": null,
                    "Key": "TreatmentDetailsMonitoringMethod",
                    "HasInfo": false,
                    "InfoText": null,
                    "Config": null,
                    "ColumnWidth": 6,
                    "HasToggleButton": false
                },
                {
                    "Value": null,
                    "PartName": "TreatmentDetails",
                    "Placeholder": null,
                    "Component": "app-search-select",
                    "ComponentVariants": null,
                    "IsDisabled": false,
                    "IsVisible": false,
                    "LabelKey": "MermSerialNoObj",
                    "Label": "Enter evriMED(MERM) Serial No",
                    "Name": "MermSerialNoObj",
                    "IsHierarchySelector": false,
                    "IsRequired": true,
                    "RemoteUrl": "/api/Merm/GetAvailableMermImeisNew",
                    "Type": null,
                    "AddOn": null,
                    "Options": null,
                    "HierarchyOptions": null,
                    "OptionsWithKeyValue": null,
                    "AdditionalInfo": null,
                    "DefaultVisibilty": false,
                    "Order": 0,
                    "OptionsWithLabel": null,
                    "Validations": {
                        "Or": null,
                        "And": [
                            {
                                "Type": "Required",
                                "Max": 0,
                                "Min": 0,
                                "IsBackendValidation": false,
                                "ValidationUrl": null,
                                "ShowServerError": false,
                                "ErrorTargetField": null,
                                "ValidationParams": null,
                                "ErrorMessage": "This field is required",
                                "ErrorMessageKey": "error_field_required",
                                "RequiredOnlyWhen": null,
                                "Regex": null
                            }
                        ]
                    },
                    "ResponseDataPath": [
                        "Data"
                    ],
                    "OptionDisplayKeys": [
                        "Value"
                    ],
                    "OptionValueKey": null,
                    "LoadImmediately": true,
                    "HierarchySelectionConfigs": null,
                    "DisabledDateConfig": null,
                    "RemoteUpdateConfig": [
                        {
                            "FormPart": "TreatmentDetails",
                            "Field": "MermSerialNoObj",
                            "Url": "/api/Merm/GetAvailableMermImeisNew",
                            "ConditionalCall": true,
                            "ValueDependency": [
                                {
                                    "CheckNotNull": false,
                                    "ExpectedValue": "MERM",
                                    "Field": "MonitoringMethod",
                                    "FormPart": "TreatmentDetails",
                                    "IsObject": false,
                                    "ObjectProperty": null
                                },
                                {
                                    "CheckNotNull": true,
                                    "ExpectedValue": null,
                                    "Field": "EnrollmentDate",
                                    "FormPart": "TreatmentDetails",
                                    "IsObject": false,
                                    "ObjectProperty": null
                                },
                                {
                                    "CheckNotNull": true,
                                    "ExpectedValue": null,
                                    "Field": "HierarchyMapping_Residence",
                                    "FormPart": "Residence",
                                    "IsObject": true,
                                    "ObjectProperty": "Id"
                                }
                            ],
                            "UrlVariables": {
                                "EnrollmentDate": [
                                    "TreatmentDetails",
                                    "EnrollmentDate"
                                ],
                                "HierarchyId": [
                                    "Residence",
                                    "HierarchyMapping_Residence",
                                    "Id"
                                ]
                            },
                            "RequestType": "GET",
                            "ResponsePath": [
                                "Data"
                            ]
                        }
                    ],
                    "RowNumber": 7,
                    "ColumnNumber": null,
                    "Id": 11134,
                    "ParentType": "PART",
                    "ParentId": 60,
                    "FieldOptions": null,
                    "ValidationList": null,
                    "DefaultValue": null,
                    "Key": "TreatmentDetailsMermSerialNoObj",
                    "HasInfo": false,
                    "InfoText": null,
                    "Config": null,
                    "ColumnWidth": 6,
                    "HasToggleButton": false
                },
                {
                    "Value": null,
                    "PartName": "TreatmentDetails",
                    "Placeholder": null,
                    "Component": "app-checkbox-group",
                    "ComponentVariants": null,
                    "IsDisabled": false,
                    "IsVisible": false,
                    "LabelKey": "MermAlarmEnabled",
                    "Label": "evriMED(MERM) Alarm Enabled",
                    "Name": "MermAlarmEnabled",
                    "IsHierarchySelector": false,
                    "IsRequired": false,
                    "RemoteUrl": null,
                    "Type": null,
                    "AddOn": null,
                    "Options": null,
                    "HierarchyOptions": null,
                    "OptionsWithKeyValue": [
                        {
                            "Value": "",
                            "Key": "true"
                        }
                    ],
                    "AdditionalInfo": null,
                    "DefaultVisibilty": false,
                    "Order": 0,
                    "OptionsWithLabel": null,
                    "Validations": {
                        "Or": null,
                        "And": []
                    },
                    "ResponseDataPath": null,
                    "OptionDisplayKeys": null,
                    "OptionValueKey": null,
                    "LoadImmediately": false,
                    "HierarchySelectionConfigs": null,
                    "DisabledDateConfig": null,
                    "RemoteUpdateConfig": null,
                    "RowNumber": 8,
                    "ColumnNumber": null,
                    "Id": 11135,
                    "ParentType": "PART",
                    "ParentId": 60,
                    "FieldOptions": "[{\"Value\" : \"\", \"Key\" : \"true\"}]",
                    "ValidationList": null,
                    "DefaultValue": null,
                    "Key": "TreatmentDetailsMermAlarmEnabled",
                    "HasInfo": false,
                    "InfoText": null,
                    "Config": null,
                    "ColumnWidth": 6,
                    "HasToggleButton": false
                },
                {
                    "Value": null,
                    "PartName": "TreatmentDetails",
                    "Placeholder": null,
                    "Component": "app-select",
                    "ComponentVariants": null,
                    "IsDisabled": false,
                    "IsVisible": false,
                    "LabelKey": "MermAlarmTime",
                    "Label": "evriMED(MERM) Alarm Time",
                    "Name": "MermAlarmTime",
                    "IsHierarchySelector": false,
                    "IsRequired": true,
                    "RemoteUrl": null,
                    "Type": null,
                    "AddOn": null,
                    "Options": null,
                    "HierarchyOptions": null,
                    "OptionsWithKeyValue": [
                        {
                            "Value": "00:00",
                            "Key": "00:00"
                        },
                        {
                            "Value": "00:30",
                            "Key": "00:30"
                        },
                        {
                            "Value": "01:00",
                            "Key": "01:00"
                        },
                        {
                            "Value": "01:30",
                            "Key": "01:30"
                        },
                        {
                            "Value": "02:00",
                            "Key": "02:00"
                        },
                        {
                            "Value": "02:30",
                            "Key": "02:30"
                        },
                        {
                            "Value": "03:00",
                            "Key": "03:00"
                        },
                        {
                            "Value": "03:30",
                            "Key": "03:30"
                        },
                        {
                            "Value": "04:00",
                            "Key": "04:00"
                        },
                        {
                            "Value": "04:30",
                            "Key": "04:30"
                        },
                        {
                            "Value": "05:00",
                            "Key": "05:00"
                        },
                        {
                            "Value": "05:30",
                            "Key": "05:30"
                        },
                        {
                            "Value": "06:00",
                            "Key": "06:00"
                        },
                        {
                            "Value": "06:30",
                            "Key": "06:30"
                        },
                        {
                            "Value": "07:00",
                            "Key": "07:00"
                        },
                        {
                            "Value": "07:30",
                            "Key": "07:30"
                        },
                        {
                            "Value": "08:00",
                            "Key": "08:00"
                        },
                        {
                            "Value": "08:30",
                            "Key": "08:30"
                        },
                        {
                            "Value": "09:00",
                            "Key": "09:00"
                        },
                        {
                            "Value": "09:30",
                            "Key": "09:30"
                        },
                        {
                            "Value": "10:00",
                            "Key": "10:00"
                        },
                        {
                            "Value": "10:30",
                            "Key": "10:30"
                        },
                        {
                            "Value": "11:00",
                            "Key": "11:00"
                        },
                        {
                            "Value": "11:30",
                            "Key": "11:30"
                        },
                        {
                            "Value": "12:00",
                            "Key": "12:00"
                        },
                        {
                            "Value": "12:30",
                            "Key": "12:30"
                        },
                        {
                            "Value": "13:00",
                            "Key": "13:00"
                        },
                        {
                            "Value": "13:30",
                            "Key": "13:30"
                        },
                        {
                            "Value": "14:00",
                            "Key": "14:00"
                        },
                        {
                            "Value": "14:30",
                            "Key": "14:30"
                        },
                        {
                            "Value": "15:00",
                            "Key": "15:00"
                        },
                        {
                            "Value": "15:30",
                            "Key": "15:30"
                        },
                        {
                            "Value": "16:00",
                            "Key": "16:00"
                        },
                        {
                            "Value": "16:30",
                            "Key": "16:30"
                        },
                        {
                            "Value": "17:00",
                            "Key": "17:00"
                        },
                        {
                            "Value": "17:30",
                            "Key": "17:30"
                        },
                        {
                            "Value": "18:00",
                            "Key": "18:00"
                        },
                        {
                            "Value": "18:30",
                            "Key": "18:30"
                        },
                        {
                            "Value": "19:00",
                            "Key": "19:00"
                        },
                        {
                            "Value": "19:30",
                            "Key": "19:30"
                        },
                        {
                            "Value": "20:00",
                            "Key": "20:00"
                        },
                        {
                            "Value": "20:30",
                            "Key": "20:30"
                        },
                        {
                            "Value": "21:00",
                            "Key": "21:00"
                        },
                        {
                            "Value": "21:30",
                            "Key": "21:30"
                        },
                        {
                            "Value": "22:00",
                            "Key": "22:00"
                        },
                        {
                            "Value": "22:30",
                            "Key": "22:30"
                        },
                        {
                            "Value": "23:00",
                            "Key": "23:00"
                        },
                        {
                            "Value": "23:30",
                            "Key": "23:30"
                        }
                    ],
                    "AdditionalInfo": null,
                    "DefaultVisibilty": false,
                    "Order": 0,
                    "OptionsWithLabel": null,
                    "Validations": {
                        "Or": null,
                        "And": [
                            {
                                "Type": "Required",
                                "Max": 0,
                                "Min": 0,
                                "IsBackendValidation": false,
                                "ValidationUrl": null,
                                "ShowServerError": false,
                                "ErrorTargetField": null,
                                "ValidationParams": null,
                                "ErrorMessage": "This field is required",
                                "ErrorMessageKey": "error_field_required",
                                "RequiredOnlyWhen": null,
                                "Regex": null
                            }
                        ]
                    },
                    "ResponseDataPath": null,
                    "OptionDisplayKeys": null,
                    "OptionValueKey": null,
                    "LoadImmediately": false,
                    "HierarchySelectionConfigs": null,
                    "DisabledDateConfig": null,
                    "RemoteUpdateConfig": null,
                    "RowNumber": 9,
                    "ColumnNumber": null,
                    "Id": 11136,
                    "ParentType": "PART",
                    "ParentId": 60,
                    "FieldOptions": "[{\"Value\" : \"00:00\", \"Key\" : \"00:00\"}, {\"Value\":\"00:30\", \"Key\":\"00:30\"}, {\"Value\" : \"01:00\", \"Key\" : \"01:00\"}, {\"Value\":\"01:30\", \"Key\":\"01:30\"}, {\"Value\" : \"02:00\", \"Key\" : \"02:00\"}, {\"Value\":\"02:30\", \"Key\":\"02:30\"}, {\"Value\" : \"03:00\", \"Key\" : \"03:00\"}, {\"Value\":\"03:30\", \"Key\":\"03:30\"}, {\"Value\" : \"04:00\", \"Key\" : \"04:00\"}, {\"Value\":\"04:30\", \"Key\":\"04:30\"}, {\"Value\" : \"05:00\", \"Key\" : \"05:00\"}, {\"Value\":\"05:30\", \"Key\":\"05:30\"}, {\"Value\" : \"06:00\", \"Key\" : \"06:00\"}, {\"Value\":\"06:30\", \"Key\":\"06:30\"}, {\"Value\" : \"07:00\", \"Key\" : \"07:00\"}, {\"Value\":\"07:30\", \"Key\":\"07:30\"}, {\"Value\" : \"08:00\", \"Key\" : \"08:00\"}, {\"Value\":\"08:30\", \"Key\":\"08:30\"}, {\"Value\" : \"09:00\", \"Key\" : \"09:00\"}, {\"Value\":\"09:30\", \"Key\":\"09:30\"}, {\"Value\" : \"10:00\", \"Key\" : \"10:00\"}, {\"Value\":\"10:30\", \"Key\":\"10:30\"}, {\"Value\" : \"11:00\", \"Key\" : \"11:00\"}, {\"Value\":\"11:30\", \"Key\":\"11:30\"}, {\"Value\" : \"12:00\", \"Key\" : \"12:00\"}, {\"Value\":\"12:30\", \"Key\":\"12:30\"},{\"Value\" : \"13:00\", \"Key\" : \"13:00\"}, {\"Value\":\"13:30\", \"Key\":\"13:30\"}, {\"Value\" : \"14:00\", \"Key\" : \"14:00\"}, {\"Value\":\"14:30\", \"Key\":\"14:30\"}, {\"Value\" : \"15:00\", \"Key\" : \"15:00\"}, {\"Value\":\"15:30\", \"Key\":\"15:30\"},{\"Value\" : \"16:00\", \"Key\" : \"16:00\"}, {\"Value\":\"16:30\", \"Key\":\"16:30\"},{\"Value\" : \"17:00\", \"Key\" : \"17:00\"}, {\"Value\":\"17:30\", \"Key\":\"17:30\"}, {\"Value\" : \"18:00\", \"Key\" : \"18:00\"}, {\"Value\":\"18:30\", \"Key\":\"18:30\"}, {\"Value\" : \"19:00\", \"Key\" : \"19:00\"}, {\"Value\":\"19:30\", \"Key\":\"19:30\"}, {\"Value\" : \"20:00\", \"Key\" : \"20:00\"}, {\"Value\":\"20:30\", \"Key\":\"20:30\"}, {\"Value\" : \"21:00\", \"Key\" : \"21:00\"}, {\"Value\":\"21:30\", \"Key\":\"21:30\"},{\"Value\" : \"22:00\", \"Key\" : \"22:00\"}, {\"Value\":\"22:30\", \"Key\":\"22:30\"}, {\"Value\" : \"23:00\", \"Key\" : \"23:00\"}, {\"Value\":\"23:30\", \"Key\":\"23:30\"}]",
                    "ValidationList": null,
                    "DefaultValue": null,
                    "Key": "TreatmentDetailsMermAlarmTime",
                    "HasInfo": false,
                    "InfoText": null,
                    "Config": null,
                    "ColumnWidth": 6,
                    "HasToggleButton": false
                },
                {
                    "Value": null,
                    "PartName": "TreatmentDetails",
                    "Placeholder": null,
                    "Component": "app-checkbox-group",
                    "ComponentVariants": null,
                    "IsDisabled": false,
                    "IsVisible": false,
                    "LabelKey": "MermRefillAlarmEnabled",
                    "Label": "evriMED(MERM) Refill Alarm Enabled",
                    "Name": "MermRefillAlarmEnabled",
                    "IsHierarchySelector": false,
                    "IsRequired": false,
                    "RemoteUrl": null,
                    "Type": null,
                    "AddOn": null,
                    "Options": null,
                    "HierarchyOptions": null,
                    "OptionsWithKeyValue": [
                        {
                            "Value": "",
                            "Key": "true"
                        }
                    ],
                    "AdditionalInfo": null,
                    "DefaultVisibilty": false,
                    "Order": 0,
                    "OptionsWithLabel": null,
                    "Validations": {
                        "Or": null,
                        "And": []
                    },
                    "ResponseDataPath": null,
                    "OptionDisplayKeys": null,
                    "OptionValueKey": null,
                    "LoadImmediately": false,
                    "HierarchySelectionConfigs": null,
                    "DisabledDateConfig": null,
                    "RemoteUpdateConfig": null,
                    "RowNumber": 10,
                    "ColumnNumber": null,
                    "Id": 11137,
                    "ParentType": "PART",
                    "ParentId": 60,
                    "FieldOptions": "[{\"Value\" : \"\", \"Key\" : \"true\"}]",
                    "ValidationList": null,
                    "DefaultValue": null,
                    "Key": "TreatmentDetailsMermRefillAlarmEnabled",
                    "HasInfo": false,
                    "InfoText": null,
                    "Config": null,
                    "ColumnWidth": 6,
                    "HasToggleButton": false
                },
                {
                    "Value": null,
                    "PartName": "TreatmentDetails",
                    "Placeholder": null,
                    "Component": "app-datepicker",
                    "ComponentVariants": null,
                    "IsDisabled": false,
                    "IsVisible": false,
                    "LabelKey": "MermRefillAlarmDate",
                    "Label": "evriMED(MERM) Refill Alarm Date",
                    "Name": "MermRefillAlarmDate",
                    "IsHierarchySelector": false,
                    "IsRequired": true,
                    "RemoteUrl": null,
                    "Type": null,
                    "AddOn": null,
                    "Options": null,
                    "HierarchyOptions": null,
                    "OptionsWithKeyValue": null,
                    "AdditionalInfo": null,
                    "DefaultVisibilty": false,
                    "Order": 0,
                    "OptionsWithLabel": null,
                    "Validations": {
                        "Or": null,
                        "And": [
                            {
                                "Type": "Required",
                                "Max": 0,
                                "Min": 0,
                                "IsBackendValidation": false,
                                "ValidationUrl": null,
                                "ShowServerError": false,
                                "ErrorTargetField": null,
                                "ValidationParams": null,
                                "ErrorMessage": "This field is required",
                                "ErrorMessageKey": "error_field_required",
                                "RequiredOnlyWhen": null,
                                "Regex": null
                            }
                        ]
                    },
                    "ResponseDataPath": null,
                    "OptionDisplayKeys": null,
                    "OptionValueKey": null,
                    "LoadImmediately": false,
                    "HierarchySelectionConfigs": null,
                    "DisabledDateConfig": null,
                    "RemoteUpdateConfig": null,
                    "RowNumber": 11,
                    "ColumnNumber": null,
                    "Id": 11138,
                    "ParentType": "PART",
                    "ParentId": 60,
                    "FieldOptions": null,
                    "ValidationList": null,
                    "DefaultValue": null,
                    "Key": "TreatmentDetailsMermRefillAlarmDate",
                    "HasInfo": false,
                    "InfoText": null,
                    "Config": null,
                    "ColumnWidth": 6,
                    "HasToggleButton": false
                },
                {
                    "Value": null,
                    "PartName": "TreatmentDetails",
                    "Placeholder": null,
                    "Component": "app-select",
                    "ComponentVariants": null,
                    "IsDisabled": false,
                    "IsVisible": false,
                    "LabelKey": "MermRefillAlarmTime",
                    "Label": "evriMED(MERM) Refill Alarm Time",
                    "Name": "MermRefillAlarmTime",
                    "IsHierarchySelector": false,
                    "IsRequired": true,
                    "RemoteUrl": null,
                    "Type": null,
                    "AddOn": null,
                    "Options": null,
                    "HierarchyOptions": null,
                    "OptionsWithKeyValue": [
                        {
                            "Value": "00:00",
                            "Key": "00:00"
                        },
                        {
                            "Value": "00:30",
                            "Key": "00:30"
                        },
                        {
                            "Value": "01:00",
                            "Key": "01:00"
                        },
                        {
                            "Value": "01:30",
                            "Key": "01:30"
                        },
                        {
                            "Value": "02:00",
                            "Key": "02:00"
                        },
                        {
                            "Value": "02:30",
                            "Key": "02:30"
                        },
                        {
                            "Value": "03:00",
                            "Key": "03:00"
                        },
                        {
                            "Value": "03:30",
                            "Key": "03:30"
                        },
                        {
                            "Value": "04:00",
                            "Key": "04:00"
                        },
                        {
                            "Value": "04:30",
                            "Key": "04:30"
                        },
                        {
                            "Value": "05:00",
                            "Key": "05:00"
                        },
                        {
                            "Value": "05:30",
                            "Key": "05:30"
                        },
                        {
                            "Value": "06:00",
                            "Key": "06:00"
                        },
                        {
                            "Value": "06:30",
                            "Key": "06:30"
                        },
                        {
                            "Value": "07:00",
                            "Key": "07:00"
                        },
                        {
                            "Value": "07:30",
                            "Key": "07:30"
                        },
                        {
                            "Value": "08:00",
                            "Key": "08:00"
                        },
                        {
                            "Value": "08:30",
                            "Key": "08:30"
                        },
                        {
                            "Value": "09:00",
                            "Key": "09:00"
                        },
                        {
                            "Value": "09:30",
                            "Key": "09:30"
                        },
                        {
                            "Value": "10:00",
                            "Key": "10:00"
                        },
                        {
                            "Value": "10:30",
                            "Key": "10:30"
                        },
                        {
                            "Value": "11:00",
                            "Key": "11:00"
                        },
                        {
                            "Value": "11:30",
                            "Key": "11:30"
                        },
                        {
                            "Value": "12:00",
                            "Key": "12:00"
                        },
                        {
                            "Value": "12:30",
                            "Key": "12:30"
                        },
                        {
                            "Value": "13:00",
                            "Key": "13:00"
                        },
                        {
                            "Value": "13:30",
                            "Key": "13:30"
                        },
                        {
                            "Value": "14:00",
                            "Key": "14:00"
                        },
                        {
                            "Value": "14:30",
                            "Key": "14:30"
                        },
                        {
                            "Value": "15:00",
                            "Key": "15:00"
                        },
                        {
                            "Value": "15:30",
                            "Key": "15:30"
                        },
                        {
                            "Value": "16:00",
                            "Key": "16:00"
                        },
                        {
                            "Value": "16:30",
                            "Key": "16:30"
                        },
                        {
                            "Value": "17:00",
                            "Key": "17:00"
                        },
                        {
                            "Value": "17:30",
                            "Key": "17:30"
                        },
                        {
                            "Value": "18:00",
                            "Key": "18:00"
                        },
                        {
                            "Value": "18:30",
                            "Key": "18:30"
                        },
                        {
                            "Value": "19:00",
                            "Key": "19:00"
                        },
                        {
                            "Value": "19:30",
                            "Key": "19:30"
                        },
                        {
                            "Value": "20:00",
                            "Key": "20:00"
                        },
                        {
                            "Value": "20:30",
                            "Key": "20:30"
                        },
                        {
                            "Value": "21:00",
                            "Key": "21:00"
                        },
                        {
                            "Value": "21:30",
                            "Key": "21:30"
                        },
                        {
                            "Value": "22:00",
                            "Key": "22:00"
                        },
                        {
                            "Value": "22:30",
                            "Key": "22:30"
                        },
                        {
                            "Value": "23:00",
                            "Key": "23:00"
                        },
                        {
                            "Value": "23:30",
                            "Key": "23:30"
                        }
                    ],
                    "AdditionalInfo": null,
                    "DefaultVisibilty": false,
                    "Order": 0,
                    "OptionsWithLabel": null,
                    "Validations": {
                        "Or": null,
                        "And": [
                            {
                                "Type": "Required",
                                "Max": 0,
                                "Min": 0,
                                "IsBackendValidation": false,
                                "ValidationUrl": null,
                                "ShowServerError": false,
                                "ErrorTargetField": null,
                                "ValidationParams": null,
                                "ErrorMessage": "This field is required",
                                "ErrorMessageKey": "error_field_required",
                                "RequiredOnlyWhen": null,
                                "Regex": null
                            }
                        ]
                    },
                    "ResponseDataPath": null,
                    "OptionDisplayKeys": null,
                    "OptionValueKey": null,
                    "LoadImmediately": false,
                    "HierarchySelectionConfigs": null,
                    "DisabledDateConfig": null,
                    "RemoteUpdateConfig": null,
                    "RowNumber": 12,
                    "ColumnNumber": null,
                    "Id": 11139,
                    "ParentType": "PART",
                    "ParentId": 60,
                    "FieldOptions": "[{\"Value\" : \"00:00\", \"Key\" : \"00:00\"}, {\"Value\":\"00:30\", \"Key\":\"00:30\"}, {\"Value\" : \"01:00\", \"Key\" : \"01:00\"}, {\"Value\":\"01:30\", \"Key\":\"01:30\"}, {\"Value\" : \"02:00\", \"Key\" : \"02:00\"}, {\"Value\":\"02:30\", \"Key\":\"02:30\"}, {\"Value\" : \"03:00\", \"Key\" : \"03:00\"}, {\"Value\":\"03:30\", \"Key\":\"03:30\"}, {\"Value\" : \"04:00\", \"Key\" : \"04:00\"}, {\"Value\":\"04:30\", \"Key\":\"04:30\"}, {\"Value\" : \"05:00\", \"Key\" : \"05:00\"}, {\"Value\":\"05:30\", \"Key\":\"05:30\"}, {\"Value\" : \"06:00\", \"Key\" : \"06:00\"}, {\"Value\":\"06:30\", \"Key\":\"06:30\"}, {\"Value\" : \"07:00\", \"Key\" : \"07:00\"}, {\"Value\":\"07:30\", \"Key\":\"07:30\"}, {\"Value\" : \"08:00\", \"Key\" : \"08:00\"}, {\"Value\":\"08:30\", \"Key\":\"08:30\"}, {\"Value\" : \"09:00\", \"Key\" : \"09:00\"}, {\"Value\":\"09:30\", \"Key\":\"09:30\"}, {\"Value\" : \"10:00\", \"Key\" : \"10:00\"}, {\"Value\":\"10:30\", \"Key\":\"10:30\"}, {\"Value\" : \"11:00\", \"Key\" : \"11:00\"}, {\"Value\":\"11:30\", \"Key\":\"11:30\"}, {\"Value\" : \"12:00\", \"Key\" : \"12:00\"}, {\"Value\":\"12:30\", \"Key\":\"12:30\"},{\"Value\" : \"13:00\", \"Key\" : \"13:00\"}, {\"Value\":\"13:30\", \"Key\":\"13:30\"}, {\"Value\" : \"14:00\", \"Key\" : \"14:00\"}, {\"Value\":\"14:30\", \"Key\":\"14:30\"}, {\"Value\" : \"15:00\", \"Key\" : \"15:00\"}, {\"Value\":\"15:30\", \"Key\":\"15:30\"},{\"Value\" : \"16:00\", \"Key\" : \"16:00\"}, {\"Value\":\"16:30\", \"Key\":\"16:30\"},{\"Value\" : \"17:00\", \"Key\" : \"17:00\"}, {\"Value\":\"17:30\", \"Key\":\"17:30\"}, {\"Value\" : \"18:00\", \"Key\" : \"18:00\"}, {\"Value\":\"18:30\", \"Key\":\"18:30\"}, {\"Value\" : \"19:00\", \"Key\" : \"19:00\"}, {\"Value\":\"19:30\", \"Key\":\"19:30\"}, {\"Value\" : \"20:00\", \"Key\" : \"20:00\"}, {\"Value\":\"20:30\", \"Key\":\"20:30\"}, {\"Value\" : \"21:00\", \"Key\" : \"21:00\"}, {\"Value\":\"21:30\", \"Key\":\"21:30\"},{\"Value\" : \"22:00\", \"Key\" : \"22:00\"}, {\"Value\":\"22:30\", \"Key\":\"22:30\"}, {\"Value\" : \"23:00\", \"Key\" : \"23:00\"}, {\"Value\":\"23:30\", \"Key\":\"23:30\"}]",
                    "ValidationList": null,
                    "DefaultValue": null,
                    "Key": "TreatmentDetailsMermRefillAlarmTime",
                    "HasInfo": false,
                    "InfoText": null,
                    "Config": null,
                    "ColumnWidth": 6,
                    "HasToggleButton": false
                },
                {
                    "Value": null,
                    "PartName": "TreatmentDetails",
                    "Placeholder": null,
                    "Component": "app-input-field",
                    "ComponentVariants": null,
                    "IsDisabled": false,
                    "IsVisible": false,
                    "LabelKey": "AddedFrom",
                    "Label": "Added From",
                    "Name": "AddedFrom",
                    "IsHierarchySelector": false,
                    "IsRequired": false,
                    "RemoteUrl": null,
                    "Type": null,
                    "AddOn": null,
                    "Options": null,
                    "HierarchyOptions": null,
                    "OptionsWithKeyValue": null,
                    "AdditionalInfo": null,
                    "DefaultVisibilty": false,
                    "Order": 0,
                    "OptionsWithLabel": null,
                    "Validations": {
                        "Or": null,
                        "And": []
                    },
                    "ResponseDataPath": null,
                    "OptionDisplayKeys": null,
                    "OptionValueKey": null,
                    "LoadImmediately": false,
                    "HierarchySelectionConfigs": null,
                    "DisabledDateConfig": null,
                    "RemoteUpdateConfig": null,
                    "RowNumber": 25,
                    "ColumnNumber": null,
                    "Id": 11152,
                    "ParentType": "PART",
                    "ParentId": 60,
                    "FieldOptions": null,
                    "ValidationList": null,
                    "DefaultValue": "web",
                    "Key": "TreatmentDetailsAddedFrom",
                    "HasInfo": false,
                    "InfoText": null,
                    "Config": null,
                    "ColumnWidth": 6,
                    "HasToggleButton": false
                }
            ],
            "Triggers": null,
            "TriggerConfigs": null,
            "ValueDependencies": [
                {
                    "Lookups": [
                        {
                            "FormPart": "TreatmentDetails",
                            "Field": "TBCategory",
                            "ExpectedValue": null,
                            "ExpectedValues": null,
                            "IsObject": true,
                            "ObjectKeyAndValues": null,
                            "ObjectKeyAndValue": {
                                "DR-TB": "336",
                                "DS-TB": "168"
                            },
                            "AttributeName": null,
                            "ConstraintName": null,
                            "RestrictedValue": null,
                            "ComparisonOperator": "EQ",
                            "AnyObjectKeyValue": false,
                            "FilteredKeys": null,
                            "LookupFormPart": "TreatmentDetails",
                            "LookupField": "TBCategory"
                        }
                    ],
                    "FormPart": "TreatmentDetails",
                    "Field": "TreatmentLength",
                    "FormPartId": null,
                    "FieldId": null,
                    "Type": "VALUE",
                    "IsForm": false,
                    "PropertyAndValues": null
                }
            ],
            "FilterDependencies": [
                {
                    "Lookups": [
                        {
                            "FormPart": "Residence",
                            "Field": "HierarchyMapping_Residence",
                            "ExpectedValue": null,
                            "ExpectedValues": null,
                            "IsObject": false,
                            "ObjectKeyAndValues": null,
                            "ObjectKeyAndValue": null,
                            "AttributeName": "AdherenceTechOptions",
                            "ConstraintName": null,
                            "RestrictedValue": null,
                            "ComparisonOperator": "EQ",
                            "AnyObjectKeyValue": false,
                            "FilteredKeys": null,
                            "LookupFormPart": "Residence",
                            "LookupField": "HierarchyMapping_Residence"
                        }
                    ],
                    "FormPart": "TreatmentDetails",
                    "Field": "MonitoringMethod",
                    "FormPartId": null,
                    "FieldId": null,
                    "Type": "FILTER",
                    "IsForm": false,
                    "PropertyAndValues": null
                }
            ],
            "VisibilityDependencies": [
                {
                    "Lookups": [
                        {
                            "FormPart": "TreatmentDetails",
                            "Field": "MonitoringMethod",
                            "ExpectedValue": null,
                            "ExpectedValues": [
                                "MERM"
                            ],
                            "IsObject": false,
                            "ObjectKeyAndValues": null,
                            "ObjectKeyAndValue": null,
                            "AttributeName": null,
                            "ConstraintName": null,
                            "RestrictedValue": null,
                            "ComparisonOperator": "EQ",
                            "AnyObjectKeyValue": false,
                            "FilteredKeys": null,
                            "LookupFormPart": "TreatmentDetails",
                            "LookupField": "MonitoringMethod"
                        }
                    ],
                    "FormPart": "TreatmentDetails",
                    "Field": "MermSerialNoObj",
                    "FormPartId": null,
                    "FieldId": null,
                    "Type": "VISIBILITY",
                    "IsForm": false,
                    "PropertyAndValues": null
                },
                {
                    "Lookups": [
                        {
                            "FormPart": "TreatmentDetails",
                            "Field": "MonitoringMethod",
                            "ExpectedValue": null,
                            "ExpectedValues": [
                                "MERM"
                            ],
                            "IsObject": false,
                            "ObjectKeyAndValues": null,
                            "ObjectKeyAndValue": null,
                            "AttributeName": null,
                            "ConstraintName": null,
                            "RestrictedValue": null,
                            "ComparisonOperator": "EQ",
                            "AnyObjectKeyValue": false,
                            "FilteredKeys": null,
                            "LookupFormPart": "TreatmentDetails",
                            "LookupField": "MonitoringMethod"
                        }
                    ],
                    "FormPart": "TreatmentDetails",
                    "Field": "MermAlarmEnabled",
                    "FormPartId": null,
                    "FieldId": null,
                    "Type": "VISIBILITY",
                    "IsForm": false,
                    "PropertyAndValues": null
                },
                {
                    "Lookups": [
                        {
                            "FormPart": "TreatmentDetails",
                            "Field": "MermAlarmEnabled",
                            "ExpectedValue": null,
                            "ExpectedValues": [
                                "true"
                            ],
                            "IsObject": false,
                            "ObjectKeyAndValues": null,
                            "ObjectKeyAndValue": null,
                            "AttributeName": null,
                            "ConstraintName": null,
                            "RestrictedValue": null,
                            "ComparisonOperator": "EQ",
                            "AnyObjectKeyValue": false,
                            "FilteredKeys": null,
                            "LookupFormPart": "TreatmentDetails",
                            "LookupField": "MermAlarmEnabled"
                        },
                        {
                            "FormPart": "TreatmentDetails",
                            "Field": "MonitoringMethod",
                            "ExpectedValue": null,
                            "ExpectedValues": [
                                "MERM"
                            ],
                            "IsObject": false,
                            "ObjectKeyAndValues": null,
                            "ObjectKeyAndValue": null,
                            "AttributeName": null,
                            "ConstraintName": null,
                            "RestrictedValue": null,
                            "ComparisonOperator": "EQ",
                            "AnyObjectKeyValue": false,
                            "FilteredKeys": null,
                            "LookupFormPart": "TreatmentDetails",
                            "LookupField": "MonitoringMethod"
                        }
                    ],
                    "FormPart": "TreatmentDetails",
                    "Field": "MermAlarmTime",
                    "FormPartId": null,
                    "FieldId": null,
                    "Type": "VISIBILITY",
                    "IsForm": false,
                    "PropertyAndValues": null
                },
                {
                    "Lookups": [
                        {
                            "FormPart": "TreatmentDetails",
                            "Field": "MonitoringMethod",
                            "ExpectedValue": null,
                            "ExpectedValues": [
                                "MERM"
                            ],
                            "IsObject": false,
                            "ObjectKeyAndValues": null,
                            "ObjectKeyAndValue": null,
                            "AttributeName": null,
                            "ConstraintName": null,
                            "RestrictedValue": null,
                            "ComparisonOperator": "EQ",
                            "AnyObjectKeyValue": false,
                            "FilteredKeys": null,
                            "LookupFormPart": "TreatmentDetails",
                            "LookupField": "MonitoringMethod"
                        }
                    ],
                    "FormPart": "TreatmentDetails",
                    "Field": "MermRefillAlarmEnabled",
                    "FormPartId": null,
                    "FieldId": null,
                    "Type": "VISIBILITY",
                    "IsForm": false,
                    "PropertyAndValues": null
                },
                {
                    "Lookups": [
                        {
                            "FormPart": "TreatmentDetails",
                            "Field": "MermRefillAlarmEnabled",
                            "ExpectedValue": null,
                            "ExpectedValues": [
                                "true"
                            ],
                            "IsObject": false,
                            "ObjectKeyAndValues": null,
                            "ObjectKeyAndValue": null,
                            "AttributeName": null,
                            "ConstraintName": null,
                            "RestrictedValue": null,
                            "ComparisonOperator": "EQ",
                            "AnyObjectKeyValue": false,
                            "FilteredKeys": null,
                            "LookupFormPart": "TreatmentDetails",
                            "LookupField": "MermRefillAlarmEnabled"
                        },
                        {
                            "FormPart": "TreatmentDetails",
                            "Field": "MonitoringMethod",
                            "ExpectedValue": null,
                            "ExpectedValues": [
                                "MERM"
                            ],
                            "IsObject": false,
                            "ObjectKeyAndValues": null,
                            "ObjectKeyAndValue": null,
                            "AttributeName": null,
                            "ConstraintName": null,
                            "RestrictedValue": null,
                            "ComparisonOperator": "EQ",
                            "AnyObjectKeyValue": false,
                            "FilteredKeys": null,
                            "LookupFormPart": "TreatmentDetails",
                            "LookupField": "MonitoringMethod"
                        }
                    ],
                    "FormPart": "TreatmentDetails",
                    "Field": "MermRefillAlarmDate",
                    "FormPartId": null,
                    "FieldId": null,
                    "Type": "VISIBILITY",
                    "IsForm": false,
                    "PropertyAndValues": null
                },
                {
                    "Lookups": [
                        {
                            "FormPart": "TreatmentDetails",
                            "Field": "TBCategory",
                            "ExpectedValue": null,
                            "ExpectedValues": [
                                "DS-TB"
                            ],
                            "IsObject": false,
                            "ObjectKeyAndValues": null,
                            "ObjectKeyAndValue": null,
                            "AttributeName": null,
                            "ConstraintName": null,
                            "RestrictedValue": null,
                            "ComparisonOperator": "EQ",
                            "AnyObjectKeyValue": false,
                            "FilteredKeys": null,
                            "LookupFormPart": "TreatmentDetails",
                            "LookupField": "TBCategory"
                        }
                    ],
                    "FormPart": "TreatmentDetails",
                    "Field": "WeightBand",
                    "FormPartId": null,
                    "FieldId": null,
                    "Type": "VISIBILITY",
                    "IsForm": false,
                    "PropertyAndValues": null
                },
                {
                    "Lookups": [
                        {
                            "FormPart": "TreatmentDetails",
                            "Field": "TBCategory",
                            "ExpectedValue": null,
                            "ExpectedValues": [
                                "DR-TB"
                            ],
                            "IsObject": false,
                            "ObjectKeyAndValues": null,
                            "ObjectKeyAndValue": null,
                            "AttributeName": null,
                            "ConstraintName": null,
                            "RestrictedValue": null,
                            "ComparisonOperator": "EQ",
                            "AnyObjectKeyValue": false,
                            "FilteredKeys": null,
                            "LookupFormPart": "TreatmentDetails",
                            "LookupField": "TBCategory"
                        }
                    ],
                    "FormPart": "TreatmentDetails",
                    "Field": "NumberOfPills",
                    "FormPartId": null,
                    "FieldId": null,
                    "Type": "VISIBILITY",
                    "IsForm": false,
                    "PropertyAndValues": null
                },
                {
                    "Lookups": [
                        {
                            "FormPart": "TreatmentDetails",
                            "Field": "MermRefillAlarmEnabled",
                            "ExpectedValue": null,
                            "ExpectedValues": [
                                "true"
                            ],
                            "IsObject": false,
                            "ObjectKeyAndValues": null,
                            "ObjectKeyAndValue": null,
                            "AttributeName": null,
                            "ConstraintName": null,
                            "RestrictedValue": null,
                            "ComparisonOperator": "EQ",
                            "AnyObjectKeyValue": false,
                            "FilteredKeys": null,
                            "LookupFormPart": "TreatmentDetails",
                            "LookupField": "MermRefillAlarmEnabled"
                        },
                        {
                            "FormPart": "TreatmentDetails",
                            "Field": "MonitoringMethod",
                            "ExpectedValue": null,
                            "ExpectedValues": [
                                "MERM"
                            ],
                            "IsObject": false,
                            "ObjectKeyAndValues": null,
                            "ObjectKeyAndValue": null,
                            "AttributeName": null,
                            "ConstraintName": null,
                            "RestrictedValue": null,
                            "ComparisonOperator": "EQ",
                            "AnyObjectKeyValue": false,
                            "FilteredKeys": null,
                            "LookupFormPart": "TreatmentDetails",
                            "LookupField": "MonitoringMethod"
                        }
                    ],
                    "FormPart": "TreatmentDetails",
                    "Field": "MermRefillAlarmTime",
                    "FormPartId": null,
                    "FieldId": null,
                    "Type": "VISIBILITY",
                    "IsForm": false,
                    "PropertyAndValues": null
                }
            ],
            "DateConstraintDependencies": [
                {
                    "Lookups": [
                        {
                            "FormPart": "TreatmentDetails",
                            "Field": "TBTreatmentStartDate",
                            "ExpectedValue": null,
                            "ExpectedValues": null,
                            "IsObject": false,
                            "ObjectKeyAndValues": null,
                            "ObjectKeyAndValue": null,
                            "AttributeName": null,
                            "ConstraintName": "To",
                            "RestrictedValue": null,
                            "ComparisonOperator": "EQ",
                            "AnyObjectKeyValue": false,
                            "FilteredKeys": null,
                            "LookupFormPart": "TreatmentDetails",
                            "LookupField": "TBTreatmentStartDate"
                        }
                    ],
                    "FormPart": "TreatmentDetails",
                    "Field": "EnrollmentDate",
                    "FormPartId": null,
                    "FieldId": null,
                    "Type": "DATE_CONSTRAINT",
                    "IsForm": false,
                    "PropertyAndValues": null
                }
            ],
            "IsRequiredDependencies": [],
            "RemoteUpdateConfigs": [
                {
                    "FormPart": "TreatmentDetails",
                    "Field": "MermSerialNoObj",
                    "Url": "/api/Merm/GetAvailableMermImeisNew",
                    "ConditionalCall": true,
                    "ValueDependency": [
                        {
                            "CheckNotNull": false,
                            "ExpectedValue": "MERM",
                            "Field": "MonitoringMethod",
                            "FormPart": "TreatmentDetails",
                            "IsObject": false,
                            "ObjectProperty": null
                        },
                        {
                            "CheckNotNull": true,
                            "ExpectedValue": null,
                            "Field": "EnrollmentDate",
                            "FormPart": "TreatmentDetails",
                            "IsObject": false,
                            "ObjectProperty": null
                        },
                        {
                            "CheckNotNull": true,
                            "ExpectedValue": null,
                            "Field": "HierarchyMapping_Residence",
                            "FormPart": "Residence",
                            "IsObject": true,
                            "ObjectProperty": "Id"
                        }
                    ],
                    "UrlVariables": {
                        "EnrollmentDate": [
                            "TreatmentDetails",
                            "EnrollmentDate"
                        ],
                        "HierarchyId": [
                            "Residence",
                            "HierarchyMapping_Residence",
                            "Id"
                        ]
                    },
                    "RequestType": "GET",
                    "ResponsePath": [
                        "Data"
                    ]
                }
            ],
            "ValuePropertyDependencies": [],
            "CompoundValueDependencies": null,
            "SaveEndpoint": "/api/Patients/Add?isPatientDetailsRequired=false",
            "SaveText": "Add Patient",
            "SaveTextKey": "_add_patient"
        },
        "ExistingData": {}
    },
    "Error": null
}

export default testForm
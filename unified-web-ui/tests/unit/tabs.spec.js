import { mount } from '@vue/test-utils'
import Tabs from '@/app/shared/components/Tabs.vue'

describe('Tab.vue', () => {
  it('renders correct tab title and content when passed', () => {
    const $route = {
      path: '/some/path',
      name: 'name'
    }
    const tabs = [
      {
        tab_title: 'home',
        tab_content:
            'This is the home page. Your sample begins here...'
      }]
    const wrapper = mount(Tabs, {
      propsData: { tabs },
      mocks: {
        $t: (key) => key,
        $route
      }
    })
    expect(wrapper.find('.tab_title').text()).toMatch(tabs[0].tab_title)
  })

  test('renders correct tab title and content when passed', async () => {
    const $route = {
      path: '/12345/Sample',
      name: 'sample'
    }
    const tabs = [
      {
        tab_title: 'HOME',
        tab_content:
            'This is the home page. Your sample begins here...'
      },
      {
        tab_title: 'SAMPLE',
        tab_content:
            'This is the sample page. Your sample is here!'
      }]
    const wrapper = mount(Tabs, {
      propsData: { tabs },
      stubs: ['router-link', 'router-view'],
      mocks: {
        $t: (key) => key,
        $route
      }
    })
    const index = 1
    await wrapper.findAll('.tab_title').at(index).trigger('click.native')
    expect(wrapper.vm.active_tab).toBe(index) // checking updated
  })
})

import { mount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import Vuetify from 'vuetify'
import Modal from '@/app/shared/components/Modal.vue'
import Patient from '@/app/Pages/dashboard/patient/Patient.vue'
import Button from '@/app/shared/components/Button.vue'
import Select from '@/app/shared/components/Select.vue'
import Vue from 'vue'
const localVue = createLocalVue()
const $t = () => {}
const $route = {
  path: '/some/path',
  name: 'name',
  params: { patientId: '10740837' }
}
localVue.use(Vuex)
window.EventBus = new Vue()
Vue.use(Vuetify)
const testData = {
  Title: 'ff 234',
  Overview: [{ Key: 'Patient Id', Value: '10740837' }, { Key: 'Current Episode Id', Value: '10740837' }, { Key: 'Old Nikshay Id', Value: '-' }, { Key: 'Type of Patient', Value: 'Private' }, { Key: 'Sector', Value: 'Private' }, { Key: 'Duplication Status', Value: 'Unique System Identified' }, { Key: 'Status', Value: 'Outcome Assigned (Notified)' }, { Key: 'Adherence Technology', Value: 'Envelopes - 99DOTS' }, { Key: 'Episode No.', Value: '1' }, { Key: 'Next Refill Date', Value: 'N/A' }],
  Episodes: [10740837, 10741033],
  Patient: {
    Id: 10740837,
    FirstName: 'ff',
    LastName: '234',
    Address: 'eqwe',
    Gender: 'Female',
    Age: 23,
    ModuleAccess: { ENROLLMENT: {}, TEST_RESULTS: {}, TREATMENT_DETAILS: {}, ADHERENCE: {}, TAB_ADVERSE_REACTION: {}, DISPENSATION: {}, DBT: {}, CLOSE_CASE: {}, NOTES: {}, COMORBIDITY: {}, CONTACT_TRACING: {}, CENTER_MAPPING: {}, PRESCRIPTIONS: {}, ENGAGEMENT: {}, STAFF: {}, MERM: {}, VOT: {}, DELETE_PATIENT: {}, PATIENT_FOLLOW_UP: {} },
    Priority: 'HIGH',
    TypeOfCase: 'Retreatment: Recurrent',
    CurrentTags: '',
    DeploymentCode: 'IND',
    PrimaryPhone: '2434343434',
    PrimaryPhoneOwner: '',
    SecondaryPhones: [],
    CurrentHierarchyId: 368140,
    ResidenceHierarchyId: 313,
    CurrentHierarchy: { Id: 368140, Level: 5, Name: 'Shreya Hospital ', Type: 'HUB', Code: '044625', ParentId: 8904, DrugRegimen: 'FDC', HasMERM: true, CountryCode: 'IND', Level1Id: 1, Level1Name: 'India', Level1Type: 'COUNTRY', Level2Id: 19, Level2Name: 'Karnataka', Level2Type: 'STATE', Level3Id: 313, Level3Name: 'Belgaum', Level3Type: 'DISTRICT', Level4Id: 8904, Level4Name: 'ATHANI', Level4Type: 'TU', Level5Id: null, Level5Name: null, Level5Type: null, Level6Id: null, Level6Name: null, Level6Type: null, ExtraData: null, GeoLocation: null, ShowChildrenInPatientList: false, HasChildren: false },
    ResidenceHierarchy: {
      Id: 313,
      Level: 3,
      Name: 'Belgaum',
      Type: 'DISTRICT',
      Code: 'BEL',
      ParentId: 19,
      DrugRegimen: 'FDC',
      HasMERM: true,
      CountryCode: 'IND',
      Level1Id: 1,
      Level1Name: 'India',
      Level1Type: 'COUNTRY',
      Level2Id: 19,
      Level2Name: 'Karnataka',
      Level2Type: 'STATE',
      Level3Id: null,
      Level3Name: null,
      Level3Type: null,
      Level4Id: null,
      Level4Name: null,
      Level4Type: null,
      Level5Id: null,
      Level5Name: null,
      Level5Type: null,
      Level6Id: null,
      Level6Name: null,
      Level6Type: null,
      ExtraData: '{"DtoName":"test","Email":"a@a.com","PhoneOffice":"6767676767","MobileNo":"1344554676","DtcAddress":"test","DtcPincode":"123456","PfmsDistrictCode":0,"MDDSDistrictCode":null,"DeoMobile":null,"DeoName":null,"UpdatedBy":"dto-KABEL","UpdatedDate":"2021-11-26 17:43:9"}',
      GeoLocation: null,
      ShowChildrenInPatientList: true,
      HasChildren: true
    },
    ArtHierarchyId: null,
    ArtHierarchy: null,
    WeightBand: null,
    PreArtNumber: null,
    ArtNumber: null,
    NikshayId: null,
    TbNumber: null,
    TbCategory: null,
    TbTreatmentStartDate: '2022-02-01T00:00:00',
    EnrollmentDate: '2022-02-01T00:00:00',
    EndDate: '2022-02-16T00:00:00',
    TreatmentOutcome: 'TREATMENT_REGIMEN_CHANGED',
    OnTreatment: false,
    Stage: 'DIAGNOSED_OUTCOME_ASSIGNED',
    LastDosage: null,
    TypeOfPatient: 'IndiaTbPrivate',
    AdherenceString: '6666666666666666',
    CompleteAdherenceString: '6666666666666666',
    AdherenceStatus: '\u0000',
    OnMerm: false,
    CanHaveMerm: true,
    RegistrationDate: '2022-02-08T20:35:03.0333495',
    FathersName: null,
    Pincode: '423434',
    Taluka: null,
    Town: null,
    Ward: null,
    Landmark: null,
    DiagnosisDate: '2022-02-01T00:00:00',
    DiagnosisBasis: 'FLLPA',
    MonitoringMethod: '99DOTS',
    RefillMonitoring: null,
    NewOrPreviouslyTreated: null,
    HivStatus: 'Unknown',
    TreatmentType: null,
    DrugSusceptibility: null,
    TbType: 'Pulmonary',
    ShouldHaveNikshayId: true,
    EngagementTabEnabled: true,
    IvrEnabled: false,
    IsMarkAsDuplicateVisible: true,
    Area: 'Unknown',
    MaritalStatus: 'Unknown',
    SocioeconomicStatus: 'Unknown',
    Occupation: 'Unknown',
    KeyPopulation: 'Contact of Known TB Patients',
    ContactPersonAddress: null,
    ContactPersonName: null,
    ContactPersonPhone: null,
    TreatmentSupporterName: null,
    TreatmentSupporterPrimaryPhone: null,
    TreatmentSupporterSecondaryPhone: null,
    HierarchyMobileNumber: null,
    HierarychyName: null,
    HierarchyName: null,
    IsLTBICase: true,
    Imei: null,
    LastBattery: null,
    MermLastSeen: null,
    MermLastOpened: null,
    IsActive: null,
    DeactivatedOn: null,
    RTHours: null,
    MermStartDate: null,
    AdherenceDispensationRelation: true,
    TypeOfCaseFinding: 'Passive (Routine programme)',
    VideoCallAccess: true,
    AllowAlterAttentionAccess: true
  }
}

const getStore = (actions) => {
  const formattedActions = {
    ...actions
  }

  return new Vuex.Store({
    modules: {
      Patient: {
        actions: formattedActions,
        namespaced: true
      }
    }
  })
}

describe('Patient.vue', () => {
  it('renders patient page', async () => {
    const wrapper = mount(Patient, {
      store: getStore({
        getPatient: async () => (
          Promise.resolve(
            {
              Title: 'ff 234',
              Overview: [],
              Episodes: [10740837, 10741033],
              Patient: {
                Id: 10740837,
                FirstName: 'ff',
                LastName: '234',
                Address: 'eqwe',
                Gender: 'Female',
                Age: 23,
                Priority: 'HIGH',
                TypeOfCase: 'Retreatment: Recurrent',
                CurrentTags: '',
                DeploymentCode: 'IND',
                PrimaryPhone: '2434343434',
                PrimaryPhoneOwner: '',
                SecondaryPhones: [],
                CurrentHierarchyId: 368140,
                ResidenceHierarchyId: 313,
                CurrentHierarchy: { Id: 368140, Level: 5, Name: 'Shreya Hospital ', Type: 'HUB', Code: '044625', ParentId: 8904, DrugRegimen: 'FDC', HasMERM: true, CountryCode: 'IND', Level1Id: 1, Level1Name: 'India', Level1Type: 'COUNTRY', Level2Id: 19, Level2Name: 'Karnataka', Level2Type: 'STATE', Level3Id: 313, Level3Name: 'Belgaum', Level3Type: 'DISTRICT', Level4Id: 8904, Level4Name: 'ATHANI', Level4Type: 'TU', Level5Id: null, Level5Name: null, Level5Type: null, Level6Id: null, Level6Name: null, Level6Type: null, ExtraData: null, GeoLocation: null, ShowChildrenInPatientList: false, HasChildren: false },
                ResidenceHierarchy: {
                  Id: 313,
                  Level: 3,
                  Name: 'Belgaum',
                  Type: 'DISTRICT',
                  Code: 'BEL',
                  ParentId: 19,
                  DrugRegimen: 'FDC',
                  HasMERM: true,
                  CountryCode: 'IND',
                  Level1Id: 1,
                  Level1Name: 'India',
                  Level1Type: 'COUNTRY',
                  Level2Id: 19,
                  Level2Name: 'Karnataka',
                  Level2Type: 'STATE',
                  Level3Id: null,
                  Level3Name: null,
                  Level3Type: null,
                  Level4Id: null,
                  Level4Name: null,
                  Level4Type: null,
                  Level5Id: null,
                  Level5Name: null,
                  Level5Type: null,
                  Level6Id: null,
                  Level6Name: null,
                  Level6Type: null,
                  ExtraData: '{"DtoName":"test","Email":"a@a.com","PhoneOffice":"6767676767","MobileNo":"1344554676","DtcAddress":"test","DtcPincode":"123456","PfmsDistrictCode":0,"MDDSDistrictCode":null,"DeoMobile":null,"DeoName":null,"UpdatedBy":"dto-KABEL","UpdatedDate":"2021-11-26 17:43:9"}',
                  GeoLocation: null,
                  ShowChildrenInPatientList: true,
                  HasChildren: true
                },
                ArtHierarchyId: null,
                ArtHierarchy: null,
                WeightBand: null,
                PreArtNumber: null,
                ArtNumber: null,
                NikshayId: null,
                TbNumber: null,
                TbCategory: null,
                TbTreatmentStartDate: '2022-02-01T00:00:00',
                EnrollmentDate: '2022-02-01T00:00:00',
                EndDate: '2022-02-16T00:00:00',
                TreatmentOutcome: 'TREATMENT_REGIMEN_CHANGED',
                OnTreatment: false,
                Stage: 'PRESUMPTIVE_OPEN',
                LastDosage: null,
                TypeOfPatient: 'IndiaTbPrivate',
                AdherenceString: '6666666666666666',
                CompleteAdherenceString: '6666666666666666'
              }
            }
          )
        ),
        setPatientId: async () => {}
      }),
      localVue,
      mocks: { $t, $route }
    })
    expect(wrapper.find('.fa-md').exists()).toBe(true)
  })

  it('renders patient data', async () => {
    const wrapper = mount(Patient, {
      store: getStore({
        getPatient: async () => (
          Promise.resolve(
            {
              Title: 'ff 234',
              Overview: [{ Key: 'Patient Id', Value: '10740837' }, { Key: 'Current Episode Id', Value: '10740837' }, { Key: 'Old Nikshay Id', Value: '-' }, { Key: 'Type of Patient', Value: 'Private' }, { Key: 'Sector', Value: 'Private' }, { Key: 'Duplication Status', Value: 'Unique System Identified' }, { Key: 'Status', Value: 'Outcome Assigned (Notified)' }, { Key: 'Adherence Technology', Value: 'Envelopes - 99DOTS' }, { Key: 'Episode No.', Value: '1' }, { Key: 'Next Refill Date', Value: 'N/A' }],
              Episodes: [10740837, 10741033],
              Patient: {
                Id: 10740837,
                FirstName: 'ff',
                LastName: '234',
                Address: 'eqwe',
                Gender: 'Female',
                Age: 23,
                ModuleAccess: { ENROLLMENT: {}, TEST_RESULTS: {}, TREATMENT_DETAILS: {}, ADHERENCE: {}, TAB_ADVERSE_REACTION: {}, DISPENSATION: {}, DBT: {}, CLOSE_CASE: {}, NOTES: {}, COMORBIDITY: {}, CONTACT_TRACING: {}, CENTER_MAPPING: {}, PRESCRIPTIONS: {}, ENGAGEMENT: {}, STAFF: {}, MERM: {}, VOT: {}, DELETE_PATIENT: {}, PATIENT_FOLLOW_UP: {} },
                Priority: 'HIGH',
                TypeOfCase: 'Retreatment: Recurrent',
                CurrentTags: '',
                DeploymentCode: 'IND',
                PrimaryPhone: '2434343434',
                PrimaryPhoneOwner: '',
                SecondaryPhones: [],
                CurrentHierarchyId: 368140,
                ResidenceHierarchyId: 313,
                CurrentHierarchy: { Id: 368140, Level: 5, Name: 'Shreya Hospital ', Type: 'HUB', Code: '044625', ParentId: 8904, DrugRegimen: 'FDC', HasMERM: true, CountryCode: 'IND', Level1Id: 1, Level1Name: 'India', Level1Type: 'COUNTRY', Level2Id: 19, Level2Name: 'Karnataka', Level2Type: 'STATE', Level3Id: 313, Level3Name: 'Belgaum', Level3Type: 'DISTRICT', Level4Id: 8904, Level4Name: 'ATHANI', Level4Type: 'TU', Level5Id: null, Level5Name: null, Level5Type: null, Level6Id: null, Level6Name: null, Level6Type: null, ExtraData: null, GeoLocation: null, ShowChildrenInPatientList: false, HasChildren: false },
                ResidenceHierarchy: {
                  Id: 313,
                  Level: 3,
                  Name: 'Belgaum',
                  Type: 'DISTRICT',
                  Code: 'BEL',
                  ParentId: 19,
                  DrugRegimen: 'FDC',
                  HasMERM: true,
                  CountryCode: 'IND',
                  Level1Id: 1,
                  Level1Name: 'India',
                  Level1Type: 'COUNTRY',
                  Level2Id: 19,
                  Level2Name: 'Karnataka',
                  Level2Type: 'STATE',
                  Level3Id: null,
                  Level3Name: null,
                  Level3Type: null,
                  Level4Id: null,
                  Level4Name: null,
                  Level4Type: null,
                  Level5Id: null,
                  Level5Name: null,
                  Level5Type: null,
                  Level6Id: null,
                  Level6Name: null,
                  Level6Type: null,
                  ExtraData: '{"DtoName":"test","Email":"a@a.com","PhoneOffice":"6767676767","MobileNo":"1344554676","DtcAddress":"test","DtcPincode":"123456","PfmsDistrictCode":0,"MDDSDistrictCode":null,"DeoMobile":null,"DeoName":null,"UpdatedBy":"dto-KABEL","UpdatedDate":"2021-11-26 17:43:9"}',
                  GeoLocation: null,
                  ShowChildrenInPatientList: true,
                  HasChildren: true
                },
                ArtHierarchyId: null,
                ArtHierarchy: null,
                WeightBand: null,
                PreArtNumber: null,
                ArtNumber: null,
                NikshayId: null,
                TbNumber: null,
                TbCategory: null,
                TbTreatmentStartDate: '2022-02-01T00:00:00',
                EnrollmentDate: '2022-02-01T00:00:00',
                EndDate: '2022-02-16T00:00:00',
                TreatmentOutcome: 'TREATMENT_REGIMEN_CHANGED',
                OnTreatment: false,
                Stage: 'DIAGNOSED_OUTCOME_ASSIGNED',
                LastDosage: null,
                TypeOfPatient: 'IndiaTbPrivate',
                AdherenceString: '6666666666666666',
                CompleteAdherenceString: '6666666666666666',
                AdherenceStatus: '\u0000',
                OnMerm: false,
                CanHaveMerm: true,
                RegistrationDate: '2022-02-08T20:35:03.0333495',
                FathersName: null,
                Pincode: '423434',
                Taluka: null,
                Town: null,
                Ward: null,
                Landmark: null,
                DiagnosisDate: '2022-02-01T00:00:00',
                DiagnosisBasis: 'FLLPA',
                MonitoringMethod: '99DOTS',
                RefillMonitoring: null,
                NewOrPreviouslyTreated: null,
                HivStatus: 'Unknown',
                TreatmentType: null,
                DrugSusceptibility: null,
                TbType: 'Pulmonary',
                ShouldHaveNikshayId: true,
                EngagementTabEnabled: true,
                IvrEnabled: false,
                IsMarkAsDuplicateVisible: true,
                Area: 'Unknown',
                MaritalStatus: 'Unknown',
                SocioeconomicStatus: 'Unknown',
                Occupation: 'Unknown',
                KeyPopulation: 'Contact of Known TB Patients',
                ContactPersonAddress: null,
                ContactPersonName: null,
                ContactPersonPhone: null,
                TreatmentSupporterName: null,
                TreatmentSupporterPrimaryPhone: null,
                TreatmentSupporterSecondaryPhone: null,
                HierarchyMobileNumber: null,
                HierarychyName: null,
                HierarchyName: null,
                IsLTBICase: true,
                Imei: null,
                LastBattery: null,
                MermLastSeen: null,
                MermLastOpened: null,
                IsActive: null,
                DeactivatedOn: null,
                RTHours: null,
                MermStartDate: null,
                AdherenceDispensationRelation: true,
                TypeOfCaseFinding: 'Passive (Routine programme)',
                VideoCallAccess: true,
                AllowAlterAttentionAccess: true
              }
            }
          )
        ),
        setPatientId: async () => {}
      }),
      localVue,
      mocks: {
        $router: {
          push: path => path
        },
        $t,
        $route
      }
    })
    await wrapper.setData({ title: testData.Title.toString(), countryCode: 'IND', isPatientDataLoaded: true, patientEpisodes: [{ Key: '12345', Value: '12345' }, { Key: '23456', Value: '23456' }], overview: testData.Overview })
    await wrapper.vm.$nextTick()
    expect(wrapper.find('.episode-selector').exists()).toBe(true)
    expect(wrapper.find('.dropdown-menu').exists()).toBe(true)
    expect(wrapper.find('.v-list-item').exists()).toBe(true)
  })

  it('renders patient data', async () => {
    const wrapper = mount(Patient, {
      store: getStore({
        getPatient: async () => (
          Promise.resolve(
            {
              Title: 'ff 234',
              Overview: [{ Key: 'Patient Id', Value: '10740837' }, { Key: 'Current Episode Id', Value: '10740837' }, { Key: 'Old Nikshay Id', Value: '-' }, { Key: 'Type of Patient', Value: 'Private' }, { Key: 'Sector', Value: 'Private' }, { Key: 'Duplication Status', Value: 'Unique System Identified' }, { Key: 'Status', Value: 'Outcome Assigned (Notified)' }, { Key: 'Adherence Technology', Value: 'Envelopes - 99DOTS' }, { Key: 'Episode No.', Value: '1' }, { Key: 'Next Refill Date', Value: 'N/A' }],
              Episodes: [10740837, 10741033],
              Patient: {
                Id: 10740837,
                FirstName: 'ff',
                LastName: '234',
                Address: 'eqwe',
                Gender: 'Female',
                Age: 23,
                ModuleAccess: { ENROLLMENT: {}, TEST_RESULTS: {}, TREATMENT_DETAILS: {}, ADHERENCE: {}, TAB_ADVERSE_REACTION: {}, DISPENSATION: {}, DBT: {}, CLOSE_CASE: {}, NOTES: {}, COMORBIDITY: {}, CONTACT_TRACING: {}, CENTER_MAPPING: {}, PRESCRIPTIONS: {}, ENGAGEMENT: {}, STAFF: {}, MERM: {}, VOT: {}, DELETE_PATIENT: {}, PATIENT_FOLLOW_UP: {} },
                Priority: 'HIGH',
                TypeOfCase: 'Retreatment: Recurrent',
                CurrentTags: '',
                DeploymentCode: 'IND',
                PrimaryPhone: '2434343434',
                PrimaryPhoneOwner: '',
                SecondaryPhones: [],
                CurrentHierarchyId: 368140,
                ResidenceHierarchyId: 313,
                CurrentHierarchy: { Id: 368140, Level: 5, Name: 'Shreya Hospital ', Type: 'HUB', Code: '044625', ParentId: 8904, DrugRegimen: 'FDC', HasMERM: true, CountryCode: 'IND', Level1Id: 1, Level1Name: 'India', Level1Type: 'COUNTRY', Level2Id: 19, Level2Name: 'Karnataka', Level2Type: 'STATE', Level3Id: 313, Level3Name: 'Belgaum', Level3Type: 'DISTRICT', Level4Id: 8904, Level4Name: 'ATHANI', Level4Type: 'TU', Level5Id: null, Level5Name: null, Level5Type: null, Level6Id: null, Level6Name: null, Level6Type: null, ExtraData: null, GeoLocation: null, ShowChildrenInPatientList: false, HasChildren: false },
                ResidenceHierarchy: {
                  Id: 313,
                  Level: 3,
                  Name: 'Belgaum',
                  Type: 'DISTRICT',
                  Code: 'BEL',
                  ParentId: 19,
                  DrugRegimen: 'FDC',
                  HasMERM: true,
                  CountryCode: 'IND',
                  Level1Id: 1,
                  Level1Name: 'India',
                  Level1Type: 'COUNTRY',
                  Level2Id: 19,
                  Level2Name: 'Karnataka',
                  Level2Type: 'STATE',
                  Level3Id: null,
                  Level3Name: null,
                  Level3Type: null,
                  Level4Id: null,
                  Level4Name: null,
                  Level4Type: null,
                  Level5Id: null,
                  Level5Name: null,
                  Level5Type: null,
                  Level6Id: null,
                  Level6Name: null,
                  Level6Type: null,
                  ExtraData: '{"DtoName":"test","Email":"a@a.com","PhoneOffice":"6767676767","MobileNo":"1344554676","DtcAddress":"test","DtcPincode":"123456","PfmsDistrictCode":0,"MDDSDistrictCode":null,"DeoMobile":null,"DeoName":null,"UpdatedBy":"dto-KABEL","UpdatedDate":"2021-11-26 17:43:9"}',
                  GeoLocation: null,
                  ShowChildrenInPatientList: true,
                  HasChildren: true
                },
                ArtHierarchyId: null,
                ArtHierarchy: null,
                WeightBand: null,
                PreArtNumber: null,
                ArtNumber: null,
                NikshayId: null,
                TbNumber: null,
                TbCategory: null,
                TbTreatmentStartDate: '2022-02-01T00:00:00',
                EnrollmentDate: '2022-02-01T00:00:00',
                EndDate: '2022-02-16T00:00:00',
                TreatmentOutcome: 'TREATMENT_REGIMEN_CHANGED',
                OnTreatment: false,
                Stage: 'DIAGNOSED_OUTCOME_ASSIGNED',
                LastDosage: null,
                TypeOfPatient: 'IndiaTbPrivate',
                AdherenceString: '6666666666666666',
                CompleteAdherenceString: '6666666666666666',
                AdherenceStatus: '\u0000',
                OnMerm: false,
                CanHaveMerm: true,
                RegistrationDate: '2022-02-08T20:35:03.0333495',
                FathersName: null,
                Pincode: '423434',
                Taluka: null,
                Town: null,
                Ward: null,
                Landmark: null,
                DiagnosisDate: '2022-02-01T00:00:00',
                DiagnosisBasis: 'FLLPA',
                MonitoringMethod: '99DOTS',
                RefillMonitoring: null,
                NewOrPreviouslyTreated: null,
                HivStatus: 'Unknown',
                TreatmentType: null,
                DrugSusceptibility: null,
                TbType: 'Pulmonary',
                ShouldHaveNikshayId: true,
                EngagementTabEnabled: true,
                IvrEnabled: false,
                IsMarkAsDuplicateVisible: true,
                Area: 'Unknown',
                MaritalStatus: 'Unknown',
                SocioeconomicStatus: 'Unknown',
                Occupation: 'Unknown',
                KeyPopulation: 'Contact of Known TB Patients',
                ContactPersonAddress: null,
                ContactPersonName: null,
                ContactPersonPhone: null,
                TreatmentSupporterName: null,
                TreatmentSupporterPrimaryPhone: null,
                TreatmentSupporterSecondaryPhone: null,
                HierarchyMobileNumber: null,
                HierarychyName: null,
                HierarchyName: null,
                IsLTBICase: true,
                Imei: null,
                LastBattery: null,
                MermLastSeen: null,
                MermLastOpened: null,
                IsActive: null,
                DeactivatedOn: null,
                RTHours: null,
                MermStartDate: null,
                AdherenceDispensationRelation: true,
                TypeOfCaseFinding: 'Passive (Routine programme)',
                VideoCallAccess: true,
                AllowAlterAttentionAccess: true
              }
            }
          )
        ),
        setPatientId: async () => {}
      }),
      localVue,
      mocks: {
        $router: {
          push: path => path
        },
        $t,
        $route
      }
    })
    await wrapper.setData({ title: testData.Title.toString(), countryCode: 'IND', isPatientDataLoaded: true, patientEpisodes: [{ Key: '23456', Value: '23456' }], overview: testData.Overview })
    await wrapper.vm.$nextTick()
    expect(wrapper.find('.episode-selector').exists()).toBe(false)
  })

  it('renders video call buttons', async () => {
    const wrapper = mount(Patient, {
      store: getStore({
        getPatient: async () => (
          Promise.resolve(
            {
              Title: 'ff 234',
              Overview: [{ Key: 'Patient Id', Value: '10740837' }, { Key: 'Current Episode Id', Value: '10740837' }, { Key: 'Old Nikshay Id', Value: '-' }, { Key: 'Type of Patient', Value: 'Private' }, { Key: 'Sector', Value: 'Private' }, { Key: 'Duplication Status', Value: 'Unique System Identified' }, { Key: 'Status', Value: 'Outcome Assigned (Notified)' }, { Key: 'Adherence Technology', Value: 'Envelopes - 99DOTS' }, { Key: 'Episode No.', Value: '1' }, { Key: 'Next Refill Date', Value: 'N/A' }],
              Episodes: [10740837, 10741033],
              Patient: {
                Id: 10740837,
                FirstName: 'ff',
                LastName: '234',
                Address: 'eqwe',
                Gender: 'Female',
                Age: 23,
                ModuleAccess: { ENROLLMENT: {}, TEST_RESULTS: {}, TREATMENT_DETAILS: {}, ADHERENCE: {}, TAB_ADVERSE_REACTION: {}, DISPENSATION: {}, DBT: {}, CLOSE_CASE: {}, NOTES: {}, COMORBIDITY: {}, CONTACT_TRACING: {}, CENTER_MAPPING: {}, PRESCRIPTIONS: {}, ENGAGEMENT: {}, STAFF: {}, MERM: {}, VOT: {}, DELETE_PATIENT: {}, PATIENT_FOLLOW_UP: {} },
                Priority: 'HIGH',
                TypeOfCase: 'Retreatment: Recurrent',
                CurrentTags: '',
                DeploymentCode: 'IND',
                PrimaryPhone: '2434343434',
                PrimaryPhoneOwner: '',
                SecondaryPhones: [],
                CurrentHierarchyId: 368140,
                ResidenceHierarchyId: 313,
                CurrentHierarchy: { Id: 368140, Level: 5, Name: 'Shreya Hospital ', Type: 'HUB', Code: '044625', ParentId: 8904, DrugRegimen: 'FDC', HasMERM: true, CountryCode: 'IND', Level1Id: 1, Level1Name: 'India', Level1Type: 'COUNTRY', Level2Id: 19, Level2Name: 'Karnataka', Level2Type: 'STATE', Level3Id: 313, Level3Name: 'Belgaum', Level3Type: 'DISTRICT', Level4Id: 8904, Level4Name: 'ATHANI', Level4Type: 'TU', Level5Id: null, Level5Name: null, Level5Type: null, Level6Id: null, Level6Name: null, Level6Type: null, ExtraData: null, GeoLocation: null, ShowChildrenInPatientList: false, HasChildren: false },
                ResidenceHierarchy: {
                  Id: 313,
                  Level: 3,
                  Name: 'Belgaum',
                  Type: 'DISTRICT',
                  Code: 'BEL',
                  ParentId: 19,
                  DrugRegimen: 'FDC',
                  HasMERM: true,
                  CountryCode: 'IND',
                  Level1Id: 1,
                  Level1Name: 'India',
                  Level1Type: 'COUNTRY',
                  Level2Id: 19,
                  Level2Name: 'Karnataka',
                  Level2Type: 'STATE',
                  Level3Id: null,
                  Level3Name: null,
                  Level3Type: null,
                  Level4Id: null,
                  Level4Name: null,
                  Level4Type: null,
                  Level5Id: null,
                  Level5Name: null,
                  Level5Type: null,
                  Level6Id: null,
                  Level6Name: null,
                  Level6Type: null,
                  ExtraData: '{"DtoName":"test","Email":"a@a.com","PhoneOffice":"6767676767","MobileNo":"1344554676","DtcAddress":"test","DtcPincode":"123456","PfmsDistrictCode":0,"MDDSDistrictCode":null,"DeoMobile":null,"DeoName":null,"UpdatedBy":"dto-KABEL","UpdatedDate":"2021-11-26 17:43:9"}',
                  GeoLocation: null,
                  ShowChildrenInPatientList: true,
                  HasChildren: true
                },
                ArtHierarchyId: null,
                ArtHierarchy: null,
                WeightBand: null,
                PreArtNumber: null,
                ArtNumber: null,
                NikshayId: null,
                TbNumber: null,
                TbCategory: null,
                TbTreatmentStartDate: '2022-02-01T00:00:00',
                EnrollmentDate: '2022-02-01T00:00:00',
                EndDate: '2022-02-16T00:00:00',
                TreatmentOutcome: 'TREATMENT_REGIMEN_CHANGED',
                OnTreatment: false,
                Stage: 'DIAGNOSED_OUTCOME_ASSIGNED',
                LastDosage: null,
                TypeOfPatient: 'IndiaTbPrivate',
                AdherenceString: '6666666666666666',
                CompleteAdherenceString: '6666666666666666',
                AdherenceStatus: '\u0000',
                OnMerm: false,
                CanHaveMerm: true,
                RegistrationDate: '2022-02-08T20:35:03.0333495',
                FathersName: null,
                Pincode: '423434',
                Taluka: null,
                Town: null,
                Ward: null,
                Landmark: null,
                DiagnosisDate: '2022-02-01T00:00:00',
                DiagnosisBasis: 'FLLPA',
                MonitoringMethod: '99DOTS',
                RefillMonitoring: null,
                NewOrPreviouslyTreated: null,
                HivStatus: 'Unknown',
                TreatmentType: null,
                DrugSusceptibility: null,
                TbType: 'Pulmonary',
                ShouldHaveNikshayId: true,
                EngagementTabEnabled: true,
                IvrEnabled: false,
                IsMarkAsDuplicateVisible: true,
                Area: 'Unknown',
                MaritalStatus: 'Unknown',
                SocioeconomicStatus: 'Unknown',
                Occupation: 'Unknown',
                KeyPopulation: 'Contact of Known TB Patients',
                ContactPersonAddress: null,
                ContactPersonName: null,
                ContactPersonPhone: null,
                TreatmentSupporterName: null,
                TreatmentSupporterPrimaryPhone: null,
                TreatmentSupporterSecondaryPhone: null,
                HierarchyMobileNumber: null,
                HierarychyName: null,
                HierarchyName: null,
                IsLTBICase: true,
                Imei: null,
                LastBattery: null,
                MermLastSeen: null,
                MermLastOpened: null,
                IsActive: null,
                DeactivatedOn: null,
                RTHours: null,
                MermStartDate: null,
                AdherenceDispensationRelation: true,
                TypeOfCaseFinding: 'Passive (Routine programme)',
                VideoCallAccess: true,
                AllowAlterAttentionAccess: true
              }
            }
          )
        ),
        setPatientId: async () => {},
        videoCall: async () => {}
      }),
      localVue,
      mocks: {
        $router: {
          push: path => path
        },
        $t,
        $route
      }
    })
    await wrapper.setData({ title: testData.Title.toString(), overview: testData.Overview, patientObj: testData, isPatientDataLoaded: true })
    await wrapper.findAllComponents(Button).at(0).trigger('click')
    expect(wrapper.findComponent(Button).exists()).toBe(true)
  })

  it('renders select', async () => {
    const wrapper = mount(Patient, {
      store: getStore({
        getPatient: async () => (
          Promise.resolve(
            {
              Title: 'ff 234',
              Overview: [],
              Episodes: [10740837, 10741033],
              Patient: {
                Id: 10740837,
                FirstName: 'ff',
                LastName: '234',
                Address: 'eqwe',
                Gender: 'Female',
                Age: 23,
                ModuleAccess: { ENROLLMENT: {}, TEST_RESULTS: {}, TREATMENT_DETAILS: {}, ADHERENCE: {}, TAB_ADVERSE_REACTION: {}, DISPENSATION: {}, DBT: {}, CLOSE_CASE: {}, NOTES: {}, COMORBIDITY: {}, CONTACT_TRACING: {}, CENTER_MAPPING: {}, PRESCRIPTIONS: {}, ENGAGEMENT: {}, STAFF: {}, MERM: {}, VOT: {}, DELETE_PATIENT: {}, PATIENT_FOLLOW_UP: {} },
                Priority: 'HIGH',
                TypeOfCase: 'Retreatment: Recurrent',
                CurrentTags: '',
                DeploymentCode: 'IND',
                PrimaryPhone: '2434343434',
                PrimaryPhoneOwner: '',
                SecondaryPhones: [],
                CurrentHierarchyId: 368140,
                ResidenceHierarchyId: 313,
                CurrentHierarchy: { Id: 368140, Level: 5, Name: 'Shreya Hospital ', Type: 'HUB', Code: '044625', ParentId: 8904, DrugRegimen: 'FDC', HasMERM: true, CountryCode: 'IND', Level1Id: 1, Level1Name: 'India', Level1Type: 'COUNTRY', Level2Id: 19, Level2Name: 'Karnataka', Level2Type: 'STATE', Level3Id: 313, Level3Name: 'Belgaum', Level3Type: 'DISTRICT', Level4Id: 8904, Level4Name: 'ATHANI', Level4Type: 'TU', Level5Id: null, Level5Name: null, Level5Type: null, Level6Id: null, Level6Name: null, Level6Type: null, ExtraData: null, GeoLocation: null, ShowChildrenInPatientList: false, HasChildren: false },
                ResidenceHierarchy: {
                  Id: 313,
                  Level: 3,
                  Name: 'Belgaum',
                  Type: 'DISTRICT',
                  Code: 'BEL',
                  ParentId: 19,
                  DrugRegimen: 'FDC',
                  HasMERM: true,
                  CountryCode: 'IND',
                  Level1Id: 1,
                  Level1Name: 'India',
                  Level1Type: 'COUNTRY',
                  Level2Id: 19,
                  Level2Name: 'Karnataka',
                  Level2Type: 'STATE',
                  Level3Id: null,
                  Level3Name: null,
                  Level3Type: null,
                  Level4Id: null,
                  Level4Name: null,
                  Level4Type: null,
                  Level5Id: null,
                  Level5Name: null,
                  Level5Type: null,
                  Level6Id: null,
                  Level6Name: null,
                  Level6Type: null,
                  ExtraData: '{"DtoName":"test","Email":"a@a.com","PhoneOffice":"6767676767","MobileNo":"1344554676","DtcAddress":"test","DtcPincode":"123456","PfmsDistrictCode":0,"MDDSDistrictCode":null,"DeoMobile":null,"DeoName":null,"UpdatedBy":"dto-KABEL","UpdatedDate":"2021-11-26 17:43:9"}',
                  GeoLocation: null,
                  ShowChildrenInPatientList: true,
                  HasChildren: true
                },
                ArtHierarchyId: null,
                ArtHierarchy: null,
                WeightBand: null,
                PreArtNumber: null,
                ArtNumber: null,
                NikshayId: null,
                TbNumber: null,
                TbCategory: null,
                TbTreatmentStartDate: '2022-02-01T00:00:00',
                EnrollmentDate: '2022-02-01T00:00:00',
                EndDate: '2022-02-16T00:00:00',
                TreatmentOutcome: 'TREATMENT_REGIMEN_CHANGED',
                OnTreatment: false,
                Stage: 'PRESUMPTIVE_OPEN',
                LastDosage: null,
                TypeOfPatient: 'IndiaTbPrivate',
                AdherenceString: '6666666666666666',
                CompleteAdherenceString: '6666666666666666'
              }
            }
          )
        ),
        setPatientId: async () => {}
      }),
      localVue,
      mocks: {
        $router: {
          push: path => path
        },
        $t,
        $route
      }
    })
    await wrapper.setData({ title: testData.Title.toString(), countryCode: 'IND', isPatientDataLoaded: true, patientEpisodes: [{ Key: '12345', Value: '12345' }, { Key: '23456', Value: '23456' }], overview: testData.Overview })
    expect(wrapper.findComponent(Select).exists()).toBe(true)
    await wrapper.findComponent(Select).findAll('.dropdown ul li').at(0).trigger('click')
  })

  it('renders attention modal', async () => {
    const wrapper = mount(Patient, {
      store: getStore({
        getPatient: async () => (
          Promise.resolve(
            {
              Title: 'ff 234',
              Overview: [],
              Episodes: [10740837, 10741033],
              Patient: {
                Id: 10740837,
                FirstName: 'ff',
                LastName: '234',
                Address: 'eqwe',
                Gender: 'Female',
                Age: 23,
                Priority: 'HIGH',
                TypeOfCase: 'Retreatment: Recurrent',
                CurrentTags: '',
                DeploymentCode: 'IND',
                PrimaryPhone: '2434343434',
                PrimaryPhoneOwner: '',
                SecondaryPhones: [],
                CurrentHierarchyId: 368140,
                ResidenceHierarchyId: 313,
                CurrentHierarchy: { Id: 368140, Level: 5, Name: 'Shreya Hospital ', Type: 'HUB', Code: '044625', ParentId: 8904, DrugRegimen: 'FDC', HasMERM: true, CountryCode: 'IND', Level1Id: 1, Level1Name: 'India', Level1Type: 'COUNTRY', Level2Id: 19, Level2Name: 'Karnataka', Level2Type: 'STATE', Level3Id: 313, Level3Name: 'Belgaum', Level3Type: 'DISTRICT', Level4Id: 8904, Level4Name: 'ATHANI', Level4Type: 'TU', Level5Id: null, Level5Name: null, Level5Type: null, Level6Id: null, Level6Name: null, Level6Type: null, ExtraData: null, GeoLocation: null, ShowChildrenInPatientList: false, HasChildren: false },
                ResidenceHierarchy: {
                  Id: 313,
                  Level: 3,
                  Name: 'Belgaum',
                  Type: 'DISTRICT',
                  Code: 'BEL',
                  ParentId: 19,
                  DrugRegimen: 'FDC',
                  HasMERM: true,
                  CountryCode: 'IND',
                  Level1Id: 1,
                  Level1Name: 'India',
                  Level1Type: 'COUNTRY',
                  Level2Id: 19,
                  Level2Name: 'Karnataka',
                  Level2Type: 'STATE',
                  Level3Id: null,
                  Level3Name: null,
                  Level3Type: null,
                  Level4Id: null,
                  Level4Name: null,
                  Level4Type: null,
                  Level5Id: null,
                  Level5Name: null,
                  Level5Type: null,
                  Level6Id: null,
                  Level6Name: null,
                  Level6Type: null,
                  ExtraData: '{"DtoName":"test","Email":"a@a.com","PhoneOffice":"6767676767","MobileNo":"1344554676","DtcAddress":"test","DtcPincode":"123456","PfmsDistrictCode":0,"MDDSDistrictCode":null,"DeoMobile":null,"DeoName":null,"UpdatedBy":"dto-KABEL","UpdatedDate":"2021-11-26 17:43:9"}',
                  GeoLocation: null,
                  ShowChildrenInPatientList: true,
                  HasChildren: true
                },
                ArtHierarchyId: null,
                ArtHierarchy: null,
                WeightBand: null,
                PreArtNumber: null,
                ArtNumber: null,
                NikshayId: null,
                TbNumber: null,
                TbCategory: null,
                TbTreatmentStartDate: '2022-02-01T00:00:00',
                EnrollmentDate: '2022-02-01T00:00:00',
                EndDate: '2022-02-16T00:00:00',
                TreatmentOutcome: 'TREATMENT_REGIMEN_CHANGED',
                OnTreatment: false,
                Stage: 'PRESUMPTIVE_OPEN',
                LastDosage: null,
                TypeOfPatient: 'IndiaTbPrivate',
                AdherenceString: '6666666666666666',
                CompleteAdherenceString: '6666666666666666',
                AllowAlterAttentionAccess: true
              }
            }
          )
        ),
        setPatientId: async () => {},
        alterAttentionUpdate: async () => {}
      }),
      localVue,
      mocks: {
        $router: {
          push: path => path
        },
        $t,
        $route
      }
    })
    await wrapper.setData({
      title: testData.Title.toString(),
      countryCode: 'IND',
      isPatientDataLoaded: true,
      isModalVisible: true,
      attentionModalValue: [
        { Value: 'HIGH', Label: '_high' },
        { Value: 'MEDIUM', Label: '_medium' },
        { Value: 'LOW', Label: '_low' }
      ]
    })
    wrapper.findComponent(Modal).vm.$emit('click')
    await wrapper.setData({ isModalVisible: false })
    expect(wrapper.find('.modal-backdrop').exists()).toBe(false)
    expect(wrapper.find('.btn-close').exists()).toBe(false)
    expect(wrapper.find('.btn-primary').exists()).toBe(false)
    await wrapper.setData({ isModalVisible: false })
    await wrapper.find('#alterAttention').trigger('click')
    expect(wrapper.findComponent(Modal).exists()).toBe(true)
    wrapper.findComponent(Modal).vm.$emit('close')
  })
})

import {
  mount
} from '@vue/test-utils'
import ChipsCancel from '@/app/shared/components/svg-icons/chipsCancel.vue'

const testData = {
  color: 'primary'
}

describe('Chips unit test', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(ChipsCancel)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  test('Rendering props', () => {
    const wrapper = mount(ChipsCancel, {
      propsData: {
        ...testData
      },
      mocks: {
        $t: (key) => key
      }
    })
    expect(wrapper.props().color).toBe('primary')
  })

  test('created assings color', () => {
    var wrapper = mount(ChipsCancel, {
      propsData: {
        ...testData
      },
      mocks: {
        $t: (key) => key
      }
    })
    expect(wrapper.vm.color).toBe('primary')
    testData.color = 'grey'
    wrapper = mount(ChipsCancel, {
      propsData: {
        ...testData,
        mocks: {
          $t: (key) => key
        }
      }
    })
    expect(wrapper.vm.color).toBe('grey')
    testData.color = 'black'
    wrapper = mount(ChipsCancel, {
      propsData: {
        ...testData
      },
      mocks: {
        $t: (key) => key
      }
    })
    expect(wrapper.vm.color).toBe('black')
  })
})

import { mount, createLocalVue } from '@vue/test-utils'
import { Map, List } from 'immutable'
import Form from '@/app/shared/components/Form.vue'
import Button from '@/app/shared/components/Button.vue'
import flushPromises from 'flush-promises'
import TreatmentCenterTab from '../../src/app/Pages/dashboard/patient/components/TreatmentCenterTab'
import Vuex from 'vuex'
import Vue from 'vue'

const localVue = createLocalVue()

localVue.use(Vuex)
window.EventBus = new Vue()

jest.mock('../../src/utils/toastUtils', () => {
  return {
    __esModule: true,
    defaultToast: jest.fn(),
    ToastType: {
      Success: 'Success',
      Error: 'Error',
      Warning: 'Warning',
      Neutal: 'Neutal'
    }
  }
})

describe('TreatmentCenterTab.vue', () => {
  let store
  let actions
  beforeEach(() => {
    actions = {
      getFormData: jest.fn(),
      setIsEditing: jest.fn(),
      loadForm: jest.fn(),
      startSaving: jest.fn(),
      save: async () => (
        Promise.resolve({
          Success: true,
          Error: null,
          Data: 'Success'
        })
      ),
      endSaving: jest.fn()
    }
    store = new Vuex.Store({
      modules: {
        Form: {
          namespaced: true,
          state: {
            isSaving: false,
            isLoading: false,
            isEditing: true,
            hasFormError: false,
            adherenceTechnology: { MERM: false, VOT: false },
            existingData: null,
            existingVModel: {},
            form: {},
            title: '',
            remoteUpdateConfigsMappedByField: Map(),
            remoteUpdateConfigs: [],
            hierarchyConfigs: Map(),
            valueDependencies: List(),
            formPartsMappedByName: Map(),
            fieldsMappedByFormPartName: Map(),
            fieldsMappedByNameOriginal: Map(),
            fieldsMappedByName: null,
            allFields: null,
            visibilityDependencies: List(),
            isRequiredDependencies: List(),
            filterDependencies: List(),
            visibilityDependenciesMappedByFormPartName: null,
            dateConstraintDependency: List(),
            formFieldsByFormPartName: {},
            saveEndpoint: '',
            saveText: '',
            saveUrlParams: '',
            getFormUrlParams: '',
            replaceExistingDataFromServer: true,
            recaptchaVerified: false,
            otp: '',
            editAccess: false,
            valuePropertyDependencies: List()
          },
          actions
        }
      }
    })
  })

  it('render editdetails and form component', async () => {
    store.state.Form.isLoading = true
    const wrapper = mount(TreatmentCenterTab, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })

    await flushPromises()
    await wrapper.setData({ formActive: false })
    expect(wrapper.find('#treatmentCenterTab').exists()).toBe(true)
    await wrapper.findAllComponents(Button).at(0).trigger('click')
    expect(wrapper.find('.submit').exists()).toBe(false)
  })

  it('Loading to be false', async () => {
    store.state.Form.isLoading = false
    const wrapper = mount(TreatmentCenterTab, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })

    await flushPromises()
    await wrapper.setData({ formActive: false })
    expect(wrapper.find('#treatmentCenterTab').exists()).toBe(true)
    await wrapper.findAllComponents(Button).at(0).trigger('click')
    expect(wrapper.find('.submit').exists()).toBe(true)
  })

  it('cancel and form component', async () => {
    store.state.Form.isLoading = false
    const wrapper = mount(TreatmentCenterTab, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })

    await flushPromises()
    await wrapper.setData({ formActive: true })
    expect(wrapper.find('#treatmentCenterTab').exists()).toBe(true)
    await wrapper.findAllComponents(Button).at(0).trigger('click')
    expect(wrapper.find('.submit').exists()).toBe(false)
  })

  it('cancel button with loading as true', async () => {
    store.state.Form.isLoading = true
    const wrapper = mount(TreatmentCenterTab, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })

    await flushPromises()
    await wrapper.setData({ formActive: true })
    expect(wrapper.find('#treatmentCenterTab').exists()).toBe(true)
    await wrapper.findAllComponents(Button).at(0).trigger('click')
    expect(wrapper.find('.submit').exists()).toBe(false)
  })

  it('press submit', async () => {
    store.state.Form.isLoading = false
    const wrapper = mount(TreatmentCenterTab, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })

    await wrapper.setData({ formActive: true })
    await flushPromises()
    expect(wrapper.findComponent(Form).exists()).toBe(true)
    await wrapper.findComponent(Form).findComponent(Button).trigger('click')
    expect(wrapper.find('.secondary').exists()).toBe(true)
  })
})

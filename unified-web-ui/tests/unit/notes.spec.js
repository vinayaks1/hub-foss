import { mount, createLocalVue } from '@vue/test-utils'
import Notes from '../../src/app/Pages/dashboard/patient/components/Notes'
import Modal from '@/app/shared/components/Modal'
import Vuex from 'vuex'
const localVue = createLocalVue()
localVue.use(Vuex)

describe('Notes page Tests', () => {
  let store
  let actions
  beforeEach(() => {
    actions = {
      getPatientNotes: jest.fn(),
      addTagsAndNote: jest.fn()
    }
    store = new Vuex.Store({
      modules: {
        Notes: {
          namespaced: true,
          actions
        }
      }
    })
  })

  it('notes loaded correctly', async () => {
    var sampleResponse = { Success: true, tableList: [{ id: '1', value: 'Option 1' }, { id: '2', value: 'Option 2' }, { id: '3', value: 'Option 3' }, { id: '4', value: 'Option 4' }, { id: '5', value: 'Option 5' }, { id: '6', value: 'Option 6' }, { id: '7', value: 'Option 7' }, { id: '8', value: 'Option 8' }, { id: '9', value: 'Option 4' }, { id: '10', value: 'Option 5' }, { id: '11', value: 'Option 6' }, { id: '12', value: 'Option 7' }, { id: '13', value: 'Option 8' }], Error: null }
    actions.getPatientNotes.mockReturnValue(sampleResponse)
    mount(Notes, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    expect(actions.getPatientNotes.mock.calls).toHaveLength(0)
  })

  it('adding tags and notes', async () => {
    var wrapper = mount(Notes, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    await wrapper.setData({ addingNote: true })
    var sampleResponse = { Success: true, tableList: [{ id: '1', value: 'Option 1' }, { id: '2', value: 'Option 2' }, { id: '3', value: 'Option 3' }, { id: '4', value: 'Option 4' }, { id: '5', value: 'Option 5' }, { id: '6', value: 'Option 6' }, { id: '7', value: 'Option 7' }, { id: '8', value: 'Option 8' }, { id: '9', value: 'Option 4' }, { id: '10', value: 'Option 5' }, { id: '11', value: 'Option 6' }, { id: '12', value: 'Option 7' }, { id: '13', value: 'Option 8' }], Error: null }
    actions.getPatientNotes.mockReturnValue(sampleResponse)
    wrapper.findComponent(Modal).vm.$emit('click')
    expect(actions.addTagsAndNote.mock.calls).toHaveLength(0)
    wrapper.findComponent(Modal).vm.$emit('close')
    await wrapper.setData({ addingNote: false })
    expect(wrapper.find('.modal-backdrop').exists()).toBe(false)
  })

  it('fetch formatted date', () => {
    const wrapper = mount(Notes, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    const testResult = wrapper.vm.getFormattedDate('2021-10-12 18:30:00')
    expect(testResult).toBe('12/10/2021 18:30')
  })
})

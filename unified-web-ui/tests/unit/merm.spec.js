import { mount, createLocalVue } from '@vue/test-utils'
import MERM from '../../src/app/Pages/dashboard/patient/components/MERM.vue'
import EditMerm from '../../src/app/Pages/dashboard/patient/components/EditMerm.vue'
import Button from '@/app/shared/components/Button'
import Vuex from 'vuex'
const localVue = createLocalVue()
localVue.use(Vuex)

describe('MERM page Tests', () => {
  let store
  let actions
  beforeEach(() => {
    actions = {
      getMermDetails: jest.fn(),
      updateMermDetails: jest.fn(),
      getLastSeenAndBattery: jest.fn()
    }
    store = new Vuex.Store({
      modules: {
        MERM: {
          namespaced: true,
          state: {
            mermDetails: [],
            mermDetailsLoading: false,
            mermDetailsLoaded: false
          },
          actions
        },
        EditMerm: {
          namespaced: true,
          state: {
            mermDetails: []
          },
          actions
        }
      }
    })
  })

  it('items loaded correctly', () => {
    const wrapper = mount(MERM, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    wrapper.vm.edit()
    expect(actions.getMermDetails.mock.calls).toHaveLength(1)
  })

  it('items updated correctly', () => {
    const wrapper = mount(EditMerm, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    wrapper.findAllComponents(Button).trigger('click')
    expect(actions.updateMermDetails.mock.calls).toHaveLength(1)
  })

  it('fetch merm metrics', () => {
    const wrapper = mount(EditMerm, {
      store,
      localVue,
      mocks: {
        $t: (key) => key
      }
    })
    actions.getLastSeenAndBattery.mockReturnValue('Test Data')
    wrapper.vm.getMermMetrics()
    expect(actions.getLastSeenAndBattery.mock.calls).toHaveLength(1)
  })
})

import {
  createLocalVue,
  shallowMount,
  mount
} from '@vue/test-utils'
import Table from '@/app/shared/components/Table.vue'
const sortData = {
  sortBy: 'name',
  sortDesc: false
}
const tempItem = {
  Clickable: true,
  ClickableLink: 'hello'
}
describe('Table unit test', () => {
  test('Rendering props', () => {
    const wrapper = mount(Table, {
      propsData: {
        pageSize: 5,
        columns: [{}],
        data: [{}]
      },
      mocks: {
        $t: (key) => {
          return { key: key }
        }
      }
    })
    expect(wrapper.props().pageSize).toBe(5)
    expect(wrapper.props().columns.length).toBe(1)
    expect(wrapper.props().data.length).toBe(1)
  })

  test('emit event test', async () => {
    const wrapper = mount(Table, {
      propsData: {
        pageSize: 4,
        columns: [{}],
        data: [{}]
      },
      mocks: {
        $t: (key) => {
          return { key: key }
        }
      }
    })
    wrapper.vm.$emit('sortTableEvent')
    wrapper.vm.$emit('sortTableEvent', sortData)
    await wrapper.vm.$nextTick()
    expect(wrapper.emitted().sortTableEvent).toBeTruthy()
    expect(wrapper.emitted().sortTableEvent.length).toBe(2)
    expect(wrapper.emitted().sortTableEvent[1][0].sortBy).toEqual('name')
    expect(wrapper.emitted().sortTableEvent[1][0].sortDesc).toEqual(false)
  })
  test('methods', async () => {
    const localVue = createLocalVue()
    const wrapper = shallowMount(Table, {
      localVue,
      propsData: {
        pageSize: 4,
        columns: [{}],
        data: [{}]
      },
      mocks: {
        $t: (key) => {
          return { key: key }
        }
      }
    })
    wrapper.vm.GoToPatient = jest.fn()
    wrapper.vm.sortEvent(sortData)
    wrapper.vm.GoToPatient(tempItem)
    expect(wrapper.vm.GoToPatient).toHaveBeenCalledWith(tempItem)
  })
})

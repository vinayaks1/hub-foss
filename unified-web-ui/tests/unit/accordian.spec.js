import { mount, createLocalVue } from '@vue/test-utils'
import Accordian from '@/app/shared/components/Accordion.vue'
import { accordionState } from '@/utils/accordionUtils.js'

describe('Accordian Tests using default settings', () => {
  const localVue = createLocalVue()
  localVue.use({ Accordian })
  const testData = {
    content: 'gdgfdtdsgsdsgdsgreewrefew',
    titleText: 'test',
    contentIsHtml: false
  }

  const wrapper = mount(Accordian, {
    localVue,
    propsData: {
      ...testData
    },
    mocks: {
      $t: (key) => key
    }
  })
  it('has mounted', () => {
    expect(wrapper.find('div').exists()).toBe(true)
  })

  it('by default should be collpsed', () => {
    expect(wrapper.find('.customAccordionContainer').exists()).toBe(true)
  })

  it('expand on click', async () => {
    const expandBtn = wrapper.find('.expand-collapse-button')
    await expandBtn.trigger('click')
    expect(wrapper.find('.customAccordionContainer').exists()).toBe(false)
    expect(wrapper.find('.customAccordionContainerExpanded').exists()).toBe(true)
    await expandBtn.trigger('click') // restoring it for next test
  })

  it('collapses when button clicked', async () => {
    const expandBtn = wrapper.find('.expand-collapse-button')
    await expandBtn.trigger('click')
    await expandBtn.trigger('click')
    expect(wrapper.find('.customAccordionContainerExpanded').exists()).toBe(false)
    expect(wrapper.find('.customAccordionContainer').exists()).toBe(true)
  })
})

describe('Accordian Tests explicit settings', () => {
  const localVue = createLocalVue()
  localVue.use({ Accordian })
  const testData = {
    content: '<p><a href="http://www.google.com">This is a link</a></p>',
    titleText: 'test',
    contentIsHtml: true,
    AccordionStatus: accordionState.Expanded

  }

  const wrapper = mount(Accordian, {
    localVue,
    propsData: {
      ...testData
    },
    mocks: {
      $t: (key) => key
    }
  })

  it('has mounted', () => {
    expect(wrapper.find('div').exists()).toBe(true)
  })

  it('by default should be expanded', () => {
    expect(wrapper.find('.customAccordionContainerExpanded').exists()).toBe(true)
  })
  it('should work correctly if initial state is expanded', async () => {
    const expandBtn = wrapper.find('.expand-collapse-button')
    await expandBtn.trigger('click')
    expect(wrapper.find('.customAccordionContainerExpanded').exists()).toBe(false)
    expect(wrapper.find('.customAccordionContainer').exists()).toBe(true)
    await expandBtn.trigger('click')
    expect(wrapper.find('.customAccordionContainer').exists()).toBe(false)
    expect(wrapper.find('.customAccordionContainerExpanded').exists()).toBe(true)
  })
})

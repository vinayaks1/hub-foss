import { createLocalVue, mount } from '@vue/test-utils'
import VerticalFormPart from '@/app/shared/components/VerticalFormPart.vue'
import Vuex from 'vuex'
import FormStore from '@/app/shared/store/modules/form'
import flushPromises from 'flush-promises'
import Vue from 'vue'
import Form from '@/app/shared/components/Form.vue'

const localVue = createLocalVue()

localVue.use(Vuex)
window.EventBus = new Vue()

jest.mock('../../src/utils/toastUtils', () => {
  return {
    __esModule: true,
    defaultToast: jest.fn(),
    ToastType: {
      Success: 'Success',
      Error: 'Error',
      Warning: 'Warning',
      Neutal: 'Neutal'
    }
  }
})

const getStore = (actions) => {
  const formattedActions = {
    ...FormStore.actions,
    ...actions
  }

  return new Vuex.Store({
    modules: {
      Form: {
        state: FormStore.state,
        actions: formattedActions,
        mutations: FormStore.mutations,
        namespaced: true
      }
    }
  })
}

const formWithOneInput = {
  Success: true,
  Data: {
    Form: {
      Name: 'SingleInputForm1',
      Title: '_dummy_input_form',
      Parts: [
        {
          FormName: 'SingleInputForm1',
          Name: 'input_form_01',
          Title: '',
          TitleKey: 'input_form_01',
          Order: 1,
          IsVisible: true,
          Id: 119,
          Type: 'vertical-form-part',
          Rows: null,
          Columns: null,
          RowDataName: 'input_form_01',
          AllowRowOpen: false,
          AllowRowDelete: false,
          ListItemTitle: null,
          ItemDescriptionField: null,
          IsRepeatable: false,
          RecordId: null
        }
      ],
      PartOptions: null,
      Fields: [
        {
          Value: '',
          PartName: 'input_form_01',
          Placeholder: null,
          Component: 'app-input-field',
          IsDisabled: false,
          IsVisible: true,
          LabelKey: 'Validate_input__Required_01',
          Label: null,
          Name: 'Validate_input__Required_01',
          IsHierarchySelector: false,
          IsRequired: false,
          RemoteUrl: null,
          Type: null,
          AddOn: null,
          Options: null,
          HierarchyOptions: null,
          OptionsWithKeyValue: null,
          AdditionalInfo: null,
          DefaultVisibilty: false,
          Order: 0,
          OptionsWithLabel: null,
          Validations: {
            Or: null,
            And: [
            ]
          },
          ResponseDataPath: null,
          OptionDisplayKeys: null,
          OptionValueKey: null,
          LoadImmediately: false,
          HierarchySelectionConfigs: null,
          DisabledDateConfig: null,
          RemoteUpdateConfig: null,
          RowNumber: 16,
          ColumnNumber: null,
          Id: 11407,
          ParentType: 'PART',
          ParentId: 119,
          FieldOptions: null,
          ValidationList: 'Required',
          DefaultValue: '',
          Key: 'input_form_01Validate_input__Required_01',
          HasInfo: false,
          InfoText: null,
          Config: null,
          ColumnWidth: 6,
          HasToggleButton: false
        }
      ],
      Triggers: null,
      TriggerConfigs: null,
      ValueDependencies: [],
      FilterDependencies: [],
      VisibilityDependencies: [],
      DateConstraintDependencies: [],
      IsRequiredDependencies: [],
      RemoteUpdateConfigs: [],
      ValuePropertyDependencies: [],
      CompoundValueDependencies: null,
      SaveEndpoint: '/api/patients',
      SaveText: null,
      SaveTextKey: '_submit'
    },
    ExistingData: {}
  },
  Error: null
}

const getNewForm = () => {
  return JSON.parse(JSON.stringify(formWithOneInput))
}

describe('Vertical Form Part Tests', () => {
  it('renders props.headingText when passed', async () => {
    const formData = getNewForm()
    const heading = formData.Data.Form.Parts[0].Name
    const wrapper = mount(Form, {
      store: getStore({
        getFormData: async () => (
          Promise.resolve(
            formData
          )
        )
      }),
      localVue,
      propsData: { headingText: heading },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    expect(wrapper.findComponent(VerticalFormPart).find('#vertical-heading').text()).toBe(heading)
  })
})

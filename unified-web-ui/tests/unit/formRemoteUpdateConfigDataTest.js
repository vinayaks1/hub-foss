const testForm = {
  Success: true,
  Error: null,
  Data: {
    Form: {
      Name: 'FormName',
      Title: 'FormName',
      Parts: [
        {
          FormName: 'FormName',
          Name: 'PartName1',
          Title: 'PartName 1',
          TitleKey: 'PartName1',
          Order: 1,
          IsVisible: true,
          Id: 1,
          Type: 'grid-form-part',
          Rows: 4,
          Columns: 3,
          RowDataName: 'PartName 1',
          AllowRowOpen: false,
          AllowRowDelete: false,
          ListItemTitle: null,
          ItemDescriptionField: null,
          IsRepeatable: false,
          RecordId: null
        }
      ],
      PartOptions: null,
      Fields: [
        {
          Value: null,
          PartName: 'PartName1',
          Placeholder: null,
          Component: 'app-select',
          IsDisabled: false,
          IsVisible: true,
          LabelKey: 'Field1',
          Label: 'Field_1',
          Name: 'Field1',
          IsHierarchySelector: false,
          IsRequired: false,
          RemoteUrl: '',
          Type: null,
          AddOn: null,
          Options: null,
          HierarchyOptions: null,
          OptionsWithKeyValue: [
            {
              Value: 'Value 1',
              Key: 'Key1'
            },
            {
              Value: 'Value 2',
              Key: 'Key2'
            }
          ],
          AdditionalInfo: null,
          DefaultVisibilty: false,
          Order: 0,
          OptionsWithLabel: null,
          Validations: {
            Or: null,
            And: []
          },
          ResponseDataPath: null,
          OptionDisplayKeys: null,
          OptionValueKey: null,
          LoadImmediately: false,
          HierarchySelectionConfigs: null,
          DisabledDateConfig: null,
          RemoteUpdateConfig: [{
            Url: '/UnifiedHome/DemoApi',
            FormPart: 'PartName1',
            Field: 'Field2',
            ResponsePath: ['Data'],
            ConditionalCall: 'true',
            ValueDependency: [
              {
                CheckNotNull: 'true',
                FormPart: 'PartName1',
                Field: 'Field1'
              }
            ],
            UrlVariables: {
              id: ['PartName1', 'Field1']
            }
          }],
          RowNumber: 1,
          ColumnNumber: null,
          Id: 1,
          ParentType: 'PART',
          ParentId: 1,
          FieldOptions: '',
          ValidationList: '',
          DefaultValue: '',
          Key: 'PartName1Field1',
          HasInfo: false,
          InfoText: null,
          Config: null,
          ColumnWidth: 1,
          HasToggleButton: false
        },
        {
          Value: null,
          PartName: 'PartName1',
          Placeholder: null,
          Component: 'app-select',
          IsDisabled: false,
          IsVisible: true,
          LabelKey: 'Field2',
          Label: 'Field_2',
          Name: 'Field2',
          IsHierarchySelector: false,
          IsRequired: false,
          RemoteUrl: '',
          Type: null,
          AddOn: null,
          Options: null,
          HierarchyOptions: null,
          OptionsWithKeyValue: null,
          AdditionalInfo: null,
          DefaultVisibilty: false,
          Order: 0,
          OptionsWithLabel: null,
          Validations: {
            Or: null,
            And: []
          },
          ResponseDataPath: null,
          OptionDisplayKeys: null,
          OptionValueKey: null,
          LoadImmediately: false,
          HierarchySelectionConfigs: null,
          DisabledDateConfig: null,
          RemoteUpdateConfig: null,
          RowNumber: 2,
          ColumnNumber: null,
          Id: 2,
          ParentType: 'PART',
          ParentId: 1,
          FieldOptions: '',
          ValidationList: '',
          DefaultValue: '',
          Key: 'PartName1Field2',
          HasInfo: false,
          InfoText: null,
          Config: null,
          ColumnWidth: 1,
          HasToggleButton: false
        },
        {
          Value: null,
          PartName: 'PartName1',
          Placeholder: null,
          Component: 'app-select',
          IsDisabled: false,
          IsVisible: true,
          LabelKey: 'Field3',
          Label: 'Field_3',
          Name: 'Field3',
          IsHierarchySelector: false,
          IsRequired: false,
          RemoteUrl: '',
          Type: null,
          AddOn: null,
          Options: null,
          HierarchyOptions: null,
          OptionsWithKeyValue: [
            {
              Value: 'Value 1',
              Key: 'Key1'
            },
            {
              Value: 'Value 2',
              Key: 'Key2'
            },
            {
              Value: 'Value 3',
              Key: 'Key3'
            },
            {
              Value: 'Value 4',
              Key: 'Key4'
            }
          ],
          AdditionalInfo: null,
          DefaultVisibilty: false,
          Order: 0,
          OptionsWithLabel: null,
          Validations: {
            Or: null,
            And: []
          },
          ResponseDataPath: null,
          OptionDisplayKeys: null,
          OptionValueKey: null,
          LoadImmediately: false,
          HierarchySelectionConfigs: null,
          DisabledDateConfig: null,
          RemoteUpdateConfig: [{
            Url: '/UnifiedHome/DemoApi',
            FormPart: 'PartName1',
            Field: 'Field2',
            ResponsePath: ['Data'],
            ConditionalCall: 'true',
            ValueDependency: [
              {
                CheckNotNull: 'true',
                FormPart: 'PartName1',
                Field: 'Field3'
              }
            ],
            UrlVariables: {
              id: ['PartName1', 'Field3']
            }
          }],
          RowNumber: 3,
          ColumnNumber: null,
          Id: 3,
          ParentType: 'PART',
          ParentId: 1,
          FieldOptions: '',
          ValidationList: '',
          DefaultValue: '',
          Key: 'PartName1Field3',
          HasInfo: false,
          InfoText: null,
          Config: null,
          ColumnWidth: 1,
          HasToggleButton: false,
          isMultiSelect: true
        }
      ],
      Triggers: null,
      TriggerConfigs: null,
      ValueDependencies: [],
      FilterDependencies: [],
      VisibilityDependencies: [],
      DateConstraintDependencies: [],
      IsRequiredDependencies: [],
      RemoteUpdateConfigs: null,
      ValuePropertyDependencies: [],
      CompoundValueDependencies: null,
      SaveEndpoint: '/API/Dispensation/AddDispensation/',
      SaveText: 'Submit',
      SaveTextKey: 'submit'
    },
    ExistingData: {}
  }
}

export default testForm

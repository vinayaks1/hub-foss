import { shallowMount } from '@vue/test-utils'
import Modal from '@/app/shared/components/Modal.vue'

describe('Modal.vue', () => {
  it('basic classes test', () => {
    const wrapper = shallowMount(Modal, {
      mocks: {
        $t: (key) => key
      }
    })
    expect(wrapper.find('.modal-backdrop').exists()).toBe(true)
  })

  it('close click', async () => {
    const wrapper = shallowMount(Modal, {
      mocks: {
        $t: (key) => key
      }
    })
    await wrapper.vm.close()
    expect(wrapper.emitted('close')).toBeTruthy()
    await wrapper.vm.click()
    expect(wrapper.emitted('click')).toBeTruthy()
  })
})

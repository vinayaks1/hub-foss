import { getCookie, setCookie } from '@/utils/cookieUtils.js'

describe('cookie', () => {
  it('returns null when cookie is not available', () => {
    expect(getCookie('test')).toBeNull()
  })

  it('gets correct cookie when set', () => {
    setCookie('test', 'testvalue', 1)
    expect(getCookie('test')).toMatch('testvalue')
  })

  it('sets or updates correct cookie when set', () => {
    const newTestValue = 'newtestvalue'
    setCookie('test', newTestValue, 1)
    expect(getCookie('test')).toMatch(newTestValue)
  })

  it('sets or updates empty string value when set', () => {
    const emptyTestValue = ''
    setCookie('test', emptyTestValue, 1)
    expect(getCookie('test')).toMatch(emptyTestValue)
  })

  it('sets or updates cookie for infinite time or no time', () => {
    const emptyTestValue = ''
    setCookie('test', emptyTestValue)
    expect(getCookie('test')).toMatch(emptyTestValue)
  })
})

import { mount, createLocalVue } from '@vue/test-utils'
import TreeView from '@/app/shared/components/TreeView.vue'
import Vuetify from 'vuetify'
import Vue from 'vue'

Vue.use(Vuetify)
describe('TreeView.vue', () => {
  const localVue = createLocalVue()
  let vuetify
  beforeEach(() => {
    vuetify = new Vuetify()
  })

  it('renders props.treeData when passed', async () => {
    const treeData = [
      {
        id: 1,
        name: 'Applications :',
        children: [
          { id: 2, name: 'Calendar : app' },
          { id: 3, name: 'Chrome : app' },
          { id: 4, name: 'Webstorm : app' }
        ]
      }
    ]
    const valueData = [3]
    const wrapper = mount(TreeView, {
      localVue,
      vuetify,
      model: valueData,
      propsData: {
        treeData: treeData,
        selectable: true
      },
      mocks: {
        $t: (key) => key
      }
    })
    expect(wrapper.find('.mdi-checkbox-marked').exists()).toBe(false)

    wrapper.find('.v-treeview-node__root').trigger('click')
    await wrapper.vm.$nextTick()
    await wrapper.vm.$nextTick()

    wrapper.find('.v-treeview-node__checkbox').trigger('click')
    await wrapper.vm.$nextTick()
    await wrapper.vm.$nextTick()

    expect(wrapper.find('.mdi-checkbox-marked').exists()).toBe(true)
    expect(wrapper.find('.mdi-checkbox-blank-outline').exists()).toBe(false)
    expect(wrapper.emitted().input).toBeTruthy()
  })
})

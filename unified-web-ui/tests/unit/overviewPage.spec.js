import { mount, createLocalVue } from '@vue/test-utils'
import Overview from '../../src/app/Pages/dashboard/Overview/Overview'
import Slider from '../../src/app/Pages/dashboard/Overview/components/Slider'
import StatsCardGroup from '../../src/app/Pages/dashboard/Overview/components/StatsCardGroup'
import StatsCard from '../../src/app/Pages/dashboard/Overview/components/StatsCard'
import ChartsUI from '../../src/app/Pages/dashboard/Overview/components/ChartsUI'
import Selector from '../../src/app/Pages/dashboard/Overview/components/Selector'
import ChartSummary from '../../src/app/Pages/dashboard/Overview/components/ChartSummary'

import Vuex from 'vuex'


const localVue = createLocalVue()
localVue.use(Vuex)
const $t = () => {}

describe('Overview Page Tests', () => {
  let store
  let actions
  beforeEach(() => {
    actions = {
      updateOverviewConfig: jest.fn(),
      loadStats: jest.fn(),
      loadOverviewStats: jest.fn(),
      loadChart: jest.fn(),
      loadChartsToBeDisplayed: jest.fn(),
      loadSliderImages: jest.fn(),
      onSlideStart: jest.fn()
    }
    store = new Vuex.Store({
      modules: {
        Overview: {
          namespaced: true,
          state: {
            isSliderEnabled: true,
            isStatsEnabled: true,
            isChartsEnabled: true,
            stats: [],
            charts: [],
            sliderImages: ['test'],
            typesOfStats: []
          },
          actions
        }
      }
    })
  })

  it('calls updateOverviewConfig on Mount', () => {
    process.env.VUE_APP_THEME = 'hub'
    const wrapper = mount(Overview, { store, localVue, mocks:{ $t } })
    wrapper.vm.$nextTick()
    expect(actions.updateOverviewConfig.mock.calls).toHaveLength(1)
  })

  it('Slider enabled', async () => {
    store.state.Overview.isSliderEnabled = true
    const wrapper = mount(Overview, { store, localVue, mocks:{ $t } })
    await wrapper.setData({ loaded: true });
    console.log(wrapper.html())
    expect(wrapper.find('#slider-container').exists()).toBe(true)
  })
  
  it('onSlideStart test', async () => {
    const wrapper = mount(Slider, { store, localVue, mocks:{ $t } })
    await wrapper.setData({ isSliderEnabled: true });
    await wrapper.setData({ loading: false });
    await wrapper.setData({ sliding: false });
    await wrapper.find('#carousel-1').trigger('sliding-start')
    expect(wrapper.vm.$data.sliding).toBe(true)
  })
  
   it('onSlideEnd test', async () => {
    const wrapper = mount(Slider, { store, localVue, mocks:{ $t } })
    await wrapper.setData({ isSliderEnabled: true });
    await wrapper.setData({ loading: false });
    await wrapper.setData({ sliding: true });
    await wrapper.find('#carousel-1').trigger('sliding-end')
    expect(wrapper.vm.$data.sliding).toBe(false)
  })
  
  it('ShowImg test', async () => {
    const wrapper = mount(Slider, { store, localVue, mocks:{ $t } })
    await wrapper.setData({ isSliderEnabled: true });
    await wrapper.setData({ loading: false });
    await wrapper.setData({ index: 0 });
    await wrapper.find('#slider-container').trigger('click')
    expect(wrapper.vm.$data.src).toBe('test')
  })


  it('Stats enabled', async () => {
    store.state.Overview.isStatsEnabled = true
    const wrapper = mount(Overview, { store, localVue, mocks:{ $t } })
    await wrapper.setData({ loaded: true });
    expect(wrapper.find('.title-text').exists()).toBe(true)
  })
  
  it('StatsCardGroup mount', async () => {
    store.state.Overview.isStatsEnabled = true
    const wrapper = mount(StatsCardGroup, { store, localVue, mocks:{ $t } })
    await wrapper.setData({ loading: false });
    console.log(wrapper.html())
    expect(wrapper.find('.stats-card-group').exists()).toBe(true)
  })

  it('Charts enabled', async () => {
    store.state.Overview.isChartsEnabled = true
    const wrapper = mount(Overview, { store, localVue, mocks:{ $t } })
    await wrapper.setData({ loaded: true });
    expect(wrapper.find('.charts-container').exists()).toBe(true)
  })
  
  it('ChartsUI mount', async () => {
    store.state.Overview.isChartsEnabled = true
    const wrapper = mount(ChartsUI, { store, localVue, mocks:{ $t } ,propsData: {
        chartType: 'area',
      }})
    await wrapper.setData({ loading: false });
    expect(wrapper.find('.custom-empty-msg').exists()).toBe(true)
  })
  
  it('Selector mount', async () => {
    const wrapper = mount(Selector, { store, localVue, mocks:{ $t } })
    await wrapper.setData({ loading: false });
    expect(wrapper.find('b-form-select').exists()).toBe(true)
    await wrapper.find('b-form-select').trigger('change')
  })
  
  it('HandleChange Selector test', async () => {
    const wrapper = mount(Selector, { store, localVue, mocks:{ $t } })
    await wrapper.setData({ isChartsEnabled: true });
    await wrapper.setData({ loading: false });
    await wrapper.setData({ selected: 'last_month' });
    await wrapper.find('b-form-select').trigger('change')
    await wrapper.setData({ selected: 'six_months' });
    await wrapper.find('b-form-select').trigger('change')
    await wrapper.setData({ selected: 'last_week' });
    await wrapper.find('b-form-select').trigger('change')
    await wrapper.setData({ selected: 'all_time' });
    await wrapper.find('b-form-select').trigger('change')
  })
  
  it('ChartsSummary mount', async () => {
    store.state.Overview.isChartsEnabled = true
    const wrapper = mount(ChartSummary, { store, localVue, mocks:{ $t } })
    await wrapper.setData({ loading: false });
    expect(wrapper.find('.card-body').exists()).toBe(true)
  })
  
  it('ChartsSummary LoadTable test', async () => {
    store.state.Overview.isChartsEnabled = true
    const wrapper = mount(ChartSummary, { store, localVue, mocks:{ $t }, 
      propsData: {
        dataHeadings: "test",
        chartOptions: {
          "label" : "testlabel",
          "colors" : "testcolour"
        },
        chartData : ["testarray1", "testarray2"]
      }, } )
    await wrapper.vm.$nextTick()
    await wrapper.setData({ loading: false });
    expect(wrapper.find('.card-body').exists()).toBe(true)
    const loadTableItems = jest.fn()
    wrapper.setMethods({
      loadTableItems: loadTableItems
    })
    /* eslint-disable */
    expect(loadTableItems).toHaveBeenCalled
    /* eslint-enable */
    expect(wrapper.vm.$data.fields).toBe("test")
  })
  
  it('Loading test', async () => {
    const wrapper = mount(Slider, { store, localVue, mocks:{ $t } })
    await wrapper.setData({ loading: true });
    expect(wrapper.find('.loading-container').exists()).toBe(true)
  })
  
  it('StatsCard test', async () => {
    const wrapper = mount(StatsCard, { store, localVue, mocks:{ $t } })
    await wrapper.setData({ loading: false });
    expect(wrapper.find('.stats-card').exists()).toBe(true)
  })
  
})

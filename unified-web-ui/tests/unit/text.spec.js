import { shallowMount, createLocalVue } from '@vue/test-utils'

import Text from '@/app/shared/components/Text.vue'

import Vuex from 'vuex'
const localVue = createLocalVue()

localVue.use(Vuex)
const getStore = () => {
  return new Vuex.Store({
  })
}

describe('Text.vue', () => {
  it('renders props.label when passed', () => {
    const label = 'new label'
    const wrapper = shallowMount(Text, {
      store: getStore(),
      localVue,
      propsData: { label },
      mocks: {
        $t: (key) => key
      }
    })
    expect(wrapper.find('.text-styling-one').text()).toBe(label)
  })
})

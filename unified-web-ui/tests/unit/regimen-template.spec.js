import { mount, shallowMount, createLocalVue } from '@vue/test-utils'
import RegimenTemplate from '../../src/app/Pages/dashboard/RegimenTemplate/RegimenTemplate'
import Vuex from 'vuex'
import Button from '@/app/shared/components/Button.vue'
const localVue = createLocalVue()
localVue.use(Vuex)

describe('Add Regimen Template tests', () => {
	// render the component
	let methods =  {
		successfulClick: jest.fn()
	}
	const $t = () => {}
	const wrapper = mount(RegimenTemplate, {
		mocks: {
			$t: (key) => key
		  },
		methods : methods
	})
	it('renders title and component when created', () => {
	    // check that the title is rendered
	    expect(wrapper.text()).toContain('_add_medication_template')
	    expect(wrapper.find('#regimen-template-app')).toBeTruthy()
  	})

  	it('click is successful', async () => {
		const submitFunction = jest.spyOn(RegimenTemplate.methods, "successfulClick");

	    // check that the title is rendered
	    await wrapper.findComponent(Button).find('button').trigger('click')
	    expect(methods.successfulClick).toHaveBeenCalled();
  	})

})
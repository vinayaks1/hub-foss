import { isNotNullNorEmpty, createMapOf, IsNullOrUndefinedOrEmptyString, isNullOrUndefined, getFormattedDate, getFormattedDateForRegistration, padZeroes, isObjectValueDefined } from '@/utils/utils.js'

describe('utils.js Tests', () => {
  let obj
  it('null test', () => {
    obj = null
    expect(isNotNullNorEmpty(obj)).toBeFalsy()
  })

  it('undefined test', () => {
    expect(isNotNullNorEmpty(obj)).toBeFalsy()
  })
  it('empty obj test', () => {
    obj = {}
    expect(isNotNullNorEmpty(obj)).toBeFalsy()
  })
  it('empty string test', () => {
    obj = ''
    expect(isNotNullNorEmpty(obj)).toBeFalsy()
  })
  it('empty array test', () => {
    obj = []
    expect(isNotNullNorEmpty(obj)).toBeFalsy()
  })

  it('non empty obj test', () => {
    obj = { a: 1 }
    expect(isNotNullNorEmpty(obj)).toBeTruthy()
  })
  it('non empty string test', () => {
    obj = 'xxxx'
    expect(isNotNullNorEmpty(obj)).toBeTruthy()
  })
  it('non empty array test', () => {
    obj = [1, 2, 3]
    expect(isNotNullNorEmpty(obj)).toBeTruthy()
  })

  it('createMapOf test', () => {
    const inputArrayOfObjects= [{key1: 'val1', key2: 'val2'}, {key1: 'val11', key2: 'val22'}]
    const key = 'key1'
    const returnedMap = createMapOf(key,inputArrayOfObjects)
    expect(returnedMap.get('val1')['key1']).toBe('val1')
  })

  it('IsNullOrUndefinedOrEmptyString test', () => {
    expect(IsNullOrUndefinedOrEmptyString(null)).toBeTruthy()
    expect(IsNullOrUndefinedOrEmptyString('')).toBeTruthy()
    expect(IsNullOrUndefinedOrEmptyString(undefined)).toBeTruthy()
    expect(IsNullOrUndefinedOrEmptyString('a')).toBeFalsy()
  })

  it('isNullOrUndefined test', () => {
    expect(isNullOrUndefined(null)).toBeTruthy()
    expect(isNullOrUndefined(undefined)).toBeTruthy()
    expect(isNullOrUndefined('')).toBeFalsy()
  })

  it('padZeroes test', () => {
    expect(padZeroes('abc',4)).toBe('0abc')
  })

  it('isObjectValueDefined test', () => {
    expect(isObjectValueDefined('')).toBeFalsy()
    expect(isObjectValueDefined({})).toBeFalsy()
    expect(isObjectValueDefined({key: 'val'})).toBeTruthy()
  })

  it('getFormattedDate test', () => {
    expect(getFormattedDate('2022-01-01')).toBe('01-01-2022')
  })

  it('getFormattedDateForRegistration test', () => {
    expect(getFormattedDateForRegistration('01-01-2022')).toBe('2022-01-01')
  })
})

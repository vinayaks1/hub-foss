import { mount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import Vuetify from 'vuetify'
import DeletePatient from '@/app/Pages/dashboard/patient/components/DeletePatient.vue'
import Button from '@/app/shared/components/Button.vue'
import Form from '@/app/shared/components/Form.vue'
import Vue from 'vue'
import { Map, List } from 'immutable'
import flushPromises from 'flush-promises'

const localVue = createLocalVue()
const $t = () => {}
localVue.use(Vuex)
window.EventBus = new Vue()
jest.mock('../../src/utils/toastUtils', () => {
  return {
    __esModule: true,
    defaultToast: jest.fn(),
    ToastType: {
      Success: 'Success',
      Error: 'Error',
      Warning: 'Warning',
      Neutal: 'Neutal'
    }
  }
})
describe('DeletePatient.vue', () => {
    let store
    let actions
    beforeEach(() => {
      actions = {
        getFormData: jest.fn(),
        setIsEditing: jest.fn(),
        loadForm: jest.fn(),
        startSaving: jest.fn(),
        save: async () => (
          Promise.resolve({
            Success: true,
            Error: null,
            Data: 'Success'
          })
        ),
        endSaving: jest.fn()
      }
      store = new Vuex.Store({
        modules: {
          Form: {
            namespaced: true,
            state: {
                isSaving: false,
                isLoading: false,
                isEditing: true,
                hasFormError: false,
                adherenceTechnology: { MERM: false, VOT: false },
                existingData: null,
                existingVModel: {},
                form: {},
                title: '',
                remoteUpdateConfigsMappedByField: Map(),
                remoteUpdateConfigs: [],
                hierarchyConfigs: Map(),
                valueDependencies: List(),
                formPartsMappedByName: Map(),
                fieldsMappedByFormPartName: Map(),
                fieldsMappedByNameOriginal: Map(),
                fieldsMappedByName: null,
                allFields: null,
                visibilityDependencies: List(),
                isRequiredDependencies: List(),
                filterDependencies: List(),
                visibilityDependenciesMappedByFormPartName: null,
                dateConstraintDependency: List(),
                formFieldsByFormPartName: {},
                saveEndpoint: '',
                saveText: '',
                saveUrlParams: '',
                getFormUrlParams: '',
                replaceExistingDataFromServer: true,
                recaptchaVerified: false,
                otp: '',
                editAccess: false,
                valuePropertyDependencies: List()
              },
            actions
          },
          Patient: {
            namespaced: true,
            state: {
                patientId: null
            },
            actions
          }
        }
      })
    })

    it('render editdetails and form component', async () => {
        const wrapper = mount(DeletePatient, { store, localVue, mocks: { $router: { push: path => path }, $t } })
        await flushPromises()
        window.open = jest.fn()
        expect(wrapper.find('#DeletePatient').exists()).toBe(true)
        expect(wrapper.findComponent(Form).exists()).toBe(true)
        await wrapper.findComponent(Form).findComponent(Button).trigger('click')
      })
})
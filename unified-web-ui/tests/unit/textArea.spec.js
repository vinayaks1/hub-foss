import {mount, shallowMount,createLocalVue } from '@vue/test-utils'
import FormStore from '@/app/shared/store/modules/form'
import TextArea from '@/app/shared/components/TextArea.vue'
import flushPromises from 'flush-promises'
import Vue from 'vue'
import Form from '@/app/shared/components/Form.vue'
import Vuex from 'vuex'
const localVue = createLocalVue()

localVue.use(Vuex)
const getStore = () => {
  return new Vuex.Store({
  })
}
const getStoreWithActions = (actions) => {
  const formattedActions = {
    ...FormStore.actions,
    ...actions
  }

  return new Vuex.Store({
    modules: {
      Form: {
        state: FormStore.state,
        actions: formattedActions,
        mutations: FormStore.mutations,
        namespaced: true
      }
    }
  })
}
const isRequiredErrorMsg = 'error_field_required'

const formWithOneInput = {
  "Success": true,
  "Data": {
      "Form": {
          "Name": "SingleInputForm1",
          "Title": "_dummy_input_form",
          "Parts": [
              {
                  "FormName": "SingleInputForm1",
                  "Name": "input_form_01",
                  "Title": "",
                  "TitleKey": "input_form_01",
                  "Order": 1,
                  "IsVisible": true,
                  "Id": 119,
                  "Type": "vertical-form-part",
                  "Rows": null,
                  "Columns": null,
                  "RowDataName": "input_form_01",
                  "AllowRowOpen": false,
                  "AllowRowDelete": false,
                  "ListItemTitle": null,
                  "ItemDescriptionField": null,
                  "IsRepeatable": false,
                  "RecordId": null
              }
          ],
          "PartOptions": null,
          "Fields": [
              {
                  "Value": null,
                  "PartName": "input_form_01",
                  "Placeholder": null,
                  "Component": "app-textarea",
                  "IsDisabled": false,
                  "IsVisible": true,
                  "LabelKey": "Validate_input__Required_01",
                  "Label": null,
                  "Name": "Validate_input__Required_01",
                  "IsHierarchySelector": false,
                  "IsRequired": false,
                  "RemoteUrl": null,
                  "Type": null,
                  "AddOn": null,
                  "Options": null,
                  "HierarchyOptions": null,
                  "OptionsWithKeyValue": null,
                  "AdditionalInfo": null,
                  "DefaultVisibilty": false,
                  "Order": 0,
                  "OptionsWithLabel": null,
                  "Validations": {
                      "Or": null,
                      "And": [
                      ]
                  },
                  "ResponseDataPath": null,
                  "OptionDisplayKeys": null,
                  "OptionValueKey": null,
                  "LoadImmediately": false,
                  "HierarchySelectionConfigs": null,
                  "DisabledDateConfig": null,
                  "RemoteUpdateConfig": null,
                  "RowNumber": 16,
                  "ColumnNumber": null,
                  "Id": 11407,
                  "ParentType": "PART",
                  "ParentId": 119,
                  "FieldOptions": null,
                  "ValidationList": "Required",
                  "DefaultValue": null,
                  "Key": "input_form_01Validate_input__Required_01",
                  "HasInfo": false,
                  "InfoText": null,
                  "Config": null,
                  "ColumnWidth": 6,
                  "HasToggleButton": false
              }
          ],
          "Triggers": null,
          "TriggerConfigs": null,
          "ValueDependencies": [],
          "FilterDependencies": [],
          "VisibilityDependencies": [],
          "DateConstraintDependencies": [],
          "IsRequiredDependencies": [],
          "RemoteUpdateConfigs": [],
          "ValuePropertyDependencies": [],
          "CompoundValueDependencies": null,
          "SaveEndpoint": "/api/patients",
          "SaveText": null,
          "SaveTextKey": "_submit"
      },
      "ExistingData": {}
  },
  "Error": null
} 

const getNewForm = () => {
  return JSON.parse(JSON.stringify(formWithOneInput))
}
const dummyValidation = {
  "Type": "Dummy",
  "Max": 0,
  "Min": 0,
  "IsBackendValidation": false,
  "ValidationUrl": null,
  "ShowServerError": false,
  "ErrorTargetField": null,
  "ValidationParams": null,
  "ErrorMessage": "Dummy validation message",
  "ErrorMessageKey": "error_dummy",
  "RequiredOnlyWhen": null,
  "Regex": null
}

const getDummyValidation = () => {
  return [JSON.parse(JSON.stringify(dummyValidation))]
}

describe('TextArea.vue', () => {
  it('renders props.label when passed', () => {
    const label = 'new label'
    const wrapper = shallowMount(TextArea, {
      store: getStore(),
      localVue,
      propsData: { label },
      mocks: {
        $t: (key) => key
      }
    })
    expect(wrapper.text()).toMatch(label)
  })
})

describe('TextArea.vue', () => {
  it('display isRequiredErrorMsg when props.isRequired is passed', async () => {
    const isRequired = true
    const label = 'new label'
    const wrapper = mount(TextArea, {
      store:getStoreWithActions ({
                getFormData: async () => (
                  Promise.resolve(
                    formData
                  )
                )
      }),
      localVue,
      propsData: { isRequired, label },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises()
    expect(wrapper.find('.error-message').text()).toMatch(isRequiredErrorMsg)
  })
})

describe('TextArea.vue', () => {
  it('removes isRequiredErrorMsg when data is entered', async () => {
    const isRequired = true
    const label = 'new label'
    const wrapper = shallowMount(TextArea, {
      store: getStore(),
      localVue,
      propsData: { isRequired, label },
      mocks: {
        $t: (key) => key
      }
    })
    expect(wrapper.find('p.error-message').text()).toMatch(isRequiredErrorMsg)
    const textInput = wrapper.find('textarea')
    await textInput.setValue('new value')
    expect(wrapper.find('.error-container').exists()).toBe(false)
  })
})

describe('TextArea.vue', () => {
  it('display back the isRequiredErrorMsg when data is removed', async () => {
    const isRequired = true
    const label = 'new label'
    const wrapper = shallowMount(TextArea, {
      store: getStore(),
      localVue,
      propsData: { isRequired, label },
      mocks: {
        $t: (key) => key
      }
    })
    expect(wrapper.find('p.error-message').text()).toMatch(isRequiredErrorMsg)
    const textInput = wrapper.find('textarea')
    await textInput.setValue('new value')
    await textInput.setValue('')
    expect(wrapper.find('p.error-message').text()).toMatch(isRequiredErrorMsg)
  })
})

describe('TextArea.vue', () => {
  it('check if validation is invoked on value change and error message displayed', async () => {
    const label = 'new_label'
    const formData = getNewForm()
    const dummyValidation = getDummyValidation()
    formData['Data']['Form']['Fields'][0]['Label'] = label
    formData['Data']['Form']['Fields'][0]['Validations']['And'] = dummyValidation
    const negativeDummy = (_) => false
    const wrapper = mount(Form, {
      store: getStoreWithActions({
        getFormData: async () => (
          Promise.resolve(
            formData
          )
        )
      }),
      localVue,
      propsData: { isEditing: true, name: 'DummyForm' },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises() 
    await wrapper.findComponent(TextArea).setData({validationMapper: {Dummy:negativeDummy},localValue:'abc'})
    await wrapper.findComponent(TextArea).find('textarea').trigger('change')
    wrapper.findComponent(TextArea).vm.$emit('input','a')
    await wrapper.findComponent(TextArea).vm.$nextTick()
    expect(wrapper.findComponent(TextArea).find('.error-message').text()).toMatch(dummyValidation[0]['ErrorMessage'])
  })
})

describe('TextArea.vue', () => {
  it('check if validation is invoked on value change and error message not displayed if validation return true', async () => {
    const label = 'new_label'
    const formData = getNewForm()
    const dummyValidation = getDummyValidation()
    formData['Data']['Form']['Fields'][0]['Label'] = label
    formData['Data']['Form']['Fields'][0]['Validations']['And'] = dummyValidation
    const postiveDummy = (_) => true
    const wrapper = mount(Form, {
      store: getStoreWithActions ({
        getFormData: async () => (
          Promise.resolve(
            formData
          )
        )
      }),
      localVue,
      propsData: { isEditing: true, name: 'DummyForm' },
      mocks: {
        $t: (key) => key
      }
    })
    await flushPromises() 
    await wrapper.findComponent(TextArea).setData({validationMapper: {Dummy:postiveDummy},localValue:'abc'})
    await wrapper.findComponent(TextArea).find('textarea').trigger('change')
    expect(wrapper.findComponent(TextArea).find('.error-message').exists()).toBeFalsy()
  })
})

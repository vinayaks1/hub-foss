import { mount, createLocalVue } from '@vue/test-utils'
import AddStaffDetails from '../../src/app/Pages/dashboard/StaffDetails/AddStaffDetails'
import Button from '@/app/shared/components/Button.vue'
import Vuex from 'vuex'

const localVue = createLocalVue()
localVue.use(Vuex)
const $t = () => {}
const staffObj = {
    "ID": 49692,
    "Name": "test3",
    "Designation": "test3",
    "PrimaryNumber": "9799778",
    "SecondaryNumber": null,
    "EmailAddress": null,
    "AlertFrequency": "NONE",
    "ReceiveRNTCPAdditionUpdate": false,
    "CanReceiveRNTCPAdditionUpdate": false,
    "CanReceiveARTAdditionUpdate": false,
    "CanChoosePatients": true,
    "LinkSomePatients": true,
    "CountryCode": "+91",
    "alertFrequencyRadioGroup": [
        {
            "Value": "None",
            "Key": "NONE"
        },
        {
            "Value": "Weekly",
            "Key": "WEEKLY"
        }
    ],
    "hierarchyId": 268279,
    "patientList": [
        {
            "Key": 310952,
            "Value": "310952:",
            "LinkText": "John Doe",
            "Link": "/dashboard/patient/310952",
            "ExtraInfo": null
        }
    ],
    "LinkedPatientIds": [
        310952
    ],
    "CanHaveDailyAlertFrequency": false
}
describe('Staff Details Tests', () => {
    let store
    let actions
    beforeEach(() => {
      actions = {
        deleteFieldStaff: jest.fn(),
        fieldStaffDetails: jest.fn(),
        updateFieldStaff: jest.fn(),
        addFieldStaff: jest.fn(),
        setPreviousHierarchyId: jest.fn(),
        setViewOnly: jest.fn()
      }
      store = new Vuex.Store({
        modules: {
            AddStaff: {
            namespaced: true,
            state: {
                isViewOnly: false,
                addStaffObj: {},
                formloading: true,
                editMode: false,
                previousHierarchyId: 0
            },
            actions
          }
        }
      })
    })

    it('load', () => {
        const wrapper = mount(AddStaffDetails, { store, localVue, mocks: { $t } })
        wrapper.vm.$nextTick()
        global.window = Object.create(window);
        const url = "http://dummy.com";
        Object.defineProperty(window, 'location', {
            writable: true,
            value: {
            href: url,
            pathname: '/UpdateStaff/1'
            }
        });
        expect(window.location.href).toEqual(url);  
    })

    it('loaded', async () => {
        store.state.AddStaff.formloading = false
        store.state.AddStaff.addStaffObj = staffObj
        const setErrorMessage = jest.fn()
        const unSetErrorMessage = jest.fn()
        const wrapper = mount(AddStaffDetails, { store, localVue, mocks: { $t }, computed:{}, methods: { setErrorMessage: setErrorMessage, unSetErrorMessage: unSetErrorMessage }})
        wrapper.vm.$nextTick()
        global.window = Object.create(window)
        const url = "http://dummy.com";
        Object.defineProperty(window, 'location', {
            writable: true,
            value: {
            href: url,
            pathname: '/AddStaff/1'
            }
        })
        expect(window.location.href).toEqual(url)
        expect(wrapper.findComponent(Button).exists()).toBe(true)
        wrapper.findAllComponents(Button).at(0).vm.$emit('click')
        wrapper.findAllComponents(Button).at(1).vm.$emit('click')
        await wrapper.setData({ title: 'Add Staff', Name: 'test', Designation: 'Designation', PrimaryNumber: '123456789', updatedPatientsLink: true })
        wrapper.findAllComponents(Button).at(1).vm.$emit('click')
        await wrapper.setData({ title: 'UpdateStaff', Name: 'test', Designation: 'Designation', PrimaryNumber: '123456789' })
        wrapper.findAllComponents(Button).at(1).vm.$emit('click')
        let localThis = { updatedPatientsLink: false }
        expect(AddStaffDetails.computed.showTable.call(localThis)).toBe(false)
        localThis = { updatedPatientsLink: true }
        expect(AddStaffDetails.computed.showTable.call(localThis)).toBe(true)
    })
})
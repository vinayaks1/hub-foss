# Unified App

The Unified Web application aims to create components for the design library and provide an interface to stitch along the different workflows of the patient management portal. The UFE Web also customises the functionality

## Install the dependencies

```bash
yarn
# or
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)

```bash
yarn serveHL
```

### Lint the files

```bash
yarn lint
```

### Build the app for production

Please ensure to add NODE_ENV=production to the configuration file before linting and serving the application with the given mode

### Customisation 

For re-using the Hub UI but changing the default styling use the _hubvariables.scss file 
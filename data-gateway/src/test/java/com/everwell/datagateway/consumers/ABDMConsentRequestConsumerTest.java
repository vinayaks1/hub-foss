package com.everwell.datagateway.consumers;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.service.ABDMHelperService;
import com.everwell.datagateway.utils.CacheUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import static com.everwell.datagateway.constants.QueueConstants.ABDM_LINK_CARE_CONTEXT_QUEUE;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

public class ABDMConsentRequestConsumerTest extends BaseTest {

    @Spy
    @InjectMocks
    ABDMConsentRequestConsumer abdmConsentRequestConsumer;

    @Mock
    ABDMHelperService abdmHelper;

    @InjectMocks
    CacheUtils cacheUtils;

    @Mock
    RedisTemplate<String, Object> redisTemplate;

    @Mock
    private ValueOperations valueOperations;

    @BeforeEach
    public void init() {
        cacheUtils = new CacheUtils(redisTemplate);
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
    }


    @Test
    void consume() {
        String input = "{\"userId\":299803,\"abdmConsentInitiationRequest\":{\"requestId\":\"93fef6d4-7f45-4b4f-a5fb-ddfdd0b5b3e2\",\"timestamp\":\"2023-02-21T12:58:05.025000\",\"consent\":{\"purpose\":{\"text\":\"Self requested\",\"code\":\"CAREMGT\",\"refUri\":\"string\"},\"patient\":{\"id\":\"91187171704708@sbx\"},\"hiu\":{\"id\":\"NIKSHAY_HIU\"},\"requester\":{\"name\":\"NIKSHAY\",\"identifier\":{\"type\":\"DTO\",\"value\":\"324\",\"system\":\"https://nikshay.in\"}},\"hiTypes\":[\"OPConsultation\",\"Prescription\",\"DischargeSummary\",\"DiagnosticReport\",\"ImmunizationRecord\",\"HealthDocumentRecord\",\"WellnessRecord\"],\"permission\":{\"accessMode\":\"VIEW\",\"dateRange\":{\"from\":\"2022-06-21T11:05:00.442Z\",\"to\":\"2022-06-23T11:05:00.442Z\"},\"dataEraseAt\":\"2023-06-23T11:05:00.442Z\",\"frequency\":{\"unit\":\"HOUR\",\"value\":1,\"repeats\":1}}}},\"consentId\":\"19\"}";
        Message msg = MessageBuilder.withBody(input.getBytes()).build();
        abdmConsentRequestConsumer.consume(msg);
        Mockito.verify(abdmConsentRequestConsumer, Mockito.times(1)).outgoingWebhook(any(), any());
    }

    @Test
    void outgoingWebhook() {
        String input = "{\"userId\":299803,\"abdmConsentInitiationRequest\":{\"requestId\":\"93fef6d4-7f45-4b4f-a5fb-ddfdd0b5b3e2\",\"timestamp\":\"2023-02-21T12:58:05.025000\",\"consent\":{\"purpose\":{\"text\":\"Self requested\",\"code\":\"CAREMGT\",\"refUri\":\"string\"},\"patient\":{\"id\":\"91187171704708@sbx\"},\"hiu\":{\"id\":\"NIKSHAY_HIU\"},\"requester\":{\"name\":\"NIKSHAY\",\"identifier\":{\"type\":\"DTO\",\"value\":\"324\",\"system\":\"https://nikshay.in\"}},\"hiTypes\":[\"OPConsultation\",\"Prescription\",\"DischargeSummary\",\"DiagnosticReport\",\"ImmunizationRecord\",\"HealthDocumentRecord\",\"WellnessRecord\"],\"permission\":{\"accessMode\":\"VIEW\",\"dateRange\":{\"from\":\"2022-06-21T11:05:00.442Z\",\"to\":\"2022-06-23T11:05:00.442Z\"},\"dataEraseAt\":\"2023-06-23T11:05:00.442Z\",\"frequency\":{\"unit\":\"HOUR\",\"value\":1,\"repeats\":1}}}},\"consentId\":\"19\"}";

        Message msg = MessageBuilder.withBody(input.getBytes()).build();
        abdmConsentRequestConsumer.outgoingWebhook(ABDM_LINK_CARE_CONTEXT_QUEUE, msg);
        Mockito.verify(abdmHelper, Mockito.times(1)).consentRequestInit(any());
    }

}

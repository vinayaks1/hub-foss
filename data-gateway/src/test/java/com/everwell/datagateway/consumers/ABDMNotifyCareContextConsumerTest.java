package com.everwell.datagateway.consumers;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.service.ABDMIntegrationService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;

import static com.everwell.datagateway.constants.QueueConstants.ABDM_CARE_CONTEXT_NOTIFY_QUEUE;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

class ABDMNotifyCareContextConsumerTest extends BaseTest {

    @Spy
    @InjectMocks
    ABDMNotifyCareContextConsumer abdmNotifyCareContextConsumer;

    @Mock
    ABDMIntegrationService abdmHelper;

    @Test
    void consume() {
        String input = "{'episodeId':'10750687','personId':'41777','hiTypes':'DiagnosticReport','personName':'Ankur Mandal','abhaAddress':'ankur.test2@sbx'}";

        Message msg = MessageBuilder.withBody(input.getBytes()).build();
        abdmNotifyCareContextConsumer.consume(msg);
        Mockito.verify(abdmNotifyCareContextConsumer, Mockito.times(1)).outgoingWebhook(ABDM_CARE_CONTEXT_NOTIFY_QUEUE, msg);
    }

    @Test
    void outgoingWebhook() {
        String input = "{'episodeId':'10750687','personId':'41777','hiTypes':'DiagnosticReport','personName':'Ankur Mandal','abhaAddress':'ankur.test2@sbx'}";

        Message msg = MessageBuilder.withBody(input.getBytes()).build();
        abdmNotifyCareContextConsumer.outgoingWebhook(ABDM_CARE_CONTEXT_NOTIFY_QUEUE, msg);
        Mockito.verify(abdmHelper, Mockito.times(1)).notifyCareContext(any());
    }

    @Test
    void outgoingWebhookException() {
        String input = "{'episodeId':'10750687','personId':'41777','hiTypes':'DiagnosticReport','personName':'Ankur Mandal','abhaAddress':'ankur.test2@sbx'}";

        Message msg = MessageBuilder.withBody(input.getBytes()).build();
        abdmNotifyCareContextConsumer.outgoingWebhook(ABDM_CARE_CONTEXT_NOTIFY_QUEUE, null);
    }
}
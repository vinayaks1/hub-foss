package com.everwell.datagateway.consumers;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.service.EmailService;
import com.everwell.datagateway.service.SendGridEmailService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class DlxConsumerTest extends BaseTest {
    @Spy
    @InjectMocks
    DlxConsumer dlxConsumer;

    @Mock
    SendGridEmailService emailService;

    @Test
    void consume() {
        byte[] byteArray = {'E', 'R', 'e', 'f', 'I', 'd', 'R'};
        doNothing().when(emailService).sendText(any(), any(), any(), any());
        for (int i = 0; i < 100; i ++) {
            dlxConsumer.consume(new Message(byteArray, new MessageProperties()));
        }
        verify(emailService, times(1)).sendText(any(), any(), any(), any());
    }

}

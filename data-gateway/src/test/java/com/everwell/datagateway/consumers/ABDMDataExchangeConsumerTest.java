package com.everwell.datagateway.consumers;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.entities.Client;
import com.everwell.datagateway.entities.ClientRequests;
import com.everwell.datagateway.entities.Event;
import com.everwell.datagateway.entities.EventClient;
import com.everwell.datagateway.enums.ResponseTypeEnum;
import com.everwell.datagateway.exceptions.RestTemplateException;
import com.everwell.datagateway.handlers.ResponseHandlerMap;
import com.everwell.datagateway.handlers.impl.publishers.RefIdFHIRHandler;
import com.everwell.datagateway.models.dto.EventStreamingDTO;
import com.everwell.datagateway.models.response.TestReplyQueueResponse;
import com.everwell.datagateway.repositories.EventClientRepository;
import com.everwell.datagateway.service.ABDMHelperService;
import com.everwell.datagateway.service.ClientRequestsService;
import com.everwell.datagateway.utils.Utils;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.http.HttpStatus;

import java.io.IOException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class ABDMDataExchangeConsumerTest extends BaseTest {

    @InjectMocks
    ABDMDataExchangeConsumer abdmDataExchangeConsumer;

    @Mock
    ABDMHelperService abdmHelperService;

    @Mock
    ClientRequestsService clientRequestsService;

    @Mock
    EventClientRepository eventClientRepository;

    @Mock
    ResponseHandlerMap responseHandlerMap;

   @Mock
     RefIdFHIRHandler refIdFHIRHandler;


    @Test
    void consume() {
        byte[] byteArray = {'T', 'E', 'S', 'T'};
        ABDMDataExchangeConsumer abdmDataExchangeConsumer = Mockito.spy(ABDMDataExchangeConsumer.class);
        doNothing().when(abdmDataExchangeConsumer).outgoingWebhook(any(), any());
        abdmDataExchangeConsumer.consume(new Message(byteArray, new MessageProperties()));
        verify(abdmDataExchangeConsumer, times(1)).outgoingWebhook(any(), any());
    }

    @Test
    void outgoingWebhookDispensationTest() throws IOException {
        String queueResponse = getEventStreamingDtoAsString_Dispensation();
        byte[] byteArray = queueResponse.getBytes();

        mockMethods();
        when(refIdFHIRHandler.convertToResponseType(any())).thenReturn(getDispensationFHIR());
        abdmDataExchangeConsumer.outgoingWebhook("test", new Message(byteArray, new MessageProperties()));
        verify(abdmHelperService, times(1)).hipHiTransferNotify(any());
    }

    @Test
    void outgoingWebhookDiagnosticsTest() throws IOException {
        String queueResponse = getEventStreamingDtoAsString_Diagnostics();
        byte[] byteArray = queueResponse.getBytes();

        mockMethods();
        when(refIdFHIRHandler.convertToResponseType(any())).thenReturn(getDiagnosticsFHIR());
        abdmDataExchangeConsumer.outgoingWebhook("test", new Message(byteArray, new MessageProperties()));
        verify(abdmHelperService, times(1)).hipHiTransferNotify(any());
    }

    @Test
    void outgoingWebhookDiagnosticsTest_failureTest() throws IOException {
        String queueResponse = getEventStreamingDtoAsString_successFalse();
        byte[] byteArray = queueResponse.getBytes();

        ClientRequests clientRequests = new ClientRequests();

        doNothing().when(abdmHelperService).hipOnRequestCall(any());
        when(clientRequestsService.getClientRequestsById(any())).thenReturn(clientRequests);
        when(clientRequestsService.updateClientRequest(any())).thenReturn(1L);
        abdmDataExchangeConsumer.outgoingWebhook("test", new Message(byteArray, new MessageProperties()));
        verify(clientRequestsService, times(1)).updateClientRequest(any());
    }

    String getEventStreamingDtoAsString_Dispensation() {
        EventStreamingDTO eventStreamingDTO = new EventStreamingDTO();
        TestReplyQueueResponse testReplyQueueResponse = new TestReplyQueueResponse();
        testReplyQueueResponse.setResponse("{\"dataPullRequest\":{\"transactionId\":\"c963f8a3-291d-4bbd-8ac6-3666168dbd5f\",\"requestId\":\"1c0bd239-b80b-4a3c-ae26-0bb96fea5fdd\",\"timestamp\":\"1659353068580\",\"hiRequest\":{\"consent\":{\"id\":\"bea1f9aa-9690-4ea9-b0e0-f6f40ca580e6\"},\"dateRange\":{\"from\":\"2015-08-01T11:22:58.028985\",\"to\":\"2022-08-01T11:22:58.028993\"},\"dataPushUrl\":\"https://dev.abdm.gov.in/patient-hiu/data/notification\",\"keyMaterial\":{\"cryptoAlg\":\"ECDH\",\"curve\":\"curve25519\",\"dhPublicKey\":{\"expiry\":\"2022-08-03T11:22:58.277345\",\"parameters\":\"Ephemeral public key\",\"keyValue\":\"BEwSJfcuPvm3g9dAZ0FVAg1XpIEW0ma/biZjewDnXVe2UrNFeqXtW2e+NRoDUQgK2/VX9tnO4znmmwmyp2C8w8E=\"},\"nonce\":\"GkbZHdGdKj7yGdW+FjV9KNqgMVm5MU2bmFVJGGYmiRQ=\"}}},\"episodeMap\":{\"10750687\":[{\"ProfileType\":\"Dispensation\",\"Response\":\"{\\\"EpisodeId\\\":10750687,\\\"HierarchyId\\\":287306,\\\"UserId\\\":7945,\\\"HierarchyName\\\":\\\"Hukkerikar nagar\\\",\\\"DispensationMap\\\":{\\\"6577\\\":{\\\"Active\\\":false,\\\"ProductName\\\":\\\"3FDC CP (A)  (H75,R150 & E275),3FDC(P) (H50, R75, Z150)\\\",\\\"TimeStamp\\\":\\\"2022-07-14\\\"},\\\"6576\\\":{\\\"Active\\\":false,\\\"ProductName\\\":\\\"2FDC (P)  (H50 & R75)\\\",\\\"TimeStamp\\\":\\\"2022-07-14\\\"}}}\",\"Success\":true}]},\"errorMessage\":null}");
        testReplyQueueResponse.setSuccess("True");
        eventStreamingDTO.setEventName("hip-data-exchange");
        eventStreamingDTO.setField(testReplyQueueResponse);
        return Utils.asJsonString(eventStreamingDTO);
    }

    String getDispensationFHIR() {
        return "{\"Success\":true,\"Data\":{\"RefId\":null,\"Response\":\"{\\\"resourceType\\\":\\\"Bundle\\\",\\\"id\\\":\\\"prescription-bundle-10750687\\\",\\\"meta\\\":{\\\"versionId\\\":\\\"1\\\",\\\"lastUpdated\\\":\\\"2020-07-09T15:32:26.605+05:30\\\",\\\"profile\\\":[\\\"https://nrces.in/ndhm/fhir/r4/StructureDefinition/DocumentBundle\\\"],\\\"security\\\":[{\\\"system\\\":\\\"http://terminology.hl7.org/CodeSystem/v3-Confidentiality\\\",\\\"code\\\":\\\"V\\\",\\\"display\\\":\\\"very restricted\\\"}]},\\\"identifier\\\":{\\\"system\\\":\\\"http://hip.in\\\",\\\"value\\\":\\\"bc3c6c57-2053-4d0e-ac40-139ccccff645\\\"},\\\"type\\\":\\\"document\\\",\\\"timestamp\\\":\\\"2022-08-03T23:38:44.883+05:30\\\",\\\"entry\\\":[{\\\"fullUrl\\\":\\\"Composition/Composition-01\\\",\\\"resource\\\":{\\\"resourceType\\\":\\\"Composition\\\",\\\"id\\\":\\\"Composition-10750687\\\",\\\"meta\\\":{\\\"versionId\\\":\\\"1\\\",\\\"lastUpdated\\\":\\\"2022-08-03T23:38:44.883+05:30\\\",\\\"profile\\\":[\\\"https://nrces.in/ndhm/fhir/r4/StructureDefinition/PrescriptionRecord\\\"]},\\\"language\\\":\\\"en-IN\\\",\\\"identifier\\\":{\\\"system\\\":\\\"https://ndhm.in/phr\\\",\\\"value\\\":\\\"645bb0c3-ff7e-4123-bef5-3852a4784813\\\"},\\\"status\\\":\\\"final\\\",\\\"type\\\":{\\\"coding\\\":[{\\\"system\\\":\\\"http://snomed.info/sct\\\",\\\"code\\\":\\\"440545006\\\",\\\"display\\\":\\\"Prescription record\\\"}]},\\\"subject\\\":{\\\"reference\\\":\\\"Patient/10750687\\\"},\\\"date\\\":\\\"2022-08-03T23:38:44.883+05:30\\\",\\\"author\\\":[{\\\"reference\\\":\\\"Organization/287306\\\"}],\\\"title\\\":\\\"Prescription record\\\",\\\"section\\\":[{\\\"entry\\\":[{\\\"reference\\\":\\\"MedicationRequest/6577\\\",\\\"type\\\":\\\"MedicationRequest\\\"},{\\\"reference\\\":\\\"MedicationRequest/6576\\\",\\\"type\\\":\\\"MedicationRequest\\\"}]}]}},{\\\"fullUrl\\\":\\\"Patient/10750687\\\",\\\"resource\\\":{\\\"resourceType\\\":\\\"Patient\\\",\\\"id\\\":\\\"10750687\\\",\\\"meta\\\":{\\\"versionId\\\":\\\"1\\\",\\\"lastUpdated\\\":\\\"2022-08-03T23:38:45.007+05:30\\\",\\\"profile\\\":[\\\"https://nrces.in/ndhm/fhir/r4/StructureDefinition/Patient\\\"]},\\\"identifier\\\":[{\\\"type\\\":{\\\"text\\\":\\\"Patient\\\"},\\\"value\\\":\\\"10750687\\\"}]}},{\\\"fullUrl\\\":\\\"Organization/287306\\\",\\\"resource\\\":{\\\"resourceType\\\":\\\"Organization\\\",\\\"id\\\":\\\"287306\\\",\\\"meta\\\":{\\\"profile\\\":[\\\"https://nrces.in/ndhm/fhir/r4/StructureDefinition/Organization\\\"]},\\\"identifier\\\":[{\\\"type\\\":{\\\"text\\\":\\\"Hierarchy\\\"},\\\"system\\\":\\\"https://nikshay.in\\\",\\\"value\\\":\\\"287306\\\"}],\\\"name\\\":\\\"Hukkerikar nagar\\\"}},{\\\"fullUrl\\\":\\\"MedicationRequest/6577\\\",\\\"resource\\\":{\\\"resourceType\\\":\\\"MedicationRequest\\\",\\\"id\\\":\\\"6577\\\",\\\"meta\\\":{\\\"profile\\\":[\\\"https://nrces.in/ndhm/fhir/r4/StructureDefinition/MedicationRequest\\\"]},\\\"status\\\":\\\"completed\\\",\\\"intent\\\":\\\"original-order\\\",\\\"medicationCodeableConcept\\\":{\\\"text\\\":\\\"3FDC CP (A)  (H75,R150 & E275),3FDC(P) (H50, R75, Z150)\\\"},\\\"subject\\\":{\\\"reference\\\":\\\"Patient/10750687\\\"},\\\"authoredOn\\\":\\\"2022-07-14\\\",\\\"requester\\\":{\\\"reference\\\":\\\"Organization/287306\\\"},\\\"dosageInstruction\\\":[{\\\"text\\\":\\\"NA\\\"}]}},{\\\"fullUrl\\\":\\\"MedicationRequest/6576\\\",\\\"resource\\\":{\\\"resourceType\\\":\\\"MedicationRequest\\\",\\\"id\\\":\\\"6576\\\",\\\"meta\\\":{\\\"profile\\\":[\\\"https://nrces.in/ndhm/fhir/r4/StructureDefinition/MedicationRequest\\\"]},\\\"status\\\":\\\"completed\\\",\\\"intent\\\":\\\"original-order\\\",\\\"medicationCodeableConcept\\\":{\\\"text\\\":\\\"2FDC (P)  (H50 & R75)\\\"},\\\"subject\\\":{\\\"reference\\\":\\\"Patient/10750687\\\"},\\\"authoredOn\\\":\\\"2022-07-14\\\",\\\"requester\\\":{\\\"reference\\\":\\\"Organization/287306\\\"},\\\"dosageInstruction\\\":[{\\\"text\\\":\\\"NA\\\"}]}}]}\"}}";
    }

    String getEventStreamingDtoAsString_Diagnostics() {
        EventStreamingDTO eventStreamingDTO = new EventStreamingDTO();
        TestReplyQueueResponse testReplyQueueResponse = new TestReplyQueueResponse();
        testReplyQueueResponse.setResponse("{\"dataPullRequest\":{\"transactionId\":\"c963f8a3-291d-4bbd-8ac6-3666168dbd5f\",\"requestId\":\"1c0bd239-b80b-4a3c-ae26-0bb96fea5fdd\",\"timestamp\":\"1659353068580\",\"hiRequest\":{\"consent\":{\"id\":\"bea1f9aa-9690-4ea9-b0e0-f6f40ca580e6\"},\"dateRange\":{\"from\":\"2015-08-01T11:22:58.028985\",\"to\":\"2022-08-01T11:22:58.028993\"},\"dataPushUrl\":\"https://dev.abdm.gov.in/patient-hiu/data/notification\",\"keyMaterial\":{\"cryptoAlg\":\"ECDH\",\"curve\":\"curve25519\",\"dhPublicKey\":{\"expiry\":\"2022-08-03T11:22:58.277345\",\"parameters\":\"Ephemeral public key\",\"keyValue\":\"BEwSJfcuPvm3g9dAZ0FVAg1XpIEW0ma/biZjewDnXVe2UrNFeqXtW2e+NRoDUQgK2/VX9tnO4znmmwmyp2C8w8E=\"},\"nonce\":\"GkbZHdGdKj7yGdW+FjV9KNqgMVm5MU2bmFVJGGYmiRQ=\"}}},\"episodeMap\":{\"10750687\":[{\"ProfileType\":\"Diagnostics\",\"Response\":\"{\\\"EpisodeId\\\":10750687,\\\"HierarchyId\\\":287306,\\\"UserId\\\":7945,\\\"HierarchyName\\\":\\\"Hukkerikar nagar\\\",\\\"TestResultMap\\\":{\\\"93930\\\":{\\\"TestType\\\":\\\"Microscopy ZN\\\",\\\"FinalInterpretation\\\":\\\"Positive\\\"}}}\",\"Success\":true}]},\"errorMessage\":null}");
        testReplyQueueResponse.setSuccess("True");
        eventStreamingDTO.setEventName("hip-data-exchange");
        eventStreamingDTO.setField(testReplyQueueResponse);
        return Utils.asJsonString(eventStreamingDTO);
    }

    String getDiagnosticsFHIR() {
        return "{\"Success\":true,\"Data\":{\"RefId\":null,\"Response\":\"[\\\"{\\\\\\\"resourceType\\\\\\\":\\\\\\\"Bundle\\\\\\\",\\\\\\\"id\\\\\\\":\\\\\\\"DiagnosticReport-93930\\\\\\\",\\\\\\\"meta\\\\\\\":{\\\\\\\"versionId\\\\\\\":\\\\\\\"1\\\\\\\",\\\\\\\"lastUpdated\\\\\\\":\\\\\\\"2022-08-03T23:38:44.883+05:30\\\\\\\",\\\\\\\"profile\\\\\\\":[\\\\\\\"https://nrces.in/ndhm/fhir/r4/StructureDefinition/DocumentBundle\\\\\\\"],\\\\\\\"security\\\\\\\":[{\\\\\\\"system\\\\\\\":\\\\\\\"http://terminology.hl7.org/CodeSystem/v3-Confidentiality\\\\\\\",\\\\\\\"code\\\\\\\":\\\\\\\"V\\\\\\\",\\\\\\\"display\\\\\\\":\\\\\\\"very restricted\\\\\\\"}]},\\\\\\\"identifier\\\\\\\":{\\\\\\\"system\\\\\\\":\\\\\\\"http://hip.in\\\\\\\",\\\\\\\"value\\\\\\\":\\\\\\\"3cf54fc4-0178-4127-bb99-b20711404881\\\\\\\"},\\\\\\\"type\\\\\\\":\\\\\\\"document\\\\\\\",\\\\\\\"timestamp\\\\\\\":\\\\\\\"2022-08-03T23:38:44.883+05:30\\\\\\\",\\\\\\\"entry\\\\\\\":[{\\\\\\\"fullUrl\\\\\\\":\\\\\\\"Composition/10750687\\\\\\\",\\\\\\\"resource\\\\\\\":{\\\\\\\"resourceType\\\\\\\":\\\\\\\"Composition\\\\\\\",\\\\\\\"id\\\\\\\":\\\\\\\"Composition-93930\\\\\\\",\\\\\\\"meta\\\\\\\":{\\\\\\\"versionId\\\\\\\":\\\\\\\"1\\\\\\\",\\\\\\\"lastUpdated\\\\\\\":\\\\\\\"2022-08-03T23:38:44.883+05:30\\\\\\\",\\\\\\\"profile\\\\\\\":[\\\\\\\"https://nrces.in/ndhm/fhir/r4/StructureDefinition/DiagnosticReportRecord\\\\\\\"]},\\\\\\\"language\\\\\\\":\\\\\\\"en-IN\\\\\\\",\\\\\\\"identifier\\\\\\\":{\\\\\\\"system\\\\\\\":\\\\\\\"https://ndhm.in/phr\\\\\\\",\\\\\\\"value\\\\\\\":\\\\\\\"645bb0c3-ff7e-4123-bef5-3852a4784813\\\\\\\"},\\\\\\\"status\\\\\\\":\\\\\\\"final\\\\\\\",\\\\\\\"type\\\\\\\":{\\\\\\\"coding\\\\\\\":[{\\\\\\\"system\\\\\\\":\\\\\\\"http://snomed.info/sct\\\\\\\",\\\\\\\"code\\\\\\\":\\\\\\\"721981007\\\\\\\",\\\\\\\"display\\\\\\\":\\\\\\\"Diagnostic studies report\\\\\\\"}],\\\\\\\"text\\\\\\\":\\\\\\\"Diagnostic Report- Lab\\\\\\\"},\\\\\\\"subject\\\\\\\":{\\\\\\\"reference\\\\\\\":\\\\\\\"Patient/10750687\\\\\\\"},\\\\\\\"date\\\\\\\":\\\\\\\"2022-08-03T23:38:44.883+05:30\\\\\\\",\\\\\\\"author\\\\\\\":[{\\\\\\\"reference\\\\\\\":\\\\\\\"Practitioner/7945\\\\\\\"}],\\\\\\\"title\\\\\\\":\\\\\\\"Diagnostic Report- Lab\\\\\\\",\\\\\\\"section\\\\\\\":[{\\\\\\\"entry\\\\\\\":[{\\\\\\\"reference\\\\\\\":\\\\\\\"DiagnosticReport/93930\\\\\\\",\\\\\\\"type\\\\\\\":\\\\\\\"DiagnosticReport\\\\\\\"}]}]}},{\\\\\\\"fullUrl\\\\\\\":\\\\\\\"Patient/10750687\\\\\\\",\\\\\\\"resource\\\\\\\":{\\\\\\\"resourceType\\\\\\\":\\\\\\\"Patient\\\\\\\",\\\\\\\"id\\\\\\\":\\\\\\\"10750687\\\\\\\",\\\\\\\"meta\\\\\\\":{\\\\\\\"versionId\\\\\\\":\\\\\\\"1\\\\\\\",\\\\\\\"lastUpdated\\\\\\\":\\\\\\\"2022-08-03T23:38:45.007+05:30\\\\\\\",\\\\\\\"profile\\\\\\\":[\\\\\\\"https://nrces.in/ndhm/fhir/r4/StructureDefinition/Patient\\\\\\\"]},\\\\\\\"identifier\\\\\\\":[{\\\\\\\"type\\\\\\\":{\\\\\\\"text\\\\\\\":\\\\\\\"Patient\\\\\\\"},\\\\\\\"value\\\\\\\":\\\\\\\"10750687\\\\\\\"}]}},{\\\\\\\"fullUrl\\\\\\\":\\\\\\\"Practitioner/7945\\\\\\\",\\\\\\\"resource\\\\\\\":{\\\\\\\"resourceType\\\\\\\":\\\\\\\"Practitioner\\\\\\\",\\\\\\\"id\\\\\\\":\\\\\\\"7945\\\\\\\",\\\\\\\"meta\\\\\\\":{\\\\\\\"versionId\\\\\\\":\\\\\\\"1\\\\\\\",\\\\\\\"lastUpdated\\\\\\\":\\\\\\\"2022-08-03T23:38:45.007+05:30\\\\\\\",\\\\\\\"profile\\\\\\\":[\\\\\\\"https://nrces.in/ndhm/fhir/r4/StructureDefinition/Practitioner\\\\\\\"]},\\\\\\\"identifier\\\\\\\":[{\\\\\\\"type\\\\\\\":{\\\\\\\"text\\\\\\\":\\\\\\\"Practitioner \\\\\\\"},\\\\\\\"value\\\\\\\":\\\\\\\"7945\\\\\\\"}]}},{\\\\\\\"fullUrl\\\\\\\":\\\\\\\"DiagnosticReport/93930\\\\\\\",\\\\\\\"resource\\\\\\\":{\\\\\\\"resourceType\\\\\\\":\\\\\\\"DiagnosticReport\\\\\\\",\\\\\\\"id\\\\\\\":\\\\\\\"93930\\\\\\\",\\\\\\\"meta\\\\\\\":{\\\\\\\"versionId\\\\\\\":\\\\\\\"1\\\\\\\",\\\\\\\"lastUpdated\\\\\\\":\\\\\\\"2022-08-03T23:38:45.007+05:30\\\\\\\",\\\\\\\"profile\\\\\\\":[\\\\\\\"https://nrces.in/ndhm/fhir/r4/StructureDefinition/DiagnosticReportLab\\\\\\\"]},\\\\\\\"status\\\\\\\":\\\\\\\"final\\\\\\\",\\\\\\\"code\\\\\\\":{\\\\\\\"text\\\\\\\":\\\\\\\"Microscopy ZN\\\\\\\"},\\\\\\\"resultsInterpreter\\\\\\\":[{\\\\\\\"reference\\\\\\\":\\\\\\\"Practitioner/7945\\\\\\\"}],\\\\\\\"result\\\\\\\":[{\\\\\\\"reference\\\\\\\":\\\\\\\"Observation/93930\\\\\\\"}],\\\\\\\"conclusion\\\\\\\":\\\\\\\"Positive\\\\\\\"}},{\\\\\\\"fullUrl\\\\\\\":\\\\\\\"Observation/93930\\\\\\\",\\\\\\\"resource\\\\\\\":{\\\\\\\"resourceType\\\\\\\":\\\\\\\"Observation\\\\\\\",\\\\\\\"id\\\\\\\":\\\\\\\"93930\\\\\\\",\\\\\\\"meta\\\\\\\":{\\\\\\\"profile\\\\\\\":[\\\\\\\"https://nrces.in/ndhm/fhir/r4/StructureDefinition/Observation\\\\\\\"]},\\\\\\\"status\\\\\\\":\\\\\\\"final\\\\\\\",\\\\\\\"code\\\\\\\":{\\\\\\\"text\\\\\\\":\\\\\\\"Positive\\\\\\\"},\\\\\\\"subject\\\\\\\":{\\\\\\\"reference\\\\\\\":\\\\\\\"Patient/10750687\\\\\\\"},\\\\\\\"performer\\\\\\\":[{\\\\\\\"reference\\\\\\\":\\\\\\\"Practitioner/7945\\\\\\\"}]}}]}\\\"]\"}}";
    }

    void mockMethods() {
        ClientRequests clientRequests = new ClientRequests();
        Client client = new Client();
        Event event = new Event();
        EventClient eventClient = new EventClient();

        // set event, client details
        client.setId(1L);
        event.setId(1L);
        clientRequests.setClient(client);
        clientRequests.setEvent(event);
        eventClient.setClientId(1L);
        eventClient.setEventId(1L);
        eventClient.setResponseType(ResponseTypeEnum.FHIR_RESPONSE.toString());

        doNothing().when(abdmHelperService).hipOnRequestCall(any());
        when(clientRequestsService.getClientRequestsById(any())).thenReturn(clientRequests);
        when(eventClientRepository.findByClientIdAndEventId(any(),any())).thenReturn(eventClient);
        when(responseHandlerMap.getResponseHandler(any())).thenReturn(refIdFHIRHandler);
        when(clientRequestsService.updateClientRequest(any())).thenReturn(1L);
        when(abdmHelperService.pushToCallBackUrl(any(),any())).thenReturn(true);
        doNothing().when(clientRequestsService).setIsDeliveredTrueAndDeliveredAtTime(any(),any());
        doNothing().when(abdmHelperService).hipHiTransferNotify(any());
    }

    String getEventStreamingDtoAsString_successFalse() {
        EventStreamingDTO eventStreamingDTO = new EventStreamingDTO();
        TestReplyQueueResponse testReplyQueueResponse = new TestReplyQueueResponse();
        testReplyQueueResponse.setResponse("{\"dataPullRequest\":{\"transactionId\":\"c963f8a3-291d-4bbd-8ac6-3666168dbd5f\",\"requestId\":\"1c0bd239-b80b-4a3c-ae26-0bb96fea5fdd\",\"timestamp\":\"1659353068580\",\"hiRequest\":{\"consent\":{\"id\":\"bea1f9aa-9690-4ea9-b0e0-f6f40ca580e6\"},\"dateRange\":{\"from\":\"2015-08-01T11:22:58.028985\",\"to\":\"2022-08-01T11:22:58.028993\"},\"dataPushUrl\":\"https://dev.abdm.gov.in/patient-hiu/data/notification\",\"keyMaterial\":{\"cryptoAlg\":\"ECDH\",\"curve\":\"curve25519\",\"dhPublicKey\":{\"expiry\":\"2022-08-03T11:22:58.277345\",\"parameters\":\"Ephemeral public key\",\"keyValue\":\"BEwSJfcuPvm3g9dAZ0FVAg1XpIEW0ma/biZjewDnXVe2UrNFeqXtW2e+NRoDUQgK2/VX9tnO4znmmwmyp2C8w8E=\"},\"nonce\":\"GkbZHdGdKj7yGdW+FjV9KNqgMVm5MU2bmFVJGGYmiRQ=\"}}},\"episodeMap\":{\"10750687\":[{\"ProfileType\":\"Diagnostics\",\"Response\":\"{\\\"EpisodeId\\\":10750687,\\\"HierarchyId\\\":287306,\\\"UserId\\\":7945,\\\"HierarchyName\\\":\\\"Hukkerikar nagar\\\",\\\"TestResultMap\\\":{\\\"93930\\\":{\\\"TestType\\\":\\\"Microscopy ZN\\\",\\\"FinalInterpretation\\\":\\\"Positive\\\"}}}\",\"Success\":true}]},\"errorMessage\":null}");
        testReplyQueueResponse.setSuccess("False");
        eventStreamingDTO.setEventName("hip-data-exchange");
        eventStreamingDTO.setField(testReplyQueueResponse);
        return Utils.asJsonString(eventStreamingDTO);
    }

    @Test
    void outgoingWebhookRestTemplateException() {
        String queueResponse = getEventStreamingDtoAsString_Dispensation();
        byte[] byteArray = queueResponse.getBytes();
        doThrow(new RestTemplateException(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase())).when(abdmHelperService).hipOnRequestCall(any());
        abdmDataExchangeConsumer.outgoingWebhook("test", new Message(byteArray, new MessageProperties()));
        verify(clientRequestsService, times(0)).updateClientRequest(any());
    }

    @Test
    void outgoingWebhookException() {
        String queueResponse = getEventStreamingDtoAsString_Dispensation();
        byte[] byteArray = queueResponse.getBytes();
        doNothing().when(abdmHelperService).hipOnRequestCall(any());
        when(clientRequestsService.getClientRequestsById(any())).thenReturn(new ClientRequests());
        abdmDataExchangeConsumer.outgoingWebhook("test", new Message(byteArray, new MessageProperties()));
        verify(clientRequestsService, times(0)).updateClientRequest(any());
    }
}

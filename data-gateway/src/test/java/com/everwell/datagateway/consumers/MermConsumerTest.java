package com.everwell.datagateway.consumers;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.exceptions.NotFoundException;
import com.everwell.datagateway.exceptions.URLDisabledException;
import com.everwell.datagateway.models.dto.PublishToSubscriberUrlDTO;
import com.everwell.datagateway.service.ConsumerService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;


import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class MermConsumerTest extends BaseTest {

    @InjectMocks
    MermConsumer mermConsumer;

    @Mock
    ConsumerService consumerService;

    @Test
    void consume() {
        byte[] byteArray = {'T', 'E', 'S', 'T'};
        MermConsumer mermConsumer = Mockito.spy(MermConsumer.class);
        doNothing().when(mermConsumer).outgoingWebhook(any(), any());
        mermConsumer.consume(new Message(byteArray, new MessageProperties()));
        verify(mermConsumer, times(1)).outgoingWebhook(any(), any());
    }

    @Test
    void outgoingWebhook() {
        String request = "{\"eventName\":\"Heartbeat\",\"field\":{\"success\":true,\"message\":\"OK,2022-12-11T10:06:36,22\",\"entityId\":null,\"query\":{\"@\":\"01\",\"TT\":\"G\",\"DT\":\"2\",\"TU\":\"140822092552\",\"B\":\"4400\",\"S\":\"14\",\"T\":\"160822092552\",\"SI\":\"0\",\"V\":\"03.13\",\"CN\":\"10810\",\"SN\":\"861833048479663\"}}}";
        doNothing().when(consumerService).publishToSubscriberUrl(any(PublishToSubscriberUrlDTO.class));
        mermConsumer.outgoingWebhook("q.dg.share_merm_event", new Message(request.getBytes(), new MessageProperties()));
        verify(consumerService, times(1)).publishToSubscriberUrl(any(PublishToSubscriberUrlDTO.class));
    }

    @Test
    void outgoingWebhookNotFoundException() {
        String request = "{\"eventName\":\"Heartbeat\",\"field\":{\"success\":true,\"message\":\"OK,2022-12-11T10:06:36,22\",\"entityId\":null,\"query\":{\"@\":\"01\",\"TT\":\"G\",\"DT\":\"2\",\"TU\":\"140822092552\",\"B\":\"4400\",\"S\":\"14\",\"T\":\"160822092552\",\"SI\":\"0\",\"V\":\"03.13\",\"CN\":\"10810\",\"SN\":\"861833048479663\"}}}";
        doThrow(NotFoundException.class).when(consumerService).publishToSubscriberUrl(any(PublishToSubscriberUrlDTO.class));
        mermConsumer.outgoingWebhook("test", new Message(request.getBytes(), new MessageProperties()));
        verify(consumerService, times(1)).publishToSubscriberUrl(any(PublishToSubscriberUrlDTO.class));
    }

    @Test
    void outgoingWebhookUrlDisabledException() {
        String request = "{\"eventName\":\"Heartbeat\",\"field\":{\"success\":true,\"message\":\"OK,2022-12-11T10:06:36,22\",\"entityId\":null,\"query\":{\"@\":\"01\",\"TT\":\"G\",\"DT\":\"2\",\"TU\":\"140822092552\",\"B\":\"4400\",\"S\":\"14\",\"T\":\"160822092552\",\"SI\":\"0\",\"V\":\"03.13\",\"CN\":\"10810\",\"SN\":\"861833048479663\"}}}";
        doThrow(URLDisabledException.class).when(consumerService).publishToSubscriberUrl(any(PublishToSubscriberUrlDTO.class));
        mermConsumer.outgoingWebhook("test", new Message(request.getBytes(), new MessageProperties()));
        verify(consumerService, times(1)).publishToSubscriberUrl(any(PublishToSubscriberUrlDTO.class));
    }
}

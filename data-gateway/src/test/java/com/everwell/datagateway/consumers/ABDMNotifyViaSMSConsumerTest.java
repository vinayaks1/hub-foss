package com.everwell.datagateway.consumers;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.apiHelpers.ABDMHelper;
import com.everwell.datagateway.exceptions.NotFoundException;
import com.everwell.datagateway.exceptions.URLDisabledException;
import com.everwell.datagateway.models.request.abdm.ABDMNotifyViaSMSRequest;
import com.everwell.datagateway.models.response.RestServiceResponse;
import com.everwell.datagateway.service.ConsumerService;
import com.everwell.datagateway.utils.Utils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class ABDMNotifyViaSMSConsumerTest extends BaseTest {
    @InjectMocks
    ABDMNotifyViaSMSConsumer abdmNotifyViaSMSConsumer;

    @Mock
    ABDMHelper abdmHelperHelper;

    @Test
    void consume() {
        byte[] byteArray = {'T', 'E', 'S', 'T'};
        ABDMNotifyViaSMSConsumer abdmNotifyViaSMSConsumer = Mockito.spy(ABDMNotifyViaSMSConsumer.class);
        doNothing().when(abdmNotifyViaSMSConsumer).outgoingWebhook(any(), any());
        abdmNotifyViaSMSConsumer.consume(new Message(byteArray, new MessageProperties()));
        verify(abdmNotifyViaSMSConsumer, times(1)).outgoingWebhook(any(), any());
    }

    @Test
    void outgoingWebhookSuccess() {
        byte[] byteArray = Utils.asJsonString(new ABDMNotifyViaSMSRequest()).getBytes();
        RestServiceResponse success = new RestServiceResponse(202, "Success");
        when(abdmHelperHelper.publishToABDM(any(), any())).thenReturn(success);
        abdmNotifyViaSMSConsumer.outgoingWebhook(Utils.asJsonString(new ABDMNotifyViaSMSRequest()), new Message(byteArray, new MessageProperties()));
        verify(abdmHelperHelper, times(1)).publishToABDM(any(), any());
    }

    @Test
    void outgoingWebhookFailure() {
        byte[] byteArray = Utils.asJsonString(new ABDMNotifyViaSMSRequest()).getBytes();
        RestServiceResponse failure = new RestServiceResponse(500, "Failure");
        when(abdmHelperHelper.publishToABDM(any(), any())).thenReturn(failure);
        abdmNotifyViaSMSConsumer.outgoingWebhook(Utils.asJsonString(new ABDMNotifyViaSMSRequest()), new Message(byteArray, new MessageProperties()));
        verify(abdmHelperHelper, times(1)).publishToABDM(any(), any());
    }

}

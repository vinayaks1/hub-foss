package com.everwell.datagateway.consumers;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.exceptions.NotFoundException;
import com.everwell.datagateway.exceptions.URLDisabledException;
import com.everwell.datagateway.models.dto.PublishToSubscriberUrlDTO;
import com.everwell.datagateway.service.ConsumerService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class DispensationConsumerTest extends BaseTest {
    @InjectMocks
    DispensationConsumer dispensationConsumer;

    @Mock
    ConsumerService consumerService;
;
    DispensationConsumer dispensationConsumerObject = new DispensationConsumer();


    @Test
    void consume() {
        byte[] byteArray = {'T', 'E', 'S', 'T'};
        DispensationConsumer dispensationConsumer = Mockito.spy(DispensationConsumer.class);
        doNothing().when(dispensationConsumer).outgoingWebhook(any(), any());
        dispensationConsumer.consume(new Message(byteArray, new MessageProperties()));
        verify(dispensationConsumer, times(1)).outgoingWebhook(any(), any());
    }

    @Test
    void outgoingWebhook() {
        byte[] byteArray = {'T', 'E', 'S', 'T'};
        doNothing().when(consumerService).publishToSubscriberUrl(any(PublishToSubscriberUrlDTO.class));
        dispensationConsumer.outgoingWebhook("test", new Message(byteArray, new MessageProperties()));
        verify(consumerService, times(1)).publishToSubscriberUrl(any(PublishToSubscriberUrlDTO.class));
    }

    @Test
    void outgoingWebhookNotFoundException() {
        byte[] byteArray = {'T', 'E', 'S', 'T'};
        doThrow(NotFoundException.class).when(consumerService).publishToSubscriberUrl(any(PublishToSubscriberUrlDTO.class));
        dispensationConsumer.outgoingWebhook("test", new Message(byteArray, new MessageProperties()));
        verify(consumerService, times(1)).publishToSubscriberUrl(any(PublishToSubscriberUrlDTO.class));
    }

    @Test
    void outgoingWebhookUrlDisabledException() {
        byte[] byteArray = {'T', 'E', 'S', 'T'};
        doThrow(URLDisabledException.class).when(consumerService).publishToSubscriberUrl(any(PublishToSubscriberUrlDTO.class));
        dispensationConsumer.outgoingWebhook("test", new Message(byteArray, new MessageProperties()));
        verify(consumerService, times(1)).publishToSubscriberUrl(any(PublishToSubscriberUrlDTO.class));
    }

}

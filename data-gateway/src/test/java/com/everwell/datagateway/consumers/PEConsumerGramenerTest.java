package com.everwell.datagateway.consumers;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.entities.Client;
import com.everwell.datagateway.exceptions.NotFoundException;
import com.everwell.datagateway.exceptions.URLDisabledException;
import com.everwell.datagateway.models.dto.PublishToSubscriberUrlDTO;
import com.everwell.datagateway.service.ClientService;
import com.everwell.datagateway.service.ConsumerService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class PEConsumerGramenerTest extends BaseTest {

    @InjectMocks
    PEConsumerGramener peConsumerGramener;

    @Mock
    ConsumerService consumerService;

    PEConsumerGramener peConsumerGramenerObject = new PEConsumerGramener();

    byte[] byteArray = {'T', 'E', 'S', 'T'};

    @Test
    void consume() {
        PEConsumerGramener peConsumerGramener = Mockito.spy(PEConsumerGramener.class);
        doNothing().when(peConsumerGramener).outgoingWebhook(any(), any());
        peConsumerGramener.consume(new Message(byteArray, new MessageProperties()));
        verify(peConsumerGramener, times(1)).outgoingWebhook(any(), any());
    }

    @Test
    void outgoingWebhook() {
        doNothing().when(consumerService).publishToSubscriberUrl(any(PublishToSubscriberUrlDTO.class));
        peConsumerGramener.outgoingWebhook("test", new Message(byteArray, new MessageProperties()));
        verify(consumerService, times(1)).publishToSubscriberUrl(any(PublishToSubscriberUrlDTO.class));
    }

    @Test
    void outgoingWebhookException() {
        //NotFound Exception
        doThrow(NotFoundException.class).when(consumerService).publishToSubscriberUrl(any(PublishToSubscriberUrlDTO.class));
        peConsumerGramener.outgoingWebhook("test", new Message(byteArray, new MessageProperties()));
        verify(consumerService, times(1)).publishToSubscriberUrl(any(PublishToSubscriberUrlDTO.class));

        //UrlDisabled Exception
        doThrow(URLDisabledException.class).when(consumerService).publishToSubscriberUrl(any(PublishToSubscriberUrlDTO.class));
        peConsumerGramener.outgoingWebhook("test", new Message(byteArray, new MessageProperties()));
        verify(consumerService, times(2)).publishToSubscriberUrl(any(PublishToSubscriberUrlDTO.class));  //Called 1+1 times here
    }
}
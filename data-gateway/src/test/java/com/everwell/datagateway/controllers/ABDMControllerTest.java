package com.everwell.datagateway.controllers;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.constants.Constants;
import com.everwell.datagateway.models.dto.ABDMAcknowledgementDTO;
import com.everwell.datagateway.models.request.AbdmClientDto.DataPullRequest;
import com.everwell.datagateway.models.request.abdm.*;
import com.everwell.datagateway.models.response.PublisherResponse;
import com.everwell.datagateway.service.AuthenticationService;
import com.everwell.datagateway.models.request.ABDMAddUpdateConsentRequest;
import com.everwell.datagateway.models.response.abdm.ABDMAuthOnConfirmResponse;
import com.everwell.datagateway.models.response.abdm.OnAuthInit.ABDMOnAuthInitResponse;
import com.everwell.datagateway.service.ABDMIntegrationService;
import com.everwell.datagateway.service.PublisherService;
import com.everwell.datagateway.utils.CacheUtils;
import com.everwell.datagateway.utils.TypeConverterUtil;
import com.everwell.datagateway.utils.Utils;
import org.json.simple.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import static org.mockito.Mockito.doNothing;
import org.json.simple.JSONObject;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class ABDMControllerTest extends BaseTest {

    private MockMvc mockMvc;

    @Mock
    private PublisherService publisherService;

    @InjectMocks
    private ABDMController abdmController;

    @Mock
    private ABDMIntegrationService abdmHelper;

    @Mock
    AuthenticationService authenticationService;

    @Mock
    RedisTemplate<String, Object> redisTemplate;

    CacheUtils cacheUtils;

    private String hipRequestUri = "/v0.5/health-information/hip/request";
    private final DataPullRequest dataPullRequest = new DataPullRequest();

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(abdmController)
                .build();
        dataPullRequest.setHiRequest(new DataPullRequest.HiRequest());
        dataPullRequest.setTransactionId("123");
        dataPullRequest.setRequestId("123");
    }

    @BeforeEach
    public void init() {
        cacheUtils = new CacheUtils(redisTemplate);
    }

    private String DUMMY_KEY = "1";

    @Test
    void abdmAddUpdateConsent() throws Exception {
        String uri = "/v0.5/consents/hip/notify";
        Map<String, String> map = new HashMap<>();
        map.put("Success", "test");
        when(publisherService.publishRequestForABDM(any(), any())).thenReturn(map);
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .header(Constants.X_HIP_ID, Constants.X_HIP_ID_VALUE)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(TypeConverterUtil.convertToJsonString(getABDMAddUpdateConsentRequest()))
                )
                .andExpect(status().isAccepted());

    }

    @Test
    void abdmAddUpdateConsent_InternalServerError() throws Exception {
        String uri = "/v0.5/consents/hip/notify";
        Map<String, String> map = new HashMap<>();
        map.put("Error", "test");
        when(publisherService.publishRequestForABDM(any(), any())).thenReturn(map);
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .header(Constants.X_HIP_ID, Constants.X_HIP_ID_VALUE)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(TypeConverterUtil.convertToJsonString(getABDMAddUpdateConsentRequest()))
                )
                .andExpect(status().isInternalServerError());

    }

    @Test
    void abdmAddUpdateConsent_Unauthorized() throws Exception {
        String uri = "/v0.5/consents/hip/notify";
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .header(Constants.X_HIP_ID, "abc")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(TypeConverterUtil.convertToJsonString(getABDMAddUpdateConsentRequest()))
                )
                .andExpect(status().isUnauthorized());

    }

    private ABDMAddUpdateConsentRequest getABDMAddUpdateConsentRequest() {
        ABDMAddUpdateConsentRequest abdmAddUpdateConsentRequest = new ABDMAddUpdateConsentRequest();
        abdmAddUpdateConsentRequest.setRequestId("Test");
        ABDMAddUpdateConsentRequest.ABDMConsentNotification abdmConsentNotification = new ABDMAddUpdateConsentRequest.ABDMConsentNotification();
        abdmConsentNotification.setStatus("Test");
        abdmConsentNotification.setConsentId("Test");
        abdmAddUpdateConsentRequest.setNotification(abdmConsentNotification);
        return abdmAddUpdateConsentRequest;
    }

    private ABDMOnAuthInitResponse getOnAuthReqData() {
        ABDMOnAuthInitResponse req = new ABDMOnAuthInitResponse();
        req.requestId = "2ffce757-1543-4a30-a156-167c8388d7f9";
        req.timestamp = "2022-07-29T09:58:31.737045";
        req.auth = new ABDMOnAuthInitResponse.Auth();
        req.auth.transactionId = "5b174586-ff4f-474f-bc89-b9c3ed700504";
        req.auth.mode = "DEMOGRAPHICS";
        req.auth.meta = new ABDMOnAuthInitResponse.Auth.Meta();
        req.auth.meta.hint = null;
        req.auth.meta.expiry = "2022-07-29T11:58:31.737053";
        req.error = null;
        req.resp = new ABDMOnAuthInitResponse.Resp();
        req.resp.requestId = "934fbebc-dc0a-4724-bcfb-832e38df0eff";
        return req;
    }

    @Test
    void abdmAuthOnInit() throws Exception {
        String uri = "/v0.5/users/auth/on-init";
        ABDMOnAuthInitResponse inputData = getOnAuthReqData();
        when(abdmHelper.getCareContextLinkingToken(inputData.resp.requestId, inputData.auth.transactionId))
                .thenReturn(true);
        when(redisTemplate.hasKey(inputData.resp.requestId)).thenReturn(true);

        mockMvc.perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(TypeConverterUtil.convertToJsonString(inputData))
                )
                .andExpect(status().isAccepted());
    }

    private ABDMAuthOnConfirmResponse getAuthConfirmData() {
        ABDMAuthOnConfirmResponse req = new ABDMAuthOnConfirmResponse();
        req.requestId = "2ffce757-1543-4a30-a156-167c8388d7f9";
        req.timestamp = "2022-07-29T09:58:31.737045";
        req.auth =  new ABDMAuthOnConfirmResponse.Auth();
        req.auth.accessToken = "tkn";
        req.auth.patient = null;
        req.error = null;
        req.resp = new ABDMAuthOnConfirmResponse.Resp();
        req.resp.requestId = "934fbebc-dc0a-4724-bcfb-832e38df0eff";
        return req;
    }

    @Test
    void abdmAuthOnConfirm() throws Exception {
        String uri = "/v0.5/users/auth/on-confirm";
        ABDMAuthOnConfirmResponse inputData = getAuthConfirmData();
        when(abdmHelper.addCareContext(inputData.resp.requestId, inputData.auth.accessToken))
                .thenReturn(true);
        when(redisTemplate.hasKey(inputData.resp.requestId)).thenReturn(true);

        mockMvc.perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(TypeConverterUtil.convertToJsonString(inputData))
                )
                .andExpect(status().isAccepted());
    }


    private ABDMAcknowledgementDTO getABDMAcknowledgementData() {
        ABDMAcknowledgementDTO req = new ABDMAcknowledgementDTO();
        req.requestId = "2ffce757-1543-4a30-a156-167c8388d7f9";
        req.timestamp = "2022-07-29T09:58:31.737045";
        req.acknowledgement = new ABDMAcknowledgementDTO.Acknowledgement();
        req.acknowledgement.status = "SUCCESS";
        req.error = null;
        req.resp = new ABDMAcknowledgementDTO.Resp();
        req.resp.requestId = "934fbebc-dc0a-4724-bcfb-832e38df0eff";

        return req;
    }

    @Test
    void abdmOnAddCareContext() throws Exception {
        String uri = "/v0.5/links/link/on-add-contexts";
        ABDMAcknowledgementDTO inputData = getABDMAcknowledgementData();
        when(abdmHelper.notifyCareContext(any())).thenReturn(true);
        when(abdmHelper.getCareContextLinkingDetailsFor(any())).thenReturn(new ABDMLinkCareContextEvent());
        when(redisTemplate.hasKey(inputData.resp.requestId)).thenReturn(true);

        mockMvc.perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(TypeConverterUtil.convertToJsonString(inputData))
                )
                .andExpect(status().isAccepted());
    }

    @Test
    void abdmSMSOnNotify() throws Exception {
        String uri = "/v0.5/patients/sms/on-notify";
        JSONObject jsonObject = new JSONObject();
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .header(Constants.X_HIP_ID,Constants.X_HIP_ID_VALUE)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(TypeConverterUtil.convertToJsonString(jsonObject))
                )
                .andExpect(status().isAccepted());

    }

    @Test
    void abdmSMSOnNotify_Unauthorized() throws Exception {
        String uri = "/v0.5/patients/sms/on-notify";
        JSONObject jsonObject = new JSONObject();
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .header(Constants.X_HIP_ID,"abc")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(TypeConverterUtil.convertToJsonString(jsonObject))
                )
                .andExpect(status().isUnauthorized());

    }

    @Test
    void abdmDataExcahngeTest() throws Exception {
        when(publisherService.publishRequest(any(), any())).thenReturn(new PublisherResponse());
        mockMvc.
                perform(
                        MockMvcRequestBuilders.
                                post(hipRequestUri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(dataPullRequest))
                                .header("x-hip-id","nikshay")
                )
                .andExpect(status().isAccepted());
    }
    @Test
    public void abdmDiscoveryTest() throws Exception {
        ABDMDiscoveryRequest abdmDiscoveryRequest = new ABDMDiscoveryRequest();
        String uri = "/v0.5/care-contexts/discover";
        mockMvc.perform(
                MockMvcRequestBuilders
                        .post(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TypeConverterUtil.convertToJsonString(abdmDiscoveryRequest))
        )
                .andExpect(status().isAccepted());
    }

    @Test
    public void abdmLinkingInitiationTest() throws Exception {
        ABDMLinkInitiationRequest abdmLinkInitiationRequest = new ABDMLinkInitiationRequest();
        String uri = "/v0.5/links/link/init";
        mockMvc.perform(
                MockMvcRequestBuilders
                        .post(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TypeConverterUtil.convertToJsonString(abdmLinkInitiationRequest))
        )
                .andExpect(status().isAccepted());
    }

    @Test
    public void abdmConsentRequestInitiationTest() throws Exception {
        ABDMConsentRequestOnInitiation abdmLinkInitiationRequest = new ABDMConsentRequestOnInitiation();
        when(abdmHelper.processConsentInitiation(any())).thenReturn(true);
        String uri = "/v0.5/consent-requests/on-init";
        mockMvc.perform(
                MockMvcRequestBuilders
                        .post(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TypeConverterUtil.convertToJsonString(abdmLinkInitiationRequest))
        )
                .andExpect(status().isAccepted());
    }
    @Test
    void abdmAuthNotifyTest() throws Exception {
        ABDMAuthNotifyRequest abdmAuthNotifyRequest = Utils.convertStrToObject(getAuthNotifyJson(),ABDMAuthNotifyRequest.class);
        when(abdmHelper.addCareContextDirectAuth(any()))
                .thenReturn(true);
        when(redisTemplate.hasKey(abdmAuthNotifyRequest.auth.transactionId)).thenReturn(true);
        mockMvc.
                perform(
                        MockMvcRequestBuilders.
                                post("/v0.5/users/auth/notify")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(abdmAuthNotifyRequest))
                )
                .andExpect(status().isAccepted());
    }

    @Test
    void abdmConsentNotificationTest() throws Exception {
        ABDMConsentNotification abdmConsentNotification = Utils.convertStrToObject(getConsentNotifyJson(),ABDMConsentNotification.class);

        mockMvc.
                perform(
                        MockMvcRequestBuilders.
                                post("/v0.5/consents/hiu/notify")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(abdmConsentNotification))
                )
                .andExpect(status().isAccepted());
    }

     private String getAuthNotifyJson() {
        return "{\"requestId\":\"baf556d0-6e37-4cb5-a63e-f4a59725f3fe\",\"timestamp\":\"2023-01-03T11:40:04.765923\",\"auth\":{\"transactionId\":\"19d38558-1ef6-44c5-bc0e-6f8feee50f7a\",\"status\":\"GRANTED\",\"accessToken\":\"eyJhbGciOiJSUzUxMiJ9.eyJzdWIiOiJtb2hhbmtyaXNobmExOTk5QHNieCIsInJlcXVlc3RlclR5cGUiOiJISVAiLCJyZXF1ZXN0ZXJJZCI6Ik5JS1NIQVktSElQIiwicGF0aWVudElkIjoibW9oYW5rcmlzaG5hMTk5OUBzYngiLCJzZXNzaW9uSWQiOiJkYjQ0MGJkMi05NTc3LTQ3NDQtOWNhYi1iNGNhNDUxMzZhMTIiLCJleHAiOjE2NzI4MzI0MDQsImlhdCI6MTY3Mjc0NjAwNH0.bsAk4F2VDNjHOLFOtdvTgN28X2-Yn0BGzRLAGn8TUHdDz7b7LlzCivZzDdZszPWnx3Qw8huuf6muUmqBgH7ajhlmfdmHPbC3Bz6QGqemMPJfGxjFDKz5hQyeEPeOdbkBpf4NMrfRqPmxws8O6SsbWOoOW-yMoldQaArua_qO5ojonofSG-pFtt0Jty2cAQJ-HAgiRXZ1gEkLS-jfYk-UCX2KXE20fewykbi0LEifcRF8aE5vgnDZ40zSlTDe7Rm6eIvr3BvyoMxvkiI0BJJw4o6goZqebDkN_mx0mxk3xFagiqpMF6VZhHgFT8SP2DUCSkW5n0LVa6c3FrjQgV2NWg\",\"validity\":{\"purpose\":\"LINK\",\"requester\":{\"type\":\"HIP\",\"id\":\"NIKSHAY-HIP\",\"name\":null},\"expiry\":\"2024-01-03T11:40:04.765873\",\"limit\":1},\"patient\":null}}";
     }

     private String getConsentNotifyJson() {
        return "{\"requestId\":\"5f7a535d-a3fd-416b-b069-c97d021fbacd\",\"timestamp\":\"1970-01-01T00:00:00.000Z\",\"notification\":{\"consentRequestId\":\"5f7a535d-a3fd-416b-b069-c97d021fbacd\",\"status\":\"GRANTED\",\"consentArtefacts\":[{\"id\":\"5f7a535d-a3fd-416b-b069-c97d021fbacd\"}]}}";
     }

}
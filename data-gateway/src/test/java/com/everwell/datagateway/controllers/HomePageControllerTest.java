package com.everwell.datagateway.controllers;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.utils.TypeConverterUtil;
import org.json.simple.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class HomePageControllerTest extends BaseTest {

    private MockMvc mockMvc;

    @InjectMocks
    private HomePageController homePageController;

    @BeforeEach
    public void setup() {

        mockMvc = MockMvcBuilders.standaloneSetup(homePageController)
                .build();
    }

    @Test
    void genericFallbackError() throws Exception {
        String uri = "/";
        mockMvc.perform(MockMvcRequestBuilders.get(uri))
                .andExpect(status().isOk());
    }

    @Test
    void genericSubscriberUrl() throws Exception {
        String uri = "/v1/SubscriberUrl";
        JSONObject jsonObject = new JSONObject();
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(TypeConverterUtil.convertToJsonString(jsonObject))
                )
                .andExpect(status().isOk());

    }
}
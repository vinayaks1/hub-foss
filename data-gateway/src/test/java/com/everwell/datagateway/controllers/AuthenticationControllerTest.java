package com.everwell.datagateway.controllers;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.config.CustomExceptionHandler;
import com.everwell.datagateway.enums.AuthenticationCredentialTypeEnum;
import com.everwell.datagateway.models.request.AuthenticationRequest;
import com.everwell.datagateway.models.request.RegisterRequest;
import com.everwell.datagateway.service.AuthenticationServiceImpl;
import com.everwell.datagateway.utils.TypeConverterUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;


import java.util.Arrays;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class AuthenticationControllerTest extends BaseTest {

    private MockMvc mockMvc;

    @InjectMocks
    private AuthenticationController authenticationController;

    @Mock
    private AuthenticationServiceImpl authenticationService;

    private String registerClientUri = "/register";

    private String generateTokenUri="/token";


    @BeforeEach
    public void setup() {

        mockMvc = MockMvcBuilders.standaloneSetup(authenticationController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }


    @Test
    void registerClientSuccess() throws Exception {
        RegisterRequest registerRequest = new RegisterRequest("test", "test", "test@gmail.com");
        when(authenticationService.registerClient(registerRequest)).thenReturn(true);
        mockMvc.
                perform(
                        MockMvcRequestBuilders.
                                post(registerClientUri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(TypeConverterUtil.convertToJsonString(registerRequest))
                )
                .andExpect(status().isOk());
        verify(authenticationService, Mockito.times(1)).registerClient(any());
    }

    @Test
    void registerClientFailure() throws Exception {
        RegisterRequest registerRequest = new RegisterRequest("test", "test", "test@gmail.com");
        when(authenticationService.registerClient(any())).thenReturn(false);
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(registerClientUri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(TypeConverterUtil.convertToJsonString(registerRequest))
                )
                .andExpect(status().isConflict());
    }

    @Test
    void registerClientInvalidCredentialType() throws Exception {
        RegisterRequest registerRequest = new RegisterRequest("test", "test", "test@gmail.com", "RANDOM", null, null, null);
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(registerClientUri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(TypeConverterUtil.convertToJsonString(registerRequest))
                )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("CredentialType value can be only from "+ Arrays.toString(AuthenticationCredentialTypeEnum.values())));
    }

    @Test
    void generateToken() throws Exception {
        AuthenticationRequest authenticationRequest=new AuthenticationRequest();
        authenticationRequest.setUsername("test");
        authenticationRequest.setPassword("test");
        String sampleJWT= "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";
        when(authenticationService.generateToken(authenticationRequest)).thenReturn(sampleJWT);
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(generateTokenUri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(TypeConverterUtil.convertToJsonString(authenticationRequest))
                )
                .andExpect(status().isOk());

    }
}
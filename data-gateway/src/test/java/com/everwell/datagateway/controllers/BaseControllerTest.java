package com.everwell.datagateway.controllers;

import com.everwell.datagateway.config.CustomExceptionHandler;
import com.everwell.datagateway.entities.Client;
import com.everwell.datagateway.entities.Event;
import com.everwell.datagateway.service.AuthenticationService;
import com.everwell.datagateway.service.ClientService;
import com.everwell.datagateway.service.EventService;
import com.everwell.datagateway.utils.CacheUtils;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

public class BaseControllerTest {
    private MockMvc mockMvc;
    @InjectMocks
    private BaseController baseController;

    @Mock
    private SecurityContext securityContext;

    @Mock
    private ClientService clientService;

    @Mock
    private AuthenticationService authenticationService;

    @Mock
    private EventService eventService;

    @Mock
    RedisTemplate<String, Object> redisTemplate;

    @Mock
    private ValueOperations<String, Object> valueOperations;

    @InjectMocks
    CacheUtils cacheUtils;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(baseController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
        cacheUtils = new CacheUtils(redisTemplate);
    }

    public Authentication getAuthentication() {
        String username = "john.doe";
        String password = "password";
        String authority = "ROLE_USER";
        SimpleGrantedAuthority grantedAuthority = new SimpleGrantedAuthority(authority);
        List<SimpleGrantedAuthority> authorities = Collections.singletonList(grantedAuthority);
        SecurityContextHolder.setContext(securityContext);
        return new UsernamePasswordAuthenticationToken(username, password, authorities);
    }

    public void mockGetClientWithEventAccessCheck() {
        Client client = new Client();
        client.accessibleClient = 2L;
        client.id = 1L;
        Event event = new Event();
        event.setId(1L);
        event.setEventName("test");
        when(securityContext.getAuthentication()).thenReturn(getAuthentication());
        when(authenticationService.getCurrentUsername(getAuthentication())).thenReturn("test");
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
        when(valueOperations.get(anyString())).thenReturn("1");
        when(clientService.findByUsername(any())).thenReturn(client);
        when(eventService.findEventByEventName(any())).thenReturn(event);
        when(clientService.isClientSubscribedToEvent(anyLong(), anyLong())).thenReturn(true);
    }
}

package com.everwell.datagateway.controllers;

import ca.uhn.fhir.context.FhirContext;
import com.everwell.datagateway.config.CustomExceptionHandler;
import com.everwell.datagateway.constants.Constants;
import com.everwell.datagateway.enums.TypeOfEpisodeEnum;
import com.everwell.datagateway.models.request.EpisodeSearchRequest;
import com.everwell.datagateway.models.response.ApiResponse;
import com.everwell.datagateway.models.response.EpisodeSearchResult;
import com.everwell.datagateway.service.EpisodeServiceRestService;
import com.everwell.datagateway.service.PDQMService;
import com.everwell.datagateway.utils.Utils;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hl7.fhir.r4.model.Bundle;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class PDQMControllerTest extends BaseControllerTest {
    private MockMvc mockMvc;
    @Mock
    private PDQMService pdqmService;

    @Mock
    private EpisodeServiceRestService episodeService;

    @InjectMocks
    private PDQMController pdqmController;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(pdqmController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void getPatientDetailsTest() throws Exception {
        Bundle bundle = new Bundle();
        bundle.setType(Bundle.BundleType.SEARCHSET);
        bundle.setTotal(5);
        FhirContext fhirContext = FhirContext.forR4();
        String responseString = fhirContext.newJsonParser().encodeResourceToString(bundle);
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(responseString);
        EpisodeSearchRequest searchRequest = new EpisodeSearchRequest(10000, 0, Arrays.asList(
                Constants.ID, Constants.DELETED, Constants.FIRST_NAME, Constants.LAST_NAME, Constants.PRIMARY_PHONE_NUMBER, Constants.EMAIL_LIST, Constants.DATE_OF_BIRTH, Constants.TREATMENT_OUTCOME, Constants.ADDRESS, Constants.PIN_CODE, Constants.GENDER
        ), TypeOfEpisodeEnum.getTypeOfEpisodeList());
        mockGetClientWithEventAccessCheck();
        searchRequest.setSearch(new EpisodeSearchRequest.SupportedSearchMethods(new HashMap<>(), new HashMap<>(), new HashMap<>()));
        Mockito.when(pdqmService.getEpisodeSearchRequestFromRequest(Mockito.any())).thenReturn(searchRequest);
        Mockito.when(episodeService.episodeSearch(Mockito.any(), Mockito.anyLong())).thenReturn(new EpisodeSearchResult(1L, "test"));
        Mockito.when(pdqmService.getPatientBundle(Mockito.any(),Mockito.any(), Mockito.any())).thenReturn(jsonNode);
        mockMvc.
                perform(
                        MockMvcRequestBuilders.
                                get("/v1/Patient")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.Success").value("true"));
    }

    @Test
    public void searchPatientDetailsTest() throws Exception {
        Map<String, Object> request = new HashMap<>();
        request.put("_id", "1");
        Bundle bundle = new Bundle();
        bundle.setType(Bundle.BundleType.SEARCHSET);
        bundle.setTotal(5);
        FhirContext fhirContext = FhirContext.forR4();
        String responseString = fhirContext.newJsonParser().encodeResourceToString(bundle);
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(responseString);
        EpisodeSearchRequest searchRequest = new EpisodeSearchRequest(10000, 0, Arrays.asList(
                Constants.ID, Constants.DELETED, Constants.FIRST_NAME, Constants.LAST_NAME, Constants.PRIMARY_PHONE_NUMBER, Constants.EMAIL_LIST, Constants.DATE_OF_BIRTH, Constants.TREATMENT_OUTCOME, Constants.ADDRESS, Constants.PIN_CODE, Constants.GENDER
        ), TypeOfEpisodeEnum.getTypeOfEpisodeList());
        searchRequest.setSearch(new EpisodeSearchRequest.SupportedSearchMethods(new HashMap<>(), new HashMap<>(), new HashMap<>()));
        mockGetClientWithEventAccessCheck();
        Mockito.when(pdqmService.getEpisodeSearchRequestFromRequest(Mockito.any())).thenReturn(searchRequest);
        Mockito.when(episodeService.episodeSearch(Mockito.any(), Mockito.anyLong())).thenReturn(new EpisodeSearchResult(1L, "test"));
        Mockito.when(pdqmService.getPatientBundle(Mockito.any(),Mockito.any(), Mockito.any())).thenReturn(jsonNode);
        mockMvc.
                perform(
                        MockMvcRequestBuilders.
                                post("/v1/Patient")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                                .content(Utils.asJsonString(request))
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.Success").value("true"));
    }

    @Test
    public void getCapabilityStatementTest() throws Exception {
        String capabilityStatement = "test";
        Mockito.when(pdqmService.getCapabilityStatement()).thenReturn(capabilityStatement);
        mockMvc.
                perform(
                        MockMvcRequestBuilders.
                                get("/metadata")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk());
    }
}

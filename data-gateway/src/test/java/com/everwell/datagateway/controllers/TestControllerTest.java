package com.everwell.datagateway.controllers;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.config.CustomExceptionHandler;
import com.everwell.datagateway.models.response.PublisherResponse;
import com.everwell.datagateway.service.PublisherService;
import com.everwell.datagateway.utils.Utils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class TestControllerTest extends BaseTest {

    private MockMvc mockMvc;

    @InjectMocks
    private TestController testController;

    @Mock
    private PublisherService publisherService;

    private String addTestUri = "/add-test";
    private String updatedTestUri = "/update-test";
    private String getTestByIdUri = "/get-test-id";
    private String getTestByPatientIdUri = "/get-test-patientId";

    private String testString = "testString";
    private Long testValue = 1L;

    @BeforeEach
    public void setup() {

        mockMvc = MockMvcBuilders.standaloneSetup(testController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    void addTest() throws Exception {
        when(publisherService.publishRequest(any(), any())).thenReturn(new PublisherResponse());
        mockMvc.
                perform(
                        MockMvcRequestBuilders.
                                post(addTestUri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(testString))
                )
                .andExpect(status().isOk());
    }

    @Test
    void getTestById() throws Exception {
        when(publisherService.publishRequest(any(), any())).thenReturn(new PublisherResponse());
        mockMvc.
                perform(
                        MockMvcRequestBuilders.
                                get(getTestByIdUri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .param("testRequestId", String.valueOf(testValue))
                )
                .andExpect(status().isOk());
    }

    @Test
    void getTestByPatient() throws Exception {
        when(publisherService.publishRequest(any(), any())).thenReturn(new PublisherResponse());
        mockMvc.
                perform(
                        MockMvcRequestBuilders.
                                get(getTestByPatientIdUri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .param("patientId", String.valueOf(testValue))
                )
                .andExpect(status().isOk());
    }

    @Test
    void updateTest() throws Exception {
        when(publisherService.publishRequest(any(), any())).thenReturn(new PublisherResponse());
        mockMvc.
                perform(
                        MockMvcRequestBuilders.
                                put(updatedTestUri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(testString))
                )
                .andExpect(status().isOk());
    }
}
package com.everwell.datagateway.handlers;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.entities.Event;
import com.everwell.datagateway.entities.PublisherQueueInfo;
import com.everwell.datagateway.enums.EventEnum;
import com.everwell.datagateway.models.dto.PublisherData;
import com.everwell.datagateway.publishers.RMQPublisher;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class EventHandlerTest extends BaseTest {

    @InjectMocks
    private EventHandler eventHandler = new AbstractEventImpl();

    @Mock
    private RMQPublisher pub;


    private String testString = "testString";

    @Test
    void getEventEnumerator() {
    }

    @Test
    void publishEvent() {
        PublisherData publisherData = PublisherData.builder()
                .referenceId(2L)
                .clientName(testString)
                .data(testString)
                .build();
        Event event = new Event();
        event.setEventName(EventEnum.ADD_TEST.getEventName());
        PublisherQueueInfo publisherQueueInfo = new PublisherQueueInfo();
        publisherQueueInfo.setEventName("add-test");
        eventHandler.publishEvent(publisherData, publisherQueueInfo);
        verify(pub, times(1)).publish(any(), any(), any(), any(), any());

    }
}
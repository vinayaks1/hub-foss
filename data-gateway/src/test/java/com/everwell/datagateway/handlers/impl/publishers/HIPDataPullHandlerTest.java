package com.everwell.datagateway.handlers.impl.publishers;

import com.everwell.datagateway.enums.EventEnum;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class HIPDataPullHandlerTest {

    @Test
    void getEventEnumerator() {
        HipDataPullHandler hipDataPullHandler = new HipDataPullHandler();
        assertEquals(hipDataPullHandler.getEventEnumerator(), EventEnum.HIP_DATA_EXCHANGE);
    }
}

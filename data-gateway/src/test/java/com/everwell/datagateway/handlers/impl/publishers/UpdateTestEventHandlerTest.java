package com.everwell.datagateway.handlers.impl.publishers;

import com.everwell.datagateway.enums.EventEnum;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class UpdateTestEventHandlerTest {

    @Test
    void getEventEnumerator() {
        UpdateTestEventHandler updateTestEventHandler = new UpdateTestEventHandler();
        assertTrue(updateTestEventHandler.getEventEnumerator().equals(EventEnum.UPDATE_TEST));
    }
}
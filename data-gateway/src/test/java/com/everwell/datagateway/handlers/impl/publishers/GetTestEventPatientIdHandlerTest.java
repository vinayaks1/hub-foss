package com.everwell.datagateway.handlers.impl.publishers;

import com.everwell.datagateway.enums.EventEnum;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class GetTestEventPatientIdHandlerTest {

    @Test
    void getEventEnumerator() {
        GetTestEventPatientIdHandler getTestEventPatientIdHandler = new GetTestEventPatientIdHandler();
        assertTrue(getTestEventPatientIdHandler.getEventEnumerator().equals(EventEnum.GET_TEST_PATIENT_ID));
    }
}
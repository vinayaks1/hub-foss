package com.everwell.datagateway.handlers.impl.publishers;

import com.everwell.datagateway.enums.EventEnum;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class AddMermEntityMappingHandlerTest {

    @Test
    void getEventEnumerator() {
        AddMermEntityMappingHandler addMermEntityMappingHandler = new AddMermEntityMappingHandler();
        assertTrue(addMermEntityMappingHandler.getEventEnumerator().equals(EventEnum.ADD_MERM_ENTITY_MAPPING));
    }
}
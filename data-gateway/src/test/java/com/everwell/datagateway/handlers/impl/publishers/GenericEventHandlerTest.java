package com.everwell.datagateway.handlers.impl.publishers;

import com.everwell.datagateway.enums.EventEnum;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GenericEventHandlerTest {

    @Test
    void getEventEnumerator() {
        GenericEventHandler genericEventHandler = new GenericEventHandler();
        assertTrue(genericEventHandler.getEventEnumerator().equals(EventEnum.GENERIC_PUBLISH));
    }
}
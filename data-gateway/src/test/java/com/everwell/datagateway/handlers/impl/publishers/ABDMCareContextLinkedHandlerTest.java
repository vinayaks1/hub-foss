package com.everwell.datagateway.handlers.impl.publishers;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.enums.EventEnum;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ABDMCareContextLinkedHandlerTest {
    @Test
    public void getEventEnumerator() {
        ABDMCareContextLinkedHandler abdmCareContextLinkedHandler = new ABDMCareContextLinkedHandler();
        assertTrue(abdmCareContextLinkedHandler.getEventEnumerator().equals(EventEnum.ABDM_CARE_CONTEXT_LINKED));
    }
}
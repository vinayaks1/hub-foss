package com.everwell.datagateway.handlers.impl.publishers;

import com.everwell.datagateway.enums.EventEnum;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class UpdateMermEntityMappingHandlerTest {

    @Test
    void getEventEnumerator() {
        UpdateMermEntityMappingHandler updateMermEntityMappingHandler = new UpdateMermEntityMappingHandler();
        assertTrue(updateMermEntityMappingHandler.getEventEnumerator().equals(EventEnum.UPDATE_MERM_ENTITY_MAPPING));
    }
}
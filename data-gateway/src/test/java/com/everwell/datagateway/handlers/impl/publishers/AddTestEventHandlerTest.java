package com.everwell.datagateway.handlers.impl.publishers;

import com.everwell.datagateway.enums.EventEnum;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class AddTestEventHandlerTest {

    @Test
    void getEventEnumerator() {
        AddTestEventHandler addTestEventHandler = new AddTestEventHandler();
        assertTrue(addTestEventHandler.getEventEnumerator().equals(EventEnum.ADD_TEST));
    }
}
package com.everwell.datagateway.zuul

import com.everwell.datagateway.filters.zuul.dell.DellZuulRouteFilter
import com.everwell.datagateway.service.NikshayReportsRestService
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.mock.web.MockHttpServletRequest
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class DellZuulRouteFilterTest {

    private lateinit var dellZuulRouteFilter: DellZuulRouteFilter
    val nikshayReportsRestService: NikshayReportsRestService = mock()

    private val nikshayAuthToken = "abcd1234"

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        dellZuulRouteFilter = DellZuulRouteFilter()
        dellZuulRouteFilter.nikshayReportsRestService = nikshayReportsRestService
        Mockito.`when`(nikshayReportsRestService.getAuthToken(any())).thenAnswer { nikshayAuthToken }
    }

    @Test
    fun testZuulRouteFilterSuccess() {
        val request = MockHttpServletRequest("GET", "/dell/mstrState")
        val context = RequestContext()
        context.setRequest(request)
        RequestContext.testSetCurrentContext(context)
        val result = dellZuulRouteFilter.runFilter();
        assertEquals(context["requestURI"], "/nikshayapiv2/api/masters/mstrState")
        assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
    }

    @Test
    fun testZuulRouteFilterFailure(){
        val request = MockHttpServletRequest("GET", "/dell/unknownAPICall")
        val context = RequestContext()
        context.setRequest(request)
        RequestContext.testSetCurrentContext(context)
        val result = dellZuulRouteFilter.runFilter()
        assertEquals(context["requestURI"], null)
        assertEquals(ExecutionStatus.FAILED, result.getStatus())
    }

}

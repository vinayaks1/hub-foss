package com.everwell.datagateway.zuul

import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.filters.zuul.merm.MermZuulRouteFilter
import com.everwell.datagateway.models.response.ApiResponse
import com.everwell.datagateway.models.response.ImeiClientResponse
import com.everwell.datagateway.service.RegistryRestService
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.*
import org.springframework.mock.web.MockHttpServletRequest
import org.springframework.web.client.RestTemplate
import java.net.URL
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class MermRouteFilterTest {

    private lateinit var mermZuulRouteFilter: MermZuulRouteFilter
    val registryRestService: RegistryRestService = mock()

    val mockRestTemplate: RestTemplate = mock()
    private val authToken = "abcd1234"

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        mermZuulRouteFilter = MermZuulRouteFilter()
        mermZuulRouteFilter.registryRestService = registryRestService
        Mockito.`when`(registryRestService.getAuthToken(any())).thenAnswer { authToken }
        mermZuulRouteFilter.restTemplateForApi = mockRestTemplate
    }

    @Test
    fun testZuulRouteFilterSuccessTestFetchClientIdFromRegistry() {
        val appProperties = AppProperties()
        appProperties.registryServerUrl = "http://localhost:9210"
        appProperties.registryDefaultClientId = "29"
        mermZuulRouteFilter.appProperties = appProperties
        val request = MockHttpServletRequest("POST", "/process-merm-event")
        val context = RequestContext()
        context.setRequest(request)
        context.routeHost = URL("http://localhost:9210")
        RequestContext.testSetCurrentContext(context)
        val response = ApiResponse(
            "true", ImeiClientResponse(29L), "");
        val responseEntity = ResponseEntity(response, HttpStatus.OK)
        val headers = HttpHeaders()
        headers.add("Content-Type", MediaType.APPLICATION_JSON.toString())
        Mockito.`when`(mockRestTemplate.exchange("http://localhost:9210/v1/imei/client-id", HttpMethod.POST, HttpEntity<Any>("", headers), object : ParameterizedTypeReference<ApiResponse<ImeiClientResponse>>() {}))
            .thenAnswer { responseEntity }
        val result = mermZuulRouteFilter.runFilter();
        assertEquals(context["requestURI"], "/v1/merm/event")
        assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
    }

}
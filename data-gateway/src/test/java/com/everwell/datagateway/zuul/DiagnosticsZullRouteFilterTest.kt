package com.everwell.datagateway.zuul

import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.filters.zuul.diagnostics.DiagnosticsZuulRouteFilter
import com.everwell.datagateway.service.DiagnosticsRestService
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import com.nhaarman.mockitokotlin2.mock
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.mock.web.MockHttpServletRequest
import kotlin.test.assertEquals
import com.nhaarman.mockitokotlin2.any

@RunWith(MockitoJUnitRunner::class)
class DiagnosticsZuulFilterTest {

    private lateinit var diagnosticsZuulFilter: DiagnosticsZuulRouteFilter
    private val diagnosticsRestService: DiagnosticsRestService = mock()

    private val authToken = "abcd1234"

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        diagnosticsZuulFilter = DiagnosticsZuulRouteFilter()
        diagnosticsZuulFilter.diagnosticsRestService = diagnosticsRestService
        Mockito.`when`(diagnosticsRestService.getAuthToken(any())).thenAnswer { authToken }
        val appProperties = AppProperties()
        diagnosticsZuulFilter.appProperties = appProperties
    }

    @Test
    fun testZuulRouteFilterSuccess() {
        val request = MockHttpServletRequest("GET", "/diagnostics/v1/user?id=1")
        request.addHeader("client-id", "29")
        val context = RequestContext()
        context.request = request
        RequestContext.testSetCurrentContext(context)
        val result = diagnosticsZuulFilter.runFilter();
        assertEquals(context["requestURI"], "/v1/user?id=1")
        assertEquals(ExecutionStatus.SUCCESS, result.status)
    }

}
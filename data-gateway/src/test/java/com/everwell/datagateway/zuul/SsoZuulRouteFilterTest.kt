package com.everwell.datagateway.zuul

import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.filters.zuul.sso.SsoZuulRouteFilter
import com.everwell.datagateway.service.SsoRestService
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import com.nhaarman.mockitokotlin2.mock
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.mock.web.MockHttpServletRequest
import kotlin.test.assertEquals
import com.nhaarman.mockitokotlin2.any
import com.everwell.datagateway.constants.Constants.CLIENT_ID

@RunWith(MockitoJUnitRunner::class)
class SsoZuulRouteFilterTest {

    private lateinit var ssoZuulFilter: SsoZuulRouteFilter
    private val ssoRestService: SsoRestService = mock()

    private val authToken = "abcd1234"

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        ssoZuulFilter = SsoZuulRouteFilter()
        ssoZuulFilter.ssoRestService = ssoRestService
        Mockito.`when`(ssoRestService.getAuthToken(any())).thenAnswer { authToken }
        val appProperties = AppProperties()
        ssoZuulFilter.appProperties = appProperties
        appProperties.ssoClientIdUrlMap = mapOf("29" to "http://localhost:9090")
    }

    @Test
    fun testZuulRouteFilterSuccess() {
        val request = MockHttpServletRequest("GET", "/sso/v1/user?id=1")
        request.addHeader(CLIENT_ID, "29")
        val context = RequestContext()
        context.request = request
        RequestContext.testSetCurrentContext(context)
        val result = ssoZuulFilter.runFilter();
        assertEquals(context["requestURI"], "/v1/user?id=1")
        assertEquals(ExecutionStatus.SUCCESS, result.status)
    }

}

package com.everwell.datagateway.zuul

import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.filters.zuul.hubReports.HubReportsZuulRouteFilter
import com.everwell.datagateway.service.HubReportsRestService
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import com.nhaarman.mockitokotlin2.mock
import org.junit.Before
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import com.nhaarman.mockitokotlin2.any
import org.junit.Test
import org.springframework.mock.web.MockHttpServletRequest
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class HubReportsZuulRouterFilterTest {
    private lateinit var hubReportsZuulFilter : HubReportsZuulRouteFilter
    private val hubReportRestService: HubReportsRestService = mock()

    private val authToken = "abcd1234"

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        hubReportsZuulFilter = HubReportsZuulRouteFilter()
        hubReportsZuulFilter.hubReportsRestService = hubReportRestService;
        Mockito.`when`(hubReportRestService.getAuthToken(any())).thenAnswer { authToken }
        val appProperties = AppProperties()
        hubReportsZuulFilter.appProperties = appProperties
    }

    @Test
    fun testZuulFilterSuccess() {
        val request = MockHttpServletRequest("GET", "/reports/v1/client")
        request.addHeader("client-id", "29")
        val context = RequestContext()
        context.request = request
        RequestContext.testSetCurrentContext(context)
        val result = hubReportsZuulFilter.runFilter();
        assertEquals(context["requestURI"], "/v1/client")
        assertEquals(ExecutionStatus.SUCCESS, result.status)
    }


}
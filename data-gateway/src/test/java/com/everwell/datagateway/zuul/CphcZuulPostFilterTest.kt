import com.everwell.datagateway.filters.CphcZuulPostFilter
import com.everwell.datagateway.service.NikshayRestService
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.cloud.netflix.zuul.filters.ProxyRequestHelper
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties
import org.springframework.http.HttpStatus
import org.springframework.mock.web.MockHttpServletRequest
import org.springframework.mock.web.MockHttpServletResponse
import org.springframework.web.client.RestTemplate
import java.net.URL
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class CphcZuulPostFilterTest {

    private lateinit var cphcZuulPostFilter: CphcZuulPostFilter

    val mockRestTemplate: RestTemplate = mock()
    val mockNikshayRestService: NikshayRestService = mock()
    val proxyRequestHelper: ProxyRequestHelper = ProxyRequestHelper(ZuulProperties())
    private val nikshayAuthToken = "abcd1234"


    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        cphcZuulPostFilter = CphcZuulPostFilter()
        cphcZuulPostFilter.helper = proxyRequestHelper
        cphcZuulPostFilter.nikshayRestService = mockNikshayRestService
        cphcZuulPostFilter.restTemplateForApi = mockRestTemplate
        Mockito.`when`(mockNikshayRestService.getAuthToken(any())).thenAnswer { nikshayAuthToken }
    }
    @Test
    fun testZuulPostFilterForUnAuthorised(){
        val request = MockHttpServletRequest("POST", "/cphc/addPatient")
        val response = MockHttpServletResponse()
        val context = RequestContext()
        context.setRequest(request)
        context.response = response
        RequestContext.testSetCurrentContext(context)
        context.routeHost = URL("https://beta.nikshay.in")
        context["requestURI"] ="/api/Patients/AddIndiaTb"
        context.setResponseBody("Not authorised to call")
        context.setResponseStatusCode(HttpStatus.UNAUTHORIZED.value())
        val result = cphcZuulPostFilter.runFilter()
        assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
    }

    @Test
    fun testZuulPostFilterForAuthorised(){
        val successBodyString = "Success"
        val request = MockHttpServletRequest("POST", "/cphc/addPatient")
        val response = MockHttpServletResponse()
        val context = RequestContext()
        context.setRequest(request)
        context.response = response
        RequestContext.testSetCurrentContext(context)
        context.routeHost = URL("https://beta.nikshay.in")
        context["requestURI"] ="/api/Patients/AddIndiaTb"
        context.setResponseBody(successBodyString)
        context.setResponseStatusCode(HttpStatus.OK.value())
        val result = cphcZuulPostFilter.runFilter()
        assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
        assertEquals(context.responseBody, successBodyString)
    }

}
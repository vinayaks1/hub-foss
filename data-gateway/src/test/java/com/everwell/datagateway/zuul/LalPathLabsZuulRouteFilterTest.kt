package com.everwell.datagateway.zuul

import com.everwell.datagateway.filters.LalPathLabsZuulRouteFilter
import com.everwell.datagateway.service.NikshayRestService
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import com.nhaarman.mockitokotlin2.mock
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.mock.web.MockHttpServletRequest
import kotlin.test.assertEquals
import com.nhaarman.mockitokotlin2.any

@RunWith(MockitoJUnitRunner::class)
class LalPathLabsZuulRouteFilterTest {

    private lateinit var lalPathLabsZuulRouteFilter: LalPathLabsZuulRouteFilter
    val nikshayRestService: NikshayRestService = mock()

    private val nikshayAuthToken = "abcd1234"

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        lalPathLabsZuulRouteFilter = LalPathLabsZuulRouteFilter()
        lalPathLabsZuulRouteFilter.nikshayRestService = nikshayRestService
        Mockito.`when`(nikshayRestService.getAuthToken(any())).thenAnswer { nikshayAuthToken }
    }


    @Test
    fun testZuulRouteFilterFailure() {
        val request = MockHttpServletRequest("GET", "/lalpathlabs/unknownAPICall")
        val context = RequestContext()
        context.setRequest(request)
        RequestContext.testSetCurrentContext(context)
        val result = lalPathLabsZuulRouteFilter.runFilter()
        assertEquals(context["requestURI"], null)
        assertEquals(ExecutionStatus.FAILED, result.getStatus())
    }

}
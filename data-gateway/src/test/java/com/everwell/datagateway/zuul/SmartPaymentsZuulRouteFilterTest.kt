package com.everwell.datagateway.zuul

import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.filters.zuul.smartPayments.SmartPaymentsZuulRouteFilter
import com.everwell.datagateway.service.SmartPaymentsRestService
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.mock.web.MockHttpServletRequest
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class SmartPaymentsZuulRouteFilterTest {

    private lateinit var smartPaymentsZuulRouteFilter: SmartPaymentsZuulRouteFilter
    val smartPaymentsRestService: SmartPaymentsRestService = mock()

    private val authToken = "abcd1234"

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        smartPaymentsZuulRouteFilter = SmartPaymentsZuulRouteFilter()
        smartPaymentsZuulRouteFilter.smartPaymentsRestService = smartPaymentsRestService
        Mockito.`when`(smartPaymentsRestService.getAuthToken(any(), any())).thenAnswer { authToken }
        val appProperties = AppProperties()
        smartPaymentsZuulRouteFilter.appProperties = appProperties
    }

    @Test
    fun testZuulRouteFilterSuccess() {
        val request = MockHttpServletRequest("GET", "/smartpayments/v1/test")
        request.addHeader("client-id", "5112");
        request.addHeader("client-secret", "dadasd");
        val context = RequestContext()
        context.setRequest(request)
        RequestContext.testSetCurrentContext(context)
        val result = smartPaymentsZuulRouteFilter.runFilter();
        assertEquals(context["requestURI"], "/v1/test")
        assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
    }
}
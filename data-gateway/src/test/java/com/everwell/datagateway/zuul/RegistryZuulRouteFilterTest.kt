package com.everwell.datagateway.zuul

import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.filters.zuul.iam.IamZuulRouteFilter
import com.everwell.datagateway.filters.zuul.registry.RegistryZuulRouteFilter
import com.everwell.datagateway.service.IamRestService
import com.everwell.datagateway.service.RegistryRestService
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import com.nhaarman.mockitokotlin2.mock
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.mock.web.MockHttpServletRequest
import kotlin.test.assertEquals
import com.nhaarman.mockitokotlin2.any

@RunWith(MockitoJUnitRunner::class)
class RegistryZuulRouteFilterTest {

    private lateinit var registryZuulRouteFilter: RegistryZuulRouteFilter
    val registryRestService: RegistryRestService = mock()

    private val authToken = "abcd1234"

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        registryZuulRouteFilter = RegistryZuulRouteFilter()
        registryZuulRouteFilter.registryRestService = registryRestService
        Mockito.`when`(registryRestService.getAuthToken(any())).thenAnswer { authToken }
        val appProperties = AppProperties()
        registryZuulRouteFilter.appProperties = appProperties
    }

    @Test
    fun testZuulRouteFilterSuccess() {
        val request = MockHttpServletRequest("GET", "/registry/v1/config/OverviewComponents")
        request.addHeader("client-id", "29")
        val context = RequestContext()
        context.setRequest(request)
        RequestContext.testSetCurrentContext(context)
        val result = registryZuulRouteFilter.runFilter();
        assertEquals(context["requestURI"], "/v1/config/OverviewComponents")
        assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
    }

}
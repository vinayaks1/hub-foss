package com.everwell.datagateway.zuul

import com.everwell.datagateway.filters.zuul.registry.RegistryZuulPostFilter
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import com.nhaarman.mockitokotlin2.mock
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.http.HttpStatus
import org.springframework.mock.web.MockHttpServletRequest
import org.springframework.mock.web.MockHttpServletResponse
import org.springframework.web.client.RestTemplate
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class RegistryZuulPostFilterTest {

    private lateinit var registryZuulPostFilter: RegistryZuulPostFilter

    val mockRestTemplate: RestTemplate = mock()


    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        registryZuulPostFilter = RegistryZuulPostFilter()
        registryZuulPostFilter.restTemplateForApi = mockRestTemplate
    }

    @Test
    fun testZuulPostFilterSuccess(){
        val request = MockHttpServletRequest("POST", "registry/v1/merm/event?noforward=0&SN=862810037703229")
        val response = MockHttpServletResponse()
        val context = RequestContext()
        context.setRequest(request)
        context.response = response
        RequestContext.testSetCurrentContext(context)
        val result = registryZuulPostFilter.runFilter()
        assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
    }

    @Test
    fun testZuulPostFilterFailure(){
        val request = MockHttpServletRequest("POST", "registry/v1/merm/event?noforward=0&SN=862810037703229")
        val response = MockHttpServletResponse()
        val context = RequestContext()
        context.setRequest(request)
        context.response = response
        context.responseStatusCode = HttpStatus.OK.value()
        val registryResponse = "{\n" +
                "  \t\"success\":true,\n" +
                "\"message\":\"OK,2022-10-09T12:00:00,3\"\n" +
                "  }\n"
        context.responseDataStream = registryResponse.byteInputStream()
        RequestContext.testSetCurrentContext(context)
        val result = registryZuulPostFilter.runFilter()
        assertEquals(ExecutionStatus.FAILED, result.getStatus())
    }

    @Test
    fun testZuulPostFilterSuccessGetMermConfig(){
        val request = MockHttpServletRequest("GET", "get-merm-config?noforward=0&SN=862810037703229")
        val response = MockHttpServletResponse()
        val context = RequestContext()
        context.requestQueryParams = mapOf("SN" to listOf("862810037703229"), "noforward" to listOf("0"))
        context.setRequest(request)
        context.response = response
        RequestContext.testSetCurrentContext(context)
        val result = registryZuulPostFilter.runFilter()
        assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
    }

    @Test
    fun testZuulPostFilterGetMermConfig(){
        val request = MockHttpServletRequest("GET", "get-merm-config?noforward=0&SN=862810037703229")
        val response = MockHttpServletResponse()
        val context = RequestContext()
        context.requestQueryParams = mapOf("SN" to listOf("862810037703229"), "noforward" to listOf("0"))
        context.setRequest(request)
        context.response = response
        context.responseGZipped = false
        context.responseStatusCode = HttpStatus.OK.value()
        val registryResponse = "{\n" +
                "\t\"success\": true,\n" +
                "\t\"message\": \"\",\n" +
                "\t\"data\": {\n" +
                "\t\t\"idString\": null,\n" +
                "\t\t\"medAlarmTime\": \"22:30\",\n" +
                "\t\t\"refillDate\": \"19/07/23:22:30\",\n" +
                "\t\t\"medAlarmEnabled\": \"1\",\n" +
                "\t\t\"refillAlarmEnabled\": null,\n" +
                "\t\t\"rtHours\": \"1\",\n" +
                "\t\t\"repeatDays\": \"127\",\n" +
                "\t\t\"lidFeedback\": \"1\",\n" +
                "\t\t\"ntpServer\": null,\n" +
                "\t\t\"timeZoneOffset\": \"22\"\n" +
                "\t}\n" +
                "}"
        context.responseDataStream = registryResponse.byteInputStream()
        RequestContext.testSetCurrentContext(context)
        val result = registryZuulPostFilter.runFilter()
        assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
    }

    @Test
    fun testZuulPostFilterGetMermConfigDataNull(){
        val request = MockHttpServletRequest("GET", "get-merm-config?noforward=0&SN=862810037703229")
        val response = MockHttpServletResponse()
        val context = RequestContext()
        context.requestQueryParams = mapOf("SN" to listOf("862810037703229"), "noforward" to listOf("0"))
        context.setRequest(request)
        context.response = response
        context.responseGZipped = false
        context.responseStatusCode = HttpStatus.OK.value()
        val registryResponse = "{\n" +
                "\t\"success\": true,\n" +
                "\t\"message\": \"\",\n" +
                "\t\"data\": null}"
        context.responseDataStream = registryResponse.byteInputStream()
        RequestContext.testSetCurrentContext(context)
        val result = registryZuulPostFilter.runFilter()
        assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
    }

    @Test
    fun testZuulPostFilterFailureGetMermConfig(){
        val request = MockHttpServletRequest("GET", "get-merm-config?noforward=0&SN=862810037703229")
        val response = MockHttpServletResponse()
        val context = RequestContext()
        context.setRequest(request)
        context.response = response
        RequestContext.testSetCurrentContext(context)
        val result = registryZuulPostFilter.runFilter()
        assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
    }
}
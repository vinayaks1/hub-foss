package com.everwell.datagateway.zuul

import com.everwell.datagateway.filters.zuul.registry.RegistryZuulPostFilter
import com.everwell.datagateway.filters.zuul.registry.RegistryZuulPreFilter
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import com.nhaarman.mockitokotlin2.mock
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.http.HttpStatus
import org.springframework.mock.web.MockHttpServletRequest
import org.springframework.mock.web.MockHttpServletResponse
import org.springframework.web.client.RestTemplate
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class RegistryZuulPreFilterTest {
    private lateinit var registryZuulPreFilter: RegistryZuulPreFilter

    val mockRestTemplate: RestTemplate = mock()


    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        registryZuulPreFilter = RegistryZuulPreFilter()
        registryZuulPreFilter.restTemplateForApi = mockRestTemplate
    }

    @Test
    fun testZuulPretFilterSuccessForwardToNikshay(){
        val request = MockHttpServletRequest("POST", "http://localhost:8080/registry/v1/merm/event?noforward=0&SN=862810037703229")
        request.queryString = "noforward=0&SN=862810037703229"
        val context = RequestContext()
        context.setRequest(request)
        RequestContext.testSetCurrentContext(context)
        val result = registryZuulPreFilter.runFilter()
        assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
    }

    @Test
    fun testZuulPretFilterSuccess(){
        val request = MockHttpServletRequest("POST", "http://localhost:8080/registry/v1/merm/event?noforward=0&SN=862810037703229")
        request.queryString = "noforward=1&SN=862810037703229"
        val context = RequestContext()
        context.setRequest(request)
        RequestContext.testSetCurrentContext(context)
        val result = registryZuulPreFilter.runFilter()
        assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
    }

}
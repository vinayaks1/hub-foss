package com.everwell.datagateway.zuul

import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.constants.SecurityConstants
import com.everwell.datagateway.enums.LegacyRoutesEnum
import com.everwell.datagateway.filters.zuul.callLogs.CallLogsZuulRouteFilter
import com.everwell.datagateway.service.RegistryRestService
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.http.HttpMethod
import org.springframework.mock.web.MockHttpServletRequest
import org.springframework.web.client.RestTemplate
import java.net.URL
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class CallLogsRouteFilterTest {

    private lateinit var callLogsZuulRouteFilter: CallLogsZuulRouteFilter
    val registryRestService: RegistryRestService = mock()

    val mockRestTemplate: RestTemplate = mock()
    private val authToken = "abcd1234"

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        callLogsZuulRouteFilter = CallLogsZuulRouteFilter()
        callLogsZuulRouteFilter.registryRestService = registryRestService
        Mockito.`when`(registryRestService.getAuthToken(any())).thenAnswer { authToken }
        callLogsZuulRouteFilter.restTemplateForApi = mockRestTemplate
    }

    @Test
    fun testZuulRouteFilterSuccessTestFetchClientIdFromRegistry() {
        val appProperties = AppProperties()
        appProperties.registryServerUrl = "http://localhost:9210"
        appProperties.clientIdCallLogs = "63"
        callLogsZuulRouteFilter.appProperties = appProperties
        val request = MockHttpServletRequest(HttpMethod.POST.name, LegacyRoutesEnum.CALL_LOGS.path)
        val context = RequestContext()
        context.setRequest(request)
        context.routeHost = URL("http://localhost:9210")
        RequestContext.testSetCurrentContext(context)
        val result = callLogsZuulRouteFilter.runFilter();
        assertEquals(context["requestURI"], "/v1/africa-talking-ascent-call-back")
        assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
    }

}
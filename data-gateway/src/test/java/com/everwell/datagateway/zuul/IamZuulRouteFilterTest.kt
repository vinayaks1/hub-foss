package com.everwell.datagateway.zuul

import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.filters.zuul.iam.IamZuulRouteFilter
import com.everwell.datagateway.service.IamRestService
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import com.nhaarman.mockitokotlin2.mock
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.mock.web.MockHttpServletRequest
import kotlin.test.assertEquals
import com.nhaarman.mockitokotlin2.any
import com.everwell.datagateway.constants.Constants.CLIENT_ID

@RunWith(MockitoJUnitRunner::class)
class IamZuulRouteFilterTest {

    private lateinit var iamZuulRouteFilter: IamZuulRouteFilter
    val iamRestService: IamRestService = mock()

    private val authToken = "abcd1234"

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        iamZuulRouteFilter = IamZuulRouteFilter()
        iamZuulRouteFilter.iamRestService = iamRestService
        Mockito.`when`(iamRestService.getAuthToken(any())).thenAnswer { authToken }
        val appProperties = AppProperties()
        iamZuulRouteFilter.appProperties = appProperties
    }

    @Test
    fun testZuulRouteFilterSuccess() {
        val request = MockHttpServletRequest("GET", "/iam/v1/adherence")
        request.addHeader(CLIENT_ID, "29")
        val context = RequestContext()
        context.setRequest(request)
        RequestContext.testSetCurrentContext(context)
        val result = iamZuulRouteFilter.runFilter();
        assertEquals(context["requestURI"], "/v1/adherence")
        assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
    }

}
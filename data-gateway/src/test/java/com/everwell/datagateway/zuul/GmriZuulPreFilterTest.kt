package com.everwell.datagateway.zuul

import com.everwell.datagateway.filters.zuul.gmri.GmriZuulPreFilter
import com.netflix.zuul.ExecutionStatus
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.monitoring.MonitoringHelper
import com.nhaarman.mockitokotlin2.mock
import junit.framework.TestCase.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.mock.web.MockHttpServletRequest
import org.springframework.web.client.RestTemplate

@RunWith(MockitoJUnitRunner::class)
class GmriZuulPreFilterTest {
    lateinit var gmriZuulPreFilter: GmriZuulPreFilter

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        MonitoringHelper.initMocks()
        gmriZuulPreFilter = GmriZuulPreFilter()
    }

    @Test
    fun testZuulPreFilterSuccessGetToPost(){
        val request = MockHttpServletRequest("GET", "http://localhost:8080/gmri/v1/imei?imei=862810037703229")
        request.queryString = "imei=862810037703229"
        val context = RequestContext()
        context.setRequest(request)
        RequestContext.testSetCurrentContext(context)
        val result = gmriZuulPreFilter.runFilter()
        assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
    }

    @Test
    fun testZuulPreFilterSuccess(){
        val request = MockHttpServletRequest("GET", "http://localhost:8080/gmri/v1/imei")
        val context = RequestContext()
        context.setRequest(request)
        RequestContext.testSetCurrentContext(context)
        val result = gmriZuulPreFilter.runFilter()
        assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
    }

    @Test
    fun testZuulPreFilterSuccessGetToPostUsingExternalId(){
        val request = MockHttpServletRequest("GET", "http://localhost:8080/gmri/v1/imei?entityId=123")
        request.queryString = "entityId=123"
        val context = RequestContext()
        context.setRequest(request)
        RequestContext.testSetCurrentContext(context)
        val result = gmriZuulPreFilter.runFilter()
        assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
    }

    @Test
    fun testZuulPreFilterSuccessAllAvailableImei(){
        val request = MockHttpServletRequest("GET", "http://localhost:8080/gmri/v1/all-imei/available")
        val context = RequestContext()
        context.setRequest(request)
        RequestContext.testSetCurrentContext(context)
        val result = gmriZuulPreFilter.runFilter()
        assertEquals(ExecutionStatus.SUCCESS, result.getStatus())
    }
}
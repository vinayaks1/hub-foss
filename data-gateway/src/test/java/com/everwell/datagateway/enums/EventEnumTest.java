package com.everwell.datagateway.enums;

import org.junit.jupiter.api.Test;

import static com.everwell.datagateway.constants.Constants.ADD_TEST_EVENT;
import static org.junit.jupiter.api.Assertions.*;

class EventEnumTest {

    @Test
    void getMatch() {
        String eventName = ADD_TEST_EVENT;
        EventEnum eventEnum = EventEnum.getMatch(eventName);
        assertTrue(eventEnum.getEventName().equals(eventName));
    }
}
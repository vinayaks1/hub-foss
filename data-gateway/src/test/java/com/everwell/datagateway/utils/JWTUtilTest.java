package com.everwell.datagateway.utils;

import com.everwell.datagateway.BaseTest;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


class JWTUtilTest extends BaseTest {


    @Test
    void createAndParseJWT() {

        JWTUtil jwtUtil=new JWTUtil();
        Authentication authentication = mock(Authentication.class);
        List<GrantedAuthority> authorities = new ArrayList<>();
        User user = new User("Heisenberg", "test", authorities);
        when(authentication.getPrincipal()).thenReturn(user);


        String generatedJwtToken =jwtUtil.createJWT(authentication);
        //Should return a NotNull Jwt Token
        assertNotNull(generatedJwtToken);

        //The username for the generated token should be equal to the one as mocked
        String userName = jwtUtil.parseJWT(generatedJwtToken);
        assertEquals("Heisenberg", userName);


    }


}
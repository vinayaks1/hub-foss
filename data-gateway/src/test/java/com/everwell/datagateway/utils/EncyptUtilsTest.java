package com.everwell.datagateway.utils;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.models.dto.EncryptionRequest;
import com.everwell.datagateway.models.dto.EncryptionResponse;
import com.everwell.datagateway.models.dto.KeyMaterial;

import org.junit.Assert;
import org.junit.Test;

public class EncyptUtilsTest extends BaseTest {


    @Test
    public void encryptTest() throws Exception {

        //prepare keys
        KeyMaterial senderKeyMaterial = EncryptionUtil.generate();
        KeyMaterial receiverKeyMaterial = EncryptionUtil.generate();
        EncryptionRequest encryptionRequest = new EncryptionRequest(receiverKeyMaterial.getPublicKey(),receiverKeyMaterial.getNonce(),senderKeyMaterial,"test");

        //encrypt & decrypt
        EncryptionResponse encryptionResponse = EncryptionUtil.encrypt(encryptionRequest);
        byte[] xorOfRandom = EncryptionUtil.xorOfRandom(senderKeyMaterial.getNonce(),receiverKeyMaterial.getNonce());
        String decryptedData = EncryptionUtil.decrypt(xorOfRandom,receiverKeyMaterial.getPrivateKey(),senderKeyMaterial.getPublicKey(),encryptionResponse.getEncryptedData());

        // Assert
        Assert.assertEquals(EncryptionUtil.getBase64String(EncryptionUtil.getEncodedHIPPublicKey(EncryptionUtil.getKey(senderKeyMaterial.getPublicKey()))),encryptionResponse.getKeyToShare());
        Assert.assertEquals("test",decryptedData);
    }
}

package com.everwell.datagateway.utils;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.validation.FhirValidator;
import ca.uhn.fhir.validation.ValidationResult;
import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.constants.Constants;
import com.everwell.datagateway.enums.TypeOfEpisodeEnum;
import com.everwell.datagateway.models.dto.DiagnosticsFHIRDto;
import com.everwell.datagateway.models.dto.DispensationFHIRDto;
import com.everwell.datagateway.models.request.EpisodeSearchRequest;
import com.everwell.datagateway.models.response.EpisodeIndex;
import com.fasterxml.jackson.databind.JsonNode;
import org.bouncycastle.util.test.FixedSecureRandom;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Enumerations;
import org.hl7.fhir.r4.model.Patient;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.parameters.P;

import javax.servlet.http.HttpServletRequest;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

public class FhirUtilTest extends BaseTest {

    private static Logger LOGGER = LoggerFactory.getLogger(FhirUtilTest.class);

    @Test
    public void validateTest(){
        FhirValidator fhirValidator = new FhirValidator(FhirContext.forR4());
        ValidationResult result =  FhirUtil.validate(fhirValidator,new Bundle());
        assertFalse(result.isSuccessful());
    }

    @Test
    public void loadProfilesTest() throws FileNotFoundException {

        FhirContext fhirContext = FhirContext.forR4();
        FhirValidator validator = FhirUtil.loadProfiles(fhirContext,"filesystem:src/test/resources/fhir-test-resource/");
        ValidationResult validationResult = FhirUtil.validate(validator,FhirUtil.populateDiagnosticReportLabBundle(getTestDiagnosticsFHIRDto()).get(0));
        assertTrue(validationResult.isSuccessful());
        // TODO - fix validation errors and assert true.
    }

    @Test
    public void populateDiagnosticReportLabBundleTest() {
        Bundle diagnosticBundles = FhirUtil.populateDiagnosticReportLabBundle(getTestDiagnosticsFHIRDto()).get(0);
        assertEquals(diagnosticBundles.getEntry().get(0).getFullUrl(),"Composition/1");
        assertEquals(diagnosticBundles.getEntry().get(1).getFullUrl(),"Patient/1");
        assertEquals(diagnosticBundles.getEntry().get(2).getFullUrl(),"Practitioner/1");
        assertEquals(diagnosticBundles.getEntry().get(3).getFullUrl(),"DiagnosticReport/1");

    }

    @Test
    public void populateDispensationBundleTest() {
        Bundle diagnosticBundles = FhirUtil.populateDispensationBundle(getTestDispensationFHIRDto());
        assertEquals(diagnosticBundles.getEntry().get(1).getFullUrl(),"Patient/1");
        assertEquals(diagnosticBundles.getEntry().get(2).getFullUrl(),"Organization/1");
        assertEquals(diagnosticBundles.getEntry().get(3).getFullUrl(),"MedicationRequest/1");
    }

    private DiagnosticsFHIRDto getTestDiagnosticsFHIRDto() {
        Map<String, DiagnosticsFHIRDto.TestDetails> testDetailsMap = new HashMap<>();
        testDetailsMap.put("1",new DiagnosticsFHIRDto.TestDetails("microscopy","positive"));
        return new DiagnosticsFHIRDto(1,1,1,"test_hierarchy",testDetailsMap);
    }

    private DispensationFHIRDto getTestDispensationFHIRDto() {
        Map<String, DispensationFHIRDto.DispensationDetails> dispensationDetailsMap = new HashMap<>();
        dispensationDetailsMap.put("1", new DispensationFHIRDto.DispensationDetails(true,"test_product","2022-07-31"));
        return new DispensationFHIRDto(1,1,1,"test_hierarchy",dispensationDetailsMap);
    }

    @Test
    public void populatePatientResourceTest() {
        EpisodeIndex episodeIndex = new EpisodeIndex("1");
        episodeIndex.setDeleted(false);
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("firstName", "test");
        stageData.put("treatmentOutcome", "cured");
        stageData.put("gender", "Male");
        episodeIndex.setStageData(stageData);
        List<EpisodeIndex> episodeIndexList = new ArrayList<>();
        episodeIndexList.add(episodeIndex);
        List<Patient> response = FhirUtil.populatePatientResource(episodeIndexList);
        Assert.assertEquals(response.get(0).getGender(), Enumerations.AdministrativeGender.MALE);
    }

    @Test
    public void populatePatientBundleTest() {
        Patient patient = new Patient();
        patient.setGender(Enumerations.AdministrativeGender.FEMALE);
        patient.setId("1");
        List<Patient> patientList = new ArrayList<>();
        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        Map<String, String[]> parameterMap = new HashMap<>();
        parameterMap.put("_id", new String[]{"1"});
        EpisodeSearchRequest searchRequest = new EpisodeSearchRequest(0, 0, Arrays.asList(
                Constants.ID, Constants.DELETED, Constants.FIRST_NAME, Constants.LAST_NAME, Constants.PRIMARY_PHONE_NUMBER, Constants.EMAIL_LIST, Constants.DATE_OF_BIRTH, Constants.TREATMENT_OUTCOME, Constants.ADDRESS, Constants.PIN_CODE, Constants.GENDER
        ), TypeOfEpisodeEnum.getTypeOfEpisodeList());
        searchRequest.setSearch(new EpisodeSearchRequest.SupportedSearchMethods(new HashMap<>(), new HashMap<>(), new HashMap<>()));
        String queryString = "_count=10&_page=0";
        when(request.getQueryString()).thenReturn(queryString);
        String requestURL = "http://localhost:8080/v1/Patient";
        when(request.getRequestURL()).thenReturn(new StringBuffer(requestURL));
        JsonNode response = FhirUtil.populatePatientBundle(patientList, request, 1L, searchRequest);
        Assert.assertEquals(response.get("total").toString(), "1");
    }

    @Test
    public void generateNextPageLinkTest() {
        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        String queryString = "_count=10&_page=0";
        when(request.getQueryString()).thenReturn(queryString);
        String requestURL = "http://localhost:8080/v1/Patient";
        when(request.getRequestURL()).thenReturn(new StringBuffer(requestURL));
        String nextPageLink = FhirUtil.generateNextPageLink(request, 1);
        String expectedNextPageLink = "http://localhost:8080/v1/Patient?_count=10&_page=2";
        assertEquals(expectedNextPageLink, nextPageLink);
    }

}

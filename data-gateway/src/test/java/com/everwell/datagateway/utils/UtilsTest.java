package com.everwell.datagateway.utils;

import com.everwell.datagateway.exceptions.ValidationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class UtilsTest {


    @Test
    void readRequestBodyNullInput() throws Exception {
        assertNull(Utils.readRequestBody(null));
    }

    @Test
    void readRequestBodyValidInput() throws Exception {
        String input = "test";
        InputStream inputStream = new ByteArrayInputStream(input.getBytes());
        String result = Utils.readRequestBody(inputStream);
        assertTrue(result.equals(input));
    }

    @Test
    void getRemainingTime() {
        Calendar c = Calendar.getInstance();
        Integer answer = (86400 - ((c.get(Calendar.HOUR_OF_DAY) * 60 + c.get(Calendar.MINUTE)) * 60));
        Integer answerReturned = Utils.getRemainingTime();
        assertEquals(answerReturned, answer);
    }

    @Test
    void processResponseEntityNullResponseEntityTest() throws JsonProcessingException {
        ValidationException exception = Assertions.assertThrows(ValidationException.class, () -> {
            Utils.processResponseEntity(null, 200, String.class, "errorMessage");
        });
        assertEquals("errorMessage", exception.getMessage());
    }

    @Test
    void processResponseEntityNullBodyTest() {
        ResponseEntity<String> responseEntity = new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        ValidationException exception = Assertions.assertThrows(ValidationException.class, () -> {
            Utils.processResponseEntity(responseEntity, 200, String.class, "notFound");
        });
        assertEquals("notFound", exception.getMessage());
    }

    @Test
    void processResponseEntityUnsuccessfulResponseTest() {
        ResponseEntity<String> responseEntity = new ResponseEntity<>("Not found response", HttpStatus.NOT_FOUND);
        ValidationException exception = Assertions.assertThrows(ValidationException.class, () -> {
            Utils.processResponseEntity(responseEntity, 200, String.class, "notFound");
        });
        assertEquals("Not found response", exception.getMessage());
    }

    @Test
    void processResponseEntityTest() throws JsonProcessingException {
        ResponseEntity<String> responseEntity = new ResponseEntity<>("{\"test\":\"value\"}", HttpStatus.OK);
        Map<String, String> result = Utils.processResponseEntity(responseEntity, 200, Map.class, "errorMessage");
        assertEquals(1, result.size());
        assertEquals("value", result.get("test"));
    }

    @Test
    void buildURIStringTest() {
        String url = "http://test.com/api/test";
        Map<String, Object> params = new HashMap<>();
        params.put("param1", "value");
        String uri = Utils.buildURIString(url, params);
        assertEquals("http://test.com/api/test?param1=value", uri);
    }

    @Test
    void buildURIStringTestWithoutParams() {
        String url = "http://test.com/api";
        Map<String, Object> params = new HashMap<>();
        String uri = Utils.buildURIString(url, params);
        assertEquals("http://test.com/api", uri);
    }

}
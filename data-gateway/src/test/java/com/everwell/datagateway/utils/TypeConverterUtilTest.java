package com.everwell.datagateway.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import static org.mockito.ArgumentMatchers.any;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


class TypeConverterUtilTest {


    @Test
    void byteArrayToString() {

        //Non Empty Input
        byte[] byteArray = {'T', 'E', 'S', 'T'};
        String string = TypeConverterUtil.byteArrayToString(byteArray);
        assertEquals("TEST", string);

        //Empty Input
        byte[] newByte = new byte[0];
        String newByteString = TypeConverterUtil.byteArrayToString(newByte);
        assertEquals("", newByteString);

    }


    @Test
    void convertToJsonStringThrowException() throws JsonProcessingException {
        ObjectMapper om = Mockito.spy(new ObjectMapper());
        Mockito.when( om.writeValueAsString(any())).thenThrow(RuntimeException.class);   //Mock
        assertThrows(RuntimeException.class, () -> TypeConverterUtil.convertToJsonString(new Object()));
    }
}
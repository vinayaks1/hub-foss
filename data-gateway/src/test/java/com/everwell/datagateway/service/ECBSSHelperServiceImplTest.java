package com.everwell.datagateway.service;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.exceptions.RestTemplateException;
import com.everwell.datagateway.models.response.ecbss.TrackedEntityInstancesResponse;
import com.everwell.datagateway.service.impl.ECBSSHelperServiceImpl;
import com.everwell.datagateway.utils.Utils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

public class ECBSSHelperServiceImplTest extends BaseTest {

    @Spy
    @InjectMocks
    ECBSSHelperServiceImpl ecbssHelperService;

    @Mock
    RestTemplate restTemplate;

    @Mock
    RestService restService;

    private final String test = "Test";
    private final String error = "error";
    private final String success = "Success";
    private final String url = "http://test:8080";

    @Test
    void getTrackedEntityInstanceTest() {
        setCredentials();
        TrackedEntityInstancesResponse trackedEntityInstancesResponse = new TrackedEntityInstancesResponse();
        ResponseEntity<String> responseEntity = new ResponseEntity<>(Utils.asJsonString(trackedEntityInstancesResponse), HttpStatus.OK);
        when(restTemplate.exchange(anyString(), eq(HttpMethod.GET), any(HttpEntity.class), eq(String.class)))
                .thenReturn(responseEntity);
        TrackedEntityInstancesResponse response = ecbssHelperService.getTrackedEntityInstance(test, test);
        assertNotNull(response);
    }

    @Test
    void getTrackedEntityInstanceTestError() {
        setCredentials();
        when(restTemplate.exchange(anyString(), eq(HttpMethod.GET), any(HttpEntity.class), eq(String.class)))
                .thenThrow(RestTemplateException.class);
        TrackedEntityInstancesResponse response = ecbssHelperService.getTrackedEntityInstance(test, test);
        Assertions.assertNull(response);
    }

    @Test
    void getTrackedEntityInstanceTestErrorNoResponse() {
        setCredentials();
        TrackedEntityInstancesResponse trackedEntityInstancesResponse = new TrackedEntityInstancesResponse();
        ResponseEntity<String> responseEntity = new ResponseEntity<>(Utils.asJsonString(trackedEntityInstancesResponse), HttpStatus.BAD_REQUEST);
        when(restTemplate.exchange(anyString(), eq(HttpMethod.GET), any(HttpEntity.class), eq(String.class)))
                .thenReturn(responseEntity);
        TrackedEntityInstancesResponse response = ecbssHelperService.getTrackedEntityInstance(test, test);
        assertNull(response);
    }

    private void setCredentials() {
        ecbssHelperService.setBaseUrl(url);
        ecbssHelperService.setUsername(test);
        ecbssHelperService.setPassword(test);
    }

}

package com.everwell.datagateway.service;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.entities.Client;
import com.everwell.datagateway.enums.StatusCodeEnum;
import com.everwell.datagateway.exceptions.LoginException;
import com.everwell.datagateway.models.request.AuthenticationRequest;
import com.everwell.datagateway.models.request.RegisterRequest;
import com.everwell.datagateway.utils.JWTUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


class AuthenticationServiceImplTest extends BaseTest {

    @InjectMocks
    AuthenticationServiceImpl authenticationService;

    @Mock
    AuthenticationManager authenticationManager;

    @Mock
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Mock
    UserDetailsServiceImpl userDetailsService;

    @Mock
    ClientService clientService;

    @Mock
    ClientAuthenticationService clientAuthenticationService;

    private String testUsername = "everwell";

    @Test
    void registerClient() {
        //In case of null input
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setUsername(null);
        Boolean answer = authenticationService.registerClient(registerRequest);
        assertEquals(false, answer);
    }

    @Test
    void getCurrentUsername() {
        Authentication authentication = mock(Authentication.class);
        when(authentication.getName()).thenReturn(testUsername);
        String expectedUsername = authenticationService.getCurrentUsername(authentication);
        assertEquals(testUsername, expectedUsername);
    }


    @Test
    void generateToken() {
        AuthenticationRequest authenticationRequest = new AuthenticationRequest();
        authenticationRequest.setUsername("Heisenberg");
        authenticationRequest.setPassword("test");
        Authentication authentication = mock(Authentication.class);
        List<GrantedAuthority> authorities = new ArrayList<>();
        User user = new User(authenticationRequest.getUsername(), authenticationRequest.getUsername(), authorities);
        when(authentication.getPrincipal()).thenReturn(user);
        when(authenticationManager.authenticate(any())).thenReturn(authentication);
        String returnedToken = authenticationService.generateToken(authenticationRequest);
        assertNotNull(returnedToken);  // Token generation success

        //The username for the generated token should be equal
        String userName = JWTUtil.parseJWT(returnedToken);
        assertEquals("Heisenberg", userName);
    }

    @Test
    void generateTokenBadCredentials() {
        AuthenticationRequest authenticationRequest = new AuthenticationRequest();
        authenticationRequest.setUsername("Heisenberg");
        authenticationRequest.setPassword("test");
        when(authenticationManager.authenticate(any())).thenThrow(BadCredentialsException.class);
        LoginException loginException = Assertions.assertThrows(LoginException.class, () -> {
            authenticationService.generateToken(authenticationRequest);
        });
        StatusCodeEnum invalidCredentials = StatusCodeEnum.INVALID_CREDENTIALS;
        assertEquals(invalidCredentials, loginException.getStatusCodeEnum());
        assertEquals(invalidCredentials.getMessage(), loginException.getMessage());
    }

    @Test
    void testRegisterClientSuccess() {
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setUsername("test");
        registerRequest.setPassword("test");
        registerRequest.setPassword("test@gmail.com");
        when(clientService.isClientExists(registerRequest.getUsername())).thenReturn(false);
        doNothing().when(clientAuthenticationService).validateCredentials(any());
        doNothing().when(clientAuthenticationService).saveAuthenticationDetails(any());
        Boolean returned = authenticationService.registerClient(registerRequest);
        assertEquals(true, returned);
        verify(clientAuthenticationService, Mockito.times(1)).saveAuthenticationDetails(any());
        verify(clientAuthenticationService, Mockito.times(1)).validateCredentials(any());
    }

    @Test
    void testRegisterClientTestWithAccessibleCLient() {
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setUsername("test");
        registerRequest.setPassword("test");
        registerRequest.setPassword("test@gmail.com");
        registerRequest.setAccessibleClient(1L);
        when(clientService.isClientExists(registerRequest.getUsername())).thenReturn(false);
        doNothing().when(clientAuthenticationService).validateCredentials(any());
        doNothing().when(clientAuthenticationService).saveAuthenticationDetails(any());
        Boolean returned = authenticationService.registerClient(registerRequest);
        assertEquals(true, returned);
        verify(clientAuthenticationService, Mockito.times(1)).saveAuthenticationDetails(any());
        verify(clientAuthenticationService, Mockito.times(1)).validateCredentials(any());
    }


}
package com.everwell.datagateway.service

import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.entities.Client
import com.everwell.datagateway.models.response.GenericAuthResponse
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.http.HttpEntity
import org.springframework.web.client.RestTemplate
import kotlin.reflect.jvm.internal.impl.load.kotlin.JvmType

@RunWith(MockitoJUnitRunner.Silent::class)
class HubRestServiceTest {
    private var hubRestServiceImp : HubRestServiceImp = HubRestServiceImp()

    @Test
    fun testAuthentication(){
        val clientService : ClientService = mock()
        val restTemplate : RestTemplate = mock()
        val restService : RestService = mock()
        val client : Client = mock()
        val httpEntity : HttpEntity<JvmType.Object> = mock()
        val authenticationService : AuthenticationService = mock()

        val appProperties = AppProperties()
        appProperties.hubServerUrl = "http://localhost.com/"
        appProperties.hubUserGrantType=""
        appProperties.hubUserName= "umang"
        appProperties.hubUserPassword= ""

        hubRestServiceImp.appProperties = appProperties
        hubRestServiceImp.restService = restService
//        hubRestServiceImp.restTemplateForAuthentication = restTemplate
        hubRestServiceImp.clientService = clientService
        hubRestServiceImp.authenticationService = authenticationService

        val genericAuthResponseMock = GenericAuthResponse("abcded1234",259999)
        Mockito.`when`(restService.buildHeaderAndRequest(ArgumentMatchers.anyMap())).thenAnswer { httpEntity }
        Mockito.`when`(clientService.findByUsername("umang")).thenAnswer { client }
        Mockito.`when`(authenticationService.getCurrentUsername(any())).thenAnswer { ArgumentMatchers.anyString() }
        Mockito.`when`(restTemplate.postForObject("http://localhost.com/Token", httpEntity, GenericAuthResponse::class.java)).thenAnswer{genericAuthResponseMock}
//        val genericAuthResponse = hubRestServiceImp.authenticate()
//        Assert.assertEquals(genericAuthResponseMock.access_token, genericAuthResponse);

    }
}
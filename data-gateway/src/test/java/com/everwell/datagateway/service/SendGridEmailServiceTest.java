package com.everwell.datagateway.service;

import com.everwell.datagateway.BaseTest;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


class SendGridEmailServiceTest extends BaseTest {

    @InjectMocks
    SendGridEmailService sendGridEmailService;

    @Mock
    private SendGrid sendGrid;

    private String from = "test@gmail.com";
    private String to = "test2@gmail.com";
    private String subject = "test";


    @Test
    void makeContentClient() {
        //Make content on valid input
        String content = sendGridEmailService.makeContentClient("eventName", "Error!");
        assertNotNull(content);
    }

    @Test
    void makeContentDeveloper() {
        //Make content on valid input
        String content = sendGridEmailService.makeContentDeveloper("test", "eventName", "Message", "Error!");
        assertNotNull(content);
    }


    @Test
    void sendEmail() throws IOException {
        Content content = new Content("text/plain", "test");
        Mail mail = new Mail(new Email(from), subject, new Email(to), content);
        Response response = new Response();
        response.setStatusCode(200);
        when(sendGrid.api(any())).thenReturn(response);
        Response responseReturned = sendGridEmailService.sendEmail(from, to, subject, content);
        assertEquals(response.getStatusCode(), responseReturned.getStatusCode());
    }

    @Test
    void sendEmailThrowsException() throws IOException {
        Content content = new Content("text/plain", "test");
        Mail mail = new Mail(new Email(from), subject, new Email(to), content);
        Response response = new Response(); // will return just new Response()
        when(sendGrid.api(any())).thenThrow(IOException.class);   //throw Exception
        Response responseReturned = sendGridEmailService.sendEmail(from, to, subject, content);
        assertEquals(response.getStatusCode(), responseReturned.getStatusCode());

    }

    @Test
    void sendText() throws IOException {
        SendGridEmailService sendGridEmailService = new SendGridEmailService(sendGrid);
        when(sendGrid.api(any())).thenReturn(new Response());
        sendGridEmailService.sendText(from, to, subject, "body");
        verify(sendGrid, times(1)).api(any());
    }
}
package com.everwell.datagateway.service;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.constants.QueueConstants;
import com.everwell.datagateway.entities.*;
import com.everwell.datagateway.enums.EmailSeverityEnum;
import com.everwell.datagateway.enums.StatusCodeEnum;
import com.everwell.datagateway.exceptions.NotFoundException;
import com.everwell.datagateway.exceptions.RestTemplateException;
import com.everwell.datagateway.exceptions.URLDisabledException;
import com.everwell.datagateway.models.dto.AuthenticationDetailsDto;
import com.everwell.datagateway.models.dto.PublishToSubscriberUrlDTO;
import com.everwell.datagateway.models.request.FailureEmailContent;
import com.everwell.datagateway.models.response.RestServiceResponse;
import com.everwell.datagateway.repositories.EventClientRepository;
import com.everwell.datagateway.utils.CacheUtils;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import static com.everwell.datagateway.constants.Constants.REDIS_PREFIX;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;


class ConsumerServiceTest extends BaseTest {


    @InjectMocks
    ConsumerService consumerService;

    @Mock
    private RestService restService;

    @Mock
    private EmailService sendGridEmailService;

    @Mock
    private SubscriberUrlService subscriberUrlService;

    @Mock
    private EventClientRepository eventClientRepository;

    @Mock
    private EventService eventService;

    @Mock
    private ClientService clientService;

    @Mock
    private ClientAuthenticationService clientAuthenticationService;

    @Mock
    private ClientRequestsService clientRequestsService;

    @Mock
    private OutgoingWebhookLogService outgoingWebhookLogService;

    @Mock
    RedisTemplate<String, Object> redisTemplate;

    @Mock
    private ValueOperations valueOperations;

    CacheUtils cacheUtils;

    @BeforeEach
    public void init() {
        cacheUtils = new CacheUtils(redisTemplate);
    }

    private String DUMMY_KEY = "1";
    private Client client = new Client();
    private long incrementBy = 1;

    public Client getClient() {
        Client client = new Client();
        client.setUsername("test");
        return client;
    }

    public Event getEvent() {
        Event event = new Event();
        event.setEventName("test");
        return  event;
    }

    @Test
    void publishToSubscriberUrlEventNotFoundException1() {
        NotFoundException notFoundException = assertThrows(NotFoundException.class, () -> consumerService.publishToSubscriberUrl(new PublishToSubscriberUrlDTO("test", null, "Test Message", 1L, false)));
        StatusCodeEnum notFoundSentry = StatusCodeEnum.EVENT_NOT_FOUND_SENTRY;
        assertEquals(notFoundSentry.getCode().value(), notFoundException.getStatusCode());
    }

    @Test
    void publishToSubscriberUrlEventNotFoundException2() {
        //Invalid or Null Parameter for clientName and EventName => Will throw an custom Database exception, as coded
        assertThrows(NotFoundException.class, () -> consumerService.publishToSubscriberUrl(new PublishToSubscriberUrlDTO("test", null, "Test Message", 1L, false)));
    }

    @Test
    void publishToSubscriberUrlClientNotFoundException3() {
        when(clientService.findByUsername(any())).thenReturn(null);
        when(eventService.findEventByEventName(any())).thenReturn(getEvent());
        NotFoundException notFoundException = assertThrows(NotFoundException.class, () -> consumerService.publishToSubscriberUrl(new PublishToSubscriberUrlDTO(null, "test", "Test Message", 1L, false)));
        StatusCodeEnum notFoundSentry = StatusCodeEnum.CLIENT_NOT_FOUND_SENTRY;
        assertEquals(notFoundSentry.getCode().value(), notFoundException.getStatusCode());
    }

    @Test
    void publishToSubscriberUrlClientNotFoundException() {
        Event event = new Event();
        event.setEventName("test");
        when(clientService.findByUsername(any())).thenReturn(null);
        when(eventService.findEventByEventName(any())).thenReturn(getEvent());
        assertThrows(NotFoundException.class, () -> consumerService.publishToSubscriberUrl(new PublishToSubscriberUrlDTO(null, "test", "Test Message", 1L, false)));
    }

    @Test
    void publishToSubscriberUrlTestSubscriberUrlNull() {
        when(clientService.findByUsername(any())).thenReturn(new Client());
        when(eventService.findEventByEventName(any())).thenReturn(new Event());
        when(eventClientRepository.findByClientIdAndEventId(any(), any())).thenReturn(new EventClient());
        SubscriberUrl subscriberUrl = null;
        when(subscriberUrlService.findByEventClientId(any())).thenReturn(subscriberUrl);
        Assertions.assertThrows(URLDisabledException.class, () -> {
            consumerService.publishToSubscriberUrl(new PublishToSubscriberUrlDTO("test", "007", "Test", 1L, false));
        });
    }

    @Test
    void publishToSubscriberUrlTestSuccess() {
        when(clientService.findByUsername(any())).thenReturn(new Client());
        when(eventService.findEventByEventName(any())).thenReturn(new Event());
        when(eventClientRepository.findByClientIdAndEventId(any(), any())).thenReturn(new EventClient());
        SubscriberUrl subscriberUrl = new SubscriberUrl();
        subscriberUrl.setIsEnabled(true);
        subscriberUrl.setEventClient(new EventClient());
        when(outgoingWebhookLogService.saveOutgoingWebhookLog(any())).thenReturn(1L);
        doNothing().when(outgoingWebhookLogService).updateOutgoingWebhookLog(any(), any());
        when(subscriberUrlService.findByEventClientId(any())).thenReturn(subscriberUrl);
        when(clientAuthenticationService.getSpecificAuthenticationDetails(any(), any())).thenReturn(new AuthenticationDetailsDto());
        when(clientRequestsService.getClientRequestsById(any())).thenReturn(new ClientRequests());
        RestServiceResponse success = new RestServiceResponse(200, "Success");
        when(restService.httpRequest(any(), any(), any())).thenReturn(success);
        consumerService.publishToSubscriberUrl(new PublishToSubscriberUrlDTO("test", "007", "Test", 1L, true));
        verify(outgoingWebhookLogService, times(1)).updateOutgoingWebhookLog(any(), any());
    }


    @Test
    void publishToSubscriberUrlTestFailureRestTemplateException() {
        Client client = getClient();
        client.setId(Long.parseLong(DUMMY_KEY));
        when(clientService.findByUsername(any())).thenReturn(client);
        when(eventService.findEventByEventName(any())).thenReturn(new Event());
        when(eventClientRepository.findByClientIdAndEventId(any(), any())).thenReturn(new EventClient());
        when(clientRequestsService.getClientRequestsById(any())).thenReturn(new ClientRequests());
        SubscriberUrl subscriberUrl = new SubscriberUrl();
        subscriberUrl.setIsEnabled(true);
        subscriberUrl.setEventClient(new EventClient());
        when(subscriberUrlService.findByEventClientId(any())).thenReturn(subscriberUrl);
        when(clientAuthenticationService.getSpecificAuthenticationDetails(any(), any())).thenReturn(new AuthenticationDetailsDto());
        //Failure
        RestServiceResponse failure = new RestServiceResponse(500, "Failure");
        when(restService.httpRequest(any(), any(), any())).thenReturn(failure);
        //mocking Cache
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
        when(redisTemplate.hasKey(eq(REDIS_PREFIX+DUMMY_KEY))).thenReturn(true);
        Long lowSevValue = EmailSeverityEnum.LOW_SEVERITY.getSeverityValue();
        Long initial = lowSevValue - 1;
        when(valueOperations.get(eq(REDIS_PREFIX+DUMMY_KEY))).thenReturn(String.valueOf(initial));
        when(valueOperations.increment(eq(REDIS_PREFIX+DUMMY_KEY), eq(incrementBy))).thenReturn(initial + incrementBy);
        ConsumerService consumerServiceMock = mock(ConsumerService.class);
        //send failure email and return exception
        SendGridEmailService sendGridEmailService = mock(SendGridEmailService.class);
        FailureEmailContent failureEmailContent = new FailureEmailContent();
        failureEmailContent.setEventName("test");
        failureEmailContent.setHttpPostResponse("success");
        failureEmailContent.setQueueMessage("Message");
        failureEmailContent.setSeverity("LOW");
        consumerService.sendFailureEmailToClient("rajshukla865@gmail.com", failureEmailContent);
        Assertions.assertThrows(RestTemplateException.class, () -> {
            consumerService.publishToSubscriberUrl(new PublishToSubscriberUrlDTO(client.getUsername(), "007", "Test", 1L, false));
        });
    }

    @Test
    void testCacheNoIncrementWhenValueIsGreaterThanHighSeverity() {
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
        when(redisTemplate.hasKey(eq(REDIS_PREFIX+DUMMY_KEY))).thenReturn(true);
        client.setId(Long.parseLong(DUMMY_KEY));
        Long highSevValuePlusOne = EmailSeverityEnum.HIGH_SEVERITY.getSeverityValue() + 1; //greater than high severity
        when(valueOperations.get(eq(REDIS_PREFIX+DUMMY_KEY))).thenReturn(String.valueOf(highSevValuePlusOne));
        String returnedNull = consumerService.getSeverityFromCache(client);  // Should return null , as greater than HIGH SEVERITY
        assertNull(returnedNull);
    }

    @Test
    void testLowSeverityReturnWhenLowSeverityCount() {
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
        when(redisTemplate.hasKey(eq(REDIS_PREFIX+DUMMY_KEY))).thenReturn(true);
        client.setId(Long.parseLong(DUMMY_KEY));
        Long lowSevValue = EmailSeverityEnum.LOW_SEVERITY.getSeverityValue();
        Long initial = lowSevValue - 1;
        when(valueOperations.get(eq(REDIS_PREFIX+DUMMY_KEY))).thenReturn(String.valueOf(initial));
        when(valueOperations.increment(eq(REDIS_PREFIX+DUMMY_KEY), eq(incrementBy))).thenReturn(initial + incrementBy);
        String returnedLow = consumerService.getSeverityFromCache(client);
        assertTrue(returnedLow.equals(EmailSeverityEnum.LOW_SEVERITY.getSeverityName()));
    }

    @Test
    void testMediumSeverityReturnWhenMediumSeverityCount() {
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
        when(redisTemplate.hasKey(eq(REDIS_PREFIX+DUMMY_KEY))).thenReturn(true);
        client.setId(Long.parseLong(DUMMY_KEY));
        Long midSevValue = EmailSeverityEnum.MEDIUM_SEVERITY.getSeverityValue();
        Long initial = midSevValue - 1;
        when(valueOperations.get(eq(REDIS_PREFIX+DUMMY_KEY))).thenReturn(String.valueOf(initial));
        when(valueOperations.increment(eq(REDIS_PREFIX+DUMMY_KEY), eq(incrementBy))).thenReturn(initial + incrementBy);
        String returnedMid = consumerService.getSeverityFromCache(client);
        assertTrue(returnedMid.equals(EmailSeverityEnum.MEDIUM_SEVERITY.getSeverityName()));
    }

    @Test
    void testHighSeverityReturnWhenHighSeverityCount() {
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
        when(redisTemplate.hasKey(eq(REDIS_PREFIX+DUMMY_KEY))).thenReturn(true);
        client.setId(Long.parseLong(DUMMY_KEY));
        Long highSevValue = EmailSeverityEnum.HIGH_SEVERITY.getSeverityValue();
        Long initial = highSevValue - 1;
        when(valueOperations.get(eq(REDIS_PREFIX+DUMMY_KEY))).thenReturn(String.valueOf(initial));
        when(valueOperations.increment(eq(REDIS_PREFIX+DUMMY_KEY), eq(incrementBy))).thenReturn(initial + incrementBy);
        String returnedHigh = consumerService.getSeverityFromCache(client);
        assertTrue(returnedHigh.equals(EmailSeverityEnum.HIGH_SEVERITY.getSeverityName()));
    }

    @Test
    void testCheckSeverityAndSendToDlq() {
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
        when(redisTemplate.hasKey(eq(REDIS_PREFIX+DUMMY_KEY))).thenReturn(true);
        client.setId(Long.parseLong(DUMMY_KEY));
        when(valueOperations.get(eq(REDIS_PREFIX+DUMMY_KEY))).thenReturn(String.valueOf(1000));
        Message message = new Message(new byte[]{}, new MessageProperties());
        message.getMessageProperties().setHeader(QueueConstants.HIGH_SEV_HEADER, "true");
        boolean expected = consumerService.checkSeverityAndSendToDlq(client, message);
        Assert.assertFalse(expected);
    }

}
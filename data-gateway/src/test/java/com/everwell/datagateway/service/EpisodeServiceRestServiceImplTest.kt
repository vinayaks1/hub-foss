package com.everwell.datagateway.service

import com.everwell.datagateway.BaseTest
import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.models.response.*
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.util.LinkedMultiValueMap
import org.springframework.web.client.RestTemplate
import java.time.LocalDateTime
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class EpisodeServiceRestServiceImplTest  : BaseTest() {

    @InjectMocks
    lateinit var episodeServiceRestServiceImp: EpisodeServiceRestServiceImp

    @Test
    fun testAuthentication() {
        val appProperties = AppProperties()
        appProperties.episodeServiceUrl = "http://localhost:9090"

        val restTemplate : RestTemplate = mock()
        val restService : RestService = mock()

        episodeServiceRestServiceImp.restService = restService
        episodeServiceRestServiceImp.restTemplateForAuthentication = restTemplate
        episodeServiceRestServiceImp.appProperties = appProperties

        val genericAuthResponseMock = ApiResponse<MicroServiceGenericAuthResponse>(
                "true", MicroServiceGenericAuthResponse(1L, "userService", "auth_token", 1234L), "");
        val responseEntity = ResponseEntity(genericAuthResponseMock, HttpStatus.OK)
        val headers = mutableMapOf<String, String>()
        headers["X-Client-Id"] = "29"
        val params: LinkedMultiValueMap<String, String> = LinkedMultiValueMap()
        Mockito.`when`(restTemplate.exchange(appProperties.episodeServiceUrl + "/v1/client", HttpMethod.GET, episodeServiceRestServiceImp.getHttpEntity(headers, params), object : ParameterizedTypeReference<ApiResponse<MicroServiceGenericAuthResponse>>() {}))
                .thenAnswer { responseEntity }
        val genericAuthResponse = episodeServiceRestServiceImp.getAuthToken("29")
        Assert.assertEquals(genericAuthResponseMock.data?.authToken, genericAuthResponse);
    }

    @Test
    fun getEpisodesFromPersonTest() {
        val appProperties = AppProperties()
        appProperties.episodeServiceUrl = "http://localhost:9090"
        appProperties.genericClientId = "29"

        val restTemplate : RestTemplate = mock()
        val restService : RestService = mock()

        episodeServiceRestServiceImp.restService = restService
        episodeServiceRestServiceImp.restTemplateForAuthentication = restTemplate
        episodeServiceRestServiceImp.appProperties = appProperties

        val response = ApiResponse<List<GetEpisodeByPersonResponse>>(
                "true", listOf(GetEpisodeByPersonResponse(1,"date")), "");
        val responseEntity = ResponseEntity(response, HttpStatus.OK)
        val headers = mutableMapOf<String, String>()
        headers["Authorization"] = "Bearer 29"
        headers["X-Client-Id"] = "29"
        val params: LinkedMultiValueMap<String, String> = LinkedMultiValueMap()
        Mockito.`when`(restTemplate.exchange(appProperties.episodeServiceUrl + "/v1/episodes/personId/29?includeDetails=false", HttpMethod.GET, episodeServiceRestServiceImp.getHttpEntity(headers,params), object : ParameterizedTypeReference<ApiResponse<List<GetEpisodeByPersonResponse>>>() {}))
                .thenAnswer { responseEntity }
        val episodeByPersonResponse = episodeServiceRestServiceImp.getEpisodesFromPerson("29", 29)
        Assert.assertEquals(response.data?.get(0)?.id, episodeByPersonResponse.data?.get(0)?.id);
    }

    @Test
    fun updateEpisodeDetailsTest() {
        val appProperties = AppProperties()
        appProperties.episodeServiceUrl = "http://localhost:9090"
        appProperties.genericClientId = "29"

        val restTemplate : RestTemplate = mock()
        val restService : RestService = mock()

        episodeServiceRestServiceImp.restService = restService
        episodeServiceRestServiceImp.restTemplateForAuthentication = restTemplate
        episodeServiceRestServiceImp.appProperties = appProperties

        val response = ApiResponse(
                "true", GetEpisodeByPersonResponse(1,"date"), "");
        val responseEntity = ResponseEntity(response, HttpStatus.OK)
        val headers = mutableMapOf<String, String>()
        headers["Authorization"] = "Bearer 29"
        headers["X-Client-Id"] = "29"
        val params: MutableMap<String,Any> = mutableMapOf()
        params["episodeId"] = "1234"
        Mockito.`when`(restTemplate.exchange(appProperties.episodeServiceUrl + "/v1/episode", HttpMethod.PUT, episodeServiceRestServiceImp.getHttpEntity(headers,params), object : ParameterizedTypeReference<ApiResponse<GetEpisodeByPersonResponse>>() {}))
                .thenAnswer { responseEntity }
        val episodeByPersonResponse = episodeServiceRestServiceImp.updateEpisodeDetails("29", params)
        Assert.assertEquals(response.data?.id, episodeByPersonResponse.data?.id);
    }

    @Test
    fun getDefaultDiseaseForClientIdTest() {
        val appProperties = AppProperties()
        appProperties.episodeServiceUrl = "http://localhost:9090"
        appProperties.genericClientId = "1"
        val restTemplate : RestTemplate = mock()
        val restService : RestService = mock()
        episodeServiceRestServiceImp.restService = restService
        episodeServiceRestServiceImp.restTemplateForAuthentication = restTemplate
        episodeServiceRestServiceImp.appProperties = appProperties
        val genericAuthResponseMock = ApiResponse<MicroServiceGenericAuthResponse>(
                "true", MicroServiceGenericAuthResponse(1L, "userService", "auth_token", 1234L), "");
        val getTokenResponseEntity = ResponseEntity(genericAuthResponseMock, HttpStatus.OK)
        val getTokenHeaders = mutableMapOf<String, String>()
        getTokenHeaders["X-Client-Id"] = "1"
        val getTokenParams: LinkedMultiValueMap<String, String> = LinkedMultiValueMap()
        val diseaseTemplate = DiseaseTemplate(1L, "test", 1L, false, "test", LocalDateTime.now(), LocalDateTime.now())
        val response = ApiResponse<DiseaseTemplate>(
                "true", diseaseTemplate, "");
        val responseEntity = ResponseEntity(response, HttpStatus.OK)
        val headers = mutableMapOf<String, String>()
        headers["Authorization"] = "Bearer auth_token"
        headers["X-Client-Id"] = "1"
        val params: LinkedMultiValueMap<String, String> = LinkedMultiValueMap()
        Mockito.`when`(restTemplate.exchange(appProperties.episodeServiceUrl + "/v1/client", HttpMethod.GET, episodeServiceRestServiceImp.getHttpEntity(getTokenHeaders, getTokenParams), object : ParameterizedTypeReference<ApiResponse<MicroServiceGenericAuthResponse>>() {}))
                .thenAnswer { getTokenResponseEntity }
        val responseType: ParameterizedTypeReference<ApiResponse<DiseaseTemplate>> =
                object : ParameterizedTypeReference<ApiResponse<DiseaseTemplate>>() {}
        Mockito.`when`(restTemplate.exchange(appProperties.episodeServiceUrl + "/v1/disease/default", HttpMethod.GET, episodeServiceRestServiceImp.getHttpEntity(headers, params), responseType))
                .thenAnswer { responseEntity }
        val defaultDisease = episodeServiceRestServiceImp.getDefaultDiseaseForClientId(1L)
        Assert.assertEquals(defaultDisease.data?.diseaseName, diseaseTemplate.diseaseName);
    }

    @Test
    fun deleteEpisodeTest() {
        val appProperties = AppProperties()
        appProperties.episodeServiceUrl = "http://localhost:9090"
        appProperties.genericClientId = "1"
        val restTemplate : RestTemplate = mock()
        val restService : RestService = mock()
        episodeServiceRestServiceImp.restService = restService
        episodeServiceRestServiceImp.restTemplateForAuthentication = restTemplate
        episodeServiceRestServiceImp.appProperties = appProperties
        val genericAuthResponseMock = ApiResponse<MicroServiceGenericAuthResponse>(
                "true", MicroServiceGenericAuthResponse(1L, "userService", "auth_token", 1234L), "");
        val getTokenResponseEntity = ResponseEntity(genericAuthResponseMock, HttpStatus.OK)
        val getTokenHeaders = mutableMapOf<String, String>()
        getTokenHeaders["X-Client-Id"] = "1"
        val getTokenParams: LinkedMultiValueMap<String, String> = LinkedMultiValueMap()
        val response = ApiResponse<String>(
                "true", "success", "");
        val responseEntity = ResponseEntity(response, HttpStatus.OK)
        val headers = mutableMapOf<String, String>()
        headers["Authorization"] = "Bearer auth_token"
        headers["X-Client-Id"] = "1"
        val body = mutableMapOf<String, Any>()
        body["episodeId"] = 1L;
        Mockito.`when`(restTemplate.exchange(appProperties.episodeServiceUrl + "/v1/client", HttpMethod.GET, episodeServiceRestServiceImp.getHttpEntity(getTokenHeaders, getTokenParams), object : ParameterizedTypeReference<ApiResponse<MicroServiceGenericAuthResponse>>() {}))
                .thenAnswer { getTokenResponseEntity }
        val responseType: ParameterizedTypeReference<ApiResponse<String>> =
                object : ParameterizedTypeReference<ApiResponse<String>>() {}
        Mockito.`when`(restTemplate.exchange(appProperties.episodeServiceUrl + "/v1/episode/delete", HttpMethod.POST, episodeServiceRestServiceImp.getHttpEntity(headers, body), responseType))
                .thenAnswer { responseEntity }
        val deleteResponse = episodeServiceRestServiceImp.deleteEpisode(1L,1L)
        Assert.assertEquals(deleteResponse.data.toString(), "success")
    }

}
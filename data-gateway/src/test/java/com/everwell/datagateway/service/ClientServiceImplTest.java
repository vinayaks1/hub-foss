package com.everwell.datagateway.service;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.entities.Client;
import com.everwell.datagateway.entities.Event;
import com.everwell.datagateway.entities.EventClient;
import com.everwell.datagateway.models.request.SubscribeEventRequest;
import com.everwell.datagateway.repositories.ClientRepository;
import com.everwell.datagateway.repositories.EventClientRepository;
import com.everwell.datagateway.repositories.SubscriberUrlRepository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class ClientServiceImplTest extends BaseTest {

    @InjectMocks
    ClientServiceImpl clientService;

    @Mock
    private ClientRepository clientRepository;

    @Mock
    private EventService eventService;

    @Mock
    private AuthenticationService authenticationService;

    //Don't delete. Used in mocking
    @Mock
    private EventClientRepository eventClientRepository;

    //Don't delete. Used in mocking
    @Mock
    private SubscriberUrlRepository subscriberUrlRepository;

    @Mock
    ClientAuthenticationService clientAuthenticationService;

    private String testUsername = "test";
    private String testIp = "127.0.0.1";

    private String testEventname = "test";

    private final Long clientId = 29L;

    private final Long eventId = 1L;

    @Test
    void findByUserNameNullInput() {
        Client client = clientService.findByUsername(null);
        assertNull(client);
    }

    @Test
    void findByIpNullInput() {
        Client client = clientService.findByIp(null);
        assertNull(client);
    }

    @Test
    void findByUsernameValidInput() {
        Client client = new Client();
        client.setUsername(testUsername);
        when(clientRepository.findByUsername(testUsername)).thenReturn(client);
        Client expectedClient = clientService.findByUsername(client.getUsername());
        assertEquals(testUsername, expectedClient.getUsername());
    }

    @Test
    void findByIpValidInput() {
        Client client = new Client();
        client.setIp(testIp);
        when(clientRepository.findByIp(testIp)).thenReturn(client);
        Client expectedClient = clientService.findByIp(client.getIp());
        assertEquals(testIp, expectedClient.getIp());
    }

    @Test
    void isClientExists() {
        Client client = new Client();
        client.setUsername(testUsername);
        when(clientRepository.findByUsername(testUsername)).thenReturn(client);
        Boolean answer = clientService.isClientExists(client.getUsername());
        assertEquals(true, answer);
    }

    @Test
    void getClientByIdNullInput() {
        Client client = clientService.getClientById(null);
        assertNull(client);
    }

    @Test
    void getClientByIdValidInput() {
        Client client = new Client();
        client.setId(1L);
        client.setUsername("test");
        client.setFailureEmail("test@gmail.com");
        Optional<Client> optionalClient = Optional.of(client);
        when(clientRepository.findById(1L)).thenReturn(optionalClient);
        Client expectedClient = clientService.getClientById(client.getId());
        assertEquals(1L, expectedClient.getId());
        assertEquals(client.getUsername(), expectedClient.getUsername());
        assertEquals(client.getFailureEmail(), expectedClient.getFailureEmail());
    }

    @Test
    void subscribeEventNullInput() {
        SubscribeEventRequest subscribeEventRequest = new SubscribeEventRequest();
        Boolean answer = clientService.subscribeEvent(subscribeEventRequest);
        assertEquals(false, answer);
    }

    @Test
    void subscribeEventClientNull() {
        //If Client Null , then also false
        when(eventService.isEventExists(testEventname)).thenReturn(true);
        when(authenticationService.getCurrentUsername(any())).thenReturn("client");
        when(clientRepository.findByUsername(any())).thenReturn(null); // client null mocked
        SubscribeEventRequest subscribeEventRequest = new SubscribeEventRequest();
        subscribeEventRequest.setEventName(testEventname); //Event name "test"
        Boolean expectedAnswer = clientService.subscribeEvent(subscribeEventRequest);
        assertEquals(false, expectedAnswer);

    }


    @Test
    void subscribeEventValidInput() {
        //When "test" then return true
        when(eventService.isEventExists(testEventname)).thenReturn(true);
        when(authenticationService.getCurrentUsername(any())).thenReturn("client");
        when(clientRepository.findByUsername(any())).thenReturn(new Client());
        when(eventService.findEventByEventName(any())).thenReturn(new Event());
        doNothing().when(clientAuthenticationService).validateCredentials(any());
        doNothing().when(clientAuthenticationService).saveAuthenticationDetails(any());
        SubscribeEventRequest subscribeEventRequest = new SubscribeEventRequest();
        subscribeEventRequest.setEventName(testEventname); //Event name "test"
        Boolean expectedAnswer = clientService.subscribeEvent(subscribeEventRequest);
        assertEquals(true, expectedAnswer);
        verify(clientAuthenticationService, Mockito.times(1)).saveAuthenticationDetails(any());
        verify(clientAuthenticationService, Mockito.times(1)).validateCredentials(any());
    }

    @Test
    void saveClient() {
        when(clientRepository.save(Mockito.any(Client.class)))
                .thenAnswer(i -> i.getArguments()[0]);
        Client client = new Client();
        client.setUsername("test");
        clientService.saveClient(client);
        verify(clientRepository, times(1)).save(any());
    }

    @Test
    void saveClientNull() {
        Client client = new Client();
        client.setUsername(null);
        clientService.saveClient(client);
        verify(clientRepository, times(0)).save(any()); // 0 times, as there's return statement if username is null
    }

    @Test
    void isClientSubscribedToEventTrue() {
        when(eventClientRepository.findByClientIdAndEventIdAndActiveTrue(clientId, eventId)).thenReturn(new EventClient());
        assertTrue(clientService.isClientSubscribedToEvent(clientId, eventId));
    }

    @Test
    void isClientSubscribedToEventFalse() {
        when(eventClientRepository.findByClientIdAndEventIdAndActiveTrue(clientId, eventId)).thenReturn(null);
        assertFalse(clientService.isClientSubscribedToEvent(clientId, eventId));
    }

    @Test
    void subscribeEventAlreadySubscribed() {
        //When "test" then return true
        when(eventService.isEventExists(testEventname)).thenReturn(true);
        when(authenticationService.getCurrentUsername(any())).thenReturn(testUsername);
        when(clientRepository.findByUsername(any())).thenReturn(new Client());
        when(eventService.findEventByEventName(any())).thenReturn(new Event());
        SubscribeEventRequest subscribeEventRequest = new SubscribeEventRequest();
        subscribeEventRequest.setEventName(testEventname);
        when(eventClientRepository.findByClientIdAndEventId(any(), any())).thenReturn(new EventClient());
        Boolean result = clientService.subscribeEvent(subscribeEventRequest);
        assertFalse(result);
    }
}
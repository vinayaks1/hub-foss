package com.everwell.datagateway.service;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.apiHelpers.ABDMHelper;
import com.everwell.datagateway.entities.Client;
import com.everwell.datagateway.entities.Event;
import com.everwell.datagateway.entities.EventClient;
import com.everwell.datagateway.entities.SubscriberUrl;
import com.everwell.datagateway.enums.StatusCodeEnum;
import com.everwell.datagateway.exceptions.NotFoundException;
import com.everwell.datagateway.exceptions.RestTemplateException;
import com.everwell.datagateway.exceptions.URLDisabledException;
import com.everwell.datagateway.models.response.RestServiceResponse;
import com.everwell.datagateway.repositories.EventClientRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


class ABDMConsumerServiceTest extends BaseTest {


    @InjectMocks
    ABDMConsumerService abdmConsumerService;

    @Mock
    private SubscriberUrlService subscriberUrlService;

    @Mock
    private EventClientRepository eventClientRepository;

    @Mock
    private EventService eventService;

    @Mock
    private ClientService clientService;

    @Mock
    private ABDMHelper abdmHelper;

    private Client client = new Client();

    public Event getEvent() {
        Event event = new Event();
        event.setEventName("test");
        return  event;
    }

    @Test
    void publishToABDMSubscriberUrlEventNotFoundException1() {
        NotFoundException notFoundException = assertThrows(NotFoundException.class, () -> abdmConsumerService.publishToABDMSubscriberUrl("test", null, "Test Message"));
        StatusCodeEnum notFoundSentry = StatusCodeEnum.EVENT_NOT_FOUND_SENTRY;
        assertEquals(notFoundSentry.getCode().value(), notFoundException.getStatusCode());
    }

    @Test
    void publishToABDMSubscriberUrlEventNotFoundException2() {
        //Invalid or Null Parameter for clientName and EventName => Will throw an custom Database exception, as coded
        assertThrows(NotFoundException.class, () -> abdmConsumerService.publishToABDMSubscriberUrl("test", null, "Test Message"));
    }

    @Test
    void publishToABDMSubscriberUrlClientNotFoundException3() {
        when(clientService.findByUsername(any())).thenReturn(null);
        when(eventService.findEventByEventName(any())).thenReturn(getEvent());
        NotFoundException notFoundException = assertThrows(NotFoundException.class, () -> abdmConsumerService.publishToABDMSubscriberUrl(null, "test", "Test Message"));
        StatusCodeEnum notFoundSentry = StatusCodeEnum.CLIENT_NOT_FOUND_SENTRY;
        assertEquals(notFoundSentry.getCode().value(), notFoundException.getStatusCode());
    }

    @Test
    void publishToABDMSubscriberUrlClientNotFoundException() {
        when(clientService.findByUsername(any())).thenReturn(null);
        when(eventService.findEventByEventName(any())).thenReturn(getEvent());
        assertThrows(NotFoundException.class, () -> abdmConsumerService.publishToABDMSubscriberUrl(null, "test", "Test Message"));
    }

    @Test
    void publishToABDMSubscriberUrlTestSubscriberUrlNull() {
        when(clientService.findByUsername(any())).thenReturn(new Client());
        when(eventService.findEventByEventName(any())).thenReturn(new Event());
        when(eventClientRepository.findByClientIdAndEventId(any(), any())).thenReturn(new EventClient());
        SubscriberUrl subscriberUrl = null;
        when(subscriberUrlService.findByEventClientId(any())).thenReturn(subscriberUrl);
        Assertions.assertThrows(URLDisabledException.class, () -> {
            abdmConsumerService.publishToABDMSubscriberUrl("test", "007", "Test");
        });
    }

    @Test
    void publishToABDMSubscriberUrlTestSuccess() {
        when(clientService.findByUsername(any())).thenReturn(new Client());
        when(eventService.findEventByEventName(any())).thenReturn(new Event());
        when(eventClientRepository.findByClientIdAndEventId(any(), any())).thenReturn(new EventClient());
        SubscriberUrl subscriberUrl = new SubscriberUrl();
        subscriberUrl.setIsEnabled(true);
        subscriberUrl.setEventClient(new EventClient());
        when(subscriberUrlService.findByEventClientId(any())).thenReturn(subscriberUrl);
        RestServiceResponse success = new RestServiceResponse(202, "Success");
        when(abdmHelper.publishToABDM(any(), any())).thenReturn(success);
        abdmConsumerService.publishToABDMSubscriberUrl("test", "007", "Test");
    }

    @Test
    void publishToABDMSubscriberUrlTestFailure() {
        when(clientService.findByUsername(any())).thenReturn(new Client());
        when(eventService.findEventByEventName(any())).thenReturn(new Event());
        when(eventClientRepository.findByClientIdAndEventId(any(), any())).thenReturn(new EventClient());
        SubscriberUrl subscriberUrl = new SubscriberUrl();
        subscriberUrl.setIsEnabled(true);
        subscriberUrl.setEventClient(new EventClient());
        when(subscriberUrlService.findByEventClientId(any())).thenReturn(subscriberUrl);
        RestServiceResponse failure = new RestServiceResponse(500, "Failure");
        when(abdmHelper.publishToABDM(any(), any())).thenReturn(failure);
        assertThrows(RestTemplateException.class, () -> abdmConsumerService.publishToABDMSubscriberUrl("test", "test", "Test Message"));
    }

}
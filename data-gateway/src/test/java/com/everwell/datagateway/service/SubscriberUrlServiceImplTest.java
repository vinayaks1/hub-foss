package com.everwell.datagateway.service;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.entities.EventClient;
import com.everwell.datagateway.entities.SubscriberUrl;
import com.everwell.datagateway.repositories.SubscriberUrlRepository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


class SubscriberUrlServiceImplTest extends BaseTest {

    @InjectMocks
    SubscriberUrlServiceImpl subscriberUrlService;

    @Mock
    private SubscriberUrlRepository subscriberUrlRepository;

    @Test
    void findByEventClientIdNullInput() {
        EventClient eventClient = new EventClient();
        SubscriberUrl subscriberUrl = subscriberUrlService.findByEventClientId(eventClient);
        assertNull(subscriberUrl);
    }


    @Test
    void findByEventClientId() {
        SubscriberUrl subscriberUrl = new SubscriberUrl();
        subscriberUrl.setId(1L);
        subscriberUrl.setUrl("www.test.com");
        when(subscriberUrlRepository.findByEventClient(any())).thenReturn(subscriberUrl);
        EventClient input = new EventClient();
        input.setEventId(1L);
        input.setClientId(1L);
        SubscriberUrl subscriberUrlReturned = subscriberUrlService.findByEventClientId(input);
        assertEquals("www.test.com", subscriberUrlReturned.getUrl());
        assertEquals(subscriberUrl.getId(),subscriberUrlReturned.getId());
    }


}
package com.everwell.datagateway.service

import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.entities.Client
import com.everwell.datagateway.models.response.ApiResponse
import com.everwell.datagateway.models.response.GenericAuthResponse
import com.everwell.datagateway.models.response.LinkInitiationOtpResponse
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyMap
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.exchange
import kotlin.reflect.jvm.internal.impl.load.kotlin.JvmType

@RunWith(MockitoJUnitRunner.Silent::class)
class NikshayRestServiceTest {


    private var nikshayRestServiceImp: NikshayRestServiceImp = NikshayRestServiceImp()


    @Test
    fun testAuthentication(){
        val clientService : ClientService = mock()
        val restTemplate : RestTemplate = mock()
        val restService : RestService = mock()
        val client : Client = mock()
        val httpEntity : HttpEntity<JvmType.Object> = mock()
        val authenticationService : AuthenticationService = mock()

        val appProperties = AppProperties()
        appProperties.nikshayServerUrl = "http://localhost.com/"
        appProperties.nikshayUserGrantType=""
        appProperties.nikshayUserName= "umang"
        appProperties.nikshayUserPassword = ""

        nikshayRestServiceImp.appProperties = appProperties
        nikshayRestServiceImp.restService = restService
        nikshayRestServiceImp.restTemplateForAuthentication = restTemplate
        nikshayRestServiceImp.clientService = clientService
        nikshayRestServiceImp.authenticationService = authenticationService

        val genericAuthResponseMock = GenericAuthResponse("abcded1234",25999)
        val responseEntity = ResponseEntity(genericAuthResponseMock, HttpStatus.OK)
        Mockito.`when`(restService.buildHeaderAndRequest(anyMap())).thenAnswer { httpEntity }
        Mockito.`when`(clientService.findByUsername("umang")).thenAnswer { client }
        Mockito.`when`(authenticationService.getCurrentUsername(any())).thenAnswer { anyString() }
        Mockito.`when`(restTemplate.postForObject("http://localhost.com/Token", httpEntity,GenericAuthResponse::class.java)).thenAnswer{genericAuthResponseMock}
        val params: LinkedMultiValueMap<String, String> = LinkedMultiValueMap()
        params["grant_type"] = appProperties.nikshayUserGrantType
        params["username"] = appProperties.nikshayUserName
        params["password"] = appProperties.nikshayUserPassword
        Mockito.`when`(restTemplate.exchange(appProperties.nikshayServerUrl + "Token", HttpMethod.POST, nikshayRestServiceImp.getHttpEntity(HashMap(), params), object : ParameterizedTypeReference<GenericAuthResponse>() {}))
                .thenAnswer { responseEntity }
        val genericAuthResponse = nikshayRestServiceImp.getAuthToken(null)
        Assert.assertEquals(genericAuthResponseMock.access_token, genericAuthResponse);
    }

    @Test
    fun testSendOtpLinkInitiation() {
        val appProperties = AppProperties()
        appProperties.nikshayServerUrl = "http://localhost.com/"
        appProperties.nikshayUserGrantType=""
        appProperties.nikshayUserName= "umang"
        appProperties.nikshayUserPassword = ""

        val restTemplate : RestTemplate = mock()
        val restService : RestService = mock()
        val clientService : ClientService = mock()
        val authenticationService : AuthenticationService = mock()

        nikshayRestServiceImp.appProperties = appProperties
        nikshayRestServiceImp.restService = restService
        nikshayRestServiceImp.restTemplateForAuthentication = restTemplate
        nikshayRestServiceImp.clientService = clientService
        nikshayRestServiceImp.authenticationService = authenticationService

        val response = ApiResponse<LinkInitiationOtpResponse>(
                "true", LinkInitiationOtpResponse("1234","test"), "");
        val responseEntity = ResponseEntity(response, HttpStatus.OK)
        val headers = mutableMapOf<String, String>()
        headers["Authorization"] = "Bearer 29"
        val data = mutableMapOf<String,String>()
        Mockito.`when`(restTemplate.exchange(appProperties.nikshayServerUrl + "api/Patients/SendOtpForLinkInitiation/54283", HttpMethod.POST, nikshayRestServiceImp.getHttpEntity(headers,data), object : ParameterizedTypeReference<ApiResponse<LinkInitiationOtpResponse>>() {}))
                .thenAnswer { responseEntity }
        val otpResponse = nikshayRestServiceImp.sendOtpLinkInitiation("29", "54283")
        Assert.assertEquals(response.data?.otp, otpResponse.data?.otp);
    }

    @Test
    fun testAuthentication_LIMS(){
        val clientService : ClientService = mock()
        val restTemplate : RestTemplate = mock()
        val restService : RestService = mock()
        val client : Client = mock()
        val httpEntity : HttpEntity<JvmType.Object> = mock()
        val authenticationService : AuthenticationService = mock()

        val appProperties = AppProperties()
        appProperties.nikshayServerUrl = "http://localhost.com/"
        appProperties.limsNikshayUserGrantType=""
        appProperties.limsNikshayUserName= "umang"
        appProperties.limsNikshayUserPassword = ""

        nikshayRestServiceImp.appProperties = appProperties
        nikshayRestServiceImp.restService = restService
        nikshayRestServiceImp.restTemplateForAuthentication = restTemplate
        nikshayRestServiceImp.clientService = clientService
        nikshayRestServiceImp.authenticationService = authenticationService

        val genericAuthResponseMock = GenericAuthResponse("abcded1234",25999)
        val responseEntity = ResponseEntity(genericAuthResponseMock, HttpStatus.OK)
        Mockito.`when`(restService.buildHeaderAndRequest(anyMap())).thenAnswer { httpEntity }
        Mockito.`when`(clientService.findByUsername("umang")).thenAnswer { client }
        Mockito.`when`(authenticationService.getCurrentUsername(any())).thenAnswer { anyString() }
        Mockito.`when`(restTemplate.postForObject("http://localhost.com/Token", httpEntity,GenericAuthResponse::class.java)).thenAnswer{genericAuthResponseMock}
        val params: LinkedMultiValueMap<String, String> = LinkedMultiValueMap()
        params["grant_type"] = appProperties.limsNikshayUserGrantType
        params["username"] = appProperties.limsNikshayUserName
        params["password"] = appProperties.limsNikshayUserPassword
        Mockito.`when`(restTemplate.exchange(appProperties.nikshayServerUrl + "Token", HttpMethod.POST, nikshayRestServiceImp.getHttpEntity(HashMap(), params), object : ParameterizedTypeReference<GenericAuthResponse>() {}))
                .thenAnswer { responseEntity }
        val genericAuthResponse = nikshayRestServiceImp.getAuthTokenForLims("lims")
        Assert.assertEquals(genericAuthResponseMock.access_token, genericAuthResponse);
    }

}
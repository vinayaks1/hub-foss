package com.everwell.datagateway.service;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.apiHelpers.ABDMHelper;
import com.everwell.datagateway.models.request.abdm.*;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class ABDMHelperTest extends BaseTest {

    @InjectMocks
    ABDMHelper abdmHelper;

    @Mock
    RestTemplate client;

    @Test
    void exchangeTest() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("X-CM-ID","1");
        HttpEntity<String> request = new HttpEntity<>("data", headers);
        Map<String,String> response = new HashMap<>();
        response.put("accessToken","abcd");
        when(client.postForEntity(anyString(),any(),any())).thenReturn(new ResponseEntity<>(response, HttpStatus.ACCEPTED));
        Map<Integer,String> restServiceResponse = abdmHelper.getABDMResponse("https://www.google.com/",request);
        assertEquals(restServiceResponse.entrySet().iterator().next().getKey(), 202);
    }

    @Test
    void getTokenTest() {
        Map<String,String> response = new HashMap<>();
        response.put("accessToken","abcd");
        when(client.postForEntity(anyString(),any(),any())).thenReturn(new ResponseEntity<>(response, HttpStatus.ACCEPTED));
        abdmHelper.getToken();
    }

    @Test
    void authInitRequestTest() {
        Map<String,String> response = new HashMap<>();
        response.put("accessToken","abcd");
        when(client.postForEntity(anyString(),any(),any())).thenReturn(new ResponseEntity<>(response, HttpStatus.ACCEPTED));
        abdmHelper.authInitRequest(new ABDMInitateAuthRequest("test","test"));
    }

    @Test
    void authInitRequestTest_Unauthorised() {
        Map<String,String> response = new HashMap<>();
        response.put("accessToken","abcd");
        when(client.postForEntity(anyString(),any(),any())).thenReturn(new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED));
        abdmHelper.authInitRequest(new ABDMInitateAuthRequest("test","test"));
    }

    @Test
    void authConfirmRequestTest() {
        Map<String,String> response = new HashMap<>();
        response.put("accessToken","abcd");
        when(client.postForEntity(anyString(),any(),any())).thenReturn(new ResponseEntity<>(response, HttpStatus.ACCEPTED));
        abdmHelper.authConfirmRequest(new ABDMDemographicLinkingTokenRequest("test",new ABDMDemographicLinkingTokenRequest.Credential("test","test","test","test","test")));
    }

    @Test
    void authConfirmRequestTest_Unauthorised() {
        Map<String,String> response = new HashMap<>();
        response.put("accessToken","abcd");
        when(client.postForEntity(anyString(),any(),any())).thenReturn(new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED));
        abdmHelper.authConfirmRequest(new ABDMDemographicLinkingTokenRequest("test",new ABDMDemographicLinkingTokenRequest.Credential("test","test","test","test","test")));
    }

    @Test
    void addCareContextRequestTest() {
        Map<String,String> response = new HashMap<>();
        response.put("accessToken","abcd");
        when(client.postForEntity(anyString(),any(),any())).thenReturn(new ResponseEntity<>(response, HttpStatus.ACCEPTED));
        abdmHelper.addCareContextRequest(new ABDMLinkCareContextRequest("test","test","test","test","test"));
    }

    @Test
    void addCareContextRequestTest_Unauthorised() {
        Map<String,String> response = new HashMap<>();
        response.put("accessToken","abcd");
        when(client.postForEntity(anyString(),any(),any())).thenReturn(new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED));
        abdmHelper.addCareContextRequest(new ABDMLinkCareContextRequest("test","test","test","test","test"));
    }

    @Test
    void notifyCareContextTest() {
        Map<String,String> response = new HashMap<>();
        response.put("accessToken","abcd");
        when(client.postForEntity(anyString(),any(),any())).thenReturn(new ResponseEntity<>(response, HttpStatus.ACCEPTED));
        abdmHelper.notifyCareContext(new ABDMNotifyCareContextRequest(new ABDMNotifyCareContextEvent(1L,1L,"test","test","test"),"test"));
    }

    @Test
    void notifyCareContextTest_Unauthorised() {
        Map<String,String> response = new HashMap<>();
        response.put("accessToken","abcd");
        when(client.postForEntity(anyString(),any(),any())).thenReturn(new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED));
        abdmHelper.notifyCareContext(new ABDMNotifyCareContextRequest(new ABDMNotifyCareContextEvent(1L,1L,"test","test","test"),"test"));
    }

    @Test
    void authOnNotifyTest() {
        Map<String,String> response = new HashMap<>();
        response.put("accessToken","abcd");
        when(client.postForEntity(anyString(),any(),any())).thenReturn(new ResponseEntity<>(response, HttpStatus.ACCEPTED));
        abdmHelper.authOnNotify(new ABDMAuthOnNotifyRequest("baf556d0-6e37-4cb5-a63e-f4a59725f3fe"));
    }

    @Test
    void authOnNotifyTest_Unauthorised() {
        Map<String,String> response = new HashMap<>();
        response.put("accessToken","abcd");
        when(client.postForEntity(anyString(),any(),any())).thenReturn(new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED));
        abdmHelper.authOnNotify(new ABDMAuthOnNotifyRequest("baf556d0-6e37-4cb5-a63e-f4a59725f3fe"));
    }

    @Test
    void hiuConsentOnNotifyTest() {
        Map<String,String> response = new HashMap<>();
        response.put("accessToken","abcd");
        when(client.postForEntity(anyString(),any(),any())).thenReturn(new ResponseEntity<>(response, HttpStatus.ACCEPTED));
        abdmHelper.hiuConsentOnNotify(new ABDMConsentOnNotifyRequest("baf556d0-6e37-4cb5-a63e-f4a59725f3fe", Collections.emptyList()));
    }

    @Test
    void hiuConsentOnNotifyTest_Unauthorised() {
        Map<String,String> response = new HashMap<>();
        response.put("accessToken","abcd");
        when(client.postForEntity(anyString(),any(),any())).thenReturn(new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED));
        abdmHelper.hiuConsentOnNotify(new ABDMConsentOnNotifyRequest("baf556d0-6e37-4cb5-a63e-f4a59725f3fe",Collections.emptyList()));
    }
}

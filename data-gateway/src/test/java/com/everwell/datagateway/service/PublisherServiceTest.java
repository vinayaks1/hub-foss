package com.everwell.datagateway.service;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.constants.Constants;
import com.everwell.datagateway.entities.*;
import com.everwell.datagateway.enums.EventEnum;
import com.everwell.datagateway.enums.StatusCodeEnum;
import com.everwell.datagateway.exceptions.ForbiddenException;
import com.everwell.datagateway.exceptions.NotFoundException;
import com.everwell.datagateway.handlers.EventHandler;
import com.everwell.datagateway.handlers.EventHandlerMap;
import com.everwell.datagateway.models.response.PublisherResponse;
import com.everwell.datagateway.repositories.EventClientRepository;
import com.everwell.datagateway.repositories.PublisherQueueInfoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PublisherServiceTest extends BaseTest {

    @InjectMocks
    private PublisherServiceImpl testPublisherService;


    @Mock
    private ClientRequestsService clientRequestsService;

    @Mock
    private SubscriberUrlService subscriberUrlService;

    @Mock
    private EventService eventService;

    @Mock
    private ClientService clientService;

    @Mock
    private AuthenticationService authenticationService;

    @Mock
    private EventClientRepository eventClientRepository;

    @Mock
    private EventHandlerMap eventHandlerMap;

    @Mock
    private EventHandler eventHandler;

    @Mock
    private PublisherQueueInfoRepository publisherQueueInfoRepository;

    private String testString = "testString";

    private Long testId = 1L;

    @Test
    void publishAddTestEventNotFound() {
        String addTestRequest = testString;
        EventEnum eventEnum = EventEnum.ADD_TEST;
        when(eventService.findEventByEventName(eventEnum.getEventName())).thenReturn(null);
        when(authenticationService.getCurrentUsername(any())).thenReturn(testString);
        NotFoundException notFoundException = Assertions.assertThrows(NotFoundException.class, () -> {
            testPublisherService.publishRequest(addTestRequest, EventEnum.ADD_TEST.getEventName());
        });
        assertTrue(notFoundException.getError().equals(StatusCodeEnum.CLIENT_OR_EVENT_NOT_FOUND.getMessage()));
    }

    @Test
    void publishAddTestClientNotFound() {
        String addTestRequest = testString;
        EventEnum eventEnum = EventEnum.ADD_TEST;
        when(clientService.findByUsername(any())).thenReturn(null);

        NotFoundException notFoundException = Assertions.assertThrows(NotFoundException.class, () -> {
            testPublisherService.publishRequest(addTestRequest, eventEnum.getEventName());
        });
        assertTrue(notFoundException.getError().equals(StatusCodeEnum.CLIENT_OR_EVENT_NOT_FOUND.getMessage()));
    }

    @Test
    void publishAddTestNotSubscribedToEvent() {
        String addTestRequest = testString;
        EventEnum eventEnum = EventEnum.ADD_TEST;
        when(eventService.findEventByEventName(eventEnum.getEventName())).thenReturn(getEvent());
        when(clientService.findByUsername(any())).thenReturn(getClient());
        when(clientService.isClientSubscribedToEvent(testId, testId)).thenReturn(false);
        ForbiddenException notFoundException = Assertions.assertThrows(ForbiddenException.class, () -> {
            testPublisherService.publishRequest(addTestRequest, eventEnum.getEventName());
        });
        assertEquals(notFoundException.getMessage(), StatusCodeEnum.CLIENT_NOT_SUBSCRIBED_TO_EVENT_OR_DISABLED.getMessage());
    }

    @Test
    void publishAddTestHandlerNotFound() {
        String addTestRequest = testString;
        EventEnum eventEnum = EventEnum.ADD_TEST;
        when(eventService.findEventByEventName(eventEnum.getEventName())).thenReturn(getEvent());
        when(clientService.findByUsername(any())).thenReturn(getClient());
        when(clientService.isClientSubscribedToEvent(testId, testId)).thenReturn(true);
        when(eventHandlerMap.getHandler(anyString())).thenReturn(null);
        NotFoundException notFoundException = Assertions.assertThrows(NotFoundException.class, () -> {
            testPublisherService.publishRequest(addTestRequest, eventEnum.getEventName());
        });
        assertEquals(notFoundException.getMessage(), StatusCodeEnum.NOT_FOUND_HANDLER.message);
    }

    @Test
    void publishAddTest() {
        String addTestRequest = testString;
        EventEnum eventEnum = EventEnum.ADD_TEST;
        when(eventService.findEventByEventName(eventEnum.getEventName())).thenReturn(getEvent());
        when(publisherQueueInfoRepository.findFirstByEventName(any())).thenReturn(new PublisherQueueInfo());
        when(clientService.findByUsername(any())).thenReturn(getClient());
        when(clientService.isClientSubscribedToEvent(testId, testId)).thenReturn(true);
        when(clientRequestsService.saveClientRequest(any())).thenReturn(testId);
        when(eventHandlerMap.getHandler(anyString())).thenReturn(eventHandler);
        PublisherResponse publisherResponse = testPublisherService.publishRequest(addTestRequest, eventEnum.getEventName());
        assertNotNull(publisherResponse.getReferenceId());

    }

    @Test
    void publishRequestForABDM() {
        String addTestRequest = testString;
        EventEnum eventEnum = EventEnum.ABDM_ADD_UPDATE_CONSENT;
        when(eventService.findEventByEventName(eventEnum.getEventName())).thenReturn(getEvent());
        when(publisherQueueInfoRepository.findFirstByEventName(any())).thenReturn(new PublisherQueueInfo());
        when(clientService.findByUsername(any())).thenReturn(getClient());
        when(clientService.isClientSubscribedToEvent(testId, testId)).thenReturn(true);
        when(clientRequestsService.saveClientRequest(any())).thenReturn(testId);
        when(eventHandlerMap.getHandler(anyString())).thenReturn(eventHandler);
        Map<String, String> response = testPublisherService.publishRequestForABDM(addTestRequest, eventEnum.getEventName());
        Map.Entry<String,String> map = response.entrySet().iterator().next();
        assertTrue(map.getKey().equals(Constants.SUCCESS));
    }

    @Test
    void publishRequestForABDMEventNotFound() {
        String addTestRequest = testString;
        EventEnum eventEnum = EventEnum.ABDM_ADD_UPDATE_CONSENT;
        when(eventService.findEventByEventName(eventEnum.getEventName())).thenReturn(null);
        Map<String, String> response = testPublisherService.publishRequestForABDM(addTestRequest, eventEnum.getEventName());
        Map.Entry<String,String> map = response.entrySet().iterator().next();
        assertTrue(map.getKey().equals(Constants.ERROR));
    }

    @Test
    void publishRequestForABDMClientNotFound() {
        String addTestRequest = testString;
        EventEnum eventEnum = EventEnum.ABDM_ADD_UPDATE_CONSENT;
        when(clientService.findByUsername(any())).thenReturn(null);
        Map<String, String> response = testPublisherService.publishRequestForABDM(addTestRequest, eventEnum.getEventName());
        Map.Entry<String,String> map = response.entrySet().iterator().next();
        assertTrue(map.getKey().equals(Constants.ERROR));
    }

    @Test
    void publishRequestForABDMNotSubscribedToEvent() {
        String addTestRequest = testString;
        EventEnum eventEnum = EventEnum.ABDM_ADD_UPDATE_CONSENT;
        when(eventService.findEventByEventName(eventEnum.getEventName())).thenReturn(getEvent());
        when(clientService.findByUsername(any())).thenReturn(getClient());
        when(clientService.isClientSubscribedToEvent(testId, testId)).thenReturn(false);
        Map<String, String> response = testPublisherService.publishRequestForABDM(addTestRequest, eventEnum.getEventName());
        Map.Entry<String,String> map = response.entrySet().iterator().next();
        assertTrue(map.getKey().equals(Constants.ERROR));
    }

    @Test
    void publishRequestForABDMHandlerNotFound() {
        String addTestRequest = testString;
        EventEnum eventEnum = EventEnum.ABDM_ADD_UPDATE_CONSENT;
        when(eventService.findEventByEventName(eventEnum.getEventName())).thenReturn(getEvent());
        when(clientService.findByUsername(any())).thenReturn(getClient());
        when(clientService.isClientSubscribedToEvent(testId, testId)).thenReturn(true);
        when(eventHandlerMap.getHandler(anyString())).thenReturn(null);
        Map<String, String> response = testPublisherService.publishRequestForABDM(addTestRequest, eventEnum.getEventName());
        Map.Entry<String,String> map = response.entrySet().iterator().next();
        assertTrue(map.getKey().equals(Constants.ERROR));
    }

    private Client getClient() {
        Client client = new Client();
        client.setId(testId);
        return client;
    }

    private Event getEvent() {
        Event event = new Event();
        event.setId(testId);
        return event;
    }
}
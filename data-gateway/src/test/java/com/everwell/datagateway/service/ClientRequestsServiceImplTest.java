package com.everwell.datagateway.service;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.entities.*;
import com.everwell.datagateway.enums.RefIdStatusEnum;
import com.everwell.datagateway.models.request.RefIdStatusRequest;
import com.everwell.datagateway.models.response.RefIdStatusResponse;
import com.everwell.datagateway.repositories.ClientRequestsArchiveRepository;
import com.everwell.datagateway.repositories.ClientRequestsRepository;
import org.assertj.core.util.Arrays;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import com.everwell.datagateway.repositories.EventClientRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.constraints.AssertTrue;
import static org.mockito.ArgumentMatchers.any;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class ClientRequestsServiceImplTest extends BaseTest {

    @InjectMocks
    private ClientRequestsServiceImpl clientRequestsService;

    @Mock
    private ClientRequestsRepository clientRequestsRepository;

    @Mock
    private ClientRequestsArchiveRepository clientRequestsArchiveRepository;
    @Mock
    private SubscriberUrlService subscriberUrlService;

    @Mock
    private EventClientRepository eventClientRepository;

    private Long testId = 2L;

    @Test
    void getStatusByRefIdReqCompletedOrNotFound() {
        RefIdStatusRequest refIdStatusRequest = new RefIdStatusRequest();
        refIdStatusRequest.setReferenceId(testId);
        when(clientRequestsRepository.findTopById(testId)).thenReturn(null);
        RefIdStatusResponse refResponse = clientRequestsService.getStatusByRefId(refIdStatusRequest);
        assertEquals(refResponse.getStatus(), RefIdStatusEnum.REQUEST_COMPLETED_OR_NOT_FOUND.getStatus());
        assertNull(refResponse.getPayload());
    }

    @Test
    void getStatusByRequestCompleteAndNotified() {
        RefIdStatusRequest refIdStatusRequest = new RefIdStatusRequest();
        refIdStatusRequest.setReferenceId(testId);
        ClientRequests clientRequests = new ClientRequests();
        clientRequests.setResponseData("Helps in debugging");
        clientRequests.setResultDelivered(true);
        when(clientRequestsRepository.findTopById(testId)).thenReturn(clientRequests);
        RefIdStatusResponse refResponse = clientRequestsService.getStatusByRefId(refIdStatusRequest);
        assertEquals(refResponse.getStatus(), RefIdStatusEnum.REQUEST_COMPLETED_AND_NOTIFIED.getStatus());
        assertEquals(refResponse.getPayload(), "Helps in debugging");
    }

    @Test
    void getStatusByRequestRequestInProcess() {
        RefIdStatusRequest refIdStatusRequest = new RefIdStatusRequest();
        refIdStatusRequest.setReferenceId(testId);
        ClientRequests clientRequests = new ClientRequests();
        clientRequests.setResponseData("Helps in debugging");
        clientRequests.setResultDelivered(false);
        when(clientRequestsRepository.findTopById(testId)).thenReturn(clientRequests);
        RefIdStatusResponse refResponse = clientRequestsService.getStatusByRefId(refIdStatusRequest);
        assertEquals(refResponse.getStatus(), RefIdStatusEnum.REQUEST_IN_PROCESS.getStatus());
        assertEquals(refResponse.getPayload(), "Helps in debugging");
    }

    @Test
    void setIsDeliveredTrueAndDeliveredAtTimeTestClientRequestNull(){
        when(clientRequestsRepository.findTopById(1L)).thenReturn(null);
        clientRequestsService.setIsDeliveredTrueAndDeliveredAtTime(1L, new Date());
        Mockito.verify(clientRequestsRepository, Mockito.times(0)).save(any());
    }

    @Test
    void setIsDeliveredTrueAndDeliveredAtTimeTest(){
        ClientRequests clientRequests = new ClientRequests();
        when(clientRequestsRepository.findTopById(1L)).thenReturn(clientRequests);
        clientRequestsService.setIsDeliveredTrueAndDeliveredAtTime(1L, new Date());
        Mockito.verify(clientRequestsRepository, Mockito.times(1)).save(any());
    }


    @Test
    void archiveClientRequestJobEnabledTest(){
        clientRequestsService.setClientRequestArchivalEnabled(true);
        Date date = Date.from(ZonedDateTime.now().minusMonths(2).toInstant());
        Date deliveredAtDateTime = Date.from(ZonedDateTime.now().minusMonths(1).toInstant());
        Event event = new Event();
        event.setId(1L);
        Client client = new Client();
        client.id = 1L;
        SubscriberUrl subUrl = new SubscriberUrl();
        subUrl.setId(1L);
        ClientRequests clientRequest = new ClientRequests(1L, subUrl, date, true, "test", event, client, "test", date, date, 200, "test");
        List<ClientRequests> clientRequests = Collections.synchronizedList(new ArrayList<>());
        clientRequests.add(clientRequest);
        doReturn(clientRequests).when(clientRequestsRepository).findByDeliveredAtLessThanEqual(any());
        clientRequestsService.archiveClientRequestData();
        Mockito.verify(clientRequestsArchiveRepository, times(1)).saveAll(any());
        Mockito.verify(clientRequestsRepository, times(1)).deleteByDeliveredAtLessThanEqual(any());
    }

    @Test
    void updateClientRequestbasedonRefId() {
        ClientRequests clientRequests = new ClientRequests();
        clientRequests.setClient(new Client());
        clientRequests.setEvent(new Event());
        when(clientRequestsRepository.findTopById(testId)).thenReturn(clientRequests);
        when(eventClientRepository.findByClientIdAndEventId(any(),any())).thenReturn(new EventClient());
        when(subscriberUrlService.findByEventClientId(any())).thenReturn(new SubscriberUrl());
        when(clientRequestsRepository.save(any())).thenReturn(clientRequests);
        clientRequestsService.updateClientRequestbasedonRefId(testId,"Test Message");
    }


    @Test
    void archiveClientRequestJobDisabledTest(){
        clientRequestsService.setClientRequestArchivalEnabled(false);
        clientRequestsService.archiveClientRequestData();
        Mockito.verify(clientRequestsArchiveRepository, times(0)).saveAll(any());
        Mockito.verify(clientRequestsRepository, times(0)).deleteByDeliveredAtLessThanEqual(any());
    }
}
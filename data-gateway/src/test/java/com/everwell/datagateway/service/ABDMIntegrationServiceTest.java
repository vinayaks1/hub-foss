package com.everwell.datagateway.service;

import com.everwell.datagateway.BaseTest;
import com.everwell.datagateway.apiHelpers.ABDMHelper;
import com.everwell.datagateway.component.AppProperties;
import com.everwell.datagateway.entities.ClientRequests;
import com.everwell.datagateway.models.dto.LinkConfirmRedisDto;
import com.everwell.datagateway.models.request.abdm.*;
import com.everwell.datagateway.models.request.abdm.ABDMLinkInitiationRequest.*;
import com.everwell.datagateway.models.response.ApiResponse;
import com.everwell.datagateway.models.response.GetEpisodeByPersonResponse;
import com.everwell.datagateway.models.response.LinkInitiationOtpResponse;
import com.everwell.datagateway.publishers.RMQPublisher;
import com.everwell.datagateway.utils.CacheUtils;
import com.everwell.datagateway.utils.Utils;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.stubbing.Answer;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;


import java.util.Collections;
import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

public class ABDMIntegrationServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    ABDMIntegrationService abdmIntegrationService;

    @InjectMocks
    CacheUtils cacheUtils;

    @Mock
    RedisTemplate<String, Object> redisTemplate;

    @Mock
    private ValueOperations valueOperations;

    @Mock
    private EventService eventService;

    @Mock
    private AuthenticationService authenticationService;

    @Mock
    private ClientService clientService;

    @Mock
    private ClientRequestsService clientRequestsService;

    @Mock
    private ABDMHelper abdmHelper;

    @Mock
    private NikshayRestService nikshayRestService;

    @Mock
    private RMQPublisher rmqPublisher;

    @Mock
    private EpisodeServiceRestService episodeServiceRestService;

    @Mock
    private AppProperties appProperties;

    public void init() {
        cacheUtils = new CacheUtils(redisTemplate);
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
    }

    @Test
    public void processLinkInitiationRequestTest() throws JsonProcessingException {
        init();
        ABDMLinkInitiationRequest abdmLinkInitiationRequest = new ABDMLinkInitiationRequest();
        abdmLinkInitiationRequest.setTransactionId("test");
        Patient patient = new Patient();
        patient.setReferenceNumber("58420");
        CareContext careContext = new CareContext();
        careContext.setReferenceNumber("10724488");
        patient.setCareContexts(Collections.singletonList(careContext));
        abdmLinkInitiationRequest.setPatient(patient);
        when(redisTemplate.opsForValue().get(eq("ON_DISCOVERY_REQUEST:test"))).thenReturn(Utils.asJsonString(getRedisDto().getAbdmOnDiscoveryRequest()));
        when(cacheUtils.hasKey(eq("ON_DISCOVERY_REQUEST:test"))).thenReturn(true);
        when(nikshayRestService.getAuthToken(any())).thenReturn("token");
        when(nikshayRestService.sendOtpLinkInitiation(any(),any())).thenReturn(new ApiResponse<>("true",new LinkInitiationOtpResponse("453652","Otp sent"),null));
        when(abdmHelper.OnLinkInit(any())).thenReturn(true);
        when(clientRequestsService.getClientRequestsById(any())).thenReturn(new ClientRequests());
        abdmIntegrationService.processLinkInitiationRequest(abdmLinkInitiationRequest);
        Mockito.verify(nikshayRestService,Mockito.times(1)).getAuthToken(any());
        Mockito.verify(nikshayRestService,Mockito.times(1)).sendOtpLinkInitiation(any(),any());
    }

    @Test
    public void processLinkConfirmationRequestTest() {
        init();
        ABDMLinkConfirmationRequest abdmLinkConfirmationRequest = new ABDMLinkConfirmationRequest();
        abdmLinkConfirmationRequest.setConfirmation(new ABDMLinkConfirmationRequest.Confirmation("58420","123564"));
        when(cacheUtils.hasKey(eq("LINK_INITIATION_REQUEST:58420"))).thenReturn(true);
        when(redisTemplate.opsForValue().get(eq("LINK_INITIATION_REQUEST:58420"))).thenReturn(Utils.asJsonString(getRedisDto()));
        when(episodeServiceRestService.updateEpisodeDetails(any(),any())).thenReturn(new ApiResponse<>("true",new GetEpisodeByPersonResponse(1234,new Date().toString()),null));
        when(episodeServiceRestService.getAuthToken(any())).thenReturn("134");
        doNothing().when(rmqPublisher).send(any(),any(),any(),any());
        when(abdmHelper.OnLinkConfirm(any())).thenReturn(true);
        when(clientRequestsService.getClientRequestsById(any())).thenReturn(new ClientRequests());
        abdmIntegrationService.processLinkConfirmationRequest(abdmLinkConfirmationRequest);
    }

    LinkConfirmRedisDto getRedisDto() {
        LinkConfirmRedisDto linkConfirmRedisDto = new LinkConfirmRedisDto();
        ABDMOnDiscoveryRequest abdmOnDiscoveryRequest = new ABDMOnDiscoveryRequest();
        abdmOnDiscoveryRequest.setPatient(new ABDMCareContextDTO("58420","10724488","test",new Date().toString()));
        linkConfirmRedisDto.setAbdmOnDiscoveryRequest(abdmOnDiscoveryRequest);
        linkConfirmRedisDto.setOtp("123564");
        return linkConfirmRedisDto;
    }

    @Test
    public void processConsentInitiationTest() {
        init();
        ABDMConsentRequestOnInitiation abdmConsentRequestOnInitiation = new ABDMConsentRequestOnInitiation();
        ABDMConsentRequestOnInitiation.Resp resp = new ABDMConsentRequestOnInitiation.Resp("3847cbb1-1302-47b5-a8ff-777e427ce38c");
        abdmConsentRequestOnInitiation.setConsentRequest(new ABDMConsentRequestOnInitiation.ConsentRequest("45cd0c41-c502-4ab3-acaa-3ee87773e362"));
        abdmConsentRequestOnInitiation.setResp(resp);
        when(cacheUtils.hasKey(eq("CONSENT_INITIATION_REQUEST:3847cbb1-1302-47b5-a8ff-777e427ce38c"))).thenReturn(true);
        when(redisTemplate.opsForValue().get(eq("CONSENT_INITIATION_REQUEST:3847cbb1-1302-47b5-a8ff-777e427ce38c"))).thenReturn(Utils.asJsonString(getRedisDto()));
        doNothing().when(rmqPublisher).send(any(),any(),any(),any());
        abdmIntegrationService.processConsentInitiation(abdmConsentRequestOnInitiation);
    }
    @Test
    public void addCareContextDirectAuthTest() throws JsonProcessingException {
        init();
        ABDMAuthNotifyRequest abdmAuthNotifyRequest = Utils.convertStrToObject(getAuthNotifyJson(),ABDMAuthNotifyRequest.class);
        when(redisTemplate.opsForValue().get(eq("19d38558-1ef6-44c5-bc0e-6f8feee50f7a"))).thenReturn("kmk@sbx");
        when(redisTemplate.opsForValue().get(eq("kmk@sbx"))).thenReturn(Utils.asJsonString(Utils.convertStrToObject(getABDMLinkCareContextEvent(),ABDMLinkCareContextEvent.class)));
        doNothing().when(abdmHelper).authOnNotify(any());
        when(abdmHelper.addCareContextRequest(any())).thenReturn("");
        Assertions.assertTrue(abdmIntegrationService.addCareContextDirectAuth(abdmAuthNotifyRequest));
    }

    @Test
    public void notifyCareContextTest() {
        ABDMNotifyCareContextEvent abdmNotifyCareContextEvent = new ABDMNotifyCareContextEvent(1L,1L,"Prescription","Test","Test@sbx");
        when(episodeServiceRestService.updateEpisodeDetails(any(),any())).thenReturn(new ApiResponse<>("true",new GetEpisodeByPersonResponse(1234,new Date().toString()),null));
        when(episodeServiceRestService.getAuthToken(any())).thenReturn("134");
        when(abdmHelper.notifyCareContext(any())).thenReturn("");
        Assertions.assertTrue(abdmIntegrationService.notifyCareContext(abdmNotifyCareContextEvent));
    }

    @Test
    public void processConsentNotificationTest() throws JsonProcessingException {
        ABDMConsentNotification abdmConsentNotification = Utils.convertStrToObject(getConsentNotifyJson(),ABDMConsentNotification.class);
        abdmIntegrationService.processConsentNotification(abdmConsentNotification);
    }

    private String getAuthNotifyJson() {
        return "{\"requestId\":\"baf556d0-6e37-4cb5-a63e-f4a59725f3fe\",\"timestamp\":\"2023-01-03T11:40:04.765923\",\"auth\":{\"transactionId\":\"19d38558-1ef6-44c5-bc0e-6f8feee50f7a\",\"status\":\"GRANTED\",\"accessToken\":\"eyJhbGciOiJSUzUxMiJ9.eyJzdWIiOiJtb2hhbmtyaXNobmExOTk5QHNieCIsInJlcXVlc3RlclR5cGUiOiJISVAiLCJyZXF1ZXN0ZXJJZCI6Ik5JS1NIQVktSElQIiwicGF0aWVudElkIjoibW9oYW5rcmlzaG5hMTk5OUBzYngiLCJzZXNzaW9uSWQiOiJkYjQ0MGJkMi05NTc3LTQ3NDQtOWNhYi1iNGNhNDUxMzZhMTIiLCJleHAiOjE2NzI4MzI0MDQsImlhdCI6MTY3Mjc0NjAwNH0.bsAk4F2VDNjHOLFOtdvTgN28X2-Yn0BGzRLAGn8TUHdDz7b7LlzCivZzDdZszPWnx3Qw8huuf6muUmqBgH7ajhlmfdmHPbC3Bz6QGqemMPJfGxjFDKz5hQyeEPeOdbkBpf4NMrfRqPmxws8O6SsbWOoOW-yMoldQaArua_qO5ojonofSG-pFtt0Jty2cAQJ-HAgiRXZ1gEkLS-jfYk-UCX2KXE20fewykbi0LEifcRF8aE5vgnDZ40zSlTDe7Rm6eIvr3BvyoMxvkiI0BJJw4o6goZqebDkN_mx0mxk3xFagiqpMF6VZhHgFT8SP2DUCSkW5n0LVa6c3FrjQgV2NWg\",\"validity\":{\"purpose\":\"LINK\",\"requester\":{\"type\":\"HIP\",\"id\":\"NIKSHAY-HIP\",\"name\":null},\"expiry\":\"2024-01-03T11:40:04.765873\",\"limit\":1},\"patient\":null}}";
    }

    private String getABDMLinkCareContextEvent() {
        return "{\"episodeId\":\"10750687\",\"personId\":\"41777\",\"hiTypes\":\"Prescription\",\"personName\":\"Ankur Mandal\",\"enrollmentDate\":\"24/06/2021\",\"identifierValue\":\"+919903246033\",\"gender\":\"M\",\"identifierType\":\"MOBILE\",\"yearOfBirth\":\"1996\",\"abhaAddress\":\"ankur.test2@sbx\"}";
    }

    private String getConsentNotifyJson() {
        return "{\"requestId\":\"5f7a535d-a3fd-416b-b069-c97d021fbacd\",\"timestamp\":\"1970-01-01T00:00:00.000Z\",\"notification\":{\"consentRequestId\":\"5f7a535d-a3fd-416b-b069-c97d021fbacd\",\"status\":\"GRANTED\",\"consentArtefacts\":[{\"id\":\"5f7a535d-a3fd-416b-b069-c97d021fbacd\"}]}}";
    }

}

package com.everwell.datagateway.filters.zuul.smartPayments

import com.everwell.datagateway.constants.Constants
import com.everwell.datagateway.constants.SecurityConstants
import com.everwell.datagateway.filters.zuul.base.BaseZuulRouteFilter
import com.everwell.datagateway.service.BaseRestService
import com.everwell.datagateway.service.SmartPaymentsRestService
import com.netflix.zuul.context.RequestContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants

open class SmartPaymentsZuulRouteFilter: BaseZuulRouteFilter() {

    @Autowired
    lateinit var smartPaymentsRestService: SmartPaymentsRestService

    override var thisURI: String
        get() = "/smartpayments"
        set(value) {}

    override fun getHeadersForAuth(context: RequestContext): MutableMap<String, String> {
        val map = mutableMapOf<String, String>()
        map["Authorization"] = SecurityConstants.TOKEN_PREFIX + smartPaymentsRestService.getAuthToken(context.request.getHeader(Constants.CLIENT_ID), context.request.getHeader(Constants.CLIENT_SECRET))!!
        return map
    }

    override fun getUrlMapping(context: RequestContext): String? {
        return context.request.requestURI.substring(thisURI.length)
    }

    override fun getRestService(): BaseRestService {
        return smartPaymentsRestService
    }

    override fun filterType(): String {
        return FilterConstants.ROUTE_TYPE
    }

    override fun filterOrder(): Int {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER + 10
    }
}
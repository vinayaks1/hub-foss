package com.everwell.datagateway.filters.zuul

import com.everwell.datagateway.constants.Constants
import com.everwell.datagateway.enums.LegacyRoutesEnum
import com.everwell.datagateway.service.AuthenticationService
import com.everwell.datagateway.service.ClientService
import com.everwell.datagateway.service.SubscriberUrlService
import com.netflix.zuul.ZuulFilter
import com.netflix.zuul.context.RequestContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.PRE_TYPE
import org.springframework.http.HttpStatus
import org.springframework.security.core.context.SecurityContextHolder
import javax.servlet.http.HttpServletRequest




open class APIAccessZuulPreFilter(

) : ZuulFilter(){

    @Autowired
    lateinit var clientService: ClientService

    @Autowired
    lateinit var authenticationService: AuthenticationService

    @Autowired
    lateinit var subscriberUrlService: SubscriberUrlService

    override fun run(): Any? {
        val ctx: RequestContext = RequestContext.getCurrentContext()
        val requestServlet: HttpServletRequest = ctx.getRequest()
        val subscriberUrls = subscriberUrlService.findByUrl(requestServlet.requestURI);
        if(subscriberUrls!=null){
            for(subscriberUrl in subscriberUrls){
                if(subscriberUrl.eventClient.clientId == clientService.findByUsername(getUserName()).id)
                    return null
            }
        }

        // User dont have access so requested URI
        ctx.setSendZuulResponse(false)
        ctx.unset()
        ctx.setResponseBody("Not authorised to call '" + requestServlet.requestURI +"' route")
        ctx.setResponseStatusCode(HttpStatus.UNAUTHORIZED.value())
        return null
    }

    override fun shouldFilter(): Boolean {
        if (LegacyRoutesEnum.isLegacyRoute(RequestContext.getCurrentContext().request.requestURI)){
            return false;
        }
        return getUserName()?.toLowerCase() != Constants.INTERNAL_CLIENT_USERNAME
    }

    override fun filterType(): String {
        return PRE_TYPE
    }

    override fun filterOrder(): Int {
       return 1
    }

    fun getUserName():String?{
        val authentication = SecurityContextHolder.getContext().authentication
        return authenticationService.getCurrentUsername(authentication)
    }

}
package com.everwell.datagateway.filters.zuul.lims

import com.everwell.datagateway.filters.zuul.base.BaseZuulRouteFilter
import com.everwell.datagateway.service.BaseRestService
import com.everwell.datagateway.service.NikshayRestService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants

open class LimsZuulRouteFilter (

): BaseZuulRouteFilter() {

    @Autowired
    lateinit var nikshayRestService: NikshayRestService

    override var thisURI: String
        get() = "/lims"
        set(value) {}

    init {
        /// keep adding the url mapping here as LIMS know which API to be called
        urlMapping.put("/lims/addPatient","/api/Patients/AddIndiaTb")
        urlMapping.put("/lims/getDuplicates","/api/Patients/GetPossibleDuplicates")
        urlMapping.put("lims/getPatientDetails","api/Patients/Get/{patientId}")
    }

    override fun getRestService(): BaseRestService {
        return nikshayRestService
    }

    override fun filterType(): String {
        return FilterConstants.ROUTE_TYPE
    }

    override fun filterOrder(): Int {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER +2
    }
}
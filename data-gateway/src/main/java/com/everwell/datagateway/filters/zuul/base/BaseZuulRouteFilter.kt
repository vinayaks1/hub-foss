package com.everwell.datagateway.filters.zuul.base

import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.constants.GenericConstants
import com.everwell.datagateway.service.BaseRestService
import com.netflix.zuul.context.RequestContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.ProxyRequestHelper
import org.springframework.http.HttpStatus
import javax.servlet.http.HttpServletRequest

abstract class BaseZuulRouteFilter : BaseZuulFilter() {
    @Autowired
    lateinit var proxyRequestHelper: ProxyRequestHelper

    @Autowired
    lateinit var appProperties: AppProperties

    abstract fun getRestService() : BaseRestService

    var urlMapping: HashMap<String, String> = HashMap()

    override fun run(): Any? {
        val ctx: RequestContext = RequestContext.getCurrentContext()
        val requestServlet: HttpServletRequest = ctx.getRequest()
        ctx["requestURI"] = getUrlMapping(ctx)
        if(ctx["requestURI"]==null){
            ctx.setSendZuulResponse(false)
            ctx.unset()
            ctx.setResponseBody("Not authorised to call '" + requestServlet.requestURI +"' route")
            ctx.setResponseStatusCode(HttpStatus.UNAUTHORIZED.value())
        }else{
            getHeadersForAuth(ctx).forEach {
                ctx.addZuulRequestHeader(it.key, it.value)
            }
        }
        return null
    }

    open fun getHeadersForAuth(context: RequestContext): MutableMap<String, String> {
        val map = mutableMapOf<String, String>()
        map[GenericConstants.auth_header] = GenericConstants.auth_type + getRestService().getAuthToken(null)
        return map
    }

    open fun getUrlMapping(context: RequestContext):String?{
        val requestServlet: HttpServletRequest = context.getRequest()
        var mappedUrl = urlMapping.get(requestServlet.requestURI)
        if(mappedUrl!=null && mappedUrl?.contains("{") && mappedUrl?.contains("}")){
            val queryParamMap = this.proxyRequestHelper.buildZuulRequestQueryParams(context.request)
            val regex = Regex("""\{([^{}]*)\}""")
            val matches = regex.findAll(mappedUrl)
            for(match in matches){
                mappedUrl = mappedUrl?.replace(match.groups[0]?.value!!,queryParamMap.get(match.groups[1]?.value)?.get(0)!!,true)
            }
        }
        return mappedUrl
    }

}
package com.everwell.datagateway.filters.zuul.dure
import com.everwell.datagateway.filters.zuul.base.BaseZuulRouteFilter
import com.everwell.datagateway.service.BaseRestService
import com.everwell.datagateway.service.NikshayReportsRestService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants

open class DureZuulRouteFilter (

): BaseZuulRouteFilter() {


    @Autowired
    lateinit var nikshayReportsRestService: NikshayReportsRestService

    override var thisURI: String
        get() = "/dure"
        set(value) {}

    init {
        /// keep adding the url mapping here as Soch know which API to be called
        urlMapping.put("/dure/mstrState","/nikshayapiv2/api/masters/mstrState")
        urlMapping.put("/dure/mstrDistrict","/nikshayapiv2/api/masters/mstrDistrict")
        urlMapping.put("/dure/mstrDmcs","/nikshayapiv2/api/masters/mstrDmcs")
        urlMapping.put("/dure/mstrTbunits","/nikshayapiv2/api/masters/mstrTbunits")
    }

    override fun getRestService(): BaseRestService {
        return nikshayReportsRestService
    }

    override fun filterType(): String {
        return FilterConstants.ROUTE_TYPE
    }

    override fun filterOrder(): Int {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER +4
    }
}
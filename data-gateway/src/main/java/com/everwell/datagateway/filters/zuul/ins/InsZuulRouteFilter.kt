package com.everwell.datagateway.filters.zuul.ins

import com.everwell.datagateway.filters.zuul.base.BaseZuulRouteFilter
import com.everwell.datagateway.service.BaseRestService
import com.everwell.datagateway.service.InsRestService
import com.netflix.zuul.context.RequestContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants

open class InsZuulRouteFilter (

): BaseZuulRouteFilter() {

    @Autowired
    lateinit var insRestService: InsRestService

    override var thisURI: String
        get() = "/ins"
        set(value) {}

    override fun getHeadersForAuth(context: RequestContext): MutableMap<String, String> {
        val map = mutableMapOf<String, String>()
        map["Authorization"] = "Bearer " + insRestService.getAuthToken(context.request.getHeader("client-id"))!!
        map["ins-client-id"] = context.request.getHeader("client-id")
        return map
    }

    override fun getUrlMapping(context: RequestContext): String? {
        return context.request.requestURI.substring(thisURI.length)
    }

    override fun getRestService(): BaseRestService {
        return insRestService
    }

    override fun filterType(): String {
        return FilterConstants.ROUTE_TYPE
    }

    override fun filterOrder(): Int {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER + 10
    }
}
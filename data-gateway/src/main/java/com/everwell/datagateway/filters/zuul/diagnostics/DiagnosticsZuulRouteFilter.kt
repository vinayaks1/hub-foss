package com.everwell.datagateway.filters.zuul.diagnostics

import com.everwell.datagateway.constants.GenericConstants
import com.everwell.datagateway.filters.zuul.base.BaseZuulRouteFilter
import com.everwell.datagateway.service.BaseRestService
import com.everwell.datagateway.service.DiagnosticsRestService
import com.netflix.zuul.context.RequestContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants

open class DiagnosticsZuulRouteFilter : BaseZuulRouteFilter() {

    @Autowired
    lateinit var diagnosticsRestService: DiagnosticsRestService

    override var thisURI: String
        get() = "/diagnostics"
        set(value) {}

    override fun getHeadersForAuth(context: RequestContext): MutableMap<String, String> {
        val map = mutableMapOf<String, String>()
        map[GenericConstants.auth_header] = GenericConstants.auth_type + diagnosticsRestService.getAuthToken(context.request.getHeader("client-id"))!!
        map["X-Diagnostics-Client-Id"] = context.request.getHeader("client-id")
        return map
    }

    override fun getUrlMapping(context: RequestContext): String? {
        return context.request.requestURI.substring(thisURI.length)
    }

    override fun getRestService(): BaseRestService {
        return diagnosticsRestService
    }

    override fun filterType(): String {
        return FilterConstants.ROUTE_TYPE
    }

    override fun filterOrder(): Int {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER + 10
    }
}
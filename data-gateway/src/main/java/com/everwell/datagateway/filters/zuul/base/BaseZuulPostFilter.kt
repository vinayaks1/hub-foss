package com.everwell.datagateway.filters.zuul.base

import com.everwell.datagateway.constants.GenericConstants
import com.everwell.datagateway.service.BaseRestService
import com.netflix.zuul.context.RequestContext
import org.json.simple.JSONObject
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.ProxyRequestHelper
import org.springframework.http.*
import org.springframework.http.client.BufferingClientHttpRequestFactory
import org.springframework.http.client.SimpleClientHttpRequestFactory
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder

open abstract class BaseZuulPostFilter : BaseZuulFilter(){


    abstract fun getRestService() : BaseRestService

    @Autowired
    lateinit var helper: ProxyRequestHelper

    var restTemplateForApi: RestTemplate = RestTemplate(BufferingClientHttpRequestFactory(SimpleClientHttpRequestFactory()))


    override fun run(): Any? {
        val ctx: RequestContext = RequestContext.getCurrentContext()
        if(ctx.responseStatusCode== HttpStatus.UNAUTHORIZED.value()){
            //getRestService().authenticate()
            // Below is the space where various headers/information can be added depending upon
            // the HTTP request type.
            ctx.responseBody = requestRestServer(ctx, HttpMethod.resolve(ctx.request.method)!!)?.body?.toJSONString()
            ctx.responseStatusCode= HttpStatus.OK.value()
        }
        return null
    }

    open fun requestRestServer(ctx: RequestContext, httpMethod: HttpMethod): ResponseEntity<JSONObject>? {
        val headers = HttpHeaders()
        headers.add(GenericConstants.auth_header, GenericConstants.auth_type + getRestService().getAuthToken(null))
        val entity: HttpEntity<String> = HttpEntity<String>(headers)
        val builder = UriComponentsBuilder.fromUriString(ctx.routeHost.toString()+ctx.get("requestURI")) // rawValidURl = http://example.com/hotels
                .queryParams(this.helper.buildZuulRequestQueryParams(ctx.request)) // The allRequestParams must have been built for all the query params
        val uriComponents = builder.build().encode() // encode() is to ensure that characters like {, }, are preserved and not encoded. Skip if not needed.
        return restTemplateForApi.exchange(uriComponents.toUri(), httpMethod,
                entity, JSONObject::class.java)
    }

}
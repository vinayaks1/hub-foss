package com.everwell.datagateway.filters.zuul.merm

import com.everwell.datagateway.constants.SecurityConstants
import com.everwell.datagateway.filters.zuul.base.BaseZuulRouteFilter
import com.everwell.datagateway.models.response.ApiResponse
import com.everwell.datagateway.models.response.ImeiClientResponse
import com.everwell.datagateway.service.BaseRestService
import com.everwell.datagateway.service.RegistryRestService
import com.everwell.datagateway.utils.Utils
import com.netflix.zuul.context.RequestContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.http.client.BufferingClientHttpRequestFactory
import org.springframework.http.client.SimpleClientHttpRequestFactory
import org.springframework.web.client.RestTemplate

open class MermZuulRouteFilter (

): BaseZuulRouteFilter() {

    @Autowired
    lateinit var registryRestService: RegistryRestService

    var restTemplateForApi: RestTemplate = RestTemplate(BufferingClientHttpRequestFactory(SimpleClientHttpRequestFactory()))

    override var thisURI: String
        get() = "/process-merm-event"
        set(value) {}

    init {
        urlMapping.put("/process-merm-event","/v1/merm/event")
    }

    override fun getHeadersForAuth(context: RequestContext): MutableMap<String, String> {
        val map = mutableMapOf<String, String>()
        var clientId : String = fetchClientId(context)
        map["Authorization"] = SecurityConstants.TOKEN_PREFIX + " " + registryRestService.getAuthToken(clientId)!!
        map["X-Client-Id"] = clientId
        return map
    }

    override fun getRestService(): BaseRestService {
        return registryRestService
    }

    override fun filterType(): String {
        return FilterConstants.ROUTE_TYPE
    }

    override fun filterOrder(): Int {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER -2
    }

    private fun fetchClientId(context: RequestContext) : String {
        val headers = HttpHeaders()
        val request = Utils.readRequestBody(context.request.inputStream)
        headers.add("Content-Type", MediaType.APPLICATION_JSON.toString())
        var clientId: String = appProperties.registryDefaultClientId;
        try {
            clientId = restTemplateForApi.exchange(
            appProperties.registryServerUrl + "/v1/imei/client-id", HttpMethod.POST, HttpEntity<Any>(request, headers),
            object : ParameterizedTypeReference<ApiResponse<ImeiClientResponse>>() {} ).body?.data?.clientId.toString()
        } catch (e : Exception) {
            // NB: James (from WisePill) expects that this endpoint will return a 2xx response
            // even if the IMEI is unknown so using default client-id
        }
        return clientId;
    }
}
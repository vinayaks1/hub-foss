package com.everwell.datagateway.filters.zuul.sso

import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.constants.Constants
import com.everwell.datagateway.filters.zuul.base.BaseZuulRouteFilter
import com.everwell.datagateway.service.BaseRestService
import com.everwell.datagateway.service.SsoRestService
import com.netflix.zuul.context.RequestContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants
import org.springframework.http.HttpStatus
import com.everwell.datagateway.constants.Constants.CLIENT_ID
import java.net.URL
import javax.servlet.http.HttpServletRequest

class SsoZuulRouteFilter :BaseZuulRouteFilter () {

    @Autowired
    lateinit var ssoRestService: SsoRestService

    override var thisURI: String
        get() = "/sso"
        set(value) {}

    override fun run(): Any? {
        val ctx: RequestContext = RequestContext.getCurrentContext()
        val requestServlet: HttpServletRequest = ctx.getRequest()
        val clientId: String = ctx.request.getHeader(CLIENT_ID)
        val ssoUrl = URL(appProperties.ssoClientIdUrlMap.get(clientId))
        ctx.routeHost = ssoUrl
        super.run()
        return null
    }

    override fun getHeadersForAuth(context: RequestContext): MutableMap<String, String> {
        val map = mutableMapOf<String, String>()
        map["X-SSO-Auth-Token"] = ssoRestService.getAuthToken(context.request.getHeader(CLIENT_ID))!!
        map["X-SSO-Client-Id"] = context.request.getHeader(CLIENT_ID)
        return map
    }

    override fun getUrlMapping(context: RequestContext): String? {
        return context.request.requestURI.substring(thisURI.length)
    }

    override fun getRestService(): BaseRestService {
        return ssoRestService
    }

    override fun filterType(): String {
        return FilterConstants.ROUTE_TYPE
    }

    override fun filterOrder(): Int {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER + 10
    }
}
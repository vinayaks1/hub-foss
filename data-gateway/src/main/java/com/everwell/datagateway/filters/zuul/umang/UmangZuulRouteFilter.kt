package com.everwell.datagateway.filters.zuul.umang

import com.everwell.datagateway.filters.zuul.base.BaseZuulRouteFilter
import com.everwell.datagateway.service.BaseRestService
import com.everwell.datagateway.service.NikshayRestService
import com.netflix.zuul.context.RequestContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.PRE_DECORATION_FILTER_ORDER
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.ROUTE_TYPE
import java.util.*
import javax.servlet.http.HttpServletRequest


open class UmangZuulRouteFilter (

): BaseZuulRouteFilter(){


    @Autowired
    lateinit var nikshayRestService: NikshayRestService

    override var thisURI: String
        get() = "/umang"
        set(value) {}

    init {
        /// keep adding the url mapping here as UMANG know which API to be called
        urlMapping.put("/umang/facilities","/api/patientFacingApp/levl5facility")
        urlMapping.put("/umang/tulist","/api/patientFacingApp/tu")
        urlMapping.put("/umang/tbInformation","/api/patientFacingApp/v1/information/tb")
        urlMapping.put("/umang/stateDistrictData","/api/patientFacingApp/stateDistrictData")
        urlMapping.put("/umang/sendOTP","/api/Patients/OTP/{patientId}")
        urlMapping.put("/umang/validateOTP","/api/Patients/OTP")
        urlMapping.put("/umang/getDBTDetails","/api/Dbt/GetCompleteDbtDetailsForPatient/{patientId}")
    }

    override fun getRestService(): BaseRestService {
        return nikshayRestService
    }

    override fun run(): Any? {
        super.run()
        val ctx: RequestContext = RequestContext.getCurrentContext()
        val requestServlet: HttpServletRequest = ctx.getRequest()
        if(ctx["requestURI"]!=null &&
                (ctx["requestURI"]!!.equals("/api/patientFacingApp/tuList") ||ctx["requestURI"]!!.equals("/api/patientFacingApp/stateDistrictData") )
        ){
           val queryParams = this.proxyRequestHelper.buildZuulRequestQueryParams(requestServlet)
            val includeNotActiveKey = "includeNotActive"
            val includeNotActiveValue = "false"
            queryParams[includeNotActiveKey] = Arrays.asList(includeNotActiveValue)
            ctx.setRequestQueryParams(queryParams)
        }
        return null
    }


    override fun filterType(): String {
        return ROUTE_TYPE
    }

    override fun filterOrder(): Int {
        return PRE_DECORATION_FILTER_ORDER+1
    }

}
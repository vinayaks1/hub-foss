package com.everwell.datagateway.filters.zuul.hub

import com.everwell.datagateway.filters.zuul.GenericZuulPostFilter
import com.everwell.datagateway.filters.zuul.base.BaseZuulPostFilter
import com.everwell.datagateway.service.BaseRestService
import com.everwell.datagateway.service.HubRestService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants

open class HubZuulPostFilter(

) : BaseZuulPostFilter(){

    @Autowired
    lateinit var hubRestService: HubRestService

    override var thisURI: String
        get() = "/hub"
        set(value) {}

    override fun getRestService(): BaseRestService {
        return hubRestService
    }

    override fun filterOrder(): Int {
        return FilterConstants.SEND_RESPONSE_FILTER_ORDER -2
    }

    override fun filterType(): String {
        return FilterConstants.POST_TYPE
    }

}
package com.everwell.datagateway.filters.zuul.gmri

import com.everwell.datagateway.filters.zuul.httpServlet.GenericHttpServletRequest
import com.everwell.datagateway.models.request.SearchIMEIRequest
import com.everwell.datagateway.utils.Utils
import com.netflix.zuul.ZuulFilter
import com.netflix.zuul.context.RequestContext
import org.apache.http.client.utils.URLEncodedUtils
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import java.nio.charset.Charset

class GmriZuulPreFilter() : ZuulFilter(){

    override fun shouldFilter(): Boolean {
        val request = RequestContext.getCurrentContext().request
        return (request.requestURI.contains("/gmri/v1/",true) && request.method.equals(HttpMethod.GET.name))
    }

    override fun filterType(): String {
        return FilterConstants.PRE_TYPE
    }

    override fun filterOrder(): Int {
        return 1
    }

    override fun run(): Any? {
        val ctx: RequestContext = RequestContext.getCurrentContext()
        val queryString = URLEncodedUtils.parse(ctx.request.queryString, Charset.forName("UTF-8"))
        if( ctx.request.requestURL.contains("/gmri/v1/imei")){
            for(query in queryString) {
                if(query.name.equals("imei")) {
                    val searchImeiRequest = SearchIMEIRequest(query.value, null, "IMEI")
                    convertGetToPost(searchImeiRequest, ctx)
                    return null
                }
                if(query.name.equals("entityId")) {
                    val searchImeiRequest = SearchIMEIRequest(null, query.value.toLongOrNull(), "PATIENT_ID")
                    convertGetToPost(searchImeiRequest, ctx)
                    return null
                }
            }
        }
        if(ctx.request.requestURL.contains("/gmri/v1/all-imei/available")) {
            val searchAvailableIMEIRequest = SearchIMEIRequest(null, null, "ACTIVE")
            convertGetToPost(searchAvailableIMEIRequest, ctx)
        }
        return null
    }

    //Method to convert GET request to POST
    private fun convertGetToPost(input : Any, ctx : RequestContext) {
        val requestData = Utils.asJsonString(input)
        val request = GenericHttpServletRequest(ctx.request, requestData.toByteArray(), HttpMethod.POST.name, MediaType.APPLICATION_JSON_VALUE )
        ctx.setRequest(request)
    }

}
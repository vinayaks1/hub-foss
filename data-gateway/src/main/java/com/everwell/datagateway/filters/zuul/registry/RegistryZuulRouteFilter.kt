package com.everwell.datagateway.filters.zuul.registry

import com.everwell.datagateway.constants.SecurityConstants.TOKEN_PREFIX
import com.everwell.datagateway.filters.zuul.base.BaseZuulRouteFilter
import com.everwell.datagateway.service.BaseRestService
import com.everwell.datagateway.service.IamRestService
import com.everwell.datagateway.service.RegistryRestService
import com.netflix.zuul.context.RequestContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants

open class RegistryZuulRouteFilter (

): BaseZuulRouteFilter() {

    @Autowired
    lateinit var registryRestService: RegistryRestService

    override var thisURI: String
        get() = "/registry"
        set(value) {}

    override fun getHeadersForAuth(context: RequestContext): MutableMap<String, String> {
        val map = mutableMapOf<String, String>()
        map["Authorization"] = TOKEN_PREFIX + " " + registryRestService.getAuthToken(context.request.getHeader("client-id"))!!
        map["X-Client-Id"] = context.request.getHeader("client-id")
        return map
    }

    override fun getUrlMapping(context: RequestContext): String? {
        return context.request.requestURI.substring(thisURI.length)
    }

    override fun getRestService(): BaseRestService {
        return registryRestService
    }

    override fun filterType(): String {
        return FilterConstants.ROUTE_TYPE
    }

    override fun filterOrder(): Int {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER + 12
    }
}
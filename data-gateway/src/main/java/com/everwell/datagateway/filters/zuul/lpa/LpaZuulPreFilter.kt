package com.everwell.datagateway.filters.zuul.lpa

import com.everwell.datagateway.constants.Constants
import com.everwell.datagateway.models.request.ChildrenLabRequest
import com.everwell.datagateway.utils.Utils
import com.netflix.zuul.ZuulFilter
import com.netflix.zuul.context.RequestContext
import org.apache.http.client.utils.URLEncodedUtils
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants
import org.springframework.http.HttpMethod
import java.nio.charset.StandardCharsets

class LpaZuulPreFilter() : ZuulFilter() {

    override fun shouldFilter(): Boolean {
        val request = RequestContext.getCurrentContext().request
        return (request.requestURI.contains("/lpa/v1/", true) && request.method.equals(HttpMethod.GET.name))
    }

    override fun filterType(): String {
        return FilterConstants.PRE_TYPE
    }

    override fun filterOrder(): Int {
        return 1
    }

    override fun run(): Any? {
        val ctx: RequestContext = RequestContext.getCurrentContext()
        val queryString = URLEncodedUtils.parse(ctx.request.queryString, StandardCharsets.UTF_8)
        if (ctx.request.requestURL.contains("/lpa/v1/available-lpa-lab")) {
            var hierarchyId: Long? = null
            var pageNo = 0
            for (query in queryString) {
                if (query.name.equals(Constants.HIERARCHY_ID)) {
                    hierarchyId = query.value.toLongOrNull()
                }
                if (query.name.equals(Constants.PAGE_NO)) {
                    pageNo = query.value.toInt()
                }
            }
            var childrenLabRequest =
                ChildrenLabRequest(hierarchyId, Constants.CDST_LAB_INCLUDING_LPA, Constants.LABS_LIST, pageNo)
            Utils.convertRequest(childrenLabRequest, ctx, HttpMethod.POST)
        }
        return null
    }
}
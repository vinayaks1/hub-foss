package com.everwell.datagateway.filters.zuul.registry

import com.everwell.datagateway.constants.Constants
import com.everwell.datagateway.filters.zuul.base.BaseZuulPostFilter
import com.everwell.datagateway.models.response.ApiResponse
import com.everwell.datagateway.models.response.MermConfigurationResponse
import com.everwell.datagateway.service.BaseRestService
import com.everwell.datagateway.service.RegistryRestService
import com.everwell.datagateway.utils.Utils
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.netflix.util.Pair
import com.netflix.zuul.context.RequestContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.*
import java.nio.charset.StandardCharsets


open class RegistryZuulPostFilter : BaseZuulPostFilter() {

    @Autowired
    lateinit var registryRestService: RegistryRestService

    @Value("\${nikshay.web.app.url}")
    lateinit var nikshayWebAppURL: String

    override var thisURI: String
        get() = "/registry"
        set(value) {}

    override fun getRestService(): BaseRestService {
        return registryRestService
    }

    override fun filterOrder(): Int {
        return FilterConstants.SEND_RESPONSE_FILTER_ORDER -2
    }

    override fun filterType(): String {
        return FilterConstants.POST_TYPE
    }

    // Filter only for process merm event and get merm config
    override fun shouldFilter():Boolean{
        val request = RequestContext.getCurrentContext().request
        return (request.requestURI.contains("registry/v1/merm/event",true) || request.requestURI.contains("process-merm-event",true) || request.requestURI.contains("get-merm-config",true))
    }

    override fun run(): Any? {
        val ctx: RequestContext = RequestContext.getCurrentContext()
        if(ctx.request.requestURI.contains("get-merm-config",true)) {
            getMermConfig(ctx)
        }
        else {
            processMermEvent(ctx)
        }
        ctx.zuulResponseHeaders.clear()
        ctx.response.setContentLength(ctx.responseBody.length)
        ctx.zuulResponseHeaders.add(Pair(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN.toString()))
        ctx.response.characterEncoding = StandardCharsets.UTF_8.name()
        ctx.zuulResponseHeaders.add(Pair(HttpHeaders.CACHE_CONTROL, CacheControl.empty().cachePrivate().headerValue))
        ctx.zuulResponseHeaders.add(Pair("Keep-Alive", "timeout=90, max=100"))
        return null
    }

    private fun processMermEvent(ctx : RequestContext) {
        if(ctx.responseStatusCode == HttpStatus.OK.value()){
            val responseData = if (ctx.responseGZipped) Utils.readGzipRequestBody(ctx.responseDataStream) else Utils.readRequestBody(ctx.responseDataStream)
            val response = Utils.convertStrToObject(responseData, ApiResponse::class.java)
            ctx.responseBody = response.data.toString()
        }
        if(ctx.responseStatusCode != HttpStatus.OK.value()) {
            ctx.responseStatusCode = HttpStatus.INTERNAL_SERVER_ERROR.value()
            ctx.responseBody = "ER"
        }
    }

    private fun getMermConfig(ctx: RequestContext) {
        ctx.responseBody = Utils.asJsonString(MermConfigurationResponse())
        try {
            if (ctx.responseStatusCode == HttpStatus.OK.value()) {
                val responseData = if (ctx.responseGZipped) Utils.readGzipRequestBody(ctx.responseDataStream) else Utils.readRequestBody(ctx.responseDataStream)
                val response: ApiResponse<MermConfigurationResponse> = Gson().fromJson(
                    responseData, object : TypeToken<ApiResponse<MermConfigurationResponse?>?>() {}.type )
                if (response.data != null) {
                    ctx.responseBody = Utils.asJsonString(MermConfigurationResponse(response.data))
                }
                // if noforward is not equal to 1 forward to nikshay
                else if (!ctx.requestQueryParams.containsKey(Constants.NO_FORWARD) || ctx.requestQueryParams.getValue(Constants.NO_FORWARD).get(0) != "1") {
                    ctx.responseBody = getMermConfigNikshay(ctx)
                }
            }
            if (ctx.responseStatusCode != HttpStatus.OK.value()) {
                if (!ctx.requestQueryParams.containsKey(Constants.NO_FORWARD) || ctx.requestQueryParams.getValue(Constants.NO_FORWARD).get(0) != "1") {
                    ctx.responseBody = getMermConfigNikshay(ctx)
                }
            }
        } catch (e : Exception){
            // In case of any error send default merm config
            ctx.responseBody = Utils.asJsonString(MermConfigurationResponse())
        }
        ctx.responseStatusCode = HttpStatus.OK.value()
    }

    private fun getMermConfigNikshay(ctx : RequestContext) : String? {
        val request = Utils.readRequestBody(ctx.request.inputStream)
        val headers = HttpHeaders()
        headers.add("Content-Type", MediaType.APPLICATION_JSON.toString())
        return restTemplateForApi.exchange(
            "$nikshayWebAppURL/GetMERMConfig.aspx?" + ctx.request.queryString,
            HttpMethod.GET,
            HttpEntity<Any>(request, headers),
            object : ParameterizedTypeReference<String>() {}
        ).body?.toString()
    }
}
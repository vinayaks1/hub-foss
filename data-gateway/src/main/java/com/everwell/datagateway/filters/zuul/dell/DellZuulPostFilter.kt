package com.everwell.datagateway.filters.zuul.dell
import com.everwell.datagateway.filters.zuul.base.BaseZuulPostFilter
import com.everwell.datagateway.service.BaseRestService
import com.everwell.datagateway.service.NikshayReportsRestService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants

open class DellZuulPostFilter(

) : BaseZuulPostFilter(){

    @Autowired
    lateinit var nikshayReportsRestService: NikshayReportsRestService

    override var thisURI: String
        get() = "/dell"
        set(value) {}

    override fun getRestService(): BaseRestService {
        return nikshayReportsRestService
    }

    override fun filterType(): String {
        return FilterConstants.POST_TYPE
    }

    override fun filterOrder(): Int {
        return FilterConstants.SEND_RESPONSE_FILTER_ORDER -2
    }
}
package com.everwell.datagateway.filters.zuul.iam

import com.everwell.datagateway.filters.zuul.base.BaseZuulRouteFilter
import com.everwell.datagateway.service.BaseRestService
import com.everwell.datagateway.service.IamRestService
import com.netflix.zuul.context.RequestContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants
import com.everwell.datagateway.constants.Constants.CLIENT_ID

open class IamZuulRouteFilter (

): BaseZuulRouteFilter() {

    @Autowired
    lateinit var iamRestService: IamRestService

    override var thisURI: String
        get() = "/iam"
        set(value) {}

    override fun getHeadersForAuth(context: RequestContext): MutableMap<String, String> {
        val map = mutableMapOf<String, String>()
        map["X-IAM-Access-Token"] = iamRestService.getAuthToken(context.request.getHeader(CLIENT_ID))!!
        map["X-IAM-Client-Id"] = context.request.getHeader(CLIENT_ID)
        return map
    }

    override fun getUrlMapping(context: RequestContext): String? {
        return context.request.requestURI.substring(thisURI.length)
    }

    override fun getRestService(): BaseRestService {
        return iamRestService
    }

    override fun filterType(): String {
        return FilterConstants.ROUTE_TYPE
    }

    override fun filterOrder(): Int {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER + 10
    }
}
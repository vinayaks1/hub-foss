package com.everwell.datagateway.filters.zuul.dell
import com.everwell.datagateway.filters.zuul.base.BaseZuulRouteFilter
import com.everwell.datagateway.service.BaseRestService
import com.everwell.datagateway.service.NikshayReportsRestService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants

open class DellZuulRouteFilter (

): BaseZuulRouteFilter() {


    @Autowired
    lateinit var nikshayReportsRestService: NikshayReportsRestService

    override var thisURI: String
        get() = "/dell"
        set(value) {}

    init {
        /// keep adding the url mapping here as Soch know which API to be called
        urlMapping.put("/dell/mstrState","/nikshayapiv2/api/masters/mstrState")
        urlMapping.put("/dell/mstrDistrict","/nikshayapiv2/api/masters/mstrDistrict")
        urlMapping.put("/dell/mstrDmcs","/nikshayapiv2/api/masters/mstrDmcs")
        urlMapping.put("/dell/mstrTbunits","/nikshayapiv2/api/masters/mstrTbunits")
    }

    override fun getRestService(): BaseRestService {
        return nikshayReportsRestService
    }

    override fun filterType(): String {
        return FilterConstants.ROUTE_TYPE
    }

    override fun filterOrder(): Int {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER +4
    }
}
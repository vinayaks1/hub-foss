package com.everwell.datagateway.filters.zuul.mermConfig

import com.everwell.datagateway.constants.Constants
import com.everwell.datagateway.enums.LegacyRoutesEnum
import com.everwell.datagateway.filters.zuul.httpServlet.GenericHttpServletRequest
import com.everwell.datagateway.utils.Utils
import com.netflix.zuul.ZuulFilter
import com.netflix.zuul.context.RequestContext
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType

class MermConfigZuulPreFilter() : ZuulFilter() {
    override fun shouldFilter(): Boolean {
        val request = RequestContext.getCurrentContext().request
        return (request.requestURI.contains(LegacyRoutesEnum.GET_MERM_CONFIG.path, true) && request.method.equals(
            HttpMethod.POST.name
        ))
    }

    override fun filterType(): String {
        return FilterConstants.PRE_TYPE
    }

    override fun filterOrder(): Int {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER
    }

    override fun run(): Any? {
        val ctx: RequestContext = RequestContext.getCurrentContext()
        convertToGet(Constants.BLANK_STRING, ctx)
        return null
    }

    //Method to convert given request to GET
    private fun convertToGet(input: Any, ctx: RequestContext) {
        val requestData = Utils.asJsonString(input)
        val request = GenericHttpServletRequest(
            ctx.request,
            requestData.toByteArray(),
            HttpMethod.GET.name,
            MediaType.APPLICATION_JSON_VALUE
        )
        ctx.setRequest(request)
    }
}
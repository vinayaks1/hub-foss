package com.everwell.datagateway.filters.zuul.hub

import com.everwell.datagateway.filters.zuul.GenericZuulRouteFilter
import com.everwell.datagateway.filters.zuul.base.BaseZuulRouteFilter
import com.everwell.datagateway.service.BaseRestService
import com.everwell.datagateway.service.HubRestService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants

open class HubZuulRouteFilter(

) : BaseZuulRouteFilter() {
    @Autowired
    lateinit var hubRestService: HubRestService

    override var thisURI: String
        get() = "/hub"
        set(value) {}

    init {
        urlMapping.put("/hub/missedAdherence","/api/Patients/CurrentMonthMissedAdherence")
        urlMapping.put("/hub/GetPatientDetails","/api/Patients/Get")
    }

    override fun getRestService(): BaseRestService {
        return hubRestService
    }

    override fun filterType(): String {
        return FilterConstants.ROUTE_TYPE
    }

    override fun filterOrder(): Int {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER +2
    }

}
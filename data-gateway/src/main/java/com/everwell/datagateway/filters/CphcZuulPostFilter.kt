package com.everwell.datagateway.filters

import com.everwell.datagateway.filters.zuul.base.BaseZuulPostFilter
import com.everwell.datagateway.service.BaseRestService
import com.everwell.datagateway.service.NikshayRestService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants

open class CphcZuulPostFilter(

) : BaseZuulPostFilter() {

    @Autowired
    lateinit var nikshayRestService: NikshayRestService

    override var thisURI: String
        get() = "/cphc"
        set(value) {}

    override fun getRestService(): BaseRestService {
        return nikshayRestService
    }

    override fun filterType(): String {
        return FilterConstants.POST_TYPE
    }

    override fun filterOrder(): Int {
        return FilterConstants.SEND_RESPONSE_FILTER_ORDER - 6
    }
}
package com.everwell.datagateway.filters.zuul.nikshay

import com.everwell.datagateway.constants.Constants
import com.everwell.datagateway.constants.Constants.LIMS_CLIENT
import com.everwell.datagateway.filters.zuul.GenericZuulRouteFilter
import com.everwell.datagateway.filters.zuul.base.BaseZuulRouteFilter
import com.everwell.datagateway.service.BaseRestService
import com.everwell.datagateway.service.NikshayRestService
import com.netflix.zuul.context.RequestContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants

open class NikshayZuulRouteFilter() :  BaseZuulRouteFilter(){

    @Autowired
    lateinit var nikshayRestService: NikshayRestService

    override var thisURI: String
        get() = "/nikshay"
        set(value) {}

    override fun getHeadersForAuth(context: RequestContext): MutableMap<String, String> {
        val map = mutableMapOf<String, String>()
        if(LIMS_CLIENT == context.request.getHeader(Constants.CLIENT_ID)){
            map["Authorization"] = "Bearer " + nikshayRestService.getAuthTokenForLims(LIMS_CLIENT)!!
        }
        else {
            map["Authorization"] = "Bearer " + nikshayRestService.getAuthToken(null)!!
        }
        return map
    }

    override fun getUrlMapping(context: RequestContext): String? {
        return context.request.requestURI.substring(thisURI.length)
    }

    override fun getRestService(): BaseRestService {
        return nikshayRestService
    }

    override fun filterType(): String {
        return FilterConstants.ROUTE_TYPE
    }

    override fun filterOrder(): Int {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER +2
    }
}
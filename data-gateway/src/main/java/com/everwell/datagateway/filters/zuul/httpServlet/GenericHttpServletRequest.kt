package com.everwell.datagateway.filters.zuul.httpServlet

import com.netflix.zuul.http.HttpServletRequestWrapper
import com.netflix.zuul.http.ServletInputStreamWrapper
import javax.servlet.ServletInputStream
import javax.servlet.http.HttpServletRequest

class GenericHttpServletRequest(request: HttpServletRequest, private val input: ByteArray,
                                private val methodName: String, private val contentType: String)
    : HttpServletRequestWrapper(request) {

    override fun getInputStream(): ServletInputStream {
        return ServletInputStreamWrapper(input);
    }
    override fun getContentLength(): Int {
        return input.size
    }

    override fun getContentLengthLong(): Long {
        return input.size.toLong()
    }

    override fun getMethod(): String {
        return methodName
    }

    override fun getContentType(): String {
        return contentType
    }
}
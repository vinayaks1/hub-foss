package com.everwell.datagateway.constants;

public class AfricaTalkingConstants {
    public static final String DEPLOYMENT_CODE = "deploymentCode";
    public static final String LINK_ID = "linkId";
    public static final String TEXT = "text";
    public static final String TO = "to";
    public static final String FROM = "from";
    public static final String ID = "id";
    public static final String DATE = "date";
    public static final String CALL_LOGS_RE_ROUTE_URI = "/v1/africa-talking-ascent-call-back";
}

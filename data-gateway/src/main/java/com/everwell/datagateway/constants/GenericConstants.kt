package com.everwell.datagateway.constants

object GenericConstants {
    const val auth_header = "Authorization"
    const val auth_type = "Bearer "
}
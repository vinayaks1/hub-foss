package com.everwell.datagateway.constants;

public class QueueConstants {
    public static final String PATIENT_ENROLLMENT_GRAMENER_QUEUE = "patient_enrollment_gramener";
    public static final String REF_ID_REPLY_QUEUE = "q.dg.ref_id_reply_queue";
    public static final String SHARE_DISPENSATION_QUEUE = "q.disp.share_dispensation_datagateway";
    public static String DLX_EXCHANGE = "ex.dg.dlx";
    public static String WAIT_EXCHANGE = "ex.dg.wait";
    public static String HIGH_SEV_WAIT_EXCHANGE = "ex.dg.wait.high.sev";
    public static String RETRY_COUNT_HEADER = "x-retry-count";
    public static String HIGH_SEV_HEADER = "x-high-sev";
    public static int RMQ_MAX_RETRY_COUNT = 3;
    public static final String DLX_QUEUE = "q.dg.dlq";
    public static final String SHARE_MERM_EVENT = "q.dg.share_merm_event";
    public static final String ABDM_ADD_UPDATE_CONSENT_QUEUE = "abdm_add_update_consent";
    public static final String DATA_EXCHANGE_QUEUE = "q.dg.abdm_data_exchange_gateway";
    public static final String ABDM_LINK_CARE_CONTEXT_QUEUE = "abdm_link_care_context";
    public static final String ABDM_CARE_CONTEXT_NOTIFY_QUEUE = "abdm_care_context_notify";
    public static final String ABDM_NOTIFY_VIA_SMS_QUEUE = "abdm_notify_via_sms";
    public static final String ABDM_HIU_QUEUE = "q.dg.initiate_consent_request";
    public static final String UPDATE_CONSENT_REQUEST_ROUTING_KEY = "abdm-hiu-consent";
    public static final String ECBSS_ADHERENCE_DATA_QUEUE = "q.dg.ecbss_adherence_data";
}
package com.everwell.datagateway.constants;

public class AdherenceConstants {
    public static final String ENTITY_ID = "entityId";
    public static final String EXTERNAL_ID_1 = "externalId1";
    public static final String EXTERNAL_ID_2 = "externalId2";
    public static final String ORG_UNIT_VALUE = "orgUnitValue";
    public static final String TOTAL_DOSES = "totalDoses";
    public static final String NO_INFO_DOSES = "noInfoDoses";
    public static final String DIGITALLY_CONFIRMED_DOSES = "digitallyConfirmedDoses";
    public static final String MANUALLY_CONFIRMED_DOSES = "manuallyConfirmedDoses";
    public static final String MANUALLY_MISSED_DOSES = "manuallyMissedDoses";
}

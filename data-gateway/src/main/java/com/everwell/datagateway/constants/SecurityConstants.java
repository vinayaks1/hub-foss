package com.everwell.datagateway.constants;

public class SecurityConstants {
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/register";
    public static final String GENERATE_TOKEN_URL="/token";
    public static final long EXPIRATION_TIME = 864_000_000;
    public static final String SECRET = "EverwellSecretJWTs";
    public static final String TEST_CONNECTION_URL="/test-connection";
    public static final String ACTUATOR_URL="/actuator/health";
    public static final String ACTUATOR_PROMETHEUS="/actuator/prometheus";
    public static final String[] AUTH_WHITELIST = {
            // -- swagger ui
            "/v2/api-docs",
            "/configuration/ui",
            "/swagger-resources/**",
            "/configuration/security",
            "/swagger-ui.html",
            "/webjars/**"
    };

}

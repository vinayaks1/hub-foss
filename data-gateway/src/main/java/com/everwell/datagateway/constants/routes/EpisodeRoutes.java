package com.everwell.datagateway.constants.routes;

import lombok.AllArgsConstructor;
import lombok.Getter;
    @Getter
    @AllArgsConstructor
    public enum EpisodeRoutes {
        EPISODE_SEARCH("/v2/episode/search"),
        CREATE_EPISODE("/v1/episode"),
        DEFAULT_DISEASE("/v1/disease/default"),
        UPDATE_EPISODE("/v1/episode"),
        DELETE_EPISODE("/v1/episode/delete");

        public final String path;
    }

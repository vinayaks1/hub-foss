package com.everwell.datagateway.repositories;

import com.everwell.datagateway.entities.EventClient;
import com.everwell.datagateway.entities.SubscriberUrl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubscriberUrlRepository extends JpaRepository<SubscriberUrl, Long> {


    SubscriberUrl findByEventClient(EventClient eventClient);
    List<SubscriberUrl> findByUrl(String url);
}

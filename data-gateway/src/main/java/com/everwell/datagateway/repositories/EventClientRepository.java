package com.everwell.datagateway.repositories;

import com.everwell.datagateway.entities.EventClient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EventClientRepository extends JpaRepository<EventClient, Long> {

    EventClient findByClientIdAndEventId(Long clientId, Long eventId);

    EventClient findByClientIdAndEventIdAndActiveTrue(Long clientId, Long eventId);
}

package com.everwell.datagateway.repositories;

import com.everwell.datagateway.entities.ClientAuthentication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientAuthenticationRepository extends JpaRepository<ClientAuthentication, Long> {
    List<ClientAuthentication> findAllByTypeIdIn(List<Long> typeIdList);
}

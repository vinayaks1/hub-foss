package com.everwell.datagateway.repositories;

import com.everwell.datagateway.entities.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {

    @Nullable
    Client findByUsername(String userName);

    @Nullable
    Client findByIp(String ip);


    @Override
    Optional<Client> findById(Long aLong);
}

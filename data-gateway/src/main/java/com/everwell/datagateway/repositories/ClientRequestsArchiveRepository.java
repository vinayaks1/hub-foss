package com.everwell.datagateway.repositories;

import com.everwell.datagateway.entities.ClientRequestsArchive;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRequestsArchiveRepository extends JpaRepository<ClientRequestsArchive, Long> {
}

package com.everwell.datagateway.repositories;

import com.everwell.datagateway.entities.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {

    Optional<Event> findByEventName(String eventName);

    Event findTopById(Long id);
}

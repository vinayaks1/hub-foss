package com.everwell.datagateway.models.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EncryptionRequest {
    private String receiverPublicKey;
    private String receiverNonce;
    private String senderPrivateKey;

    private String senderPublicKey;
    private String senderNonce;
    private String plainTextData;

    public EncryptionRequest(String receiverPublicKey, String receiverNonce,
                             KeyMaterial keyMaterial, String plainTextData) {
        this.receiverPublicKey = receiverPublicKey;
        this.receiverNonce = receiverNonce;
        this.senderPrivateKey = keyMaterial.getPrivateKey();
        this.senderPublicKey = keyMaterial.getPublicKey();
        this.senderNonce = keyMaterial.getNonce();
        this.plainTextData = plainTextData;
    }
}

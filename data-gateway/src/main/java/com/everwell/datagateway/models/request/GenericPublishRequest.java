package com.everwell.datagateway.models.request;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@Data
public class GenericPublishRequest {
    @NotNull
    String eventName;
    @NotNull
    String data;
}

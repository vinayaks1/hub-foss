package com.everwell.datagateway.models.response

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class GenericAuthResponse(
        @JsonProperty("access_token")
        var access_token:String,
        @JsonProperty("expires_in")
        var expires_in:Long?
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class MicroServiceGenericAuthResponse(
        @JsonProperty("id")
        val id: Long?,
        @JsonProperty("name")
        val name: String?,
        @JsonProperty("authToken")
        val authToken: String,
        @JsonProperty("nextRefresh")
        val nextRefresh: Long
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class UserServiceSearchResponse(
        @JsonProperty("id")
        val id: Int
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class GetEpisodeByPersonResponse(
        @JsonProperty("id")
        val id: Int,
        @JsonProperty("createdDate")
        val createdDate: String
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class ApiResponse<T> (
        @JsonProperty("success")
        val success: String?,
        @JsonProperty("data")
        val data: T?,
        @JsonProperty("message")
        val message: String?
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class LinkInitiationOtpResponse(
        @JsonProperty("Otp")
        val otp: String,
        @JsonProperty("Message")
        val message: String
)
package com.everwell.datagateway.models.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DiseaseTemplate {
    private Long id;
    private String diseaseName;
    private Long clientId;
    private Boolean isDefault;
    private String displayName;
    private LocalDateTime createdOn;
    private LocalDateTime updatedOn;
}

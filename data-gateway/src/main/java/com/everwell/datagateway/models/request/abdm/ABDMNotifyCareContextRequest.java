package com.everwell.datagateway.models.request.abdm;

import lombok.Getter;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.everwell.datagateway.constants.Constants.ABDM_TIMESTAMP_FORMAT;

@Getter
public class ABDMNotifyCareContextRequest {
    public String requestId;
    public String timestamp;
    public Notification notification;

    public ABDMNotifyCareContextRequest(ABDMNotifyCareContextEvent eventDTO, String hipName)
    {
        DateTimeFormatter formatter =  DateTimeFormatter.ofPattern(ABDM_TIMESTAMP_FORMAT);
        LocalDateTime utcNow =  LocalDateTime.now(ZoneOffset.UTC);
        this.requestId = UUID.randomUUID().toString();
        this.timestamp = formatter.format(utcNow);
        this.notification = new Notification(eventDTO, timestamp,hipName);
    }

    public static class Notification {

        public Patient patient;
        public CareContext careContext;
        public List<String> hiTypes = null;
        public String date;
        public Hip hip;

        public Notification(ABDMNotifyCareContextEvent eventDTO, String timestamp, String hipName) {
            this.date = timestamp;
            this.patient = new Patient(eventDTO.abhaAddress);
            this.hiTypes = new ArrayList<String>();
            hiTypes.add(eventDTO.hiTypes);
            this.hip = new Hip(hipName);
            this.careContext = new CareContext(eventDTO.personId,eventDTO.episodeId);
        }

        public static class CareContext {

            public String patientReference;
            public String careContextReference;

            public CareContext(long personId, long episodeId) {
                this.patientReference = Long.toString(personId);
                this.careContextReference = Long.toString(episodeId);
            }
        }

        public static class Hip {

            public String id;

            public Hip(String hipName) {
                this.id = hipName;
            }
        }

        public static class Patient {

            public String id;

            public Patient(String abhaAddress) {
                this.id = abhaAddress;
            }
        }

    }

}

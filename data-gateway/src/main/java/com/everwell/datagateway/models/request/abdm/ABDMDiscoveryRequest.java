package com.everwell.datagateway.models.request.abdm;

import lombok.Data;

import java.util.List;

@Data
public class ABDMDiscoveryRequest
{

    private Patient patient;
    private String requestId;
    private String timestamp;
    private String transactionId;

    @Data
    public static class Patient
    {
        public String id;
        public String name;
        public String gender;
        public String yearOfBirth;
        public List<ABDMIdentifiers> verifiedIdentifiers = null;
        public List<ABDMIdentifiers> unverifiedIdentifiers = null;
    }
}

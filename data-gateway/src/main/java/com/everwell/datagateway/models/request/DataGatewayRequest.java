package com.everwell.datagateway.models.request;


import com.everwell.datagateway.enums.StatusCodeEnum;

import java.io.Serializable;

//Making an interface request to standardize validations as a part of requests
public interface DataGatewayRequest extends Serializable {//Since it is a serializable, rabbittemplate can easily convert and send this

    StatusCodeEnum isValid();

}

package com.everwell.datagateway.models.dto;

import com.everwell.datagateway.entities.Client;
import com.everwell.datagateway.entities.Event;
import com.everwell.datagateway.entities.SubscriberUrl;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OutgoingWebhookLogDTO {
    private Long subscriberUrlId;
    private boolean resultDelivered;
    private String data;
    private Long eventId;
    private Long clientId;
    private Date deliveredAt;
    private Integer callbackResponseCode;
    private String response;

    public OutgoingWebhookLogDTO(String data) {
        this.data = data;
    }

    public OutgoingWebhookLogDTO(Long subscriberUrlId, Long eventId, Long clientId, Integer callbackResponseCode, Date deliveredAt, String response) {
        this.subscriberUrlId = subscriberUrlId;
        this.eventId = eventId;
        this.clientId = clientId;
        this.callbackResponseCode = callbackResponseCode;
        this.deliveredAt = deliveredAt;
        this.response = response;
    }
}

package com.everwell.datagateway.models.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class TestReplyQueueResponse {
    @JsonProperty("RefId")
    private Long refId;
    @JsonProperty("Username")
    private String username;
    @JsonProperty("Response")
    private String response;
    @JsonProperty("Success")
    private String success;
}

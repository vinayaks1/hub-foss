package com.everwell.datagateway.models.dto;

import com.everwell.datagateway.models.request.abdm.ABDMOnDiscoveryRequest;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class LinkConfirmRedisDto {
    ABDMOnDiscoveryRequest abdmOnDiscoveryRequest;
    String otp;
    String abhaAddress;
}

package com.everwell.datagateway.models.request.abdm;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ABDMLinkCareContextEvent implements Serializable {
    public long episodeId;
    public long personId;
    public String hiTypes;
    public String personName;
    public String enrollmentDate;
    public String identifierValue;
    public char gender;
    public String identifierType;
    public String abhaAddress;
    public String yearOfBirth;
}

package com.everwell.datagateway.models.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Getter
@Setter
@NoArgsConstructor
public class MessageApiResponse <T> implements Serializable {
    private String code;
    private String message;
    private T data;

    public MessageApiResponse(String message) {
        this.message = message;
    }

    public MessageApiResponse(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public MessageApiResponse(String code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data=data;
    }

    public static MessageApiResponse buildFailureResponse(String errorcode, String errorMessage) {
        MessageApiResponse messageApiResponse=new MessageApiResponse();
        messageApiResponse.code=errorcode;
        messageApiResponse.message=errorMessage;
        return messageApiResponse;
    }
}
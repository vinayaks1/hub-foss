package com.everwell.datagateway.models.request;

import com.everwell.datagateway.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Sort;

import java.util.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EpisodeSearchRequest {
    private Integer size;
    private Integer page;
    private List<String> fieldsToShow;
    private List<String> typeOfEpisode;
    String sortKey = "id";
    Sort.Direction sortDirection = Sort.Direction.ASC;
    SupportedAggMethods agg;
    SupportedSearchMethods search;
    String requestIdentifier;
    boolean count;
    Map<String, Object> range;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class SupportedAggMethods {
        List<AggregationDto> terms;
        List<AggregationDto> date;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class SupportedSearchMethods {
        Map<String, Object> must;
        Map<String, Object> mustNot;
        Map<String, Object> contains;
    }

    public EpisodeSearchRequest(int size, int page, List<String> fieldsToShow, List<String> typeOfEpisode) {
        this.size = size;
        this.page = page;
        this.fieldsToShow = fieldsToShow;
        this.typeOfEpisode = typeOfEpisode;
        this.range = new HashMap<>();
    }

    public EpisodeSearchRequest(SupportedSearchMethods search) {
        this.search = search;
    }

}

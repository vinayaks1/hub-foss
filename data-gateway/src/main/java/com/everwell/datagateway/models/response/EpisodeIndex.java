package com.everwell.datagateway.models.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EpisodeIndex {

    private String id;

    private Long clientId;

    private Long personId;

    private Boolean deleted;

    private Long diseaseId;

    private String diseaseIdOptions;

    private String createdDate;

    private String riskStatus;

    private Long currentStageId;

    private Long addedBy;

    private String stageKey;

    private String stageName;

    private String typeOfEpisode;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime startDate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime endDate;

    private String lastActivityDate;

    Map<String, Object> hierarchyMappings;

    Map<String, Object> associations;

    Map<String, Object> stageData;

    List<String> currentTags;

    private String externalId1;

    private String timeZone;

    public EpisodeIndex(String id) {
        this.id = id;
    }

    public EpisodeIndex(String id, LocalDateTime startDate, LocalDateTime endDate, Map<String, Object> stageData, Map<String, Object> hierarchyMappings) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.stageData = stageData;
        this.hierarchyMappings = hierarchyMappings;
    }

    public EpisodeIndex(String id, Map<String, Object> stageData, Map<String, Object> hierarchyMappings) {
        this.id = id;
        this.stageData = stageData;
        this.hierarchyMappings = hierarchyMappings;
    }

}


package com.everwell.datagateway.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DiagnosticsFHIRDto {

    @JsonProperty("EpisodeId")
    public int episodeId;

    @JsonProperty("HierarchyId")
    public int hierarchyId;

    @JsonProperty("UserId")
    public int userId;

    @JsonProperty("HierarchyName")
    public String hierarchyName;

    @JsonProperty("TestResultMap")
    public Map<String,TestDetails> testResultMap;

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    static
    public class TestDetails
    {
        @JsonProperty("TestType")
        public String testType;

        @JsonProperty("FinalInterpretation")
        public String finalInterpretation;
    }
}

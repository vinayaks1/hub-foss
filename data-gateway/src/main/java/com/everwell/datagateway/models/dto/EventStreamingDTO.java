package com.everwell.datagateway.models.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EventStreamingDTO {
    public String EventName;
    public Object Field;
}

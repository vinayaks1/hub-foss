package com.everwell.datagateway.models.request.abdm;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Data
public class ABDMConsentInitiationRequest {

    public String requestId;
    public String timestamp;
    public Consent consent;

    @Getter
    @Setter
    public static class Consent{
        public Purpose purpose;
        public Patient patient;
        public Hiu hiu;
        public Requester requester;
        public List<String> hiTypes;
        public Permission permission;
    }

    @Getter
    @Setter
    public static class DateRange{
        public String from;
        public String to;
    }

    @Getter
    @Setter
    public static class Frequency{
        public String unit;
        public int value;
        public int repeats;
    }

    @Getter
    @Setter
    public static class Hiu{
        public String id;
    }

    @Getter
    @Setter
    public static class Identifier{
        public String type;
        public String value;
        public String system;
    }

    @Getter
    @Setter
    public static class Patient{
        public String id;
    }

    @Getter
    @Setter
    public static class Permission{
        public String accessMode;
        public DateRange dateRange;
        public String dataEraseAt;
        public Frequency frequency;
    }

    @Getter
    @Setter
    public static class Purpose{
        public String text;
        public String code;
        public String refUri;
    }

    @Getter
    @Setter
    public static class Requester{
        public String name;
        public Identifier identifier;
    }

}

package com.everwell.datagateway.models.request.abdm;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Getter
@Setter
@Data
public class ABDMLinkInitiationRequest {
    public String requestId;
    public String timestamp;
    public String transactionId;
    public Patient patient;

    @Getter
    @Setter
    public static class CareContext{
        public String referenceNumber;
    }

    @Getter
    @Setter
    public static class Patient{
        public String id;
        public String referenceNumber;
        public List<CareContext> careContexts;
    }
}

package com.everwell.datagateway.models.response

import com.fasterxml.jackson.annotation.JsonProperty
import lombok.AllArgsConstructor
import lombok.Data
import lombok.NoArgsConstructor

@Data
@NoArgsConstructor
class MermConfigurationResponse
{
    @JsonProperty("IDString")
    var idString = ""

    @JsonProperty("MedAlarmTime")
    var medAlarmTime = ""

    @JsonProperty("RefillDate")
    var refillDate = ""

    @JsonProperty("MedAlarmEn")
    var medAlarmEnabled = "0"

    @JsonProperty("RefillAlarmEn")
    var refillAlarmEnabled = "0"

    @JsonProperty("RTHours")
    var rtHours = "1"

    @JsonProperty("TimeZoneOffset")
    var timeZoneOffset = ""

    @JsonProperty("RepeatDays")
    var repeatDays = ""

    @JsonProperty("LidFeedback")
    var lidFeedback = "1"

    @JsonProperty("NTPServer")
    var ntpServer = "134744072"

    constructor() {}

    constructor(mermConfigurationResponse: MermConfigurationResponse) {
        // In case any field is null use its default value
        this.idString = mermConfigurationResponse.idString ?: ""
        this.medAlarmTime = mermConfigurationResponse.medAlarmTime ?: ""
        this.refillDate = mermConfigurationResponse.refillDate ?: ""
        this.medAlarmEnabled = mermConfigurationResponse.medAlarmEnabled ?: "0"
        this.refillAlarmEnabled = mermConfigurationResponse.refillAlarmEnabled ?: "0"
        this.rtHours = mermConfigurationResponse.rtHours ?: "1"
        this.timeZoneOffset = mermConfigurationResponse.timeZoneOffset ?: ""
        this.repeatDays = mermConfigurationResponse.repeatDays ?: ""
        this.lidFeedback = mermConfigurationResponse.lidFeedback ?: "1"
        this.ntpServer = mermConfigurationResponse.ntpServer ?: "134744072"
    }
}


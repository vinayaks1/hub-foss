package com.everwell.datagateway.models.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RestServiceResponse {
    public Integer code;
    public String message;
    public String data;


    public RestServiceResponse(String message) {
        this.message = message;
    }

    public RestServiceResponse(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}

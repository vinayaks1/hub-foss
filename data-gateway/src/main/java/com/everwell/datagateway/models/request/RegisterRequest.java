package com.everwell.datagateway.models.request;

import com.everwell.datagateway.enums.AuthenticationCredentialTypeEnum;
import com.everwell.datagateway.exceptions.ValidationException;
import com.everwell.datagateway.models.dto.BasicAuthCredDto;
import com.everwell.datagateway.models.dto.ClientCredentialBase;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.NoClass;
import lombok.*;
import org.springframework.util.StringUtils;

import javax.validation.constraints.NotBlank;
import java.util.Arrays;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RegisterRequest {
    @NotBlank
    private String username;
    @NotBlank
    private String password;
    @NotBlank
    private String failureEmail;

    private String credentialType;

    private String ip;
    private Long accessibleClient;

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "credentialType", include = JsonTypeInfo.As.EXTERNAL_PROPERTY, defaultImpl = NoClass.class)
    @JsonSubTypes(value = {
            @JsonSubTypes.Type(value = BasicAuthCredDto.class, name = "BASIC_AUTH")
    })
    private ClientCredentialBase credentials;
    public RegisterRequest(String username, String password, String failureEmail) {
        this.username = username;
        this.password = password;
        this.failureEmail = failureEmail;
    }

    public void validate(){
        validateAuthenticationDetails(credentialType, credentials);
    }

    public static void validateAuthenticationDetails(String credentialType, ClientCredentialBase credentials) {
        if (!StringUtils.isEmpty(credentialType) && Arrays.stream(AuthenticationCredentialTypeEnum.values())
                .noneMatch(type -> credentialType.equalsIgnoreCase(type.toString()))){
            throw new ValidationException("CredentialType value can be only from "+ Arrays.toString(AuthenticationCredentialTypeEnum.values()));
        }
    }
}

package com.everwell.datagateway.models.response;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class Response<T> implements Serializable {

    @Getter
    @JsonProperty("Success")
    private boolean success;


    @Getter
    @JsonProperty("Data")
    private T data;


    public Response(boolean success, T data) {
        this.success = success;
        this.data = data;
    }
}

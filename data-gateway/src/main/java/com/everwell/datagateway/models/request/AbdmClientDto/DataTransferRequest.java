package com.everwell.datagateway.models.request.AbdmClientDto;

import com.everwell.datagateway.models.request.AbdmClientDto.DataPullRequest.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

import static com.everwell.datagateway.constants.Constants.FHIR_CHECKSUM_TYPE;
import static com.everwell.datagateway.constants.Constants.FHIR_MEDIA_TYPE;

@Getter
@Setter
@NoArgsConstructor
public class DataTransferRequest {
    public int pageNumber;
    public int pageCount;
    public String transactionId;
    public List<Entry> entries;
    public KeyMaterial keyMaterial;
    public DataTransferRequest(String publicKey, String nonce, KeyMaterial keyMaterial, String transactionId, List<Entry> entries) {
        this.keyMaterial = keyMaterial;
        this.keyMaterial.dhPublicKey.keyValue = publicKey;
        this.keyMaterial.nonce = nonce;
        this.transactionId = transactionId;
        this.entries = entries;
    }
    @Getter
    @Setter
    public static class Entry{
        public String content;
        public String media;
        public String checksum;
        public String careContextReference;
        public Entry(String content,String careContextReference) {
            this.content = content;
            this.careContextReference = careContextReference;
            this.media = FHIR_MEDIA_TYPE ;
            this.checksum = FHIR_CHECKSUM_TYPE;
        }
    }
}

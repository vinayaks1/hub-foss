package com.everwell.datagateway.models.request.abdm;

import lombok.Getter;
import lombok.Setter;

@Getter
public class ABDMAuthModesQuery {
    public String id;
    public String purpose;
    public ABDMAuthModesRequester requester;

    public ABDMAuthModesQuery(String abhaAddress)
    {
        this.id = abhaAddress;
        this.purpose = "LINK";
        this.requester =  new ABDMAuthModesRequester();
    }
}

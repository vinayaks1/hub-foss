package com.everwell.datagateway.models.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EncryptionResponse {
    private String encryptedData;
    private String keyToShare;


    public EncryptionResponse(String encryptedData, String keyToShare) {
        this.encryptedData = encryptedData;
        this.keyToShare = keyToShare;
    }
}

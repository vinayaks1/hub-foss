package com.everwell.datagateway.models.request.abdm;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ABDMNotifyViaSMSRequest {
    public String requestId;
    public String timestamp;
    public ABDMNotifyViaSMSNotificationDto notification;

    @Getter
    @Setter
    public static class ABDMNotifyViaSMSNotificationDto {
        public String phoneNo;
        public ABDMNotifyViaSMSHipDto hip;

        @Getter
        @Setter
        public static class ABDMNotifyViaSMSHipDto {
            public String name;
            public String id;
        }
    }
}

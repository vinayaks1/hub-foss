package com.everwell.datagateway.models.request

import com.netflix.ribbon.proxy.annotation.Http
import org.springframework.core.ParameterizedTypeReference

 class CustomHttpRequest<T>(
        val authUrl: String,
        val headers: MutableMap<String, String>,
        val body: EpisodeSearchRequest,
        val httpMethod: Http.HttpMethod,
        val responseType: ParameterizedTypeReference<T>
)
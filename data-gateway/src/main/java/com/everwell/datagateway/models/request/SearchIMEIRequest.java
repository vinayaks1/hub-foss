package com.everwell.datagateway.models.request;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SearchIMEIRequest {

    private String imei;

    private String languageCode;

    private Long userId;

    private Long patientId;

    private Long hierarchyId;

    private String allocationDate;

    private boolean active;

    private List<Long> hierarchyIds;

    private String searchBy;

    public SearchIMEIRequest(String imei, Long patientId, String searchBy) {
        this.imei = imei;
        this.patientId = patientId;
        this.searchBy = searchBy;
    }
}


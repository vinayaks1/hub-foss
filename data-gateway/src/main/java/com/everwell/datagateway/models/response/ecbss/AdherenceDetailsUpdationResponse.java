package com.everwell.datagateway.models.response.ecbss;

import lombok.Data;

@Data
public class AdherenceDetailsUpdationResponse {
    public String httpStatus;
    public int httpStatusCode;
    public String status;
    public String message;
    public Object response;
}

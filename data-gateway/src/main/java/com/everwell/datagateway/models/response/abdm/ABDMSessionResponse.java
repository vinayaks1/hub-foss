package com.everwell.datagateway.models.response.abdm;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ABDMSessionResponse{
    public String accessToken;
    public String expiresIn;
    public String refreshExpiresIn;
    public String refreshToken;
    public String tokenType;
}

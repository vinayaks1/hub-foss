package com.everwell.datagateway.models.dto;

import com.everwell.datagateway.models.request.EpisodeSearchRequest;
import io.swagger.models.auth.In;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Sort;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GenericEpisodeSearchRequest {
    String id;
    boolean deleted;
    String lastName;
    String firstName;
    String dateOfBirth;
    String pincode;
    String gender;
    String address;
    Integer page;
    Integer pageSize;
    String sort;
    Sort.Direction sortDirection;

}

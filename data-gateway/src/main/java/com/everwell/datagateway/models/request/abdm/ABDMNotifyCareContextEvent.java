package com.everwell.datagateway.models.request.abdm;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
@Getter
@Setter
public class ABDMNotifyCareContextEvent implements Serializable {
    public long episodeId;
    public long personId;
    public String hiTypes;
    public String personName;
    public String abhaAddress;
}

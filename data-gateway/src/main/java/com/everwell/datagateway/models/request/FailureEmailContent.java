package com.everwell.datagateway.models.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FailureEmailContent {
    private String severity;
    private String httpPostResponse;
    private String clientName;
    private String eventName;
    private String queueMessage;

    public FailureEmailContent(String severity, String httpPostResponse,String clientName, String eventName, String queueMessage)
    {
        this.severity=severity;
        this.httpPostResponse=httpPostResponse;
        this.clientName= clientName;
        this.eventName=eventName;
        this.queueMessage=queueMessage;
    }
}

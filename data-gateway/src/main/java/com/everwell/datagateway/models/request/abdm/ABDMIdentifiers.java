package com.everwell.datagateway.models.request.abdm;

import lombok.Data;

@Data
public class ABDMIdentifiers {
    public String type;
    public String value;

    public ABDMIdentifiers(String identifierType, String identifierValue) {
        type = identifierType;
        value = identifierValue;
    }
}

package com.everwell.datagateway.models.request.abdm;

import lombok.Getter;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import static com.everwell.datagateway.constants.Constants.ABDM_TIMESTAMP_FORMAT;

@Getter
public class ABDMInitateAuthRequest {
    public String requestId;
    public String timestamp;
    public ABDMAuthModeInitQuery query;

    public ABDMInitateAuthRequest(String abhaAddress, String mode)
    {
        DateTimeFormatter formatter =  DateTimeFormatter.ofPattern(ABDM_TIMESTAMP_FORMAT);
        LocalDateTime utcNow =  LocalDateTime.now(ZoneOffset.UTC);
        this.requestId = UUID.randomUUID().toString();
        this.timestamp = formatter.format(utcNow);
        this.query =  new ABDMAuthModeInitQuery(abhaAddress, mode);
    }
}

package com.everwell.datagateway.models.dto;

import lombok.Data;

@Data
public class ABDMAcknowledgementDTO {
    public String requestId;
    public String timestamp;
    public Acknowledgement acknowledgement;
    public Object error;
    public Resp resp;

    @Data
    public static class Resp {
        public String requestId;
    }

    @Data
    public static class Acknowledgement {
        public String status;
    }
}
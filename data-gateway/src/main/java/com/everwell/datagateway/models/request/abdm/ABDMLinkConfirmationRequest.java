package com.everwell.datagateway.models.request.abdm;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Data
public class ABDMLinkConfirmationRequest {
    public String requestId;
    public String timestamp;
    public Confirmation confirmation;

    @Getter
    @Setter
    @AllArgsConstructor
    public static class Confirmation{
        public String linkRefNumber;
        public String token;
    }
}

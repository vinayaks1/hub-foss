package com.everwell.datagateway.models.request.abdm;
import lombok.Getter;

@Getter
public class ABDMAuthModeInitQuery {
    public String id;
    public String purpose;
    public String authMode;
    public ABDMAuthModesRequester requester;

    public ABDMAuthModeInitQuery(String abhaAddress, String mode)
    {
        this.id = abhaAddress;
        this.purpose = "LINK";
        this.authMode = mode;
        this.requester =  new ABDMAuthModesRequester();
    }
}

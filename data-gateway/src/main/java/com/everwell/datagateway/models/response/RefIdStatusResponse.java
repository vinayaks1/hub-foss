package com.everwell.datagateway.models.response;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RefIdStatusResponse {
    private Long referenceId;
    private String payload;
    private String status;
}

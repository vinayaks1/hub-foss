package com.everwell.datagateway.models.response.abdm;

import com.everwell.datagateway.models.response.abdm.OnAuthInit.ABDMOnAuthInitResponse;
import lombok.Data;

@Data
public class ABDMAuthOnConfirmResponse {
    public String requestId;
    public String timestamp;
    public ABDMAuthOnConfirmResponse.Auth auth;
    public Error error;
    public ABDMAuthOnConfirmResponse.Resp resp;

    @Data
    public static class Auth {
        public String accessToken;
        public ABDMAuthOnConfirmResponse.Auth.Patient patient;

        @Data
        public static class Patient {

        }
    }

    @Data
    public static class Resp {
        public String requestId;
    }
}

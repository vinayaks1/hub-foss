package com.everwell.datagateway.models.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AggregationDto {
    private Integer size;
    private String key;
    private String fieldName;
}

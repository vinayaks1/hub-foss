package com.everwell.datagateway.models.request.abdm;

import lombok.Getter;
import lombok.NoArgsConstructor;

import static com.everwell.datagateway.constants.Constants.X_HIP_ID_VALUE;

@Getter
@NoArgsConstructor
public class ABDMAuthModesRequester {
    public String type ="HIP";
    public String id = X_HIP_ID_VALUE;
}

package com.everwell.datagateway.models.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateUserRequest {

    public long UserId;

    public List<ExternalIdRequest> ExternalIds;

    @Data
    @AllArgsConstructor
    public static class ExternalIdRequest
    {
        public String type;
        public String value;
    }
}

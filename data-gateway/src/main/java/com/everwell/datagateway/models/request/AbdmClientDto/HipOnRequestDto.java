package com.everwell.datagateway.models.request.AbdmClientDto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import static com.everwell.datagateway.constants.Constants.ABDM_HI_REQUEST_SESSION_STATUS;
import static com.everwell.datagateway.constants.Constants.TIMESTAMP_FORMAT;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class HipOnRequestDto {

    public String requestId;
    public String timestamp;
    public HiRequest hiRequest;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public ABDMError error;
    public Resp resp;

    public HipOnRequestDto(String transactionId, String requestId, String errorMessage) {
        DateTimeFormatter formatter =  DateTimeFormatter.ofPattern(TIMESTAMP_FORMAT);
        LocalDateTime utcNow =  LocalDateTime.now(ZoneOffset.UTC);
        this.requestId = UUID.randomUUID().toString();
        this.timestamp = formatter.format(utcNow);
        this.hiRequest = new HiRequest(transactionId);
        this.resp = new Resp(requestId);
        if(errorMessage != null){
            this.error = new ABDMError(1000,errorMessage);
        }
    }

    @Getter
    @Setter
    public static class HiRequest{
        public String transactionId;
        public String sessionStatus;
        public HiRequest(String transactionId) {
            this.transactionId = transactionId;
            this.sessionStatus = ABDM_HI_REQUEST_SESSION_STATUS;
        }
    }

    @Getter
    @Setter
    public static class Resp{
        public String requestId;
        public Resp(String requestId) {
            this.requestId = requestId;
        }
    }

    @Getter
    @Setter
    public static class ABDMError {
        public int code;
        public String message;

        public ABDMError(int code, String message)
        {
            this.code = code;
            this.message = message;
        }
    }
}

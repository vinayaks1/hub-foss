package com.everwell.datagateway.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommonFHIRDto {
    @JsonProperty("ProfileType")
    public String profileType;
    @JsonProperty("Response")
    public String response;
    @JsonProperty("Success")
    public boolean success ;
}

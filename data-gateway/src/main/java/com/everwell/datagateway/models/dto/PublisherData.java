package com.everwell.datagateway.models.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class PublisherData {
    String clientName;
    Long clientId;
    Long referenceId;
    String data;
}

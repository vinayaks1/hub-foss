package com.everwell.datagateway.models.response.abdm;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Error {
    public int code;
    public String message;
}

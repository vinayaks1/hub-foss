package com.everwell.datagateway.models.response.abdm.OnAuthInit;

import com.everwell.datagateway.models.response.abdm.Error;
import lombok.Data;

@Data
public class ABDMOnAuthInitResponse{
    public String requestId;
    public String timestamp;
    public Auth auth;
    public Error error;
    public Resp resp;

    @Data
    public static class Auth {
        public String transactionId;
        public String mode;
        public Meta meta;

        @Data
        public static class Meta {

            public String hint;
            public String expiry;
        }
    }

    @Data
    public static class Resp {
        public String requestId;
    }
}

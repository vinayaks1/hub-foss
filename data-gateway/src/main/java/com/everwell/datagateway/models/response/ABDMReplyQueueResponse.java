package com.everwell.datagateway.models.response;

import com.everwell.datagateway.models.dto.CommonFHIRDto;
import com.everwell.datagateway.models.request.AbdmClientDto.DataPullRequest;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ABDMReplyQueueResponse {

    public DataPullRequest dataPullRequest;
    public Map<String, List<CommonFHIRDto>> episodeMap;
    public String errorMessage;
}

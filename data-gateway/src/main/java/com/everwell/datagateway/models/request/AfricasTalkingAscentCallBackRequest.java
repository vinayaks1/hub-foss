package com.everwell.datagateway.models.request;

import com.everwell.datagateway.constants.AfricaTalkingConstants;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AfricasTalkingAscentCallBackRequest {
    String deploymentCode;
    String linkId;
    String text;
    String to;
    String id;
    String date;
    String from;
    String body;

    public AfricasTalkingAscentCallBackRequest(Map<String, String> request) {
        this.deploymentCode = request.get(AfricaTalkingConstants.DEPLOYMENT_CODE);
        this.linkId = request.get(AfricaTalkingConstants.LINK_ID);
        this.text = request.get(AfricaTalkingConstants.TEXT);
        this.to = request.get(AfricaTalkingConstants.TO);
        this.id = request.get(AfricaTalkingConstants.ID);
        this.date = request.get(AfricaTalkingConstants.DATE);
        this.from = request.get(AfricaTalkingConstants.FROM);
        this.body = request.entrySet().stream()
                .map(entry -> entry.getKey() + "=" + entry.getValue())
                .collect(Collectors.joining("&"));
    }
}

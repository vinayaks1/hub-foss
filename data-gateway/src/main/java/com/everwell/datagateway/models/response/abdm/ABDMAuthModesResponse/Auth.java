package com.everwell.datagateway.models.response.abdm.ABDMAuthModesResponse;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class Auth {
    public String purpose;
    public List<String> modes;
}

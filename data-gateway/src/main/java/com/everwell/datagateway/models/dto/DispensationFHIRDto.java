package com.everwell.datagateway.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DispensationFHIRDto {
    @JsonProperty("EpisodeId")
    public int episodeId;

    @JsonProperty("HierarchyId")
    public int hierarchyId;

    @JsonProperty("UserId")
    public int userId;

    @JsonProperty("HierarchyName")
    public String hierarchyName;

    @JsonProperty("DispensationMap")
    public Map<String, DispensationDetails> dispensationMap;

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    static
    public class DispensationDetails
    {
        @JsonProperty("Active")
        public boolean active;
        @JsonProperty("ProductName")
        public String productName;
        @JsonProperty("TimeStamp")
        public String timeStamp;
    }
}

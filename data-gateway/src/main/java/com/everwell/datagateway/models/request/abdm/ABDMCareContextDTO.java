package com.everwell.datagateway.models.request.abdm;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Getter
public class ABDMCareContextDTO {

    private final String referenceNumber;
    private final String display;
    private List<CareContext> careContexts = null;

    public ABDMCareContextDTO(String personID, String episodeId, String name, String enrollmentDate) {
        this.referenceNumber = personID;
        this.display = name;
        this.careContexts = new ArrayList<>();
        careContexts.add(new CareContext(episodeId, enrollmentDate));
    }

    public ABDMCareContextDTO(String personID, String name, Map<String, String> episodeIdEnrollmentDateMap) {
        this.referenceNumber = personID;
        this.display = name;
        this.careContexts = new ArrayList<>();
        for (Map.Entry<String, String> item : episodeIdEnrollmentDateMap.entrySet()) {
            careContexts.add(new CareContext(item.getKey(), item.getValue()));
        }
    }

    @Getter
    public static class CareContext {

        private final String referenceNumber;
        private final String display;

        public CareContext(String episodeId, String enrollmentDate) {
            this.referenceNumber = episodeId;
            this.display = "Episode details for TB, registered on " + enrollmentDate;
        }
    }
}


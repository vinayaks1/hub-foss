package com.everwell.datagateway.models.request.abdm;

import com.everwell.datagateway.models.response.abdm.Error;
import com.everwell.datagateway.models.response.abdm.Response;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.everwell.datagateway.constants.Constants.ABDM_TIMESTAMP_FORMAT;

@Data
public class ABDMConsentOnNotifyRequest {
    public String requestId;
    public String timestamp;
    public List<Acknowledgement> acknowledgement;
    public Error error;
    public Response resp;

    public ABDMConsentOnNotifyRequest(String requestId,List<ABDMConsentNotification.ConsentArtefact> consentArtefacts){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(ABDM_TIMESTAMP_FORMAT);
        LocalDateTime utcNow = LocalDateTime.now(ZoneOffset.UTC);
        this.requestId = UUID.randomUUID().toString();
        this.timestamp = formatter.format(utcNow);
        this.acknowledgement = consentArtefacts.stream().map(ca -> new Acknowledgement("OK",ca.getId())).collect(Collectors.toList());
        this.resp = new Response(requestId);
    }

    @Data
    @AllArgsConstructor
    public static class Acknowledgement{
        public String status;
        public String consentId;
    }
}

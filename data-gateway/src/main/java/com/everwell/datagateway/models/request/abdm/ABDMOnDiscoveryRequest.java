package com.everwell.datagateway.models.request.abdm;

import com.everwell.datagateway.models.response.abdm.Error;
import com.everwell.datagateway.models.response.abdm.Response;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static com.everwell.datagateway.constants.Constants.ABDM_TIMESTAMP_FORMAT;

@Data
@NoArgsConstructor
@Getter
@Setter
public class ABDMOnDiscoveryRequest {

    private ABDMCareContextDTO patient;
    private String requestId;
    private String timestamp;
    private String transactionId;
    private List<String> matchedBy;
    private Error error;
    private Response resp;

    public ABDMOnDiscoveryRequest(String transactionID, String personID, String name, String requestId, Map<String, String> episodeIdEnrollmentDateMap) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(ABDM_TIMESTAMP_FORMAT);
        LocalDateTime utcNow = LocalDateTime.now(ZoneOffset.UTC);
        this.requestId = UUID.randomUUID().toString();
        this.timestamp = formatter.format(utcNow);
        this.transactionId = transactionID;
        this.resp = new Response(requestId);
        this.patient = new ABDMCareContextDTO(personID, name, episodeIdEnrollmentDateMap);
    }

    public ABDMOnDiscoveryRequest(String transactionID,String message, String requestId) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(ABDM_TIMESTAMP_FORMAT);
        LocalDateTime utcNow = LocalDateTime.now(ZoneOffset.UTC);
        this.requestId = UUID.randomUUID().toString();
        this.timestamp = formatter.format(utcNow);
        this.transactionId = transactionID;
        this.resp = new Response(requestId);
        this.error = new Error(1000,message);

    }
}


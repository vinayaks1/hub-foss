package com.everwell.datagateway.models.request.abdm;


import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class ABDMAuthNotifyRequest {
    public String requestId;
    public String timestamp;
    public Auth auth;

    @Getter
    @Setter
    public static class Auth{
        public String transactionId;
        public String status;
        public String accessToken;
        public Validity validity;
        public Object patient;
    }

    @Getter
    @Setter
    public static class Requester{
        public String type;
        public String id;
        public Object name;
    }

    @Getter
    @Setter
    public static class Validity{
        public String purpose;
        public Requester requester;
        public String expiry;
        public int limit;
    }
}

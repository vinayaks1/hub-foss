package com.everwell.datagateway.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "client_requests")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ClientRequests {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToOne
    private SubscriberUrl subscriberUrl;
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    private Boolean resultDelivered = false;
    @Column(columnDefinition = "TEXT")
    private String responseData;
    @OneToOne
    private Event event;
    @OneToOne
    private Client client;
    @Column(columnDefinition = "TEXT")
    private String requestData;
    private Date deliveredAt;
    private Date refIdDeliveredAt;
    private Integer callbackResponseCode;
    private String responsePayload;
}

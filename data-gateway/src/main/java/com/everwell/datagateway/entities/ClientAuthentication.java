package com.everwell.datagateway.entities;

import com.everwell.datagateway.enums.AuthenticationTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "client_authentication")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ClientAuthentication {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @Column(name = "credentials")
    private String credentials;

    @Column(name = "credential_type")
    private String credentialsType;

    @Column(name = "type")
    private AuthenticationTypeEnum type;

    @Column(name = "type_id")
    private Long typeId;

    public ClientAuthentication(String credentials, String credentialsType, AuthenticationTypeEnum type, Long typeId) {
        this.credentials = credentials;
        this.credentialsType = credentialsType;
        this.type = type;
        this.typeId = typeId;
    }

}
package com.everwell.datagateway.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "client_requests_archive")
@Data
@NoArgsConstructor
public class ClientRequestsArchive {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long clientRequestId;
    private Date createdAt;
    private Boolean resultDelivered;
    private Long clientId;
    private Long eventId;
    private Long subscriberUrlId;
    @Column(columnDefinition = "TEXT")
    private String responseData;
    @Column(columnDefinition = "TEXT")
    private String requestData;
    private Date deliveredAt;
    private Date refIdDeliveredAt;
    private Integer callbackResponseCode;

    public ClientRequestsArchive(Long clientRequestId, Date createdAt, Boolean resultDelivered, Long clientId, Long eventId, Long subscriberUrlId, String responseData,String requestData, Date deliveredAt, Date refIdDeliveredAt, Integer callbackResponseCode){
        this.clientRequestId = clientRequestId;
        this.createdAt = createdAt;
        this.resultDelivered = resultDelivered;
        this.clientId = clientId;
        this.eventId = eventId;
        this.subscriberUrlId = subscriberUrlId;
        this.responseData = responseData;
        this.requestData = requestData;
        this.deliveredAt = deliveredAt;
        this.refIdDeliveredAt = refIdDeliveredAt;
        this.callbackResponseCode = callbackResponseCode;
    }
}

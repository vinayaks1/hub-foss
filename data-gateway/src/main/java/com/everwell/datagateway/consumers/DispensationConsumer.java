package com.everwell.datagateway.consumers;

import com.everwell.datagateway.entities.Client;
import com.everwell.datagateway.enums.ConsumerQueueInfoEnum;
import com.everwell.datagateway.exceptions.NotFoundException;
import com.everwell.datagateway.exceptions.RedisException;
import com.everwell.datagateway.exceptions.RestTemplateException;
import com.everwell.datagateway.exceptions.URLDisabledException;
import com.everwell.datagateway.models.dto.PublishToSubscriberUrlDTO;
import com.everwell.datagateway.service.ClientService;
import com.everwell.datagateway.service.ConsumerService;
import com.everwell.datagateway.utils.TypeConverterUtil;
import io.sentry.Sentry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import static com.everwell.datagateway.constants.QueueConstants.*;

@Component
public class DispensationConsumer implements Consumer, ConsumerWebhook {


    private static Logger LOGGER = LoggerFactory.getLogger(DispensationConsumer.class);


    @Autowired
    private ConsumerService consumerService;


    @Override
    @RabbitListener(queues = SHARE_DISPENSATION_QUEUE, containerFactory = "rmqPrefetchCount20", concurrency = "3")
    public void consume(Message message) {
        LOGGER.debug("Message from queue is " + new String(message.getBody()));
        outgoingWebhook(SHARE_DISPENSATION_QUEUE, message);
    }

    @Override
    public void outgoingWebhook(String consumer, Message message) {
        try {
            String eventName = ConsumerQueueInfoEnum.getEventName(consumer);
            String clientName = ConsumerQueueInfoEnum.getClientName(consumer);
            consumerService.publishToSubscriberUrl(new PublishToSubscriberUrlDTO(clientName, eventName, new String(message.getBody()), null, false));
        } catch (NotFoundException | URLDisabledException e) {
            LOGGER.warn("NotFound or URLDisabled exception thrown");
        } catch (RestTemplateException | RedisException e) {
            LOGGER.warn("RestTemplate or Redis Exception thrown :" + e.getMessage());
            Sentry.capture(e);
            throw e;
        }
    }
}

package com.everwell.datagateway.consumers;

import com.everwell.datagateway.entities.*;
import com.everwell.datagateway.enums.ConsumerQueueInfoEnum;
import com.everwell.datagateway.exceptions.RedisException;
import com.everwell.datagateway.exceptions.RestTemplateException;
import com.everwell.datagateway.handlers.ResponseHandler;
import com.everwell.datagateway.handlers.ResponseHandlerMap;
import com.everwell.datagateway.models.dto.EventStreamingDTO;
import com.everwell.datagateway.models.dto.PublishToSubscriberUrlDTO;
import com.everwell.datagateway.models.response.ReferenceIdRMQResponse;
import com.everwell.datagateway.models.response.Response;
import com.everwell.datagateway.models.response.TestReplyQueueResponse;
import com.everwell.datagateway.repositories.EventClientRepository;
import com.everwell.datagateway.service.ClientRequestsService;
import com.everwell.datagateway.service.ConsumerService;
import com.everwell.datagateway.service.SubscriberUrlService;
import com.everwell.datagateway.utils.Utils;
import io.sentry.Sentry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.everwell.datagateway.constants.QueueConstants.REF_ID_REPLY_QUEUE;


@Component//This annotation is required by rabbit
public class ReferenceIdReplyConsumer implements Consumer, ConsumerWebhook {

    private static Logger LOGGER = LoggerFactory.getLogger(ReferenceIdReplyConsumer.class);

    @Autowired
    private ConsumerService consumerService;

    @Autowired
    private ClientRequestsService clientRequestsService;

    @Autowired
    private EventClientRepository eventClientRepository;


    @Autowired
    private SubscriberUrlService subscriberUrlService;

    @Autowired
    private ResponseHandlerMap responseHandlerMap;

    @Override
    @RabbitListener(queues = {REF_ID_REPLY_QUEUE},containerFactory = "rmqPrefetchCount20", concurrency = "3")
    public void consume(Message message) {
        LOGGER.debug("Message from " + REF_ID_REPLY_QUEUE + " is " + new String(message.getBody()));
        outgoingWebhook(ConsumerQueueInfoEnum.REF_ID_REPLY_QUEUE.getQueueName(), message);
    }


    @Override
    public void outgoingWebhook(String queueName, Message messageRmq) {
        try {
            String message = new String(messageRmq.getBody());
            EventStreamingDTO eventStreamingDTO = Utils.convertStrToObject(message,EventStreamingDTO.class);
            String fieldAsJsonString = Utils.asJsonString(eventStreamingDTO.getField());
            TestReplyQueueResponse testReplyQueueResponse = Utils.convertStrToObject(fieldAsJsonString,TestReplyQueueResponse.class);
            ReferenceIdRMQResponse referenceIDRMQResponse = ReferenceIdRMQResponse.builder()
                    .RefId(testReplyQueueResponse.getRefId())
                    .Response(testReplyQueueResponse.getResponse())
                    .build();
            Response<ReferenceIdRMQResponse> response = new Response<ReferenceIdRMQResponse>(Boolean.parseBoolean(testReplyQueueResponse.getSuccess()), referenceIDRMQResponse);
            Long referenceId = referenceIDRMQResponse.getRefId();
            ClientRequests clientRequests = clientRequestsService.getClientRequestsById(referenceId);
            Client client = clientRequests.getClient();
            Event event = clientRequests.getEvent();
            boolean sentToDlq = consumerService.checkSeverityAndSendToDlq(client, messageRmq);
            if (!sentToDlq) {
                EventClient eventClient = eventClientRepository.findByClientIdAndEventId(client.getId(), event.getId());
                SubscriberUrl subscriberUrl = subscriberUrlService.findByEventClientId(eventClient);
                clientRequests.setSubscriberUrl(subscriberUrl);
                ResponseHandler handler = responseHandlerMap.getResponseHandler(eventClient.responseType);
                String payload = handler.convertToResponseType(response);
                clientRequests.setResponseData(payload);
                clientRequestsService.updateClientRequest(clientRequests);
                String clientName = client.getUsername();
                String eventName = event.getEventName();
                consumerService.publishToSubscriberUrl(new PublishToSubscriberUrlDTO(clientName, eventName, payload, referenceId, false));
                clientRequestsService.setIsDeliveredTrueAndDeliveredAtTime(referenceId, new java.util.Date());
                LOGGER.debug("Client request with id - " + referenceId + " delivered successfully");
            }
        } catch (RestTemplateException | RedisException | IllegalArgumentException e) {
            LOGGER.warn("RestTemplate|Redis|IllegalArgumentException Exception thrown :" + e.getMessage());
            Sentry.capture(e);
            throw e;
        } catch (Exception e) {
            LOGGER.warn("Exception thrown :" + e.getMessage());
            Sentry.capture(e);
        }
    }
}


package com.everwell.datagateway.consumers;

import com.everwell.datagateway.exceptions.RedisException;
import com.everwell.datagateway.exceptions.RestTemplateException;
import com.everwell.datagateway.models.dto.ABDMConsentRequestDto;
import com.everwell.datagateway.service.ABDMHelperService;
import com.everwell.datagateway.utils.CacheUtils;
import com.everwell.datagateway.utils.Utils;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.sentry.Sentry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.everwell.datagateway.constants.Constants.ABDM_CACHE_DISCOVERY_REQUEST_TTL;
import static com.everwell.datagateway.constants.Constants.ABDM_CONSENT_INITIATION_CACHE_PREFIX;
import static com.everwell.datagateway.constants.QueueConstants.*;

@Component
public class ABDMConsentRequestConsumer implements Consumer,ConsumerWebhook{

    @Autowired
    private ABDMHelperService abdmHelperService;

    private static Logger LOGGER = LoggerFactory.getLogger(ABDMConsentRequestConsumer.class);

    @Override
    @RabbitListener(queues = ABDM_HIU_QUEUE, containerFactory = "rmqPrefetchCount20", concurrency = "3")
    public void consume(Message message) {
        String msg = new String(message.getBody());
        LOGGER.debug(msg);
        outgoingWebhook(ABDM_HIU_QUEUE,message);
    }

    @Override
    public void outgoingWebhook(String queueName, Message message) {
        try {
            ABDMConsentRequestDto abdmConsentRequestDto = Utils.convertStrToObject(new String(message.getBody()), ABDMConsentRequestDto.class);
            String requestId = abdmConsentRequestDto.getAbdmConsentInitiationRequest().getRequestId();
            CacheUtils.putIntoCache(ABDM_CONSENT_INITIATION_CACHE_PREFIX + requestId , Utils.asJsonString(abdmConsentRequestDto), ABDM_CACHE_DISCOVERY_REQUEST_TTL);
            abdmHelperService.consentRequestInit(abdmConsentRequestDto.getAbdmConsentInitiationRequest());
        }
        catch (RestTemplateException | IllegalArgumentException | RedisException | JsonProcessingException e){
            LOGGER.warn("RestTemplate|Redis|IllegalArgumentException Exception thrown :" + e.getMessage());
            Sentry.capture(e);
        }
    }
}

package com.everwell.datagateway.consumers;

import com.everwell.datagateway.entities.Client;
import com.everwell.datagateway.entities.ClientRequests;
import com.everwell.datagateway.entities.Event;
import com.everwell.datagateway.entities.EventClient;
import com.everwell.datagateway.exceptions.RestTemplateException;
import com.everwell.datagateway.handlers.ResponseHandler;
import com.everwell.datagateway.handlers.ResponseHandlerMap;
import com.everwell.datagateway.models.dto.*;
import com.everwell.datagateway.models.request.AbdmClientDto.HipHITransferNotifyRequest;
import com.everwell.datagateway.models.response.ABDMReplyQueueResponse;
import com.everwell.datagateway.models.request.AbdmClientDto.DataTransferRequest;
import com.everwell.datagateway.models.request.AbdmClientDto.DataTransferRequest.*;
import com.everwell.datagateway.models.request.AbdmClientDto.HipOnRequestDto;
import com.everwell.datagateway.models.response.ReferenceIdRMQResponse;
import com.everwell.datagateway.models.response.Response;
import com.everwell.datagateway.models.response.TestReplyQueueResponse;
import com.everwell.datagateway.repositories.EventClientRepository;
import com.everwell.datagateway.service.ABDMHelperService;
import com.everwell.datagateway.service.ClientRequestsService;
import com.everwell.datagateway.utils.EncryptionUtil;
import com.everwell.datagateway.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import io.sentry.Sentry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.everwell.datagateway.constants.Constants.DIAGNOSTIC_PROFILE;
import static com.everwell.datagateway.constants.Constants.DISPENSATION_PROFILE;
import static com.everwell.datagateway.constants.QueueConstants.DATA_EXCHANGE_QUEUE;

@Component
public class ABDMDataExchangeConsumer implements Consumer,ConsumerWebhook {

    @Autowired
    ResponseHandlerMap responseHandlerMap;

    @Autowired
    private ABDMHelperService abdmHelperService;

    @Autowired
    private ClientRequestsService clientRequestsService;

    @Autowired
    private EventClientRepository eventClientRepository;

    @Value("${NDHM_HIP_NAME:}")
    String hipId;

    private static Logger LOGGER = LoggerFactory.getLogger(ABDMDataExchangeConsumer.class);

    @Override
    @RabbitListener(queues = {DATA_EXCHANGE_QUEUE},containerFactory = "rmqPrefetchCount20", concurrency = "3")
    public void consume(Message message) {
        String msg = new String(message.getBody());
        LOGGER.info(msg);
        outgoingWebhook(DATA_EXCHANGE_QUEUE,message);
    }

    @Override
    public void outgoingWebhook(String queueName, Message message) {

        try{
            EventStreamingDTO eventStreamingDTO = Utils.convertStrToObject(new String(message.getBody()),EventStreamingDTO.class);
            String fieldAsJsonString = Utils.asJsonString(eventStreamingDTO.getField());
            TestReplyQueueResponse testReplyQueueResponse = Utils.convertStrToObject(fieldAsJsonString,TestReplyQueueResponse.class);
            ReferenceIdRMQResponse referenceIDRMQResponse = ReferenceIdRMQResponse.builder()
                                                            .RefId(testReplyQueueResponse.getRefId())
                                                            .Response(testReplyQueueResponse.getResponse())
                                                            .build();
            ABDMReplyQueueResponse abdmReplyQueueResponse = Utils.convertStrToObject(testReplyQueueResponse.getResponse(),ABDMReplyQueueResponse.class);
            HipOnRequestDto hipOnRequestDto = new HipOnRequestDto(abdmReplyQueueResponse.getDataPullRequest().getTransactionId(),abdmReplyQueueResponse.getDataPullRequest().getRequestId(), abdmReplyQueueResponse.getErrorMessage());
            LOGGER.info(Utils.asJsonString(hipOnRequestDto));

            // /hip/on-request call to abdm gateway
            abdmHelperService.hipOnRequestCall(hipOnRequestDto);
            Long referenceId = referenceIDRMQResponse.getRefId();
            ClientRequests clientRequests = clientRequestsService.getClientRequestsById(referenceId);
            Client client = clientRequests.getClient();
            Event event = clientRequests.getEvent();
            if(!Boolean.parseBoolean(testReplyQueueResponse.getSuccess())){
                String payload = Utils.asJsonString(hipOnRequestDto);
                clientRequests.setResponseData(payload); // 255 is length of payload column i.e. varchar(255)
                clientRequestsService.updateClientRequest(clientRequests);
            }
            else {
                EventClient eventClient = eventClientRepository.findByClientIdAndEventId(client.getId(), event.getId());
                ResponseHandler responseHandler = responseHandlerMap.getResponseHandler(eventClient.responseType);
                String payload = prepareDataForTransfer(abdmReplyQueueResponse,responseHandler);
                clientRequests.setResponseData(payload);
                clientRequestsService.updateClientRequest(clientRequests);

                // push data to datapush url
                boolean success = abdmHelperService.pushToCallBackUrl(abdmReplyQueueResponse.getDataPullRequest().getHiRequest().getDataPushUrl(),payload);

                if(success) {
                    clientRequestsService.setIsDeliveredTrueAndDeliveredAtTime(referenceId,new Date());
                    List<String> episodeIds = new ArrayList<>(abdmReplyQueueResponse.getEpisodeMap().keySet());
                    String transactionId = abdmReplyQueueResponse.getDataPullRequest().getTransactionId();
                    String consentId = abdmReplyQueueResponse.getDataPullRequest().getHiRequest().getConsent().id;
                    HipHITransferNotifyRequest hipHITransferNotifyRequest = new HipHITransferNotifyRequest(transactionId,consentId,episodeIds,hipId);
                    LOGGER.info(Utils.asJsonString(hipHITransferNotifyRequest));
                    // /hip/transfer/notify call to abdm gateway
                    abdmHelperService.hipHiTransferNotify(Utils.asJsonString(hipHITransferNotifyRequest));
                }
            }
        } catch (RestTemplateException | IllegalArgumentException e){
            LOGGER.warn("RestTemplate|IllegalArgumentException Exception thrown :" + e.getMessage());
            Sentry.capture(e);
        } catch (Exception e) {
            LOGGER.warn("Exception thrown :" + e.getMessage());
            Sentry.capture(e);
        }
    }

    String prepareDataForTransfer(ABDMReplyQueueResponse abdmReplyQueueResponse,ResponseHandler responseHandler) throws Exception {
        KeyMaterial keyMaterial = EncryptionUtil.generate();
        String publicKey = EncryptionUtil.getBase64String(EncryptionUtil.getEncodedHIPPublicKey(EncryptionUtil.getKey(keyMaterial.getPublicKey())));
        String nonce = keyMaterial.getNonce();
        List<Entry> entries = new ArrayList<>();
        String receiverKey = abdmReplyQueueResponse.getDataPullRequest().getHiRequest().getKeyMaterial().getDhPublicKey().keyValue;
        String receiverNonce = abdmReplyQueueResponse.getDataPullRequest().getHiRequest().getKeyMaterial().nonce;

        for(Map.Entry<String, List<CommonFHIRDto>> entry : abdmReplyQueueResponse.getEpisodeMap().entrySet()){
            for(CommonFHIRDto commonFHIRDto : entry.getValue()){

                ReferenceIdRMQResponse referenceIdRMQResponse = ReferenceIdRMQResponse.builder()
                                                                .Response(Utils.asJsonString(commonFHIRDto))
                                                                .build();
                referenceIdRMQResponse.setResponse(Utils.asJsonString(commonFHIRDto));
                String fhirResponse = responseHandler.convertToResponseType(new Response<>(true, referenceIdRMQResponse));
                Response<ReferenceIdRMQResponse> response = Utils.convertStrToObject(fhirResponse,new TypeReference<Response<ReferenceIdRMQResponse>>(){});
                if(commonFHIRDto.getProfileType().equals(DIAGNOSTIC_PROFILE)) {
                    String bundleList = response.getData().getResponse();
                    List<String> serializedBundles = Utils.convertStrToObject(bundleList, new TypeReference<List<String>>(){});
                    for(String bundle : serializedBundles) {
                        EncryptionRequest encryptionRequest = new EncryptionRequest(receiverKey,receiverNonce,keyMaterial,bundle);
                        EncryptionResponse encryptionResponse = EncryptionUtil.encrypt(encryptionRequest);
                        entries.add(new Entry(encryptionResponse.getEncryptedData(),entry.getKey()));
                    }
                }

                else if(commonFHIRDto.getProfileType().equals(DISPENSATION_PROFILE )) {
                    String serializedBundle = response.getData().getResponse();
                    EncryptionRequest encryptionRequest = new EncryptionRequest(receiverKey,receiverNonce,keyMaterial,serializedBundle);
                    EncryptionResponse encryptionResponse = EncryptionUtil.encrypt(encryptionRequest);
                    entries.add(new Entry(encryptionResponse.getEncryptedData(),entry.getKey()));
                }

            }
        }
        DataTransferRequest dataTransferRequest = new DataTransferRequest(publicKey,nonce,abdmReplyQueueResponse.getDataPullRequest().getHiRequest().getKeyMaterial(),abdmReplyQueueResponse.getDataPullRequest().getTransactionId(),entries);
        LOGGER.info("[prepareDataForTransfer] Data Transfer Request : " + Utils.asJsonString(dataTransferRequest));
        return Utils.asJsonString(dataTransferRequest);
    }

}

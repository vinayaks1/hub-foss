package com.everwell.datagateway.consumers;

import com.everwell.datagateway.enums.ConsumerQueueInfoEnum;
import com.everwell.datagateway.exceptions.NotFoundException;
import com.everwell.datagateway.exceptions.RedisException;
import com.everwell.datagateway.exceptions.RestTemplateException;
import com.everwell.datagateway.exceptions.URLDisabledException;
import com.everwell.datagateway.models.dto.PublishToSubscriberUrlDTO;
import com.everwell.datagateway.service.ConsumerService;
import io.sentry.Sentry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.everwell.datagateway.constants.QueueConstants.PATIENT_ENROLLMENT_GRAMENER_QUEUE;


@Component
public class PEConsumerGramener implements Consumer, ConsumerWebhook {


    private static Logger LOGGER = LoggerFactory.getLogger(PEConsumerGramener.class);


    @Autowired
    private ConsumerService consumerService;


    @Override
    @RabbitListener(queues = PATIENT_ENROLLMENT_GRAMENER_QUEUE,containerFactory = "rmqPrefetchCount20", concurrency = "3")
    public void consume(Message message) {
       LOGGER.debug("Message from queue is " + new String(message.getBody()));
       outgoingWebhook(PATIENT_ENROLLMENT_GRAMENER_QUEUE, message);
    }

    @Override
    public void outgoingWebhook(String consumer, Message message) {
        try {
            String eventName = ConsumerQueueInfoEnum.getEventName(consumer);
            String clientName = ConsumerQueueInfoEnum.getClientName(consumer);
            consumerService.publishToSubscriberUrl(new PublishToSubscriberUrlDTO(clientName, eventName, new String(message.getBody()), null, false));
        } catch (NotFoundException | URLDisabledException e) {
            LOGGER.warn("NotFound or URLDisabled exception thrown");
        }  catch (RestTemplateException | RedisException e) {
            LOGGER.warn("RestTemplate or Redis Exception thrown :" + e.getMessage());
            Sentry.capture(e);
            throw e;
        }
    }


}

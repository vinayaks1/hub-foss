package com.everwell.datagateway.consumers;

import com.everwell.datagateway.apiHelpers.ABDMHelper;
import com.everwell.datagateway.models.request.abdm.ABDMNotifyViaSMSRequest;
import com.everwell.datagateway.models.response.RestServiceResponse;
import com.everwell.datagateway.utils.Utils;
import io.sentry.Sentry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.everwell.datagateway.constants.QueueConstants.ABDM_NOTIFY_VIA_SMS_QUEUE;

@Component
public class ABDMNotifyViaSMSConsumer implements Consumer, ConsumerWebhook {


    private static Logger LOGGER = LoggerFactory.getLogger(ABDMNotifyViaSMSConsumer.class);


    @Autowired
    private ABDMHelper abdmHelper;

    private static String abdmSMSNotifyUrl = "/v0.5/patients/sms/notify2";

    @Override
    @RabbitListener(queues = ABDM_NOTIFY_VIA_SMS_QUEUE, containerFactory = "rmqPrefetchCount20", concurrency = "3")
    public void consume(Message message) {
        LOGGER.info("Message from queue is " + new String(message.getBody()));
        outgoingWebhook(ABDM_NOTIFY_VIA_SMS_QUEUE, message);
    }

    @Override
    public void outgoingWebhook(String consumer, Message messageRmq) {
        try {
            String message = new String(messageRmq.getBody());
            ABDMNotifyViaSMSRequest abdmNotifyViaSMSRequest = Utils.convertStrToObject(message,ABDMNotifyViaSMSRequest.class);
            RestServiceResponse response = abdmHelper.publishToABDM(Utils.asJsonString(abdmNotifyViaSMSRequest), abdmSMSNotifyUrl);
            if(response.getCode() == 202)
            {
                LOGGER.info("[Notify Via SMS: Message Published to ABDM]");
            }
            else{
                LOGGER.info("[Notify Via SMS: Message not Published to ABDM] " + response.getMessage());
            }
        } catch (Exception e) {
            LOGGER.warn("Exception thrown :" + e);
            Sentry.capture(e);
        }
    }
}

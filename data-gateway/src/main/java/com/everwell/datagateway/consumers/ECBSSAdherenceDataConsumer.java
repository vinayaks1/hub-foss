package com.everwell.datagateway.consumers;

import com.everwell.datagateway.constants.Constants;
import com.everwell.datagateway.constants.ECBSSConstants;
import com.everwell.datagateway.enums.ConsumerQueueInfoEnum;
import com.everwell.datagateway.enums.EventEnum;
import com.everwell.datagateway.enums.StatusCodeEnum;
import com.everwell.datagateway.exceptions.NotFoundException;
import com.everwell.datagateway.models.dto.EventStreamingDTO;
import com.everwell.datagateway.models.dto.PublishToSubscriberUrlDTO;
import com.everwell.datagateway.models.dto.ecbss.AdherenceDetailsDto;
import com.everwell.datagateway.models.dto.ecbss.EpisodeDataDto;
import com.everwell.datagateway.models.request.ecbss.AdherenceDataRequest;
import com.everwell.datagateway.models.response.ecbss.TrackedEntityInstancesResponse;
import com.everwell.datagateway.publishers.RMQPublisher;
import com.everwell.datagateway.service.ConsumerService;
import com.everwell.datagateway.service.ECBSSHelperService;
import com.everwell.datagateway.utils.Utils;
import io.sentry.Sentry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.everwell.datagateway.constants.QueueConstants.ECBSS_ADHERENCE_DATA_QUEUE;

@Component
public class ECBSSAdherenceDataConsumer implements Consumer, ConsumerWebhook {

    private static Logger LOGGER = LoggerFactory.getLogger(ECBSSAdherenceDataConsumer.class);

    @Autowired
    private ECBSSHelperService eCBSSHelper;

    @Autowired
    private RMQPublisher rabbitMQPublisherService;

    @Autowired
    private ConsumerService consumerService;

    @Override
    @RabbitListener(queues = ECBSS_ADHERENCE_DATA_QUEUE, containerFactory = "rmqPrefetchCount20", concurrency = "3")
    public void consume(Message message) {
        LOGGER.debug("Message from queue is " + new String(message.getBody()));
        outgoingWebhook(ECBSS_ADHERENCE_DATA_QUEUE, message);
    }

    @Override
    public void outgoingWebhook(String consumer, Message messageRmq) {
        try {
            String message = new String(messageRmq.getBody());
            String clientId = (String) messageRmq.getMessageProperties().getHeaders().get(Constants.RMQ_HEADER_CLIENT_ID);
            if (StringUtils.isEmpty(clientId)) {
                throw new NotFoundException(StatusCodeEnum.CLIENT_ID_UNAVAILABLE);
            }
            //Convert message received to AdherenceDataRequest object
            AdherenceDataRequest adherenceDataRequest = Utils.convertStrToObject(message, AdherenceDataRequest.class);
            AdherenceDetailsDto adherenceDetailsDto = adherenceDataRequest.getEcbssAdherenceDetailsDto();
            // Fetch and update ExternalId2 mapping for given episode if not mapped already
            if (StringUtils.isEmpty(adherenceDataRequest.getExternalId2())) {
                adherenceDetailsDto = fetchAndProcessTrackedEntityInstance(adherenceDataRequest, clientId);
            }
            // Send the adherenceData to ECBSS
            sendAdherenceData(adherenceDetailsDto, ConsumerQueueInfoEnum.getClientName(consumer));
        } catch (Exception e) {
            Sentry.capture(e);
        }
    }

    /**
     * Method to fetch trackedEntityInstance (ExternalId2) from ECBSS for given OrgUnit and ExternalId1 of episode
     *
     * @param adherenceDataRequest - adherenceDataRequest received in message
     * @param clientId             - clientId of the given episode
     * @return adherenceDetails which is to be sent to ECBSS
     */
    private AdherenceDetailsDto fetchAndProcessTrackedEntityInstance(AdherenceDataRequest adherenceDataRequest, String clientId) {
        AdherenceDetailsDto adherenceDetailsDto = null;
        // Fetch trackedEntityInstance from ECBSS for the given externalId1 and orgUnitId
        TrackedEntityInstancesResponse trackedEntityInstancesResponse = eCBSSHelper.getTrackedEntityInstance(adherenceDataRequest.getEcbssAdherenceDetailsDto().getOrgUnit(), adherenceDataRequest.getExternalId1());
        if (null != trackedEntityInstancesResponse) {
            ArrayList<TrackedEntityInstancesResponse.TrackedEntityInstance> trackedEntityInstancesArray = trackedEntityInstancesResponse.getTrackedEntityInstances();
            // Update externalId2 only if single trackedEntityInstance is received
            if (!CollectionUtils.isEmpty(trackedEntityInstancesArray) && trackedEntityInstancesArray.size() == 1 && !StringUtils.isEmpty(trackedEntityInstancesArray.get(0).getTrackedEntityInstance())) {
                String trackedEntityInstance = trackedEntityInstancesArray.get(0).getTrackedEntityInstance();
                adherenceDetailsDto = adherenceDataRequest.getEcbssAdherenceDetailsDto();
                adherenceDetailsDto.setTrackedEntityInstance(trackedEntityInstance);
                // Update the fetched trackedEntityInstance (ExternalId2) for the given episode
                updateExternalId(adherenceDataRequest.getEntityId(), trackedEntityInstance, clientId);
            } else {
                Sentry.capture(ECBSSConstants.MULTIPLE_TRACKED_ENTITY_INSTANCE_RECEIVED + Utils.asJsonString(trackedEntityInstancesArray));
            }
        }
        return adherenceDetailsDto;
    }

    /**
     * Method to push the externalId to platform which is to be mapped for the given entityId
     *
     * @param entityId    - The entity whose externalId is to be added
     * @param externalId2 - The externalId which is to be mapped
     * @param clientId    - Client for whose entity externalId is to be updated
     */
    private void updateExternalId(String entityId, String externalId2, String clientId) {
        EpisodeDataDto episodeDataDto = new EpisodeDataDto(entityId, externalId2);
        EventStreamingDTO eventStreamingDTO = new EventStreamingDTO();
        eventStreamingDTO.setField(episodeDataDto);
        Map<String, Object> headers = new HashMap<>();
        headers.put(Constants.RMQ_HEADER_CLIENT_ID, clientId);
        rabbitMQPublisherService.send(eventStreamingDTO, Constants.ECBSS_UPDATE_EPISODE_DATA_ROUTING_KEY, Constants.EXCHANGE, headers);
    }

    /**
     * Method to publish given adherence data for given client on the url subscribed to SEND_ADHERENCE_DATA event
     *
     * @param adherenceDetailsDto - The adherence details which is to be published
     * @param clientName          - The client for which message is to be published
     */
    private void sendAdherenceData(AdherenceDetailsDto adherenceDetailsDto, String clientName) {
        if (null != adherenceDetailsDto) {
            String data = Utils.asJsonString(adherenceDetailsDto);
            PublishToSubscriberUrlDTO publishToSubscriberUrlDTO = new PublishToSubscriberUrlDTO(clientName, EventEnum.SEND_ADHERENCE_DATA.getEventName(), data, null, true);
            consumerService.publishToSubscriberUrl(publishToSubscriberUrlDTO);
        }
    }
}

package com.everwell.datagateway.consumers;

import com.everwell.datagateway.models.request.abdm.ABDMLinkCareContextEvent;
import com.everwell.datagateway.models.request.abdm.ABDMNotifyCareContextEvent;
import com.everwell.datagateway.service.ABDMIntegrationService;
import com.everwell.datagateway.service.ConsumerService;
import com.everwell.datagateway.utils.TypeConverterUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.everwell.datagateway.constants.QueueConstants.ABDM_CARE_CONTEXT_NOTIFY_QUEUE;

@Component
public class ABDMNotifyCareContextConsumer implements Consumer, ConsumerWebhook {

    private static Logger LOGGER = LoggerFactory.getLogger(ABDMNotifyCareContextConsumer.class);

    @Autowired
    private ConsumerService consumerService;
    @Autowired
    private ABDMIntegrationService abdmHelper;

    @Override
    @RabbitListener(queues = ABDM_CARE_CONTEXT_NOTIFY_QUEUE, containerFactory = "rmqPrefetchCount20", concurrency = "3")
    public void consume(Message message) {
        outgoingWebhook(ABDM_CARE_CONTEXT_NOTIFY_QUEUE, message);
    }

    @Override
    public void outgoingWebhook(String queueName, Message message) {
        try {
            String msg = TypeConverterUtil.byteArrayToString(message.getBody());
            Gson gson = new GsonBuilder().create();
            ABDMNotifyCareContextEvent notifyCareContextDetails = gson.fromJson(msg, ABDMNotifyCareContextEvent.class);
            abdmHelper.notifyCareContext(notifyCareContextDetails);
        }
        catch (Exception ex)
        {
            LOGGER.info(ex.toString());
        }
    }
}

package com.everwell.datagateway.consumers;

import com.everwell.datagateway.constants.Constants;
import com.everwell.datagateway.constants.EmailConstants;
import com.everwell.datagateway.constants.QueueConstants;
import com.everwell.datagateway.exceptions.ValidationException;
import com.everwell.datagateway.service.SendGridEmailService;
import io.sentry.Sentry;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;


@Component
public class DlxConsumer implements Consumer {

    private static Logger LOGGER = LoggerFactory.getLogger(DlxConsumer.class);

    @Autowired
    private SendGridEmailService emailService;

    private List<String> messagesToSend = new ArrayList<>();

    private BlockingQueue<String> queuedMessages = new LinkedBlockingQueue<>();

    @Value("${sendgrid.mail.senders}")
    private String SENDERS_EMAIL;

    @Value("${sendgrid.mail.developers}")
    private String EVERWELL_DEVELOPERS_MAIL;

    @Override
    @RabbitListener(queues = QueueConstants.DLX_QUEUE, containerFactory = "rmqPrefetchCount20", concurrency = "2")
    public void consume(Message message) {
        try {
            String body = new String(message.getBody());
            int startIndex = body.indexOf(Constants.REF_ID_RESPONSE);
            int endIndex = body.indexOf('R', startIndex + 1);
            if (startIndex != -1 && endIndex != -1) {

                body = body.substring(startIndex - 1, endIndex - 2);
                queuedMessages.put(body);

                if (queuedMessages.size() >= EmailConstants.BULK_MESSAGES_LIMIT) {
                    queuedMessages.drainTo(messagesToSend, EmailConstants.BULK_MESSAGES_LIMIT);
                    String emailBody = String.join("\n", messagesToSend);
                    messagesToSend.clear();
                    emailService.sendText(SENDERS_EMAIL, EVERWELL_DEVELOPERS_MAIL, EmailConstants.DLQ_EMAIL_SUBJECT, emailBody);
                }
            }
        } catch (Exception e) {
            LOGGER.warn("[DlxConsumer] Exception thrown :" + e.getMessage());
            Sentry.capture(e);
        }
    }
}

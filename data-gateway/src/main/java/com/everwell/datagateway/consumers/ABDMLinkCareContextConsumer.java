package com.everwell.datagateway.consumers;

import com.everwell.datagateway.models.request.abdm.ABDMLinkCareContextEvent;
import com.everwell.datagateway.service.ABDMIntegrationService;
import com.everwell.datagateway.utils.TypeConverterUtil;
import com.everwell.datagateway.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.everwell.datagateway.constants.QueueConstants.ABDM_LINK_CARE_CONTEXT_QUEUE;

@Component
public class ABDMLinkCareContextConsumer implements Consumer, ConsumerWebhook {

    private static Logger LOGGER = LoggerFactory.getLogger(ABDMLinkCareContextConsumer.class);

    @Autowired
    private ABDMIntegrationService abdmHelper;


    @Override
    @RabbitListener(queues = ABDM_LINK_CARE_CONTEXT_QUEUE, containerFactory = "rmqPrefetchCount20", concurrency = "3")
    public void consume(Message message) {
        outgoingWebhook(ABDM_LINK_CARE_CONTEXT_QUEUE, message);
    }

    @Override
    public void outgoingWebhook(String queueName, Message message) {
        try {
            String msg = TypeConverterUtil.byteArrayToString(message.getBody());
            Gson gson = new GsonBuilder().create();
            ABDMLinkCareContextEvent careContextDetails = gson.fromJson(msg, ABDMLinkCareContextEvent.class);
            LOGGER.info("CareContext 1st req "+Utils.asJsonString(careContextDetails));
            String resp = abdmHelper.initiateDemographicAuth(careContextDetails);
        }
        catch (Exception ex)
        {
            LOGGER.info(ex.toString());
        }
    }
}

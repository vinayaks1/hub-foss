package com.everwell.datagateway.controllers;

import com.everwell.datagateway.constants.Constants;
import com.everwell.datagateway.enums.EventEnum;
import com.everwell.datagateway.enums.StatusCodeEnum;
import com.everwell.datagateway.models.dto.ABDMAcknowledgementDTO;
import com.everwell.datagateway.models.request.AbdmClientDto.DataPullRequest;
import com.everwell.datagateway.models.request.abdm.*;
import com.everwell.datagateway.models.response.PublisherResponse;
import com.everwell.datagateway.models.response.abdm.Error;
import com.everwell.datagateway.service.AuthenticationService;
import com.everwell.datagateway.utils.CacheUtils;
import com.everwell.datagateway.models.request.ABDMAddUpdateConsentRequest;
import com.everwell.datagateway.models.request.abdm.ABDMLinkConfirmationRequest;
import com.everwell.datagateway.models.request.abdm.ABDMLinkInitiationRequest;
import com.everwell.datagateway.models.response.ABDMErrorResponse;
import com.everwell.datagateway.models.response.MessageApiResponse;
import com.everwell.datagateway.models.response.abdm.ABDMAuthOnConfirmResponse;
import com.everwell.datagateway.models.response.abdm.OnAuthInit.ABDMOnAuthInitResponse;
import com.everwell.datagateway.service.ABDMIntegrationService;
import com.everwell.datagateway.service.EmailService;
import com.everwell.datagateway.service.PublisherService;
import com.everwell.datagateway.utils.Utils;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Map;
import com.everwell.datagateway.models.request.abdm.ABDMDiscoveryRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.json.simple.JSONObject;



@RestController
public class ABDMController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ABDMController.class);

    @Autowired
    private PublisherService publisherService;

    @Autowired
    private ABDMIntegrationService abdmHelper;

    @Autowired
    AuthenticationService authenticationService;
    @Autowired
    private EmailService emailService;

    @PostMapping("/v0.5/users/auth/on-init")
    public ResponseEntity abdmAuthOnInit(@RequestBody ABDMOnAuthInitResponse data) {
        LOGGER.info("ABDMOnAuthInitResponse Request Received "+ Utils.asJsonString(data));
        LOGGER.info("Cache "+ Boolean.toString(CacheUtils.hasKey(data.resp.requestId)));
        if (CacheUtils.hasKey(data.resp.requestId)) {
            boolean success = abdmHelper.getCareContextLinkingToken(data.resp.requestId, data.auth.transactionId);
            if (success)
                return new ResponseEntity(HttpStatus.ACCEPTED);
        }
        return new ResponseEntity("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @PostMapping("/v0.5/users/auth/on-confirm")
    public ResponseEntity abdmAuthOnConfirm(@RequestBody ABDMAuthOnConfirmResponse data) {
        LOGGER.info("ABDMAuthOnConfirmResponse Request Received "+ Utils.asJsonString(data));
        LOGGER.info("Cache "+ Boolean.toString(CacheUtils.hasKey(data.resp.requestId)));
        if (CacheUtils.hasKey(data.resp.requestId)) {
            boolean success = abdmHelper.addCareContext(data.resp.requestId, data.auth.accessToken);
            if (success)
                return new ResponseEntity(HttpStatus.ACCEPTED);
        }
        return new ResponseEntity("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @PostMapping("/v0.5/links/link/on-add-contexts")
    public ResponseEntity abdmOnAddCareContext(@RequestBody ABDMAcknowledgementDTO data) {
        LOGGER.info("ABDMAcknowledgementDTO Request Received "+ Utils.asJsonString(data));
        LOGGER.info("Cache "+ Boolean.toString(CacheUtils.hasKey(data.resp.requestId)));
        if (CacheUtils.hasKey(data.resp.requestId)) {
            ABDMLinkCareContextEvent careContextDetails = abdmHelper.getCareContextLinkingDetailsFor(data.resp.requestId);
            ABDMNotifyCareContextEvent event = new ABDMNotifyCareContextEvent(
                    careContextDetails.episodeId,
                    careContextDetails.personId,
                    careContextDetails.hiTypes,
                    careContextDetails.personName,
                    careContextDetails.abhaAddress
            );
            boolean success = abdmHelper.notifyCareContext(event);
            if (success)
                return new ResponseEntity(HttpStatus.ACCEPTED);
        }
        return new ResponseEntity("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
    }


    @ApiOperation(value = "Add and Update Consent for ABDM", authorizations = {@Authorization(value = "jwtToken")})
    @PostMapping("/v0.5/consents/hip/notify")
    public ResponseEntity abdmAddUpdateConsent(@Valid @RequestBody ABDMAddUpdateConsentRequest consentData, @RequestHeader(name = Constants.X_HIP_ID) String hipId) {
        LOGGER.info("[ABDM Consent request received :]" + Utils.asJsonString(consentData));
        if (hipId.equals(Constants.X_HIP_ID_VALUE)) {
            Map<String, String> publisherResponse = publisherService.publishRequestForABDM(Utils.asJsonString(consentData), EventEnum.ABDM_ADD_UPDATE_CONSENT.getEventName());
            Map.Entry<String, String> responseMap = publisherResponse.entrySet().iterator().next();
            if (responseMap.getKey() == Constants.ERROR) {
                return new ResponseEntity(new ABDMErrorResponse(new MessageApiResponse(Constants.ABDM_EXCEPTION_CODE, responseMap.getValue())), StatusCodeEnum.INTERNAL_SERVER_ERROR.getCode());
            }
            return new ResponseEntity(HttpStatus.ACCEPTED);
        }
        return new ResponseEntity(new ABDMErrorResponse(new MessageApiResponse(Constants.ABDM_EXCEPTION_CODE, StatusCodeEnum.INVALID_CREDENTIALS.getMessage())), StatusCodeEnum.INVALID_CREDENTIALS.getCode());
    }

    @ApiOperation(value = "SMS Notify Response from ABDM", authorizations = {@Authorization(value = "jwtToken")})
    @PostMapping("/v0.5/patients/sms/on-notify")
    public ResponseEntity abdmSMSOnNotify(@Valid @RequestBody Object object, @RequestHeader(name = Constants.X_HIP_ID) String hipId) {
        if(hipId.equals(Constants.X_HIP_ID_VALUE))
        {
            LOGGER.info("[SMS On-Notify request received :]" + object.toString());
            return new ResponseEntity(HttpStatus.ACCEPTED);
        }
        LOGGER.warn("[Invalid Credentials]");
        return new ResponseEntity(new ABDMErrorResponse(new MessageApiResponse(Constants.ABDM_EXCEPTION_CODE, StatusCodeEnum.INVALID_CREDENTIALS.getMessage())), StatusCodeEnum.INVALID_CREDENTIALS.getCode());
    }

    @ApiOperation(value = "Data Pull Request", authorizations = {@Authorization(value = "jwtToken")})
    @PostMapping("/v0.5/health-information/hip/request")
    public ResponseEntity abdmDataPull(@Valid @RequestBody DataPullRequest dataPullRequest, @RequestHeader(name = Constants.X_HIP_ID) String hipId) {
        LOGGER.info("[data pull request received :]" + Utils.asJsonString(dataPullRequest));
        PublisherResponse publisherResponse = publisherService.publishRequest(Utils.asJsonString(dataPullRequest), EventEnum.HIP_DATA_EXCHANGE.getEventName());
          return new ResponseEntity(HttpStatus.ACCEPTED);
    }
    @ApiOperation(value = "Discovers Episodes based on identifiers", authorizations = {@Authorization(value = "jwtToken")})
    @PostMapping("/v0.5/care-contexts/discover")
    public ResponseEntity abdmDiscovery(@Valid @RequestBody ABDMDiscoveryRequest request)
    {
        Gson gson =  new GsonBuilder().create();
        String json = gson.toJson(request);
        LOGGER.info("[abdmDiscovery]" + json);
        abdmHelper.processDiscoveryRequest(request);
        return new ResponseEntity(HttpStatus.ACCEPTED);
    }

    @ApiOperation(value = "Sends an OTP to initiate linking of episodes", authorizations = {@Authorization(value = "jwtToken")})
    @PostMapping("/v0.5/links/link/init")
    public ResponseEntity abdmDiscoveryInitiateLinking(@Valid @RequestBody ABDMLinkInitiationRequest abdmLinkInitiationRequest) throws JsonProcessingException {
        String json = Utils.asJsonString(abdmLinkInitiationRequest);
        LOGGER.info("[abdmDiscoveryInitiateLinking]" + json);
        abdmHelper.processLinkInitiationRequest(abdmLinkInitiationRequest);
        return new ResponseEntity(HttpStatus.ACCEPTED);
    }

    @ApiOperation(value = "Sends an OTP to initiate linking of episodes", authorizations = {@Authorization(value = "jwtToken")})
    @PostMapping("/v0.5/links/link/confirm")
    public ResponseEntity abdmDiscoveryLinkConfirmation(@Valid @RequestBody ABDMLinkConfirmationRequest abdmLinkConfirmationRequest) throws JsonProcessingException {
        String json = Utils.asJsonString(abdmLinkConfirmationRequest);
        LOGGER.info("[abdmDiscoveryLinkConfirmation]" + json);
        abdmHelper.processLinkConfirmationRequest(abdmLinkConfirmationRequest);
        return new ResponseEntity(HttpStatus.ACCEPTED);
    }

    @ApiOperation(value = "Response to consent request", authorizations = {@Authorization(value = "jwtToken")})
    @PostMapping("/v0.5/consent-requests/on-init")
    public ResponseEntity abdmConsentRequestInitiation(@Valid @RequestBody ABDMConsentRequestOnInitiation abdmConsentRequestOnInitiation) throws JsonProcessingException {
        LOGGER.info("[ABDMConsentRequestOnInitiation]" + Utils.asJsonString(abdmConsentRequestOnInitiation));
        boolean success = abdmHelper.processConsentInitiation(abdmConsentRequestOnInitiation);
        if(!success) {
            return new ResponseEntity(new Error(1000,"Invalid data sent"),HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(HttpStatus.ACCEPTED);
    }
    @PostMapping("/v0.5/users/auth/notify")
    public ResponseEntity abdmAuthNotify(@RequestBody ABDMAuthNotifyRequest abdmAuthNotifyRequest) {
        LOGGER.info("[abdm auth-notify request received :]" + Utils.asJsonString(abdmAuthNotifyRequest));
        if (CacheUtils.hasKey(abdmAuthNotifyRequest.getAuth().transactionId)) {
            boolean success = abdmHelper.addCareContextDirectAuth(abdmAuthNotifyRequest);
            if (success)
                return new ResponseEntity(HttpStatus.ACCEPTED);
        }
        return new ResponseEntity("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ApiOperation(value = "Response to consent request", authorizations = {@Authorization(value = "jwtToken")})
    @PostMapping("/v0.5/consents/hiu/notify")
    public ResponseEntity abdmConsentNotification(@Valid @RequestBody ABDMConsentNotification abdmConsentNotification) throws JsonProcessingException {
        LOGGER.info("[abdmConsentNotification]" + Utils.asJsonString(abdmConsentNotification));
        abdmHelper.processConsentNotification(abdmConsentNotification);
        return new ResponseEntity(HttpStatus.ACCEPTED);
    }

}

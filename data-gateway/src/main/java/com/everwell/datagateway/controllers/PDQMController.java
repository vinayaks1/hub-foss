package com.everwell.datagateway.controllers;

import com.everwell.datagateway.entities.Client;
import com.everwell.datagateway.enums.EventEnum;
import com.everwell.datagateway.exceptions.PDQMCustomException;
import com.everwell.datagateway.models.request.EpisodeSearchRequest;
import com.everwell.datagateway.models.response.EpisodeSearchResult;
import com.everwell.datagateway.models.response.Response;
import com.everwell.datagateway.service.EpisodeServiceRestService;
import com.everwell.datagateway.service.PDQMService;
import com.fasterxml.jackson.databind.JsonNode;
import io.sentry.Sentry;
import org.hl7.fhir.r4.model.CapabilityStatement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
public class PDQMController extends BaseController {
    private static Logger LOGGER = LoggerFactory.getLogger(PDQMController.class);

    @Autowired
    EpisodeServiceRestService episodeService;

    @Autowired
    PDQMService pdqmService;

    @RequestMapping(value = "/v1/Patient", method = RequestMethod.GET)
    public ResponseEntity<?> getPatientDetails(HttpServletRequest request) {
        LOGGER.debug("PDQM getPatientDetails request accepted " + request);
        ResponseEntity<?> responseEntity = new ResponseEntity<>(HttpStatus.OK);
        Client client = getClientWithEventAccessCheck(EventEnum.GET_PATIENT_DETAILS.getEventName());
        try {
            EpisodeSearchRequest episodeSearchRequest = pdqmService.getEpisodeSearchRequestFromRequest(new HashMap<>(request.getParameterMap()));
            EpisodeSearchResult episodeSearchResult = episodeService.episodeSearch(episodeSearchRequest, client.getAccessibleClient());
            JsonNode bundleResponse = pdqmService.getPatientBundle(request, episodeSearchRequest, episodeSearchResult);
            responseEntity = ResponseEntity.ok(new Response<>(true, bundleResponse));
        } catch (PDQMCustomException ex) {
            responseEntity = ResponseEntity.badRequest().body(pdqmService.getOperationOutcomeForPDQMException(ex));
        } catch (Exception ex) {
            Sentry.capture(ex);
            responseEntity = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(pdqmService.getOperationOutcomeForException(ex));
        }
        return responseEntity;
    }

    @RequestMapping(value = "/v1/Patient", method = RequestMethod.POST)
    public ResponseEntity<?> searchPatientDetails(@RequestBody Map<String, Object> requestInput) {
        LOGGER.debug("PDQM searchPatientDetails request accepted " + requestInput);
        ResponseEntity<?> responseEntity = new ResponseEntity<>(HttpStatus.OK);
        Client client = getClientWithEventAccessCheck(EventEnum.SEARCH_PATIENT_DETAILS.getEventName());
        try {
            EpisodeSearchRequest episodeSearchRequest = pdqmService.getEpisodeSearchRequestFromRequest(new HashMap<>(requestInput));
            EpisodeSearchResult episodeSearchResult = episodeService.episodeSearch(episodeSearchRequest, client.getAccessibleClient());
            JsonNode bundleResponse = pdqmService.getPatientBundle(null, episodeSearchRequest, episodeSearchResult);
            responseEntity = ResponseEntity.ok(new Response<>(true, bundleResponse));
        } catch (PDQMCustomException ex) {
            responseEntity = ResponseEntity.badRequest().body(pdqmService.getOperationOutcomeForPDQMException(null));
        } catch (Exception ex) {
            Sentry.capture(ex);
            responseEntity = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(pdqmService.getOperationOutcomeForException(ex));
        }
        return responseEntity;
    }

    @RequestMapping(value = "metadata", method = RequestMethod.GET)
    public ResponseEntity<String> capabilityStatement() {
        LOGGER.debug("get capability statement request accepted");
        String capabilityStatement = pdqmService.getCapabilityStatement();
        return new ResponseEntity<>(capabilityStatement, HttpStatus.OK);
    }

}

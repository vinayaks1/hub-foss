package com.everwell.datagateway.controllers;

import ca.uhn.fhir.rest.server.exceptions.ForbiddenOperationException;
import com.everwell.datagateway.entities.Client;
import com.everwell.datagateway.enums.StatusCodeEnum;
import com.everwell.datagateway.exceptions.NotFoundException;
import com.everwell.datagateway.service.AuthenticationService;
import com.everwell.datagateway.service.ClientService;
import com.everwell.datagateway.service.EventService;
import com.everwell.datagateway.utils.CacheUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BaseController {

    @Autowired
    ClientService clientService;

    @Autowired
    EventService eventService;

    @Autowired
    AuthenticationService authenticationService;

    public Client getClientWithEventAccessCheck(String methodName) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authenticationService.getCurrentUsername(authentication);
        Client client = clientService.findByUsername(username);
        String eventId = CacheUtils.getFromCache(methodName);
        if(null == eventId) {
            eventId = eventService.findEventByEventName(methodName).getId().toString();
            CacheUtils.putIntoCache(methodName, eventId);
        }
        if(clientService.isClientSubscribedToEvent(client.id, Long.parseLong(eventId))) {
            return client;
        } else {
            throw new NotFoundException(StatusCodeEnum.CLIENT_NOT_SUBSCRIBED_TO_EVENT_OR_DISABLED);
        }
    }

}

package com.everwell.datagateway.controllers;


import com.everwell.datagateway.enums.EventEnum;
import com.everwell.datagateway.models.response.PublisherResponse;
import com.everwell.datagateway.models.response.Response;
import com.everwell.datagateway.service.PublisherService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController//the annotation which tells SpringBoot to treat this class as a REST API class
public class TestController {//This class defines methods related to test management, like adding or updating

    private static Logger LOGGER = LoggerFactory.getLogger(TestController.class);


    @Autowired
    private PublisherService publisherService;

    @ApiOperation(value = "Add Test", authorizations = {@Authorization(value = "jwtToken")})
    @PostMapping("/add-test")
    public ResponseEntity addTest(@RequestBody String addTestRequest) {// Adds test to the add-test queue and returns JSONObject in response
        PublisherResponse publisherResponse = publisherService.publishRequest(addTestRequest, EventEnum.ADD_TEST.getEventName());
        LOGGER.debug("***Add Test received***");
        return new ResponseEntity(new Response<>(true, publisherResponse), HttpStatus.OK);
    }


    @ApiOperation(value = "Get Test By Id", authorizations = {@Authorization(value = "jwtToken")})
    @GetMapping("/get-test-id")
    public @ResponseBody
    ResponseEntity<Response<PublisherResponse>> getTestById(@RequestParam(value = "testRequestId") Long testRequestId) {// Adds test to the add-test queue and returns JSONObject in response
        PublisherResponse publisherResponse = publisherService.publishRequest(testRequestId.toString(), EventEnum.GET_TEST_ID.getEventName());
        LOGGER.debug("***Get Test By Id received***");
        return new ResponseEntity(new Response<>(true, publisherResponse), HttpStatus.OK);
    }

    @ApiOperation(value = "Get Test By PatientId", authorizations = {@Authorization(value = "jwtToken")})
    @GetMapping("/get-test-patientId")
    public ResponseEntity getTestByPatient(@RequestParam(value = "patientId") Long patient) {// Adds test to the add-test queue and returns JSONObject in response
        PublisherResponse publisherResponse = publisherService.publishRequest(patient.toString(), EventEnum.GET_TEST_PATIENT_ID.getEventName());
        LOGGER.debug("***Get Test By PatientId received***");
        return new ResponseEntity(new Response<>(true, publisherResponse), HttpStatus.OK);
    }

    @ApiOperation(value = "Update Test", authorizations = {@Authorization(value = "jwtToken")})
    @PutMapping("/update-test")
    public ResponseEntity updateTest(@RequestBody String updateTestRequest) {// Adds test to the add-test queue and returns JSONObject in response
        PublisherResponse publisherResponse = publisherService.publishRequest(updateTestRequest, EventEnum.UPDATE_TEST.getEventName());
        LOGGER.debug("***Update Test received***");
        return new ResponseEntity(new Response<>(true, publisherResponse), HttpStatus.OK);
    }


}

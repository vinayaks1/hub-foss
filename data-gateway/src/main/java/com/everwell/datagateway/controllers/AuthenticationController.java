package com.everwell.datagateway.controllers;

import com.everwell.datagateway.enums.StatusCodeEnum;
import com.everwell.datagateway.models.request.AuthenticationRequest;
import com.everwell.datagateway.models.request.RegisterRequest;
import com.everwell.datagateway.models.response.MessageApiResponse;
import com.everwell.datagateway.service.AuthenticationServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;


@RestController
@CrossOrigin
public class AuthenticationController {

    @Autowired
    private AuthenticationServiceImpl authenticationService;

    @PostMapping(value = "/token")
    public ResponseEntity<MessageApiResponse<String>> generateToken(@RequestBody @Valid AuthenticationRequest authenticationRequest) throws Exception {
        String token = authenticationService.generateToken(authenticationRequest);
        StatusCodeEnum success = StatusCodeEnum.SUCCESS;
        return new ResponseEntity<>(new MessageApiResponse<>(success.getCode().toString(), success.getMessage(), token), success.getCode());
    }

    @PostMapping(value = "/register")
    public ResponseEntity<MessageApiResponse<String>> registerClient(@RequestBody @Valid RegisterRequest registerRequest) {
        registerRequest.validate();
        Boolean isRegistered = authenticationService.registerClient(registerRequest);
        if (isRegistered) {
            StatusCodeEnum success = StatusCodeEnum.SUCCESS;
            return new ResponseEntity<>(new MessageApiResponse<>(success.getMessage()), success.getCode());
        } else {
            StatusCodeEnum conflict = StatusCodeEnum.CONFLICT;
            return new ResponseEntity<>(new MessageApiResponse<>(conflict.getMessage()), conflict.getCode());
        }
    }
}

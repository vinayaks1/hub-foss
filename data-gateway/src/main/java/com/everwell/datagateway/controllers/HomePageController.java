package com.everwell.datagateway.controllers;

import com.everwell.datagateway.enums.StatusCodeEnum;
import com.everwell.datagateway.models.response.MessageApiResponse;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomePageController {

    private static Logger LOGGER = LoggerFactory.getLogger(HomePageController.class);

    @GetMapping("/")//map this function to the route / and method POST
    public ResponseEntity genericFallbackError(){
        return new ResponseEntity(new MessageApiResponse("Version: 1.3 (Update on 18th Dec 2020)"), StatusCodeEnum.SUCCESS.getCode());//Return successful response
    }

    @PostMapping("/v1/SubscriberUrl")//map this function to the route / and method POST
    public ResponseEntity genericSubscriberUrl(@RequestBody JSONObject jsonObject){
        LOGGER.debug(jsonObject.toString());
        return new ResponseEntity(new MessageApiResponse(jsonObject.toString()), StatusCodeEnum.SUCCESS.getCode());//Return successful response
    }
}

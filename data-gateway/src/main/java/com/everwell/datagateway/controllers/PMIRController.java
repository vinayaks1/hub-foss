package com.everwell.datagateway.controllers;

import com.everwell.datagateway.entities.Client;
import com.everwell.datagateway.enums.EventEnum;
import com.everwell.datagateway.models.response.ApiResponse;
import com.everwell.datagateway.models.response.Episode;
import com.everwell.datagateway.models.response.Response;
import com.everwell.datagateway.service.EpisodeServiceRestService;
import com.everwell.datagateway.service.PMIRService;
import com.everwell.datagateway.utils.FhirUtil;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Patient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
public class PMIRController extends BaseController {
    private static Logger LOGGER = LoggerFactory.getLogger(PMIRController.class);
    @Autowired
    PMIRService pmirService;

    @Autowired
    EpisodeServiceRestService episodeService;

    @RequestMapping(value = "v1/patient", method = RequestMethod.POST)
    public ResponseEntity<Response<Episode>> createPatient(@RequestBody String request) {
        LOGGER.debug("PMIR create patient request received " + request);
        Client client = getClientWithEventAccessCheck(EventEnum.CREATE_PATIENT.getEventName());
        Patient patient = FhirUtil.getPatientFromRequest(request);
        Map<String, Object> createEpisodeRequest = pmirService.createUpdateEpisodeRequest(patient,false, client.getAccessibleClient());
        ApiResponse<Episode> episode = episodeService.createEpisode(createEpisodeRequest, client.getAccessibleClient());
        return new ResponseEntity<>(new Response<>(true, episode.getData()), HttpStatus.CREATED);
    }

    @RequestMapping(value = "v1/patient", method = RequestMethod.PUT)
    public ResponseEntity<Response<Episode>> updatePatient(@RequestBody String request) {
        LOGGER.debug("PMIR update patient request received " + request);
        Client client = getClientWithEventAccessCheck(EventEnum.UPDATE_PATIENT.getEventName());
        Patient patient = FhirUtil.getPatientFromRequest(request);
        Map<String, Object> updateEpisodeRequest = pmirService.createUpdateEpisodeRequest(patient, true, client.getAccessibleClient());
        ApiResponse<Episode> episode = episodeService.updateEpisode(updateEpisodeRequest, client.getAccessibleClient());
        return new ResponseEntity<>(new Response<>(true, episode.getData()), HttpStatus.OK);
    }

    @RequestMapping(value = "v1/patient", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deletePatient(@RequestBody String request) {
        LOGGER.debug("Delete patient request received " + request);
        Patient patient = FhirUtil.getPatientFromRequest(request);
        Client client = getClientWithEventAccessCheck(EventEnum.DELETE_PATIENT.getEventName());
        episodeService.deleteEpisode(patient.getIdElement().getIdPartAsLong(), client.getAccessibleClient());
        return new ResponseEntity<>(null, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "v1/patients", method = RequestMethod.POST)
    public ResponseEntity<Response<List<Episode>>> createMultiplePatients(@RequestBody String request) {
        Client client = getClientWithEventAccessCheck(EventEnum.ADD_MULTIPLE_PATIENTS.getEventName());
        List<Patient> patientList = pmirService.getPatientList(request);
        List<Episode> episodeList = pmirService.addUpdateMultiplePatients(patientList, client.getAccessibleClient(), false);
        return new ResponseEntity<>(new Response<>(true, episodeList), HttpStatus.CREATED);
    }

    @RequestMapping(value = "v1/patients", method = RequestMethod.PUT)
    public ResponseEntity<Response<List<Episode>>> updateMultiplePatients(@RequestBody String request) {
        Client client = getClientWithEventAccessCheck(EventEnum.UPDATE_MULTIPLE_PATIENTS.getEventName());
        List<Patient> patientList = pmirService.getPatientList(request);
        List<Episode> episodeList = pmirService.addUpdateMultiplePatients(patientList, client.getAccessibleClient(), true);
        return new ResponseEntity<>(new Response<>(true, episodeList), HttpStatus.OK);
    }
}
package com.everwell.datagateway.handlers.impl.publishers;

import com.everwell.datagateway.enums.AuthenticationCredentialTypeEnum;
import com.everwell.datagateway.exceptions.ValidationException;
import com.everwell.datagateway.handlers.AuthenticationHandler;
import com.everwell.datagateway.models.dto.BasicAuthCredDto;
import com.everwell.datagateway.utils.EncryptUtil;
import com.everwell.datagateway.utils.Utils;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

@Component
public class BasicAuthenticationHandler extends AuthenticationHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(BasicAuthenticationHandler.class);

    @Override
    public AuthenticationCredentialTypeEnum getEventEnumerator() {
        return AuthenticationCredentialTypeEnum.BASIC_AUTH;
    }

    @Override
    public String encryptSensitiveDetailsInObject(String credentials) throws Exception {
        BasicAuthCredDto basicAuthCredentials = Utils.convertStrToObject(credentials, BasicAuthCredDto.class);
        String encryptedPassword  = EncryptUtil.encrypt(basicAuthCredentials.getPassword(), basicAuthCredentials.getUsername());
        basicAuthCredentials.setPassword(encryptedPassword);
        return Utils.asJsonString(basicAuthCredentials);
    }

    @Override
    public HttpHeaders setUpAuthentication(String credentials, String credentialType) {
        HttpHeaders httpHeaders = new HttpHeaders();
        try {
            BasicAuthCredDto basicAuthCredentials = Utils.convertStrToObject(credentials, BasicAuthCredDto.class);
            String password =  EncryptUtil.decrypt(basicAuthCredentials.getPassword(), basicAuthCredentials.getUsername());
            httpHeaders.setBasicAuth(basicAuthCredentials.getUsername(), password);
        }catch (Exception ex) {
            LOGGER.error("[setUpBasicAuthOnUrl] unable to set Basic Authentication");
            throw new ValidationException("Unable to set up Basic Authentication on Subscriber Url " + ex);
        }
        return httpHeaders;
    }

    @Override
    public void validateCredentialDto(String credentials) throws JsonProcessingException {
        BasicAuthCredDto basicAuthCredentials = Utils.convertStrToObject(credentials, BasicAuthCredDto.class);
        basicAuthCredentials.validateBasicAuthCredDto(basicAuthCredentials.getUsername(), basicAuthCredentials.getPassword());
    }
}

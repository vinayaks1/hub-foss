package com.everwell.datagateway.handlers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Service
public class AuthenticationHandlerMap {
    private final Map<String, AuthenticationHandler> authenticationMap = new HashMap<>();

    @Autowired
    public void setAuthenticationMap(List<AuthenticationHandler> authenticationHandlerListList) {
        if(!CollectionUtils.isEmpty(authenticationHandlerListList)){
            for(AuthenticationHandler authenticationBase : authenticationHandlerListList) {
                authenticationMap.put(authenticationBase.getEventEnumerator().toString(), authenticationBase);
            }
        }
    }

    public AuthenticationHandler getAuthentication(String credentialType)
    {
        return authenticationMap.get(credentialType);
    }
}


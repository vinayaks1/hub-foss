package com.everwell.datagateway.handlers;

import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ResponseHandlerMap {

    private final Map<String, ResponseHandler> responseHandlerMap= new HashMap<>();

    public ResponseHandlerMap(List<ResponseHandler> responseHandlerList) {
        if(!CollectionUtils.isEmpty(responseHandlerList)){
            for(ResponseHandler handler : responseHandlerList) {
                responseHandlerMap.put(handler.getResponseEnumerator().toString(),handler);
            }
        }
    }

    public ResponseHandler getResponseHandler(String responseType) {
        return  responseHandlerMap.get(responseType);
    }
}

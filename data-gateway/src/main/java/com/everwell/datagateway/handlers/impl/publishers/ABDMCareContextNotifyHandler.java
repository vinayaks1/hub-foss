package com.everwell.datagateway.handlers.impl.publishers;

import com.everwell.datagateway.enums.EventEnum;
import com.everwell.datagateway.handlers.EventHandler;

public class ABDMCareContextNotifyHandler extends EventHandler {
    @Override
    public EventEnum getEventEnumerator()
    {
        return EventEnum.ABDM_CARE_CONTEXT_NOTIFY;
    }
}

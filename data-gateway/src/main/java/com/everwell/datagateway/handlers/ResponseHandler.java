package com.everwell.datagateway.handlers;

import com.everwell.datagateway.enums.ResponseTypeEnum;
import com.everwell.datagateway.models.response.ReferenceIdRMQResponse;
import com.everwell.datagateway.models.response.Response;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.io.IOException;

public abstract class ResponseHandler {

    public abstract ResponseTypeEnum getResponseEnumerator();

    public abstract String convertToResponseType(Response<ReferenceIdRMQResponse> response) throws Exception;
}

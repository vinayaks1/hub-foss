package com.everwell.datagateway.handlers;

import com.everwell.datagateway.enums.AuthenticationCredentialTypeEnum;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.HttpHeaders;

public abstract class AuthenticationHandler {

    public abstract  AuthenticationCredentialTypeEnum getEventEnumerator();

    public abstract String encryptSensitiveDetailsInObject(String credentials) throws Exception;

    public abstract HttpHeaders setUpAuthentication(String credentials, String credentialType);

    public abstract void validateCredentialDto(String credentials) throws JsonProcessingException;
}

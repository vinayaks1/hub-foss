package com.everwell.datagateway.handlers;

import com.everwell.datagateway.entities.Event;
import com.everwell.datagateway.enums.EventEnum;
import com.everwell.datagateway.repositories.EventRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class EventHandlerMap {

    private static Logger LOGGER = LoggerFactory.getLogger(EventHandlerMap.class);

    private Map<String, EventHandler> stringEventHandlerHashMap = new HashMap<>();
    private Map<EventEnum, EventHandler> enumEventHandlerHashMap = new HashMap<>();

    @Autowired
    private EventRepository eventRepository;


    @Autowired
    public EventHandlerMap(List<EventHandler> eventHandlerList) {
        if (!CollectionUtils.isEmpty(eventHandlerList)) {
            for (EventHandler eventHandler : eventHandlerList) {
                enumEventHandlerHashMap.put(eventHandler.getEventEnumerator(), eventHandler);
            }
        }
    }


    @PostConstruct
    public void initialise() {
        LOGGER.debug("Initialising stringEventHandlerHashMap Map");
        for (Event event : eventRepository.findAll()) {
            EventEnum matchedEnum = EventEnum.getMatch(event.getEventName());
            EventHandler handler;
            if(null == matchedEnum) {
               LOGGER.error("No match found while initialising stringEventHandlerHashMap for " + event.getEventName());
               handler = enumEventHandlerHashMap.get(EventEnum.GENERIC_PUBLISH);
            }
            else {
                handler = enumEventHandlerHashMap.get(matchedEnum);
            }
            stringEventHandlerHashMap.put(event.getEventName(), handler);
        }
    }

    public EventHandler getHandler(String eventName) {
        return stringEventHandlerHashMap.get(eventName);
    }

    
}

package com.everwell.datagateway.handlers.impl.publishers;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.validation.FhirValidator;
import ca.uhn.fhir.validation.SingleValidationMessage;
import ca.uhn.fhir.validation.ValidationResult;
import com.everwell.datagateway.enums.FHIRStructureEnum;
import com.everwell.datagateway.enums.ResponseTypeEnum;
import com.everwell.datagateway.handlers.ResponseHandler;
import com.everwell.datagateway.models.dto.CommonFHIRDto;
import com.everwell.datagateway.models.dto.DiagnosticsFHIRDto;
import com.everwell.datagateway.models.dto.DispensationFHIRDto;
import com.everwell.datagateway.models.response.ReferenceIdRMQResponse;
import com.everwell.datagateway.models.response.Response;
import com.everwell.datagateway.utils.FhirUtil;
import com.everwell.datagateway.utils.Utils;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.hl7.fhir.r4.model.Bundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class RefIdFHIRHandler extends ResponseHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(RefIdFHIRHandler.class);

    @Value("${fhir.profiles.path}")
    private  String fhirFolderPath;

    @Override
    public ResponseTypeEnum getResponseEnumerator() {
        return ResponseTypeEnum.FHIR_RESPONSE;
    }

    @Override
    public String convertToResponseType(Response<ReferenceIdRMQResponse> response) throws IOException {
        ReferenceIdRMQResponse referenceIdRMQResponse = response.getData();
        CommonFHIRDto commonFHIRDto = Utils.convertStrToObject(referenceIdRMQResponse.getResponse(),CommonFHIRDto.class);
        LOGGER.info(Utils.asJsonString(commonFHIRDto));
        String serializedResponse = null;
        switch (FHIRStructureEnum.getEnumByName(commonFHIRDto.getProfileType())) {
            case DIAGNOSTICS:
                serializedResponse = handleDiagnosticsFhir(commonFHIRDto.getResponse());
                break;
            case DISPENSATION:
                serializedResponse = handleDispensationFhir(commonFHIRDto.getResponse());
        }
        referenceIdRMQResponse.setResponse(serializedResponse);
        response.setData(referenceIdRMQResponse);
        LOGGER.info(Utils.asJsonString(response));
        return Utils.asJsonString(response);
    }

    private String handleDiagnosticsFhir(String diagnosticsData) throws IOException {
        DiagnosticsFHIRDto diagnosticsFHIRDto = Utils.convertStrToObject(diagnosticsData,DiagnosticsFHIRDto.class);
        List<Bundle> diagnosticBundles = FhirUtil.populateDiagnosticReportLabBundle(diagnosticsFHIRDto);
        FhirContext ctx = FhirContext.forR4();
        FhirValidator validator = FhirUtil.loadProfiles(ctx,fhirFolderPath);
        List<String> response = new ArrayList<>();
        for(Bundle bundle : diagnosticBundles) {
            ValidationResult result = FhirUtil.validate(validator,bundle);
            if(result.isSuccessful()) {
                IParser parser = ctx.newJsonParser();
                String serializeBundle = parser.encodeResourceToString(bundle);
                response.add(serializeBundle);
            }
            else {
                for (SingleValidationMessage next : result.getMessages()) {
                    LOGGER.info(next.getSeverity().name() + " : " + next.getLocationString() + " " + next.getMessage());
                }
                response.add(null);
            }
        }
        return Utils.asJsonString(response);
    }

    private String handleDispensationFhir(String dispensationData) throws JsonProcessingException, FileNotFoundException {
        DispensationFHIRDto dispensationFHIRDto = Utils.convertStrToObject(dispensationData, DispensationFHIRDto.class);
        Bundle dispensationBundle = FhirUtil.populateDispensationBundle(dispensationFHIRDto);
        FhirContext ctx = FhirContext.forR4();
        FhirValidator validator = FhirUtil.loadProfiles(ctx,fhirFolderPath);
        String response = null;
        ValidationResult result = FhirUtil.validate(validator,dispensationBundle);
        if (result.isSuccessful()) {
            IParser parser = ctx.newJsonParser();
            response = parser.encodeResourceToString(dispensationBundle);
        }
        else {
            for (SingleValidationMessage next : result.getMessages()) {
                LOGGER.info(next.getSeverity().name() + " : " + next.getLocationString() + " " + next.getMessage());
            }
        }
        return response;
    }
}

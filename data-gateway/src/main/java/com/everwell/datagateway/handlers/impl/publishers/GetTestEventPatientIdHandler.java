package com.everwell.datagateway.handlers.impl.publishers;

import com.everwell.datagateway.enums.EventEnum;
import com.everwell.datagateway.handlers.EventHandler;
import org.springframework.stereotype.Component;

@Component
public class GetTestEventPatientIdHandler extends EventHandler {


    @Override
    public EventEnum getEventEnumerator() {
        return EventEnum.GET_TEST_PATIENT_ID;
    }

}

package com.everwell.datagateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
@EnableScheduling
@EnableZuulProxy
public class DataGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(DataGatewayApplication.class, args);
    }

}

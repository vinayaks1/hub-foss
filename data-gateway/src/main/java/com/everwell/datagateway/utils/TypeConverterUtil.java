package com.everwell.datagateway.utils;

import com.fasterxml.jackson.databind.ObjectMapper;

public class TypeConverterUtil {

    private static ObjectMapper objectMapper = new ObjectMapper();

    public static String byteArrayToString(byte[] byteArray) {
        return new String(byteArray);
    }

    public static String convertToJsonString(Object obj) {
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

}

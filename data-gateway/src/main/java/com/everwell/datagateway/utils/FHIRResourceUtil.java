package com.everwell.datagateway.utils;


import com.everwell.datagateway.enums.FHIRProfileEnum;;
import com.everwell.datagateway.models.dto.SharedResourceDto;
import org.hl7.fhir.r4.model.*;
import org.hl7.fhir.r4.model.DiagnosticReport.DiagnosticReportStatus;
import org.hl7.fhir.r4.model.MedicationRequest.MedicationRequestIntent;
import org.hl7.fhir.r4.model.MedicationRequest.MedicationRequestStatus;
import org.hl7.fhir.r4.model.Observation.ObservationStatus;

import java.util.Collections;

import static com.everwell.datagateway.constants.Constants.FHIR_IDENTIFIER_SYSTEM_URL;

/**
 * The FhirResourcePopulator class populates all the FHIR resources
 */
public class FHIRResourceUtil {

	public static Resource populateResource(FHIRProfileEnum profile, SharedResourceDto sharedResourceDto){

		switch (profile){

			case PATIENT: {
				Patient patient = new Patient();
				patient.setId(String.valueOf(sharedResourceDto.episodeId));
				patient.addIdentifier()
						.setType(new CodeableConcept().setText(profile.getIdentifier()))
						.setValue(String.valueOf(sharedResourceDto.episodeId));
				return patient;
			}
			case PRACTITIONER: {
				Practitioner practitioner = new Practitioner();
				practitioner.setId(String.valueOf(sharedResourceDto.userId));
				practitioner.addIdentifier()
						.setType(new CodeableConcept().setText(profile.getIdentifier())).setValue(String.valueOf(sharedResourceDto.userId));
				return practitioner;
			}
			case DIAGNOSTIC_REPORT: {
				DiagnosticReport diagnosticReportLab = new DiagnosticReport();
				diagnosticReportLab.setId(sharedResourceDto.diagnosticsEntry.getKey());
				diagnosticReportLab.setStatus(DiagnosticReportStatus.FINAL);
				diagnosticReportLab.setCategory(Collections.singletonList(new CodeableConcept().setCoding(Collections.singletonList(new Coding().setDisplay(sharedResourceDto.diagnosticsEntry.getValue().getTestType())))));
				diagnosticReportLab.setCode(
						new CodeableConcept().setText(sharedResourceDto.diagnosticsEntry.getValue().getTestType()));
				diagnosticReportLab.getResultsInterpreter()
						.add(new Reference().setReference(FHIRProfileEnum.PRACTITIONER.getReferenceText() + sharedResourceDto.userId));
				diagnosticReportLab.setConclusion(sharedResourceDto.diagnosticsEntry.getValue().getFinalInterpretation());
				diagnosticReportLab.addResult(new Reference().setReference(FHIRProfileEnum.OBSERVATION.getReferenceText() + sharedResourceDto.diagnosticsEntry.getKey()));
				return diagnosticReportLab;
			}
			case MEDICATION_REQUEST: {
				MedicationRequest medicationRequest = new MedicationRequest();
				medicationRequest.setId(sharedResourceDto.dispensationEntry.getKey());
				medicationRequest.setStatus(sharedResourceDto.dispensationEntry.getValue().isActive() ? MedicationRequestStatus.ACTIVE : MedicationRequestStatus.COMPLETED);
				medicationRequest.setIntent(MedicationRequestIntent.ORIGINALORDER);
				medicationRequest.setMedication(new CodeableConcept().setText(sharedResourceDto.dispensationEntry.getValue().getProductName()));
				medicationRequest.setSubject(new Reference().setReference(FHIRProfileEnum.PATIENT.getReferenceText()+sharedResourceDto.episodeId));
				medicationRequest.setAuthoredOnElement(new DateTimeType(sharedResourceDto.dispensationEntry.getValue().getTimeStamp()));
				medicationRequest
						.setRequester(new Reference().setReference(FHIRProfileEnum.ORGANIZATION.getReferenceText()+sharedResourceDto.hierarchyId));
				medicationRequest.addDosageInstruction(new Dosage().setText("NA"));
				return medicationRequest;
			}

			case OBSERVATION: {
				Observation observation = new Observation();
				observation.setId(sharedResourceDto.diagnosticsEntry.getKey());
				observation.setStatus(ObservationStatus.FINAL);
				observation.setCode(new CodeableConcept()
						.setText(sharedResourceDto.diagnosticsEntry.getValue().getFinalInterpretation()));
				observation.setSubject(new Reference().setReference(FHIRProfileEnum.PATIENT.getReferenceText()+sharedResourceDto.getEpisodeId()));
				observation.addPerformer().setReference(FHIRProfileEnum.PRACTITIONER.getReferenceText()+sharedResourceDto.getUserId());
				return observation;
			}

			case ORGANIZATION: {
				Organization organization = new Organization();
				organization.setId(String.valueOf(sharedResourceDto.hierarchyId));
				organization.addIdentifier()
						.setType(new CodeableConcept().setText(profile.getIdentifier()))
						.setSystem(FHIR_IDENTIFIER_SYSTEM_URL)
						.setValue(String.valueOf(sharedResourceDto.hierarchyId));
				organization.setName(sharedResourceDto.hierarchyName);
				return organization;
			}
		}

		return null;
	}
}

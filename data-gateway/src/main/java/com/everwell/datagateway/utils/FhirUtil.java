package com.everwell.datagateway.utils;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.context.support.DefaultProfileValidationSupport;
import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.validation.FhirValidator;
import ca.uhn.fhir.validation.ValidationResult;
import com.everwell.datagateway.constants.Constants;
import com.everwell.datagateway.enums.FHIRProfileEnum;
import com.everwell.datagateway.enums.FHIRStructureEnum;
import com.everwell.datagateway.enums.FhirMaritalStatusEnum;
import com.everwell.datagateway.models.dto.DiagnosticsFHIRDto;
import com.everwell.datagateway.models.dto.DiagnosticsFHIRDto.TestDetails;
import com.everwell.datagateway.models.dto.DispensationFHIRDto;
import com.everwell.datagateway.models.dto.DispensationFHIRDto.DispensationDetails;
import com.everwell.datagateway.models.dto.SharedResourceDto;
import com.everwell.datagateway.models.request.EpisodeSearchRequest;
import com.everwell.datagateway.models.response.EpisodeIndex;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.sentry.Sentry;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.commons.lang.StringUtils;
import org.hl7.fhir.common.hapi.validation.support.*;
import org.hl7.fhir.common.hapi.validation.validator.FhirInstanceValidator;
import org.hl7.fhir.r4.model.*;
import org.hl7.fhir.r4.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.r4.model.Bundle.BundleType;
import org.hl7.fhir.r4.model.Composition.SectionComponent;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static com.everwell.datagateway.constants.Constants.*;

public class FhirUtil {

    static ZoneId z = ZoneId.of(FHIR_TIMEZONE ) ;
    static ZonedDateTime zdt = ZonedDateTime.now( z ) ;

    public static ValidationResult validate(FhirValidator validator,Bundle bundle) {
        return validator.validateWithResult(bundle);
    }


    public static FhirValidator loadProfiles(FhirContext ctx,String fhirFolderPath) throws FileNotFoundException {

        IParser parser = ctx.newXmlParser();
        ValidationSupportChain supportChain = new ValidationSupportChain();

        DefaultProfileValidationSupport defaultSupport = new DefaultProfileValidationSupport(ctx);

        PrePopulatedValidationSupport prePopulatedSupport = new PrePopulatedValidationSupport(ctx);
        StructureDefinition sd ;

        // Read all Profile Structure Definitions
        String[] fileList = new File(fhirFolderPath).list(new WildcardFileFilter("*.xml"));
        if (fileList != null) {
            for(String file:fileList)
            {
                //Parse All Profiles and add to prepopulated support
                sd = parser.parseResource(StructureDefinition.class, new FileReader(fhirFolderPath+file));
                prePopulatedSupport.addStructureDefinition(sd);
            }
        }

        //Add Snapshot Generation Support
        SnapshotGeneratingValidationSupport snapshotGenerator = new SnapshotGeneratingValidationSupport(ctx);

        //Add prepopulated support consisting all structure definitions and Terminology support
        supportChain.addValidationSupport(defaultSupport);
        supportChain.addValidationSupport(prePopulatedSupport);
        supportChain.addValidationSupport(snapshotGenerator);
        supportChain.addValidationSupport(new InMemoryTerminologyServerValidationSupport(ctx));
        supportChain.addValidationSupport(new CommonCodeSystemsTerminologyService(ctx));

        // Create a validator using the FhirInstanceValidator module and register.
        FhirInstanceValidator instanceValidator = new FhirInstanceValidator(supportChain);
        return ctx.newValidator().registerValidatorModule(instanceValidator);
    }

    // Populate Composition for DiagnosticReport Lab
    static Composition populateDiagnosticReportRecordLabCompositionResource(int episodeId, int hierarchyId,int userId, Map.Entry<String,TestDetails> entry)
    {
        Composition composition = new Composition();

        // Set logical id of this artifact
        composition.setId(FHIRProfileEnum.COMPOSITION.getReferenceText() +entry.getKey());

        // Set language of the resource content
        composition.setLanguage(FHIR_LANGUGAGE_CODE);
        // Status can be preliminary | final | amended | entered-in-error
        composition.setStatus(Composition.CompositionStatus.FINAL);

        // Kind of composition ("Diagnostic studies report")
        CodeableConcept type = composition.getType();
        type.addCoding(new Coding(FHIRStructureEnum.DIAGNOSTICS.getCodingSystem(), FHIRStructureEnum.DIAGNOSTICS.getCode(), FHIRStructureEnum.DIAGNOSTICS.getTitle()));
        type.setText(FHIRStructureEnum.DIAGNOSTICS.getTitle());

        // Set subject - Who and/or what the composition/DiagnosticReport record is about
        composition.setSubject(new Reference().setReference(FHIRProfileEnum.PATIENT.getReferenceText() + episodeId));

        // Set Timestamp
        composition.setDateElement(new DateTimeType(StringUtils.substringBefore(zdt.toString(),"[")));

        // Set author - Who and/or what authored the composition/DiagnosticReport record
        composition.addAuthor(new Reference().setReference(FHIRProfileEnum.PRACTITIONER.getReferenceText()+ userId));

        // Set a Human Readable name/title
        composition.setTitle(FHIRStructureEnum.DIAGNOSTICS.getTitle());

        // Composition is broken into sections / DiagnosticReport Lab record contains single section to define the relevant medication requests
        // Entry is a reference to data that supports this section
        Reference reference1 = new Reference();
        reference1.setReference(FHIRProfileEnum.DIAGNOSTIC_REPORT.getReferenceText() + entry.getKey());
        reference1.setType(FHIRProfileEnum.DIAGNOSTIC_REPORT.getName());

        SectionComponent section = new SectionComponent();
        section.addEntry(reference1);
        composition.addSection(section);

        return composition;
    }

    public static List<Bundle> populateDiagnosticReportLabBundle(DiagnosticsFHIRDto diagnosticsFHIRDto)
    {

        List<Bundle> diagnosticBundles = new ArrayList<>();

        for(Map.Entry<String, TestDetails> entry : diagnosticsFHIRDto.getTestResultMap().entrySet()){

            Bundle diagnosticReportBundle = new Bundle();

            SharedResourceDto sharedResourceDto = new SharedResourceDto(entry,diagnosticsFHIRDto);
            // Set logical id of this artifact
            diagnosticReportBundle.setId(FHIRProfileEnum.DIAGNOSTIC_REPORT.getReferenceText() + entry.getKey());

            // Set version-independent identifier for the Composition
            Identifier identifier = diagnosticReportBundle.getIdentifier();
            identifier.setValue(FHIRStructureEnum.DIAGNOSTICS.getValue());
            identifier.setSystem(FHIR_IDENTIFIER_SYSTEM_URL);

            // Set Bundle Type
            diagnosticReportBundle.setType(BundleType.DOCUMENT);

            // Set Timestamp
            diagnosticReportBundle.setTimestampElement(new InstantType(StringUtils.substringBefore(zdt.toString(),"[")));

            // Add resources entries for bundle with Full URL
            List<BundleEntryComponent> listBundleEntries = diagnosticReportBundle.getEntry();

            List<FHIRProfileEnum> entryList = Arrays.asList(FHIRProfileEnum.PATIENT,FHIRProfileEnum.PRACTITIONER,FHIRProfileEnum.DIAGNOSTIC_REPORT,FHIRProfileEnum.OBSERVATION);

            BundleEntryComponent composition = new Bundle.BundleEntryComponent();
            composition.setFullUrl(FHIRProfileEnum.COMPOSITION.getReferenceText() + diagnosticsFHIRDto.getEpisodeId());
            composition.setResource(populateDiagnosticReportRecordLabCompositionResource(diagnosticsFHIRDto.getEpisodeId(),diagnosticsFHIRDto.getHierarchyId(),diagnosticsFHIRDto.getUserId(),entry));
            listBundleEntries.add(composition);

            for(FHIRProfileEnum fpe : entryList) {
                BundleEntryComponent bundleEntry = new Bundle.BundleEntryComponent();
                bundleEntry.setFullUrl(fpe.getReferenceText()+sharedResourceDto.profileReferenceMap.get(fpe.getKey()));
                bundleEntry.setResource(FHIRResourceUtil.populateResource(fpe,sharedResourceDto));
                listBundleEntries.add(bundleEntry);
            }

            diagnosticBundles.add(diagnosticReportBundle);
        }

        return diagnosticBundles;
    }

    static Composition populatePrescriptionCompositionResource(DispensationFHIRDto dispensationFHIRDto)
    {
        Composition composition = new Composition();

        // Set logical id of this artifact
        composition.setId(FHIRProfileEnum.COMPOSITION.getReferenceText()+dispensationFHIRDto.getEpisodeId());

        // Set language of the resource content
        composition.setLanguage(FHIR_LANGUGAGE_CODE);

        // Status can be preliminary | final | amended | entered-in-error
        composition.setStatus(Composition.CompositionStatus.FINAL);

        // Kind of composition ("Prescription record ")
        composition.setType(new CodeableConcept(new Coding(FHIRStructureEnum.DISPENSATION.getCodingSystem(), FHIRStructureEnum.DISPENSATION.getCode(), FHIRStructureEnum.DISPENSATION.getTitle())));

        // Set subject - Who and/or what the composition/Prescription record is about
        composition.setSubject(new Reference().setReference(FHIRProfileEnum.PATIENT.getReferenceText()+dispensationFHIRDto.getEpisodeId()));

        // Set Timestamp
        composition.setDateElement(new DateTimeType(StringUtils.substringBefore(zdt.toString(),"[")));

        // Set author - Who and/or what authored the composition/Presciption record
        composition.addAuthor(new Reference().setReference(FHIRProfileEnum.ORGANIZATION.getReferenceText()+dispensationFHIRDto.getHierarchyId()));

        // Set a Human Readable name/title
        composition.setTitle(FHIRStructureEnum.DISPENSATION.getTitle());

        // Composition is broken into sections / Prescription record contains single section to define the relevant medication requests
        // Entry is a reference to data that supports this section

        SectionComponent section = new SectionComponent();

        for(Map.Entry<String, DispensationDetails> entry : dispensationFHIRDto.dispensationMap.entrySet()){
            Reference reference = new Reference();
            reference.setReference(FHIRProfileEnum.MEDICATION_REQUEST.getReferenceText()+ entry.getKey());
            reference.setType(FHIRProfileEnum.MEDICATION_REQUEST.getName());
            section.addEntry(reference);
        }

        composition.addSection(section);

        return composition;
    }

    public static Bundle populateDispensationBundle(DispensationFHIRDto dispensationFHIRDto)
    {
        Bundle prescriptionBundle = new Bundle();

        // Set logical id of this artifact
        prescriptionBundle.setId(FHIRProfileEnum.MEDICATION_REQUEST.getIdentifier()+dispensationFHIRDto.getEpisodeId());

        SharedResourceDto sharedResourceDto = new SharedResourceDto(dispensationFHIRDto);

        // Set version-independent identifier for the Bundle
        Identifier identifier = prescriptionBundle.getIdentifier();
        identifier.setValue(FHIRStructureEnum.DISPENSATION.getValue());
        identifier.setSystem(FHIR_IDENTIFIER_SYSTEM_URL);

        // Set Bundle Type
        prescriptionBundle.setType(BundleType.DOCUMENT);

        // Set Timestamp
        prescriptionBundle.setTimestampElement(new InstantType(StringUtils.substringBefore(zdt.toString(),"[")));

        List<FHIRProfileEnum> entryList = Arrays.asList(FHIRProfileEnum.PATIENT,FHIRProfileEnum.ORGANIZATION);

        // Add resources entries for bundle with Full URL
        List<BundleEntryComponent> listBundleEntries = prescriptionBundle.getEntry();

        BundleEntryComponent composition = new BundleEntryComponent();
        composition.setFullUrl(FHIRProfileEnum.COMPOSITION.getIdentifier()+sharedResourceDto.episodeId);
        composition.setResource(populatePrescriptionCompositionResource(dispensationFHIRDto));
        listBundleEntries.add(composition);

        for(FHIRProfileEnum fpe : entryList) {
            BundleEntryComponent bundleEntry = new Bundle.BundleEntryComponent();
            bundleEntry.setFullUrl(fpe.getReferenceText()+sharedResourceDto.profileReferenceMap.get(fpe.getKey()));
            bundleEntry.setResource(FHIRResourceUtil.populateResource(fpe,sharedResourceDto));
            listBundleEntries.add(bundleEntry);
        }

        for(Map.Entry<String,DispensationDetails> entry : dispensationFHIRDto.getDispensationMap().entrySet()) {
            sharedResourceDto.setDispensationEntry(entry);
            BundleEntryComponent bundleEntry = new BundleEntryComponent();
            bundleEntry.setFullUrl(FHIRProfileEnum.MEDICATION_REQUEST.getReferenceText()+ entry.getKey());
            bundleEntry.setResource(FHIRResourceUtil.populateResource(FHIRProfileEnum.MEDICATION_REQUEST,sharedResourceDto));
            listBundleEntries.add(bundleEntry);
        }

        return prescriptionBundle;
    }

    public static List<Patient> populatePatientResource(List<EpisodeIndex> episodeIndexList) {
        List<Patient> patientList = new ArrayList<>();
        episodeIndexList.forEach( e -> {
            Patient patient = new Patient();
            patient.setId(e.getId());
            patient.setActive(!e.getDeleted());
            patient.addName().setFamily(null != e.getStageData().get(LAST_NAME) ?  e.getStageData().get(LAST_NAME).toString() : null).addGiven(e.getStageData().get(FIRST_NAME).toString());
            if(null != e.getStageData().get(GENDER)) {
                String genderString = e.getStageData().get(GENDER).toString().toLowerCase();
                patient.setGender(Enumerations.AdministrativeGender.fromCode(genderString));
            }
            patient.setTelecom(getPatientTelecomDetails(e.getStageData()));
            patient.setAddress(Collections.singletonList(getPatientAddress(e.getStageData())));
            patient.setDeceased((null != e.getStageData().get(TREATMENT_OUTCOME) && e.getStageData().get(TREATMENT_OUTCOME).toString().equals(DIED)) ? new BooleanType(true) : new BooleanType(false));
            if(null != e.getStageData().get(DATE_OF_BIRTH)) {
                patient.setBirthDate(new Date(Long.parseLong(e.getStageData().get(DATE_OF_BIRTH).toString())));
            }
            if(null != e.getStageData().get(MARITAL_STATUS)) {
                String maritalStatusCode = FhirMaritalStatusEnum.getCodeByValue(e.getStageData().get(MARITAL_STATUS).toString());
                CodeableConcept maritalStatus = new CodeableConcept();
                maritalStatus.addCoding(new Coding().setCode(maritalStatusCode).setDisplay(e.getStageData().get(MARITAL_STATUS).toString()));
                patient.setMaritalStatus(maritalStatus);
            }
            if(null != e.getAssociations() && null != e.getAssociations().get(EXTERNAL_ID1)) {
                Identifier identifier = new Identifier();
                identifier.setValue(e.getAssociations().get(EXTERNAL_ID1).toString());
                patient.setIdentifier(Collections.singletonList(identifier));
            }
            patient.setContact(getPatientContactDetails(e.getStageData()));
            patientList.add(patient);
        });
        return patientList;
    }

    public static JsonNode populatePatientBundle(List<Patient> patientList, HttpServletRequest request, Long size, EpisodeSearchRequest episodeSearchRequest) {
        int page = episodeSearchRequest.getPage();
        int pageSize = episodeSearchRequest.getSize();
        FhirContext fhirContext = FhirContext.forR4();
        Bundle bundle = new Bundle();
        bundle.setType(Bundle.BundleType.SEARCHSET);
        bundle.setTotal(size.intValue());
        if(bundle.getTotal() > page * pageSize && bundle.getTotal() > page + pageSize && null != request) {
            Bundle.BundleLinkComponent nextLink = new Bundle.BundleLinkComponent();
            nextLink.setRelation(NEXT);
            nextLink.setUrl(generateNextPageLink(request, page));
            bundle.getLink().add(nextLink);
        }
        patientList.forEach( p -> {
            bundle.addEntry().setResource(p);
        });
        String responseString = fhirContext.newJsonParser().encodeResourceToString(bundle);
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            JsonNode jsonNode = objectMapper.readTree(responseString);
            return jsonNode;
        } catch (Exception e) {
            Sentry.capture(e);
        }
        return null;
    }

    public static String generateNextPageLink(HttpServletRequest request, int page) {
        String queryString = request.getQueryString();
        String updatedQueryString = queryString.replaceAll("(^|&)\\_page=[^&]*", "$1_page=" + (page + 1)); // increase page number by 1 for next page link
        String nextPageUrl = request.getRequestURL() + Constants.QUESTION_MARK + updatedQueryString;
        return nextPageUrl;
    }

    private static List<ContactPoint> getPatientTelecomDetails(Map<String, Object> stageData) {
        List<ContactPoint> telecom = new ArrayList<>();
        ContactPoint telecomPhone = new ContactPoint()
                .setSystem(ContactPoint.ContactPointSystem.PHONE)
                .setValue(Objects.toString(stageData.get(PRIMARY_PHONE_NUMBER), null))
                .setUse(ContactPoint.ContactPointUse.MOBILE);
        telecom.add(telecomPhone);
        List<Map<String, String>> userEmailList = (List<Map<String, String>>) stageData.get(EMAIL_LIST);
        if (userEmailList != null && !userEmailList.isEmpty()) {
            ContactPoint telecomEmail = new ContactPoint()
                    .setSystem(ContactPoint.ContactPointSystem.EMAIL)
                    .setValue(userEmailList.get(0).get(EMAIL_ID));
            telecom.add(telecomEmail);
        }
        return  telecom;
    }

    private static List<Patient.ContactComponent> getPatientContactDetails(Map<String, Object> stageData) {
        String contactPersonName = (String) stageData.get(CONTACT_PERSON_NAME);
        String contactPersonAddress = (String) stageData.get(CONTACT_PERSON_ADDRESS);
        String contactPersonPhone = (String) stageData.get(CONTACT_PERSON_PHONE);
        Patient.ContactComponent contact = new Patient.ContactComponent();
        if (contactPersonName != null) {
            List<String> contactNameCombined = Arrays.asList(contactPersonName.split(" "));
            String firstName = String.join(" ", contactNameCombined.subList(0, contactNameCombined.size() - 1));
            String lastName = contactNameCombined.get(contactNameCombined.size() - 1);
            contact.getName().addGiven(firstName);
            contact.getName().setFamily(lastName);
        }
        if (contactPersonAddress != null) {
            contact.getAddress().setText(contactPersonAddress);
        }
        if (contactPersonPhone != null) {
            ContactPoint patientContactMobile = new ContactPoint();
            patientContactMobile.setValue(contactPersonPhone);
            patientContactMobile.setSystem(ContactPoint.ContactPointSystem.PHONE);
            contact.setTelecom(Collections.singletonList(patientContactMobile));
        }
        return Collections.singletonList(contact);
    }

    private static Address getPatientAddress(Map<String, Object> stageData) {
        Address address = new Address();
        address.addLine(Objects.toString(stageData.get(ADDRESS), null));
        address.setCity(Objects.toString(stageData.get(CITY), null));
        address.setPostalCode(Objects.toString(stageData.get(PIN_CODE), null));
        return address;
    }

    public static Patient getPatientFromRequest(String request) {
        FhirContext fhirContext = FhirContext.forR4();
        IParser parser = fhirContext.newJsonParser();
        Patient patient = parser.parseResource(Patient.class, request);
        return patient;
    }

    public static String getStringFromList(List<StringType> stringTypeList) {
        if(null == stringTypeList) {
            return null;
        }
        return stringTypeList.stream().map(StringType::toString).collect(Collectors.joining(" "));
    }

    public static Bundle getBundleFromRequest(String request) {
        FhirContext fhirContext = new FhirContext();
        IParser parser = fhirContext.newJsonParser();
        return parser.parseResource(Bundle.class, request);
    }

    public static List<Patient> getPatientListFromBundle(Bundle bundle) {
        List<Patient> patientList = new ArrayList<>();
        for (Bundle.BundleEntryComponent entry : bundle.getEntry()) {
            Resource resource = entry.getResource();
            if (null != resource.getResourceType() && Constants.BUNDLE.equals(resource.getResourceType().name())) {
                Bundle nestedBundle = (Bundle) resource;
                for (Bundle.BundleEntryComponent nestedEntry : nestedBundle.getEntry()) {
                    Resource nestedResource = nestedEntry.getResource();
                    if (null != nestedResource.getResourceType() && Constants.PATIENT.equals(nestedResource.getResourceType().name())) {
                        try {
                        Patient patient = (Patient) nestedResource;
                        patientList.add(patient);
                        } catch (Exception exception) {
                            Sentry.capture(exception);
                        }
                    }
                }
            }
        }
        return patientList;
    }
}

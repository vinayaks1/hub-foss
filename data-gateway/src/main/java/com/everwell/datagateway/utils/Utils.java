package com.everwell.datagateway.utils;

import com.everwell.datagateway.constants.Constants;
import com.everwell.datagateway.exceptions.ValidationException;
import com.everwell.datagateway.filters.zuul.httpServlet.GenericHttpServletRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.zuul.context.RequestContext;
import io.sentry.Sentry;
import lombok.Getter;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.zip.GZIPInputStream;

public class Utils {
    @Getter
    private static ObjectMapper objectMapper = new ObjectMapper();


    private static final Logger LOGGER = LoggerFactory.getLogger(Utils.class);

    public static String readRequestBody(InputStream inputStream) throws IOException {
        StringWriter writer = null;
        if (inputStream != null) {
            writer = new StringWriter();
            IOUtils.copy(inputStream, writer, "UTF-8");
        }
        return writer != null ? writer.toString() : null;
    }


    public static String asJsonString(final Object obj) {
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T convertStrToObject(String jsonArray, Class<T> type) throws JsonProcessingException {
        return objectMapper.readValue(jsonArray, type);
    }

    public static <T> T convertStrToObject(String json, TypeReference<T> typeReference) throws IOException {
//      Disabling the FAIL_ON_UNKNOWN_PROPERTIES so that in case the input json has extra properties
//      other than the properties in the Class that needs to be converted to, jackson mapper doesn't throw exception
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        return objectMapper.readValue(json, typeReference);
    }

    public static Integer getRemainingTime() {  // Returns remaining time in day in seconds
        Integer secondsInADay = 86400;
        Calendar c = Calendar.getInstance();
        Integer nowTimeElapsedInSeconds = (c.get(Calendar.HOUR_OF_DAY) * 60 + c.get(Calendar.MINUTE)) * 60;
        LOGGER.debug("Current time in s " + nowTimeElapsedInSeconds);
        LOGGER.debug("TTL :" + (secondsInADay - nowTimeElapsedInSeconds));
        return (secondsInADay - nowTimeElapsedInSeconds);
    }

    public static String getTimeinFormat(String format, Long seconds) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        LocalDateTime utcNow = LocalDateTime.now(ZoneOffset.UTC);
        LocalDateTime utcNowPlusSec = utcNow.plusSeconds(seconds);
        return formatter.format(utcNowPlusSec);
    }

    public static String readGzipRequestBody(InputStream inputStream) throws IOException {
        StringWriter writer = null;
        if (inputStream != null) {
            writer = new StringWriter();
            GZIPInputStream gzipInput = new GZIPInputStream(inputStream);
            IOUtils.copy(gzipInput, writer, "UTF-8");
        }
        return writer != null ? writer.toString() : null;
    }

    /**
     * Method to convert input request Method
     *
     * @param input  - request body which is to be sent
     * @param ctx    - context of given request
     * @param method - HTTP method to which input request is to be converted
     */
    public static void convertRequest(Object input, RequestContext ctx, HttpMethod method) {
        String requestData = asJsonString(input);
        HttpServletRequest request = new GenericHttpServletRequest(
                ctx.getRequest(),
                requestData.getBytes(StandardCharsets.UTF_8),
                method.name(),
                MediaType.APPLICATION_JSON_VALUE
        );
        ctx.setRequest(request);
    }

    public static String convertDateToTimeStamp(String dateString) {
        String format = "yyyy-MM-dd"; // Date format
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        try {
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = formatter.parse(dateString);
            long unixTimestamp = date.getTime();
            return String.valueOf(unixTimestamp);
        } catch (Exception e) {
            Sentry.capture(e);
        }
        return null;
    }

    public static void capitalizeString(String[] values) {
        for (int index = 0; index < values.length; index++) {
            values[index] = StringUtils.capitalize(values[index]);
        }
    }

    public static boolean checkStringContainsOnlyNumbers(String value) {
        String pattern = Constants.NUMERIC_PATTERN;
        return value.matches(pattern);
    }

    public static <T> T processResponseEntity(ResponseEntity<String> responseEntity, int statusCode, Class<T> type, String errorMessage) throws JsonProcessingException {
        if (null == responseEntity || null == responseEntity.getBody()) {
            throw new ValidationException(errorMessage);
        } else if (!responseEntity.getStatusCode().is2xxSuccessful()) {
            throw new ValidationException(responseEntity.getBody());
        }
        return statusCode == responseEntity.getStatusCodeValue() ? convertStrToObject(responseEntity.getBody(), type) : null;
    }

    public static String buildURIString(String url, Map<String, Object> queryParams) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
        if (!CollectionUtils.isEmpty(queryParams)) {
            queryParams.forEach(builder::queryParam);
        }
        return builder.toUriString();
    }


    public static String getFormattedDateNew(LocalDateTime localDateTime) {
        if (localDateTime == null) return null;
        return getFormattedDateNew(localDateTime, Constants.DATE_TIME_FORMAT);
    }

    public static String getFormattedDateNew(LocalDateTime localDateTime, String format) {
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
            return localDateTime.format(formatter);
        } catch (Exception ex){
            return null;
        }
    }

    public static String convertDateToString(Date date, String format) {
        if(null == date) {
            return  null;
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(date);
    }

    public static final String getFullName(String firstName, String lastName) {
        return firstName + " " + Objects.toString(lastName, "");
    }
}


package com.everwell.datagateway.exceptions;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RestTemplateException extends RuntimeException {


    private Integer statusCode;
    private String error;

    public RestTemplateException(Integer statusCode, String error) {
        this.statusCode = statusCode;
        this.error = error;
    }


}
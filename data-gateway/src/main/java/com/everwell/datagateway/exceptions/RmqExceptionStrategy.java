package com.everwell.datagateway.exceptions;

import com.everwell.datagateway.config.RmqDlxHelper;
import com.everwell.datagateway.constants.Constants;
import com.everwell.datagateway.constants.QueueConstants;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.ImmediateAcknowledgeAmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.support.ListenerExecutionFailedException;
import org.springframework.util.ErrorHandler;

public class RmqExceptionStrategy implements ErrorHandler {

    @Override
    public void handleError(Throwable throwable) {
        if (throwable instanceof ListenerExecutionFailedException) {
            Message message = ((ListenerExecutionFailedException) throwable).getFailedMessage();
            String routingKey = message.getMessageProperties().getReceivedRoutingKey();
            boolean isHighSev =  Boolean.parseBoolean((String) message.getMessageProperties().getHeaders().getOrDefault(QueueConstants.HIGH_SEV_HEADER, "false"));
            Long retryDelay = isHighSev ? Constants.HIGH_SEV_RETRY_DELAY : Constants.RMQ_RETRY_DELAY;
            RmqDlxHelper.triggerRetryMechanism(message, routingKey, retryDelay);
            throw new ImmediateAcknowledgeAmqpException("[RMQ handleError] - Handled Error, Triggering Retry Mechanism");
        }
        throw new AmqpRejectAndDontRequeueException("Exception at consumer", throwable);
    }
}

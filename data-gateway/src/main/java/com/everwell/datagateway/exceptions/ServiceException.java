package com.everwell.datagateway.exceptions;

public class ServiceException extends RuntimeException {

    public ServiceException(String exception) {
        super(exception);
    }
}

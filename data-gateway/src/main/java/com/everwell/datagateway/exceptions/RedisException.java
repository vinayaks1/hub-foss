package com.everwell.datagateway.exceptions;

import com.everwell.datagateway.enums.StatusCodeEnum;

public class RedisException extends RuntimeException {

    private StatusCodeEnum statusCodeEnum;
    private Exception e;

    public RedisException(Exception e) {
        super(e);
    }

    public RedisException(StatusCodeEnum statusCodeEnum) {
        this.statusCodeEnum = statusCodeEnum;
    }
}

package com.everwell.datagateway.enums;

public enum AuthenticationTypeEnum {
    CLIENT,
    SUBSCRIBER_URL
}

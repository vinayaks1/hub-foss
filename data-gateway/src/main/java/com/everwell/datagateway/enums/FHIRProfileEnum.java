package com.everwell.datagateway.enums;

import com.everwell.datagateway.models.dto.DiagnosticsFHIRDto;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum FHIRProfileEnum {

    PATIENT("Patient","Patient","Patient/","episodeId"),
    PRACTITIONER("Practitioner","Practitioner","Practitioner/","userId"),
    DIAGNOSTIC_REPORT("DiagnosticReport","","DiagnosticReport/","testId"),
    MEDICATION_REQUEST("MedicationRequest","","MedicationRequest/",""),
    OBSERVATION("Observation","","Observation/","testId"),
    ORGANIZATION("Organization","Hierarchy","Organization/",""),
    COMPOSITION("DiagnosticReport","","Composition/","episodeId");

    @Getter
    private final String name;

    @Getter
    private final String identifier;

    @Getter
    private final String referenceText;

    @Getter
    private final String key;
}

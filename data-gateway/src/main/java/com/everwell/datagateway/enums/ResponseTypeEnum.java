package com.everwell.datagateway.enums;

public enum ResponseTypeEnum {
    SIMPLE_JSON_RESPONSE,
    FHIR_RESPONSE
}

package com.everwell.datagateway.enums;

import com.everwell.datagateway.constants.Constants;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import static com.everwell.datagateway.constants.Constants.*;

@RequiredArgsConstructor
public enum EventEnum {
    ADD_TEST(ADD_TEST_EVENT),
    GET_TEST_ID(GET_TEST_BY_ID_EVENT),
    GET_TEST_PATIENT_ID(GET_TEST_BY_PATIENT_ID_EVENT),
    UPDATE_TEST(UPDATE_TEST_EVENT),
    GENERIC_PUBLISH(GENERIC_PUBLISH_EVENT),
    PROCESS_CALL(PROCESS_CALL_EVENT),    
    ABDM_CARE_CONTEXT_LINKED(ABDM_CARE_CONTEXT_LINKED_EVENT),
    ABDM_CARE_CONTEXT_NOTIFY(ABDM_CARE_CONTEXT_NOTIFY_EVENT),
    ABDM_ADD_UPDATE_CONSENT(ABDM_ADD_UPDATE_CONSENT_EVENT),
    HIP_DATA_EXCHANGE(HIP_DATA_EXCHANGE_EVENT),
    ADD_MERM_ENTITY_MAPPING(ADD_MERM_ENTITY_MAPPING_EVENT),
    UPDATE_MERM_ENTITY_MAPPING(UPDATE_MERM_ENTITY_MAPPING_EVENT),
    DEALLOCATE_MERM_ENTITY(DEALLOCATE_MERM_ENTITY_EVENT),
    SEND_ADHERENCE_DATA(SEND_ADHERENCE_DATA_EVENT),
    GET_PATIENT_DETAILS(Constants.GET_PATIENT_DETAILS),
    SEARCH_PATIENT_DETAILS(Constants.SEARCH_PATIENT_DETAILS),
    CREATE_PATIENT(Constants.CREATE_PATIENT),
    UPDATE_PATIENT(Constants.UPDATE_PATIENT),
    DELETE_PATIENT(Constants.DELETE_PATIENT),
    ADD_MULTIPLE_PATIENTS("createMultiplePatients"),
    UPDATE_MULTIPLE_PATIENTS("updateMultiplePatients");
    @Getter
    private final String eventName;

    public static EventEnum getMatch(String eventName) {
        EventEnum eventEnum = null;
        for (EventEnum eventEnum1 : EventEnum.values()) {
            if (eventEnum1.eventName.equals(eventName)) {
                eventEnum = eventEnum1;
                break;
            }
        }

        return eventEnum;
    }
}

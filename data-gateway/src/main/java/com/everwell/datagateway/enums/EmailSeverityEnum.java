package com.everwell.datagateway.enums;

import lombok.Getter;

public enum EmailSeverityEnum {

    LOW_SEVERITY("LOW SEVERITY",1L),
    MEDIUM_SEVERITY("MEDIUM SEVERITY",100L),
    HIGH_SEVERITY("HIGH SEVERITY",1000L);


    @Getter
    private String severityName;
    @Getter
    private Long severityValue;

    EmailSeverityEnum(String severityName,Long severityValue)
    {
        this.severityName=severityName;
        this.severityValue = severityValue;
    }
}

package com.everwell.datagateway.enums;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public enum StatusCodeEnum {

    //Status Code for successful operation. Generic.
    SUCCESS(HttpStatus.OK, "Successful Response."),

    //Status Code for Failed operation. Generic.
    FAILURE(HttpStatus.I_AM_A_TEAPOT, "Hardly used failure response for generic failures"),


    //RabbitMQ related Codes
    FAILED_TO_PUBLISH_ADD_TEST(HttpStatus.INTERNAL_SERVER_ERROR, "RabbitMQ publisher has encountered an error while publishing the add Test event."),
    FAILED_TO_PUBLISH_UPDATE_TEST(HttpStatus.INTERNAL_SERVER_ERROR, "RabbitMQ publisher has encountered an error while publishing the add Test event."),

    //Security/Credentials related Status COdes
    INVALID_CREDENTIALS(HttpStatus.UNAUTHORIZED, "Invalid Credentials. Please enter the credentials correctly."),
    CONFLICT(HttpStatus.CONFLICT, "Username Already Exists"),
    EVENT_NOT_FOUND(HttpStatus.BAD_REQUEST, "EventName doesn't exist"),
    CANNOT_SUBSCRIBE_TO_EVENT(HttpStatus.BAD_REQUEST, "Cannot Subscribe to event!"),
    INTERNAL_SERVER_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, "Internal Server Error"),
    BAD_REQUEST(HttpStatus.BAD_REQUEST, "Bad Request"),
    EVENT_NOT_FOUND_SENTRY(HttpStatus.NOT_FOUND, "This Event doesn't exists: "),
    CLIENT_NOT_FOUND_SENTRY(HttpStatus.NOT_FOUND, "This Client doesn't exists: "),
    URL_DISABLED_SENTRY(HttpStatus.NOT_FOUND, "Subscriber url is null , or Subscriber url is disabled for EventClientId: "),
    CLIENT_OR_EVENT_NOT_FOUND(HttpStatus.NOT_FOUND,"Client or event not found"),
    CLIENT_NOT_SUBSCRIBED_TO_EVENT_OR_DISABLED(HttpStatus.FORBIDDEN,"Client not subscribed to event or event subscription is disabled"),
    NOT_FOUND_HANDLER(HttpStatus.NOT_FOUND, "No event handler found"),
    CLIENT_NOT_SUBSCRIBED_TO_EVENT(HttpStatus.NOT_FOUND,"Client not subscribed to event"),
    CLIENT_ID_UNAVAILABLE(HttpStatus.NOT_FOUND, "Client Id is not available"),
    MOBILE_NUMBER_REQUIRED(HttpStatus.BAD_REQUEST, "Phone is mandatory in telecom details"),
    FIRST_NAME_REQUIRED(HttpStatus.BAD_REQUEST, "Given is mandatory in name details");
    //Only getter declared as set will most probably happen during constructor call
    public HttpStatus code;
    public String message;

    StatusCodeEnum(HttpStatus code, String message) {
        this.code = code;
        this.message = message;
    }

}

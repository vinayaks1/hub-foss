package com.everwell.datagateway.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import static com.everwell.datagateway.constants.Constants.*;

@RequiredArgsConstructor
public enum RefIdStatusEnum {
    REQUEST_IN_PROCESS(REQUEST_IN_PROCESS_STATUS),
    REQUEST_COMPLETED_AND_NOTIFIED(REQUEST_COMPLETED_AND_NOTIFIED_STATUS),
    REQUEST_COMPLETED_OR_NOT_FOUND(REQUEST_COMPLETED_OR_NOT_FOUND_STATUS);
    @Getter
    private final String status;


}

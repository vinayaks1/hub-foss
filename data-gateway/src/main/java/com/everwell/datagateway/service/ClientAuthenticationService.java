package com.everwell.datagateway.service;

import com.everwell.datagateway.models.dto.AuthenticationDetailsDto;
import com.everwell.datagateway.models.dto.ClientAuthenticationDto;

import java.util.List;
import java.util.Map;

public interface ClientAuthenticationService {
    void validateCredentials(ClientAuthenticationDto clientAuthenticationDto);

    void saveAuthenticationDetails(ClientAuthenticationDto clientAuthenticationDto);

    AuthenticationDetailsDto getSpecificAuthenticationDetails(Long clientId, Long eventClientId);

    Map<String, AuthenticationDetailsDto> getAuthenticationDetails(List<Long> typeIdList);

}

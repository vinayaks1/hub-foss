package com.everwell.datagateway.service;

import com.everwell.datagateway.models.response.Episode;
import org.hl7.fhir.r4.model.Patient;

import java.util.List;
import java.util.Map;

public interface PMIRService {

    Map<String, Object> createUpdateEpisodeRequest(Patient patient, boolean isUpdate, Long clientId);

    List<Patient> getPatientList(String request);
    List<Episode> addUpdateMultiplePatients(List<Patient> patientList, Long accessibleClientId, boolean update);
}

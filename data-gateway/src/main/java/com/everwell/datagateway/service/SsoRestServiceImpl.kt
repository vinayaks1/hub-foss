package com.everwell.datagateway.service

import com.everwell.datagateway.constants.Constants
import com.everwell.datagateway.models.response.ApiResponse
import com.everwell.datagateway.models.response.MicroServiceGenericAuthResponse
import org.slf4j.LoggerFactory
import org.springframework.core.ParameterizedTypeReference
import org.springframework.stereotype.Service
import java.util.*

@Service
open class SsoRestServiceImpl : SsoRestService() {

    private val LOGGER = LoggerFactory.getLogger(SsoRestServiceImpl::class.simpleName)

    override fun getAuthToken(client_id: String?): String? {
        if(!clientIdAccessTokenMap.contains(client_id) || Date().time >= clientIdRefreshTimeMap[client_id]!!){
            LOGGER.debug("[SsoRestServiceImpl] Fetching auth token...")
            val headers = mutableMapOf<String, String>()
            headers["X-SSO-Client-Id"] = client_id!!
            val response = authenticate(
                appProperties.ssoClientIdUrlMap.get(client_id) + "/v1/ssoserver/client",
                headers,
                object:  ParameterizedTypeReference<ApiResponse<MicroServiceGenericAuthResponse>>() {})

            clientIdAccessTokenMap[client_id] = response.body?.data?.authToken !!
            if (response.body?.data?.nextRefresh == null ||  response.body?.data?.nextRefresh!! == 0L)
                clientIdRefreshTimeMap[client_id] = Date().time + Constants.MICROSERVICE_REFRESH_EXPIRY
            else
                clientIdRefreshTimeMap[client_id] = response.body?.data?.nextRefresh!!
        }
        return clientIdAccessTokenMap[client_id]
    }
}
package com.everwell.datagateway.service.impl;

import com.everwell.datagateway.constants.ECBSSConstants;
import com.everwell.datagateway.models.response.ecbss.TrackedEntityInstancesResponse;
import com.everwell.datagateway.service.ECBSSHelperService;
import com.everwell.datagateway.service.RestService;
import com.everwell.datagateway.utils.Utils;
import io.sentry.Sentry;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
public class ECBSSHelperServiceImpl implements ECBSSHelperService {

    @Setter
    @Value("${ECBSS_API_ENDPOINT:}")
    String baseUrl;

    @Setter
    @Value("${ECBSS_USERNAME:}")
    String username;

    @Setter
    @Value("${ECBSS_PASSWORD:}")
    String password;

    @Autowired
    RestService restService;

    public RestTemplate restTemplate = new RestTemplate();

    private HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth(username, password);
        return headers;
    }

    /**
     * Method to fetch TrackedEntityInstance for the given OrgUnitId and ExternalId1
     * @param orgUnitId - OrgUnitId whose TrackedEntityInstance is to be fetched
     * @param externalId1 - ExternalId1 whose TrackedEntityInstance is to be fetched
     * @return TrackedEntityInstance fetched from ECBSS
     */
    @Override
    public TrackedEntityInstancesResponse getTrackedEntityInstance(String orgUnitId, String externalId1) {
        HttpHeaders headers = getHeaders();
        String url = baseUrl + ECBSSConstants.GET_TRACKED_ENTITY_ENDPOINT;
        Map<String, Object> params = new HashMap<>();
        params.put(ECBSSConstants.OU_KEY, orgUnitId);
        params.put(ECBSSConstants.FILTER_KEY, ECBSSConstants.FILTER + externalId1);
        params.put(ECBSSConstants.PROGRAM_KEY, ECBSSConstants.PROGRAM);
        params.put(ECBSSConstants.OU_MODE_KEY, ECBSSConstants.OUMODE);
        String uri = Utils.buildURIString(url, params);
        HttpEntity<String> request = new HttpEntity<>(headers);
        TrackedEntityInstancesResponse trackedEntityInstancesResponse = null;
        try {
            ResponseEntity<String> responseEntity = restTemplate.exchange(uri, HttpMethod.GET, request, String.class);
            trackedEntityInstancesResponse = Utils.processResponseEntity(responseEntity, HttpStatus.OK.value(), TrackedEntityInstancesResponse.class, ECBSSConstants.ERROR_MESSAGE_TRACKED_ENTITY_INSTANCE);
        } catch (Exception e) {
            Sentry.capture(e);
        }
        return trackedEntityInstancesResponse;
    }
}

package com.everwell.datagateway.service

import com.everwell.datagateway.models.request.EpisodeSearchRequest
import com.everwell.datagateway.models.response.*

abstract class EpisodeServiceRestService: BaseRestService() {
    abstract fun getEpisodesFromPerson(token: String, userId: Int): ApiResponse<List<GetEpisodeByPersonResponse>>
    abstract fun updateEpisodeDetails(token: String, data: Map<String, Any>): ApiResponse<GetEpisodeByPersonResponse>
    abstract fun episodeSearch(episodeSearchRequest: EpisodeSearchRequest, clientId: Long): EpisodeSearchResult
    abstract fun createEpisode(createEpisodeRequest: Map<String, Object>, clientId: Long): ApiResponse<Episode>
    abstract fun getDefaultDiseaseForClientId(clientId: Long): ApiResponse<DiseaseTemplate>
    abstract fun updateEpisode(createEpisodeRequest: Map<String, Object>, clientId: Long): ApiResponse<Episode>
    abstract fun deleteEpisode(episodeId: Long, clientId: Long): ApiResponse<String>
}
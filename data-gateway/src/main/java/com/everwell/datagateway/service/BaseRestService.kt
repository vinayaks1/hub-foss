package com.everwell.datagateway.service

import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.entities.Client
import com.everwell.datagateway.models.request.CustomHttpRequest
import com.everwell.datagateway.models.request.EpisodeSearchRequest
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.*
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import org.springframework.web.client.RestTemplate

abstract class BaseRestService {

    public var restTemplateForAuthentication: RestTemplate? = null

    @Autowired
    lateinit var clientService: ClientService

    @Autowired
    lateinit var restService: RestService

    @Autowired
    lateinit var appProperties: AppProperties

    @Autowired
    lateinit var authenticationService: AuthenticationService

    var accessToken: String? = null

    var clientIdAccessTokenMap: MutableMap<String, String> = mutableMapOf()

    var clientIdRefreshTimeMap: MutableMap<String, Long> = mutableMapOf()

    init {
        accessToken = null
        clientIdRefreshTimeMap = mutableMapOf()
        clientIdAccessTokenMap = mutableMapOf()
    }


    fun <T> authenticate(authUrl: String, headers: MutableMap <String, String>, responseType: ParameterizedTypeReference<T>): ResponseEntity<T> {
        return authenticate(authUrl, headers, LinkedMultiValueMap<String, String>(), HttpMethod.GET, responseType)
    }

    fun <T> authenticate(authUrl: String, headers: MutableMap <String, String>, body: MultiValueMap<String, String>, responseType: ParameterizedTypeReference<T>): ResponseEntity<T> {
        return authenticate(authUrl, headers, body, HttpMethod.POST, responseType)
    }

    fun <T> authenticate(authUrl: String, headers: MutableMap <String, String>, body: MultiValueMap<String, String>, httpMethod: HttpMethod, responseType: ParameterizedTypeReference<T>): ResponseEntity<T> {
        if (restTemplateForAuthentication == null) restTemplateForAuthentication = RestTemplate()
        return restTemplateForAuthentication!!.exchange(authUrl, httpMethod, getHttpEntity(headers, body), responseType)
    }

    fun <T> authenticate(authUrl: String, headers: MutableMap <String, String>, body: Map<String, Any>, httpMethod: HttpMethod, responseType: ParameterizedTypeReference<T>): ResponseEntity<T> {
        if (restTemplateForAuthentication == null) restTemplateForAuthentication = RestTemplate()
        return restTemplateForAuthentication!!.exchange(authUrl, httpMethod, getHttpEntity(headers, body), responseType)
    }

    fun <T> authenticate(authUrl: String, headers: MutableMap <String, String>, body: EpisodeSearchRequest, httpMethod: HttpMethod, responseType: ParameterizedTypeReference<T>): ResponseEntity<T> {        if (restTemplateForAuthentication == null) restTemplateForAuthentication = RestTemplate()
        if (restTemplateForAuthentication == null) restTemplateForAuthentication = RestTemplate()
        try {
            return restTemplateForAuthentication!!.exchange(authUrl, httpMethod, getHttpEntity(headers, body), responseType)
        } catch (ex: Exception) {
            throw Exception(ex.message)
        }
    }

    public fun getHttpEntity(headers: MutableMap <String, String>, body: MultiValueMap<String, String>) : HttpEntity<Any> {
        val httpHeaders = HttpHeaders()
        headers.forEach {
            httpHeaders.add(it.key, it.value)
        }
        httpHeaders.add("Accept", MediaType.APPLICATION_JSON.toString())
        if (body.size > 0)
            httpHeaders.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED.toString())
        return HttpEntity(body, httpHeaders)
    }

    fun getHttpEntity(headers: MutableMap <String, String>, body: Map<String, Any>) : HttpEntity<Any> {
        val httpHeaders = HttpHeaders()
        headers.forEach {
            httpHeaders.add(it.key, it.value)
        }
        httpHeaders.add("Accept", MediaType.APPLICATION_JSON.toString())
        if (body.isNotEmpty())
            httpHeaders.add("Content-Type", MediaType.APPLICATION_JSON_VALUE)
        return HttpEntity(body, httpHeaders)
    }

    fun getHttpEntity(headers: MutableMap <String, String>, body: EpisodeSearchRequest) : HttpEntity<Any> {
        val httpHeaders = HttpHeaders()
        headers.forEach {
            httpHeaders.add(it.key, it.value)
        }
        httpHeaders.add("Accept", MediaType.APPLICATION_JSON.toString())
        httpHeaders.add("Content-Type", MediaType.APPLICATION_JSON_VALUE)
        return HttpEntity(body, httpHeaders)
    }

    fun provideCurrentClient(): Client?{
        return clientService.findByUsername(getUserName())
    }

    fun saveAccessToken(access_token: String?){
        val client = provideCurrentClient()
        if(client!=null) client.authTokenForProxy = access_token
        clientService.saveClient(client)
    }

    fun getUserName(): String?{
        val authentication = SecurityContextHolder.getContext().authentication
        return authenticationService.getCurrentUsername(authentication)
    }

    abstract fun getAuthToken(client_id: String?): String?

}
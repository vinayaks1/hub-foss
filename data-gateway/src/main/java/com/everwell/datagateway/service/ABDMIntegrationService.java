package com.everwell.datagateway.service;

import com.everwell.datagateway.apiHelpers.ABDMHelper;
import com.everwell.datagateway.component.AppProperties;
import com.everwell.datagateway.constants.Constants;
import com.everwell.datagateway.entities.Client;
import com.everwell.datagateway.entities.ClientRequests;
import com.everwell.datagateway.entities.Event;
import com.everwell.datagateway.models.dto.ABDMConsentRequestDto;
import com.everwell.datagateway.models.dto.EventStreamingDTO;
import com.everwell.datagateway.models.dto.LinkConfirmRedisDto;
import com.everwell.datagateway.models.request.UpdateUserRequest;
import com.everwell.datagateway.models.request.UpdateUserRequest.*;
import com.everwell.datagateway.models.request.abdm.*;
import com.everwell.datagateway.models.response.ApiResponse;
import com.everwell.datagateway.models.response.GetEpisodeByPersonResponse;
import com.everwell.datagateway.models.response.LinkInitiationOtpResponse;
import com.everwell.datagateway.publishers.RMQPublisher;
import com.everwell.datagateway.utils.CacheUtils;
import com.everwell.datagateway.utils.Utils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.sentry.Sentry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;


import java.util.*;
import java.util.stream.Collectors;

import static com.everwell.datagateway.constants.Constants.*;
import static com.everwell.datagateway.constants.QueueConstants.UPDATE_CONSENT_REQUEST_ROUTING_KEY;

@Service
public class ABDMIntegrationService {

    @Value("${NDHM_HIP_NAME:}")
    private String HIP;
    @Autowired
    private ABDMHelper abdmApiHelper;
    @Autowired
    private PublisherService publisherService;
    @Autowired
    private EmailService emailService;

    @Autowired
    private UserServiceRestService userServiceRestService;

    @Autowired
    private EpisodeServiceRestService episodeServiceRestService;

    @Autowired
    private EventService eventService;

    @Autowired
    private ClientService clientService;

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private ClientRequestsService clientRequestsService;

    @Autowired
    private AppProperties appProperties;

    @Autowired
    private NikshayRestService nikshayRestService;

    @Autowired
    private RMQPublisher rmqPublisher;

    Logger logger = LoggerFactory.getLogger(ABDMIntegrationService.class);

    @Value("${abdm.m2.direct.auth:}")
    boolean directAuthEnabled;

    private static final Logger LOGGER = LoggerFactory.getLogger(ABDMIntegrationService.class);

    public String initiateDemographicAuth(ABDMLinkCareContextEvent data) {
        ABDMInitateAuthRequest req = directAuthEnabled ? new ABDMInitateAuthRequest(data.abhaAddress, DIRECT_AUTH_MODE) : new ABDMInitateAuthRequest(data.abhaAddress, DEMOGRAPHICS_AUTH_MODE);
        Gson gson = new GsonBuilder().create();
        String jsonCareContextDetails = gson.toJson(data);
        CacheUtils.putIntoCache(data.abhaAddress, jsonCareContextDetails, 7200);
        CacheUtils.putIntoCache(req.requestId, req.query.id, 7200);
        return abdmApiHelper.authInitRequest(req);
    }

    public boolean getCareContextLinkingToken(String requestId, String transactionId) {
        try
        {
            ABDMLinkCareContextEvent careContextDetails = getCareContextLinkingDetailsFor(requestId);
            ABDMDemographicLinkingTokenRequest.Credential cred = new ABDMDemographicLinkingTokenRequest.Credential(
                    careContextDetails.personName,
                    Character.toString(careContextDetails.gender),
                    careContextDetails.yearOfBirth,
                    careContextDetails.identifierType,
                    careContextDetails.identifierValue);
            ABDMDemographicLinkingTokenRequest linkingTokenReq = new ABDMDemographicLinkingTokenRequest(transactionId, cred);
            LOGGER.info("linkingTokenReq " + Utils.asJsonString(linkingTokenReq));
            if(directAuthEnabled){
                CacheUtils.putIntoCache(transactionId, careContextDetails.abhaAddress, 7200);
                return true;
            }
            CacheUtils.putIntoCache(linkingTokenReq.requestId, careContextDetails.abhaAddress, 7200);
            String response = abdmApiHelper.authConfirmRequest(linkingTokenReq);
            return null != response;
        }
        catch(Exception ex)
        {
            LOGGER.info("getCareContextLinkingToken " + ex.toString());
            Sentry.capture(ex);
        }
        return false;
    }

    public boolean addCareContext(String requestId, String accessToken) {
        try{
            ABDMLinkCareContextEvent careContextDetails = getCareContextLinkingDetailsFor(requestId);
            LOGGER.info("ABDMLinkCareContextEvent "+ Utils.asJsonString(careContextDetails));
            ABDMLinkCareContextRequest linkingRequest = new ABDMLinkCareContextRequest(
                    accessToken,
                    Long.toString(careContextDetails.personId),
                    Long.toString(careContextDetails.episodeId),
                    careContextDetails.personName,
                    careContextDetails.enrollmentDate);
            CacheUtils.putIntoCache(linkingRequest.requestId, careContextDetails.abhaAddress, 7200);
            String response = abdmApiHelper.addCareContextRequest(linkingRequest);
            if (null != response) {
                return true;
            }
        }catch(Exception ex)
        {
            LOGGER.info("getCareContextLinkingToken " + ex.toString());
            Sentry.capture(ex);
        }
        return false;
    }

    public boolean notifyCareContext(ABDMNotifyCareContextEvent event) {
        String episodeServiceToken = episodeServiceRestService.getAuthToken(appProperties.genericClientId);
        boolean success = true;
        Map<String,Object> map = new HashMap<>();
        map.put(EPISODE_ASSOCIATION_CARE_CONTEXT, Boolean.TRUE);
        map.put("episodeId",event.episodeId);
        if(episodeServiceToken != null) {
            ApiResponse<GetEpisodeByPersonResponse> episodeResponse = episodeServiceRestService.updateEpisodeDetails(episodeServiceToken,map);
            if(!Boolean.parseBoolean(episodeResponse.getSuccess())) {
                success = false;
            }
        }
        ABDMNotifyCareContextRequest req = new ABDMNotifyCareContextRequest(event, HIP);
        String response = abdmApiHelper.notifyCareContext(req);
        if(null == response){
            success = false;
        }
        return success;
    }

    public ABDMLinkCareContextEvent getCareContextLinkingDetailsFor(String requestId) {
        Gson gson = new GsonBuilder().create();
        String abhaAddress = CacheUtils.getFromCache(requestId);
        String careContextDetailsString = CacheUtils.getFromCache(abhaAddress);
        ABDMLinkCareContextEvent careContextDetails = gson.fromJson(careContextDetailsString, ABDMLinkCareContextEvent.class);
        return careContextDetails;
    }

    public ABDMOnDiscoveryRequest getOnDiscoveryRequestDetailsFor(String key) {
        Gson gson = new GsonBuilder().create();
        String reqString = CacheUtils.getFromCache(key);
        ABDMOnDiscoveryRequest req = gson.fromJson(reqString, ABDMOnDiscoveryRequest.class);
        return req;
    }

    public void processDiscoveryRequest(ABDMDiscoveryRequest request) {

        Long refId = saveClientDetails(USER_DISCOVERY_EVENT,Utils.asJsonString(request));
        ABDMOnDiscoveryRequest data = getEpisodeDetailsWith(
                request.getTransactionId(),
                request.getRequestId(),
                getMatcherMapFrom(request));
        CacheUtils.putIntoCache(ABDM_ON_DISCOVERY_CACHE_PREFIX + data.getTransactionId(), Utils.asJsonString(data), ABDM_CACHE_DISCOVERY_REQUEST_TTL);
        logger.info("[ABDMOnDiscoveryRequest]" + Utils.asJsonString(data));
        boolean success = abdmApiHelper.onDiscovery(data);
        if(success) {
            ClientRequests clientRequests = clientRequestsService.getClientRequestsById(refId);
            String payload = Utils.asJsonString(data);
            clientRequests.setResponseData(payload.substring(0, Math.min(payload.length(), 255)));
            clientRequests.setResultDelivered(true);
            clientRequestsService.updateClientRequest(clientRequests);
        }
    }

    private Map<String, String> getMatcherMapFrom(ABDMDiscoveryRequest request) {
        Map<String, String> reqData = new HashMap<>();

        reqData.put(NAME, request.getPatient().name);
        reqData.put(YEAR_OF_BIRTH, request.getPatient().yearOfBirth);

        switch (request.getPatient().gender) {
            case "M":
                reqData.put(GENDER, GENDER_MALE);
                break;
            case "F":
                reqData.put(GENDER, GENDER_FEMALE);
                break;
            default:
                reqData.put(GENDER, GENDER_TRANSGENDER);
        }

        for (ABDMIdentifiers i : request.getPatient().getVerifiedIdentifiers()) {
            if (i.getType().equalsIgnoreCase(ABDM_IDENTIFIER_MOBILE))
                reqData.put(ABDM_IDENTIFIER_MOBILE, removeINDPhonePrefix(i.getValue()));
            if (i.getType().equalsIgnoreCase(Constants.ABDM_IDENTIFIER_HEALTH_NUMBER))
                reqData.put(Constants.HEALTH_NUMBER_IN_USER_SERVICE, i.getValue());
            if (i.getType().equalsIgnoreCase(ABDM_IDENTIFIER_HEALTH_ID))
                reqData.put(HEALTH_ID_IN_USER_SERVICE, i.getValue());
        }
        return reqData;
    }

    private String removeINDPhonePrefix(String number) {
        if (number.startsWith("+91-"))
            return number.substring(4);
        return number;
    }

    private ABDMOnDiscoveryRequest getEpisodeDetailsWith(String transactionId, String requestId, Map<String,String> data) {
        ApiResponse<Integer>  userResponse = null;
        ApiResponse<List<GetEpisodeByPersonResponse>> episodeResponse = null;
        ABDMOnDiscoveryRequest req;

        String userServiceToken = userServiceRestService.getAuthToken(appProperties.genericClientId);
        logger.info("[userServiceToken]" + userServiceToken);
        if(userServiceToken != null) {
            userResponse =  userServiceRestService.searchUser(userServiceToken,data);
            logger.info(Utils.asJsonString(userResponse));
        }

        String episodeServiceToken = episodeServiceRestService.getAuthToken(appProperties.genericClientId);
        logger.info("[episodeServiceToken]" + episodeServiceToken);
        if(episodeServiceToken != null && userResponse != null && Boolean.parseBoolean(userResponse.getSuccess())) {
            episodeResponse = episodeServiceRestService.getEpisodesFromPerson(episodeServiceToken,userResponse.getData());
            logger.info(Utils.asJsonString(episodeResponse));
        }
        else if(userResponse != null && !Boolean.parseBoolean(userResponse.getSuccess())) {
           req = new ABDMOnDiscoveryRequest(
                    transactionId,
                    userResponse.getMessage(),requestId);
           return req;
        }

        Map<String, String> resp = new HashMap<>();

        if(episodeResponse != null && Boolean.parseBoolean(episodeResponse.getSuccess())) {
            for(GetEpisodeByPersonResponse episode : episodeResponse.getData()) {
                resp.put(String.valueOf(episode.getId()),episode.getCreatedDate());
            }
            req = new ABDMOnDiscoveryRequest(
                    transactionId,
                    userResponse.getData().toString(),
                    data.get(NAME),
                    requestId,
                    resp);
            return req;
        }
        req = new ABDMOnDiscoveryRequest(transactionId,GENERIC_ERROR,requestId);

        Gson gson = new GsonBuilder().create();

        return req;
    }

    Long saveClientDetails(String eventName,String requestData) {
        Event event = eventService.findEventByEventName(eventName);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String clientName = authenticationService.getCurrentUsername(authentication);
        Client client = clientService.findByUsername(clientName);
        ClientRequests clientRequests = new ClientRequests();
        clientRequests.setEvent(event);
        clientRequests.setClient(client);
        clientRequests.setRequestData(requestData);

        return clientRequestsService.saveClientRequest(clientRequests);
    }

    public void processLinkInitiationRequest(ABDMLinkInitiationRequest request) throws JsonProcessingException {
        Long refId = saveClientDetails(USER_LINK_INITIATION_EVENT,Utils.asJsonString(request));
        ABDMOnLinkInitiationRequest abdmOnLinkInitiationRequest = sendOtpSms(request);
        boolean success = abdmApiHelper.OnLinkInit(abdmOnLinkInitiationRequest);
       logger.info("[ABDMOnLinkInitiationRequest]"+Utils.asJsonString(abdmOnLinkInitiationRequest));
        if(success) {
            ClientRequests clientRequests = clientRequestsService.getClientRequestsById(refId);
            String payload = Utils.asJsonString(abdmOnLinkInitiationRequest);
            clientRequests.setResponseData(payload.substring(0, Math.min(payload.length(), 255)));
            clientRequests.setResultDelivered(true);
            clientRequestsService.updateClientRequest(clientRequests);
        }
    }

    public ABDMOnLinkInitiationRequest sendOtpSms(ABDMLinkInitiationRequest request) {
        ApiResponse<LinkInitiationOtpResponse> response = null;
        ABDMOnLinkInitiationRequest abdmOnLinkInitiationRequest;
        try {
            String userId = request.patient.getReferenceNumber();
            logger.info("[ON_DISCOVERY_REQUEST]" + CacheUtils.hasKey(ABDM_ON_DISCOVERY_CACHE_PREFIX + request.getTransactionId()));
            if(CacheUtils.hasKey(ABDM_ON_DISCOVERY_CACHE_PREFIX + request.getTransactionId())){
                String token = nikshayRestService.getAuthToken(null);
                if(null != token && null != userId) {
                    response = nikshayRestService.sendOtpLinkInitiation(token,userId);
                }
            }

            if(response != null) {
                if(Boolean.parseBoolean(response.getSuccess())) {
                    abdmOnLinkInitiationRequest = new ABDMOnLinkInitiationRequest(request.transactionId,request.requestId,userId,response.getData().getMessage(),Utils.getTimeinFormat(ABDM_TIMESTAMP_FORMAT,600L));
                    cacheLinkRequest(request,response.getData());
                }
                else {
                    abdmOnLinkInitiationRequest = new ABDMOnLinkInitiationRequest(request.transactionId,request.requestId,response.getMessage());
                }
            }
            else {
                abdmOnLinkInitiationRequest = new ABDMOnLinkInitiationRequest(request.transactionId,request.requestId,GENERIC_ERROR);
            }
        }
        catch (Exception ex) {
            Sentry.capture(ex);
            abdmOnLinkInitiationRequest = new ABDMOnLinkInitiationRequest(request.transactionId,request.requestId,GENERIC_ERROR);
        }
        return abdmOnLinkInitiationRequest;
    }

    public void processLinkConfirmationRequest(ABDMLinkConfirmationRequest abdmLinkConfirmationRequest) {
        Long refId = saveClientDetails(USER_LINK_CONFIRMATION_EVENT,Utils.asJsonString(abdmLinkConfirmationRequest));
       ABDMOnLinkConfirmationRequest onLinkConfirmRequest = validateOtpAndAddCareContexts(abdmLinkConfirmationRequest);
        boolean success = abdmApiHelper.OnLinkConfirm(onLinkConfirmRequest);
        logger.info("[ABDMOnLinkConfirmationRequest]"+Utils.asJsonString(onLinkConfirmRequest));
        if(success) {
            ClientRequests clientRequests = clientRequestsService.getClientRequestsById(refId);
            String payload = Utils.asJsonString(onLinkConfirmRequest);
            clientRequests.setResponseData(payload.substring(0, Math.min(payload.length(), 255)));
            clientRequests.setResultDelivered(true);
            clientRequestsService.updateClientRequest(clientRequests);
        }
    }

    private void cacheLinkRequest(ABDMLinkInitiationRequest linkInitiationRequest, LinkInitiationOtpResponse otpResponse) {
        ABDMOnDiscoveryRequest onDiscoveryRequest = getOnDiscoveryRequestDetailsFor(ABDM_ON_DISCOVERY_CACHE_PREFIX +linkInitiationRequest.transactionId);
        List<String> episodeIds = linkInitiationRequest.patient.getCareContexts().stream().map(p -> p.referenceNumber).collect(Collectors.toList());
        Map<String,String> episodeIdEnrollmentDateMap  = new HashMap<>();
        for(ABDMCareContextDTO.CareContext careContext : onDiscoveryRequest.getPatient().getCareContexts()) {
            if(episodeIds.contains(careContext.getReferenceNumber())) {
                episodeIdEnrollmentDateMap.put(careContext.getReferenceNumber(),careContext.getDisplay());
            }
        }
        String personId = onDiscoveryRequest.getPatient().getReferenceNumber();
        String name = onDiscoveryRequest.getPatient().getDisplay();
        String abhaAddress = linkInitiationRequest.patient.getId();
        onDiscoveryRequest.setPatient(new ABDMCareContextDTO(personId,name,episodeIdEnrollmentDateMap));
        LinkConfirmRedisDto linkConfirmRedisDto = new LinkConfirmRedisDto(onDiscoveryRequest, otpResponse.getOtp(),abhaAddress);
        CacheUtils.putIntoCache(ABDM_LINK_INITIATION_CACHE_PREFIX +personId,Utils.asJsonString(linkConfirmRedisDto),ABDM_CACHE_DISCOVERY_REQUEST_TTL);
    }

    public ABDMOnLinkConfirmationRequest validateOtpAndAddCareContexts(ABDMLinkConfirmationRequest abdmLinkConfirmationRequest) {
        Gson gson = new GsonBuilder().create();
        ABDMOnLinkConfirmationRequest abdmOnLinkConfirmationRequest;

        try {
            String personId = abdmLinkConfirmationRequest.getConfirmation().getLinkRefNumber();
            if(!CacheUtils.hasKey(ABDM_LINK_INITIATION_CACHE_PREFIX + personId)) {
               return new ABDMOnLinkConfirmationRequest(abdmLinkConfirmationRequest.requestId,GENERIC_ERROR);
            }
            String json = CacheUtils.getFromCache(ABDM_LINK_INITIATION_CACHE_PREFIX + personId);
            LinkConfirmRedisDto redisDto = gson.fromJson(json,LinkConfirmRedisDto.class);
            logger.info("[LinkConfirmRedisDto]" + Utils.asJsonString(redisDto));
            String token = abdmLinkConfirmationRequest.confirmation.getToken();
            if(token.equals(redisDto.getOtp())) {
                String episodeServiceToken = episodeServiceRestService.getAuthToken(appProperties.genericClientId);
                List<ABDMCareContextDTO.CareContext> episodeDto = redisDto.getAbdmOnDiscoveryRequest().getPatient().getCareContexts();

                if(episodeServiceToken != null) {
                    Map<String,Object> map = new HashMap<>();
                    map.put(EPISODE_ASSOCIATION_CARE_CONTEXT, Boolean.TRUE);
                    boolean success = true;
                    for(ABDMCareContextDTO.CareContext episode : episodeDto) {
                        // Add Care context for episode
                        map.put("episodeId",episode.getReferenceNumber());
                        ApiResponse<GetEpisodeByPersonResponse> episodeResponse = episodeServiceRestService.updateEpisodeDetails(episodeServiceToken,map);
                        if(!Boolean.parseBoolean(episodeResponse.getSuccess())) {
                            success = false;
                            break;
                        }
                        logger.info(Utils.asJsonString(episodeResponse));
                        map.remove("episodeId");
                    }

                    if(success) {
                        // Update user's abha address
                        Map<String,Object> headersMap = new HashMap<>();
                        headersMap.put("X-US-Client-Id",appProperties.genericClientId);
                        UpdateUserRequest updateUserRequest = new UpdateUserRequest(Long.parseLong(personId),Collections.singletonList(new ExternalIdRequest("HEALTH_ADDRESS",redisDto.getAbhaAddress())));
                        EventStreamingDTO eventStreamingDTO = new EventStreamingDTO();
                        eventStreamingDTO.setField(updateUserRequest);
                        rmqPublisher.send(eventStreamingDTO,UPDATE_USER_ROUTING_KEY,UPDATE_USER_EXCHANGE,headersMap);
                        abdmOnLinkConfirmationRequest = new ABDMOnLinkConfirmationRequest(redisDto.getAbdmOnDiscoveryRequest().getPatient(),abdmLinkConfirmationRequest.getRequestId());
                    }
                    else {
                        abdmOnLinkConfirmationRequest = new ABDMOnLinkConfirmationRequest(abdmLinkConfirmationRequest.getRequestId(),GENERIC_ERROR);
                    }
                }
                else {
                    abdmOnLinkConfirmationRequest = new ABDMOnLinkConfirmationRequest(abdmLinkConfirmationRequest.getRequestId(),GENERIC_ERROR);
                }
            }
            else {
                abdmOnLinkConfirmationRequest = new ABDMOnLinkConfirmationRequest(abdmLinkConfirmationRequest.getRequestId(),ABDM_INVALID_OTP_ERROR);
            }
        }
        catch (Exception e) {
            Sentry.capture(e);
            abdmOnLinkConfirmationRequest = new ABDMOnLinkConfirmationRequest(abdmLinkConfirmationRequest.getRequestId(),GENERIC_ERROR);
        }
        return abdmOnLinkConfirmationRequest;
    }

    public boolean processConsentInitiation(ABDMConsentRequestOnInitiation abdmConsentRequestOnInitiation) {
        saveClientDetails(CONSENT_INITIATION_EVENT,Utils.asJsonString(abdmConsentRequestOnInitiation));
        Gson gson = new GsonBuilder().create();
        String requestId = abdmConsentRequestOnInitiation.getResp().getRequestId();
        if(!CacheUtils.hasKey(ABDM_CONSENT_INITIATION_CACHE_PREFIX + requestId)) {
            return false;
        }

        String json = CacheUtils.getFromCache(ABDM_CONSENT_INITIATION_CACHE_PREFIX + requestId);
        ABDMConsentRequestDto abdmConsentRequestDto = gson.fromJson(json,ABDMConsentRequestDto.class);
        String consentRequestId = abdmConsentRequestOnInitiation.getConsentRequest().getId();
        abdmConsentRequestDto.setConsentRequestId(consentRequestId);
        rmqPublisher.send(abdmConsentRequestDto,UPDATE_CONSENT_REQUEST_ROUTING_KEY ,UPDATE_USER_EXCHANGE,new HashMap<>());

        return true;
    }
    public boolean addCareContextDirectAuth(ABDMAuthNotifyRequest abdmAuthNotifyRequest) {
        try{
            ABDMLinkCareContextEvent careContextDetails = getCareContextLinkingDetailsFor(abdmAuthNotifyRequest.getAuth().transactionId);
            LOGGER.info("ABDMLinkCareContextEvent "+ Utils.asJsonString(careContextDetails));
            ABDMAuthOnNotifyRequest abdmAuthOnNotifyRequest = new ABDMAuthOnNotifyRequest(abdmAuthNotifyRequest.requestId);
            abdmApiHelper.authOnNotify(abdmAuthOnNotifyRequest);
            if(!abdmAuthNotifyRequest.getAuth().status.equals("GRANTED")) {
                return true;
            }
            ABDMLinkCareContextRequest linkingRequest = new ABDMLinkCareContextRequest(
                    abdmAuthNotifyRequest.getAuth().accessToken,
                    Long.toString(careContextDetails.personId),
                    Long.toString(careContextDetails.episodeId),
                    careContextDetails.personName,
                    careContextDetails.enrollmentDate);
            CacheUtils.putIntoCache(linkingRequest.requestId, careContextDetails.abhaAddress, 7200);
            String response = abdmApiHelper.addCareContextRequest(linkingRequest);
            if (null != response) {
                return true;
            }
        }catch(Exception ex)
        {
            LOGGER.info("[addCareContextDirectAuth] " + ex.toString());
            Sentry.capture(ex);
        }
        return false;
    }

    public void processConsentNotification(ABDMConsentNotification abdmConsentNotification) {
        saveClientDetails(CONSENT_NOTIFICATION_EVENT,Utils.asJsonString(abdmConsentNotification));
        ABDMConsentRequestDto abdmConsentRequestDto = new ABDMConsentRequestDto();
        abdmConsentRequestDto.setAbdmConsentNotification(abdmConsentNotification);
        abdmConsentRequestDto.setConsentRequestId(abdmConsentNotification.getNotification().getConsentRequestId());
        rmqPublisher.send(abdmConsentRequestDto,UPDATE_CONSENT_REQUEST_ROUTING_KEY ,UPDATE_USER_EXCHANGE,new HashMap<>());
        ABDMConsentOnNotifyRequest abdmConsentOnNotifyRequest = new ABDMConsentOnNotifyRequest(abdmConsentNotification.getRequestId(),abdmConsentNotification.getNotification().getConsentArtefacts());
        abdmApiHelper.hiuConsentOnNotify(abdmConsentOnNotifyRequest);
    }

}

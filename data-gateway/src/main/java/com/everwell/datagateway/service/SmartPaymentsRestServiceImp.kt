package com.everwell.datagateway.service

import com.everwell.datagateway.constants.Constants
import com.everwell.datagateway.exceptions.ValidationException
import com.everwell.datagateway.models.response.ApiResponse
import com.everwell.datagateway.models.response.GenericAuthResponse
import com.everwell.datagateway.models.response.MicroServiceGenericAuthResponse
import org.slf4j.LoggerFactory
import org.springframework.core.ParameterizedTypeReference
import org.springframework.stereotype.Service
import org.springframework.util.LinkedMultiValueMap
import java.util.*

@Service
open class SmartPaymentsRestServiceImp : SmartPaymentsRestService() {

    private val LOGGER = LoggerFactory.getLogger(SmartPaymentsRestServiceImp::class.simpleName)


    override fun getAuthToken(client_id: String, client_secret: String): String? {
        if (!clientIdAccessTokenMap.contains(client_id) || Date().time >= clientIdRefreshTimeMap[client_id]!!) {
            LOGGER.debug("[Smart Payments] Fetching auth token...")
            val params: LinkedMultiValueMap<String, String> = LinkedMultiValueMap()
            params["grant_type"] = appProperties.smartPaymentUserGrantType
            params["client_id"] = client_id
            params["client_secret"] = client_secret
            val resp = authenticate(
                appProperties.smartPaymentsUrl + "/oauth2/token",
                HashMap(),
                params,
                object : ParameterizedTypeReference<GenericAuthResponse>() {})

            clientIdAccessTokenMap[client_id] = resp.body?.access_token!!
            clientIdRefreshTimeMap[client_id] = Date().time + Constants.AUTH_SERVER_REFRESH_EXPIRY
        }
        return clientIdAccessTokenMap[client_id]
    }

    override fun getAuthToken(client_id: String?): String? {
        throw ValidationException("[getAuthToken(client_id)] not implemented")
    }
}
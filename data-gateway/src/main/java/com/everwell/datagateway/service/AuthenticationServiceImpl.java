package com.everwell.datagateway.service;

import com.everwell.datagateway.entities.Client;
import com.everwell.datagateway.enums.AuthenticationTypeEnum;
import com.everwell.datagateway.enums.StatusCodeEnum;
import com.everwell.datagateway.exceptions.LoginException;
import com.everwell.datagateway.handlers.AuthenticationHandlerMap;
import com.everwell.datagateway.models.dto.ClientAuthenticationDto;
import com.everwell.datagateway.models.request.AuthenticationRequest;
import com.everwell.datagateway.models.request.RegisterRequest;
import com.everwell.datagateway.utils.JWTUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;


@Service
public class AuthenticationServiceImpl implements AuthenticationService {


    private static Logger logger = LoggerFactory.getLogger(AuthenticationServiceImpl.class);

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private ClientService clientService;

    @Autowired
    private ClientAuthenticationService clientAuthenticationService;

    @Autowired
    private AuthenticationHandlerMap authenticationHandlerMap;

    @Value("${default.service.client_id}")
    private Long defaultAccessibleClient;

    @Override
    public String generateToken(AuthenticationRequest authenticationRequest) {
        try {
            Authentication auth = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(), authenticationRequest.getPassword()));
            return JWTUtil.createJWT(auth);
        } catch (BadCredentialsException | DisabledException e) {
            throw new LoginException(StatusCodeEnum.INVALID_CREDENTIALS);
        }
    }

    @Override
    public Boolean registerClient(RegisterRequest registerRequest) {
        if (registerRequest.getUsername()!=null && !clientService.isClientExists(registerRequest.getUsername())) {
            ClientAuthenticationDto clientAuthenticationDto = new ClientAuthenticationDto(registerRequest.getCredentialType(), registerRequest.getCredentials(), AuthenticationTypeEnum.CLIENT, 0L);
            clientAuthenticationService.validateCredentials(clientAuthenticationDto);
            Client client = new Client();
            client.setUsername(registerRequest.getUsername());
            client.setPassword(bCryptPasswordEncoder.encode(registerRequest.getPassword()));
            client.setFailureEmail(registerRequest.getFailureEmail());
            client.setIp(registerRequest.getIp());
            client.setAccessibleClient(null != registerRequest.getAccessibleClient() ? registerRequest.getAccessibleClient() : defaultAccessibleClient);
            clientService.saveClient(client);
            clientAuthenticationDto.setTypeId(client.getId());
            clientAuthenticationService.saveAuthenticationDetails(clientAuthenticationDto);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String getCurrentUsername(Authentication authentication) {
        return authentication.getName();
    }


}

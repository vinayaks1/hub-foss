package com.everwell.datagateway.service

import com.everwell.datagateway.constants.Constants
import com.everwell.datagateway.models.response.ApiResponse
import com.everwell.datagateway.models.response.MicroServiceGenericAuthResponse
import org.slf4j.LoggerFactory
import org.springframework.core.ParameterizedTypeReference
import org.springframework.stereotype.Service
import java.util.Date


@Service
open class RegistryRestServiceImp : RegistryRestService() {

    private val LOGGER = LoggerFactory.getLogger(RegistryRestServiceImp::class.simpleName)

    override fun getAuthToken(client_id: String?): String? {
        if (!clientIdAccessTokenMap.contains(client_id) || Date().time >= clientIdRefreshTimeMap[client_id]!!) {
            LOGGER.debug("[Registry] Fetching auth token...")
            val headers = mutableMapOf<String, String>()
            headers["X-Client-Id"] = client_id!!
            val resp = authenticate(
                    appProperties.registryServerUrl + "/v1/client",
                    headers,
                    object : ParameterizedTypeReference<ApiResponse<MicroServiceGenericAuthResponse>>() {})
            clientIdAccessTokenMap[client_id] = resp.body?.data?.authToken!!
            if (resp.body?.data?.nextRefresh == null ||  resp.body?.data?.nextRefresh!! == 0L)
                clientIdRefreshTimeMap[client_id] = Date().time + Constants.MICROSERVICE_REFRESH_EXPIRY
            else
                clientIdRefreshTimeMap[client_id] = resp.body?.data?.nextRefresh!!
        }
        return clientIdAccessTokenMap[client_id]
    }

}

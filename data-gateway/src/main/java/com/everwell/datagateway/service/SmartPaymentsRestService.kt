package com.everwell.datagateway.service

abstract class SmartPaymentsRestService : BaseRestService() {

    abstract fun getAuthToken(client_id: String, client_secret: String): String?
}
package com.everwell.datagateway.service;

import com.everwell.datagateway.entities.ClientRequests;
import com.everwell.datagateway.models.request.RefIdStatusRequest;
import com.everwell.datagateway.models.response.RefIdStatusResponse;

import java.util.Date;

public interface ClientRequestsService {
    Long saveClientRequest(ClientRequests clientRequests);

    void setIsDeliveredTrueAndDeliveredAtTime(Long referenceId, Date deliveredAt);


    ClientRequests getClientRequestsById(Long id);

    RefIdStatusResponse getStatusByRefId(RefIdStatusRequest refIdStatusRequest);

    Long updateClientRequest(ClientRequests clientRequests);

    void archiveClientRequestData();
    ClientRequests updateClientRequestbasedonRefId(Long refId, String data);
}

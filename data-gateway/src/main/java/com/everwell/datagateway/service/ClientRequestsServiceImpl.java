package com.everwell.datagateway.service;

import com.everwell.datagateway.entities.*;
import com.everwell.datagateway.enums.RefIdStatusEnum;
import com.everwell.datagateway.models.request.RefIdStatusRequest;
import com.everwell.datagateway.models.response.RefIdStatusResponse;
import com.everwell.datagateway.repositories.ClientRequestsArchiveRepository;
import com.everwell.datagateway.repositories.ClientRequestsRepository;
import com.everwell.datagateway.repositories.EventClientRepository;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
public class ClientRequestsServiceImpl implements ClientRequestsService {

    private static Logger LOGGER = LoggerFactory.getLogger(ClientRequestsServiceImpl.class);

    @Autowired
    private ClientRequestsRepository clientRequestsRepository;

    @Autowired
    private ClientRequestsArchiveRepository clientRequestsArchiveRepository;
    @Autowired
    private SubscriberUrlService subscriberUrlService;

    @Autowired
    private EventClientRepository eventClientRepository;

    @Override
    public Long saveClientRequest(ClientRequests clientRequests) {
        return clientRequestsRepository.save(clientRequests).getId();
    }

    @Override
    public Long updateClientRequest(ClientRequests clientRequests) {
        return clientRequestsRepository.save(clientRequests).getId();
    }

    @Override
    public void setIsDeliveredTrueAndDeliveredAtTime(Long id, Date deliveredAt) {
        ClientRequests clientRequests = clientRequestsRepository.findTopById(id);
        if (null == clientRequests) {
            LOGGER.error("The response was sent to subscriber url, but id doesn't exist :" + id);
        }
        else {
            clientRequests.setResultDelivered(true);
            clientRequests.setDeliveredAt(deliveredAt);
            clientRequestsRepository.save(clientRequests);
        }
    }

    @Override
    public ClientRequests getClientRequestsById(Long id) {
        return clientRequestsRepository.findTopById(id);
    }

    @Override
    public RefIdStatusResponse getStatusByRefId(RefIdStatusRequest refIdStatusRequest) {
        ClientRequests clientRequests = getClientRequestsById(refIdStatusRequest.getReferenceId());
        String status = "";
        RefIdStatusResponse refIdStatusResponse = new RefIdStatusResponse();
        if(null == clientRequests)
        {
            status = RefIdStatusEnum.REQUEST_COMPLETED_OR_NOT_FOUND.getStatus();
        }
        else if(clientRequests.getResultDelivered())
        {
            refIdStatusResponse.setPayload(clientRequests.getResponseData());
            status = RefIdStatusEnum.REQUEST_COMPLETED_AND_NOTIFIED.getStatus();
        }
        else {
            refIdStatusResponse.setPayload(clientRequests.getResponseData());
            status = RefIdStatusEnum.REQUEST_IN_PROCESS.getStatus();
        }
        refIdStatusResponse.setReferenceId(refIdStatusRequest.getReferenceId());
        refIdStatusResponse.setStatus(status);
        return refIdStatusResponse;
    }
    @Value("${client.archive.enable}")
    @Setter
    private boolean clientRequestArchivalEnabled;

    @Override
    @Scheduled(fixedDelay = 24 * 60 * 60 * 1000)
    public void archiveClientRequestData(){
        if(clientRequestArchivalEnabled) {
            Date deliveredAtDateTime = Date.from(ZonedDateTime.now().minusMonths(1).toInstant());
            List<ClientRequests> clientRequests = clientRequestsRepository.findByDeliveredAtLessThanEqual(deliveredAtDateTime);
            List<ClientRequestsArchive> clientRequestsArchives = Collections.synchronizedList(new ArrayList<>());
            clientRequests.parallelStream().forEach(request -> {
                if(request.getEvent()!=null && request.getSubscriberUrl()!=null && request.getClient()!=null) {
                    ClientRequestsArchive clientRequestsArchive = new ClientRequestsArchive(
                            request.getId(), request.getCreatedAt(), request.getResultDelivered(),
                            request.getClient().getId(), request.getEvent().getId(), request.getSubscriberUrl().getId(),
                            request.getResponseData(), request.getRequestData(), request.getDeliveredAt(),
                            request.getRefIdDeliveredAt(), request.getCallbackResponseCode()
                    );
                    clientRequestsArchives.add(clientRequestsArchive);
                }
            });
            clientRequestsArchiveRepository.saveAll(clientRequestsArchives);
            clientRequestsRepository.deleteByDeliveredAtLessThanEqual(deliveredAtDateTime);
        } else {
            LOGGER.error("Client Request Archival Job is not Enabled");
        }
    }
    @Override
    public ClientRequests updateClientRequestbasedonRefId(Long refId, String data)
    {
        ClientRequests clientRequests = getClientRequestsById(refId);
        Client client = clientRequests.getClient();
        Event event = clientRequests.getEvent();
        EventClient eventClient = eventClientRepository.findByClientIdAndEventId(client.getId(), event.getId());
        SubscriberUrl subscriberUrl = subscriberUrlService.findByEventClientId(eventClient);
        clientRequests.setSubscriberUrl(subscriberUrl);
        clientRequests.setResponseData(data);
        updateClientRequest(clientRequests);
        return clientRequests;
    }

}

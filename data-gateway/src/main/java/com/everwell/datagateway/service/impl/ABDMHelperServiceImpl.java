package com.everwell.datagateway.service.impl;

import com.everwell.datagateway.exceptions.RestTemplateException;
import com.everwell.datagateway.models.request.AbdmClientDto.HipOnRequestDto;
import com.everwell.datagateway.models.request.abdm.ABDMConsentInitiationRequest;
import com.everwell.datagateway.models.response.RestServiceResponse;
import com.everwell.datagateway.service.ABDMHelperService;
import com.everwell.datagateway.utils.Utils;
import io.sentry.Sentry;
import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
public class ABDMHelperServiceImpl implements ABDMHelperService {

    private static Logger LOGGER = LoggerFactory.getLogger(ABDMHelperServiceImpl.class);

    private static String token;

    @Value("${NDHM_API_ENDPOINT:}")
    String baseUrl;

    @Value("${NDHM_X_CM_ID:}")
    String cmId;

    @Value("${NDHM_HIP_NAME:}")
    String hipId;

    @Value("${NDHM_CLIENT_ID:}")
    String clientId;

    @Value("${NDHM_CLIENT_SECRET:}")
    String clientSecret;

    @Value("${NDHM_MAX_TRIES:}")
    int maxTries;

    @Value("${NDHM_HIU_NAME:}")
    String hiuId;


    private RestTemplate client = new RestTemplate();

    public void getToken()
    {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        JSONObject postBodyJson = new JSONObject();
        postBodyJson.put("clientId",clientId);
        postBodyJson.put("clientSecret",clientSecret);
        HttpEntity<String> request = new HttpEntity<String>(postBodyJson.toString() ,headers);
        String url = baseUrl+"/v0.5/sessions";
        ResponseEntity<Map> response = client.postForEntity(url,request, Map.class);
        token = response.getBody().get("accessToken").toString();
    }

    @Override
    public void hipOnRequestCall(HipOnRequestDto data) {
        if(StringUtils.isBlank(token)){
            getToken();
        }

        String url = baseUrl + " /v0.5/health-information/hip/on-request";
        int tryCount = 0;

        while(tryCount <= maxTries) {

            tryCount++;
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("X-CM-ID",cmId);
            headers.set("X-HIP-ID",hipId);
            headers.setBearerAuth(token);

            String jsonData = Utils.asJsonString(data);
            HttpEntity<String> request = new HttpEntity<>(jsonData, headers);

            Map<Integer,String> response = exchange(url,request);
            Map.Entry<Integer,String> map = response.entrySet().iterator().next();
            if (map.getKey() == HttpStatus.UNAUTHORIZED.value()) {
                getToken();
            }
            else if(map.getKey() == HttpStatus.ACCEPTED.value()) {
                LOGGER.info("[hipOnRequestCall] " + map.getKey());
                break;
            }
            else {
                LOGGER.warn("[hipOnRequestCall] " +map.getKey() + "\n" + map.getValue());
                throw new RestTemplateException(map.getKey(),map.getValue() + "[request body] " + data);
            }
        }
    }

    @Override
    public boolean pushToCallBackUrl(String url, String data) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(data, headers);
        ResponseEntity<String> response = client.postForEntity(url, request, String.class);
        if (response.getStatusCode().equals(HttpStatus.ACCEPTED)) {
            LOGGER.info("[pushToCallBackUrl] " + response.getStatusCode());
            return true;
        }
        else {
            LOGGER.warn("[pushToCallBackUrl] " + response.getStatusCode() + "\n" + response.getBody());
            throw new RestTemplateException(response.getStatusCode().value(), response.getBody()+ "[request body] " + data);
        }
    }

    @Override
    public void hipHiTransferNotify(String data) {
        if(StringUtils.isBlank(token)){
            getToken();
        }

        LOGGER.info(token);
        String url = baseUrl + " /v0.5/health-information/notify";
        int tryCount = 0;

        while(tryCount <= maxTries) {

            tryCount++;
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("X-CM-ID",cmId);
            headers.set("X-HIP-ID",hipId);
            headers.setBearerAuth(token);

            HttpEntity<String> request = new HttpEntity<>(data, headers);
            Map<Integer,String> response = exchange(url,request);
            Map.Entry<Integer,String> map = response.entrySet().iterator().next();
            if (map.getKey() == HttpStatus.UNAUTHORIZED.value()) {
                getToken();
            }
            else if(map.getKey() == HttpStatus.ACCEPTED.value()) {
                LOGGER.info("[hipHiTransferNotify] " + map.getKey());
                break;
            }
            else {
                LOGGER.warn("[hipHiTransferNotify] " +map.getKey() + "\n" + map.getValue());
                throw new RestTemplateException(map.getKey(),map.getValue() + "[request body] " + data);
            }
        }
    }

    @Override
    public void consentRequestInit(ABDMConsentInitiationRequest req) {
        if(StringUtils.isBlank(token)){
            getToken();
        }

        String url = baseUrl + "/v0.5/consent-requests/init";
        int tryCount = 0;

        while(tryCount <= maxTries) {

            tryCount++;
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("X-CM-ID",cmId);
            headers.set("X-HIU-ID",hiuId);
            headers.setBearerAuth(token);

            String jsonData = Utils.asJsonString(req);
            HttpEntity<String> request = new HttpEntity<>(jsonData, headers);

            Map<Integer,String> response = exchange(url,request);
            Map.Entry<Integer,String> map = response.entrySet().iterator().next();
            if (map.getKey() == HttpStatus.UNAUTHORIZED.value()) {
                getToken();
            }
            else if(map.getKey() == HttpStatus.ACCEPTED.value()) {
                LOGGER.debug("[ABDMConsentInitiationRequest] " + map.getKey());
                break;
            }
            else {
                LOGGER.warn("[ABDMConsentInitiationRequest] " +map.getKey() + "\n" + map.getValue());
                throw new RestTemplateException(map.getKey(),map.getValue() + "[request body] " + req);
            }
        }
    }


    public Map<Integer,String> exchange(String url, HttpEntity<String> request) {
        Map<Integer,String> object = new HashMap<>();
        try
        {
            ResponseEntity<String> response = client.postForEntity(url, request, String.class);
            object.put(response.getStatusCode().value(),response.toString());
        }
        catch (RestClientResponseException e) {
            object.put(e.getRawStatusCode(),e.getResponseBodyAsString());
        }
        return object;
    }

}

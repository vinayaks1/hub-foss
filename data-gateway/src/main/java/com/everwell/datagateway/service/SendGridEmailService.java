package com.everwell.datagateway.service;

import com.everwell.datagateway.controllers.AuthenticationController;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class SendGridEmailService implements EmailService {

    private static Logger logger = LoggerFactory.getLogger(AuthenticationController.class);

    private SendGrid sendGrid;

    @Autowired
    public SendGridEmailService(SendGrid sendGrid) {
        this.sendGrid = sendGrid;
    }

    @Override
    public void sendText(String from, String to, String subject, String body) {
        Response response = sendEmail(from, to, subject, new Content("text/plain", body));
    }


    @Override
    public String makeContentClient(String eventName, String errorMessage) {
        return "Message not published for " + eventName + "\n\n\n Error Message: " + errorMessage.substring(0,Math.min(errorMessage.length(), 500));
    }

    @Override
    public String makeContentDeveloper(String clientName, String eventName, String message, String errorMessage) {
        return "Client : "+ clientName + "\n Event : " + eventName + " \n\n\n Message:" + message.substring(0, Math.min(message.length(), 400)) + " not published " + "\n\n\n Error Message: " + errorMessage.substring(0,Math.min(errorMessage.length(), 500));
    }


    public Response sendEmail(String from, String to, String subject, Content content) {
        Mail mail = new Mail(new Email(from), subject, new Email(to), content);
        //  mail.setReplyTo(new Email(""));
        Request request = new Request();
        Response response = null;
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            response = this.sendGrid.api(request);
            return response;
        } catch (IOException ex) {
            logger.error("Exception in send Response Sendmail is " + ex.getMessage());
            return new Response();
        }

    }
}
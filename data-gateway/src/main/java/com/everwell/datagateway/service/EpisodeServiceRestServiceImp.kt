package com.everwell.datagateway.service

import com.everwell.datagateway.constants.Constants
import com.everwell.datagateway.constants.routes.EpisodeRoutes
import com.everwell.datagateway.exceptions.PDQMCustomException
import com.everwell.datagateway.models.request.EpisodeSearchRequest
import com.everwell.datagateway.models.response.*
import com.everwell.datagateway.utils.Utils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpMethod
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.util.*

@Service
open class EpisodeServiceRestServiceImp : EpisodeServiceRestService() {

    private val logger : Logger = LoggerFactory.getLogger(EpisodeServiceRestServiceImp::class.simpleName)
    override fun getEpisodesFromPerson(token: String,userId : Int): ApiResponse<List<GetEpisodeByPersonResponse>> {
        logger.info("[EpisodeServiceImpl] Get Episodes Request...")
        val headers = mutableMapOf<String, String>()
        headers["Authorization"] = "Bearer $token"
        headers["X-Client-Id"] = appProperties.genericClientId
        val response = authenticate(
                appProperties.episodeServiceUrl + "/v1/episodes/personId/" + userId + "?includeDetails=false",
                headers,
                object:  ParameterizedTypeReference<ApiResponse<List<GetEpisodeByPersonResponse>>>() {})
        logger.info("[user id]" + response.body?.data?.get(0))
        return response.body!!
    }

    override fun updateEpisodeDetails(token: String, data: Map<String, Any>): ApiResponse<GetEpisodeByPersonResponse> {
        logger.info("[EpisodeServiceImpl] Update Episodes Request...")
        val headers = mutableMapOf<String, String>()
        headers["Authorization"] = "Bearer $token"
        headers["X-Client-Id"] = appProperties.genericClientId
        val response = authenticate(
                appProperties.episodeServiceUrl + "/v1/episode",
                headers,
                data,
                HttpMethod.PUT,
                object:  ParameterizedTypeReference<ApiResponse<GetEpisodeByPersonResponse>>() {})
        logger.info("[updateEpisodeDetails]" + response.body?.data)
        return response.body!!
    }

    override fun getAuthToken(client_id: String?): String? {
        if(!clientIdAccessTokenMap.contains(client_id) || Date().time >= clientIdRefreshTimeMap[client_id]!!){
            logger.info("[EpisodeServiceImpl] Fetching auth token...")
            val headers = mutableMapOf<String, String>()
            headers["X-Client-Id"] = client_id!!
            val response = authenticate(
                    appProperties.episodeServiceUrl + "/v1/client",
                    headers,
                    object:  ParameterizedTypeReference<ApiResponse<MicroServiceGenericAuthResponse>>() {})

            clientIdAccessTokenMap[client_id] = response.body?.data?.authToken !!
            if (response.body?.data?.nextRefresh == null ||  response.body?.data?.nextRefresh!! == 0L)
                clientIdRefreshTimeMap[client_id] = Date().time + Constants.MICROSERVICE_REFRESH_EXPIRY
            else
                clientIdRefreshTimeMap[client_id] = response.body?.data?.nextRefresh!!
        }
        return clientIdAccessTokenMap[client_id]
    }

    override fun episodeSearch(episodeSearchRequest: EpisodeSearchRequest, clientId: Long): EpisodeSearchResult {
        logger.debug("[EpisodeServiceImpl] Episode search request accepted")
        val headers = getAuthorizationHeaders(clientId.toString())
        var response : ResponseEntity<ApiResponse<EpisodeSearchResult>>? = null
        try {
            response = authenticate(
                    appProperties.episodeServiceUrl + EpisodeRoutes.EPISODE_SEARCH.path,
                    headers,
                    episodeSearchRequest,
                    HttpMethod.POST,
                    object:  ParameterizedTypeReference<ApiResponse<EpisodeSearchResult>>() {})
        } catch (ex: Exception) {
            throw Exception(ex.message)
        }
        return response.body.data!!
    }

    override fun createEpisode(createEpisodeRequest: Map<String, Object>, clientId: Long): ApiResponse<Episode> {
        logger.debug("[EpisodeServiceImpl] Create episode request accepted")
        val headers = getAuthorizationHeaders(clientId.toString())
        val response = authenticate(
                appProperties.episodeServiceUrl + EpisodeRoutes.CREATE_EPISODE.path,
                headers,
                createEpisodeRequest,
                HttpMethod.POST,
                object:  ParameterizedTypeReference<ApiResponse<Episode>>() {})
        return response.body!!
    }

    override fun getDefaultDiseaseForClientId(clientId: Long): ApiResponse<DiseaseTemplate> {
        logger.debug("[EpisodeServiceImpl] Default disease for clientId request accepted")
        val headers = getAuthorizationHeaders(clientId.toString())
        val response = authenticate(
                appProperties.episodeServiceUrl + EpisodeRoutes.DEFAULT_DISEASE.path,
                headers,
                object:  ParameterizedTypeReference<ApiResponse<DiseaseTemplate>>() {})
        return response.body!!
    }

    override fun updateEpisode(createEpisodeRequest: Map<String, Object>, clientId: Long): ApiResponse<Episode> {
        logger.debug("[EpisodeServiceImpl] Update episode request accepted")
        val headers = getAuthorizationHeaders(clientId.toString())
        val response = authenticate(
                appProperties.episodeServiceUrl + EpisodeRoutes.UPDATE_EPISODE.path,
                headers,
                createEpisodeRequest,
                HttpMethod.PUT,
                object:  ParameterizedTypeReference<ApiResponse<Episode>>() {})
        return response.body!!
    }

    override fun deleteEpisode(episodeId: Long, clientId: Long): ApiResponse<String> {
        logger.debug("[EpisodeServiceImpl] Delete episode request accepted")
        val headers = getAuthorizationHeaders(clientId.toString())
        val deleteEpisodeRequest: MutableMap<String, Any> = mutableMapOf()
        deleteEpisodeRequest[Constants.EPISODE_ID] = episodeId
        val response = authenticate(
                appProperties.episodeServiceUrl + EpisodeRoutes.DELETE_EPISODE.path,
                headers,
                deleteEpisodeRequest,
                HttpMethod.POST,
                object:  ParameterizedTypeReference<ApiResponse<String>>() {})
        return response.body
    }

    private fun getAuthorizationHeaders(clientId : String) : MutableMap<String, String> {
        val headers = mutableMapOf<String, String>()
        val token = getAuthToken(clientId)
        headers[Constants.AUTHORIZATION] = Constants.BEARER + " " + "$token"
        headers[Constants.X_CLIENT_ID] = clientId
        return headers
    }

}
package com.everwell.datagateway.service

import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.constants.Constants.NIKSHAY_REFRESH_EXPIRY
import com.everwell.datagateway.entities.Client
import com.everwell.datagateway.models.response.ApiResponse
import com.everwell.datagateway.models.response.GenericAuthResponse
import com.everwell.datagateway.models.response.LinkInitiationOtpResponse
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpMethod
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import org.springframework.util.LinkedMultiValueMap
import org.springframework.web.client.RestClientResponseException
import org.springframework.web.client.RestTemplate
import java.util.*
import kotlin.collections.HashMap


@Service
class NikshayRestServiceImp : NikshayRestService() {

    private val logger: Logger = LoggerFactory.getLogger(NikshayRestServiceImp::class.simpleName)
    override fun sendOtpLinkInitiation(token: String, userId: String): ApiResponse<LinkInitiationOtpResponse> {
        val headers = mutableMapOf<String, String>()
        headers["Authorization"] = "Bearer $token"
        val response = authenticate(
                appProperties.nikshayServerUrl + "api/Patients/SendOtpForLinkInitiation/" + userId,
                headers,
                HashMap(),
                HttpMethod.POST,
                object:  ParameterizedTypeReference<ApiResponse<LinkInitiationOtpResponse>>() {})
        logger.info("[sendOtpLinkInitiation]" + response.body)
        return response.body!!
    }

    override fun getAuthToken(client_id: String?): String? {
        val client = provideCurrentClient()
        if(accessToken!=null && client!=null && client.authTokenForProxy==null){
            client.authTokenForProxy = accessToken
            clientService.saveClient(client)
        } else {
            val params: LinkedMultiValueMap<String, String> = LinkedMultiValueMap()
            params["grant_type"] = appProperties.nikshayUserGrantType
            params["username"] = appProperties.nikshayUserName
            params["password"] = appProperties.nikshayUserPassword
            val resp = authenticate(
                    appProperties.nikshayServerUrl + "Token",
                    HashMap(),
                    params,
                    object : ParameterizedTypeReference<GenericAuthResponse>() {})
            accessToken = resp.body?.access_token
            saveAccessToken(accessToken)
        }
        return accessToken
    }

     override fun getAuthTokenForLims(client_id: String?): String? {
         if (!clientIdAccessTokenMap.contains(client_id) || Date().time >= clientIdRefreshTimeMap[client_id]!!) {
             val params: LinkedMultiValueMap<String, String> = LinkedMultiValueMap()
             params["grant_type"] = appProperties.limsNikshayUserGrantType
             params["username"] = appProperties.limsNikshayUserName
             params["password"] = appProperties.limsNikshayUserPassword
             val resp = authenticate(
                     appProperties.nikshayServerUrl + "Token",
                     HashMap(),
                     params,
                     object : ParameterizedTypeReference<GenericAuthResponse>() {})
             clientIdAccessTokenMap[client_id!!] = resp.body?.access_token !!
             if (resp.body?.expires_in == null ||  resp.body?.expires_in == 0L)
                 clientIdRefreshTimeMap[client_id] = Date().time + NIKSHAY_REFRESH_EXPIRY
             else
                 clientIdRefreshTimeMap[client_id] = (resp.body?.expires_in!! * 1000 ) + Date().time
         }

         return clientIdAccessTokenMap[client_id]
    }

}
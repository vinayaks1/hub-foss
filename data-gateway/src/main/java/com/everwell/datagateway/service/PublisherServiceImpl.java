package com.everwell.datagateway.service;

import com.everwell.datagateway.constants.Constants;
import com.everwell.datagateway.entities.Client;
import com.everwell.datagateway.entities.ClientRequests;
import com.everwell.datagateway.entities.Event;
import com.everwell.datagateway.entities.PublisherQueueInfo;
import com.everwell.datagateway.enums.StatusCodeEnum;
import com.everwell.datagateway.exceptions.ForbiddenException;
import com.everwell.datagateway.exceptions.NotFoundException;
import com.everwell.datagateway.handlers.EventHandler;
import com.everwell.datagateway.handlers.EventHandlerMap;
import com.everwell.datagateway.models.dto.PublisherData;
import com.everwell.datagateway.models.response.PublisherResponse;
import com.everwell.datagateway.repositories.PublisherQueueInfoRepository;
import io.sentry.Sentry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class PublisherServiceImpl implements PublisherService {


    @Autowired
    private ClientRequestsService clientRequestsService;

    @Autowired
    private EventService eventService;

    @Autowired
    private ClientService clientService;

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private EventHandlerMap eventHandlerMap;

    @Autowired
    private PublisherQueueInfoRepository publisherQueueInfoRepository;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    private static Logger LOGGER = LoggerFactory.getLogger(PublisherServiceImpl.class);

    @Override
    public PublisherResponse publishRequest(String data, String eventName) {
        Event event = eventService.findEventByEventName(eventName);
        PublisherQueueInfo publisherQueueInfo = publisherQueueInfoRepository.findFirstByEventName(eventName);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String clientName = authenticationService.getCurrentUsername(authentication);
        Client client = clientService.findByUsername(clientName);
        if (null == event || client == null) {
            StatusCodeEnum statusCodeEnum = StatusCodeEnum.CLIENT_OR_EVENT_NOT_FOUND;
            throw new NotFoundException(statusCodeEnum.getCode().value(), statusCodeEnum.getMessage());
        }
        if (!clientService.isClientSubscribedToEvent(client.getId(), event.getId())) {
            throw new ForbiddenException(StatusCodeEnum.CLIENT_NOT_SUBSCRIBED_TO_EVENT_OR_DISABLED.getMessage());
        }
        EventHandler eventHandler = eventHandlerMap.getHandler(eventName);
        if (null == eventHandler) {
            throw new NotFoundException(StatusCodeEnum.NOT_FOUND_HANDLER);
        }
        ClientRequests clientRequests = new ClientRequests();
        clientRequests.setEvent(event);
        clientRequests.setClient(client);
        clientRequests.setRequestData(data);
        Long referenceId = clientRequestsService.saveClientRequest(clientRequests);
        PublisherData publisherData = PublisherData.builder()
                .referenceId(referenceId)
                .clientName(clientName)
                .clientId(client.getId())
                .data(data)
                .build();
        eventHandler.publishEvent(publisherData, publisherQueueInfo);
        PublisherResponse publisherResponse = new PublisherResponse();
        publisherResponse.setReferenceId(referenceId);
        clientRequests.setRefIdDeliveredAt(new java.util.Date());
        clientRequestsService.updateClientRequest(clientRequests);
        return publisherResponse;
    }

    @Override
    public Map<String, String> publishRequestForABDM(String data, String eventName) {
        LOGGER.info("publishRequestForABDM: " + eventName);
        Map<String, String> map = new HashMap<String, String>();
        try {
            PublisherResponse publisherResponse = publishRequest(data, eventName);
            LOGGER.info("publishRequestForABDM Success ");
            map.put(Constants.SUCCESS, "publisherResponse");
        } catch (NotFoundException | ForbiddenException ex) {
            LOGGER.error("Error during Publish ABDM Consent Request " + ex);
            Sentry.capture(ex);
            map.put(Constants.ERROR, ex.getMessage());
        }
        return map;
    }
}

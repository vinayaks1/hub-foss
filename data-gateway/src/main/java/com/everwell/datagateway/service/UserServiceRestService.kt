package com.everwell.datagateway.service

import com.everwell.datagateway.models.response.ApiResponse

abstract class UserServiceRestService : BaseRestService() {
    abstract fun searchUser(token: String,data: Map<String,String>) : ApiResponse<Int>
}
package com.everwell.datagateway.service

import com.everwell.datagateway.constants.Constants
import com.everwell.datagateway.models.response.ApiResponse
import com.everwell.datagateway.models.response.MicroServiceGenericAuthResponse
import com.everwell.datagateway.models.response.UserServiceSearchResponse
import org.slf4j.LoggerFactory
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpMethod
import org.springframework.stereotype.Service
import java.util.*

@Service
open class UserServiceRestServiceImpl : UserServiceRestService() {

    private val LOGGER = LoggerFactory.getLogger(UserServiceRestServiceImpl::class.simpleName)


    override fun searchUser(token: String,data: Map<String,String>) : ApiResponse<Int> {
        LOGGER.info("[UserServiceImpl] User Search Request...")
        val headers = mutableMapOf<String, String>()
        headers["X-US-Access-Token"] = token
        headers["X-US-Client-Id"] = appProperties.genericClientId
        val response = authenticate(
                appProperties.userServiceUrl + "/v1/users/search",
                headers,
                data,
                HttpMethod.POST,
                object:  ParameterizedTypeReference<ApiResponse<UserServiceSearchResponse>>() {})
        LOGGER.info("[user id]" + response.body?.data?.id)
        return ApiResponse(response.body?.success,response.body?.data?.id,response.body?.message)
    }

    override fun getAuthToken(client_id: String?): String? {
     if(!clientIdAccessTokenMap.contains(client_id) || Date().time >= clientIdRefreshTimeMap[client_id]!!){
         LOGGER.debug("[UserServiceImpl] Fetching auth token...")
         val headers = mutableMapOf<String, String>()
         headers["X-US-Client-Id"] = client_id!!
         val response = authenticate(
                 appProperties.userServiceUrl + "/v1/client",
                 headers,
                 object:  ParameterizedTypeReference<ApiResponse<MicroServiceGenericAuthResponse>>() {})

         clientIdAccessTokenMap[client_id] = response.body?.data?.authToken !!
         if (response.body?.data?.nextRefresh == null ||  response.body?.data?.nextRefresh!! == 0L)
             clientIdRefreshTimeMap[client_id] = Date().time + Constants.MICROSERVICE_REFRESH_EXPIRY
         else
             clientIdRefreshTimeMap[client_id] = response.body?.data?.nextRefresh!!
     }
        return clientIdAccessTokenMap[client_id]
    }


}
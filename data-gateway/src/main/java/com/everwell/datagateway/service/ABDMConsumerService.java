package com.everwell.datagateway.service;

import com.everwell.datagateway.apiHelpers.ABDMHelper;
import com.everwell.datagateway.entities.*;
import com.everwell.datagateway.enums.StatusCodeEnum;
import com.everwell.datagateway.exceptions.NotFoundException;
import com.everwell.datagateway.exceptions.RestTemplateException;
import com.everwell.datagateway.exceptions.URLDisabledException;
import com.everwell.datagateway.models.response.RestServiceResponse;
import com.everwell.datagateway.repositories.EventClientRepository;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ABDMConsumerService {


    private static Logger LOGGER = LoggerFactory.getLogger(ABDMConsumerService.class);

    @Autowired
    private ABDMHelper abdmHelper;

    @Autowired
    private SubscriberUrlService subscriberUrlService;

    @Autowired
    private EventClientRepository eventClientRepository;

    @Autowired
    private EventService eventService;

    @Autowired
    private ClientService clientService;

    public void publishToABDMSubscriberUrl(String clientName, String eventName, String data) {
        Client client = clientService.findByUsername(clientName);
        Event event = eventService.findEventByEventName(eventName);
        if (null == event) {
            LOGGER.error("[publishToABDMSubscriberUrl] This Event doesn't exists: " + eventName);
            StatusCodeEnum eventNotFoundSentry=StatusCodeEnum.EVENT_NOT_FOUND_SENTRY;
            throw new NotFoundException(eventNotFoundSentry.getCode().value(),eventNotFoundSentry.getMessage() + eventName);
        }
        if(null == client) {
            LOGGER.error("[publishToABDMSubscriberUrl] This Client doesn't exists: " + clientName);
            StatusCodeEnum clientNotFoundSentry=StatusCodeEnum.CLIENT_NOT_FOUND_SENTRY;
            throw new NotFoundException(clientNotFoundSentry.getCode().value(),clientNotFoundSentry.getMessage() + clientName);
        }
        EventClient eventClient = eventClientRepository.findByClientIdAndEventId(client.getId(), event.getId());
        SubscriberUrl subscriberUrl = subscriberUrlService.findByEventClientId(eventClient);
        if (null == subscriberUrl || !subscriberUrl.getIsEnabled()) {
            LOGGER.info("[publishToABDMSubscriberUrl] Subscriber url is null, or Subscriber url is disabled for EventClientId: " + eventClient.getClientId());
            StatusCodeEnum urlDisabledSentry=StatusCodeEnum.URL_DISABLED_SENTRY;
            throw new URLDisabledException(urlDisabledSentry.getCode().value(),urlDisabledSentry.getMessage() + eventClient.getClientId());
        }
        RestServiceResponse response = abdmHelper.publishToABDM(data, subscriberUrl.getUrl());
        LOGGER.info(response.getCode() + "\n" + response.getMessage());
        if (response.getCode() == 202) {
            LOGGER.info("[publishToABDMSubscriberUrl] Message Published for " + subscriberUrl.getUrl());
            // TODO - Consult to Classify different statuses or not, And also should there be check on mandatory fields
            try {
                Gson gson = new Gson();
                gson.toJson(response.getMessage());
                LOGGER.info(gson.toString());
                //Converted to JSON to use,if there are different success status in response ( Eg : Gramener )
            } catch (JsonParseException err) {
                LOGGER.error("[publishToABDMSubscriberUrl] JSON Parsing Error " + err.toString());
            }
        } else {
            throw new RestTemplateException(response.getCode(), response.getMessage());
        }
    }


}

package com.everwell.datagateway.service

import com.everwell.datagateway.component.AppProperties
import com.everwell.datagateway.entities.Client
import com.everwell.datagateway.models.response.GenericAuthResponse
import org.json.simple.JSONObject
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpMethod
import org.springframework.http.client.BufferingClientHttpRequestFactory
import org.springframework.http.client.SimpleClientHttpRequestFactory
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import org.springframework.util.LinkedMultiValueMap
import org.springframework.web.client.RestClientResponseException
import org.springframework.web.client.RestTemplate


@Service
class NikshayReportsRestServiceImp : NikshayReportsRestService() {

    private val logger: Logger = LoggerFactory.getLogger(NikshayRestServiceImp::class.simpleName)

    override fun getAuthToken(client_id: String?): String? {
        val client = provideCurrentClient()
        if(accessToken!=null && client!=null && client.authTokenForProxy==null){
            client.authTokenForProxy = accessToken
            clientService.saveClient(client)
        } else {
            val params: LinkedMultiValueMap<String, String> = LinkedMultiValueMap()
            params["grant_type"] = appProperties.nikshayReportsUserGrantType
            params["username"] = appProperties.nikshayReportsUserName
            params["password"] = appProperties.nikshayReportsUserPassword
            val resp = authenticate(
                    appProperties.nikshayReportsServerUrl + "Nikshayapiv2/Token",
                    HashMap(),
                    params,
                    object : ParameterizedTypeReference<GenericAuthResponse>() {})
            accessToken = resp.body?.access_token
            saveAccessToken(accessToken)
        }
        return accessToken
    }

}
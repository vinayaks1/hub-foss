package com.everwell.datagateway.service;

import ca.uhn.fhir.context.FhirContext;
import com.everwell.datagateway.constants.Constants;
import com.everwell.datagateway.enums.TypeOfEpisodeEnum;
import com.everwell.datagateway.exceptions.PDQMCustomException;
import com.everwell.datagateway.exceptions.ValidationException;
import com.everwell.datagateway.models.dto.ParamPrefixAndValue;
import com.everwell.datagateway.models.request.EpisodeSearchRequest;
import com.everwell.datagateway.models.request.RequestParamAndModifiers;
import com.everwell.datagateway.models.response.EpisodeSearchResult;
import com.everwell.datagateway.utils.FhirUtil;
import com.everwell.datagateway.utils.Utils;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.sentry.Sentry;
import org.hl7.fhir.r4.model.*;
import org.hl7.fhir.r4.model.codesystems.SearchParamType;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import springfox.documentation.spring.web.json.Json;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Service
public class PDQMServiceImpl implements PDQMService{
    @Override
    public void createEpisodeSearchRequest(EpisodeSearchRequest searchRequest, RequestParamAndModifiers paramAndModifiers) {
        switch (paramAndModifiers.getParamName()) {
            case Constants.GENDER:
                if(!Constants.SUPPORTED_GENDER_CODE.contains(Objects.toString(paramAndModifiers.getParamValues()[0], null))) {
                    throw new ValidationException(Constants.CODE_NOT_SUPPORTED);
                }
                Utils.capitalizeString(paramAndModifiers.getParamValues());
            case Constants.FIRST_NAME:
            case Constants.ADDRESS:
            case Constants.LAST_NAME:
                setSearchRequestForStringTypeFHIRParams(paramAndModifiers.getParamName(), paramAndModifiers.getParamModifier(), paramAndModifiers.getParamValues(), searchRequest);
                break;
            case Constants.COUNT:
                searchRequest.setSize(Integer.parseInt(paramAndModifiers.getParamValues()[0]));
                break;
            case Constants.PAGE:
                searchRequest.setPage(Integer.parseInt(paramAndModifiers.getParamValues()[0]));
                break;
            case Constants.DELETED:
                searchRequest.getSearch().getMustNot().put(Constants.DELETED, paramAndModifiers.getParamValues());
                break;
            case Constants.PIN_CODE:
                searchRequest.getSearch().getMust().put(Constants.PIN_CODE, paramAndModifiers.getParamValues());
            case Constants.DATE_OF_BIRTH:
                setEpisodeSearchRequestForBirthDate(paramAndModifiers, searchRequest);
                break;
            case Constants.SORT:
                setSortParameterInSearchRequest(paramAndModifiers.getParamValues(), searchRequest);
                break;
            case Constants.TELECOM:
                setEpisodeSearchRequestForTelecom(paramAndModifiers, searchRequest);
                break;
        }
    }

    @Override
    public EpisodeSearchRequest getEpisodeSearchRequestFromRequest(Map<String, Object> parameterMap) throws PDQMCustomException {
        EpisodeSearchRequest searchRequest = new EpisodeSearchRequest(10000, 0, Arrays.asList(
                Constants.ID, Constants.DELETED, Constants.FIRST_NAME, Constants.LAST_NAME, Constants.PRIMARY_PHONE_NUMBER, Constants.EMAIL_LIST, Constants.DATE_OF_BIRTH, Constants.TREATMENT_OUTCOME, Constants.ADDRESS,
                Constants.PIN_CODE, Constants.GENDER, Constants.CITY, Constants.MARITAL_STATUS, Constants.CONTACT_PERSON_ADDRESS, Constants.CONTACT_PERSON_NAME, Constants.CONTACT_PERSON_PHONE, Constants.LANGUAGE, Constants.EXTERNAL_ID1
        ), TypeOfEpisodeEnum.getTypeOfEpisodeList());
        searchRequest.setSearch(new EpisodeSearchRequest.SupportedSearchMethods(new HashMap<>(), new HashMap<>(), new HashMap<>()));
        for (Map.Entry<String, Object> entry : parameterMap.entrySet()) {
            String paramName = entry.getKey();
            Object paramValue = entry.getValue();
            String[] paramValues;
            if (paramValue instanceof String[]) {
                paramValues = (String[]) paramValue;
            } else {
                paramValues = new String[]{(String) paramValue};
            }
            List<ParamPrefixAndValue> paramPrefixAndValueList = new ArrayList<>();
            if (parameterMap.containsKey(Constants._ID)) {
                setEpisodeSearchRequestWithId(searchRequest, parameterMap, paramValues, paramName);
                return searchRequest;
            }
            else if(parameterMap.containsKey(Constants.IDENTIFIER)) {
                searchRequest.getSearch().getMust().put(Constants.EXTERNAL_ID1, parameterMap.get(Constants.IDENTIFIER));
                return searchRequest;
            }
            if (paramName.contains(Constants.COLON)) {  // check if paramName has a modifier
                String[] nameAndModifier = paramName.split(Constants.COLON);
                paramName = nameAndModifier[0];
                String modifier = "";
                try {
                    modifier = nameAndModifier[1];
                    if(!Constants.SUPPORTED_MODIFIERS.contains(modifier)) {
                        throw new ValidationException(Constants.MODIFIER_NOT_SUPPORTED);
                    }
                    RequestParamAndModifiers paramAndModifiers = new RequestParamAndModifiers(Constants.SEARCH_PARAMETERS_MAP.get(paramName), paramValues, modifier, null);
                    createEpisodeSearchRequest(searchRequest, paramAndModifiers);
                } catch (Exception ex) {
                    throw new PDQMCustomException(ex.getMessage(), paramName, modifier, Objects.toString(paramValue, null));
                }
            } else {
                if (paramName.equals(Constants.BIRTH_DATE)) {
                    for (int index = 0; index < paramValues.length; index++) {
                        String prefix = null;
                        boolean startsWithAlphabet = Character.isLetter(paramValues[index].charAt(0));
                        if (startsWithAlphabet) {
                            prefix = paramValues[index].substring(0, 2);
                            paramValues[index] = paramValues[index].substring(2);
                        }
                        paramValues[index] = Utils.convertDateToTimeStamp(paramValues[index]);
                        paramPrefixAndValueList.add(new ParamPrefixAndValue(prefix, paramValues[index]));
                    }
                }
                try {
                    RequestParamAndModifiers paramAndModifiers = new RequestParamAndModifiers(Constants.SEARCH_PARAMETERS_MAP.get(paramName), paramValues, null, paramPrefixAndValueList);
                    createEpisodeSearchRequest(searchRequest, paramAndModifiers);
                } catch (Exception ex) {
                    throw new PDQMCustomException(ex.getMessage(), paramName, null, Objects.toString(paramValues[0], null));
                }
            }
        }
        return searchRequest;
    }

    private void setSearchRequestForStringTypeFHIRParams(String paramName, String paramModifier, String [] paramValues, EpisodeSearchRequest searchRequest) {
        if(null != paramModifier && paramModifier.equals(Constants.EXACT) && paramName.equals(Constants.FIRST_NAME)) {
            searchRequest.getSearch().getMust().put(Constants.NAME, paramValues[0]);
        }
        else if(null != paramModifier && paramModifier.equals(Constants.NOT)) {
            searchRequest.getSearch().getMustNot().put(paramName, paramValues);
        }
        else if(null != paramModifier && paramModifier.equals(Constants.MISSING) && paramValues[0].equals("True")) {
            searchRequest.getSearch().getMust().put(paramName, Constants.NULL);
        }
        else if(null != paramModifier && paramModifier.equals(Constants.MISSING) && paramValues[0].equals("False")) {
            searchRequest.getSearch().getMustNot().put(paramName, Constants.NULL);
        }
        else if(null != paramModifier && paramModifier.equals(Constants.CONTAINS)) {
            searchRequest.getSearch().getContains().put(paramName, paramValues[0]);
        }
        else {
            searchRequest.getSearch().getMust().put(paramName, paramValues);
        }
    }

    private void setSearchRequestForDateOfBirthWithPrefix(String prefix, EpisodeSearchRequest searchRequest, Map<String, Object> rangeSubQuery, String paramValue) {
        if(searchRequest.getRange().containsKey(Constants.DATE_OF_BIRTH)) {
            Map<String, Object> existingSubQuery = (Map<String, Object>) searchRequest.getRange().get(Constants.DATE_OF_BIRTH);
            existingSubQuery.put(prefix, paramValue);
        } else {
            rangeSubQuery.put(prefix, paramValue);
            searchRequest.getRange().put(Constants.DATE_OF_BIRTH, rangeSubQuery);
        }
    }

    private void setSortParameterInSearchRequest(String[] paramValues, EpisodeSearchRequest searchRequest) {
        if(paramValues[0].contains(Constants.HYPHEN)) {
            paramValues[0] = paramValues[0].substring(1);
            searchRequest.setSortDirection(Sort.Direction.DESC);
        }
        searchRequest.setSortKey(Constants.SEARCH_PARAMETERS_MAP.get(paramValues[0]));
    }

    private void setEpisodeSearchRequestWithId(EpisodeSearchRequest searchRequest, Map<String, Object> parameterMap, String[] paramValues, String paramName) {
        searchRequest.getSearch().getMust().put(Constants.ID, parameterMap.get(Constants._ID));
        if(paramName.equals(Constants._PAGE)) {
            searchRequest.setPage(Integer.parseInt(paramValues[0]));
        }
    }

    private void setEpisodeSearchRequestForBirthDate(RequestParamAndModifiers paramAndModifiers, EpisodeSearchRequest searchRequest) {
        Map<String, Object> rangeSubQuery = new HashMap<>();
        if(null != paramAndModifiers.getPrefixAndValueList()) {
            paramAndModifiers.getPrefixAndValueList().forEach( p ->
            {
                if(null != p.getPrefix() && p.getPrefix().equals(Constants.NOT_EQUAL)) {
                    searchRequest.getSearch().getMustNot().put(Constants.DATE_OF_BIRTH, p.getValue());
                }
                else{
                    if(null != p.getPrefix()) {
                        setSearchRequestForDateOfBirthWithPrefix(p.getPrefix(), searchRequest, rangeSubQuery, p.getValue());
                    } else {
                        searchRequest.getSearch().getMust().put(Constants.DATE_OF_BIRTH, p.getValue());
                    }
                }
            });
        }
        else {
            searchRequest.getSearch().getMust().put(Constants.DATE_OF_BIRTH, paramAndModifiers.getParamValues());
        }
    }

    private void setEpisodeSearchRequestForTelecom(RequestParamAndModifiers paramAndModifiers, EpisodeSearchRequest searchRequest) {
        if(paramAndModifiers.getParamValues()[0].contains(Constants.COLON)) {
            int index = paramAndModifiers.getParamValues()[0].indexOf(Constants.COLON);
            String prefixType = paramAndModifiers.getParamValues()[0].substring(0, index);
            paramAndModifiers.getParamValues()[0] = paramAndModifiers.getParamValues()[0].substring(index + 1);
            String key = (prefixType.equals(Constants.MAIL_TO) || prefixType.equals(Constants.EMAIL)) ? Constants.EMAIL_LIST : Constants.PRIMARY_PHONE_NUMBER;
            searchRequest.getSearch().getMust().put(key, paramAndModifiers.getParamValues()[0]);
        }
        String key = Utils.checkStringContainsOnlyNumbers(paramAndModifiers.getParamValues()[0]) ? Constants.PRIMARY_PHONE_NUMBER : Constants.EMAIL_LIST;
        searchRequest.getSearch().getMust().put(key, paramAndModifiers.getParamValues());
    }

    @Override
    public JsonNode getPatientBundle(HttpServletRequest request, EpisodeSearchRequest episodeSearchRequest, EpisodeSearchResult episodeSearchResult) {
        List<Patient> patientList = FhirUtil.populatePatientResource(episodeSearchResult.getEpisodeIndexList());
        JsonNode bundleResponse = FhirUtil.populatePatientBundle(patientList, request, episodeSearchResult.getTotalHits(), episodeSearchRequest);
        return bundleResponse;
    }

    @Override
    public JsonNode getOperationOutcomeForException(Exception ex) {
        OperationOutcome operationOutcome = new OperationOutcome();
        operationOutcome.addIssue().setSeverity(org.hl7.fhir.r4.model.OperationOutcome.IssueSeverity.ERROR).setCode(OperationOutcome.IssueType.EXCEPTION).getDetails().setText(ex.getMessage());
        return encodeOperationOutcomeToJson(operationOutcome);
    }

    @Override
    public JsonNode getOperationOutcomeForPDQMException(PDQMCustomException exception) {
        OperationOutcome operationOutcome = new OperationOutcome();
        String errorMessage = null;
        operationOutcome.addIssue().setSeverity(OperationOutcome.IssueSeverity.FATAL).setCode(OperationOutcome.IssueType.CODEINVALID);
        if(null != exception.getModifier() && !Constants.SUPPORTED_MODIFIERS.contains(exception.getModifier())) {
            errorMessage = "The \"" + exception.getRequestParam() + "\" parameter has the modifier \"" + exception.getModifier() + "\" which is not supported by the server";
        } else if(Constants.CODE_NOT_SUPPORTED.equals(exception.getMessage())) {
            errorMessage = "The code \"" + exception.getParamValue() + "\" is not known and not legal in this context";
            operationOutcome.getIssue().get(0).addExpression("Patient." + exception.getRequestParam());
        } else if(!Constants.SEARCH_PARAMETERS_MAP.containsKey(exception.getRequestParam())) {
            errorMessage = "The \"" + exception.getRequestParam() + "\" parameter is not supported by the server";
        }
        addOperationOutcomeDetails(operationOutcome, errorMessage, null != exception.getModifier() ? exception.getModifier() : exception.getRequestParam());
        return encodeOperationOutcomeToJson(operationOutcome);
    }


    private JsonNode encodeOperationOutcomeToJson(OperationOutcome operationOutcome) {
        FhirContext fhirContext = FhirContext.forR4();
        String responseString = fhirContext.newJsonParser().encodeResourceToString(operationOutcome);
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            JsonNode jsonNode = objectMapper.readTree(responseString);
            return jsonNode;
        } catch (Exception e) {
            Sentry.capture(e);
        }
        return null;
    }

    private void addOperationOutcomeDetails(OperationOutcome operationOutcome, String errorMessage, String location) {
        operationOutcome.getIssue().get(0)
                .getDetails().setText(errorMessage);
        operationOutcome.getIssue().get(0)
                .addLocation("http.name:" + location);
    }

    @Override
    public String getCapabilityStatement() {
        CapabilityStatement capabilityStatement = new CapabilityStatement();
        capabilityStatement.setName("Everwell FHIR capability statement").setTitle("Capability statement");
        capabilityStatement.setStatus(Enumerations.PublicationStatus.ACTIVE);
        Coding jurisdictionCoding = new Coding().setSystem("urn:iso:std:iso:3166").setCode("IND");
        CodeableConcept jurisdiction = new CodeableConcept().addCoding(jurisdictionCoding);
        capabilityStatement.setJurisdiction(Collections.singletonList(jurisdiction));
        capabilityStatement.setKind(CapabilityStatement.CapabilityStatementKind.CAPABILITY);
        capabilityStatement.setFhirVersion(Enumerations.FHIRVersion._4_0_0);
        capabilityStatement.addFormat("application/json");
        capabilityStatement.setLanguage("en-IN");
        capabilityStatement.getRestFirstRep().setMode(CapabilityStatement.RestfulCapabilityMode.SERVER);
        capabilityStatement.setExperimental(false);
        capabilityStatement.setDate(new Date(2023, 07, 01));
        capabilityStatement.setDescription("This CapabilityStatement outlines the support for the PDQM and PMIR profiles of IHE. It includes two endpoints specifically designed for the PDQM profile: GET v1/patient and POST v1/patient. These endpoints enable interaction with patient resources and facilitate operations related to the PDQM profile." +
                "The PDQM profile supports a comprehensive search functionality, allowing you to query patient resources based on parameters such as given, family, _id, identifier, birthdate, address, postalcode, gender, and telecom. Pagination and sorting capabilities are also available for efficient data retrieval." +
                "In addition to the PDQM profile, the CapabilityStatement defines endpoints dedicated to the PMIR profile, enabling CRUD (Create, Read, Update, Delete) operations on patient resources. The POST v1/patient endpoint facilitates the creation of new patients, the PUT v1/patient endpoint supports updates to existing patient data, and the DELETE v1/patient endpoint allows for the removal of patients from the system. Furthermore, the POST and PUT v1/patients endpoints are designed to handle batch operations, enabling the creation and update of multiple patients simultaneously. " +
                "To authenticate and access these APIs, a JWT token is required as a bearer token. This token can be obtained by sending a POST request to the v1/token endpoint with the username and password provided in the request body.");
        capabilityStatement.setPurpose("This CapabilityStatement provides the description of the FHIR server's capabilities for interoperability and data exchange.");
        capabilityStatement.setPublisherElement(new StringType("Everwell Health Solution Pvt Ltd"));
        setCapabilityStatementAuthentication(capabilityStatement);
        setRestProtocol(capabilityStatement);
        setRestResourceDetails(capabilityStatement);
        FhirContext fhirContext = new FhirContext();
        return fhirContext.newJsonParser().setPrettyPrint(true).encodeResourceToString(capabilityStatement);
    }

    private void setCapabilityStatementAuthentication(CapabilityStatement capabilityStatement) {
        Coding jwtCoding = new Coding().setSystem("http://terminology.hl7.org/CodeSystem/restful-security-service").setCode("JWT").setDisplay("JWT Token Authentication");
        CodeableConcept jwtService = new CodeableConcept().addCoding(jwtCoding);
        capabilityStatement.getRestFirstRep().getSecurity().addService(jwtService);
        Coding ipWhitelistingCoding = new Coding().setSystem("http://terminology.hl7.org/CodeSystem/restful-security-service").setCode("IP_WHITELISTING").setDisplay("IP Whitelisting Authentication");
        CodeableConcept ipWhitelistingService = new CodeableConcept().addCoding(ipWhitelistingCoding);
        capabilityStatement.getRestFirstRep().getSecurity().addService(ipWhitelistingService);
    }

    private void setRestResourceDetails(CapabilityStatement capabilityStatement) {
        CapabilityStatement.CapabilityStatementRestComponent restComponent = new CapabilityStatement.CapabilityStatementRestComponent();
        CapabilityStatement.CapabilityStatementRestResourceComponent patientResource = new CapabilityStatement.CapabilityStatementRestResourceComponent()
                .setType(Constants.PATIENT).setConditionalRead(CapabilityStatement.ConditionalReadStatus.NOTSUPPORTED).setConditionalDelete(CapabilityStatement.ConditionalDeleteStatus.NOTSUPPORTED).setVersioning(CapabilityStatement.ResourceVersionPolicy.NOVERSION);
        CapabilityStatement.TypeRestfulInteraction[] interactions = { CapabilityStatement.TypeRestfulInteraction.READ, CapabilityStatement.TypeRestfulInteraction.SEARCHTYPE, CapabilityStatement.TypeRestfulInteraction.CREATE, CapabilityStatement.TypeRestfulInteraction.DELETE, CapabilityStatement.TypeRestfulInteraction.UPDATE };
        for (CapabilityStatement.TypeRestfulInteraction interaction : interactions) {
            patientResource.addInteraction().setCode(interaction);
        }
        String[] searchParams = { Constants._ID, "active", "family", "given", "identifier", "telecom", "birthdate", "address", "address-city", "address-postalcode", "gender" };
        for (String param : searchParams) {
            patientResource.addSearchParam().setName(param).setType(Enumerations.SearchParamType.STRING);
        }
        restComponent.addResource(patientResource);
        CapabilityStatement.CapabilityStatementRestResourceComponent bundleResource = new CapabilityStatement.CapabilityStatementRestResourceComponent()
                .setType("Bundle")
                .setConditionalRead(CapabilityStatement.ConditionalReadStatus.NOTSUPPORTED)
                .setConditionalDelete(CapabilityStatement.ConditionalDeleteStatus.NOTSUPPORTED)
                .setVersioning(CapabilityStatement.ResourceVersionPolicy.NOVERSION);
        for (CapabilityStatement.TypeRestfulInteraction interaction : interactions) {
            bundleResource.addInteraction().setCode(interaction);
        }
        restComponent.addResource(bundleResource);
        capabilityStatement.addRest(restComponent);
    }

    private void setRestProtocol(CapabilityStatement capabilityStatement) {
        CapabilityStatement.CapabilityStatementMessagingComponent messagingComponent = new CapabilityStatement.CapabilityStatementMessagingComponent();
        CapabilityStatement.CapabilityStatementMessagingEndpointComponent endpointComponent = new CapabilityStatement.CapabilityStatementMessagingEndpointComponent();
        Coding protocolCoding = new Coding();
        protocolCoding.setSystem("http://hl7.org/fhir/ValueSet/endpoint-protocol").setCode("http").setDisplay("HTTP");
        endpointComponent.setProtocol(protocolCoding);
        messagingComponent.addEndpoint(endpointComponent);
        capabilityStatement.setMessaging(Collections.singletonList(messagingComponent));
    }
}


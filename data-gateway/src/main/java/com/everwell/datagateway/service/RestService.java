package com.everwell.datagateway.service;


import com.everwell.datagateway.constants.Constants;
import com.everwell.datagateway.entities.SubscriberUrl;
import com.everwell.datagateway.handlers.AuthenticationHandler;
import com.everwell.datagateway.handlers.AuthenticationHandlerMap;
import com.everwell.datagateway.models.dto.AuthenticationDetailsDto;
import com.everwell.datagateway.models.response.RestServiceResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Service
public class RestService {
    @Autowired
    private AuthenticationHandlerMap authenticationHandlerMap;

    private static final Logger logger = LoggerFactory.getLogger(RestService.class);

    private RestTemplate restTemplate;

    public RestService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public RestServiceResponse httpRequest(String data, SubscriberUrl subscriberUrl, AuthenticationDetailsDto clientAuthenticationDto) {
        String url = subscriberUrl.getUrl();
        HttpMethod httpMethod = null != subscriberUrl.getRequestMethod() ? HttpMethod.resolve(subscriberUrl.getRequestMethod()) : HttpMethod.POST;
        logger.debug(url + " received in Rest Service to Post Request");
        ResponseEntity<String> response;
        HttpHeaders httpHeaders = getHttpHeaders(clientAuthenticationDto);
        HttpEntity<String> entity = new HttpEntity<>(data, httpHeaders);
        RestServiceResponse restServiceResponse;
        try {
            RestTemplate restTemplate = new RestTemplate();
            response = restTemplate.exchange(url, httpMethod, entity, String.class);
            restServiceResponse = new RestServiceResponse(response.getStatusCode().value(), response.toString());
        } catch (RestClientResponseException e) {
            logger.error("Exception " + e.getMessage());
            restServiceResponse = new RestServiceResponse(e.getRawStatusCode(), e.getResponseBodyAsString());
        } catch (Exception e) {
            logger.error("Exception " + e.getMessage());
            restServiceResponse = new RestServiceResponse(e.getMessage());
        }
        return restServiceResponse;
    }

    private HttpHeaders getHttpHeaders(AuthenticationDetailsDto clientAuthenticationDto) {
        HttpHeaders httpHeaders = new HttpHeaders();
        if (!StringUtils.isEmpty(clientAuthenticationDto.getCredentials()) && !StringUtils.isEmpty(clientAuthenticationDto.getCredentialType())) {
            AuthenticationHandler authenticationHandler = authenticationHandlerMap.getAuthentication(clientAuthenticationDto.getCredentialType());
            if (null != authenticationHandler) {
                httpHeaders = authenticationHandler.setUpAuthentication(clientAuthenticationDto.getCredentials(), clientAuthenticationDto.getCredentialType());
            }
        }
        httpHeaders.setCacheControl(CacheControl.noCache());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        return httpHeaders;
    }

    public HttpEntity<Object> buildHeaderAndRequest(Map<String, String> postParams) {

        MultiValueMap<String, String> postParamsMultiValue = new LinkedMultiValueMap();
        postParamsMultiValue.add(Constants.GRANT_TYPE, postParams.get(Constants.GRANT_TYPE));
        postParamsMultiValue.add(Constants.USERNAME, postParams.get(Constants.USERNAME));
        postParamsMultiValue.add(Constants.PASSWORD, postParams.get(Constants.PASSWORD));

        // create headers
        HttpHeaders headers = new HttpHeaders();
        headers.setCacheControl(CacheControl.noCache());
        // set `content-type` header
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        // build the request
        return new HttpEntity<>(postParamsMultiValue, headers);
    }
}

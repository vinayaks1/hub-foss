package com.everwell.datagateway.service

import com.everwell.datagateway.constants.Constants
import com.everwell.datagateway.constants.ZuulConstants
import com.everwell.datagateway.models.response.ApiResponse
import com.everwell.datagateway.models.response.MicroServiceGenericAuthResponse
import org.slf4j.LoggerFactory
import org.springframework.core.ParameterizedTypeReference
import org.springframework.stereotype.Service
import java.util.*

@Service
open class HubReportsRestServiceImp : HubReportsRestService() {

    private val LOGGER = LoggerFactory.getLogger(HubReportsRestServiceImp::class.simpleName)

    override fun getAuthToken(client_id: String?): String? {
        if (!clientIdAccessTokenMap.contains(client_id) || Date().time >= clientIdRefreshTimeMap[client_id]!!) {
            LOGGER.debug("[HubReportsRestServiceImpl] Fetching auth token...")
            val headers = mutableMapOf<String, String>()
            headers[ZuulConstants.CLIENT_ID_HUB_REPORTS_IDENTIFIER] = client_id!!
            val resp = authenticate(
                    appProperties.hubReportsUrl + "/v1/client",
                    headers,
                    object : ParameterizedTypeReference<ApiResponse<MicroServiceGenericAuthResponse>>() {})
            clientIdAccessTokenMap[client_id] = resp.body?.data?.authToken!!
            if (resp.body?.data?.nextRefresh == null || resp.body?.data?.nextRefresh!! == 0L)
                clientIdRefreshTimeMap[client_id] = Date().time + Constants.MICROSERVICE_REFRESH_EXPIRY
            else
                clientIdRefreshTimeMap[client_id] = resp.body?.data?.nextRefresh!!
        }
        return clientIdAccessTokenMap[client_id]
    }
}
package com.everwell.datagateway.service;

import com.everwell.datagateway.entities.EventClient;
import com.everwell.datagateway.entities.SubscriberUrl;

import java.util.List;

public interface SubscriberUrlService {
    SubscriberUrl findByEventClientId(EventClient eventClient);
    List<SubscriberUrl> findByUrl(String url);
}

package com.everwell.datagateway.service;

import com.everwell.datagateway.models.response.ecbss.TrackedEntityInstancesResponse;

public interface ECBSSHelperService {
    TrackedEntityInstancesResponse getTrackedEntityInstance(String orgUnitId, String externalId1);
}

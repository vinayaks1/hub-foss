package com.everwell.datagateway.service;

import com.everwell.datagateway.entities.EventClient;
import com.everwell.datagateway.entities.SubscriberUrl;
import com.everwell.datagateway.repositories.SubscriberUrlRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class SubscriberUrlServiceImpl implements SubscriberUrlService {

    @Autowired
    private SubscriberUrlRepository subscriberUrlRepository;


    @Override
    public SubscriberUrl findByEventClientId(EventClient eventClient) {
        return subscriberUrlRepository.findByEventClient(eventClient);
    }

    @Override
    public List<SubscriberUrl> findByUrl(String url) {
        return subscriberUrlRepository.findByUrl(url);
    }
}

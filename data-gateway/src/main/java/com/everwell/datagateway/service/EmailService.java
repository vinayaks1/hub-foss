package com.everwell.datagateway.service;

import java.util.List;

public interface EmailService {
    void sendText(String from, String to, String subject, String body);

    String makeContentClient(String eventName, String errorMessage);

    String makeContentDeveloper(String clientName, String eventName, String message, String errorMessage);
}

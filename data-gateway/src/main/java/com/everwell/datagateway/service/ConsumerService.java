package com.everwell.datagateway.service;

import com.everwell.datagateway.config.RmqDlxHelper;
import com.everwell.datagateway.constants.QueueConstants;
import com.everwell.datagateway.entities.*;
import com.everwell.datagateway.enums.EmailSeverityEnum;
import com.everwell.datagateway.enums.StatusCodeEnum;
import com.everwell.datagateway.exceptions.NotFoundException;
import com.everwell.datagateway.exceptions.RestTemplateException;
import com.everwell.datagateway.exceptions.URLDisabledException;
import com.everwell.datagateway.models.dto.AuthenticationDetailsDto;
import com.everwell.datagateway.models.dto.OutgoingWebhookLogDTO;
import com.everwell.datagateway.models.dto.PublishToSubscriberUrlDTO;
import com.everwell.datagateway.models.request.FailureEmailContent;
import com.everwell.datagateway.models.response.RestServiceResponse;
import com.everwell.datagateway.repositories.EventClientRepository;
import com.everwell.datagateway.utils.CacheUtils;
import com.everwell.datagateway.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.everwell.datagateway.constants.Constants.*;
import static com.everwell.datagateway.constants.EmailConstants.FAILURE_EMAIL_SUBJECT;

@Service
public class ConsumerService {


    private static Logger LOGGER = LoggerFactory.getLogger(ConsumerService.class);

    @Autowired
    private RestService restService;

    @Autowired
    private EmailService sendGridEmailService;

    @Autowired
    private SubscriberUrlService subscriberUrlService;


    @Autowired
    private EventClientRepository eventClientRepository;

    @Autowired
    private EventService eventService;

    @Autowired
    private ClientService clientService;

    @Autowired
    private ClientAuthenticationService clientAuthenticationService;

    @Autowired
    private ClientRequestsService clientRequestsService;

    @Autowired
    private OutgoingWebhookLogService outgoingWebhookLogService;

    @Value("${sendgrid.mail.senders}")
    private String SENDERS_EMAIL;

    @Value("${sendgrid.mail.developers}")
    private String EVERWELL_DEVELOPERS_MAIL;


    public void publishToSubscriberUrl(PublishToSubscriberUrlDTO publishToSubscriberUrlDTO) {
        String clientName = publishToSubscriberUrlDTO.getClientName();
        String eventName = publishToSubscriberUrlDTO.getEventName();
        String message = publishToSubscriberUrlDTO.getMessage();
        Long referenceId = publishToSubscriberUrlDTO.getReferenceId();
        Long outgoingWebhookLogId = null;
        if(publishToSubscriberUrlDTO.isSaveOutgoingWebhookLog()) {
            OutgoingWebhookLogDTO outgoingWebhookLogDTO = new OutgoingWebhookLogDTO(message);
            outgoingWebhookLogId = outgoingWebhookLogService.saveOutgoingWebhookLog(outgoingWebhookLogDTO);
        }
        Client client = clientService.findByUsername(clientName);
        Event event = eventService.findEventByEventName(eventName);
        if (null == event) {
            LOGGER.error("[publishToSubscriberUrl] This Event doesn't exists: " + eventName);
            StatusCodeEnum eventNotFoundSentry=StatusCodeEnum.EVENT_NOT_FOUND_SENTRY;
            throw new NotFoundException(eventNotFoundSentry.getCode().value(),eventNotFoundSentry.getMessage() + eventName);
        }
        if(null == client) {
            LOGGER.error("[publishToSubscriberUrl] This Client doesn't exists: " + clientName);
            StatusCodeEnum clientNotFoundSentry=StatusCodeEnum.CLIENT_NOT_FOUND_SENTRY;
            throw new NotFoundException(clientNotFoundSentry.getCode().value(),clientNotFoundSentry.getMessage() + clientName);
        }
        EventClient eventClient = eventClientRepository.findByClientIdAndEventId(client.getId(), event.getId());
        SubscriberUrl subscriberUrl = subscriberUrlService.findByEventClientId(eventClient);
        if (null == subscriberUrl || !subscriberUrl.getIsEnabled()) {
            LOGGER.debug("[publishToSubscriberUrl] Subscriber url is null, or Subscriber url is disabled for EventClientId: " + eventClient.getClientId());
            StatusCodeEnum urlDisabledSentry=StatusCodeEnum.URL_DISABLED_SENTRY;
            throw new URLDisabledException(urlDisabledSentry.getCode().value(),urlDisabledSentry.getMessage() + eventClient.getClientId());
        }
        AuthenticationDetailsDto authenticationDetailsDto = clientAuthenticationService.getSpecificAuthenticationDetails(client.getId(), eventClient.getId());
        RestServiceResponse response = restService.httpRequest(message, subscriberUrl, authenticationDetailsDto);
        LOGGER.debug(response.getCode() + "\n" + response.getMessage());
        if(publishToSubscriberUrlDTO.isSaveOutgoingWebhookLog()) {
            OutgoingWebhookLogDTO outgoingWebhookLogDTO = new OutgoingWebhookLogDTO(subscriberUrl.getId(), event.getId(), client.getId(), response.getCode(), new Date(), response.getMessage());
            outgoingWebhookLogService.updateOutgoingWebhookLog(outgoingWebhookLogId, outgoingWebhookLogDTO);
        }
        if(referenceId != null) {
            ClientRequests clientRequests = clientRequestsService.getClientRequestsById(referenceId);
            clientRequests.setCallbackResponseCode(response.getCode());
            clientRequestsService.updateClientRequest(clientRequests);
        }
        if (response.getCode() == 200) {
            LOGGER.debug("[publishToSubscriberUrl] Message Published for " + subscriberUrl.getUrl());
            // TODO - Consult to Classify different statuses or not, And also should there be check on mandatory fields
            try {
                Gson gson = new Gson();
                gson.toJson(response.getMessage());
//                LOGGER.info(gson.toString());
                //Converted to JSON to use,if there are different success status in response ( Eg : Gramener )
            } catch (JsonParseException err) {
                LOGGER.error("[publishToSubscriberUrl] JSON Parsing Error " + err.toString());
            }
        } else {
            //Do redis work
            String severity = getSeverityFromCache(client);
            LOGGER.debug("[publishToSubscriberUrl] Severity is: " + severity);
            if (severity == null) {
                throw new RestTemplateException(response.getCode(), response.getMessage());
            }
            FailureEmailContent failureEmailContent = new FailureEmailContent(severity, response.getMessage(),clientName, eventName, message);
            sendFailureEmailToClient(client.getFailureEmail(), failureEmailContent);   //Notify Client
            //After sending mail requeue message back by throwing exception
            throw new RestTemplateException(response.getCode(), response.getMessage());
        }
    }

    public void sendFailureEmailToClient(String failureEmail, FailureEmailContent failureEmailContent) {
        String subject = FAILURE_EMAIL_SUBJECT + " [" + failureEmailContent.getSeverity() + "]";
        String errorMessage = failureEmailContent.getHttpPostResponse();
        String clientContent = sendGridEmailService.makeContentClient(failureEmailContent.getEventName(), errorMessage);
        String developerContent = sendGridEmailService.makeContentDeveloper(failureEmailContent.getClientName(),failureEmailContent.getEventName(), failureEmailContent.getQueueMessage(), errorMessage);
        this.sendGridEmailService.sendText(SENDERS_EMAIL, failureEmail, subject, clientContent); // to client
        this.sendGridEmailService.sendText(SENDERS_EMAIL, EVERWELL_DEVELOPERS_MAIL, subject, developerContent); // to developers
        LOGGER.debug("Mail Sent");
    }


    public String getSeverityFromCache(Client client) {

        String severity = null;
        LOGGER.debug("[getSeverityFromCache] Client is: " + client.getId());
        String clientID = REDIS_PREFIX +client.getId().toString();
        Long failureCount;
        if (CacheUtils.hasKey(clientID)) {
            failureCount = Long.parseLong(Objects.requireNonNull(CacheUtils.getFromCache(clientID)));
            if (failureCount > EmailSeverityEnum.HIGH_SEVERITY.getSeverityValue()) {
                LOGGER.debug("[getSeverityFromCacheNULL]Cache value is: " + "Key: " + clientID + " Value: " + failureCount);
                return null;
            }
            failureCount = CacheUtils.updateCacheByValue(clientID, 1); // Update
        } else {
            failureCount = 1L;
            String failureCountString = failureCount.toString();
            LOGGER.debug("Failure Count in String is " + failureCountString);
            CacheUtils.putIntoCache(clientID, failureCountString, Utils.getRemainingTime());  // Initialis

        }


        LOGGER.debug("[getSeverityFromCache]Cache value is: " + "Key: " + clientID + " Value: " + failureCount);
        if (failureCount.equals(EmailSeverityEnum.LOW_SEVERITY.getSeverityValue())) {
            severity = EmailSeverityEnum.LOW_SEVERITY.getSeverityName();
        } else if (failureCount.equals(EmailSeverityEnum.MEDIUM_SEVERITY.getSeverityValue())) {
            severity = EmailSeverityEnum.MEDIUM_SEVERITY.getSeverityName();
        } else if (failureCount.equals(EmailSeverityEnum.HIGH_SEVERITY.getSeverityValue())) {
            severity = EmailSeverityEnum.HIGH_SEVERITY.getSeverityName();
        }


        return severity;
    }

    public boolean checkSeverityAndSendToDlq(Client client, Message message) {
        String clientID = REDIS_PREFIX + client.getId().toString();
        long failureCount;
        if (CacheUtils.hasKey(clientID)) {
            failureCount = Long.parseLong(Objects.requireNonNull(CacheUtils.getFromCache(clientID)));
            boolean isHighSev =  Boolean.parseBoolean((String) message.getMessageProperties().getHeaders().getOrDefault(QueueConstants.HIGH_SEV_HEADER, "false"));
            if (failureCount >= EmailSeverityEnum.HIGH_SEVERITY.getSeverityValue() && !isHighSev) {
                LOGGER.debug("Message " + new String(message.getBody()) + " aborted due to HIGH SERVERITY" + " for client " + client.getUsername());
                message.getMessageProperties().setHeader(QueueConstants.HIGH_SEV_HEADER, "true");
                RmqDlxHelper.triggerRetryMechanism(message, message.getMessageProperties().getReceivedRoutingKey(), HIGH_SEV_RETRY_DELAY);
                return true;
            }
        }
        return false;
    }


}

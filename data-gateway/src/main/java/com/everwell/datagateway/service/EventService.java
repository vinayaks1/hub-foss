package com.everwell.datagateway.service;

import com.everwell.datagateway.entities.Event;

public interface EventService {
    Event  findEventByEventName(String eventName);

    Boolean isEventExists(String eventName);


    Event getEventById(Long eventId);
}

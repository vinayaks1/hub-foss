package com.everwell.datagateway.apiHelpers;

import com.everwell.datagateway.enums.StatusCodeEnum;
import com.everwell.datagateway.exceptions.RestTemplateException;
import com.everwell.datagateway.models.request.abdm.*;
import com.everwell.datagateway.filters.Generated;
import com.everwell.datagateway.models.response.RestServiceResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.sentry.Sentry;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
public class ABDMHelper {

    private static final Logger logger = LoggerFactory.getLogger(ABDMHelper.class);
    private static String token;
    @Value("${NDHM_API_ENDPOINT:}")
    String baseUrl;
    @Value("${NDHM_X_CM_ID:}")
    String cmId;
    @Value("${NDHM_CLIENT_ID:}")
    String clientId;
    @Value("${NDHM_CLIENT_SECRET:}")
    String clientSecret;
    @Value("${NDHM_MAX_TRIES:}")
    int maxTries;
    private RestTemplate client = new RestTemplate();
    private Gson gson = new GsonBuilder().create();

    public void getToken() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        JSONObject postBodyJson = new JSONObject();
        postBodyJson.put("clientId", clientId);
        postBodyJson.put("clientSecret", clientSecret);
        HttpEntity<String> request = new HttpEntity<String>(postBodyJson.toString(), headers);
        String url = baseUrl + "/v0.5/sessions";
        ResponseEntity<Map> response = client.postForEntity(url, request, Map.class);
        token = response.getBody().get("accessToken").toString();
        logger.info("[getToken end]"+token);
    }

    private HttpHeaders getHeadersForABDMApiCall()
    {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("X-CM-ID", cmId);
        headers.setBearerAuth(token);
        return headers;
    }

    public RestServiceResponse publishToABDM(String data, String abdmUrl) {
        try {
            if (client == null) {
                client = new RestTemplate();
            }
            if (token == "" || token == null)
                getToken();
            int tryCount = 0;
            String url = baseUrl + abdmUrl;
            while (tryCount <= maxTries) {
                tryCount++;
                HttpEntity<String> request = new HttpEntity<String>(data, getHeadersForABDMApiCall());
                Map<Integer, String> response = getABDMResponse(url, request);
                Map.Entry<Integer, String> map = response.entrySet().iterator().next();
                if (map.getKey() == HttpStatus.UNAUTHORIZED.value()) {
                    getToken();
                } else {
                    return new RestServiceResponse(map.getKey(), map.getValue());
                }
            }
        } catch (RestClientResponseException e) {
            logger.info("Exception " + e.getMessage());
            return new RestServiceResponse(e.getRawStatusCode(), e.getResponseBodyAsString());
        }
        return new RestServiceResponse(HttpStatus.UNAUTHORIZED.value(), StatusCodeEnum.INVALID_CREDENTIALS.getMessage());
    }

    //This function is using for get the response of ABDM
    public Map<Integer, String> getABDMResponse(String url, HttpEntity<String> request) {
        Map<Integer, String> object = new HashMap<>();
        try {
            ResponseEntity<String> response = client.postForEntity(url, request, String.class);
            object.put(response.getStatusCode().value(), response.toString());
        } catch (RestClientResponseException e) {
            object.put(e.getRawStatusCode(), e.getResponseBodyAsString());
        }
        return object;
    }

    public String authInitRequest(ABDMInitateAuthRequest data) {
        try {
            if (token == "" || token == null)
                getToken();
            int tryCount = 0;
            String url = baseUrl + "/v0.5/users/auth/init";
            while (tryCount <= maxTries) {
                tryCount++;
                String json = gson.toJson(data);
                HttpEntity<String> request = new HttpEntity<String>(json, getHeadersForABDMApiCall());
                Map<Integer, String> response = getABDMResponse(url, request);
                Map.Entry<Integer, String> map = response.entrySet().iterator().next();
                if (map.getKey() == HttpStatus.UNAUTHORIZED.value()) {
                    getToken();
                } else {
                    return map.getValue();
                }
            }
        } catch (RestClientResponseException e) {
            logger.info("Exception " + e.getMessage());
        }
        return null;
    }

    public String authConfirmRequest(ABDMDemographicLinkingTokenRequest data) {
        try {
            if (token == "" || token == null)
                getToken();
            int tryCount = 0;

            String url = baseUrl + "/v0.5/users/auth/confirm";
            while (tryCount <= maxTries) {
                tryCount++;

                String json = gson.toJson(data);
                HttpEntity<String> request = new HttpEntity<String>(json, getHeadersForABDMApiCall());
                Map<Integer, String> response = getABDMResponse(url, request);
                Map.Entry<Integer, String> map = response.entrySet().iterator().next();
                if (map.getKey() == HttpStatus.UNAUTHORIZED.value()) {
                    getToken();
                } else {
                    return "";
                }

            }
        } catch (RestClientResponseException e) {
            logger.info("Exception " + e.getMessage());
        }
        return null;
    }

    public String addCareContextRequest(ABDMLinkCareContextRequest data) {
        try {
            if (token == "" || token == null)
                getToken();
            int tryCount = 0;


            String url = baseUrl + "/v0.5/links/link/add-contexts";
            while (tryCount <= maxTries) {
                tryCount++;
                String json = gson.toJson(data);
                HttpEntity<String> request = new HttpEntity<String>(json, getHeadersForABDMApiCall());
                Map<Integer, String> response = getABDMResponse(url, request);
                Map.Entry<Integer, String> map = response.entrySet().iterator().next();
                if (map.getKey() == HttpStatus.UNAUTHORIZED.value()) {
                    getToken();
                } else {
                    return "";
                }
            }
        } catch (Exception e) {
            logger.info("Exception " + e.getMessage());
            Sentry.capture(e);
        }
        return null;
    }

    public String notifyCareContext(ABDMNotifyCareContextRequest req) {
        try {
            if (token == "" || token == null)
                getToken();
            int tryCount = 0;

            String url = baseUrl + "/v0.5/links/context/notify";


            while (tryCount <= maxTries) {
                tryCount++;
                String json = gson.toJson(req);
                HttpEntity<String> request = new HttpEntity<String>(json, getHeadersForABDMApiCall());
                Map<Integer, String> response = getABDMResponse(url, request);
                Map.Entry<Integer, String> map = response.entrySet().iterator().next();
                if (map.getKey() == HttpStatus.UNAUTHORIZED.value()) {
                    getToken();
                } else {
                    return "";
                }
            }
        } catch (Exception e) {
            logger.info("Exception " + e.getMessage());
            Sentry.capture(e);
        }
        return null;
    }


    public boolean onDiscovery(ABDMOnDiscoveryRequest req) {
        boolean success = false;
        try {
            if (StringUtils.hasText(token))
                getToken();
            int tryCount = 0;

            String url = baseUrl + "/v0.5/care-contexts/on-discover";
            while (tryCount <= maxTries) {
                tryCount++;
                String json = gson.toJson(req);
                HttpEntity<String> request = new HttpEntity<String>(json, getHeadersForABDMApiCall());
                Map<Integer, String>response = getABDMResponse(url,request);
                Map.Entry<Integer, String> map = response.entrySet().iterator().next();
                if (map.getKey() == HttpStatus.UNAUTHORIZED.value()) {
                    getToken();
                } else {
                    logger.info("[OnDiscovery]" + map.getKey() +" " + map.getValue());
                    success = true;
                    break;
                }
            }
        } catch (RestClientResponseException e) {
            logger.info("Exception " + e.getMessage());
        }
        return success;
    }

    public boolean OnLinkInit(ABDMOnLinkInitiationRequest req) {
        boolean success = false;
        try {
            if (!StringUtils.hasText(token))
                getToken();
            int tryCount = 0;

            String url = baseUrl + "/v0.5/links/link/on-init";
            while (tryCount <= maxTries) {
                tryCount++;
                String json = gson.toJson(req);
                HttpEntity<String> request = new HttpEntity<>(json, getHeadersForABDMApiCall());
                Map<Integer, String> response = getABDMResponse(url, request);
                Map.Entry<Integer, String> map = response.entrySet().iterator().next();
                if (map.getKey() == HttpStatus.UNAUTHORIZED.value()) {
                    getToken();
                } else {
                    logger.info("[OnLinkInit]" + map.getKey() +" " + map.getValue());
                    success = true;
                    break;
                }
            }
        } catch (Exception e) {
            logger.info("Exception " + e.getMessage());
            Sentry.capture(e);
        }
        return success;
    }


    public boolean OnLinkConfirm(ABDMOnLinkConfirmationRequest req) {
        boolean success = false;
        try {
            if (!StringUtils.hasText(token))
                getToken();
            int tryCount = 0;

            String url = baseUrl + "/v0.5/links/link/on-confirm";
            while (tryCount <= maxTries) {
                tryCount++;
                String json = gson.toJson(req);
                HttpEntity<String> request = new HttpEntity<>(json, getHeadersForABDMApiCall());
                Map<Integer, String> response = getABDMResponse(url, request);
                Map.Entry<Integer, String> map = response.entrySet().iterator().next();
                if (map.getKey() == HttpStatus.UNAUTHORIZED.value()) {
                    getToken();
                } else {
                    logger.info("[OnLinkConfirm]" + map.getKey() +" " + map.getValue());
                    success = true;
                    break;
                }
            }
        } catch (Exception e) {
            logger.info("Exception " + e.getMessage());
            Sentry.capture(e);
        }
        return success;
    }
  
    public void authOnNotify(ABDMAuthOnNotifyRequest req) {
        try {
            if (!StringUtils.hasText(token))
                getToken();
            int tryCount = 0;

            String url = baseUrl + "/v0.5/users/auth/on-notify";


            while (tryCount <= maxTries) {
                tryCount++;
                String json = gson.toJson(req);
                HttpEntity<String> request = new HttpEntity<String>(json, getHeadersForABDMApiCall());
                Map<Integer, String> response = getABDMResponse(url, request);
                Map.Entry<Integer, String> map = response.entrySet().iterator().next();
                if (map.getKey() == HttpStatus.UNAUTHORIZED.value()) {
                    getToken();
                } else if(map.getKey() == HttpStatus.ACCEPTED.value()) {
                    return;
                }
                else {
                    throw new RestTemplateException(map.getKey(),map.getValue() + "[request body] " + req);
                }
            }
        } catch (Exception e) {
            logger.info("Exception " + e.getMessage());
            Sentry.capture(e);
        }
    }

    public void hiuConsentOnNotify(ABDMConsentOnNotifyRequest req) {
        try {
            if (!StringUtils.hasText(token))
                getToken();
            int tryCount = 0;

            String url = baseUrl + "/v0.5/consents/hiu/on-notify";


            while (tryCount <= maxTries) {
                tryCount++;
                String json = gson.toJson(req);
                HttpEntity<String> request = new HttpEntity<String>(json, getHeadersForABDMApiCall());
                Map<Integer, String> response = getABDMResponse(url, request);
                Map.Entry<Integer, String> map = response.entrySet().iterator().next();
                if (map.getKey() == HttpStatus.UNAUTHORIZED.value()) {
                    getToken();
                } else if(map.getKey() == HttpStatus.ACCEPTED.value()) {
                    return;
                }
                else {
                    throw new RestTemplateException(map.getKey(),map.getValue() + "[request body] " + req);
                }
            }
        } catch (Exception e) {
            logger.info("Exception " + e.getMessage());
            Sentry.capture(e);
        }
    }
}

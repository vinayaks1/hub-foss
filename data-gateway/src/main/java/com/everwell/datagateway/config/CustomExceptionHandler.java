package com.everwell.datagateway.config;

import com.everwell.datagateway.enums.StatusCodeEnum;
import com.everwell.datagateway.exceptions.*;
import com.everwell.datagateway.models.response.MessageApiResponse;
import com.everwell.datagateway.utils.SentryUtils;
import io.sentry.Sentry;
import io.sentry.event.EventBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.io.IOException;
import java.io.InputStream;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {


    private static final Logger LOGGER = LoggerFactory.getLogger(CustomExceptionHandler.class);

    @ResponseBody
    @ExceptionHandler(LoginException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    protected MessageApiResponse handleLoginException(LoginException exception)
    {
        return  MessageApiResponse.buildFailureResponse(exception.getStatusCodeEnum().getCode().toString() ,exception.getStatusCodeEnum().getMessage());
    }

    @ResponseBody
    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    protected MessageApiResponse handleNotFoundException(NotFoundException exception)
    {
        Sentry.capture(exception);
        return MessageApiResponse.buildFailureResponse(StatusCodeEnum.INTERNAL_SERVER_ERROR.getCode().toString(), StatusCodeEnum.INTERNAL_SERVER_ERROR.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(RestTemplateException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected MessageApiResponse handleRestTemplateException(RestTemplateException exception)
    {
        Sentry.capture(exception);
        return MessageApiResponse.buildFailureResponse(StatusCodeEnum.INTERNAL_SERVER_ERROR.getCode().toString(), StatusCodeEnum.INTERNAL_SERVER_ERROR.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(ValidationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected MessageApiResponse handleValidationException(ValidationException exception)
    {
        return MessageApiResponse.buildFailureResponse(StatusCodeEnum.BAD_REQUEST.getCode().toString(), exception.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(ForbiddenException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    protected MessageApiResponse handleForbiddenException(ForbiddenException exception) {
        return MessageApiResponse.buildFailureResponse(HttpStatus.FORBIDDEN.toString(), exception.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public final MessageApiResponse handleAllExceptions(Exception ex, WebRequest request, InputStream inputStream) throws IOException {

        EventBuilder builder = SentryUtils.eventBuilder(ex,request,inputStream);

        Sentry.capture(builder);
        LOGGER.error("[handleAllExceptions] exception occurred for request body " + ex);
        return MessageApiResponse.buildFailureResponse(StatusCodeEnum.INTERNAL_SERVER_ERROR.getCode().toString(), StatusCodeEnum.INTERNAL_SERVER_ERROR.getMessage());
    }
}

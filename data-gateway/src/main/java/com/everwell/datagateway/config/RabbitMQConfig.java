package com.everwell.datagateway.config;

import com.everwell.datagateway.exceptions.RmqExceptionStrategy;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.RabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.boot.autoconfigure.amqp.SimpleRabbitListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.amqp.core.Queue;
import com.everwell.datagateway.constants.QueueConstants;

@Configuration
public class RabbitMQConfig {
    //default prefetch count is of 250, setting it to 20
    @Bean
    public RabbitListenerContainerFactory<SimpleMessageListenerContainer> rmqPrefetchCount20(
            SimpleRabbitListenerContainerFactoryConfigurer configurer,
            ConnectionFactory rabbitConnectionFactory
    ) {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        configurer.configure(factory, rabbitConnectionFactory);
        factory.setPrefetchCount(20);
        factory.setErrorHandler(new RmqExceptionStrategy());
        return factory;
    }

    @Bean
    Queue shareDispensationQueue() {
        return new Queue(QueueConstants.SHARE_DISPENSATION_QUEUE, true, false, false);
    }

    @Bean
    Queue patientEnrollmentGramenerQueue() {
        return new Queue(QueueConstants.PATIENT_ENROLLMENT_GRAMENER_QUEUE, true, false, false);
    }

    @Bean
    Queue refIdReplyQueue() {
        return new Queue(QueueConstants.REF_ID_REPLY_QUEUE, true, false, false);
    }

    @Bean
    Queue dlxQueue() {
        return new Queue(QueueConstants.DLX_QUEUE, true, false, false);
    }

    @Bean
    Queue abdmAddUpdateConsentQueue() {
        return new Queue(QueueConstants.ABDM_ADD_UPDATE_CONSENT_QUEUE, true, false, false);
    }

    @Bean
    Queue abdmDataExchangeGatewayQueue() {
        return new Queue(QueueConstants.DATA_EXCHANGE_QUEUE, true, false, false);
    }

    @Bean
    Queue abdmLinkCareContextQueue() {
        return new Queue(QueueConstants.ABDM_LINK_CARE_CONTEXT_QUEUE, true, false, false);
    }

    @Bean
    Queue abdmCareContextNotifyQueue() {
        return new Queue(QueueConstants.ABDM_CARE_CONTEXT_NOTIFY_QUEUE, true, false, false);
    }

    @Bean
    Queue abdmNotifyViaSmsQueue() {
        return new Queue(QueueConstants.ABDM_NOTIFY_VIA_SMS_QUEUE, true, false, false);
    }

    @Bean
    Queue shareMermEventQueue() {
        return new Queue(QueueConstants.SHARE_MERM_EVENT, true, false, false);
    }

    @Bean
    Queue initiateConsentRequest() {
        return new Queue(QueueConstants.ABDM_HIU_QUEUE, true, false, false);
    }

    @Bean
    Queue adherenceDataQueue() {
        return new Queue(QueueConstants.ECBSS_ADHERENCE_DATA_QUEUE, true, false, false);
    }
}

package com.everwell.datagateway.config;

import com.everwell.datagateway.constants.QueueConstants;
import com.everwell.datagateway.publishers.RMQPublisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RmqDlxHelper {

    private static Logger LOGGER = LoggerFactory.getLogger(RmqDlxHelper.class);


    public static RMQPublisher rabbitMQPublisherService;

    @Autowired
    RmqDlxHelper(RMQPublisher rabbitMQPublisherService) {
        this.rabbitMQPublisherService = rabbitMQPublisherService;
    }

    public static void triggerRetryMechanism(Message message, String routingKey, Long retryDelay) {
        int retryCount = Integer.valueOf((String) message.getMessageProperties().getHeaders().getOrDefault(QueueConstants.RETRY_COUNT_HEADER, "0"));
        if (retryCount >= QueueConstants.RMQ_MAX_RETRY_COUNT) {
            LOGGER.debug("[triggerRetryMechanism] moving to DLX: " + QueueConstants.DLX_EXCHANGE);
            rabbitMQPublisherService.send(new String(message.getBody()), routingKey, QueueConstants.DLX_EXCHANGE, null, message.getMessageProperties().getHeaders());
        } else {
            message.getMessageProperties().setHeader(QueueConstants.RETRY_COUNT_HEADER, String.valueOf(retryCount + 1));
            boolean isHighSev =  Boolean.parseBoolean((String) message.getMessageProperties().getHeaders().getOrDefault(QueueConstants.HIGH_SEV_HEADER, "false"));
            String exchange = isHighSev ? QueueConstants.HIGH_SEV_WAIT_EXCHANGE : QueueConstants.WAIT_EXCHANGE;
            rabbitMQPublisherService.send(new String(message.getBody()), routingKey, exchange, String.valueOf(retryDelay), message.getMessageProperties().getHeaders());
            LOGGER.debug("[triggerRetryMechanism] retry count: " + (retryCount + 1) + " for routing key: " + routingKey);
        }
    }
}


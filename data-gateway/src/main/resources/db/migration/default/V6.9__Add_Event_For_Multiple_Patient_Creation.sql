DO $$

BEGIN
IF NOT EXISTS (SELECT * FROM public.event WHERE event_name = 'createMultiplePatients') THEN
    INSERT INTO public.event (event_name, created_at,updated_at) VALUES('createMultiplePatients', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
END IF;

IF NOT EXISTS (SELECT * FROM public.event WHERE event_name = 'updateMultiplePatients') THEN
    INSERT INTO public.event (event_name, created_at,updated_at) VALUES('updateMultiplePatients', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
END IF;

END $$;

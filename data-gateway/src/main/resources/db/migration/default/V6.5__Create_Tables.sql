create table if not exists event
(
    id bigserial
    constraint event_pkey
    primary key,
    created_at timestamp,
    event_name varchar(255),
    updated_at timestamp
);

create table if not exists client
(
    id bigserial
    constraint client_pkey
    primary key,
    created_at timestamp,
    failure_email varchar(255),
    password varchar(255),
    updated_at timestamp,
    client_name varchar(255),
    auth_token_for_proxy text,
    ip varchar(255),
    accessible_client bigint
)
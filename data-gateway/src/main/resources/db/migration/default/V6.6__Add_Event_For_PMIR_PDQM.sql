DO $$

BEGIN

IF NOT EXISTS (SELECT * FROM public.event WHERE event_name = 'getPatientDetails') THEN
    INSERT INTO public.event (event_name, created_at,updated_at) VALUES('getPatientDetails', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
END IF;

IF NOT EXISTS (SELECT * FROM public.event WHERE event_name = 'searchPatientDetails') THEN
    INSERT INTO public.event (event_name, created_at,updated_at) VALUES('searchPatientDetails', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
END IF;

IF NOT EXISTS (SELECT * FROM public.event WHERE event_name = 'createPatient') THEN
    INSERT INTO public.event (event_name, created_at, updated_at) VALUES('createPatient', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
END IF;

IF NOT EXISTS (SELECT * FROM public.event WHERE event_name = 'updatePatient') THEN
    INSERT INTO public.event (event_name, created_at, updated_at) VALUES('updatePatient', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
END IF;

IF NOT EXISTS (SELECT * FROM public.event WHERE event_name = 'deletePatient') THEN
    INSERT INTO public.event (event_name, created_at, updated_at) VALUES('deletePatient', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
END IF;

END $$;
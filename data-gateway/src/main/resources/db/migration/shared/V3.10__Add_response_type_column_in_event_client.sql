ALTER TABLE event_client
ADD COLUMN if not exists response_type varchar(255);

update event_client
set response_type = 'SIMPLE_JSON_RESPONSE';
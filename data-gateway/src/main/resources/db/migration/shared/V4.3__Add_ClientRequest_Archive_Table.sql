create table if not exists client_requests_archive
(
    id                      bigserial not null,
    client_request_id       bigint,
    created_at              timestamp,
    result_delivered        boolean,
    client_id               bigint,
    event_id                bigint,
    subscriber_url_id       bigint,
    response_data           text,
    request_data            text,
    delivered_at            timestamp,
    ref_id_delivered_at     timestamp,
    primary key (id)
);

create index if not exists client_requests_delivered_at
on client_requests (delivered_at);
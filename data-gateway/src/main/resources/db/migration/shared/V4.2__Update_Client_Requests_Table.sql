ALTER TABLE public.client_requests
ADD if not exists  response_data TEXT NULL;

ALTER TABLE public.client_requests
ADD if not exists  request_data TEXT NULL;

ALTER TABLE public.client_requests
ADD if not exists  delivered_at timestamp NULL;

ALTER TABLE public.client_requests
ADD if not exists  ref_id_delivered_at timestamp NULL;

ALTER TABLE public.client_requests
ADD if not exists callback_response_code INTEGER NULL;

UPDATE public.client_requests
SET response_data = payload;

ALTER TABLE public.client_requests
DROP COLUMN IF EXISTS payload;
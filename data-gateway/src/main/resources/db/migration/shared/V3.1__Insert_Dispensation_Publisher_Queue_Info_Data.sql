INSERT INTO public.publisher_queue_info (queue_name, event_name, exchange,routing_key, reply_to_routing_key)
VALUES ('q.dg.disp_add_products','dispensation-add-products','direct-incoming','q.dg.disp_add_products','dispensation-add-products') ON CONFLICT (id) DO NOTHING;

INSERT INTO public.publisher_queue_info (queue_name, event_name, exchange,routing_key, reply_to_routing_key)
VALUES ('q.dg.disp_stock_credit','dispensation-stock-credit','direct-incoming','q.dg.disp_stock_credit','dispensation-stock-credit') ON CONFLICT (id) DO NOTHING;

INSERT INTO public.publisher_queue_info (queue_name, event_name, exchange,routing_key, reply_to_routing_key)
VALUES ('q.dg.disp_stock_debit','dispensation-stock-debit','direct-incoming','q.dg.disp_stock_debit','dispensation-stock-debit') ON CONFLICT (id) DO NOTHING;

INSERT INTO public.publisher_queue_info (queue_name, event_name, exchange,routing_key, reply_to_routing_key)
VALUES ('q.dg.disp_stock_search','dispensation-stock-search','direct-incoming','q.dg.disp_stock_search','dispensation-stock-search') ON CONFLICT (id) DO NOTHING;

INSERT INTO public.publisher_queue_info (queue_name, event_name, exchange,routing_key, reply_to_routing_key)
VALUES ('q.dg.disp_product_search','dispensation-product-search','direct-incoming','q.dg.disp_product_search','dispensation-product-search') ON CONFLICT (id) DO NOTHING;
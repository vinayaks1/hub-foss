INSERT INTO public.event (event_name, created_at,updated_at) VALUES('dispensation-add-products', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO public.event (event_name, created_at,updated_at) VALUES('dispensation-stock-credit', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO public.event (event_name, created_at,updated_at) VALUES('dispensation-stock-debit', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO public.event (event_name, created_at,updated_at) VALUES('dispensation-stock-search', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO public.event (event_name, created_at,updated_at) VALUES('dispensation-product-search', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

# User Service

User Service is created to abstract user management features across all Everwell services.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Things to be installed to run User Service

* Java 11
* IntelliJ IDEA / Eclipse or any editor capable of running Java
* Install Maven
* Add Lombok Plugin

For running pure development environment

* Install Postgres
* Install Rabbitmq

### Installing and Deployment

A step by step series of examples that tell you how to get a development env running

A dockerfile has been created to easily deploy the application with a specific environment
The dockerfile is able to choose a host of options as present in
```
docker build MVN_PROFILE=<ENVIRONMENT> -t "userservice-v1:user-service"
```

For the first time installation this would pull all images through the maven and use it internally for building the executable version of the application.

Before running the docker image within the container, please make sure that all dependencies has been started.

To run the image just made, use
```
docker run --net=host -d userservice-v1:user-service
```

The application details as started in the container can be checked as
```
docker logs <CONTAINER_ID>

2021-03-11 05:44:54.386  INFO 1 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 9099 (http) with context path ''
2021-03-11 05:44:54.406  INFO 1 --- [           main] o.s.boot.StartupInfoLogger.logStarted : Started InitApplication
```

```

## Built With

* [Springboot](https://spring.io/projects/spring-boot) - The Java framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [Mockito](https://github.com/mockito/mockito) - Used to generate mocks for testing independently

## License

This project is licensed under the MIT License
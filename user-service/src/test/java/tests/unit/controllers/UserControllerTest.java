package tests.unit.controllers;

import com.everwell.userservice.enums.DeduplicationScheme;
import com.everwell.userservice.exceptions.CustomExceptionHandler;

import com.everwell.userservice.controllers.UserController;
import com.everwell.userservice.models.db.User;
import com.everwell.userservice.models.db.UserMobile;
import com.everwell.userservice.models.dtos.*;
import com.everwell.userservice.services.UserService;
import com.everwell.userservice.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import tests.BaseTestNoSpring;
import static com.everwell.userservice.constants.Constants.X_US_CLIENT_ID;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;


public class UserControllerTest extends BaseTestNoSpring
{
    @InjectMocks
    private UserController userController;

    @Mock
    private UserService userService;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(userController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void testGetUserById() throws Exception
    {
        long clientId = 1L;
        UserResponse user = new UserResponse();
        user.setDateOfBirth(new Date());
        when(userService.get(anyLong(), anyLong())).thenReturn(user);
        String uri = "/v1/user";
        mockMvc.perform(
                MockMvcRequestBuilders.get(uri)
                        .header(X_US_CLIENT_ID,Long.toString(clientId))
                        .param("id", "1")
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetUserById_NotFound() throws Exception
    {
        long clientId = 1L;
        when(userService.get(anyLong(), anyLong())).thenReturn(null);
        String uri = "/v1/user";
        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .header(X_US_CLIENT_ID,Long.toString(clientId))
                        .param("id", "1")
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetUsersByPrimaryPhone() throws Exception
    {
        long clientId = 1L;
        when(userService.getByPrimaryPhone(anyString(), anyLong())).thenReturn(new ArrayList<>());
        String phoneNumber = "9988799887";
        String uri = "/v1/users/primaryPhone/" + phoneNumber;
        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .header(X_US_CLIENT_ID,Long.toString(clientId))
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value(new ArrayList<>()));
    }

    @Test
    public void testFetchDuplicates_PrimaryPhoneAndGender_Empty() throws Exception
    {
        long clientId = 1L;
        FetchDuplicatesRequestForPrimaryPhoneAndGender request = new FetchDuplicatesRequestForPrimaryPhoneAndGender("9988799887", "Male");
        request.setOffset(0);
        request.setCount(10);
        request.setScheme(DeduplicationScheme.PRIMARYPHONE_GENDER);

        when(userService.fetchDuplicates(any(), any())).thenReturn(new ArrayList<>());
        String uri = "/v1/users/duplicates";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                        .header(X_US_CLIENT_ID,Long.toString(clientId))
                        .content(Utils.asJsonString(request))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value(new ArrayList<>()));
    }

    @Test
    public void testFetchDuplicates_PrimaryPhoneAndGender() throws Exception
    {
        long clientId = 1L;
        FetchDuplicatesRequestForPrimaryPhoneAndGender request = new FetchDuplicatesRequestForPrimaryPhoneAndGender("9988799887", "Male");
        request.setOffset(0);
        request.setCount(10);
        request.setScheme(DeduplicationScheme.PRIMARYPHONE_GENDER);

        UserResponse sampleUser = new UserResponse();
        sampleUser.setId(1L);
        sampleUser.setFirstName("John");
        sampleUser.setLastName("Doe");

        when(userService.fetchDuplicates(any(), any())).thenReturn(Collections.singletonList(sampleUser));
        String uri = "/v1/users/duplicates";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                        .header(X_US_CLIENT_ID,Long.toString(clientId))
                        .content(Utils.asJsonString(request))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].id").value(1L))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].firstName").value("John"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].lastName").value("Doe"));
    }

    @Test
    public void testCreateUser() throws Exception
    {
        long clientId = 1L;
        AddUserRequest addUserRequest = new AddUserRequest();
        addUserRequest.setFirstName("ABCD");
        addUserRequest.setLastName("EFGH");
        addUserRequest.setDateOfBirth(Utils.getFormattedDate(new Date(), "yyyy-MM-dd"));
        addUserRequest.setAddress("abcd");
        addUserRequest.setCity("abc");
        addUserRequest.setGender("Male");

        addUserRequest.setMobileList(Collections.singletonList(new UserMobileRequest("9988799887", true, false, null)));
        addUserRequest.setEmailList(Collections.singletonList(new UserEmailRequest("john.doe@gmail.com", true)));

        UserResponse userResponse = new UserResponse();
        userResponse.setFirstName(addUserRequest.getFirstName());
        userResponse.setLastName(addUserRequest.getLastName());

        when(userService.create(any(), any(), anyBoolean())).thenReturn(userResponse);
        String uri = "/v1/users";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                        .header(X_US_CLIENT_ID,Long.toString(clientId))
                        .content(Utils.asJsonString(addUserRequest))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.firstName").value(addUserRequest.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.lastName").value(addUserRequest.getLastName()));
    }
    @Test
    public void testCreateUserMultipleMobiles() throws Exception
    {
        long clientId = 1L;
        long userId = 1L;
        String mobileNumber1 = "9988799887";
        String mobileNumber2 = "9121231231";
        String mobileOwner1 = "patient";
        AddUserRequest addUserRequest = new AddUserRequest();
        addUserRequest.setFirstName("ABCD");
        addUserRequest.setLastName("EFGH");
        addUserRequest.setDateOfBirth(Utils.getFormattedDate(new Date(), "yyyy-MM-dd"));
        addUserRequest.setAddress("abcd");
        addUserRequest.setCity("abc");
        addUserRequest.setGender("Male");
        List<UserMobileRequest> mobileList = new ArrayList<>();
        UserMobileRequest userMobileRequest1 = new UserMobileRequest(mobileNumber1, true, false, mobileOwner1);
        UserMobileRequest userMobileRequest2 = new UserMobileRequest(mobileNumber2, false, false, null);
        mobileList.add(userMobileRequest1);
        mobileList.add(userMobileRequest2);

        List<UserMobile> userMobiles = new ArrayList<>();
        UserMobile userMobile1 = new UserMobile(mobileNumber1, userId, true, null, null, false, mobileOwner1);
        UserMobile userMobile2 = new UserMobile(mobileNumber2, userId, false, null, null, false, null);
        userMobiles.add(userMobile1);
        userMobiles.add(userMobile2);

        addUserRequest.setMobileList(mobileList);
        addUserRequest.setEmailList(Collections.singletonList(new UserEmailRequest("john.doe@gmail.com", true)));

        UserResponse userResponse = new UserResponse();
        userResponse.setFirstName(addUserRequest.getFirstName());
        userResponse.setLastName(addUserRequest.getLastName());
        userResponse.setId(userId);
        userResponse.setMobileList(userMobiles);

        when(userService.create(any(), any(), anyBoolean())).thenReturn(userResponse);
        String uri = "/v1/users";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                        .header(X_US_CLIENT_ID,Long.toString(clientId))
                        .content(Utils.asJsonString(addUserRequest))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.firstName").value(addUserRequest.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.lastName").value(addUserRequest.getLastName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.mobileList[0].number").value(mobileNumber1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.mobileList[0].owner").value(mobileOwner1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.mobileList[1].number").value(mobileNumber2));
    }

    @Test
    public void testUpdateUser() throws Exception {
        long clientId = 1L;
        UpdateUserRequest updateUserRequest = new UpdateUserRequest();
        updateUserRequest.setUserId(1L);
        updateUserRequest.setFirstName("ABC");

        User user = new User();
        user.setId(1L);
        user.setFirstName("DEF");
        doNothing().when(userService).update(any(),any());
        String uri = "/v1/users";
        mockMvc.perform(MockMvcRequestBuilders.put(uri)
                .header(X_US_CLIENT_ID,Long.toString(clientId))
                .content(Utils.asJsonString(updateUserRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value("user updated successfully"));
    }
    
    @Test
    public void testDeleteUser() throws Exception {
        long clientId = 1L;
        DeleteUserRequest deleteUserRequest = new DeleteUserRequest(1L, 500L, "USER");
        doNothing().when(userService).delete(any(),any());
        String uri = "/v1/users";
        mockMvc.perform(MockMvcRequestBuilders.delete(uri)
                        .header(X_US_CLIENT_ID,Long.toString(clientId))
                        .content(Utils.asJsonString(deleteUserRequest))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value("user details deleted successfully"));
    }

    @Test
    public void testGetUserRelation() throws Exception {
        String uri = "/v1/user/relation?userId=1&status=ACCEPT";
        when(userService.getUserRelations(eq(1L), eq("ACCEPT"))).thenReturn(new ArrayList<>());

        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetUserRelationNoUserId() throws Exception {
        String uri = "/v1/user/relation?userId=&status=ACCEPT";

        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testGetUserRelationNoStatus() throws Exception {
        String uri = "/v1/user/relation?userId=1&status=";

        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testGetUserRelationByPhone() throws Exception {
        String uri = "/v1/users/relation?phoneNumber=123&primaryUserId=1";
        when(userService.getUserRelationsPrimaryPhone(eq("123"), eq(1L))).thenReturn(new ArrayList<>());

        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetUserRelationByPhoneNoNumber() throws Exception {
        String uri = "/v1/users/relation?phoneNumber=&primaryUserId=1";

        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testGetUserRelationByPhoneNoPrimaryUserId() throws Exception {
        String uri = "/v1/users/relation?phoneNumber=123&primaryUserId=";

        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testAddRelation() throws Exception {
        String uri = "/v1/user/relation";
        UserRelationDto userRelationDto = new UserRelationDto(1L, 2L, "test", "test", "test", "ACCEPT", false);
        doNothing().when(userService).addUserRelation(any());

        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                        .content(Utils.asJsonString(userRelationDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testAddRelationNoPrimaryUserId() throws Exception {
        String uri = "/v1/user/relation";
        UserRelationDto userRelationDto = new UserRelationDto(null, 2L, "test", "test", "test", "ACCEPT", false);

        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                        .content(Utils.asJsonString(userRelationDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testAddRelationNoSecondaryUserId() throws Exception {
        String uri = "/v1/user/relation";
        UserRelationDto userRelationDto = new UserRelationDto(1L, null, "test", "test", "test", "ACCEPT", false);

        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                        .content(Utils.asJsonString(userRelationDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testAddRelationSameUserId() throws Exception {
        String uri = "/v1/user/relation";
        UserRelationDto userRelationDto = new UserRelationDto(1L, 1L, "test", "test", "test", "ACCEPT", false);

        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                        .content(Utils.asJsonString(userRelationDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateRelation() throws Exception {
        String uri = "/v1/user/relation";
        UserRelationDto userRelationDto = new UserRelationDto(1L, 2L, "test", "test", "test", "ACCEPT", false);
        doNothing().when(userService).updateUserRelation(any());

        mockMvc.perform(MockMvcRequestBuilders.put(uri)
                        .content(Utils.asJsonString(userRelationDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteRelation() throws Exception {
        String uri = "/v1/user/relation";
        UserRelationDto userRelationDto = new UserRelationDto(1L, 2L, "test", "test", "test", "ACCEPT", false);
        doNothing().when(userService).deleteUserRelation(any());

        mockMvc.perform(MockMvcRequestBuilders.delete(uri)
                        .content(Utils.asJsonString(userRelationDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
    @Test
    public void searchUser_nameEmpty() throws Exception {
        long clientId = 1L;
        FilterUserRequest filterUserRequest = new FilterUserRequest("abc@sbx","42-7175-8604-4553","9865763451",null,"Male","1999");
        String uri = "/v1/users/search";

        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .header(X_US_CLIENT_ID,Long.toString(clientId))
                .content(Utils.asJsonString(filterUserRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("name cannot be empty"));
    }

    @Test
    public void searchUser_success() throws Exception {
        long clientId = 1L;
        FilterUserRequest filterUserRequest = new FilterUserRequest("abc@sbx","42-7175-8604-4553","9865763451","test","Male","1999");
        String uri = "/v1/users/search";
        UserResponse sampleUser = new UserResponse();
        sampleUser.setId(1L);
        sampleUser.setFirstName("John");
        sampleUser.setLastName("Doe");
        when(userService.search(any(),any())).thenReturn(sampleUser);

        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .header(X_US_CLIENT_ID,Long.toString(clientId))
                .content(Utils.asJsonString(filterUserRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.id").value(1L))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.firstName").value("John"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.lastName").value("Doe"));
    }
}

package tests.unit.controllers;

import com.everwell.userservice.controllers.UserVitalController;
import com.everwell.userservice.exceptions.CustomExceptionHandler;
import com.everwell.userservice.models.db.User;
import com.everwell.userservice.models.dtos.vitals.*;
import com.everwell.userservice.models.http.requests.AddVitalDetailsRequest;
import com.everwell.userservice.models.http.requests.VitalDetailsRequest;
import com.everwell.userservice.models.http.requests.VitalGoalRequest;
import com.everwell.userservice.services.UserService;
import com.everwell.userservice.services.UserVitalService;
import com.everwell.userservice.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import tests.BaseTestNoSpring;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.everwell.userservice.constants.Constants.X_US_CLIENT_ID;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserVitalControllerTest extends BaseTestNoSpring {
    @InjectMocks
    private UserVitalController userVitalController;

    @Mock
    private UserVitalService userVitalService;

    @Mock
    private UserService userService;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(userVitalController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    private VitalDetailsDto getVitalDetailsDto() {
        List<VitalDetailsDto.VitalsData> vitalsDataList = new ArrayList<>();
        List<VitalsTimeData> vitalsTimeDataList = new ArrayList<>();
        vitalsTimeDataList.add(new VitalsTimeData("2022-11-29 00:00:00", 1L, 1L, "100"));
        vitalsDataList.add(new VitalDetailsDto.VitalsData("water", 1L, vitalsTimeDataList));

        List<VitalDetailsDto.VitalsAggregatedData> vitalsAggregatedDataList = new ArrayList<>();
        List<VitalsAggregatedTimeData> vitalsAggregatedTimeDataList = new ArrayList<>();
        vitalsAggregatedTimeDataList.add(new VitalsAggregatedTimeData("2022-11-29 00:00:00", 1L, 1L, 100f, 50f));
        vitalsAggregatedDataList.add(new VitalDetailsDto.VitalsAggregatedData("water", 1L, vitalsAggregatedTimeDataList));

       return new VitalDetailsDto(new ArrayList<>(), vitalsDataList, vitalsAggregatedDataList);
    }

    @Test
    public void testVitalsGoalByUserId() throws Exception {
        int clientId = 1;
        List<VitalGoalDto> vitalGoalDtoList = new ArrayList<>();
        vitalGoalDtoList.add(new VitalGoalDto(1L, "test", 10f, LocalDateTime.now(), null));
        when(userService.getUser(any(), any())).thenReturn(new User());
        when(userVitalService.getVitalsGoalByUserId(any())).thenReturn(vitalGoalDtoList);
        String uri = "/v1/users/1/vitals-goal";
        mockMvc.perform(
                        MockMvcRequestBuilders.get(uri)
                                .header(X_US_CLIENT_ID,Long.toString(clientId))
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].vitalId").value(vitalGoalDtoList.get(0).getVitalId()));
    }

    @Test
    public void testSaveUserVitalsGoalWithVitalId() throws Exception {
        int clientId = 1;
        VitalGoalRequest vitalGoalRequest = new VitalGoalRequest(1L, null, 10f);
        when(userService.getUser(any(), any())).thenReturn(new User());
        doNothing().when(userVitalService).saveUserVitalsGoal(any(), any());
        String uri = "/v1/users/1/vitals-goal";
        mockMvc.perform(
                        MockMvcRequestBuilders.post(uri)
                                .header(X_US_CLIENT_ID,Long.toString(clientId))
                                .content(Utils.asJsonString(vitalGoalRequest))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value("User vitals goal details saved successfully"));
    }

    @Test
    public void testSaveUserVitalsGoalWithVitalIdAndVitalName() throws Exception {
        int clientId = 1;
        VitalGoalRequest vitalGoalRequest = new VitalGoalRequest(1L, "test", 10f);
        when(userService.getUser(any(), any())).thenReturn(new User());
        doNothing().when(userVitalService).saveUserVitalsGoal(any(), any());
        String uri = "/v1/users/1/vitals-goal";
        mockMvc.perform(
                        MockMvcRequestBuilders.post(uri)
                                .header(X_US_CLIENT_ID,Long.toString(clientId))
                                .content(Utils.asJsonString(vitalGoalRequest))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value("User vitals goal details saved successfully"));
    }

    @Test
    public void testSaveUserVitalsGoalWithNullVitalValue() throws Exception
    {
        int clientId = 1;
        VitalGoalRequest vitalGoalRequest = new VitalGoalRequest(1L, null, null);
        String uri = "/v1/users/1/vitals-goal";
        mockMvc.perform(
                        MockMvcRequestBuilders.post(uri)
                                .header(X_US_CLIENT_ID,Long.toString(clientId))
                                .content(Utils.asJsonString(vitalGoalRequest))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testSaveUserVitalsGoalWithNullVitalIdAndNullVitalName() throws Exception
    {
        int clientId = 1;
        VitalGoalRequest vitalGoalRequest = new VitalGoalRequest(null, null, 10f);
        String uri = "/v1/users/1/vitals-goal";
        mockMvc.perform(
                        MockMvcRequestBuilders.post(uri)
                                .header(X_US_CLIENT_ID,Long.toString(clientId))
                                .content(Utils.asJsonString(vitalGoalRequest))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testSaveUserVitalsGoalWithNullVitalIdAndEmptyVitalName() throws Exception {
        int clientId = 1;
        VitalGoalRequest vitalGoalRequest = new VitalGoalRequest(null, "", 10f);
        String uri = "/v1/users/1/vitals-goal";
        mockMvc.perform(
                        MockMvcRequestBuilders.post(uri)
                                .header(X_US_CLIENT_ID,Long.toString(clientId))
                                .content(Utils.asJsonString(vitalGoalRequest))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testSaveUserVitalsGoalWithNullVitalIdAndValidVitalName() throws Exception
    {
        int clientId = 1;
        VitalGoalRequest vitalGoalRequest = new VitalGoalRequest(null, "test", 10f);
        when(userService.getUser(any(), any())).thenReturn(new User());
        doNothing().when(userVitalService).saveUserVitalsGoal(any(), any());
        String uri = "/v1/users/1/vitals-goal";
        mockMvc.perform(
                        MockMvcRequestBuilders.post(uri)
                                .header(X_US_CLIENT_ID,Long.toString(clientId))
                                .content(Utils.asJsonString(vitalGoalRequest))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value("User vitals goal details saved successfully"));
    }

    @Test
    public void testUpdateUserVitalsGoal() throws Exception
    {
        int clientId = 1;
        VitalGoalRequest vitalGoalRequest = new VitalGoalRequest(null, "water", 10f);
        when(userService.getUser(any(), any())).thenReturn(new User());
        doNothing().when(userVitalService).updateUserVitalsGoal(any(), any());

        String uri = "/v1/users/1/vitals-goal";
        mockMvc.perform(
                        MockMvcRequestBuilders.put(uri)
                                .header(X_US_CLIENT_ID,Long.toString(clientId))
                                .content(Utils.asJsonString(vitalGoalRequest))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value("User vitals goal details updated successfully"));
    }

    @Test
    public void testDeleteUserVitalsGoalByVitalId() throws Exception {
        int clientId = 1;
        when(userService.getUser(any(), any())).thenReturn(new User());
        doNothing().when(userVitalService).deleteUserVitalsGoal(any());
        String uri = "/v1/users/1/vitals-goal";
        mockMvc.perform(
                        MockMvcRequestBuilders.delete(uri)
                                .header(X_US_CLIENT_ID,Long.toString(clientId))
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value("User vitals goal details deleted successfully"));
    }

    @Test
    public void testGetVitalsDetailsByUserIdWithVitalIdList() throws Exception {
        String uri = "/v1/users/vitals/bulk";
        long clientId = 1L;
        List<User> userList = new ArrayList<>();
        userList.add(new User());
        VitalDetailsRequest vitalDetailsRequest = new VitalDetailsRequest(Collections.singletonList(1L), Collections.singletonList(1L), null, true, true, false, false, "2022-11-28 00:00:00", "2022-11-30 00:00:00");
        VitalDetailsDto vitalDetailsDto = getVitalDetailsDto();
        doNothing().when(userService).validateUserIdList(any(), any());
        when(userVitalService.getUserVitalsDetails(any())).thenReturn(vitalDetailsDto);
        mockMvc.perform(
                        MockMvcRequestBuilders.post(uri)
                                .header(X_US_CLIENT_ID,Long.toString(clientId))
                                .content(Utils.asJsonString(vitalDetailsRequest))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.logs[0].vitalId").value(1L))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.usersVitalGoalData").value(new ArrayList<>()));;
    }

    @Test
    public void testGetVitalsDetailsByUserIdWithVitalNameList() throws Exception {
        String uri = "/v1/users/vitals/bulk";
        long clientId = 1L;
        List<User> userList = new ArrayList<>();
        userList.add(new User());
        VitalDetailsRequest vitalDetailsRequest = new VitalDetailsRequest(Collections.singletonList(1L), null, Collections.singletonList("water"), false, true, false, false, "2022-11-28 00:00:00", "2022-11-30 00:00:00");
        VitalDetailsDto vitalDetailsDto = getVitalDetailsDto();
        doNothing().when(userService).validateUserIdList(any(), any());
        when(userVitalService.getUserVitalsDetails(any())).thenReturn(vitalDetailsDto);
        mockMvc.perform(
                        MockMvcRequestBuilders.post(uri)
                                .header(X_US_CLIENT_ID,Long.toString(clientId))
                                .content(Utils.asJsonString(vitalDetailsRequest))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.logs[0].vitalName").value("water"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.usersVitalGoalData").value(new ArrayList<>()));;
    }

    @Test
    public void testGetVitalsDetailsByUserIdWithVitalIdAndVitalNameList() throws Exception {
        String uri = "/v1/users/vitals/bulk";
        long clientId = 1L;
        List<User> userList = new ArrayList<>();
        userList.add(new User());
        VitalDetailsRequest vitalDetailsRequest = new VitalDetailsRequest(Collections.singletonList(1L), Collections.singletonList(1L), Collections.singletonList("water"), false, true, false, false, "2022-11-28 00:00:00", "2022-11-30 00:00:00");
        VitalDetailsDto vitalDetailsDto = getVitalDetailsDto();
        doNothing().when(userService).validateUserIdList(any(), any());
        when(userVitalService.getUserVitalsDetails(any())).thenReturn(vitalDetailsDto);
        mockMvc.perform(
                        MockMvcRequestBuilders.post(uri)
                                .header(X_US_CLIENT_ID,Long.toString(clientId))
                                .content(Utils.asJsonString(vitalDetailsRequest))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.logs[0].vitalId").value(1L))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.logs[0].vitalName").value("water"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.usersVitalGoalData").value(new ArrayList<>()));;
    }

    @Test
    public void testGetVitalsDetailsByUserIdWithInvalidUserIds() throws Exception {
        String uri = "/v1/users/vitals/bulk";
        long clientId = 1L;
        VitalDetailsRequest vitalDetailsRequest = new VitalDetailsRequest(null, Collections.singletonList(1L), null, false, true, false, false, "2022-11-28 00:00:00", "2022-11-30 00:00:00");
        mockMvc.perform(
                        MockMvcRequestBuilders.post(uri)
                                .header(X_US_CLIENT_ID,Long.toString(clientId))
                                .content(Utils.asJsonString(vitalDetailsRequest))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testGetVitalsDetailsByUserIdWithInvalidVitalIdAndVitalNameList() throws Exception {
        String uri = "/v1/users/vitals/bulk";
        long clientId = 1L;
        VitalDetailsRequest vitalDetailsRequest = new VitalDetailsRequest(Collections.singletonList(1L), null, null, false, true, false, false, "2022-11-28 00:00:00", "2022-11-30 00:00:00");
        mockMvc.perform(
                        MockMvcRequestBuilders.post(uri)
                                .header(X_US_CLIENT_ID,Long.toString(clientId))
                                .content(Utils.asJsonString(vitalDetailsRequest))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testGetVitalsDetailsByUserIdWithInvalidStartDateTime() throws Exception {
        long clientId = 1L;
        VitalDetailsRequest vitalDetailsRequest = new VitalDetailsRequest(Collections.singletonList(1L), Collections.singletonList(1L), null, false, true, false, false, null, "2022-11-30 00:00:00");
        String uri = "/v1/users/vitals/bulk";
        mockMvc.perform(
                        MockMvcRequestBuilders.post(uri)
                                .header(X_US_CLIENT_ID,Long.toString(clientId))
                                .content(Utils.asJsonString(vitalDetailsRequest))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testSaveUserVitalsDetailsWithVitalIdsValue() throws Exception {
        long clientId = 1L;
        String uri = "/v1/users/1/vitals";
        List<VitalDto> vitalDtoList = new ArrayList<>();
        vitalDtoList.add(new VitalDto(1L, null, "100L"));
        AddVitalDetailsRequest addVitalDetailsRequest = new AddVitalDetailsRequest(vitalDtoList);
        when(userService.getUser(any(), any())).thenReturn(new User());
        doNothing().when(userVitalService).saveUserVitalsDetails(any(), any());
        mockMvc.perform(
                        MockMvcRequestBuilders.post(uri)
                                .header(X_US_CLIENT_ID,Long.toString(clientId))
                                .content(Utils.asJsonString(addVitalDetailsRequest))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value("User vitals details saved successfully"));;
    }

    @Test
    public void testSaveUserVitalsDetailsWithVitalNameValue() throws Exception {
        long clientId = 1L;
        String uri = "/v1/users/1/vitals";
        List<VitalDto> vitalDtoList = new ArrayList<>();
        vitalDtoList.add(new VitalDto(null, "water", "100L"));
        AddVitalDetailsRequest addVitalDetailsRequest = new AddVitalDetailsRequest(vitalDtoList);
        when(userService.getUser(any(), any())).thenReturn(new User());
        doNothing().when(userVitalService).saveUserVitalsDetails(any(), any());
        mockMvc.perform(
                        MockMvcRequestBuilders.post(uri)
                                .header(X_US_CLIENT_ID,Long.toString(clientId))
                                .content(Utils.asJsonString(addVitalDetailsRequest))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value("User vitals details saved successfully"));;
    }

    @Test
    public void testSaveUserVitalsDetailsWithVitalIdAndVitalNameValue() throws Exception {
        long clientId = 1L;
        String uri = "/v1/users/1/vitals";
        List<VitalDto> vitalDtoList = new ArrayList<>();
        vitalDtoList.add(new VitalDto(1L, "water", "100L"));
        AddVitalDetailsRequest addVitalDetailsRequest = new AddVitalDetailsRequest(vitalDtoList);
        when(userService.getUser(any(), any())).thenReturn(new User());
        doNothing().when(userVitalService).saveUserVitalsDetails(any(), any());
        mockMvc.perform(
                        MockMvcRequestBuilders.post(uri)
                                .header(X_US_CLIENT_ID,Long.toString(clientId))
                                .content(Utils.asJsonString(addVitalDetailsRequest))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value("User vitals details saved successfully"));;
    }

    @Test
    public void testSaveUserVitalsDetailsWithEmptyVitalDetails() throws Exception {
        String uri = "/v1/users/1/vitals";
        long clientId = 1L;
        AddVitalDetailsRequest addVitalDetailsRequest = new AddVitalDetailsRequest(new ArrayList<>());
        mockMvc.perform(
                        MockMvcRequestBuilders.post(uri)
                                .header(X_US_CLIENT_ID,Long.toString(clientId))
                                .content(Utils.asJsonString(addVitalDetailsRequest))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testSaveUserVitalsDetailsWithNullVitalDetails() throws Exception {
        String uri = "/v1/users/1/vitals";
        long clientId = 1L;
        AddVitalDetailsRequest addVitalDetailsRequest = new AddVitalDetailsRequest(null);
        mockMvc.perform(
                        MockMvcRequestBuilders.post(uri)
                                .header(X_US_CLIENT_ID,Long.toString(clientId))
                                .content(Utils.asJsonString(addVitalDetailsRequest))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testSaveUserVitalsDetailsWithInvalidVitalIdAndNameData() throws Exception {
        String uri = "/v1/users/1/vitals";
        long clientId = 1L;
        List<VitalDto> vitalDtoList = new ArrayList<>();
        vitalDtoList.add(new VitalDto(null, null, "100L"));
        AddVitalDetailsRequest addVitalDetailsRequest = new AddVitalDetailsRequest(vitalDtoList);
        mockMvc.perform(
                        MockMvcRequestBuilders.post(uri)
                                .header(X_US_CLIENT_ID,Long.toString(clientId))
                                .content(Utils.asJsonString(addVitalDetailsRequest))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testSaveUserVitalsDetailsWithInvalidVitalValue() throws Exception {
        String uri = "/v1/users/1/vitals";
        long clientId = 1L;
        List<VitalDto> vitalDtoList = new ArrayList<>();
        vitalDtoList.add(new VitalDto(1L, null, null));
        AddVitalDetailsRequest addVitalDetailsRequest = new AddVitalDetailsRequest(vitalDtoList);
        mockMvc.perform(
                        MockMvcRequestBuilders.post(uri)
                                .header(X_US_CLIENT_ID,Long.toString(clientId))
                                .content(Utils.asJsonString(addVitalDetailsRequest))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }
}

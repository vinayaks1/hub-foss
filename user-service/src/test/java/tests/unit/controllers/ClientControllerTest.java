package tests.unit.controllers;

import com.everwell.userservice.controllers.ClientController;
import com.everwell.userservice.exceptions.CustomExceptionHandler;
import com.everwell.userservice.models.http.requests.ClientRequest;
import com.everwell.userservice.models.http.responses.ClientResponse;
import com.everwell.userservice.services.ClientService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import tests.BaseTest;

import static com.everwell.userservice.constants.Constants.X_US_CLIENT_ID;
import static com.everwell.userservice.utils.Utils.asJsonString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ClientControllerTest extends BaseTest {

    @Autowired
    private MockMvc mockMvc;

    @InjectMocks
    private ClientController clientController;

    @Mock
    private ClientService clientService;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(clientController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void testRegisterClient() throws Exception {
        ClientRequest clientRequest = new ClientRequest("TestClient", "Test_Client");
        ClientResponse clientResponse = new ClientResponse(1L, "TestClient");
        when(clientService.registerClient(any())).thenReturn(clientResponse);
        mockMvc
                .perform(post("/v1/client")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(clientRequest)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.message").value("client registered successfully"))
                .andExpect(jsonPath("$.data.id").value(clientResponse.getId()))
                .andExpect(jsonPath("$.data.name").value(clientResponse.getName()));
    }

    @Test
    public void testRegisterClientWithNullName() throws Exception {
        ClientRequest clientRequest = new ClientRequest("", "Test@123");
        ClientResponse clientResponse = new ClientResponse(1L, "TestClient");
        when(clientService.registerClient(any())).thenReturn(clientResponse);
        mockMvc
                .perform(post("/v1/client")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(clientRequest)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("client name cannot be null or empty"));
    }

    @Test
    public void testRegisterClientWithNullPassword() throws Exception {
        ClientRequest clientRequest = new ClientRequest("Test", "");
        ClientResponse clientResponse = new ClientResponse(1L, "TestClient");
        when(clientService.registerClient(any())).thenReturn(clientResponse);
        mockMvc
                .perform(post("/v1/client")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(clientRequest)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("client password cannot be null or empty"));
    }

    @Test
    public void testRegisterClientWithUsernameLengthGreaterThan255() throws Exception {
        ClientRequest clientRequest = new ClientRequest("Testqwetrytyuuoioiuoiuiobhbgggiugiuggggggggggggggggggggvuyfffffffffffffffffffffffffoooooooooooooooooooooooooooooooooooooooofffffffffffffffffffffffffffffffffffffffffffffffuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuddddddddddddddddddddddddddddddddddddddddddddddddddnjhjk", "Test@123");
        ClientResponse clientResponse = new ClientResponse(1L, "TestClient");
        when(clientService.registerClient(any())).thenReturn(clientResponse);
        mockMvc
                .perform(post("/v1/client")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(clientRequest)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("client name length should be less than 255 characters"));
    }

    @Test
    public void testRegisterClientWithPasswordLengthGreaterThan255() throws Exception {
        ClientRequest clientRequest = new ClientRequest("Test", "qwetrytyuuoioiuoiuiobhbgggiugiuggggggggggggggggggggvuyfffffffffffffffffffffffffoooooooooooooooooooooooooooooooooooooooofffffffffffffffffffffffffffffffffffffffffffffffuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuddddddddddddddddddddddddddddddddddddddddddddddddddnjhjk");
        ClientResponse clientResponse = new ClientResponse(1L, "TestClient");
        when(clientService.registerClient(any())).thenReturn(clientResponse);
        mockMvc
                .perform(post("/v1/client")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(clientRequest)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("client password length should be less than 255 characters"));
    }

    @Test
    public void testGetClient() throws Exception {
        long clientId = 1L;
        ClientResponse clientResponse = new ClientResponse(1L, "TestClient", "authToken");
        when(clientService.getClientDetails(any())).thenReturn(clientResponse);
        mockMvc
                .perform(get("/v1/client")
                        .header(X_US_CLIENT_ID, Long.toString(clientId)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message").value("client details fetched successfully"))
                .andExpect(jsonPath("$.data.id").value(clientResponse.getId()))
                .andExpect(jsonPath("$.data.name").value(clientResponse.getName()));
    }

}

package tests.unit.controllers;

import com.everwell.userservice.controllers.ABHAController;
import com.everwell.userservice.exceptions.CustomExceptionHandler;
import com.everwell.userservice.models.dtos.*;
import com.everwell.userservice.models.http.requests.*;
import com.everwell.userservice.models.http.responses.*;
import com.everwell.userservice.services.ABHAService;
import com.everwell.userservice.services.UserService;
import com.everwell.userservice.utils.EncryptionUtils;
import com.everwell.userservice.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import tests.BaseTest;

import java.util.ArrayList;

import static com.everwell.userservice.constants.Constants.*;
import static com.everwell.userservice.constants.ABHAConstants.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ABHAControllerTest extends BaseTest
{
    @InjectMocks
    private ABHAController abhaController;

    @Mock
    private ABHAService abhaService;

    @Mock
    private UserService userService;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(abhaController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void testGenerateOtpForAadhaar() throws Exception
    {
        AadhaarOTPRequest aadhaarOTPRequest = new AadhaarOTPRequest(EncryptionUtils.encrypt("123456123456"));
        AadhaarOTPResponse aadhaarOTPResponse = new AadhaarOTPResponse();
        aadhaarOTPResponse.setTxnId("12334-abcd");
        when(abhaService.generateOtp(any(), anyString())).thenReturn(aadhaarOTPResponse);
        String uri = "/v1/abha/registration/aadhaar/generate-otp";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .content(Utils.asJsonString(aadhaarOTPRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.txnId").value(aadhaarOTPResponse.getTxnId()));
    }

    @Test
    public void testResendOtpForAadhaar() throws Exception
    {
        ResendOTPRequest resendOTPRequest = new ResendOTPRequest();
        resendOTPRequest.setTxnId("12334-abcd");
        doNothing().when(abhaService).resendOtp(any(), anyString());
        String uri = "/v1/abha/registration/aadhaar/resend-otp";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .content(Utils.asJsonString(resendOTPRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value(true));
    }

    @Test
    public void testVerifyOtpForAadhaar() throws Exception
    {
        OTPVerifyRequest otpVerifyRequest = new OTPVerifyRequest();
        otpVerifyRequest.setOtp("123456");
        otpVerifyRequest.setTxnId("12334-abcd");
        AadhaarOTPResponse aadhaarOTPResponse = new AadhaarOTPResponse();
        aadhaarOTPResponse.setTxnId("12334-abcd");
        when(abhaService.verifyOtp(any(), anyString())).thenReturn(aadhaarOTPResponse);
        String uri = "/v1/abha/registration/aadhaar/verify-otp";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .param("otpType", "aadhaar")
                .content(Utils.asJsonString(otpVerifyRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.txnId").value(aadhaarOTPResponse.getTxnId()));
    }

    @Test
    public void testGenerateOtpForMobile() throws Exception
    {
        MobileOTPRequest mobileOTPRequest = new MobileOTPRequest("09897654321", "12334-abcd");
        AadhaarOTPResponse aadhaarOTPResponse = new AadhaarOTPResponse();
        aadhaarOTPResponse.setTxnId("12334-abcd");
        when(abhaService.generateOtp(any(), anyString())).thenReturn(aadhaarOTPResponse);
        String uri = "/v1/abha/registration/aadhaar/generate-mobile-otp";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .content(Utils.asJsonString(mobileOTPRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.txnId").value(aadhaarOTPResponse.getTxnId()));
    }

    @Test
    public void testVerifyOtpForMobile() throws Exception
    {
        OTPVerifyRequest otpVerifyRequest = new OTPVerifyRequest();
        otpVerifyRequest.setOtp("123456");
        otpVerifyRequest.setTxnId("12334-abcd");
        AadhaarOTPResponse aadhaarOTPResponse = new AadhaarOTPResponse();
        aadhaarOTPResponse.setTxnId("12334-abcd");
        when(abhaService.verifyOtp(any(), anyString())).thenReturn(aadhaarOTPResponse);
        String uri = "/v1/abha/registration/aadhaar/verify-otp";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .param("otpType", "mobile")
                .content(Utils.asJsonString(otpVerifyRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.txnId").value(aadhaarOTPResponse.getTxnId()));
    }

    @Test
    public void testCreateAbha() throws Exception
    {
        Long clientId = 1L;
        ABHACreateRequest abhaCreateRequest = new ABHACreateRequest();
        abhaCreateRequest.setTxnId("12334-abcd");
        abhaCreateRequest.setUserId(1L);
        ABHACreateResponse abhaCreateResponse = new ABHACreateResponse();
        abhaCreateResponse.setName("Marcus E");
        abhaCreateResponse.setHealthIdNumber("91-1234-1234-1234");
        UpdateUserRequest updateUserRequest = new UpdateUserRequest();
        updateUserRequest.setUserId(1L);
        updateUserRequest.setFirstName("Marcus");
        when(abhaService.createAbha(any())).thenReturn(abhaCreateResponse);
        when(abhaService.buildUpdateUserReq(anyLong(), anyString(), any())).thenReturn(updateUserRequest);
        doNothing().when(userService).update(any(), anyLong());
        String uri = "/v1/abha/registration/aadhaar/create-abha";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .header(X_US_CLIENT_ID,Long.toString(clientId))
                .content(Utils.asJsonString(abhaCreateRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.healthIdNumber").value(abhaCreateResponse.getHealthIdNumber()));
    }

    @Test
    public void testSearchAbha() throws Exception
    {
        ABHASearchRequest abhaSearchRequest = new ABHASearchRequest("91-1234-1234-1234");
        ABHASearchResponse abhaSearchResponse = new ABHASearchResponse("ACTIVE");
        when(abhaService.searchAbha(any())).thenReturn(abhaSearchResponse);
        String uri = "/v1/abha/search/health-id";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .content(Utils.asJsonString(abhaSearchRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.status").value(abhaSearchResponse.getStatus()));
    }

    @Test
    public void testGenerateOtpForHealthId() throws Exception
    {
        HealthIdOTPRequest healthIdOTPRequest = new HealthIdOTPRequest("AADHAAR_OTP", "91-1234-1234-1234");
        AadhaarOTPResponse aadhaarOTPResponse = new AadhaarOTPResponse();
        aadhaarOTPResponse.setTxnId("12334-abcd");
        when(abhaService.generateOtp(any(), anyString())).thenReturn(aadhaarOTPResponse);
        String uri = "/v1/abha/auth/generate-otp/health-id";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .content(Utils.asJsonString(healthIdOTPRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.txnId").value(aadhaarOTPResponse.getTxnId()));
    }

    @Test
    public void testResendAuthOtp() throws Exception
    {
        ResendOTPRequest resendOTPRequest = new ResendOTPRequest("12334-abcd", "AADHAAR_OTP");
        doNothing().when(abhaService).resendOtp(any(), anyString());
        String uri = "/v1/abha/auth/resend-otp";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .content(Utils.asJsonString(resendOTPRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value(true));
    }

    @Test
    public void testConfirmWithAadhaarOtp() throws Exception
    {
        Long clientId = 1L;
        Long userId = 1L;
        OTPVerifyRequest otpVerifyRequest = new OTPVerifyRequest();
        otpVerifyRequest.setOtp("123456");
        otpVerifyRequest.setTxnId("12334-abcd");
        otpVerifyRequest.setUserId(userId);
        ABHAAuthResponse abhaAuthResponse = new ABHAAuthResponse();
        abhaAuthResponse.setToken("eyJhbGciOiJSUzUxMiJ9");
        ABHAProfileResponse abhaProfileResponse = new ABHAProfileResponse();
        abhaProfileResponse.setAddress("4/13 temporary address");
        abhaProfileResponse.setPincode("350006");
        UpdateUserRequest updateUserRequest = new UpdateUserRequest();
        updateUserRequest.setUserId(userId);
        updateUserRequest.setFirstName("Marcus");
        when(abhaService.verifyOtpForAuth(any())).thenReturn(abhaAuthResponse);
        when(abhaService.getProfile(anyString())).thenReturn(abhaProfileResponse);
        when(abhaService.buildUpdateUserReq(anyLong(), anyString(), any())).thenReturn(updateUserRequest);
        doNothing().when(userService).update(any(), anyLong());
        String uri = "/v1/abha/auth/confirm-with-aadhaar-otp";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .header(X_US_CLIENT_ID,Long.toString(clientId))
                .content(Utils.asJsonString(otpVerifyRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.token").value(abhaAuthResponse.getToken()));
    }

    @Test
    public void testGetAbhaProfile() throws Exception
    {
        ABHAProfileResponse abhaProfileResponse = new ABHAProfileResponse();
        abhaProfileResponse.setAddress("4/13 temporary address");
        abhaProfileResponse.setPincode("350006");
        when(abhaService.getProfile(anyString())).thenReturn(abhaProfileResponse);
        String uri = "/v1/abha/account/profile";
        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                .header(ABHA_X_TOKEN,"ejy340ijff")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.pincode").value(abhaProfileResponse.getPincode()));
    }

    @Test
    public void testGetAbhaCardImage() throws Exception
    {
        byte[] imageByteArr = "ABHA Image".getBytes();
        when(abhaService.getAbhaImage(anyLong(), anyString())).thenReturn(imageByteArr);
        String uri = "/v1/abha/account/image?imageName=fafds&userId=21344";
        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                .param("imageName", "card")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value(true));
    }

    @Test
    public void testGetAbhaQRImage() throws Exception
    {
        byte[] imageByteArr = "ABHA Image".getBytes();
        when(abhaService.getAbhaImage(anyLong(), anyString())).thenReturn(imageByteArr);
        String uri = "/v1/abha/account/image?imageName=fafds&userId=21344";
        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                .param("imageName", "qr")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value(true));
    }

    @Test
    public void testGenerateOtpForMobileEmailLogin() throws Exception
    {
        MobileEmailOTPRequest mobileEmailOTPRequest = new MobileEmailOTPRequest();
        mobileEmailOTPRequest.setValue("JdfjlkjlfjjUOIUjoj==");

        MobileEmailOTPRequester mobileEmailOTPRequester = new MobileEmailOTPRequester();
        mobileEmailOTPRequester.setType("PHR");
        mobileEmailOTPRequester.setId("phr_001");


        mobileEmailOTPRequest.setRequester(mobileEmailOTPRequester);
        mobileEmailOTPRequest.setPurpose("REGISTRATION");
        mobileEmailOTPRequest.setAuthMode("MOBILE_OTP");
        mobileEmailOTPRequest.setUserId(1234L);


        MobileEmailOTPResponse mobileEmailOTPResponse = new MobileEmailOTPResponse();
        mobileEmailOTPResponse.setTransactionId("12334-abcd");
        mobileEmailOTPResponse.setAuthMode("MOBILE_OTP");
        mobileEmailOTPResponse.setRequesterId("phr_001");


        when(abhaService.generateOtpForMobileEmailLogin(any(), anyString())).thenReturn(mobileEmailOTPResponse);
        String uri = "/v1/mobileEmail/login/generateMobileEmailOtp";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .content(Utils.asJsonString(mobileEmailOTPRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.transactionId").value(mobileEmailOTPResponse.getTransactionId()));
    }

    @Test
    public void testGenerateOtpForMobileEmailLoginWithInvalidUserId() throws Exception
    {
        MobileEmailOTPRequest mobileEmailOTPRequest = new MobileEmailOTPRequest();
        mobileEmailOTPRequest.setValue("JdfjlkjlfjjUOIUjoj==");

        MobileEmailOTPRequester mobileEmailOTPRequester = new MobileEmailOTPRequester();
        mobileEmailOTPRequester.setType("PHR");
        mobileEmailOTPRequester.setId("phr_001");


        mobileEmailOTPRequest.setRequester(mobileEmailOTPRequester);
        mobileEmailOTPRequest.setPurpose("REGISTRATION");


        MobileEmailOTPResponse mobileEmailOTPResponse = new MobileEmailOTPResponse();
        mobileEmailOTPResponse.setTransactionId("12334-abcd");
        mobileEmailOTPResponse.setAuthMode("MOBILE_OTP");
        mobileEmailOTPResponse.setRequesterId("phr_001");


        when(abhaService.generateOtpForMobileEmailLogin(any(), anyString())).thenReturn(mobileEmailOTPResponse);
        String uri = "/v1/mobileEmail/login/generateMobileEmailOtp";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .content(Utils.asJsonString(mobileEmailOTPRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testGenerateOtpForMobileEmailLoginWithInvalidAuthMode() throws Exception
    {
        MobileEmailOTPRequest mobileEmailOTPRequest = new MobileEmailOTPRequest();
        mobileEmailOTPRequest.setValue("JdfjlkjlfjjUOIUjoj==");

        MobileEmailOTPRequester mobileEmailOTPRequester = new MobileEmailOTPRequester();
        mobileEmailOTPRequester.setType("PHR");
        mobileEmailOTPRequester.setId("phr_001");


        mobileEmailOTPRequest.setRequester(mobileEmailOTPRequester);
        mobileEmailOTPRequest.setPurpose("REGISTRATION");
        mobileEmailOTPRequest.setUserId(1234L);


        MobileEmailOTPResponse mobileEmailOTPResponse = new MobileEmailOTPResponse();
        mobileEmailOTPResponse.setTransactionId("12334-abcd");
        mobileEmailOTPResponse.setAuthMode("MOBILE_OTP");
        mobileEmailOTPResponse.setRequesterId("phr_001");


        when(abhaService.generateOtpForMobileEmailLogin(any(), anyString())).thenReturn(mobileEmailOTPResponse);
        String uri = "/v1/mobileEmail/login/generateMobileEmailOtp";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .content(Utils.asJsonString(mobileEmailOTPRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testGenerateOtpForMobileEmailLoginWithInvalidPurpose() throws Exception
    {
        MobileEmailOTPRequest mobileEmailOTPRequest = new MobileEmailOTPRequest();
        mobileEmailOTPRequest.setValue("JdfjlkjlfjjUOIUjoj==");

        MobileEmailOTPRequester mobileEmailOTPRequester = new MobileEmailOTPRequester();
        mobileEmailOTPRequester.setType("PHR");
        mobileEmailOTPRequester.setId("phr_001");


        mobileEmailOTPRequest.setRequester(mobileEmailOTPRequester);
        mobileEmailOTPRequest.setAuthMode("MOBILE_OTP");
        mobileEmailOTPRequest.setUserId(1234L);


        MobileEmailOTPResponse mobileEmailOTPResponse = new MobileEmailOTPResponse();
        mobileEmailOTPResponse.setTransactionId("12334-abcd");
        mobileEmailOTPResponse.setAuthMode("MOBILE_OTP");
        mobileEmailOTPResponse.setRequesterId("phr_001");


        when(abhaService.generateOtpForMobileEmailLogin(any(), anyString())).thenReturn(mobileEmailOTPResponse);
        String uri = "/v1/mobileEmail/login/generateMobileEmailOtp";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .content(Utils.asJsonString(mobileEmailOTPRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testGenerateOtpForMobileEmailLoginWithInvalidValue() throws Exception
    {
        MobileEmailOTPRequest mobileEmailOTPRequest = new MobileEmailOTPRequest();

        MobileEmailOTPRequester mobileEmailOTPRequester = new MobileEmailOTPRequester();
        mobileEmailOTPRequester.setType("PHR");
        mobileEmailOTPRequester.setId("phr_001");


        mobileEmailOTPRequest.setRequester(mobileEmailOTPRequester);
        mobileEmailOTPRequest.setPurpose("REGISTRATION");
        mobileEmailOTPRequest.setAuthMode("MOBILE_OTP");
        mobileEmailOTPRequest.setUserId(1234L);


        MobileEmailOTPResponse mobileEmailOTPResponse = new MobileEmailOTPResponse();
        mobileEmailOTPResponse.setTransactionId("12334-abcd");
        mobileEmailOTPResponse.setAuthMode("MOBILE_OTP");
        mobileEmailOTPResponse.setRequesterId("phr_001");


        when(abhaService.generateOtpForMobileEmailLogin(any(), anyString())).thenReturn(mobileEmailOTPResponse);
        String uri = "/v1/mobileEmail/login/generateMobileEmailOtp";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .content(Utils.asJsonString(mobileEmailOTPRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }


    @Test
    public void testVerifyOtpForMobileEmailLogin() throws Exception
    {
        MobileEmailVerifyOTPRequest mobileEmailVerifyOTPRequest = new MobileEmailVerifyOTPRequest();
        mobileEmailVerifyOTPRequest.setAuthCode("MOBILE_OTP");
        mobileEmailVerifyOTPRequest.setTransactionId("12334-abcd");
        mobileEmailVerifyOTPRequest.setUserId(1234L);

        MobileEmailVerifyOTPResponse mobileEmailVerifyOTPResponse = new MobileEmailVerifyOTPResponse();
        mobileEmailVerifyOTPResponse.setTransactionId("12334-abcd");
        mobileEmailVerifyOTPResponse.setMappedPhrAddress(new ArrayList<String>());
        mobileEmailVerifyOTPResponse.setMobileEmail("MOBILE");


        when(abhaService.verifyOtpForMobileEmailLogin(any(), anyString())).thenReturn(mobileEmailVerifyOTPResponse);
        String uri = "/v1/mobileEmail/login/verify-otp";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .content(Utils.asJsonString(mobileEmailVerifyOTPRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.transactionId").value(mobileEmailVerifyOTPRequest.getTransactionId()));
    }

    @Test
    public void testVerifyOtpForMobileEmailLoginWithInvalidAuthMode() throws Exception
    {
        MobileEmailVerifyOTPRequest mobileEmailVerifyOTPRequest = new MobileEmailVerifyOTPRequest();
        mobileEmailVerifyOTPRequest.setUserId(12334L);

        MobileEmailVerifyOTPResponse mobileEmailVerifyOTPResponse = new MobileEmailVerifyOTPResponse();
        mobileEmailVerifyOTPResponse.setTransactionId("12334-abcd");
        mobileEmailVerifyOTPResponse.setMappedPhrAddress(new ArrayList<String>());
        mobileEmailVerifyOTPResponse.setMobileEmail("MOBILE");


        when(abhaService.verifyOtpForMobileEmailLogin(any(), anyString())).thenReturn(mobileEmailVerifyOTPResponse);
        String uri = "/v1/mobileEmail/login/verify-otp";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .content(Utils.asJsonString(mobileEmailVerifyOTPRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testVerifyOtpForMobileEmailLoginWithInvalidUserId() throws Exception
    {
        MobileEmailVerifyOTPRequest mobileEmailVerifyOTPRequest = new MobileEmailVerifyOTPRequest();
        mobileEmailVerifyOTPRequest.setAuthCode("MOBILE_OTP");

        MobileEmailVerifyOTPResponse mobileEmailVerifyOTPResponse = new MobileEmailVerifyOTPResponse();
        mobileEmailVerifyOTPResponse.setTransactionId("12334-abcd");
        mobileEmailVerifyOTPResponse.setMappedPhrAddress(new ArrayList<String>());
        mobileEmailVerifyOTPResponse.setMobileEmail("MOBILE");


        when(abhaService.verifyOtpForMobileEmailLogin(any(), anyString())).thenReturn(mobileEmailVerifyOTPResponse);
        String uri = "/v1/mobileEmail/login/verify-otp";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .content(Utils.asJsonString(mobileEmailVerifyOTPRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }



    @Test
    public void testUserAuthorizedTokenForMobileEmail() throws Exception
    {
        UserAuthorizedTokenForMobileEmailRequest userAuthorizedTokenForMobileEmailRequest = new UserAuthorizedTokenForMobileEmailRequest();
        userAuthorizedTokenForMobileEmailRequest.setPatientId("239484");
        userAuthorizedTokenForMobileEmailRequest.setTransactionId("12334-abcd");
        userAuthorizedTokenForMobileEmailRequest.setRequesterId("8402-432u-jkll-jd48");
        userAuthorizedTokenForMobileEmailRequest.setUserId(12334L);

        UserAuthorizedTokenResponse userAuthorizedTokenResponse = new UserAuthorizedTokenResponse();
        userAuthorizedTokenResponse.setToken("abcde");

        when(abhaService.userAuthorizedTokenForMobileEmail(any(),anyString())).thenReturn(userAuthorizedTokenResponse);
        String uri = "/v1/mobileEmail/login/userAuthorizedToken";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .header(ABHA_T_TOKEN, "eyJhbGciOiJSUzUxMiJ9")
                .content(Utils.asJsonString(userAuthorizedTokenForMobileEmailRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.token").value(userAuthorizedTokenResponse.getToken()));
    }

    @Test
    public void testUserAuthorizedTokenForMobileEmailWithInvalidUserId() throws Exception
    {
        UserAuthorizedTokenForMobileEmailRequest userAuthorizedTokenForMobileEmailRequest = new UserAuthorizedTokenForMobileEmailRequest();
        userAuthorizedTokenForMobileEmailRequest.setPatientId("239484");
        userAuthorizedTokenForMobileEmailRequest.setRequesterId("8402-432u-jkll-jd48");

        UserAuthorizedTokenResponse userAuthorizedTokenResponse = new UserAuthorizedTokenResponse();
        userAuthorizedTokenResponse.setToken("abcde");

        when(abhaService.userAuthorizedTokenForMobileEmail(any(),anyString())).thenReturn(userAuthorizedTokenResponse);
        String uri = "/v1/mobileEmail/login/userAuthorizedToken";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .header(ABHA_T_TOKEN, "eyJhbGciOiJSUzUxMiJ9")
                .content(Utils.asJsonString(userAuthorizedTokenForMobileEmailRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUserAuthorizedTokenForMobileEmailWithInvalidPatientId() throws Exception
    {
        UserAuthorizedTokenForMobileEmailRequest userAuthorizedTokenForMobileEmailRequest = new UserAuthorizedTokenForMobileEmailRequest();
        userAuthorizedTokenForMobileEmailRequest.setUserId(12334L);
        userAuthorizedTokenForMobileEmailRequest.setRequesterId("8402-432u-jkll-jd48");

        UserAuthorizedTokenResponse userAuthorizedTokenResponse = new UserAuthorizedTokenResponse();
        userAuthorizedTokenResponse.setToken("abcde");

        when(abhaService.userAuthorizedTokenForMobileEmail(any(),anyString())).thenReturn(userAuthorizedTokenResponse);
        String uri = "/v1/mobileEmail/login/userAuthorizedToken";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .header(ABHA_T_TOKEN, "eyJhbGciOiJSUzUxMiJ9")
                .content(Utils.asJsonString(userAuthorizedTokenForMobileEmailRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }


    @Test
    public void testShareProfile() throws Exception
    {
        ShareProfileRequest shareProfileRequest = new ShareProfileRequest();
        shareProfileRequest.setRequestId("8402-432u-jkll-jd48");
        shareProfileRequest.setSource("ANDROID");
        shareProfileRequest.setUserId(1234L);


        Intent intent = new Intent();
        intent.setType("REGISTRATION");

        HipDetails hipDetails = new HipDetails();
        hipDetails.setCode("123456");
        hipDetails.setHipId("NIKSHAY_HUI");

        shareProfileRequest.setHipDetails(hipDetails);
        shareProfileRequest.setIntent(intent);

        ShareProfileResponse shareProfileResponse = new ShareProfileResponse();
        shareProfileResponse.setTokenNumber("31");


        when(abhaService.shareProfile(any(),anyString())).thenReturn(shareProfileResponse);
        String uri = "/v1/share/profile";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .header(ABHA_X_AUTH_TOKEN, "eyJhbGciOiJSUzUxMiJ9")
                .content(Utils.asJsonString(shareProfileRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.tokenNumber").value(shareProfileResponse.getTokenNumber()));
    }

    @Test
    public void testShareProfileForInvalidUserId() throws Exception
    {
        ShareProfileRequest shareProfileRequest = new ShareProfileRequest();
        shareProfileRequest.setSource("ANDROID");
        shareProfileRequest.setRequestId("8402-432u-jkll-jd48");


        Intent intent = new Intent();
        intent.setType("REGISTRATION");

        HipDetails hipDetails = new HipDetails();
        hipDetails.setCode("123456");
        hipDetails.setHipId("NIKSHAY_HUI");

        shareProfileRequest.setHipDetails(hipDetails);
        shareProfileRequest.setIntent(intent);

        ShareProfileResponse shareProfileResponse = new ShareProfileResponse();
        shareProfileResponse.setTokenNumber("31");


        when(abhaService.shareProfile(any(),anyString())).thenReturn(shareProfileResponse);
        String uri = "/v1/share/profile";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .header(ABHA_X_AUTH_TOKEN, "eyJhbGciOiJSUzUxMiJ9")
                .content(Utils.asJsonString(shareProfileRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testShareProfileForInvalidRequestId() throws Exception
    {
        ShareProfileRequest shareProfileRequest = new ShareProfileRequest();
        shareProfileRequest.setSource("ANDROID");
        shareProfileRequest.setUserId(1234L);

        Intent intent = new Intent();
        intent.setType("REGISTRATION");

        HipDetails hipDetails = new HipDetails();
        hipDetails.setCode("123456");
        hipDetails.setHipId("NIKSHAY_HUI");

        shareProfileRequest.setHipDetails(hipDetails);
        shareProfileRequest.setIntent(intent);

        ShareProfileResponse shareProfileResponse = new ShareProfileResponse();
        shareProfileResponse.setTokenNumber("31");


        when(abhaService.shareProfile(any(),anyString())).thenReturn(shareProfileResponse);
        String uri = "/v1/share/profile";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .header(ABHA_X_AUTH_TOKEN, "eyJhbGciOiJSUzUxMiJ9")
                .content(Utils.asJsonString(shareProfileRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testShareProfileForInvalidIntent() throws Exception
    {
        ShareProfileRequest shareProfileRequest = new ShareProfileRequest();
        shareProfileRequest.setSource("ANDROID");
        shareProfileRequest.setRequestId("8402-432u-jkll-jd48");
        shareProfileRequest.setUserId(1234L);

        HipDetails hipDetails = new HipDetails();
        hipDetails.setCode("123456");
        hipDetails.setHipId("NIKSHAY_HUI");

        shareProfileRequest.setHipDetails(hipDetails);

        ShareProfileResponse shareProfileResponse = new ShareProfileResponse();
        shareProfileResponse.setTokenNumber("31");


        when(abhaService.shareProfile(any(),anyString())).thenReturn(shareProfileResponse);
        String uri = "/v1/share/profile";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .header(ABHA_X_AUTH_TOKEN, "eyJhbGciOiJSUzUxMiJ9")
                .content(Utils.asJsonString(shareProfileRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }


    @Test
    public void testShareProfileForInvalidSource() throws Exception
    {
        ShareProfileRequest shareProfileRequest = new ShareProfileRequest();
        shareProfileRequest.setRequestId("8402-432u-jkll-jd48");
        shareProfileRequest.setUserId(1234L);

        HipDetails hipDetails = new HipDetails();
        hipDetails.setCode("123456");
        hipDetails.setHipId("NIKSHAY_HUI");

        shareProfileRequest.setHipDetails(hipDetails);

        ShareProfileResponse shareProfileResponse = new ShareProfileResponse();
        shareProfileResponse.setTokenNumber("31");


        when(abhaService.shareProfile(any(),anyString())).thenReturn(shareProfileResponse);
        String uri = "/v1/share/profile";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .header(ABHA_X_AUTH_TOKEN, "eyJhbGciOiJSUzUxMiJ9")
                .content(Utils.asJsonString(shareProfileRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testShareProfileWithInvalidHipDetails() throws Exception
    {
        ShareProfileRequest shareProfileRequest = new ShareProfileRequest();
        shareProfileRequest.setRequestId("8402-432u-jkll-jd48");
        shareProfileRequest.setSource("ANDROID");
        shareProfileRequest.setUserId(1234L);

        Intent intent = new Intent();
        intent.setType("REGISTRATION");
        shareProfileRequest.setIntent(intent);

        ShareProfileResponse shareProfileResponse = new ShareProfileResponse();
        shareProfileResponse.setTokenNumber("31");


        when(abhaService.shareProfile(any(),anyString())).thenReturn(shareProfileResponse);
        String uri = "/v1/share/profile";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .header(ABHA_X_AUTH_TOKEN, "eyJhbGciOiJSUzUxMiJ9")
                .content(Utils.asJsonString(shareProfileRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }


    @Test
    public void testGetProvider() throws Exception
    {
        ABHAProviderResponse abhaProviderResponse = new ABHAProviderResponse();
        ABHAProviderIdentifier abhaProviderIdentifier = new ABHAProviderIdentifier();
        abhaProviderIdentifier.setId("NIKSHAY_HUI");
        abhaProviderIdentifier.setName("NIKSHAY");
        abhaProviderResponse.setIdentifier(abhaProviderIdentifier);



        when(abhaService.getProviderDetails(anyString(), anyLong())).thenReturn(abhaProviderResponse);
        String uri = "/v1/abha/provider/NIKSHAY_HUI?userId=1234";
        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                .pathInfo("/NIKSHAY_HUI")
                .header(ABHA_X_AUTH_TOKEN, "eyJhbGciOiJSUzUxMiJ9")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.identifier.name").value(abhaProviderResponse.getIdentifier().getName()));
    }

    @Test
    public void testGetAbhaAccountProfile() throws Exception {
        ABHAProfileResponse abhaProfileResponse = new ABHAProfileResponse();
        abhaProfileResponse.setAddress("4/13 temporary address");
        abhaProfileResponse.setPincode("350006");
        when(abhaService.getAbhaProfile(anyLong())).thenReturn(abhaProfileResponse);
        String uri = "/v1/abha/profile?userId=21344";
        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.pincode").value(abhaProfileResponse.getPincode()));
    }
}

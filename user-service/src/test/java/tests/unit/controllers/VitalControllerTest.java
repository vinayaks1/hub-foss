package tests.unit.controllers;

import com.everwell.userservice.controllers.VitalController;
import com.everwell.userservice.exceptions.CustomExceptionHandler;
import com.everwell.userservice.models.db.Vital;
import com.everwell.userservice.models.http.requests.VitalBulkRequest;
import com.everwell.userservice.models.http.requests.VitalRequest;
import com.everwell.userservice.models.http.responses.VitalResponse;
import com.everwell.userservice.services.VitalService;
import com.everwell.userservice.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import tests.BaseTestNoSpring;

import java.util.ArrayList;
import java.util.List;

import static com.everwell.userservice.constants.Constants.X_US_CLIENT_ID;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class VitalControllerTest extends BaseTestNoSpring {
    @InjectMocks
    private VitalController vitalController;

    @Mock
    private VitalService vitalService;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(vitalController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void testGetAllVitals() throws Exception
    {
        int clientId = 1;
        List<VitalResponse> vitalResponseList = new ArrayList<>();
        when(vitalService.getAllVitalsResponse()).thenReturn(vitalResponseList);
        String uri = "/v1/vitals";
        mockMvc.perform(
                        MockMvcRequestBuilders.get(uri)
                                .header(X_US_CLIENT_ID,Long.toString(clientId))
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value(vitalResponseList));
    }

    @Test
    public void testGetVitalsById() throws Exception
    {
        int clientId = 1;
        VitalResponse vitalResponse = new VitalResponse(1L, "vital name", 10f, 10000f, 1f, 10000f, "uomTest", false);
        when(vitalService.getVitalResponseById(any())).thenReturn(vitalResponse);
        String uri = "/v1/vitals/1";
        mockMvc.perform(
                        MockMvcRequestBuilders.get(uri)
                                .header(X_US_CLIENT_ID,Long.toString(clientId))
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.id").value(vitalResponse.getId()));
    }

    @Test
    public void testSaveVitals() throws Exception
    {
        int clientId = 1;
        List<VitalRequest> vitalRequestList = new ArrayList<>();
        vitalRequestList.add(new VitalRequest("vital name", 10f, 10000f, 1f, 10000f, "uomTest", false));
        VitalBulkRequest vitalBulkRequest = new VitalBulkRequest(vitalRequestList);

        List<Vital> vitalResponse = new ArrayList<>();
        vitalResponse.add(new Vital(1L, "vital name", 10f, 10000f, 1f, 10000f, "uomTest", false));

        when(vitalService.saveVitals(any())).thenReturn(vitalResponse);
        String uri = "/v1/vitals";
        mockMvc.perform(
                        MockMvcRequestBuilders.post(uri)
                                .header(X_US_CLIENT_ID,Long.toString(clientId))
                                .content(Utils.asJsonString(vitalBulkRequest))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].name").value(vitalResponse.get(0).getName()));
    }

    @Test
    public void testSaveVitalsWithEmptyVitalRequest() throws Exception
    {
        int clientId = 1;
        VitalBulkRequest vitalBulkRequest = new VitalBulkRequest(new ArrayList<>());
        String uri = "/v1/vitals";
        mockMvc.perform(
                        MockMvcRequestBuilders.post(uri)
                                .header(X_US_CLIENT_ID,Long.toString(clientId))
                                .content(Utils.asJsonString(vitalBulkRequest))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateVitalsById() throws Exception
    {
        int clientId = 1;
        VitalRequest vitalRequest = new VitalRequest("vital name", 10f, 10000f, 1f, 10000f, "uomTest", false);
        VitalResponse vitalResponse = new VitalResponse(1L, "vital name", 10f, 10000f, 1f, 10000f, "uomTest", false);
        when(vitalService.updateVitalsById(any(), any())).thenReturn(vitalResponse);
        String uri = "/v1/vitals/1";
        mockMvc.perform(
                        MockMvcRequestBuilders.put(uri)
                                .header(X_US_CLIENT_ID,Long.toString(clientId))
                                .content(Utils.asJsonString(vitalRequest))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.name").value(vitalResponse.getName()));
    }

    @Test
    public void testUpdateVitalsByIdWithInvalidVitalName() throws Exception {
        int clientId = 1;
        VitalRequest vitalRequest = new VitalRequest(null, 10f, 10000f, 1f, 10000f, "uomTest", false);
        String uri = "/v1/vitals/1";
        mockMvc.perform(
                        MockMvcRequestBuilders.put(uri)
                                .header(X_US_CLIENT_ID, Long.toString(clientId))
                                .content(Utils.asJsonString(vitalRequest))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testDeleteVitalsById() throws Exception
    {
        int clientId = 1;
        doNothing().when(vitalService).deleteVitalsById(any());
        String uri = "/v1/vitals/1";
        mockMvc.perform(
                        MockMvcRequestBuilders.delete(uri)
                                .header(X_US_CLIENT_ID,Long.toString(clientId))
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value("Vital deleted successfully"));
    }

}

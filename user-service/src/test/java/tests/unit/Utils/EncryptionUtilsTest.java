package tests.unit.Utils;

import com.everwell.userservice.utils.EncryptionUtils;
import org.junit.Test;
import tests.BaseTest;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class EncryptionUtilsTest extends BaseTest {

    private String algo = "AES/CBC/PKCS5Padding";
    private String key = "testkey";

    @Test
    public void testEncryptionDecryption() {
        String text = "Test";
        String cipherText = EncryptionUtils.encrypt(text);
        String plainText = EncryptionUtils.decrypt(cipherText);
        assertEquals(plainText, text);
    }

    @Test(expected = RuntimeException.class)
    public void testEncryptionNoSuchAlgorithmException() {
        EncryptionUtils.encrypt("Test", "", "");
    }

    @Test(expected = RuntimeException.class)
    public void testEncryptionInvalidKeyException() {
        EncryptionUtils.encrypt("Test", algo, "");
    }

    @Test(expected = RuntimeException.class)
    public void testEncryptionIllegalBlockSizeException() {
        EncryptionUtils.encrypt(null, algo, key);
    }

    @Test(expected = RuntimeException.class)
    public void testDecryptionNoSuchAlgorithmException() {
        EncryptionUtils.encrypt("Test", "", "");
    }

    @Test(expected = RuntimeException.class)
    public void testDecryptionInvalidKeyException() {
        EncryptionUtils.encrypt("Test", algo, "");
    }

    @Test(expected = RuntimeException.class)
    public void testDecryptionIllegalBlockSizeException() {
        EncryptionUtils.encrypt(null, algo, key);
    }

}

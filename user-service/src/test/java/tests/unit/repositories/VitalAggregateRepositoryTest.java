package tests.unit.repositories;

import com.everwell.userservice.enums.TimescaleQueryEnum;
import com.everwell.userservice.enums.TimescaleQueryFieldEnum;
import com.everwell.userservice.mapper.VitalsAggregatedTimeDataRowMapper;
import com.everwell.userservice.models.dtos.vitals.VitalsAggregatedTimeData;
import com.everwell.userservice.repositories.impl.VitalAggregateRepositoryImpl;
import org.junit.Assert;
import org.mockito.*;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

public class VitalAggregateRepositoryTest {

    @Spy
    @InjectMocks
    private VitalAggregateRepositoryImpl vitalAggregateRepository;

    @Mock
    private NamedParameterJdbcTemplate timescaleNamedParameterJdbcTemplate;

    @Spy
    private VitalsAggregatedTimeDataRowMapper vitalsAggregatedTimeDataRowMapper;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void fetchDataTest() {
        List<VitalsAggregatedTimeData> vitalsAggregatedTimeDataList = new ArrayList<>();
        vitalsAggregatedTimeDataList.add(new VitalsAggregatedTimeData("2022-11-29 00:00:00", 1L, 1L, 100f, 10f));
        List<Long> userIdList = Collections.singletonList(1L);
        List<Long> vitalIdList = Collections.singletonList(1L);
        Timestamp startTimestamp = Timestamp.valueOf("2022-11-29 00:00:00");
        Timestamp endTimestamp =  Timestamp.valueOf("2022-11-29 00:00:00");
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue(TimescaleQueryFieldEnum.START_TIMESTAMP.getFieldName(), startTimestamp)
                .addValue(TimescaleQueryFieldEnum.END_TIMESTAMP.getFieldName(), endTimestamp)
                .addValue(TimescaleQueryFieldEnum.USER_ID_LIST.getFieldName(), userIdList)
                .addValue(TimescaleQueryFieldEnum.VITAL_ID_LIST.getFieldName(), vitalIdList);

        doReturn(sqlParameterSource).when(vitalAggregateRepository).constructSqlParameterSource(any(), any(), any(), any());
        when(timescaleNamedParameterJdbcTemplate.query(TimescaleQueryEnum.GET_VITALS_AGGREGATED_DATA.getQuery(), sqlParameterSource, vitalsAggregatedTimeDataRowMapper)).thenReturn(vitalsAggregatedTimeDataList);

        List<VitalsAggregatedTimeData> vitalsAggregatedTimeDataResult = vitalAggregateRepository.fetchData(Collections.singletonList(1L), Collections.singletonList(1L), startTimestamp, endTimestamp);

        Assert.assertEquals(vitalsAggregatedTimeDataList.get(0).getUserId(), vitalsAggregatedTimeDataResult.get(0).getUserId());
        Assert.assertEquals(vitalsAggregatedTimeDataList.get(0).getTotal(), vitalsAggregatedTimeDataResult.get(0).getTotal());
        Assert.assertEquals(vitalsAggregatedTimeDataList.get(0).getAverage(), vitalsAggregatedTimeDataResult.get(0).getAverage());
        Assert.assertEquals(vitalsAggregatedTimeDataList.get(0).getVitalId(), vitalsAggregatedTimeDataResult.get(0).getVitalId());
        Assert.assertEquals(vitalsAggregatedTimeDataList.size(), vitalsAggregatedTimeDataResult.size());
        Mockito.verify(timescaleNamedParameterJdbcTemplate, Mockito.times(1)).query(TimescaleQueryEnum.GET_VITALS_AGGREGATED_DATA.getQuery(), sqlParameterSource, vitalsAggregatedTimeDataRowMapper);
    }
}

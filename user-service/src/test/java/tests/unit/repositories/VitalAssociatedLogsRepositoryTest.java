package tests.unit.repositories;

import com.everwell.userservice.enums.TimescaleQueryEnum;
import com.everwell.userservice.enums.TimescaleQueryFieldEnum;
import com.everwell.userservice.mapper.VitalsTimeDataRowMapper;
import com.everwell.userservice.models.db.timescale.VitalAssociatedLogs;
import com.everwell.userservice.models.dtos.vitals.VitalsTimeData;
import com.everwell.userservice.repositories.impl.VitalAssociatedLogsRepositoryImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

public class VitalAssociatedLogsRepositoryTest {

    @Spy
    @InjectMocks
    private VitalAssociatedLogsRepositoryImpl vitalAssociatedLogsRepository;

    @Mock
    private NamedParameterJdbcTemplate timescaleNamedParameterJdbcTemplate;

    @Spy
    private VitalsTimeDataRowMapper vitalsTimeDataRowMapper;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void saveAllTest() {
        SqlParameterSource[] batch = new SqlParameterSource[0];
        int [] rows = new int[0];
        when(timescaleNamedParameterJdbcTemplate.batchUpdate(TimescaleQueryEnum.INSERT_DATA_INTO_VITAL_ASSOCIATED_LOGS.getQuery(), batch)).thenReturn(rows);
        List<VitalAssociatedLogs> vitalAssociatedLogsList = new ArrayList<>();

        vitalAssociatedLogsRepository.saveAll(vitalAssociatedLogsList);

        Mockito.verify(timescaleNamedParameterJdbcTemplate, Mockito.times(1)).batchUpdate(TimescaleQueryEnum.INSERT_DATA_INTO_VITAL_ASSOCIATED_LOGS.getQuery(), batch);
    }

    @Test
    public void fetchLogsTest() {
        List<VitalsTimeData> vitalsTimeDataList = new ArrayList<>();
        vitalsTimeDataList.add(new VitalsTimeData("2022-11-29 00:00:00", 1L, 1L, "100"));
        List<Long> userIdList = Collections.singletonList(1L);
        List<Long> vitalIdList = Collections.singletonList(1L);
        Timestamp startTimestamp = Timestamp.valueOf("2022-11-29 00:00:00");
        Timestamp endTimestamp =  Timestamp.valueOf("2022-11-29 00:00:00");
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue(TimescaleQueryFieldEnum.START_TIMESTAMP.getFieldName(), startTimestamp)
                .addValue(TimescaleQueryFieldEnum.END_TIMESTAMP.getFieldName(), endTimestamp)
                .addValue(TimescaleQueryFieldEnum.USER_ID_LIST.getFieldName(), userIdList)
                .addValue(TimescaleQueryFieldEnum.VITAL_ID_LIST.getFieldName(), vitalIdList);

        doReturn(sqlParameterSource).when(vitalAssociatedLogsRepository).constructSqlParameterSource(any(), any(), any(), any());
        when(timescaleNamedParameterJdbcTemplate.query(TimescaleQueryEnum.GET_VITALS_ASSOCIATED_LOGS.getQuery(), sqlParameterSource, vitalsTimeDataRowMapper)).thenReturn(vitalsTimeDataList);

        List<VitalsTimeData> vitalsTimeDataListResult = vitalAssociatedLogsRepository.fetchLogs(Collections.singletonList(1L), Collections.singletonList(1L), startTimestamp, endTimestamp);

        Assert.assertEquals(vitalsTimeDataList.get(0).getUserId(), vitalsTimeDataListResult.get(0).getUserId());
        Assert.assertEquals(vitalsTimeDataList.get(0).getValue(), vitalsTimeDataListResult.get(0).getValue());
        Assert.assertEquals(vitalsTimeDataList.get(0).getVitalId(), vitalsTimeDataListResult.get(0).getVitalId());
        Assert.assertEquals(vitalsTimeDataList.size(), vitalsTimeDataListResult.size());
        Mockito.verify(timescaleNamedParameterJdbcTemplate, Mockito.times(1)).query(TimescaleQueryEnum.GET_VITALS_ASSOCIATED_LOGS.getQuery(), sqlParameterSource , vitalsTimeDataRowMapper);
    }
}

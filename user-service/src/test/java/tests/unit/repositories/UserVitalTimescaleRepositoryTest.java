package tests.unit.repositories;

import com.everwell.userservice.enums.TimescaleQueryFieldEnum;
import com.everwell.userservice.repositories.impl.UserVitalTimescaleRepositoryImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import tests.BaseTest;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class UserVitalTimescaleRepositoryTest extends BaseTest {

    @Spy
    @InjectMocks
    private UserVitalTimescaleRepositoryImpl userVitalTimescaleRepository;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void constructSqlParameterSourceTest() {
        List<Long> userIdList = Collections.singletonList(1L);
        List<Long> vitalIdList = Collections.singletonList(1L);
        Timestamp startTimestamp = Timestamp.valueOf("2022-11-29 00:00:00");
        Timestamp endTimestamp =  Timestamp.valueOf("2022-11-29 00:00:00");
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue(TimescaleQueryFieldEnum.START_TIMESTAMP.getFieldName(), startTimestamp)
                .addValue(TimescaleQueryFieldEnum.END_TIMESTAMP.getFieldName(), endTimestamp)
                .addValue(TimescaleQueryFieldEnum.USER_ID_LIST.getFieldName(), userIdList)
                .addValue(TimescaleQueryFieldEnum.VITAL_ID_LIST.getFieldName(), vitalIdList);

        SqlParameterSource sqlParameterSourceResult = userVitalTimescaleRepository.constructSqlParameterSource(userIdList, vitalIdList, startTimestamp, endTimestamp);

        Assert.assertEquals(sqlParameterSource.getValue(TimescaleQueryFieldEnum.USER_ID_LIST.getFieldName()), sqlParameterSourceResult.getValue(TimescaleQueryFieldEnum.USER_ID_LIST.getFieldName()));
        Assert.assertEquals(Objects.requireNonNull(sqlParameterSource.getParameterNames()).length, Objects.requireNonNull(sqlParameterSourceResult.getParameterNames()).length);
    }
}

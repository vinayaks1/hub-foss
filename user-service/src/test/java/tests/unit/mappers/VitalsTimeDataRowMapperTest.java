package tests.unit.mappers;

import com.everwell.userservice.mapper.VitalsTimeDataRowMapper;
import com.everwell.userservice.models.dtos.vitals.VitalsTimeData;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import tests.BaseTest;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

public class VitalsTimeDataRowMapperTest extends BaseTest {

    @Spy
    @InjectMocks
    private VitalsTimeDataRowMapper vitalsTimeDataRowMapper;

    @Mock
    ResultSet resultSet;

    @Test
    public void mapRowTest() throws SQLException {
        Mockito.when(resultSet.getTimestamp(1)).thenReturn(Timestamp.valueOf("2022-11-29 00:00:00"));
        Mockito.when(resultSet.getLong(2)).thenReturn(1L);
        Mockito.when(resultSet.getLong(3)).thenReturn(2L);
        Mockito.when(resultSet.getObject(4)).thenReturn("4");

        VitalsTimeData vitalsTimeData = vitalsTimeDataRowMapper.mapRow(resultSet, 0);

        Assert.assertEquals(1L, vitalsTimeData.getUserId().longValue());
        Assert.assertEquals(2L, vitalsTimeData.getVitalId().longValue());
        Assert.assertEquals("4", vitalsTimeData.getValue());
    }
}

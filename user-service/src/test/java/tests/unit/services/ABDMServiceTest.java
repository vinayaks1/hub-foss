package tests.unit.services;


import com.everwell.userservice.exceptions.NotFoundException;
import com.everwell.userservice.models.db.ABDMConsent;
import com.everwell.userservice.models.dtos.*;
import com.everwell.userservice.models.dtos.abdm.*;
import com.everwell.userservice.repositories.*;
import com.everwell.userservice.services.RabbitMQPublisherService;
import com.everwell.userservice.services.impl.ABDMServiceImpl;
import com.everwell.userservice.utils.Utils;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import tests.BaseTest;

import java.io.IOException;
import java.time.Instant;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import java.util.Date;
import java.util.Optional;


public class ABDMServiceTest extends BaseTest
{

    @InjectMocks
    private ABDMServiceImpl abdmServiceImpl;

    @Mock
    private ABDMConsentRepository abdmConsentRepository;

    @Mock
    private RabbitMQPublisherService rabbitMQPublisherService;

    @Mock
    private UserMedicalRecordsRepository userMedicalRecordsRepository;

    @Test
    public void addUpdateConsent_Add()
    {
        ABDMAddUpdateConsentRequest abdmAddUpdateConsentRequest = new ABDMAddUpdateConsentRequest();
        ABDMAddUpdateConsentRequest.ABDMConsentNotification abdmConsentNotification = new ABDMAddUpdateConsentRequest.ABDMConsentNotification();
        abdmConsentNotification.setConsentId("Test");
        abdmAddUpdateConsentRequest.setNotification(abdmConsentNotification);
        when(abdmConsentRepository.findConsentByConsentId(any())).thenReturn(null);

        doNothing().when(rabbitMQPublisherService).send(any(), anyString(), anyString());

        abdmServiceImpl.addUpdateConsent(new ReferenceIdRMQRequest(), abdmAddUpdateConsentRequest, any());
    }

    @Test
    public void addUpdateConsent_Update()
    {
        ABDMAddUpdateConsentRequest abdmAddUpdateConsentRequest = new ABDMAddUpdateConsentRequest();
        ABDMAddUpdateConsentRequest.ABDMConsentNotification abdmConsentNotification = new ABDMAddUpdateConsentRequest.ABDMConsentNotification();
        abdmConsentNotification.setConsentId("Test");
        abdmAddUpdateConsentRequest.setNotification(abdmConsentNotification);
        when(abdmConsentRepository.findConsentByConsentId(any())).thenReturn(new ABDMConsent());

        doNothing().when(rabbitMQPublisherService).send(any(), anyString(), anyString());

        abdmServiceImpl.addUpdateConsent(new ReferenceIdRMQRequest(), abdmAddUpdateConsentRequest, any());
    }


    @Test
    public void  getByConsentIdTest_consentGranted() throws IOException {
        String consentId = "fe8dfb75-467f-4314-824d-366814a8e469";
        ABDMConsent abdmConsent = new ABDMConsent(1L,consentId,"{\"notification\":{\"consentDetail\":{\"consentId\":\"fe8dfb75-467f-4314-824d-366814a8e469\",\"createdAt\":\"2022-07-29T10:01:06.669651\",\"purpose\":{\"text\":\"Self Requested\",\"code\":\"PATRQT\",\"refUri\":null},\"patient\":{\"id\":\"ankurtest3@sbx\"},\"consentManager\":{\"id\":\"sbx\"},\"hip\":{\"id\":\"NIKSHAY-HIP\",\"name\":null},\"hiTypes\":[\"DiagnosticReport\",\"Prescription\",\"ImmunizationRecord\",\"DischargeSummary\",\"OPConsultation\",\"HealthDocumentRecord\",\"WellnessRecord\"],\"permission\":{\"accessMode\":\"VIEW\",\"dateRange\":{\"from\":\"2015-07-29T10:01:06.639518\",\"to\":\"2022-07-29T10:01:06.639528\"},\"dataEraseAt\":\"2022-09-29T10:01:06.483299\",\"frequency\":{\"unit\":\"HOUR\",\"value\":1,\"repeats\":0}},\"careContexts\":[{\"patientReference\":\"41777\",\"careContextReference\":\"10750687\"}]},\"status\":\"GRANTED\",\"signature\":\"IE0Uky4ebPv7KG9m8XBNDgG00vQj4OU9mEaotXcssfB74UsWxAM8b50gQXFoLpdYTLn9cKfvVGVrcDMQzH821W35hwMpRwt3DQAgqWqU5+mVWdi2GJLn9nJMN4uQRDdNpWh+3UCRXMmCFLPRzzvmjtHaEM/cdB6gmZdAMaO+qhRfCu24eDcaukjxMD343GiVkzsWCFzDnS0SW8Iw4P4jfc/n6yio8qavsaD7l7TQBCDNN2c4MlS3FcZAbukpliPjWHsKKVXCy6PT83lO55Xhz79yrZH2jGSySpfEWRduN+bJ1VOFT8gxdnqx2jvXsNImtCR1Pcm3PS4DdvfcUCsUpQ==\",\"consentId\":\"fe8dfb75-467f-4314-824d-366814a8e469\",\"grantAcknowledgement\":false},\"requestId\":\"1c2cdc28-d606-4cdb-b257-34904b07794f\",\"timestamp\":\"2022-07-29T10:01:06.722336\"}",Date.from(Instant.now()),Date.from(Instant.now()),1L,"HIP");
        when(abdmConsentRepository.findConsentByConsentId(any())).thenReturn(abdmConsent);
        ABDMConsentResponse abdmConsentResponse = abdmServiceImpl.getByConsentId(consentId);
        Assert.assertEquals(abdmConsentResponse.status,"GRANTED");
        Assert.assertEquals(abdmConsentResponse.startDate,"2015-07-29T10:01:06.639518");
    }

    @Test
    public void  getByConsentIdTest_consentRevoked() throws IOException {
        String consentId = "fe8dfb75-467f-4314-824d-366814a8e469";
        ABDMConsent abdmConsent = new ABDMConsent(1L,consentId,"{\"notification\":{\"consentDetail\":{\"consentId\":\"fe8dfb75-467f-4314-824d-366814a8e469\",\"createdAt\":\"2022-07-29T10:01:06.669651\",\"purpose\":{\"text\":\"Self Requested\",\"code\":\"PATRQT\",\"refUri\":null},\"patient\":{\"id\":\"ankurtest3@sbx\"},\"consentManager\":{\"id\":\"sbx\"},\"hip\":{\"id\":\"NIKSHAY-HIP\",\"name\":null},\"hiTypes\":[\"DiagnosticReport\",\"Prescription\",\"ImmunizationRecord\",\"DischargeSummary\",\"OPConsultation\",\"HealthDocumentRecord\",\"WellnessRecord\"],\"permission\":{\"accessMode\":\"VIEW\",\"dateRange\":{\"from\":\"2015-07-29T10:01:06.639518\",\"to\":\"2022-07-29T10:01:06.639528\"},\"dataEraseAt\":\"2022-09-29T10:01:06.483299\",\"frequency\":{\"unit\":\"HOUR\",\"value\":1,\"repeats\":0}},\"careContexts\":[{\"patientReference\":\"41777\",\"careContextReference\":\"10750687\"}]},\"status\":\"REVOKED\",\"signature\":\"IE0Uky4ebPv7KG9m8XBNDgG00vQj4OU9mEaotXcssfB74UsWxAM8b50gQXFoLpdYTLn9cKfvVGVrcDMQzH821W35hwMpRwt3DQAgqWqU5+mVWdi2GJLn9nJMN4uQRDdNpWh+3UCRXMmCFLPRzzvmjtHaEM/cdB6gmZdAMaO+qhRfCu24eDcaukjxMD343GiVkzsWCFzDnS0SW8Iw4P4jfc/n6yio8qavsaD7l7TQBCDNN2c4MlS3FcZAbukpliPjWHsKKVXCy6PT83lO55Xhz79yrZH2jGSySpfEWRduN+bJ1VOFT8gxdnqx2jvXsNImtCR1Pcm3PS4DdvfcUCsUpQ==\",\"consentId\":\"fe8dfb75-467f-4314-824d-366814a8e469\",\"grantAcknowledgement\":false},\"requestId\":\"1c2cdc28-d606-4cdb-b257-34904b07794f\",\"timestamp\":\"2022-07-29T10:01:06.722336\"}",Date.from(Instant.now()),Date.from(Instant.now()),1L,"HIP");
        when(abdmConsentRepository.findConsentByConsentId(any())).thenReturn(abdmConsent);
        ABDMConsentResponse abdmConsentResponse = abdmServiceImpl.getByConsentId(consentId);
        Assert.assertEquals(abdmConsentResponse.status,"REVOKED");
    }

    @Test
    public void  getByConsentIdTest_Exception() throws IOException {
        String consentId = "fe8dfb75-467f-4314-824d-366814a8e469";
        ABDMConsent abdmConsent = new ABDMConsent(1L,consentId,"{\"notification\":{\"consentDetail\":{\"consentId\":\"fe8dfb75-467f-4314-824d-366814a8e469\",\"createdAt\":\"2022-07-29T10:01:06.669651\",\"purpose\":{\"text\":\"Self Requested\",\"code\":\"PATRQT\",\"refUri\":null},\"patient\":{\"id\":\"ankurtest3@sbx\"},\"consentManager\":{\"id\":\"sbx\"},\"hip\":{\"id\":\"NIKSHAY-HIP\",\"name\":null},\"hiTypes\":[\"DiagnosticReport\",\"Prescription\",\"ImmunizationRecord\",\"DischargeSummary\",\"OPConsultation\",\"HealthDocumentRecord\",\"WellnessRecord\"],\"permission\":{\"accessMode\":\"VIEW\",\"dateRange\":{\"from\":\"2015-07-29T10:01:06.639518\",\"to\":\"2022-07-29T10:01:06.639528\"},\"dataEraseAt\":\"2022-09-29T10:01:06.483299\",\"frequency\":{\"unit\":\"HOUR\",\"value\":1,\"repeats\":0}},\"careContexts\":[{\"patientReference\":\"41777\",\"careContextReference\":\"10750687\"}]},\"status\":\"REVOKED\",\"signature\":\"IE0Uky4ebPv7KG9m8XBNDgG00vQj4OU9mEaotXcssfB74UsWxAM8b50gQXFoLpdYTLn9cKfvVGVrcDMQzH821W35hwMpRwt3DQAgqWqU5+mVWdi2GJLn9nJMN4uQRDdNpWh+3UCRXMmCFLPRzzvmjtHaEM/cdB6gmZdAMaO+qhRfCu24eDcaukjxMD343GiVkzsWCFzDnS0SW8Iw4P4jfc/n6yio8qavsaD7l7TQBCDNN2c4MlS3FcZAbukpliPjWHsKKVXCy6PT83lO55Xhz79yrZH2jGSySpfEWRduN+bJ1VOFT8gxdnqx2jvXsNImtCR1Pcm3PS4DdvfcUCsUpQ==\",\"consentId\":\"fe8dfb75-467f-4314-824d-366814a8e469\",\"grantAcknowledgement\":false},\"requestId\":\"1c2cdc28-d606-4cdb-b257-34904b07794f\",\"timestamp\":2022-07-29T10:01:06.722336}",Date.from(Instant.now()),Date.from(Instant.now()),1L,"HIP");
        when(abdmConsentRepository.findConsentByConsentId(any())).thenReturn(abdmConsent);
        ABDMConsentResponse abdmConsentResponse = abdmServiceImpl.getByConsentId(consentId);
        Assert.assertEquals(abdmConsentResponse.status,"EXPIRED");
    }

    @Test
    public void  addUpdateConsentTest_Exception() throws IOException {
        ABDMAddUpdateConsentRequest abdmAddUpdateConsentRequest = new ABDMAddUpdateConsentRequest();
        ABDMAddUpdateConsentRequest.ABDMConsentNotification abdmConsentNotification = new ABDMAddUpdateConsentRequest.ABDMConsentNotification();
        abdmConsentNotification.setConsentId("Test");
        abdmAddUpdateConsentRequest.setNotification(abdmConsentNotification);
        when(abdmConsentRepository.findConsentByConsentId(any())).thenReturn(null);

        doNothing().when(rabbitMQPublisherService).send(any(), anyString(), anyString());
        when(abdmConsentRepository.save(any())).thenThrow(RuntimeException.class);
        abdmServiceImpl.addUpdateConsent(new ReferenceIdRMQRequest(), abdmAddUpdateConsentRequest, any());
    }

    @Test
    public void updateHIUConsentDetailsTest() {
        UpdateConsentDto updateConsentDto = new UpdateConsentDto("1","11c7ecb8-1dab-4a6b-bbe6-7a74f5851a46","1",null);
        when(abdmConsentRepository.findById(1L)).thenReturn(Optional.of(new ABDMConsent(1L, "HIU")));
        abdmServiceImpl.updateHIUConsentDetails(updateConsentDto);
        verify(abdmConsentRepository, Mockito.times(1)).save(any());
    }

    @Test(expected = NotFoundException.class)
    public void updateHIUConsentDetailsNotFound() {
        UpdateConsentDto updateConsentDto = new UpdateConsentDto("1","11c7ecb8-1dab-4a6b-bbe6-7a74f5851a46","1",new ConsentNotificationDto());
        when(abdmConsentRepository.findById(1L)).thenReturn(Optional.empty());
        abdmServiceImpl.updateHIUConsentDetails(updateConsentDto);
    }

    @Test
    public void updateHIUConsentDetails_NullConsentRequestId() {
        UpdateConsentDto updateConsentDto = new UpdateConsentDto("1",null,"1",null);
        when(abdmConsentRepository.findById(1L)).thenReturn(Optional.of(new ABDMConsent(1L, "HIU")));
        abdmServiceImpl.updateHIUConsentDetails(updateConsentDto);
        verify(abdmConsentRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void updateHIUConsentDetails_NullConsentNotificationDto() {
        UpdateConsentDto updateConsentDto = new UpdateConsentDto("1","11c7ecb8-1dab-4a6b-bbe6-7a74f5851a46","1",null);
        when(abdmConsentRepository.findById(1L)).thenReturn(Optional.of(new ABDMConsent(1L, "HIU")));
        abdmServiceImpl.updateHIUConsentDetails(updateConsentDto);
        verify(abdmConsentRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void addHIUConsentDetailsTest() {
        AddConsentRequest addConsentRequest = new AddConsentRequest(1L,null,null);
        ABDMConsent abdmConsent = new ABDMConsent(1L,"11c7ecb8-1dab-4a6b-bbe6-7a74f5851a46","{\"notification\":{\"consentDetail\":{\"consentId\":\"fe8dfb75-467f-4314-824d-366814a8e469\",\"createdAt\":\"2022-07-29T10:01:06.669651\",\"purpose\":{\"text\":\"Self Requested\",\"code\":\"PATRQT\",\"refUri\":null},\"patient\":{\"id\":\"ankurtest3@sbx\"},\"consentManager\":{\"id\":\"sbx\"},\"hip\":{\"id\":\"NIKSHAY-HIP\",\"name\":null},\"hiTypes\":[\"DiagnosticReport\",\"Prescription\",\"ImmunizationRecord\",\"DischargeSummary\",\"OPConsultation\",\"HealthDocumentRecord\",\"WellnessRecord\"],\"permission\":{\"accessMode\":\"VIEW\",\"dateRange\":{\"from\":\"2015-07-29T10:01:06.639518\",\"to\":\"2022-07-29T10:01:06.639528\"},\"dataEraseAt\":\"2022-09-29T10:01:06.483299\",\"frequency\":{\"unit\":\"HOUR\",\"value\":1,\"repeats\":0}},\"careContexts\":[{\"patientReference\":\"41777\",\"careContextReference\":\"10750687\"}]},\"status\":\"REVOKED\",\"signature\":\"IE0Uky4ebPv7KG9m8XBNDgG00vQj4OU9mEaotXcssfB74UsWxAM8b50gQXFoLpdYTLn9cKfvVGVrcDMQzH821W35hwMpRwt3DQAgqWqU5+mVWdi2GJLn9nJMN4uQRDdNpWh+3UCRXMmCFLPRzzvmjtHaEM/cdB6gmZdAMaO+qhRfCu24eDcaukjxMD343GiVkzsWCFzDnS0SW8Iw4P4jfc/n6yio8qavsaD7l7TQBCDNN2c4MlS3FcZAbukpliPjWHsKKVXCy6PT83lO55Xhz79yrZH2jGSySpfEWRduN+bJ1VOFT8gxdnqx2jvXsNImtCR1Pcm3PS4DdvfcUCsUpQ==\",\"consentId\":\"fe8dfb75-467f-4314-824d-366814a8e469\",\"grantAcknowledgement\":false},\"requestId\":\"1c2cdc28-d606-4cdb-b257-34904b07794f\",\"timestamp\":\"2022-07-29T10:01:06.722336\"}",Date.from(Instant.now()),Date.from(Instant.now()),1L,"HIP");
        when(abdmConsentRepository.save(any())).thenReturn(abdmConsent);
        abdmServiceImpl.addHIUConsentDetails(addConsentRequest);
        verify(rabbitMQPublisherService, Mockito.times(1)).send(any(),any(),any());
    }

    @Test
    public void updateHIUConsentDetails_GrantedConsent() throws Exception {
        ConsentNotificationDto consentNotificationDto = Utils.convertObject(getGrantedConsentNotificationJson(),ConsentNotificationDto.class);
        UpdateConsentDto updateConsentDto = new UpdateConsentDto("1","11c7ecb8-1dab-4a6b-bbe6-7a74f5851a46",null,consentNotificationDto);
        when(abdmConsentRepository.findConsentByConsentId("11c7ecb8-1dab-4a6b-bbe6-7a74f5851a46")).thenReturn(new ABDMConsent(1L, "HIU"));
        abdmServiceImpl.updateHIUConsentDetails(updateConsentDto);
        verify(abdmConsentRepository, Mockito.times(1)).save(any());
        verify(userMedicalRecordsRepository, Mockito.times(1)).saveAll(any());
    }

    @Test
    public void updateHIUConsentDetails_ExpiredConsent() throws Exception {
        ConsentNotificationDto consentNotificationDto = Utils.convertObject(getExpiredConsentNotificationJson(),ConsentNotificationDto.class);
        UpdateConsentDto updateConsentDto = new UpdateConsentDto("1","11c7ecb8-1dab-4a6b-bbe6-7a74f5851a46",null,consentNotificationDto);
        when(abdmConsentRepository.findConsentByConsentId("11c7ecb8-1dab-4a6b-bbe6-7a74f5851a46")).thenReturn(new ABDMConsent(1L, "HIU"));
        abdmServiceImpl.updateHIUConsentDetails(updateConsentDto);
        verify(abdmConsentRepository, Mockito.times(1)).save(any());
        verify(userMedicalRecordsRepository, Mockito.times(1)).deleteAllByConsentRequestId(any());
    }

    @Test
    public void updateHIUConsentDetails_RevokedConsent() throws Exception {
        ConsentNotificationDto consentNotificationDto = Utils.convertObject(getRevokedConsentNotificationJson(),ConsentNotificationDto.class);
        UpdateConsentDto updateConsentDto = new UpdateConsentDto("1","11c7ecb8-1dab-4a6b-bbe6-7a74f5851a46",null,consentNotificationDto);
        when(abdmConsentRepository.findConsentByConsentId("11c7ecb8-1dab-4a6b-bbe6-7a74f5851a46")).thenReturn(new ABDMConsent(1L, "HIU"));
        abdmServiceImpl.updateHIUConsentDetails(updateConsentDto);
        verify(abdmConsentRepository, Mockito.times(1)).save(any());
    }

    private String getGrantedConsentNotificationJson() {
        return "{\"requestId\":\"5f7a535d-a3fd-416b-b069-c97d021fbacd\",\"timestamp\":\"1970-01-01T00:00:00.000Z\",\"notification\":{\"consentRequestId\":\"5f7a535d-a3fd-416b-b069-c97d021fbacd\",\"status\":\"GRANTED\",\"consentArtefacts\":[{\"id\":\"5f7a535d-a3fd-416b-b069-c97d021fbacd\"}]}}";
    }

    private String getExpiredConsentNotificationJson() {
        return "{\"requestId\":\"5f7a535d-a3fd-416b-b069-c97d021fbacd\",\"timestamp\":\"1970-01-01T00:00:00.000Z\",\"notification\":{\"consentRequestId\":\"5f7a535d-a3fd-416b-b069-c97d021fbacd\",\"status\":\"EXPIRED\",\"consentArtefacts\":[{\"id\":\"5f7a535d-a3fd-416b-b069-c97d021fbacd\"}]}}";
    }

    private String getRevokedConsentNotificationJson() {
        return "{\"requestId\":\"5f7a535d-a3fd-416b-b069-c97d021fbacd\",\"timestamp\":\"1970-01-01T00:00:00.000Z\",\"notification\":{\"consentRequestId\":\"5f7a535d-a3fd-416b-b069-c97d021fbacd\",\"status\":\"REVOKED\",\"consentArtefacts\":[{\"id\":\"5f7a535d-a3fd-416b-b069-c97d021fbacd\"}]}}";
    }
}

package tests.unit.services;

import com.everwell.userservice.enums.UserRelationStatus;
import com.everwell.userservice.exceptions.ValidationException;
import com.everwell.userservice.models.db.UserRelation;
import com.everwell.userservice.models.dtos.UserRelationDto;
import com.everwell.userservice.repositories.UserRelationRepository;
import com.everwell.userservice.services.UserRelationService;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import tests.BaseTest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

public class UserRelationServiceTest extends BaseTest {
    @MockBean
    private UserRelationRepository userRelationRepository;

    @Autowired
    private UserRelationService userRelationService;

    @Test(expected = ValidationException.class)
    public void addUserRelationAlreadyExists()  {
        UserRelationDto userRelationDto =  new UserRelationDto(1L, 2L, "Parent", "Caregiver", "test", "ACCEPT", false);
        when(userRelationRepository.findByPrimaryUserIdAndSecondaryUserId(eq(1L), eq(2L))).thenReturn(new UserRelation());

        userRelationService.addUserRelation(userRelationDto);
    }
    

    @Test(expected = ValidationException.class)
    public void addUserRelationInvalidRole()  {
        UserRelationDto userRelationDto = new UserRelationDto(1L, 2L, "Parent", "abc", "test", "ACCEPT", false);
        when(userRelationRepository.findByPrimaryUserIdAndSecondaryUserId(eq(1L), eq(2L))).thenReturn(null);

        userRelationService.addUserRelation(userRelationDto);
    }

    @Test(expected = ValidationException.class)
    public void addUserRelationNoRelation()  {
        UserRelationDto userRelationDto = new UserRelationDto(1L, 2L, null, "Caregiver", "test", "ACCEPT", false);
        when(userRelationRepository.findByPrimaryUserIdAndSecondaryUserId(eq(1L), eq(2L))).thenReturn(null);

        userRelationService.addUserRelation(userRelationDto);
    }

    @Test(expected = ValidationException.class)
    public void addUserRelationInvalidRelation()  {
        UserRelationDto userRelationDto = new UserRelationDto(1L, 2L, "abc", "Caregiver", "test", "ACCEPT", false);
        when(userRelationRepository.findByPrimaryUserIdAndSecondaryUserId(eq(1L), eq(2L))).thenReturn(null);

        userRelationService.addUserRelation(userRelationDto);
    }

    @Test
    public void addUserRelation()  {
        UserRelationDto userRelationDto = new UserRelationDto(1L, 2L, "Parent", "Caregiver", "test", null, false);
        when(userRelationRepository.findByPrimaryUserIdAndSecondaryUserId(eq(1L), eq(2L))).thenReturn(null);

        userRelationService.addUserRelation(userRelationDto);
        Mockito.verify(userRelationRepository, Mockito.times(1)).save(any());
    }

    @Test(expected = ValidationException.class)
    public void deleteUserRelationDoesNotExists()  {
        UserRelationDto userRelationDto =  new UserRelationDto(1L, 2L, "Parent", "Caregiver", "test", "ACCEPT", false);
        when(userRelationRepository.findByPrimaryUserIdAndSecondaryUserId(eq(1L), eq(2L))).thenReturn(null);

        userRelationService.deleteUserRelation(userRelationDto);
    }

    @Test
    public void deleteUserRelation()  {
        UserRelationDto userRelationDto = new UserRelationDto(1L, 2L, "Parent", "Caregiver", "test", "ACCEPT", false);
        when(userRelationRepository.findByPrimaryUserIdAndSecondaryUserId(eq(1L), eq(2L))).thenReturn(new UserRelation(userRelationDto));
        when(userRelationRepository.findByPrimaryUserIdAndSecondaryUserId(eq(2L), eq(1L))).thenReturn(new UserRelation(userRelationDto));

        userRelationService.deleteUserRelation(userRelationDto);
        Mockito.verify(userRelationRepository, Mockito.times(1)).deleteAll(any());
    }

    @Test(expected = ValidationException.class)
    public void updateUserRelationDoesNotExists()  {
        UserRelationDto userRelationDto =  new UserRelationDto(1L, 2L, "Parent", "Caregiver", "test", "ACCEPT", false);
        when(userRelationRepository.findByPrimaryUserIdAndSecondaryUserId(eq(1L), eq(2L))).thenReturn(null);

        userRelationService.updateUserRelation(userRelationDto);
    }

    @Test(expected = ValidationException.class)
    public void testUpdateUserRelationNoStatus()  {
        UserRelationDto userRelationDto = new UserRelationDto(1L, 2L, "Parent", "Caregiver", "test", "", false);
        UserRelation userRelation = new UserRelation(userRelationDto);
        userRelation.setStatus(UserRelationStatus.PENDING.getValue());
        when(userRelationRepository.findByPrimaryUserIdAndSecondaryUserId(eq(1L), eq(2L))).thenReturn(userRelation);

        userRelationService.updateUserRelation(userRelationDto);
    }

    @Test(expected = ValidationException.class)
    public void testUpdateUserRelationInvalidStatus()  {
        UserRelationDto userRelationDto = new UserRelationDto(1L, 2L, "Parent", "Caregiver", "test", "abc", false);
        UserRelation userRelation = new UserRelation(userRelationDto);
        userRelation.setStatus(UserRelationStatus.PENDING.getValue());
        when(userRelationRepository.findByPrimaryUserIdAndSecondaryUserId(eq(1L), eq(2L))).thenReturn(userRelation);

        userRelationService.updateUserRelation(userRelationDto);
    }

    @Test
    public void testUpdateUserRelationFromPending()  {
        UserRelationDto userRelationDto = new UserRelationDto(1L, 2L, "Parent", "Caregiver", "test", "ACCEPT", false);
        UserRelation userRelation = new UserRelation(userRelationDto);
        userRelation.setStatus(UserRelationStatus.PENDING.getValue());
        when(userRelationRepository.findByPrimaryUserIdAndSecondaryUserId(eq(1L), eq(2L))).thenReturn(userRelation);

        userRelationService.updateUserRelation(userRelationDto);
        Mockito.verify(userRelationRepository, Mockito.times(2)).save(any());
    }

    @Test
    public void testUpdateUserRelationFromDeny()  {
        UserRelationDto userRelationDto = new UserRelationDto(1L, 2L, "Parent", "Caregiver", "test", "DENY", false);
        UserRelation userRelation = new UserRelation(userRelationDto);
        when(userRelationRepository.findByPrimaryUserIdAndSecondaryUserId(eq(1L), eq(2L))).thenReturn(userRelation);
        when(userRelationRepository.findByPrimaryUserIdAndSecondaryUserId(eq(2L), eq(1L))).thenReturn(null);

        userRelationService.updateUserRelation(userRelationDto);
        Mockito.verify(userRelationRepository, Mockito.times(1)).deleteAll(any());
    }
}

package tests.unit.services;

import com.everwell.userservice.exceptions.ValidationException;
import com.everwell.userservice.models.dtos.*;
import com.everwell.userservice.models.http.requests.*;
import com.everwell.userservice.models.http.responses.*;
import com.everwell.userservice.services.impl.ABHAServiceImpl;
import com.everwell.userservice.utils.CacheUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.http.*;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;
import tests.BaseTest;

import java.util.*;

import static com.everwell.userservice.constants.ABHAConstants.*;
import static com.everwell.userservice.enums.ABHARoute.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

public class ABHAServiceTest extends BaseTest
{
    @Spy
    @InjectMocks
    private ABHAServiceImpl abhaService;

    @Mock
    RedisTemplate<String, Object> redisTemplate;

    CacheUtils cacheUtils;

    @Mock
    private ValueOperations<String, Object> valueOperations;

    @Mock
    RestTemplate restTemplate;

    @Before
    public void setUp() {
        ReflectionTestUtils.setField(abhaService, "accessEndpoint", "localhost");
        ReflectionTestUtils.setField(abhaService, "apiEndpoint", "localhost");
        ReflectionTestUtils.setField(abhaService, "clientId", "localhost");
        ReflectionTestUtils.setField(abhaService, "clientSecret", "localhost");
        cacheUtils = new CacheUtils(redisTemplate);
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
    }

    @Test
    public void testVerifyOtpForAadhaar() throws Exception {
        OTPVerifyRequest otpVerifyRequest = new OTPVerifyRequest("123456", "abcd1234", 1L, "PATIENT", 2L,"AADHAAR_OTP");
        AadhaarOTPResponse aadhaarOTPResponse = new AadhaarOTPResponse("abcd1234", "9876543210");
        ResponseEntity responseEntity = new ResponseEntity(aadhaarOTPResponse, HttpStatus.OK);
        HttpHeaders localHeaders = new HttpHeaders();
            localHeaders.add(ABHA_X_TOKEN, "Bearer ");
        HttpEntity<Map> request = new HttpEntity<>(localHeaders);
        Mockito.when(restTemplate.exchange(eq("localhost" + VERIFY_OTP_AADHAAR.getPath()), (HttpMethod) any(), any(), (ParameterizedTypeReference<Object>) any())).thenReturn(responseEntity);
        AadhaarOTPResponse response = abhaService.verifyOtp(otpVerifyRequest, "aadhaar");
        assertEquals(response.getTxnId(), aadhaarOTPResponse.getTxnId());
    }

    @Test
    public void testVerifyOtpForMobile() throws Exception {
        OTPVerifyRequest otpVerifyRequest = new OTPVerifyRequest("123456", "abcd1234", 1L, "PATIENT", 2L,"MOBILE_OTP");
        AadhaarOTPResponse aadhaarOTPResponse = new AadhaarOTPResponse("abcd1234", "9876543210");
        ResponseEntity responseEntity = new ResponseEntity(aadhaarOTPResponse, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(eq("localhost" + VERIFY_OTP_MOBILE.getPath()), (HttpMethod) any(), any(), (ParameterizedTypeReference<Object>) any())).thenReturn(responseEntity);
        AadhaarOTPResponse response = abhaService.verifyOtp(otpVerifyRequest, "mobile");
        assertEquals(response.getTxnId(), aadhaarOTPResponse.getTxnId());
    }

    @Test
    public void testCreateAbha() throws Exception {
        ABHACreateRequest abhaCreateRequest = new ABHACreateRequest();
        abhaCreateRequest.setTxnId("abcd1234");
        ABHACreateResponse abhaCreateResponse = new ABHACreateResponse();
        abhaCreateResponse.setName("Marcus E");
        abhaCreateResponse.setHealthIdNumber("91-1234-1234-1234");
        ResponseEntity responseEntity = new ResponseEntity(abhaCreateResponse, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(eq("localhost" + CREATE_ABHA_PREVERIFIED.getPath()), (HttpMethod) any(), any(), (ParameterizedTypeReference<Object>) any())).thenReturn(responseEntity);
        ABHACreateResponse response = abhaService.createAbha(abhaCreateRequest);
        assertEquals(response.getHealthIdNumber(), abhaCreateResponse.getHealthIdNumber());
    }

    @Test
    public void testBuildUpdateUserReqForNewAbha() throws Exception {
        ABHACreateResponse abhaCreateResponse = new ABHACreateResponse();
        abhaCreateResponse.setFirstName("Adam");
        abhaCreateResponse.setMiddleName("N");
        abhaCreateResponse.setGender("Male");
        abhaCreateResponse.setDistrictName("Somewhere");
        abhaCreateResponse.setDayOfBirth("1");
        abhaCreateResponse.setMonthOfBirth("1");
        abhaCreateResponse.setYearOfBirth("1111");
        abhaCreateResponse.setMobile("9876543210");
        abhaCreateResponse.setHealthId("91123412341234@sbx");
        abhaCreateResponse.setHealthIdNumber("91-1234-1234-1234");
        UpdateUserRequest response = abhaService.buildUpdateUserReq(1L, "abcd1234", abhaCreateResponse);
        assertEquals(response.getFirstName(), abhaCreateResponse.getFirstName() + " " + abhaCreateResponse.getMiddleName());
        assertEquals(response.getDateOfBirth(), String.format("%s-%s-%s", abhaCreateResponse.getYearOfBirth(), abhaCreateResponse.getMonthOfBirth(), abhaCreateResponse.getDayOfBirth()));
        Assert.assertNull(response.getLastName());
        Assert.assertNull(response.getPincode());
        Assert.assertNull(response.getAddress());

    }

    @Test
    public void testBuildUpdateUserReqForExistingAbha() throws Exception {
        ABHAProfileResponse abhaProfileResponse = new ABHAProfileResponse();
        abhaProfileResponse.setFirstName("Adam");
        abhaProfileResponse.setMiddleName("N");
        abhaProfileResponse.setGender("Male");
        abhaProfileResponse.setDistrictName("Somewhere");
        abhaProfileResponse.setDayOfBirth("1");
        abhaProfileResponse.setMonthOfBirth("1");
        abhaProfileResponse.setYearOfBirth("1111");
        abhaProfileResponse.setMobile("9876543210");
        abhaProfileResponse.setHealthId("91123412341234@sbx");
        abhaProfileResponse.setHealthIdNumber("91-1234-1234-1234");
        abhaProfileResponse.setLastName("Eve");
        abhaProfileResponse.setAddress("4/13 temporary address");
        abhaProfileResponse.setPincode("350006");
        UpdateUserRequest response = abhaService.buildUpdateUserReq(1L, "abcd1234", abhaProfileResponse);
        assertEquals(response.getFirstName(), abhaProfileResponse.getFirstName() + " " + abhaProfileResponse.getMiddleName());
        assertEquals(response.getDateOfBirth(), String.format("%s-%s-%s", abhaProfileResponse.getYearOfBirth(), abhaProfileResponse.getMonthOfBirth(), abhaProfileResponse.getDayOfBirth()));
        assertEquals(response.getLastName(), abhaProfileResponse.getLastName());
        assertEquals(Optional.ofNullable(response.getPincode()), Optional.ofNullable(Integer.parseInt(abhaProfileResponse.getPincode())));
        assertEquals(response.getAddress(), abhaProfileResponse.getAddress());
    }

    @Test
    public void testSearchAbha() throws Exception {
        ABHASearchRequest abhaSearchRequest = new ABHASearchRequest("91-1234-1234-1234");
        ABHASearchResponse abhaSearchResponse = new ABHASearchResponse("ACTIVE");
        ResponseEntity responseEntity = new ResponseEntity(abhaSearchResponse, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(eq("localhost" + SEARCH_ABHA_HEALTH_ID.getPath()), (HttpMethod) any(), any(), (ParameterizedTypeReference<Object>) any())).thenReturn(responseEntity);
        ABHASearchResponse response = abhaService.searchAbha(abhaSearchRequest);
        assertEquals(response.getStatus(), abhaSearchResponse.getStatus());
    }

    @Test
    public void testGenerateOtpForAadhaar() throws Exception {
        AadhaarOTPRequest aadhaarOTPRequest = new AadhaarOTPRequest("123456789123");
        AadhaarOTPResponse aadhaarOTPResponse = new AadhaarOTPResponse();
        aadhaarOTPResponse.setTxnId("12334-abcd");
        ResponseEntity responseEntity = new ResponseEntity(aadhaarOTPResponse, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(eq("localhost" + GENERATE_OTP_AADHAAR.getPath()), (HttpMethod) any(), any(), (ParameterizedTypeReference<Object>) any())).thenReturn(responseEntity);
        AadhaarOTPResponse response = abhaService.generateOtp(aadhaarOTPRequest, GENERATE_OTP_AADHAAR.getPath());
        assertEquals(response.getTxnId(), aadhaarOTPResponse.getTxnId());
    }

    @Test
    public void testGenerateAccessTokenForPost() throws Exception {
        AadhaarOTPRequest aadhaarOTPRequest = new AadhaarOTPRequest("123456789123");
        AadhaarOTPResponse aadhaarOTPResponse = new AadhaarOTPResponse();
        aadhaarOTPResponse.setTxnId("12334-abcd");
        Map<String,String> object = new HashMap<>();
        object.put("accessToken", "eyJhbGciOiJSUzUxMiJ9");
        ResponseEntity responseEntity1 = new ResponseEntity(HttpStatus.UNAUTHORIZED);
        ResponseEntity responseEntity2 = new ResponseEntity(aadhaarOTPResponse, HttpStatus.OK);
        ResponseEntity tokenResponseEntity = new ResponseEntity(object, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(eq("localhost" + GENERATE_OTP_AADHAAR.getPath()), (HttpMethod) any(), any(), (ParameterizedTypeReference<Object>) any())).thenReturn(responseEntity2);
        Mockito.when(restTemplate.exchange(eq("localhost" + VERIFY_OTP_MOBILE.getPath()), (HttpMethod) any(), any(), (ParameterizedTypeReference<Object>) any())).thenReturn(tokenResponseEntity);
        AadhaarOTPResponse response = abhaService.generateOtp(aadhaarOTPRequest, GENERATE_OTP_AADHAAR.getPath());
        Mockito.verify(abhaService, Mockito.times(1)).generateOtp(any(), anyString());
        assertEquals(response.getTxnId(), aadhaarOTPResponse.getTxnId());
    }

    @Test(expected = ValidationException.class)
    public void testGenerateOtpForAadhaar_emptyBody() throws Exception {
        AadhaarOTPRequest aadhaarOTPRequest = new AadhaarOTPRequest("123456789123");
        AadhaarOTPResponse aadhaarOTPResponse = new AadhaarOTPResponse();
        aadhaarOTPResponse.setTxnId("12334-abcd");
        Map<String,String> object = new HashMap<>();
        object.put("accessToken", "eyJhbGciOiJSUzUxMiJ9");
        ResponseEntity responseEntity = new ResponseEntity(null, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(eq("localhost" + GENERATE_OTP_AADHAAR.getPath()), (HttpMethod) any(), any(), (ParameterizedTypeReference<Object>) any())).thenReturn(responseEntity);
        AadhaarOTPResponse response = abhaService.generateOtp(aadhaarOTPRequest, GENERATE_OTP_AADHAAR.getPath());
    }

    @Test
    public void testGenerateOtpForMobile() throws Exception {
        MobileOTPRequest mobileOTPRequest = new MobileOTPRequest("09897654321", "12334-abcd");
        AadhaarOTPResponse aadhaarOTPResponse = new AadhaarOTPResponse();
        aadhaarOTPResponse.setTxnId("12334-abcd");
        ResponseEntity responseEntity = new ResponseEntity(aadhaarOTPResponse, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(eq("localhost" + GENERATE_OTP_MOBILE.getPath()), (HttpMethod) any(), any(), (ParameterizedTypeReference<Object>) any())).thenReturn(responseEntity);
        AadhaarOTPResponse response = abhaService.generateOtp(mobileOTPRequest, GENERATE_OTP_MOBILE.getPath());
        assertEquals(response.getTxnId(), aadhaarOTPResponse.getTxnId());
    }

    @Test(expected = ValidationException.class)
    public void testGenerateOtpForMobile_emptyBody() throws Exception {
        MobileOTPRequest mobileOTPRequest = new MobileOTPRequest("09897654321", "12334-abcd");
        AadhaarOTPResponse aadhaarOTPResponse = new AadhaarOTPResponse();
        aadhaarOTPResponse.setTxnId("12334-abcd");
        ResponseEntity responseEntity = new ResponseEntity(null, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(eq("localhost" + GENERATE_OTP_MOBILE.getPath()), (HttpMethod) any(), any(), (ParameterizedTypeReference<Object>) any())).thenReturn(responseEntity);
        AadhaarOTPResponse response = abhaService.generateOtp(mobileOTPRequest, GENERATE_OTP_MOBILE.getPath());
    }

    @Test
    public void testGenerateOtpForAuth() throws Exception {
        HealthIdOTPRequest healthIdOTPRequest = new HealthIdOTPRequest("AADHAAR_OTP", "91-1234-1234-1234");
        AadhaarOTPResponse aadhaarOTPResponse = new AadhaarOTPResponse();
        aadhaarOTPResponse.setTxnId("12334-abcd");
        ResponseEntity responseEntity = new ResponseEntity(aadhaarOTPResponse, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(eq("localhost" + GENERATE_OTP_AUTH.getPath()), (HttpMethod) any(), any(), (ParameterizedTypeReference<Object>) any())).thenReturn(responseEntity);
        AadhaarOTPResponse response = abhaService.generateOtp(healthIdOTPRequest, GENERATE_OTP_AUTH.getPath());
        assertEquals(response.getTxnId(), aadhaarOTPResponse.getTxnId());
    }

    @Test
    public void testResendOtpForAadhaar() throws Exception {
        ResendOTPRequest resendOTPRequest = new ResendOTPRequest();
        resendOTPRequest.setTxnId("12334-abcd");
        ResponseEntity responseEntity = new ResponseEntity("OK", HttpStatus.OK);
        Mockito.when(restTemplate.exchange(eq("localhost" + RESEND_OTP_AADHAAR.getPath()), (HttpMethod) any(), any(), (ParameterizedTypeReference<Object>) any())).thenReturn(responseEntity);
        abhaService.resendOtp(resendOTPRequest, RESEND_OTP_AADHAAR.getPath());
        Mockito.verify(abhaService, Mockito.times(1)).resendOtp(any(), anyString());
    }

    @Test
    public void testResendOtpForAuth() throws Exception {
        ResendOTPRequest resendOTPRequest = new ResendOTPRequest("12334-abcd", "AADHAAR_OTP");
        ResponseEntity responseEntity = new ResponseEntity("OK", HttpStatus.OK);
        Mockito.when(restTemplate.exchange(eq("localhost" + RESEND_OTP_AUTH.getPath()), (HttpMethod) any(), any(), (ParameterizedTypeReference<Object>) any())).thenReturn(responseEntity);
        abhaService.resendOtp(resendOTPRequest, RESEND_OTP_AUTH.getPath());
        Mockito.verify(abhaService, Mockito.times(1)).resendOtp(any(), anyString());
    }

    @Test
    public void testVerifyOtpForAuth() throws Exception {
        Long userId = 1L;
        OTPVerifyRequest otpVerifyRequest = new OTPVerifyRequest();
        otpVerifyRequest.setOtp("123456");
        otpVerifyRequest.setTxnId("12334-abcd");
        otpVerifyRequest.setUserId(userId);
        ABHAAuthResponse abhaAuthResponse = new ABHAAuthResponse();
        abhaAuthResponse.setToken("eyJhbGciOiJSUzUxMiJ9");
        ResponseEntity responseEntity = new ResponseEntity(abhaAuthResponse, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(eq("localhost" + VERIFY_OTP_AUTH.getPath()), (HttpMethod) any(), any(), (ParameterizedTypeReference<Object>) any())).thenReturn(responseEntity);
        ABHAAuthResponse response = abhaService.verifyOtpForAuth(otpVerifyRequest);
        assertEquals(response.getToken(), abhaAuthResponse.getToken());
    }

    @Test
    public void testGetProfile() throws Exception {
        ABHAProfileResponse abhaProfileResponse = new ABHAProfileResponse();
        abhaProfileResponse.setAddress("4/13 temporary address");
        abhaProfileResponse.setPincode("350006");
        ResponseEntity responseEntity = new ResponseEntity(abhaProfileResponse, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(eq("localhost" + GET_ABHA_PROFILE.getPath()), (HttpMethod) any(), any(), (ParameterizedTypeReference<Object>) any())).thenReturn(responseEntity);
        ABHAProfileResponse response = abhaService.getProfile("eyJhbGciOiJSUzUxMiJ9");
        assertEquals(response.getPincode(), abhaProfileResponse.getPincode());
    }

    @Test
    public void testGetAbhaImageCard() throws Exception {
        Long userId = 43242L;
        byte[] imageByteArr = "ABHA card Image".getBytes();
        ResponseEntity responseEntity = new ResponseEntity(imageByteArr, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(eq("localhost" + GET_ABHA_CARD_IMAGE.getPath()), (HttpMethod) any(), any(), (ParameterizedTypeReference<Object>) any())).thenReturn(responseEntity);
        String cacheKey = ABHA_X_AUTH_TOKEN+ "_" +userId;
        when(valueOperations.get(cacheKey)).thenReturn("{\"XYZ\": \"ABCD\"}");
        byte[] response = abhaService.getAbhaImage(userId, "card");
        assertEquals(response, imageByteArr);
    }

    @Test
    public void testGetAbhaImageQr() throws Exception {
        Long userId = 43242L;
        byte[] imageByteArr = "ABHA QR Image".getBytes();
        ResponseEntity responseEntity = new ResponseEntity(imageByteArr, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(eq("localhost" + GET_ABHA_QR_IMAGE.getPath()), (HttpMethod) any(), any(), (ParameterizedTypeReference<Object>) any())).thenReturn(responseEntity);
        String cacheKey = ABHA_X_AUTH_TOKEN+ "_" +userId;
        when(valueOperations.get(cacheKey)).thenReturn("{\"XYZ\": \"ABCD\"}");
        byte[] response = abhaService.getAbhaImage(userId, "qr");
        assertEquals(response, imageByteArr);
    }

    @Test
    public void testGenerateAccessTokenForGet() throws Exception {
        Long userId = 43242L;
        Map<String,String> object = new HashMap<>();
        object.put("accessToken", "eyJhbGciOiJSUzUxMiJ9");
        byte[] imageByteArr = "ABHA QR Image".getBytes();
        ResponseEntity responseEntity1 = new ResponseEntity(HttpStatus.UNAUTHORIZED);
        ResponseEntity responseEntity2 = new ResponseEntity(imageByteArr, HttpStatus.OK);
        ResponseEntity tokenResponseEntity = new ResponseEntity(object, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(eq("localhost" + GET_ABHA_QR_IMAGE.getPath()), eq(HttpMethod.GET), ArgumentMatchers.any(), (ParameterizedTypeReference<Object>) any()))
                .thenReturn(responseEntity2);
        Mockito.when(restTemplate.exchange(eq("localhost" + GET_ACCESS_TOKEN.getPath()), (HttpMethod) any(), any(), (ParameterizedTypeReference<Object>) any())).thenReturn(tokenResponseEntity);
        String cacheKey = ABHA_X_AUTH_TOKEN+ "_" +userId;
        when(valueOperations.get(cacheKey)).thenReturn("{\"XYZ\": \"ABCD\"}");
        byte[] response = abhaService.getAbhaImage(userId, "qr");
        assertEquals(response, imageByteArr);
        Mockito.verify(abhaService, Mockito.times(1)).getAbhaImage(anyLong(), anyString());
    }

    @Test(expected = ValidationException.class)
    public void testGetAbhaQrImage_emptyBody() throws Exception {
        ResponseEntity responseEntity = new ResponseEntity(null, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(eq("localhost" + GET_ABHA_QR_IMAGE.getPath()), (HttpMethod) any(), any(), (ParameterizedTypeReference<Object>) any())).thenReturn(responseEntity);
        byte[] response = abhaService.getAbhaImage(43242L, "qr");
    }

    @Test(expected = ValidationException.class)
    public void testGetAbhaCardImage_emptyBody() throws Exception {
        ResponseEntity responseEntity = new ResponseEntity(null, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(eq("localhost" + GET_ABHA_CARD_IMAGE.getPath()), (HttpMethod) any(), any(), (ParameterizedTypeReference<Object>) any())).thenReturn(responseEntity);;
        byte[] response = abhaService.getAbhaImage(43242L, "card");
    }


    @Test
    public void testGenerateOtpForMobileEmailLogin() throws Exception {
        Long userId = 43242L;
        MobileEmailOTPRequest mobileEmailOTPRequest = new MobileEmailOTPRequest();
        mobileEmailOTPRequest.setValue("JdfjlkjlfjjUOIUjoj==");

//        MobileEmailOTPRequester mobileEmailOTPRequester = new MobileEmailOTPRequester();
//        mobileEmailOTPRequester.setType("PHR");
//        mobileEmailOTPRequester.setId("phr_001");


//        mobileEmailOTPRequest.setRequester(mobileEmailOTPRequester);
        mobileEmailOTPRequest.setPurpose("REGISTRATION");
        mobileEmailOTPRequest.setAuthMode("MOBILE_OTP");

        MobileEmailOTPResponse mobileEmailOTPResponse = new MobileEmailOTPResponse();
        mobileEmailOTPResponse.setTransactionId("12334-abcd");
        mobileEmailOTPResponse.setAuthMode("MOBILE_OTP");
        mobileEmailOTPResponse.setRequesterId("phr_001");


        ResponseEntity responseEntity = new ResponseEntity(mobileEmailOTPResponse, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(eq("localhost" + GENERATE_OTP_FOR_MOBILE_EMAIL_LOGIN.getPath()) ,(HttpMethod) any(), any(), (ParameterizedTypeReference<Object>) any())).thenReturn(responseEntity);
        MobileEmailOTPResponse response = abhaService.generateOtpForMobileEmailLogin(mobileEmailOTPRequest, GENERATE_OTP_FOR_MOBILE_EMAIL_LOGIN.getPath());
        assertEquals(response.getTransactionId(), mobileEmailOTPResponse.getTransactionId());
    }


    @Test
    public void testVerifyOtpForMobileEmailLogin() throws Exception {
        Long userId = 43242L;
        MobileEmailVerifyOTPRequest mobileEmailVerifyOTPRequest = new MobileEmailVerifyOTPRequest();
        mobileEmailVerifyOTPRequest.setAuthCode("MOBILE_OTP");
        mobileEmailVerifyOTPRequest.setTransactionId("12334-abcd");
        mobileEmailVerifyOTPRequest.setUserId(userId);

        MobileEmailVerifyOTPResponse mobileEmailVerifyOTPResponse = new MobileEmailVerifyOTPResponse();
        mobileEmailVerifyOTPResponse.setTransactionId("12334-abcd");
        mobileEmailVerifyOTPResponse.setMappedPhrAddress(new ArrayList<String>());
        mobileEmailVerifyOTPResponse.setMobileEmail("MOBILE");

        String cacheKey = DEFAULT_ABHA_TRANSACTION_SESSION_ID_CACHE_LABEL+ userId;
        when(valueOperations.get(cacheKey)).thenReturn("{\"XYZ\": \"ABCD\"}");


        ResponseEntity responseEntity = new ResponseEntity(mobileEmailVerifyOTPResponse, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(eq("localhost" + VERIFY_OTP_FOR_MOBILE_EMAIL_LOGIN.getPath()) ,(HttpMethod) any(), any(), (ParameterizedTypeReference<Object>) any())).thenReturn(responseEntity);
        MobileEmailVerifyOTPResponse response = abhaService.verifyOtpForMobileEmailLogin(mobileEmailVerifyOTPRequest, VERIFY_OTP_FOR_MOBILE_EMAIL_LOGIN.getPath());
        assertEquals(response.getTransactionId(), mobileEmailVerifyOTPResponse.getTransactionId());
    }


    @Test
    public void testUserAuthorizedTokenForMobileEmail() throws Exception {
        Long userId = 43242L;
        UserAuthorizedTokenForMobileEmailRequest userAuthorizedTokenForMobileEmailRequest = new UserAuthorizedTokenForMobileEmailRequest();
        userAuthorizedTokenForMobileEmailRequest.setPatientId("239484");
        userAuthorizedTokenForMobileEmailRequest.setTransactionId("12334-abcd");
        userAuthorizedTokenForMobileEmailRequest.setRequesterId("8402-432u-jkll-jd48");
        userAuthorizedTokenForMobileEmailRequest.setUserId(userId);

        UserAuthorizedTokenResponse userAuthorizedTokenResponse = new UserAuthorizedTokenResponse();
        userAuthorizedTokenResponse.setToken("abcde");

        String cacheKey = DEFAULT_ABHA_TRANSACTION_SESSION_ID_CACHE_LABEL+ userId;
        when(valueOperations.get(cacheKey)).thenReturn("{\"XYZ\": \"ABCD\"}");

        ResponseEntity responseEntity = new ResponseEntity(userAuthorizedTokenResponse, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(eq("localhost" + AUTH_CONFIRM_OTP_FOR_MOBILE_EMAIL_LOGIN.getPath()) ,(HttpMethod) any(), any(), (ParameterizedTypeReference<Object>) any())).thenReturn(responseEntity);
        UserAuthorizedTokenResponse response = abhaService.userAuthorizedTokenForMobileEmail(userAuthorizedTokenForMobileEmailRequest, AUTH_CONFIRM_OTP_FOR_MOBILE_EMAIL_LOGIN.getPath());
        assertEquals(response.getToken(), userAuthorizedTokenResponse.getToken());
    }


    @Test
    public void testShareProfile() throws Exception {
        Long userId = 43242L;
        ShareProfileRequest shareProfileRequest = new ShareProfileRequest();
        shareProfileRequest.setRequestId("8402-432u-jkll-jd48");
        shareProfileRequest.setSource("ANDROID");
        shareProfileRequest.setUserId(userId);


        Intent intent = new Intent();
        intent.setType("REGISTRATION");

        HipDetails hipDetails = new HipDetails();
        hipDetails.setCode("123456");
        hipDetails.setHipId("NIKSHAY_HUI");

        shareProfileRequest.setHipDetails(hipDetails);
        shareProfileRequest.setIntent(intent);

        ShareProfileResponse shareProfileResponse = new ShareProfileResponse();
        shareProfileResponse.setTokenNumber("31");

        String cacheKey = ABHA_X_AUTH_TOKEN+ "_" + userId;
        when(valueOperations.get(cacheKey)).thenReturn("{\"XYZ\": \"ABCD\"}");

        ResponseEntity responseEntity = new ResponseEntity(shareProfileResponse, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(eq("localhost" + SHARE_PROFILE.getPath()) ,(HttpMethod) any(), any(), (ParameterizedTypeReference<Object>) any())).thenReturn(responseEntity);
        ShareProfileResponse response = abhaService.shareProfile(shareProfileRequest, SHARE_PROFILE.getPath());
        assertEquals(response.getTokenNumber(), shareProfileResponse.getTokenNumber());
    }


    @Test
    public void testGetProviderDetails() throws Exception {
        Long userId = 43242L;
        ABHAProviderResponse abhaProviderResponse = new ABHAProviderResponse();
        ABHAProviderIdentifier abhaProviderIdentifier = new ABHAProviderIdentifier();
        abhaProviderIdentifier.setId("NIKSHAY_HUI");
        abhaProviderIdentifier.setName("NIKSHAY");
        abhaProviderResponse.setIdentifier(abhaProviderIdentifier);

        String cacheKey = ABHA_X_AUTH_TOKEN+ "_" + userId;
        when(valueOperations.get(cacheKey)).thenReturn("{\"XYZ\": \"ABCD\"}");

        ResponseEntity responseEntity = new ResponseEntity(abhaProviderResponse, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(anyString(), (HttpMethod) any(), any(), (ParameterizedTypeReference<Object>) any())).thenReturn(responseEntity);
        ABHAProviderResponse response = abhaService.getProviderDetails("NIKSHAY_HUI",userId);
        assertEquals(response.getIdentifier().getName(), abhaProviderResponse.getIdentifier().getName());
    }

    @Test
    public void testGetAbhaProfile() throws Exception {
        Long userId = 43242L;
        ABHAProfileResponse abhaProfileResponse = new ABHAProfileResponse();
        abhaProfileResponse.setAddress("4/13 temporary address");
        abhaProfileResponse.setPincode("350006");

        String cacheKey = ABHA_X_AUTH_TOKEN+ "_" + userId;
        when(valueOperations.get(cacheKey)).thenReturn("{\"XYZ\": \"ABCD\"}");

        ResponseEntity responseEntity = new ResponseEntity(abhaProfileResponse, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(eq("localhost" + GET_ABHA_PROFILE.getPath()), (HttpMethod) any(), any(), (ParameterizedTypeReference<Object>) any())).thenReturn(responseEntity);
        ABHAProfileResponse response = abhaService.getAbhaProfile(userId);
        assertEquals(response.getPincode(), abhaProfileResponse.getPincode());
    }


}

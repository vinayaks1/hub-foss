package tests.unit.services;

import com.everwell.userservice.models.db.User;
import com.everwell.userservice.models.dtos.EventStreamingDto;
import com.everwell.userservice.models.dtos.UpdateUserRequest;
import com.everwell.userservice.models.dtos.abdm.UpdateConsentDto;
import com.everwell.userservice.repositories.UserRepository;
import com.everwell.userservice.services.ABDMService;
import com.everwell.userservice.services.RabbitMQConsumerService;
import com.everwell.userservice.services.UserService;
import com.everwell.userservice.utils.Utils;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import tests.BaseTest;
import org.springframework.amqp.core.Message;

import static com.everwell.userservice.constants.Constants.X_US_CLIENT_ID;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

public class RabbitMQConsumerServiceTest extends BaseTest
{
    @MockBean
    private UserRepository userRepository;

    @Autowired
    private RabbitMQConsumerService rabbitMQConsumerService;

    @MockBean
    private UserService userService;

    @MockBean
    private ABDMService abdmService;

    @Test
    public void testUpdateUser() throws Exception
    {
        long clientId = 1L;
        UpdateUserRequest updateUserRequest = new UpdateUserRequest();
        updateUserRequest.setUserId(1L);
        updateUserRequest.setFirstName("ABC");

        User user = new User();
        user.setId(1L);
        user.setFirstName("DEF");
        when(userRepository.findByIdAndIsDeletedAndClientId(anyLong(), anyBoolean(), anyLong())).thenReturn(user);


        EventStreamingDto<UpdateUserRequest> dto = new EventStreamingDto<>("update-user", updateUserRequest);
        MessageProperties messageProperties = new MessageProperties();
        messageProperties.setHeader(X_US_CLIENT_ID, "1");
        Message message = new Message(Utils.asJsonString(dto).getBytes(), messageProperties);

        rabbitMQConsumerService.updateUserDetails(message);

        verify(userService, Mockito.times(1)).update(any(), any());

    }

    @Test
    public void testUpdateConsentDetails() throws Exception {
        UpdateConsentDto dto = new UpdateConsentDto("1","11c7ecb8-1dab-4a6b-bbe6-7a74f5851a46","1",null);
        MessageProperties messageProperties = new MessageProperties();
        messageProperties.setHeader(X_US_CLIENT_ID, "1");
        Message message = new Message(Utils.asJsonString(dto).getBytes(), messageProperties);
        doNothing().when(abdmService).updateHIUConsentDetails(dto);
        rabbitMQConsumerService.updateConsent(message);
        verify(abdmService, Mockito.times(1)).updateHIUConsentDetails(any());
    }
}

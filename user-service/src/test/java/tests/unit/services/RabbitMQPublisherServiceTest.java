package tests.unit.services;

import com.everwell.userservice.services.impl.RabbitMQPublisherServiceImpl;
import org.junit.Before;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.amqp.core.AmqpTemplate;
import tests.BaseTest;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.*;

public class RabbitMQPublisherServiceTest extends BaseTest {

    @InjectMocks
    RabbitMQPublisherServiceImpl rabbitMQPublisherService;

    @Mock
    AmqpTemplate amqpTemplate;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void sendTest() {
        rabbitMQPublisherService.send("new message", "exchange", "key");
        Mockito.verify(amqpTemplate, Mockito.times(1)).convertAndSend(eq("exchange"), eq("key"), anyString());
    }

    @Test
    public void sendWithHeadersTest() {
        Map<String, Object> map = new HashMap<>();
        map.put("clientId", 29);
        rabbitMQPublisherService.send("new message", "exchange", "key", map);
        Mockito.verify(amqpTemplate, Mockito.times(1)).convertAndSend(eq("exchange"), eq("key"), anyString(), any());
    }

}

package tests.unit.services;


import com.everwell.userservice.exceptions.NotFoundException;
import com.everwell.userservice.exceptions.ValidationException;
import com.everwell.userservice.models.db.Client;
import com.everwell.userservice.models.http.requests.ClientRequest;
import com.everwell.userservice.models.http.responses.ClientResponse;
import com.everwell.userservice.repositories.ClientRepository;
import com.everwell.userservice.services.AuthenticationService;
import com.everwell.userservice.services.impl.ClientServiceImpl;
import com.everwell.userservice.utils.Utils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import tests.BaseTest;

import java.text.ParseException;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ClientServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    private ClientServiceImpl clientService;

    @Mock
    private ClientRepository clientRepository;

    @Mock
    private AuthenticationService authenticationService;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testRegisterClient() throws ParseException {
        ClientRequest request = new ClientRequest("TestClient", "password");
        when(clientRepository.findClientByName(any())).thenReturn(null);
        ClientResponse response = clientService.registerClient(request);

        Assert.assertEquals(request.getName(), response.getName());
        verify(clientRepository, Mockito.times(1)).save(any());
    }

    @Test(expected = ValidationException.class)
    public void testRegisterClientAlreadyExists() throws ParseException {
        Client client = new Client(1L, "TestClient", "password", Utils.convertStringToDate("01-09-2020 00:00:00"));
        ClientRequest request = new ClientRequest("TestClient", "password");
        when(clientRepository.findClientByName(any())).thenReturn(client);
        clientService.registerClient(request);
        verify(clientRepository, Mockito.times(0)).save(any());
    }

    @Test
    public void testGetClientDetails() throws ParseException {
        Client client = new Client(1L, "TestClient", "password", Utils.convertStringToDate("01-09-2020 00:00:00"));
        when(clientRepository.findById(any())).thenReturn(Optional.of(client));
        when(authenticationService.generateToken(any())).thenReturn("TestSecureToken");
        ClientResponse response = clientService.getClientDetails(1L);

        Assert.assertEquals(client.getName(), response.getName());
        Assert.assertEquals(client.getId(), response.getId());
        Assert.assertNotNull(response.getAuthToken());
    }

    @Test
    public void testGetClientById() throws ParseException {
        Client client = new Client(1L, "TestClient", "password", Utils.convertStringToDate("01-09-2020 00:00:00"));
        when(clientRepository.findById(any())).thenReturn(Optional.of(client));
        Client actualClient = clientService.getClientById(1L);

        Assert.assertEquals(client.getName(), actualClient.getName());
    }

    @Test(expected = NotFoundException.class)
    public void testGetClientByIdNotFound() {
        when(clientRepository.findById(any())).thenReturn(Optional.empty());
        clientService.getClientById(1L);
    }

    @Test(expected = ValidationException.class)
    public void testGetClientByIdWithIdNull() {
        when(clientRepository.findById(any())).thenReturn(Optional.empty());
        clientService.getClientById(null);
    }
}
package com.everwell.userservice.controllers;

import com.everwell.userservice.models.dtos.Response;
import com.everwell.userservice.models.http.requests.ClientRequest;
import com.everwell.userservice.models.http.responses.ClientResponse;
import com.everwell.userservice.services.ClientService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.everwell.userservice.constants.Constants.X_US_CLIENT_ID;
@RestController
public class ClientController {

    private static Logger LOGGER = LoggerFactory.getLogger(ClientController.class);

    @Autowired
    private ClientService clientService;

    @RequestMapping(value = "/v1/client",  method = RequestMethod.POST)
    @ApiOperation(
            value = "Register new client",
            notes = "Client will get registered with the given credentials"
    )
    public ResponseEntity<Response<ClientResponse>> registerClient(@RequestBody ClientRequest clientRequest) {
        LOGGER.info("[registerClient request received ]"+clientRequest);
        clientRequest.validate();
        ClientResponse clientResponse = clientService.registerClient(clientRequest);
        Response<ClientResponse> response = new Response<>(clientResponse, "client registered successfully");
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @ApiOperation(
            value = "Get client details",
            notes = "Generates a new access token and will fetch the client details with the token"
    )
    @RequestMapping(value = "/v1/client", method = RequestMethod.GET)
    public ResponseEntity<Response<ClientResponse>> getClientDetails(@RequestHeader(name = X_US_CLIENT_ID) Long id) {
        LOGGER.info("[getClientDetails] request received for client id "+id);
        ClientResponse clientResponse = clientService.getClientDetails(id);
        Response<ClientResponse> response = new Response<>(clientResponse, "client details fetched successfully");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}

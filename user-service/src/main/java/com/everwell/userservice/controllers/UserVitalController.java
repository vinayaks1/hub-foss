package com.everwell.userservice.controllers;

import com.everwell.userservice.models.dtos.Response;
import com.everwell.userservice.models.dtos.vitals.VitalDetailsDto;
import com.everwell.userservice.models.dtos.vitals.VitalGoalDto;
import com.everwell.userservice.models.http.requests.AddVitalDetailsRequest;
import com.everwell.userservice.models.http.requests.VitalDetailsRequest;
import com.everwell.userservice.models.http.requests.VitalGoalRequest;
import com.everwell.userservice.services.UserService;
import com.everwell.userservice.services.UserVitalService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.everwell.userservice.constants.Constants.X_US_CLIENT_ID;

@RestController
public class UserVitalController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserVitalController.class);

    @Autowired
    private UserVitalService userVitalService;

    @Autowired
    private UserService userService;

    @ApiOperation(
            value = "Get all User Vitals Goal Details",
            notes = "Used to get all the user vitals goals details for an userId"
    )
    @GetMapping(value = "/v1/users/{userId}/vitals-goal")
    public ResponseEntity<Response<List<VitalGoalDto>>> getVitalsGoalByUserId(@PathVariable("userId") Long userId, @RequestHeader(name = X_US_CLIENT_ID) Long clientId)
    {
        LOGGER.info("[getVitalsGoalByUserId] request received for userId : " + userId);
        userService.getUser(userId, clientId);
        List<VitalGoalDto> vitalGoalDtoList = userVitalService.getVitalsGoalByUserId(userId);
        Response<List<VitalGoalDto>> response = new Response<>(true, vitalGoalDtoList);
        LOGGER.info("[getVitalsGoalByUserId] response generated : "+response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(
            value = "Save User Vitals Goal Details",
            notes = "Used to save the user vital goal details for an userId"
    )
    @PostMapping(value = "/v1/users/{userId}/vitals-goal")
    public ResponseEntity<Response<String>> saveUserVitalsGoal(@PathVariable("userId") Long userId, @RequestBody VitalGoalRequest vitalGoalRequest, @RequestHeader(name = X_US_CLIENT_ID) Long clientId)
    {
        LOGGER.info("[saveUserVitalsGoal] request received for userId : " + userId + " with body : " + vitalGoalRequest);
        vitalGoalRequest.validate();
        userService.getUser(userId, clientId);
        userVitalService.saveUserVitalsGoal(userId, vitalGoalRequest);
        return new ResponseEntity<>(new Response<>(true, "User vitals goal details saved successfully"), HttpStatus.CREATED);
    }

    @ApiOperation(
            value = "Update User Vitals Goal Details",
            notes = "Used to update the user vitals goals details for a userId"
    )
    @PutMapping(value = "/v1/users/{userId}/vitals-goal")
    public ResponseEntity<Response<String>> updateUserVitalsGoal(@PathVariable("userId") Long userId,  @RequestBody VitalGoalRequest vitalGoalRequest,  @RequestHeader(name = X_US_CLIENT_ID) Long clientId)
    {
        LOGGER.info("[updateUserVitalsGoal] request received for userId : " + userId + " with body : " + vitalGoalRequest);
        vitalGoalRequest.validate();
        userService.getUser(userId, clientId);
        userVitalService.updateUserVitalsGoal(userId, vitalGoalRequest);
        return new ResponseEntity<>(new Response<>(true, "User vitals goal details updated successfully"), HttpStatus.OK);
    }

    @ApiOperation(
            value = "Delete all User Vitals Goal Details",
            notes = "Used to delete all the user vitals goals details for a userId"
    )
    @DeleteMapping(value = "/v1/users/{userId}/vitals-goal")
    public ResponseEntity<Response<String>> deleteUserVitalsGoal(@PathVariable("userId") Long userId, @RequestHeader(name = X_US_CLIENT_ID) Long clientId)
    {
        LOGGER.info("[deleteUserVitalsGoal] request received for userId : " + userId);
        userService.getUser(userId, clientId);
        userVitalService.deleteUserVitalsGoal(userId);
        return new ResponseEntity<>(new Response<>(true, "User vitals goal details deleted successfully"), HttpStatus.OK);
    }

    @ApiOperation(
            value = "Get all User Vitals Details",
            notes = "Used to get all the user vitals details for a list of userIds"
    )
    @PostMapping(value = "/v1/users/vitals/bulk")
    public ResponseEntity<Response<VitalDetailsDto>> getVitalsDetailsByUserId(@RequestBody VitalDetailsRequest vitalDetailsRequest, @RequestHeader(name = X_US_CLIENT_ID) Long clientId)
    {
        // TODO fetch user-relations if includeUserRelations is true
        LOGGER.info("[getVitalsDetailsByUserId] request accepted : " + vitalDetailsRequest);
        vitalDetailsRequest.validate();
        userService.validateUserIdList(vitalDetailsRequest.getUserIds(), clientId);
        VitalDetailsDto vitalGoalDtoList = userVitalService.getUserVitalsDetails(vitalDetailsRequest);
        Response<VitalDetailsDto> response = new Response<>(true, vitalGoalDtoList);
        LOGGER.info("[getVitalsDetailsByUserId] response generated : "+response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(
            value = "Save User Vitals Details",
            notes = "Used to save the user vital details for an userId"
    )
    @PostMapping(value = "/v1/users/{userId}/vitals")
    public ResponseEntity<Response<String>> saveUserVitalsDetails(@PathVariable("userId") Long userId, @RequestBody AddVitalDetailsRequest addVitalDetailsRequest, @RequestHeader(name = X_US_CLIENT_ID) Long clientId)
    {
        LOGGER.info("[saveUserVitalsDetails] request accepted for userId : " + userId + " request : " + addVitalDetailsRequest);
        addVitalDetailsRequest.validate();
        userService.getUser(userId, clientId);
        userVitalService.saveUserVitalsDetails(userId, addVitalDetailsRequest);
        return new ResponseEntity<>(new Response<>(true, "User vitals details saved successfully"), HttpStatus.CREATED);
    }
}

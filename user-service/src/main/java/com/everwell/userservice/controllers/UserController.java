package com.everwell.userservice.controllers;

import com.everwell.userservice.exceptions.NotFoundException;
import com.everwell.userservice.models.dtos.*;
import com.everwell.userservice.services.UserService;
import com.everwell.userservice.utils.Utils;
import com.everwell.userservice.exceptions.ValidationException;
import com.everwell.userservice.models.dtos.*;
import com.everwell.userservice.services.UserService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import static com.everwell.userservice.constants.Constants.X_US_CLIENT_ID;

@RestController
public class UserController
{
    @Autowired
    private UserService userService;

    private static Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @ApiOperation(
            value = "Get User Details By User Id",
            notes = "Used to get user details by User Id"
    )
    @RequestMapping(value = "/v1/user", method = RequestMethod.GET)
    public ResponseEntity<Response<UserResponse>> getUserById(@RequestParam(value = "id", required = true) Long id, @RequestHeader(name = X_US_CLIENT_ID) Long clientId) throws Exception
    {
        UserResponse user = userService.get(id, clientId);

        if(null == user)
        {
            throw new NotFoundException("User Not Found for given Id");
        }

        return new ResponseEntity<>(new Response<>(user,"data fetched successfully"), HttpStatus.OK);
    }

    @ApiOperation(
            value = "Get Users by Primary Phonenumber",
            notes = "Used to get list of active users who have the given phone number as their primary number"
    )
    @RequestMapping(value = "/v1/users/primaryPhone/{phoneNumber}", method = RequestMethod.GET)
    public ResponseEntity<Response<List<UserResponse>>> getUsersByPrimaryPhone(@PathVariable("phoneNumber") String phoneNumber, @RequestHeader(name = X_US_CLIENT_ID) Long clientId) throws Exception
    {
        return new ResponseEntity<>(new Response<>(userService.getByPrimaryPhone(phoneNumber, clientId),"data fetched successfully"), HttpStatus.OK);
    }

    @ApiOperation(
            value = "Get Possible duplicates for given inputs",
            notes = "Used to get list of active users who match the given criteria"
    )
    @RequestMapping(value = "/v1/users/duplicates", method = RequestMethod.POST)
    public ResponseEntity<Response<List<UserResponse>>> fetchPossibleDuplicates(@RequestBody FetchDuplicatesRequest request, @RequestHeader(name = X_US_CLIENT_ID) Long clientId) throws Exception
    {
        return new ResponseEntity<>(new Response<>(userService.fetchDuplicates(request, clientId), "data fetched successfully"), HttpStatus.OK);
    }

    @ApiOperation(
            value = "Add New User",
            notes = "Used to add a new user"
    )
    @RequestMapping(value = "/v1/users", method = RequestMethod.POST)
    public ResponseEntity<Response<UserResponse>> add(@RequestBody AddUserRequest request, @RequestHeader(name = X_US_CLIENT_ID) Long clientId) throws Exception
    {
        return new ResponseEntity<>(new Response<>(userService.create(request, clientId, false), "user created successfully"), HttpStatus.OK);
    }

    @ApiOperation(
            value = "Update User",
            notes = "Used to update user details"
    )
    @RequestMapping(value = "/v1/users", method = RequestMethod.PUT)
    public ResponseEntity<Response<String>> update(@RequestBody UpdateUserRequest request, @RequestHeader(name = X_US_CLIENT_ID) Long clientId) throws Exception
    {
        LOGGER.info("[update] update user request " + Utils.asJsonString(request));
        userService.update(request, clientId);
        return new ResponseEntity<>(new Response<>(true,"user updated successfully"), HttpStatus.OK);
    }

    @ApiOperation(
            value = "Delete User",
            notes = "It is used to soft delete the user details associated with an user-id"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "user details deleted successfully"),
            @ApiResponse(code = 404, message = "User with given id not found")
    })
    @DeleteMapping(value = "/v1/users")
    public ResponseEntity<Response<String>> delete(@RequestBody DeleteUserRequest deleteUserRequest, @RequestHeader(name = X_US_CLIENT_ID) Long clientId)
    {
        LOGGER.debug("[delete] delete user request " + deleteUserRequest);
        userService.delete(deleteUserRequest, clientId);
        return new ResponseEntity<>(new Response<>(true, "user details deleted successfully"), HttpStatus.OK);
    }
           
    @GetMapping(value = "/v1/user/relation")
    public ResponseEntity<Response<List<UserResponse>>> getUserRelations(@RequestParam Long userId, @RequestParam String status) throws Exception {
        LOGGER.info("[getUserRelations] request received for " + userId);
        if (null == userId || userId == 0) {
            throw new ValidationException("user id cannot be empty");
        }
        if (StringUtils.isEmpty(status)) {
            throw new ValidationException("status cannot be empty");
        }
        List<UserResponse> userResponse = userService.getUserRelations(userId, status);
        return new ResponseEntity<>(new Response<>(true, userResponse), HttpStatus.OK);
    }

    @PostMapping(value = "/v1/user/relation")
    public ResponseEntity<Response<String>> addRelation(@RequestBody UserRelationDto userRelationDto) {
        LOGGER.info("[addRelation] request received for " + userRelationDto);
        userRelationDto.validate(userRelationDto);
        userService.addUserRelation(userRelationDto);
        return new ResponseEntity<>(new Response<>(true, "relation added"), HttpStatus.OK);
    }

    @PutMapping(value = "/v1/user/relation")
    public ResponseEntity<Response<String>> updateRelation(@RequestBody UserRelationDto userRelationDto) {
        LOGGER.info("[updateRelation request received for " + userRelationDto);
        userRelationDto.validate(userRelationDto);
        userService.updateUserRelation(userRelationDto);
        return new ResponseEntity<>(new Response<>(true, "Relation updated"), HttpStatus.OK);
    }

    @DeleteMapping(value = "/v1/user/relation")
    public ResponseEntity<Response<String>> deleteRelation(@RequestBody UserRelationDto userRelationDto) {
        LOGGER.info("[deleteRelationrequest received for " + userRelationDto);
        userRelationDto.validate(userRelationDto);
        userService.deleteUserRelation(userRelationDto);
        return new ResponseEntity<>(new Response<>(true, "Relation deleted"), HttpStatus.OK);
    }

    @ApiOperation(
            value = "Get User Relations by Primary Phone number of secondary users and Primary User Id",
            notes = "Get User Relations by Primary Phone number of secondary users and Primary User Id"
    )
    @RequestMapping(value = "/v1/users/relation", method = RequestMethod.GET)
    public ResponseEntity<Response<List<UserResponse>>> getUserRelationsByPrimaryPhone(@RequestParam("phoneNumber") String phoneNumber, @RequestParam("primaryUserId") Long primaryUserId) throws Exception
    {
        if (StringUtils.isEmpty(phoneNumber)) {
            throw new ValidationException("phone number cannot be empty!");
        }
        if (null == primaryUserId || primaryUserId == 0) {
            throw new ValidationException("primary user id cannot be empty");
        }
        return new ResponseEntity<>(new Response<>(userService.getUserRelationsPrimaryPhone(phoneNumber, primaryUserId),"data fetched successfully"), HttpStatus.OK);
    }
    @ApiOperation(   
            value = "filter users",
            notes = "Used to filter users"
    )
    @RequestMapping(value = "/v1/users/search", method = RequestMethod.POST)
    public ResponseEntity<Response<UserResponse>> search(@RequestBody FilterUserRequest request, @RequestHeader(name = X_US_CLIENT_ID) Long clientId) throws Exception
    {
        LOGGER.info("[User Search Request Received]" + Utils.asJsonString(request));
        request.validate();
        UserResponse user = userService.search(request, clientId);

        if(StringUtils.isEmpty(request.mobile) && StringUtils.isEmpty(request.healthId) && StringUtils.isEmpty(request.healthAddress)) {
            return new ResponseEntity<>(new Response<>(false,null,"no verified identifer was specified"), HttpStatus.OK);
        }

        if(null == user) {
            return new ResponseEntity<>(new Response<>(false,null,"more than one definitive match for the given request"), HttpStatus.OK);
        }
        return new ResponseEntity<>(new Response<>(user, "data fetched successfully"), HttpStatus.OK);
    }
}

package com.everwell.userservice.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidationUtils
{
    public static boolean isValidNumber(String string)
    {
        Pattern pattern = Pattern.compile("^[0-9]+$");
        Matcher matcher = pattern.matcher(string);
        return matcher.find();
    }
}

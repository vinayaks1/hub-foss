package com.everwell.userservice.utils;

import io.sentry.event.EventBuilder;
import io.sentry.event.helper.BasicRemoteAddressResolver;
import io.sentry.event.interfaces.ExceptionInterface;
import io.sentry.event.interfaces.HttpInterface;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;
import java.io.InputStream;

@Component
public class SentryUtils {

    private static String releaseVersion;

    @Value("${userservice.sentry.release.version}")
    public void setVersion(String version) {
        this.releaseVersion = version;
    }

    public static EventBuilder eventBuilder(Exception ex, WebRequest request, InputStream inputStream) throws IOException {
        EventBuilder builder = new EventBuilder()
                .withSentryInterface(new ExceptionInterface(ex))
                .withSentryInterface(new HttpInterface(((ServletWebRequest) request).getRequest(), new BasicRemoteAddressResolver(), Utils.readRequestBody(inputStream)))
                .withRelease(releaseVersion);

        return builder;


    }
}

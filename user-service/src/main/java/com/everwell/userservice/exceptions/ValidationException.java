package com.everwell.userservice.exceptions;

public class ValidationException extends RuntimeException {

    public ValidationException(String exception) {
        super(exception);
    }
}

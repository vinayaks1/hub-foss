package com.everwell.userservice.filters;

import com.everwell.userservice.exceptions.NotFoundException;
import com.everwell.userservice.exceptions.ValidationException;
import com.everwell.userservice.models.db.Client;
import com.everwell.userservice.services.AuthenticationService;
import com.everwell.userservice.services.ClientService;
import com.everwell.userservice.utils.Utils;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.UnsupportedJwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;

import static com.everwell.userservice.constants.Constants.X_US_AUTH_TOKEN;
import static com.everwell.userservice.constants.Constants.X_US_CLIENT_ID;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private ClientService clientService;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain)
            throws ServletException, IOException {

        final String httpServletRequestToken = httpServletRequest.getHeader(X_US_AUTH_TOKEN);
        final String httpServletRequestClientId = httpServletRequest.getHeader(X_US_CLIENT_ID);

        if (!StringUtils.isEmpty(httpServletRequestClientId) && !StringUtils.isEmpty(httpServletRequestToken)
                && null == SecurityContextHolder.getContext().getAuthentication()) {

            try{
                Client client = clientService.getClientById(Long.parseLong(httpServletRequestClientId));
                if(authenticationService.isValidToken(httpServletRequestToken, client)){
                    UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(client, null, Collections.emptyList());
                    usernamePasswordAuthenticationToken
                            .setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
                    SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
                }

            } catch (IllegalArgumentException | UnsupportedJwtException | ValidationException e) {
                Utils.writeCustomResponseToOutputStream(httpServletResponse, e, HttpStatus.BAD_REQUEST);
                return;
            } catch (ExpiredJwtException e) {
                Utils.writeCustomResponseToOutputStream(httpServletResponse, e, HttpStatus.UNAUTHORIZED);
                return;
            } catch (NotFoundException e) {
                Utils.writeCustomResponseToOutputStream(httpServletResponse, e, HttpStatus.NOT_FOUND);
                return;
            } catch (Exception e) {
                Utils.writeCustomResponseToOutputStream(httpServletResponse, e, HttpStatus.INTERNAL_SERVER_ERROR);
                return;
            }
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);

    }
}

package com.everwell.userservice.repositories.impl;


import com.everwell.userservice.enums.TimescaleQueryFieldEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

import static com.everwell.userservice.constants.Constants.TIMESCALE_JDBC_BEAN_NAME;


@Repository
public class UserVitalTimescaleRepositoryImpl {

    @Autowired
    @Qualifier(TIMESCALE_JDBC_BEAN_NAME)
    private NamedParameterJdbcTemplate timescaleNamedParameterJdbcTemplate;

    public SqlParameterSource constructSqlParameterSource(List<Long> userIdList, List<Long> vitalIdList, Timestamp startTimestamp, Timestamp endTimestamp) {
        return  new MapSqlParameterSource()
                .addValue(TimescaleQueryFieldEnum.START_TIMESTAMP.getFieldName(), startTimestamp)
                .addValue(TimescaleQueryFieldEnum.END_TIMESTAMP.getFieldName(), endTimestamp)
                .addValue(TimescaleQueryFieldEnum.USER_ID_LIST.getFieldName(), userIdList)
                .addValue(TimescaleQueryFieldEnum.VITAL_ID_LIST.getFieldName(), vitalIdList);
    }

    public <T> List <T> processQueryForResult(String query, List<Long> userIdList, List<Long> vitalIdList,
                                              Timestamp startTimestamp, Timestamp endTimestamp, RowMapper<T> rowMapper) {
        SqlParameterSource parameters = constructSqlParameterSource(userIdList, vitalIdList, startTimestamp, endTimestamp);
        return timescaleNamedParameterJdbcTemplate.query(query, parameters, rowMapper);
    }
}

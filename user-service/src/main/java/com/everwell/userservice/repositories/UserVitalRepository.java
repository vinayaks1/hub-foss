package com.everwell.userservice.repositories;

import com.everwell.userservice.models.db.UserVitalGoal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserVitalRepository extends JpaRepository<UserVitalGoal, Long> {
    List<UserVitalGoal> findAllByUserId(Long userId);

    List<UserVitalGoal> findByUserIdAndVitalId(Long userId, Long vitalId);

    void deleteAllByUserId(Long userId);

    List<UserVitalGoal> findByUserIdInAndVitalIdIn(List<Long> userIdList, List<Long> vitalIdList);
}

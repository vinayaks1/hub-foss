package com.everwell.userservice.repositories;

import com.everwell.userservice.models.db.UserRelation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRelationRepository extends JpaRepository<UserRelation, Long> {

    List<UserRelation> findAllByPrimaryUserIdAndStatus(Long primaryUserId, Integer status);

    List<UserRelation> findAllByPrimaryUserId(Long primaryUserId);

    UserRelation findByPrimaryUserIdAndSecondaryUserId(Long primaryUserId, Long secondaryUserId);
}

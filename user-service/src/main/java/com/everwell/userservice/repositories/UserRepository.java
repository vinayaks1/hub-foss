package com.everwell.userservice.repositories;

import com.everwell.userservice.models.db.User;
import com.everwell.userservice.models.dtos.UserCompleteDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long>
{
    User findByIdAndIsDeletedAndClientId(Long id, boolean isDeleted, Long clientId);

    List<User> findAllByIdInAndIsDeletedAndClientId(List<Long> userIds, boolean isDeleted, Long clientId);

    List<User> findAllByIdInAndIsDeletedFalseAndGenderAndClientId(List<Long> userIds, String gender, Long clientId);

    @Query(value = "select new " + "com.everwell.userservice.models.dtos.UserCompleteDto(um.id, um.createdAt, um.isPrimary, um.isVerified, um.number, um.stoppedAt, um.updatedAt, um.userId, um.owner, " +
            "ue.id, ue.createdAt, ue.emailId, ue.isPrimary, ue.isVerified, ue.stoppedAt, ue.updatedAt, ue.userId, ua.id, ua.area, " +
            "ua.landmark, ua.taluka, ua.town, ua.ward, ua.userId, ucp.id, ucp.address, ucp.name, ucp.phone, ucp.userId) " +
            "from User as u " +
            "left join UserMobile um on u.id = um.userId " +
            "left join UserEmail ue on u.id = ue.userId " +
            "left join UserAddress ua on u.id = ua.userId " +
            "left join UserContactPerson ucp on u.id = ucp.userId where u.id = :userId and u.clientId = :clientId and ue.stoppedAt is null and um.stoppedAt is null", nativeQuery = false)
    List<UserCompleteDto> getAllUserDetailByUserId(Long userId, Long clientId);
}

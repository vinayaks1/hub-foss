package com.everwell.userservice.repositories;

import com.everwell.userservice.models.db.ABDMConsent;
import com.everwell.userservice.models.db.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ABDMConsentRepository extends JpaRepository<ABDMConsent, Long>
{
    ABDMConsent findConsentByConsentId(String consentId);
}

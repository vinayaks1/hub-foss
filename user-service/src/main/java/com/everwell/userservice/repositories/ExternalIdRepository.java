package com.everwell.userservice.repositories;

import com.everwell.userservice.models.db.ExternalId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExternalIdRepository extends JpaRepository<ExternalId, Long>
{
    List<ExternalId> findAllByUserId(Long userId);
    List<ExternalId> findAllByValueAndType(String value,String type);
}

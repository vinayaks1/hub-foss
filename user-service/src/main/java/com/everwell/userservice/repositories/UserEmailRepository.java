package com.everwell.userservice.repositories;

import com.everwell.userservice.models.db.UserEmail;
import com.everwell.userservice.models.db.UserMobile;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserEmailRepository extends JpaRepository<UserEmail, Long>
{
    List<UserEmail> findAllByUserIdAndStoppedAtNull(Long userId);

    List<UserEmail> findAllByUserIdInAndStoppedAtNull(List<Long> userIds);
}

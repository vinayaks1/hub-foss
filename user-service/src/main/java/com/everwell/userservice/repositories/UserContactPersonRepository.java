package com.everwell.userservice.repositories;


import com.everwell.userservice.models.db.UserContactPerson;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UserContactPersonRepository extends JpaRepository<UserContactPerson, Long>
{
    UserContactPerson findByUserId(Long userId);
}
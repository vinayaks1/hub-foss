package com.everwell.userservice.repositories;

import com.everwell.userservice.models.db.Verification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VerificationRepository extends JpaRepository<Verification, Long>
{
    List<Verification> findAllByUserIdAndStoppedAtNull(Long userId);
}

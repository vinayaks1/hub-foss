package com.everwell.userservice.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum UserRelationStatus {
    PENDING("PENDING", 0),
    ACCEPT("ACCEPT", 1),
    DENY("DENY", 2),
    ALL("ALL", 3);

    @Getter
    private final String name;

    @Getter
    private final Integer value;

    public static Integer getStatusValue(String name) {
        Integer statusValue = null;
        for(UserRelationStatus code: values()) {
            if(code.name.toLowerCase().equalsIgnoreCase(name)) {
                statusValue = code.value;
                break;
            }
        }
        return statusValue;
    }

    public static String getStatusName(Integer value) {
        String statusName = null;
        for(UserRelationStatus code: values()) {
            if(code.value.equals(value)) {
                statusName = code.name;
                break;
            }
        }
        return statusName;
    }
}

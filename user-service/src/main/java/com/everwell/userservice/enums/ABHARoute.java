package com.everwell.userservice.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ABHARoute
{
    GET_ACCESS_TOKEN("/gateway/v0.5/sessions"),
    VERIFY_OTP_AADHAAR("/api/v1/registration/aadhaar/verifyOTP"),
    VERIFY_OTP_MOBILE("/api/v1/registration/aadhaar/verifyMobileOTP"),
    CREATE_ABHA_PREVERIFIED("/api/v1/registration/aadhaar/createHealthIdWithPreVerified"),
    SEARCH_ABHA_HEALTH_ID("/api/v1/search/searchByHealthId"),
    GENERATE_OTP_AADHAAR("/api/v1/registration/aadhaar/generateOtp"),
    GENERATE_OTP_MOBILE("/api/v1/registration/aadhaar/generateMobileOTP"),
    GENERATE_OTP_AUTH("/api/v1/auth/init"),
    RESEND_OTP_AADHAAR("/api/v1/registration/aadhaar/resendAadhaarOtp"),
    RESEND_OTP_AUTH("/api/v1/auth/resendAuthOTP"),
    VERIFY_OTP_AUTH("/api/v1/auth/confirmWithAadhaarOtp"),
    VERIFY_OTP_WITH_MOBILE("/api/v1/auth/confirmWithMobileOTP"),
    GET_ABHA_PROFILE("/api/v1/account/profile"),
    GET_ABHA_CARD_IMAGE("/api/v1/account/getPngCard"),
    GET_ABHA_QR_IMAGE("/api/v1/account/qrCode"),

    GENERATE_OTP_FOR_MOBILE_EMAIL_LOGIN("/cm/v1/apps/login/mobileEmail/auth-init"),
    VERIFY_OTP_FOR_MOBILE_EMAIL_LOGIN("/cm/v1/apps/login/mobileEmail/pre-Verify"),
    AUTH_CONFIRM_OTP_FOR_MOBILE_EMAIL_LOGIN("/cm/v1/apps/login/mobileEmail/auth-confirm"),
    SHARE_PROFILE("/cm/v1/patients/profile/share"),


    PROVIDERS("/cm/providers/");


    private final String path;
}

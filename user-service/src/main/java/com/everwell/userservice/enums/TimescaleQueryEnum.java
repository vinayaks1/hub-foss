package com.everwell.userservice.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum TimescaleQueryEnum {
    INSERT_DATA_INTO_VITAL_LOGS("INSERT INTO vital_logs VALUES (now(), :userId, :vitalId, :vitalValue)"),
    INSERT_DATA_INTO_VITAL_ASSOCIATED_LOGS("INSERT INTO vital_associated_logs VALUES (now(), :userId, :vitalId, :vitalValue)"),
    GET_VITALS_LOGS("select time, user_id, vital_id, vital_value  from vital_logs where  time >= :startTimestamp and time <= :endTimestamp and user_id IN (:userIds) and vital_id IN (:vitalIds)"),
    GET_VITALS_ASSOCIATED_LOGS("select time, user_id, vital_id, vital_value  from vital_associated_logs where  time >= :startTimestamp and time <= :endTimestamp and user_id IN (:userIds) and vital_id IN (:vitalIds)"),
    GET_VITALS_AGGREGATED_DATA("select  half_hour, user_id, vital_id, total, average  from vital_summary_half_hourly where  half_hour >= :startTimestamp and half_hour <= :endTimestamp and user_id IN (:userIds) and vital_id IN (:vitalIds)");

    @Getter
    private final String query;
}

package com.everwell.userservice.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum VitalValidation {

    VITAL_ID_NOT_FOUND("Vital id does not exists"),
    VITAL_NAME_NOT_FOUND("Vital name does not exists"),
    USER_VITAL_EXISTS("User vital goal already exists"),
    VITAL_ID_OR_VITAL_NAME("Either vitalId or vitalName is required"),
    VITAL_LIST_INVALID("vitals list cannot be empty or null"),
    VITAL_NAME_INVALID("vital name cannot be null or empty"),
    VITAL_VALUE_INVALID("Vital value cannot be null or empty"),
    VITAL_DETAILS_INVALID("Vital details cannot be empty or null"),
    END_DATE_VALIDATION("End date cannot be before start date"),
    USER_ID_LIST_INVALID("userIds cannot be null or empty"),
    VITAL_ID_AND_VITAL_NAME_LIST_INVALID("vitalIds and vitalNames cannot be null or empty! atleast one required"),
    START_DATE_TIME_INVALID("Start date time cannot be null or empty"),
    VITAL_ID_OR_VITAL_NAME_NOT_FOUND("Vital details where vital Id is %s and vital name is %s does not exist"),
    VITAL_ID_NOT_FOUND_WITH_VITAL_ID("Vital id : %s does not exists"),
    VITAL_NAME_NOT_FOUND_WITH_VITAL_NAME("Vital name : %s does not exists");

    @Getter
    private final String message;
}

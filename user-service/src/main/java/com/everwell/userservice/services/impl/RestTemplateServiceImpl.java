package com.everwell.userservice.services.impl;

import com.everwell.userservice.exceptions.ValidationException;
import com.everwell.userservice.models.dtos.Response;
import com.everwell.userservice.services.RestTemplateService;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

import static com.everwell.userservice.constants.ABHAConstants.ABHA_X_TOKEN;


@Service
public class RestTemplateServiceImpl implements RestTemplateService {

    public HttpHeaders headers;

    private RestTemplate restTemplate;

    public RestTemplateServiceImpl() {
        headers = new HttpHeaders();
        restTemplate = new RestTemplate();
    }

    public String generateAccessToken() {
        return "token"; // access token code can be implemented in this function in future
    }

    @Override
    public <T> ResponseEntity<Response<T>> getExchange(String url, String authToken, ParameterizedTypeReference<T> typeReference) {
        return genericExchange(url, HttpMethod.GET, typeReference, null, authToken,null);
    }

    @Override
    public <T> ResponseEntity<Response<T>> getExchange(String url, HttpHeaders extraHeaders, ParameterizedTypeReference<T> typeReference) {
        return genericExchange(url, HttpMethod.GET, typeReference, null, null,extraHeaders);
    }

    @Override
    public <T> ResponseEntity<Response<T>> postExchange(String url, Object requestObj, ParameterizedTypeReference<T> typeReference) {
        return genericExchange(url, HttpMethod.POST, typeReference, requestObj, null,null);
    }

    @Override
    public <T> ResponseEntity<Response<T>> postExchange(String url, Object requestObj, HttpHeaders extraHeaders,ParameterizedTypeReference<T> typeReference) {
        return genericExchange(url, HttpMethod.POST, typeReference, requestObj, null,extraHeaders);
    }

    @Override
    public <T> ResponseEntity<Response<T>> genericExchange(String url, HttpMethod method, ParameterizedTypeReference<T> parameterizedTypeReference, Object requestObject, String authToken, HttpHeaders extraHeaders) {
        int retryCount = 2;
        boolean retry = false;
        ResponseEntity responseEntity = null;
        do {
            try {
                HttpHeaders localHeaders = new HttpHeaders();
                if (null != authToken) {
                    localHeaders.add(ABHA_X_TOKEN, "Bearer " + authToken);
                }
                localHeaders.addAll(headers);
                if(extraHeaders!=null){
                    localHeaders.addAll(extraHeaders);
                }

                HttpEntity<?> request = new HttpEntity<>(requestObject, localHeaders);
                responseEntity = restTemplate.exchange(url, method, request, parameterizedTypeReference);
                retry = false;
                if (responseEntity.getBody() == null) {
                    throw new ValidationException(String.format("[status Code - %d] Response body is empty", responseEntity.getStatusCode().value()));
                }
            } catch (HttpStatusCodeException e) {
                if (e.getStatusCode() == HttpStatus.UNAUTHORIZED) {
                    generateAccessToken();
                    retry = true;
                }
            }
        } while (retry && retryCount-- > 0);
        return responseEntity;
    }

}

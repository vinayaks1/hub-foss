package com.everwell.userservice.services;

import com.everwell.userservice.models.db.Client;
import com.everwell.userservice.models.http.requests.ClientRequest;
import com.everwell.userservice.models.http.responses.ClientResponse;

public interface ClientService {

    ClientResponse registerClient(ClientRequest clientRequest);

    ClientResponse getClientDetails(Long id);

    Client getClientById(Long id);

    Client getClientByToken();

}

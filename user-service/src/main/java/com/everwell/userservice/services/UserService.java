package com.everwell.userservice.services;

import com.everwell.userservice.models.db.User;
import com.everwell.userservice.models.dtos.*;

import java.util.List;

public interface UserService
{
    UserResponse get(Long id, Long clientId) throws Exception;

    List<UserResponse> getByPrimaryPhone(String phoneNumber, Long clientId) throws Exception;

    UserResponse create(AddUserRequest addUserRequest, Long clientId, boolean ackRequired) throws Exception;

    void delete(DeleteUserRequest deleteUserRequest, Long clientId);

    void update(UpdateUserRequest updateUserRequest, Long clientId) throws Exception;

    void updateMobiles(UpdateMobileRequest updateMobileRequest, Long clientId);

    void updateEmails(UpdateEmailRequest updateEmailRequest, Long clientId);

    List<UserResponse> fetchDuplicates(FetchDuplicatesRequest fetchDuplicatesRequest, Long clientId) throws Exception;

    User getUser(Long id, Long clientId);

    void validateUserIdList(List<Long> userIdList, Long clientId);
    
    List<UserResponse> getUserRelations(Long userId, String status) throws Exception;

    void addUserRelation(UserRelationDto addUserRelationDto);

    void deleteUserRelation(UserRelationDto addUserRelationDto);

    void updateUserRelation(UserRelationDto userRelationDto);

    List<UserResponse> getUserRelationsPrimaryPhone(String phoneNumber, Long primaryUserId) throws Exception;
    UserResponse search(FilterUserRequest filterUserRequest, Long clientId) throws Exception;
}

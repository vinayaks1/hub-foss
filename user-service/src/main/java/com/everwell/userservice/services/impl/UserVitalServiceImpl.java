package com.everwell.userservice.services.impl;

import com.everwell.userservice.enums.VitalValidation;
import com.everwell.userservice.exceptions.NotFoundException;
import com.everwell.userservice.exceptions.ValidationException;
import com.everwell.userservice.models.db.UserVitalGoal;
import com.everwell.userservice.models.db.Vital;
import com.everwell.userservice.models.db.timescale.VitalAssociatedLogs;
import com.everwell.userservice.models.db.timescale.VitalLogs;
import com.everwell.userservice.models.dtos.vitals.*;
import com.everwell.userservice.models.http.requests.AddVitalDetailsRequest;
import com.everwell.userservice.models.http.requests.VitalDetailsRequest;
import com.everwell.userservice.models.http.requests.VitalGoalRequest;
import com.everwell.userservice.models.dtos.vitals.UserVitalGoalDto;
import com.everwell.userservice.models.dtos.vitals.VitalDetailsDto;
import com.everwell.userservice.models.http.responses.VitalResponse;
import com.everwell.userservice.repositories.*;
import com.everwell.userservice.services.UserVitalService;
import com.everwell.userservice.services.VitalService;
import com.everwell.userservice.utils.Utils;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserVitalServiceImpl implements UserVitalService {

    @Autowired
    private UserVitalRepository userVitalRepository;

    @Autowired
    private VitalLogsRepository vitalLogsRepository;

    @Autowired
    private VitalAssociatedLogsRepository vitalAssociatedLogsRepository;

    @Autowired
    private VitalAggregateRepository vitalAggregateRepository;

    @Autowired
    private VitalService vitalService;


    @Override
    public List<VitalGoalDto> getVitalsGoalByUserId(Long userId) {
        List<VitalGoalDto> vitalGoalDtoList = new ArrayList<>();
        List<UserVitalGoal> userVitalGoalList = userVitalRepository.findAllByUserId(userId);
        Map<Long, String> vitalIdToVitalNameMap = vitalService.getAllVitalsResponse()
                .stream()
                .collect(Collectors.toMap(VitalResponse::getId, VitalResponse::getName));
        userVitalGoalList.forEach(userVitalGoal -> {
            Long vitalId = userVitalGoal.getVitalId();
            vitalGoalDtoList.add(new VitalGoalDto(vitalId, vitalIdToVitalNameMap.get(vitalId), userVitalGoal.getValue(), userVitalGoal.getStartDate(), userVitalGoal.getEndDate()));
        });
        return vitalGoalDtoList;
    }

    @Override
    public List<UserVitalGoalDto> getVitalsGoalsForUserIdListAndVitalIdList(List<Long> userIdList, List<Long> vitalIdList) {
        List<UserVitalGoalDto> userVitalGoalDtoList = new ArrayList<>();
        List<UserVitalGoal> userVitalGoalList = userVitalRepository.findByUserIdInAndVitalIdIn(userIdList, vitalIdList);
        userVitalGoalList.forEach( userVitalGoal -> {
            userVitalGoalDtoList.add(new UserVitalGoalDto(userVitalGoal.getUserId(), userVitalGoal.getVitalId(), userVitalGoal.getValue(), userVitalGoal.getStartDate(), userVitalGoal.getEndDate()));
        });
        return userVitalGoalDtoList;
    }

    @Override
    public void saveUserVitalsGoal(Long userId, VitalGoalRequest vitalGoalRequest) {
        Long vitalId = vitalService.processAndFetchVitalId(vitalGoalRequest.getVitalId(), vitalGoalRequest.getVitalName());
        List<UserVitalGoal> userVitalGoalList = userVitalRepository.findByUserIdAndVitalId(userId, vitalId);
        if (!CollectionUtils.isEmpty(userVitalGoalList)) {
            throw new ValidationException(VitalValidation.USER_VITAL_EXISTS.getMessage());
        }
        UserVitalGoal userVitalGoal = new UserVitalGoal(userId, vitalId, vitalGoalRequest.getValue(), LocalDateTime.now(), null);
        userVitalRepository.save(userVitalGoal);
    }

    @Override
    public void updateUserVitalsGoal(Long userId, VitalGoalRequest vitalGoalRequest) {
        Long vitalId = vitalService.processAndFetchVitalId(vitalGoalRequest.getVitalId(), vitalGoalRequest.getVitalName());
        List<UserVitalGoal> userVitalGoalList = userVitalRepository.findByUserIdAndVitalId(userId, vitalId);
        Optional<UserVitalGoal> userVitalGoal = userVitalGoalList
                .stream()
                .filter(i->null == i.getEndDate())
                .findFirst();
        if (!userVitalGoal.isPresent()) {
           UserVitalGoal newUserVitalGoal = new UserVitalGoal(userId, vitalId, vitalGoalRequest.getValue(), LocalDateTime.now(), null);
           userVitalRepository.save(newUserVitalGoal);
        } else {
            Float goalValue = vitalGoalRequest.getValue();
            List<UserVitalGoal> updatedUserVitalGoalList = new ArrayList<>();
            if (null != goalValue)  {
               userVitalGoal.get().setEndDate(LocalDateTime.now());
               updatedUserVitalGoalList.add(userVitalGoal.get());
               updatedUserVitalGoalList.add(new UserVitalGoal(userId, vitalId, goalValue, LocalDateTime.now(), null));
               userVitalRepository.saveAll(updatedUserVitalGoalList);
           }
        }
    }

    @Override
    @Transactional
    public void deleteUserVitalsGoal(Long userId) {
        userVitalRepository.deleteAllByUserId(userId);
    }

    private void processAssociations(Long vitalId,  Map<Long, Boolean> vitalIdToAssociationMap, List<Long> associatedVitalIdList, List<Long> singleVitalIdList) {
        boolean containsAssociations = vitalIdToAssociationMap.get(vitalId);
        if (containsAssociations) {
            associatedVitalIdList.add(vitalId);
        } else {
            singleVitalIdList.add(vitalId);
        }
    }

    private VitalTypeListDto processAndFetchVitalAssociations(BiMap<Long, String> vitalBiMap, Map<Long, Boolean> vitalIdToAssociationMap, List<Long> vitalIdList, List<String> vitalNameList) {
        List<Long> associatedVitalIdList = new ArrayList<>();
        List<Long> singleVitalIdList = new ArrayList<>();
        List<Long> formattedVitalIdList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(vitalIdList)) {
            vitalIdList.forEach(vitalId-> {
                if (!vitalIdToAssociationMap.containsKey(vitalId)) {
                    throw new NotFoundException(String.format(VitalValidation.VITAL_ID_NOT_FOUND_WITH_VITAL_ID.getMessage(),vitalId));
                }
                processAssociations(vitalId, vitalIdToAssociationMap, associatedVitalIdList, singleVitalIdList);
            });
            formattedVitalIdList = new ArrayList<>(vitalIdList);
        } else if (!CollectionUtils.isEmpty(vitalNameList)) {
            List<Long> combinedVitalIdList = new ArrayList<>();
            vitalNameList.forEach(vitalName -> {
                Long vitalId = vitalBiMap.inverse().get(vitalName);
                if (!vitalIdToAssociationMap.containsKey(vitalId)) {
                    throw new NotFoundException(String.format(VitalValidation.VITAL_NAME_NOT_FOUND_WITH_VITAL_NAME.getMessage(), vitalName));
                }
                processAssociations(vitalId, vitalIdToAssociationMap, associatedVitalIdList, singleVitalIdList);
                combinedVitalIdList.add(vitalId);
            });
            formattedVitalIdList = new ArrayList<>(combinedVitalIdList);
        } else {
            throw new ValidationException(VitalValidation.VITAL_ID_AND_VITAL_NAME_LIST_INVALID.getMessage());
        }
        return new VitalTypeListDto(associatedVitalIdList, singleVitalIdList, formattedVitalIdList);
    }

    private Map<Long, List<VitalsAggregatedTimeData>> getAggregatedVitalsData(List<Long> userIdList, VitalTypeListDto vitalTypeListDto, Timestamp startDate, Timestamp endDate) {
        return vitalTypeListDto.getSingleValuedVitalIdList().isEmpty()
                ? new HashMap<>()
                : vitalAggregateRepository.fetchData(userIdList, vitalTypeListDto.getSingleValuedVitalIdList(), startDate, endDate)
                .stream()
                .collect(Collectors.groupingBy(VitalsAggregatedTimeData::getVitalId));
    }

    private  Map<Long, List<VitalsTimeData>> getVitalLogs(List<Long> userIdList, VitalTypeListDto vitalTypeListDto, Timestamp startDate, Timestamp endDate) {
        List<VitalsTimeData> vitalLogsDataList = vitalTypeListDto.getSingleValuedVitalIdList().isEmpty()
                ? new ArrayList<>()
                : vitalLogsRepository.fetchLogs(userIdList, vitalTypeListDto.getSingleValuedVitalIdList(), startDate, endDate);
        List<VitalsTimeData> vitalAssociatedDataList = vitalTypeListDto.getAssociatedVitalIdList().isEmpty()
                ? new ArrayList<>()
                : vitalAssociatedLogsRepository.fetchLogs(userIdList, vitalTypeListDto.getAssociatedVitalIdList(), startDate, endDate);
        vitalLogsDataList.addAll(vitalAssociatedDataList);
        return vitalLogsDataList
                .stream()
                .collect(Collectors.groupingBy(VitalsTimeData::getVitalId));
    }

    @Override
    public VitalDetailsDto getUserVitalsDetails(VitalDetailsRequest vitalDetailsRequest) {
        LocalDateTime startDateTime =  Utils.convertStringToLocalDateTime(vitalDetailsRequest.getStartDateTime());
        LocalDateTime endDateTime = StringUtils.isEmpty(vitalDetailsRequest.getEndDateTime()) ? LocalDateTime.now() : Utils.convertStringToLocalDateTime(vitalDetailsRequest.getEndDateTime());
        if (endDateTime.isBefore(startDateTime)) {
            throw new ValidationException(VitalValidation.END_DATE_VALIDATION.getMessage());
        }
        List<Vital> vitals = vitalService.getAllVitals();
        List<Long> userIdList = vitalDetailsRequest.getUserIds();
        BiMap<Long, String> vitalBiMap = HashBiMap.create();
        Map<Long, Boolean> vitalIdToAssociationMap = new HashMap<>();
        vitals.forEach(vital -> {
            vitalBiMap.put(vital.getId(), vital.getName());
            vitalIdToAssociationMap.put(vital.getId(), vital.isContainsAssociations());
        });
        VitalTypeListDto vitalTypeListDto = processAndFetchVitalAssociations(vitalBiMap, vitalIdToAssociationMap, vitalDetailsRequest.getVitalIds(), vitalDetailsRequest.getVitalNames());
        Timestamp startTimestamp  =  Timestamp.valueOf(startDateTime);
        Timestamp endTimestamp = Timestamp.valueOf(endDateTime);
        Map<Long, List<VitalsAggregatedTimeData>> vitalIdToVitalsAggregatedTimeDataMap = vitalDetailsRequest.isIncludeAggregatedData() ? getAggregatedVitalsData(userIdList, vitalTypeListDto, startTimestamp, endTimestamp) : new HashMap<>();
        Map<Long, List<VitalsTimeData>> vitalIdToVitalLogsDataMap = vitalDetailsRequest.isIncludeLogs() ? getVitalLogs(userIdList, vitalTypeListDto, startTimestamp, endTimestamp) : new HashMap<>();
        List<VitalDetailsDto.VitalsAggregatedData> aggregatedData = new ArrayList<>();
        List<VitalDetailsDto.VitalsData> logData = new ArrayList<>();
        for (Long vitalId : vitalTypeListDto.getVitalIdList()) {
            if (vitalIdToVitalLogsDataMap.containsKey(vitalId)) {
                logData.add(new VitalDetailsDto.VitalsData(vitalBiMap.get(vitalId), vitalId, vitalIdToVitalLogsDataMap.get(vitalId)));
            }
            if (vitalIdToVitalsAggregatedTimeDataMap.containsKey(vitalId)) {
                aggregatedData.add(new VitalDetailsDto.VitalsAggregatedData(vitalBiMap.get(vitalId), vitalId, vitalIdToVitalsAggregatedTimeDataMap.get(vitalId)));
            }
        }
        List<UserVitalGoalDto> userVitalGoalDtoList = vitalDetailsRequest.isIncludeUserGoals() ? getVitalsGoalsForUserIdListAndVitalIdList(userIdList, vitalTypeListDto.getVitalIdList()) : new ArrayList<>();
        return new VitalDetailsDto(userVitalGoalDtoList, logData, aggregatedData);
    }

    @Override
    public void saveUserVitalsDetails(Long userId, AddVitalDetailsRequest addVitalDetailsRequest) {
        List<Vital> vitalList = vitalService.getAllVitals();
        Map<String, Long> vitalNameToVitalIdMap = new HashMap<>();
        Map<Long, Boolean> vitalIdToAssociationMap = new HashMap<>();
        vitalList.forEach(vital -> {
            vitalNameToVitalIdMap.put(vital.getName(), vital.getId());
            vitalIdToAssociationMap.put(vital.getId(), vital.isContainsAssociations());
        });
        List<VitalLogs> vitalLogsList = new ArrayList<>();
        List<VitalAssociatedLogs> vitalAssociatedLogs = new ArrayList<>();
        addVitalDetailsRequest.getVitalDetails().forEach(vitalDto -> {
            Long vitalId = null != vitalDto.getVitalId() ? vitalDto.getVitalId() : vitalNameToVitalIdMap.get(vitalDto.getVitalName().toLowerCase());
            if (!vitalIdToAssociationMap.containsKey(vitalId)) {
                throw new NotFoundException(String.format(VitalValidation.VITAL_ID_OR_VITAL_NAME_NOT_FOUND.getMessage(), vitalDto.getVitalId(), vitalDto.getVitalName()));
            }
            vitalDto.setVitalId(vitalId);
            boolean containsAssociations = vitalIdToAssociationMap.get(vitalId);
            if (containsAssociations) {
                vitalAssociatedLogs.add(new VitalAssociatedLogs(userId, vitalId, vitalDto.getValue()));
            } else {
                vitalLogsList.add(new VitalLogs(userId, vitalId, Float.parseFloat(vitalDto.getValue())));
            }
        });
        if (!vitalLogsList.isEmpty()) {
            vitalLogsRepository.saveAll(vitalLogsList);
        }
        if (!vitalAssociatedLogs.isEmpty()) {
            vitalAssociatedLogsRepository.saveAll(vitalAssociatedLogs);
        }
    }
}

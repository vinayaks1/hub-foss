package com.everwell.userservice.services.impl;

import com.everwell.userservice.exceptions.InternalServerErrorException;
import com.everwell.userservice.models.db.User;
import com.everwell.userservice.models.db.UserAddress;
import com.everwell.userservice.models.dtos.DetailsChangedDto;
import com.everwell.userservice.models.dtos.UserAddressRequest;
import com.everwell.userservice.repositories.UserAddressRepository;
import com.everwell.userservice.services.UserAddressService;
import com.everwell.userservice.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserAddressServiceImpl  implements UserAddressService {

    @Autowired
    private UserAddressRepository userAddressRepository;

    @Override
    public UserAddress addUserAddressDetails(Long userId, UserAddressRequest userAddressRequest) {
        UserAddress userAddress = new UserAddress();
        if (null != userAddressRequest) {
            userAddress = new UserAddress(userId, userAddressRequest.getTaluka(), userAddressRequest.getTown(), userAddressRequest.getWard(), userAddressRequest.getLandmark(), userAddressRequest.getArea());
            userAddressRepository.save(userAddress);
        }
        return userAddress;
    }

    @Override
    public DetailsChangedDto<UserAddress> updateUserAddressDetails(Long userId, UserAddressRequest userUpdateAddressRequest) {
        boolean detailsChanged = false;
        UserAddress updatedUserAddess = null;
        if (null != userUpdateAddressRequest) {
            UserAddress userAddress = userAddressRepository.findByUserId(userId);
            if (null != userAddress) {
                UserAddress newUserAddress = new UserAddress(userAddress.getId(), userAddress.getUserId(), userUpdateAddressRequest.getTaluka(),  userUpdateAddressRequest.getTown(), userUpdateAddressRequest.getWard(),
                        userUpdateAddressRequest.getLandmark(), userUpdateAddressRequest.getArea());
                detailsChanged = Utils.compareObjectsForValue(userAddress, newUserAddress, detailsChanged, (currField, oldVal, newVal) -> {
                    try {
                        currField.set(newUserAddress, oldVal);
                    } catch (IllegalAccessException e) {
                        throw new InternalServerErrorException("Error updating user address details");
                    }
                });
                if (detailsChanged) {
                    updatedUserAddess = userAddressRepository.save(newUserAddress);
                }
            }
        }
        return new DetailsChangedDto<>(detailsChanged, updatedUserAddess);
    }

    public UserAddress getUserAddressDetails(Long userId) {
        return userAddressRepository.findByUserId(userId);
    }
}

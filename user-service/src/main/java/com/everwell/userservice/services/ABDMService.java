package com.everwell.userservice.services;

import com.everwell.userservice.models.dtos.ReferenceIdRMQRequest;
import com.everwell.userservice.models.dtos.abdm.ABDMAddUpdateConsentRequest;
import com.everwell.userservice.models.dtos.abdm.ABDMConsentResponse;
import com.everwell.userservice.models.dtos.abdm.AddConsentRequest;
import com.everwell.userservice.models.dtos.abdm.UpdateConsentDto;

import java.io.IOException;

public interface ABDMService
{
    void addUpdateConsent(ReferenceIdRMQRequest rmqRequest, ABDMAddUpdateConsentRequest abdmAddUpdateConsentRequest, String replyTo);
    ABDMConsentResponse getByConsentId(String consentId) throws IOException;
    void updateHIUConsentDetails(UpdateConsentDto updateConsentDto);
    void addHIUConsentDetails(AddConsentRequest addConsentRequest);
}

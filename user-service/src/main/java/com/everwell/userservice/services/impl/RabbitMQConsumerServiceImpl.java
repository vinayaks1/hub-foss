package com.everwell.userservice.services.impl;

import com.everwell.userservice.constants.RMQConstants;
import com.everwell.userservice.models.dtos.*;
import com.everwell.userservice.models.dtos.abdm.ABDMAddUpdateConsentRequest;
import com.everwell.userservice.models.dtos.abdm.UpdateConsentDto;
import com.everwell.userservice.services.ABDMService;
import com.everwell.userservice.services.RabbitMQConsumerService;
import com.everwell.userservice.services.UserService;
import com.everwell.userservice.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.everwell.userservice.constants.Constants.X_US_CLIENT_ID;

@Service
public class RabbitMQConsumerServiceImpl implements RabbitMQConsumerService
{
    @Autowired
    private UserService userService;

    @Autowired
    private ABDMService abdmService;

    private static Logger LOGGER = LoggerFactory.getLogger(RabbitMQConsumerServiceImpl.class);

    @RabbitListener(queues = RMQConstants.ADD_USER_QUEUE)
    public void addUser(Message message) throws Exception
    {
        String messageBody = new String(message.getBody());
        Long clientId = Long.parseLong((String)message.getMessageProperties().getHeaders().get(X_US_CLIENT_ID));
        EventStreamingDto<AddUserRequest> dto = Utils.convertObject(messageBody, new TypeReference<EventStreamingDto<AddUserRequest>>(){});
        userService.create(dto.getField(), clientId, true);
    }


    @RabbitListener(queues = RMQConstants.DELETE_USER_QUEUE)
    public void deleteUser(Message message) throws Exception
    {
        String messageBody = new String(message.getBody());
        Long clientId = Long.parseLong((String)message.getMessageProperties().getHeaders().get(X_US_CLIENT_ID));
        EventStreamingDto<DeleteUserRequest> dto = Utils.convertObject(messageBody, new TypeReference<EventStreamingDto<DeleteUserRequest>>(){});
        userService.delete(dto.getField(), clientId);
    }

    @RabbitListener(queues = RMQConstants.UPDATE_USER_QUEUE)
    public void updateUserDetails(Message message) throws Exception
    {
        String messageBody = new String(message.getBody());
        Long clientId = Long.parseLong((String)message.getMessageProperties().getHeaders().get(X_US_CLIENT_ID));
        EventStreamingDto<UpdateUserRequest> dto = Utils.convertObject(messageBody, new TypeReference<EventStreamingDto<UpdateUserRequest>>(){});
        userService.update(dto.getField(), clientId);
    }

    @RabbitListener(queues = RMQConstants.UPDATE_USER_MOBILE_QUEUE)
    public void updateUserMobiles(Message message) throws Exception
    {
        String messageBody = new String(message.getBody());
        Long clientId = Long.parseLong((String)message.getMessageProperties().getHeaders().get(X_US_CLIENT_ID));
        EventStreamingDto<UpdateMobileRequest> dto = Utils.convertObject(messageBody, new TypeReference<EventStreamingDto<UpdateMobileRequest>>(){});
        userService.updateMobiles(dto.getField(), clientId);
    }


    @RabbitListener(queues = RMQConstants.UPDATE_USER_EMAIL_QUEUE)
    public void updateUserEmails(Message message) throws Exception
    {
        String messageBody = new String(message.getBody());
        Long clientId = Long.parseLong((String)message.getMessageProperties().getHeaders().get(X_US_CLIENT_ID));
        EventStreamingDto<UpdateEmailRequest> dto = Utils.convertObject(messageBody, new TypeReference<EventStreamingDto<UpdateEmailRequest>>(){});
        userService.updateEmails(dto.getField(), clientId);
    }

    @RabbitListener(queues = RMQConstants.ABDM_ADD_UPDATE_CONSENT_CONSUMER_QUEUE)
    public void abdmAddUpdateConsent(Message message) throws Exception
    {
        String messageBody = new String(message.getBody());
        String replyTo = message.getMessageProperties().getReplyTo();
        ReferenceIdRMQRequest rmqRequest = Utils.convertObject(messageBody, new TypeReference<ReferenceIdRMQRequest>(){});
        ABDMAddUpdateConsentRequest consentRequest = Utils.convertObject(rmqRequest.getData(), new TypeReference<ABDMAddUpdateConsentRequest>(){});
        abdmService.addUpdateConsent(rmqRequest, consentRequest, replyTo);
    }


    @RabbitListener(queues = RMQConstants.ABDM_UPDATE_HIU_CONSENT_REQUEST_QUEUE)
    public void updateConsent(Message message) throws Exception {
        String messageBody = new String(message.getBody());
        UpdateConsentDto updateConsentDto = Utils.convertObject(messageBody, new TypeReference<UpdateConsentDto>(){});
        abdmService.updateHIUConsentDetails(updateConsentDto);
    }
}

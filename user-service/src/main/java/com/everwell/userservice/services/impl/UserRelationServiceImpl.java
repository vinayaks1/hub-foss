package com.everwell.userservice.services.impl;

import com.everwell.userservice.enums.UserRelationEnum;
import com.everwell.userservice.enums.UserRelationStatus;
import com.everwell.userservice.enums.UserRole;
import com.everwell.userservice.exceptions.ValidationException;
import com.everwell.userservice.models.db.UserRelation;
import com.everwell.userservice.models.dtos.UserRelationDto;
import com.everwell.userservice.repositories.UserRelationRepository;
import com.everwell.userservice.services.UserRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserRelationServiceImpl implements UserRelationService {

    @Autowired
    private UserRelationRepository userRelationRepository;

    public void addMutualRelation(UserRelation userRelation) {
        String role = userRelation.getRole();
        if (!StringUtils.isEmpty(role)) {
            role = UserRole.getMutualRole(role);
        }
        String relation = userRelation.getRelation();
        relation = UserRelationEnum.getMutualRelation(relation);

        UserRelationDto mutualRequest = new UserRelationDto(
                userRelation.getSecondaryUserId(),
                userRelation.getPrimaryUserId(),
                relation,
                role,
                null,
                UserRelationStatus.ACCEPT.getName(),
                userRelation.isEmergencyContact()
        );
        UserRelation mutualUserRelation = new UserRelation(mutualRequest);
        userRelationRepository.save(mutualUserRelation);
    }

    @Override
    public List<UserRelation> getUserRelations(Long userId, String status) {
        Integer statusValue = UserRelationStatus.getStatusValue(status);
        if (statusValue.equals(UserRelationStatus.ALL.getValue())) {
            return userRelationRepository.findAllByPrimaryUserId(userId);
        } else {
            return userRelationRepository.findAllByPrimaryUserIdAndStatus(userId, statusValue);
        }
    }

    @Override
    public void addUserRelation(UserRelationDto userRelationDto) {
        UserRelation oldUserRelation = userRelationRepository.findByPrimaryUserIdAndSecondaryUserId(userRelationDto.getPrimaryUserId(), userRelationDto.getSecondaryUserId());
        if (null != oldUserRelation) {
            throw new ValidationException("Relation already exists");
        }
        String role = userRelationDto.getRole();
        if (!StringUtils.isEmpty(role)) {
            List<String> userRoles = Arrays.stream(UserRole.values()).map(u -> u.toString().toLowerCase()).collect(Collectors.toList());
            if (!userRoles.contains(role.toLowerCase())) {
                throw new ValidationException("Invalid role " + role);
            }
        }
        String relation = userRelationDto.getRelation();
        if (StringUtils.isEmpty(relation)) {
            throw new ValidationException("relation cannot be empty");
        }
        List<String> userRelations = Arrays.stream(UserRelationEnum.values()).map(u -> u.name().toLowerCase()).collect(Collectors.toList());
        if (!userRelations.contains(relation.toLowerCase())) {
            throw new ValidationException("Invalid relation " + relation);
        }
        UserRelation userRelation = new UserRelation(userRelationDto);
        userRelation.setStatus(UserRelationStatus.PENDING.getValue());
        userRelationRepository.save(userRelation);
    }

    @Override
    public void deleteUserRelation(UserRelationDto userRelationDto) {
        UserRelation userRelation = userRelationRepository.findByPrimaryUserIdAndSecondaryUserId(userRelationDto.getPrimaryUserId(), userRelationDto.getSecondaryUserId());
        if (null == userRelation) {
            throw new ValidationException("Relation does not exists");
        }
        List<UserRelation> userRelations = new ArrayList<>(Collections.singletonList(userRelation));
        UserRelation mutualUserRelation = userRelationRepository.findByPrimaryUserIdAndSecondaryUserId(userRelationDto.getSecondaryUserId(), userRelationDto.getPrimaryUserId());
        if (null != mutualUserRelation) {
            userRelations.add(mutualUserRelation);
        }
        userRelationRepository.deleteAll(userRelations);
    }

    @Override
    public void updateUserRelation(UserRelationDto userRelationDto) {
        UserRelation userRelation = userRelationRepository.findByPrimaryUserIdAndSecondaryUserId(userRelationDto.getPrimaryUserId(), userRelationDto.getSecondaryUserId());
        if (null == userRelation) {
            throw new ValidationException("Relation does not exists");
        }
        List<String> status = Arrays.stream(UserRelationStatus.values()).map(u -> u.getName().toLowerCase()).collect(Collectors.toList());
        String currentStatus = userRelationDto.getStatus();
        if (StringUtils.isEmpty(currentStatus) || !status.contains(currentStatus.toLowerCase())) {
            throw new ValidationException("Invalid status");
        }
        if (userRelationDto.getStatus().equalsIgnoreCase(UserRelationStatus.ACCEPT.getName()) && !userRelation.getStatus().equals(UserRelationStatus.ACCEPT.getValue())) {
            userRelation.setStatus(UserRelationStatus.ACCEPT.getValue());
            userRelationRepository.save(userRelation);
            addMutualRelation(userRelation);
        } else if (userRelationDto.getStatus().equalsIgnoreCase(UserRelationStatus.DENY.getName())){
            deleteUserRelation(userRelationDto);
        }
    }
}

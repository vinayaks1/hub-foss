package com.everwell.userservice.services;

import com.everwell.userservice.models.dtos.UpdateUserRequest;
import com.everwell.userservice.models.http.requests.*;
import com.everwell.userservice.models.http.responses.*;

import java.io.IOException;

public interface ABHAService {

    AadhaarOTPResponse verifyOtp(OTPVerifyRequest otpVerifyRequest, String otpType) throws IOException;

    ABHACreateResponse createAbha(ABHACreateRequest abhaCreateRequest) throws IOException;

    ABHASearchResponse searchAbha(ABHASearchRequest abhaSearchRequest) throws IOException;

    AadhaarOTPResponse generateOtp(Object requestObj, String urlPath) throws IOException;

    ABHAAuthResponse verifyOtpForAuth(OTPVerifyRequest otpVerifyRequest) throws IOException;

    ABHAProfileResponse getProfile(String authToken) throws IOException;

    <T> T getAbhaImage(Long userId, String imageName) throws IOException;

    UpdateUserRequest buildUpdateUserReq(Long userId, String txnId, Object abhaDetails) throws IOException;

    void resendOtp(ResendOTPRequest resendOTPRequest, String urlPath) throws IOException;

    MobileEmailOTPResponse generateOtpForMobileEmailLogin(MobileEmailOTPRequest requestObj, String urlPath) throws IOException;
    MobileEmailVerifyOTPResponse verifyOtpForMobileEmailLogin(MobileEmailVerifyOTPRequest requestObj, String urlPath) throws IOException;
    UserAuthorizedTokenResponse userAuthorizedTokenForMobileEmail(UserAuthorizedTokenForMobileEmailRequest requestObj, String urlPath) throws IOException;
    ShareProfileResponse shareProfile(ShareProfileRequest shareProfileRequest,String url) throws IOException;


    ABHAProviderResponse getProviderDetails(String providerId,Long userId) throws IOException;
    ABHAProfileResponse getAbhaProfile(Long userId) throws IOException;
}

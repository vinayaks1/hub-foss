package com.everwell.userservice.services;

import com.everwell.userservice.models.dtos.vitals.VitalGoalDto;
import com.everwell.userservice.models.http.requests.AddVitalDetailsRequest;
import com.everwell.userservice.models.http.requests.VitalDetailsRequest;
import com.everwell.userservice.models.http.requests.VitalGoalRequest;
import com.everwell.userservice.models.dtos.vitals.UserVitalGoalDto;
import com.everwell.userservice.models.dtos.vitals.VitalDetailsDto;

import java.util.List;

public interface UserVitalService {

    List<VitalGoalDto> getVitalsGoalByUserId(Long userId);

    List<UserVitalGoalDto>  getVitalsGoalsForUserIdListAndVitalIdList(List<Long> userIdList, List<Long> vitalIdList);

    void saveUserVitalsGoal(Long userId, VitalGoalRequest vitalGoalRequest);

    void updateUserVitalsGoal(Long userId, VitalGoalRequest vitalGoalRequest);

    void deleteUserVitalsGoal(Long userId);

    VitalDetailsDto getUserVitalsDetails(VitalDetailsRequest vitalDetailsRequest);

    void saveUserVitalsDetails(Long userId, AddVitalDetailsRequest addVitalDetailsRequest);
}

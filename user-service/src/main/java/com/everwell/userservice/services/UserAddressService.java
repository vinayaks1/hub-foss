package com.everwell.userservice.services;

import com.everwell.userservice.models.db.UserAddress;
import com.everwell.userservice.models.dtos.DetailsChangedDto;
import com.everwell.userservice.models.dtos.UserAddressRequest;

public interface UserAddressService {

    UserAddress addUserAddressDetails(Long userId, UserAddressRequest userAddressRequest);

    DetailsChangedDto<UserAddress> updateUserAddressDetails(Long userId, UserAddressRequest userAddressRequest);

    UserAddress getUserAddressDetails(Long userId);

}

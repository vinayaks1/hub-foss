package com.everwell.userservice.services;

import com.everwell.userservice.models.db.Vital;
import com.everwell.userservice.models.http.requests.VitalBulkRequest;
import com.everwell.userservice.models.http.requests.VitalRequest;
import com.everwell.userservice.models.http.responses.VitalResponse;

import java.util.List;

public interface VitalService {

    List<VitalResponse> getAllVitalsResponse();

    List<Vital> getAllVitals();

    VitalResponse getVitalResponseById(Long id);

    List<Vital> saveVitals(VitalBulkRequest vitalBulkRequest);

    VitalResponse updateVitalsById(Long id, VitalRequest vitalRequest);

    void deleteVitalsById(Long id);

    Long processAndFetchVitalId(Long vitalId, String vitalName);
}

package com.everwell.userservice.services.impl;

import com.everwell.userservice.enums.VitalValidation;
import com.everwell.userservice.exceptions.NotFoundException;
import com.everwell.userservice.exceptions.ValidationException;
import com.everwell.userservice.models.db.Vital;
import com.everwell.userservice.models.http.requests.VitalBulkRequest;
import com.everwell.userservice.models.http.requests.VitalRequest;
import com.everwell.userservice.models.http.responses.VitalResponse;
import com.everwell.userservice.repositories.VitalRepository;
import com.everwell.userservice.services.VitalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class VitalServiceImpl implements VitalService {

    @Autowired
    private VitalRepository vitalRepository;

    private Vital getVitalById(Long id) {
        Optional<Vital> vital = vitalRepository.findById(id);
        if (!vital.isPresent()) {
            throw new NotFoundException(VitalValidation.VITAL_ID_NOT_FOUND.getMessage());
        }
        return vital.get();
    }

    @Override
    public List<VitalResponse> getAllVitalsResponse() {
        List<VitalResponse> vitalResponseList = new ArrayList<>();
        List<Vital> vitalList = vitalRepository.findAll();
        vitalList.forEach(vital -> {
            vitalResponseList.add(new VitalResponse(vital));
        });
        return vitalResponseList;
    }

    @Override
    public List<Vital> getAllVitals() {
        return vitalRepository.findAll();
    }

    @Override
    public VitalResponse getVitalResponseById(Long id) {
        Vital vital = getVitalById(id);
        return new VitalResponse(vital);
    }

    @Override
    public List<Vital> saveVitals(VitalBulkRequest vitalBulkRequest) {
        List<Vital> vitalResponseList = new ArrayList<>();
        Map<String, Vital> vitalNameToVitalsMap = vitalRepository.findAll()
                .stream()
                .collect(Collectors.toMap(Vital::getName, i->i, (u1, u2) -> u1));
        List<Vital> vitalList = new ArrayList<>();
        Set<String> vitalsNameSet = new HashSet<>();
        vitalBulkRequest.getVitalsDetails().forEach(vitalRequest -> {
            String vitalName = vitalRequest.getName().toLowerCase();
            Vital vital = vitalNameToVitalsMap.get(vitalName);
            if (null == vital && !vitalsNameSet.contains(vitalName)) {
                vital = new Vital(vitalName, vitalRequest.getLowValue(), vitalRequest.getHighValue(), vitalRequest.getCriticalLowValue(),
                        vitalRequest.getCriticalHighValue(), vitalRequest.getUnitOfMeasurement(), vitalRequest.isContainsAssociations());
                vitalList.add(vital);
            }
            if (null != vital) {
                vitalResponseList.add(vital);
            }
            vitalsNameSet.add(vitalName);
        });
        vitalRepository.saveAll(vitalList);
        return vitalResponseList;
    }

    @Override
    public VitalResponse updateVitalsById(Long id, VitalRequest vitalRequest) {
        getVitalById(id);
        String vitalName = vitalRequest.getName().toLowerCase();
        Vital newVital = new Vital(id, vitalName, vitalRequest.getLowValue(), vitalRequest.getHighValue(), vitalRequest.getCriticalLowValue(),
                vitalRequest.getCriticalHighValue(), vitalRequest.getUnitOfMeasurement(), vitalRequest.isContainsAssociations());
        vitalRepository.save(newVital);
        return new VitalResponse(newVital);
    }

    @Override
    public void deleteVitalsById(Long id) {
        getVitalById(id);
        vitalRepository.deleteById(id);
    }

    @Override
    public Long processAndFetchVitalId(Long vitalId, String vitalName) {
        List<Vital> vitalList = vitalRepository.findAll();
        Long processedVitalId = vitalId;
        if (null != vitalId) {
            boolean isVitalIdPresent = vitalList
                    .stream()
                    .anyMatch(i->i.getId().equals(vitalId));
            if (!isVitalIdPresent) {
                throw new ValidationException(VitalValidation.VITAL_ID_NOT_FOUND.getMessage());
            }
        } else if (!StringUtils.isEmpty(vitalName)) {
            String vitalNameLowerCased = vitalName.toLowerCase();
            Optional<Vital> vital = vitalList
                    .stream()
                    .filter(i->i.getName().equals(vitalNameLowerCased))
                    .findFirst();
            if (!vital.isPresent()) {
                throw  new ValidationException(VitalValidation.VITAL_NAME_NOT_FOUND.getMessage());
            }
            processedVitalId = vital.get().getId();
        } else {
            throw  new ValidationException(VitalValidation.VITAL_ID_OR_VITAL_NAME.getMessage());
        }
        return processedVitalId;
    }
}

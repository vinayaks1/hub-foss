package com.everwell.userservice.services.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.amqp.core.AmqpTemplate;

import com.everwell.userservice.services.RabbitMQPublisherService;

import java.util.Map;

@Service
public class RabbitMQPublisherServiceImpl implements RabbitMQPublisherService
{
    @Autowired
    private AmqpTemplate template;

    public void send(Object message, String exchange, String routingKey)
    {
        template.convertAndSend(exchange, routingKey, message);
    }

    @Override
    public void send(Object message, String exchange, String routingKey, Map<String, Object> map) {
        if(message != null){
            this.template.convertAndSend(exchange, routingKey, message, m -> {
                map.forEach((k, v) -> m.getMessageProperties().setHeader(k, v));
                return m;
            });
        }
    }
}
package com.everwell.userservice.services.impl;

import com.everwell.userservice.constants.Constants;
import com.everwell.userservice.enums.UserRelationStatus;
import com.everwell.userservice.enums.UserValidation;
import com.everwell.userservice.exceptions.NotFoundException;

import com.everwell.userservice.constants.RMQConstants;
import com.everwell.userservice.exceptions.ValidationException;
import com.everwell.userservice.handlers.DeduplicationHandler;
import com.everwell.userservice.handlers.DeduplicationHandlerMap;
import com.everwell.userservice.models.db.*;
import com.everwell.userservice.models.dtos.*;
import com.everwell.userservice.models.http.responses.UserResponseForEpisode;
import com.everwell.userservice.repositories.*;
import com.everwell.userservice.models.dtos.AddUserRequest;
import com.everwell.userservice.models.dtos.UserEmailRequest;
import com.everwell.userservice.models.dtos.UserMobileRequest;
import com.everwell.userservice.services.*;
import com.everwell.userservice.utils.Utils;
import com.everwell.userservice.utils.ValidationUtils;
import io.sentry.Sentry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;
import org.apache.commons.text.similarity.*;

import static com.everwell.userservice.constants.Constants.ABDM_DISCOVERY_AGE_BAND;


@Service
public class UserServiceImpl implements UserService
{
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserMobileRepository userMobileRepository;

    @Autowired
    private UserEmailRepository userEmailRepository;

    @Autowired
    private ExternalIdRepository externalIdRepository;

    @Autowired
    private VerificationRepository verificationRepository;

    @Autowired
    private RabbitMQPublisherService rabbitMQPublisherService;

    @Autowired
    private DeduplicationHandlerMap deduplicationHandlerMap;

    @Autowired
    private UserAddressService userAddressService;

    @Autowired
    private UserContactPersonService userContactPersonService;

    @Autowired
    private UserRelationService userRelationService;

    @Autowired
    private ClientService clientService;

    private UserResponse userResponse(UserDetails userDetails) throws Exception
    {
        User user = userDetails.getUser();
        UserResponse response = Utils.convertObject(Utils.asJsonString(user), UserResponse.class);
        response.setAgeFromDateOfBirth(user.getDateOfBirth());
        response.setMobileList(userDetails.getMobileList());
        response.setEmailList(userDetails.getEmailList());
        if (!StringUtils.isEmpty(response.primaryPhone()))
            response.setPrimaryPhoneNumber(response.primaryPhone());
        if (userDetails.getUserAddress() != null)
            response.setUserAddress(userDetails.getUserAddress());
        if (userDetails.getUserContactPerson() != null)
            response.setUserContactPerson(userDetails.getUserContactPerson());

        List<Verification> verifications = this.verificationRepository.findAllByUserIdAndStoppedAtNull(user.getId());
        response.setVerifications(formatVerificationsForResponse(verifications));

        List<ExternalId> externalIds = this.externalIdRepository.findAllByUserId(user.getId());
        response.setExternalIds(formatExternalIdsForResponse(externalIds));

        return response;
    }

    @Override
    public User getUser(Long id, Long clientId) {
        User user = userRepository.findByIdAndIsDeletedAndClientId(id, false, clientId);
        if (null == user) {
            throw new NotFoundException("User Not Found for given Id");
        }
        return user;
    }

    @Override
    public void validateUserIdList(List<Long> userIdList, Long clientId) {
        Set<Long> existingUserIdSet = userRepository.findAllByIdInAndIsDeletedAndClientId(userIdList, false, clientId)
                .stream()
                .map(User::getId)
                .collect(Collectors.toSet());
        List<Long> invalidUserIdList = userIdList
                .stream()
                .filter(i->!existingUserIdSet.contains(i))
                .collect(Collectors.toList());
        if (!invalidUserIdList.isEmpty()) {
            throw new NotFoundException(String.format(UserValidation.USER_ID_NOT_FOUND.getMessage(), invalidUserIdList));
        }
    }

    public UserResponse get(Long id, Long clientId) throws Exception {
        UserResponse response = null;
        User user = userRepository.findByIdAndIsDeletedAndClientId(id, false, clientId);
        if (null != user)
        {
            List<UserCompleteDto> userCompleteDtoList = userRepository.getAllUserDetailByUserId(user.getId(), clientId);

            List<UserMobile> userMobiles =  new ArrayList<>( userCompleteDtoList.stream()
                    .filter(i->i.getUserMobileId() != null )
                    .collect(Collectors.toMap(UserCompleteDto::getUserMobileId, i-> new UserMobile(i.getUserMobileId(), i.getUserMobileUserId(), i.getUserMobileNumber(), i.getUserMobileIsPrimary(), i.getUserMobileIsVerified(), i.getUserMobileCreatedAt(),
                            i.getUserMobileUpdatedAt(), i.getUserMobileStoppedAt(), i.getUserMobileOwner()), (oldValue, newValue) -> newValue))
                    .values());

            List<UserEmail> userEmails = new ArrayList<>( userCompleteDtoList.stream()
                    .filter(i->i.getUserEmailIdentityId() != null)
                    .collect(Collectors.toMap(UserCompleteDto::getUserEmailIdentityId , i->new UserEmail(i.getUserEmailIdentityId(), i.getUserEmailUserId(), i.getUserEmailId(),
                            i.getUserEmailIsPrimary(), i.getUserEmailIsVerified(), i.getUserEmailCreatedAt(),
                            i.getUserEmailUpdatedAt(), i.getUserMobileStoppedAt()),(oldValue, newValue) -> newValue))
                    .values());

            UserAddress userAddress = userCompleteDtoList.stream().map(userCompleteDto ->  new UserAddress(userCompleteDto.getUserAddressId(), userCompleteDto.getUserAddressUserId(), userCompleteDto.getTaluka(), userCompleteDto.getTown(),
                          userCompleteDto.getWard(), userCompleteDto.getLandmark(), userCompleteDto.getArea()))
                    .filter(i->i.getId() != null)
                    .findFirst().orElse(new UserAddress());

            UserContactPerson userContactPerson = userCompleteDtoList.stream().map(userCompleteDto -> new UserContactPerson(userCompleteDto.getUserContactPersonId(), userCompleteDto.getUserContactPersonUserId(), userCompleteDto.getAddress(),
                           userCompleteDto.getName(), userCompleteDto.getPhone()))
                    .filter(i->i.getId() != null)
                    .findFirst().orElse(new UserContactPerson());

            UserDetails userDetails = new UserDetails(user, userMobiles, userEmails, userAddress, userContactPerson);
            response = userResponse(userDetails);
        }
        return response;
    }

    private List<VerificationResponse> formatVerificationsForResponse(List<Verification> verifications)
    {
        return verifications.stream().map(verification -> new VerificationResponse(verification.getType(), verification.getEntityType(), verification.getEntityValue())).collect(Collectors.toList());
    }

    private List<ExternalIdResponse> formatExternalIdsForResponse(List<ExternalId> externalIds)
    {
        return externalIds.stream().map(externalId -> new ExternalIdResponse(externalId.getType(), externalId.getValue())).collect(Collectors.toList());
    }

    @Override
    public List<UserResponse> getByPrimaryPhone(String phoneNumber, Long clientId) throws Exception
    {
        if(!ValidationUtils.isValidNumber(phoneNumber))
        {
            throw new ValidationException("Invalid phone number");
        }

        List<UserMobile> userMobiles = userMobileRepository.findAllByNumberAndIsPrimaryTrue(phoneNumber);

        List<Long> userIds = new ArrayList<>();
        for(UserMobile userMobile: userMobiles)
        {
            userIds.add(userMobile.getUserId());
        }

        List<User> activeUsers = userRepository.findAllByIdInAndIsDeletedAndClientId(userIds, false, clientId);

        return getUserResponseListFromUsers(activeUsers, null);
    }

    private List<UserResponse> getUserResponseListFromUsers(List<User> users, Set<String> requiredFields) throws Exception
    {
        List<UserResponse> userResponseList = new ArrayList<>();
        List<Long> userIds = users.stream().map(User::getId).collect(Collectors.toList());
        if(!userIds.isEmpty())
        {
            List<UserMobile> mobiles = userMobileRepository.findAllByUserIdInAndStoppedAtNull(userIds);
            List<UserEmail> emails = userEmailRepository.findAllByUserIdInAndStoppedAtNull(userIds);

            Map<Long, List<UserMobile>> userMobilesMap = new HashMap<>();
            Map<Long, List<UserEmail>> userEmailsMap = new HashMap<>();

            for(UserMobile mobile: mobiles)
            {
                List<UserMobile> mobileList = new ArrayList<>();
                if(userMobilesMap.containsKey(mobile.getUserId()))
                {
                    mobileList = userMobilesMap.get(mobile.getUserId());
                }
                mobileList.add(mobile);

                userMobilesMap.put(mobile.getUserId(), mobileList);
            }

            for(UserEmail email: emails)
            {
                List<UserEmail> emailList = new ArrayList<>();
                if(userEmailsMap.containsKey(email.getUserId()))
                {
                    emailList = userEmailsMap.get(email.getUserId());
                }
                emailList.add(email);

                userEmailsMap.put(email.getUserId(), emailList);
            }

            for(User user: users)
            {
                UserResponse userObject = Utils.convertObject(Utils.asJsonString(user), UserResponse.class);
                if(userMobilesMap.containsKey(user.getId()))
                {
                    userObject.setMobileList(userMobilesMap.get(user.getId()));
                }
                if(userEmailsMap.containsKey(user.getId()))
                {
                    userObject.setEmailList(userEmailsMap.get(user.getId()));
                }

                UserResponse userResponse = new UserResponse(userObject, requiredFields);

                userResponseList.add(userResponse);
            }
        }

        return userResponseList;
    }

    public UserResponse create(AddUserRequest addUserRequest, Long clientId, boolean ackRequired) throws Exception
    {
        addUserRequest.validate();

        User user = new User(addUserRequest, clientId);
        Date now = new Date();
        user.setCreatedAt(now);
        user.setUpdatedAt(now);

        userRepository.save(user);

        List<UserMobile> mobiles = new ArrayList<>();
        for(UserMobileRequest mobileRequest: addUserRequest.getMobileList())
        {
            mobileRequest.validate();
            mobiles.add(new UserMobile(mobileRequest.getNumber(), user.getId(), mobileRequest.isPrimary(), now, now, false, mobileRequest.getOwner()));
        }
        userMobileRepository.saveAll(mobiles);

        List<UserEmail> emails = new ArrayList<>();
        if(null != addUserRequest.getEmailList())
        {
            for(UserEmailRequest emailRequest: addUserRequest.getEmailList())
            {
                emailRequest.validate();
                emails.add(new UserEmail(emailRequest.getEmailId(), user.getId(), emailRequest.isPrimary(), now, now));
            }
            if(emails.size() > 0)
            {
                userEmailRepository.saveAll(emails);
            }
        }

        UserAddressRequest userAddressRequest = new UserAddressRequest(addUserRequest.getTaluka(), addUserRequest.getTown(), addUserRequest.getWard(), addUserRequest.getLandmark(), addUserRequest.getArea());
        UserContactPersonRequest userContactPersonRequest = new UserContactPersonRequest(addUserRequest.getContactPersonAddress(), addUserRequest.getContactPersonName(), addUserRequest.getContactPersonPhone());
        UserAddress userAddress = userAddressService.addUserAddressDetails(user.getId(), userAddressRequest);
        UserContactPerson userContactPerson = userContactPersonService.addUserContactPersonDetails(user.getId(), userContactPersonRequest);

        if(ackRequired)
        {
            rabbitMQPublisherService.send(new AddUserResponse(user.getId(), addUserRequest.getEntityId(), addUserRequest.getUserType()), RMQConstants.USER_EXCHANGE, RMQConstants.ADD_USER_ACK_ROUTING_KEY_PREFIX + addUserRequest.getSource());
        }

        UserDetails userDetails = new UserDetails(user, mobiles, emails, userAddress, userContactPerson);
        return userResponse(userDetails);
    }

    public void delete(DeleteUserRequest deleteUserRequest, Long clientId)
    {
        Date now = new Date();
        User user = userRepository.findByIdAndIsDeletedAndClientId(deleteUserRequest.getUserId(), false, clientId);
        if (null == user)
        {
            throw new NotFoundException("User with given id not found");
        }

        user.setUpdatedAt(now);
        user.setDeletedAt(now);
        user.setDeletedBy(deleteUserRequest.getDeletedBy());
        user.setDeletedByType(deleteUserRequest.getDeletedByType());
        user.setDeleted(true);

        userRepository.save(user);

        List<UserMobile> mobiles = userMobileRepository.findAllByUserIdAndStoppedAtNull(deleteUserRequest.getUserId());
        for(UserMobile mobile: mobiles)
        {
            mobile.setPrimary(false);
            mobile.setStoppedAt(now);
        }
        userMobileRepository.saveAll(mobiles);

        List<UserEmail> emails = userEmailRepository.findAllByUserIdAndStoppedAtNull(deleteUserRequest.getUserId());
        for(UserEmail email: emails)
        {
            email.setPrimary(false);
            email.setStoppedAt(now);
        }
        userEmailRepository.saveAll(emails);
    }

    public void update(UpdateUserRequest updateUserRequest, Long clientId) throws Exception
    {
        updateUserRequest.validate();

        User user = userRepository.findByIdAndIsDeletedAndClientId(updateUserRequest.getUserId(), false, clientId);

        if(null == user)
        {
            throw new NotFoundException("User with given id not found");
        }

        boolean detailsChanged = false;
        Date now = new Date();

        if(!StringUtils.isEmpty(updateUserRequest.getFirstName()) && (!updateUserRequest.getFirstName().equals(user.getFirstName())))
        {
            detailsChanged = true;
            user.setFirstName(updateUserRequest.getFirstName());
        }

        if(!StringUtils.isEmpty(updateUserRequest.getLastName()) && (!updateUserRequest.getLastName().equals(user.getLastName())))
        {
            detailsChanged = true;
            user.setLastName(updateUserRequest.getLastName());
        }

        if(!StringUtils.isEmpty(updateUserRequest.getGender()) && (!updateUserRequest.getGender().equals(user.getGender())))
        {
            detailsChanged = true;
            user.setGender(updateUserRequest.getGender());
        }

        if(!StringUtils.isEmpty(updateUserRequest.getLanguage()) && (!updateUserRequest.getLanguage().equals(user.getLanguage())))
        {
            detailsChanged = true;
            user.setLanguage(updateUserRequest.getLanguage());
        }

        if(!StringUtils.isEmpty(updateUserRequest.getAddress()) && (!updateUserRequest.getAddress().equals(user.getAddress())))
        {
            detailsChanged = true;
            user.setAddress(updateUserRequest.getAddress());
        }

        if(!StringUtils.isEmpty(updateUserRequest.getFatherName()) && (!updateUserRequest.getFatherName().equals(user.getFatherName())))
        {
            detailsChanged = true;
            user.setFatherName(updateUserRequest.getFatherName());
        }

        if((null != updateUserRequest.getPincode()) && (!updateUserRequest.getPincode().equals(user.getPincode())))
        {
            detailsChanged = true;
            user.setPincode(updateUserRequest.getPincode());
        }

        if(!StringUtils.isEmpty(updateUserRequest.getCity()) && (!updateUserRequest.getCity().equals(user.getCity())))
        {
            detailsChanged = true;
            user.setCity(updateUserRequest.getCity());
        }

        if(!StringUtils.isEmpty(updateUserRequest.getMaritalStatus()) && (!updateUserRequest.getMaritalStatus().equals(user.getMaritalStatus())))
        {
            detailsChanged = true;
            user.setMaritalStatus(updateUserRequest.getMaritalStatus());
        }

        if(!StringUtils.isEmpty(updateUserRequest.getSocioEconomicStatus()) && (!updateUserRequest.getSocioEconomicStatus().equals(user.getSocioEconomicStatus())))
        {
            detailsChanged = true;
            user.setSocioEconomicStatus(updateUserRequest.getSocioEconomicStatus());
        }

        if(!StringUtils.isEmpty(updateUserRequest.getKeyPopulation()) && (!updateUserRequest.getKeyPopulation().equals(user.getKeyPopulation())))
        {
            detailsChanged = true;
            user.setKeyPopulation(updateUserRequest.getKeyPopulation());
        }

        if((null != updateUserRequest.getHeight()) && ((null == user.getHeight()) || (!updateUserRequest.getHeight().equals(user.getHeight()))))
        {
            detailsChanged = true;
            user.setHeight(updateUserRequest.getHeight());
        }

        if((null != updateUserRequest.getWeight()) && ((null == user.getWeight()) || (!updateUserRequest.getWeight().equals(user.getWeight()))))
        {
            detailsChanged = true;
            user.setWeight(updateUserRequest.getWeight());
        }

        if(!StringUtils.isEmpty(updateUserRequest.getDateOfBirth()))
        {
            if(user.getDateOfBirth() == null || !updateUserRequest.getDateOfBirth().equals(Utils.getFormattedDate(user.getDateOfBirth(), "yyyy-MM-dd")))
            {
                user.setDateOfBirth(Utils.convertStringToDate(updateUserRequest.getDateOfBirth(), "yyyy-MM-dd"));
                detailsChanged = true;
            }
        }

        if(!StringUtils.isEmpty(updateUserRequest.getOccupation()) && (!updateUserRequest.getOccupation().equals(user.getOccupation())))
        {
            detailsChanged = true;
            user.setOccupation(updateUserRequest.getOccupation());
        }

        if((null != updateUserRequest.getExternalIds()) && !updateUserRequest.getExternalIds().isEmpty())
        {
            List<ExternalId> externalIds = new ArrayList<>();
            for(ExternalIdRequest request : updateUserRequest.getExternalIds())
            {
                ExternalId externalId = new ExternalId();
                externalId.setUserId(updateUserRequest.getUserId());
                externalId.setType(request.getType());
                externalId.setValue(request.getValue());
                externalId.setCreatedAt(now);

                externalIds.add(externalId);
            }

            this.externalIdRepository.saveAll(externalIds);
        }

        if((null != updateUserRequest.getVerifications()) && !updateUserRequest.getVerifications().isEmpty())
        {
            List<Verification> verifications = new ArrayList<>();
            for(VerificationRequest request : updateUserRequest.getVerifications())
            {
                Verification verification = new Verification();
                verification.setUserId(updateUserRequest.getUserId());
                verification.setType(request.getType());
                verification.setEntityType(request.getEntityType());
                verification.setEntityValue(request.getEntityValue());
                verification.setTransactionId(request.getTransactionId());
                verification.setCreatedAt(now);

                verifications.add(verification);
            }

            this.verificationRepository.saveAll(verifications);
        }

        if(null != updateUserRequest.getDataConsent() && (!updateUserRequest.getDataConsent().equals(user.isDataConsent())))
        {
            detailsChanged = true;
            user.setDataConsent(updateUserRequest.getDataConsent());
        }

        //Update User Mobile List
        if(!CollectionUtils.isEmpty(updateUserRequest.getMobileList())) {
            UpdateMobileRequest updateMobileRequest = new UpdateMobileRequest(updateUserRequest.getUserId(), updateUserRequest.getMobileList());
            updateMobiles(updateMobileRequest, clientId);
        }

        //Update User Email List
        if(!CollectionUtils.isEmpty(updateUserRequest.getEmailList())) {
            UpdateEmailRequest updateEmailRequest = new UpdateEmailRequest(updateUserRequest.getUserId(), updateUserRequest.getEmailList());
            updateEmails(updateEmailRequest, clientId);
        }

        UserAddressRequest userAddressRequest = new UserAddressRequest(updateUserRequest.getTaluka(), updateUserRequest.getTown(), updateUserRequest.getWard(), updateUserRequest.getLandmark(), updateUserRequest.getArea());
        UserContactPersonRequest userContactPersonRequest = new UserContactPersonRequest(updateUserRequest.getContactPersonAddress(), updateUserRequest.getContactPersonName(), updateUserRequest.getContactPersonPhone());
        DetailsChangedDto<UserAddress> addressDetailsChangedDto = userAddressService.updateUserAddressDetails(updateUserRequest.getUserId(), userAddressRequest);
        DetailsChangedDto<UserContactPerson> contactDetailsChangedDto = userContactPersonService.updateUserContactPersonDetails(updateUserRequest.getUserId(), userContactPersonRequest);

        if(detailsChanged || addressDetailsChangedDto.getDetailsChanged() || contactDetailsChangedDto.getDetailsChanged())
        {
            user.setUpdatedAt(now);
            userRepository.save(user);
            UserDetails userDetails = new UserDetails(user, null, null, addressDetailsChangedDto.getData(), contactDetailsChangedDto.getData());
            publishEventForEpisode(userResponse(userDetails), clientId);
        }
    }

    public void updateMobiles(UpdateMobileRequest updateMobileRequest, Long clientId)
    {
        updateMobileRequest.validate();

        User user = userRepository.findByIdAndIsDeletedAndClientId(updateMobileRequest.getUserId(), false, clientId);
        if(null == user)
        {
            throw new NotFoundException("User with given id not found");
        }

        Date now = new Date();

        Map<String, UserMobileRequest> mobilesFromUpdateRequest = new HashMap<>();
        Map<String, UserMobile> mobilesFromUser = new HashMap<>();

        String primaryNumber = "";
        for(UserMobileRequest userMobileRequest: updateMobileRequest.getMobileList())
        {
            if(userMobileRequest.isPrimary())
            {
                primaryNumber = userMobileRequest.getNumber();
            }
            mobilesFromUpdateRequest.put(userMobileRequest.getNumber(), userMobileRequest);
        }

        boolean detailsChanged = false;

        List<UserMobile> mobiles = userMobileRepository.findAllByUserIdAndStoppedAtNull(updateMobileRequest.getUserId());
        for(UserMobile userMobile: mobiles)
        {
            String userMobileNo = userMobile.getNumber();
            if(mobilesFromUpdateRequest.containsKey(userMobileNo))
            {
                boolean isPrimary = userMobileNo.equals(primaryNumber);
                detailsChanged = (detailsChanged || (userMobile.isPrimary() != isPrimary));
                if(userMobile.isPrimary() != isPrimary)
                {
                    userMobile.setPrimary(isPrimary);
                    userMobile.setUpdatedAt(now);
                }

                boolean verified = mobilesFromUpdateRequest.get(primaryNumber).isVerified();

                if(userMobile.isVerified() != verified )
                {
                    userMobile.setVerified(verified);
                    userMobile.setUpdatedAt(now);
                    detailsChanged = true;
                }
                if(!Objects.equals(userMobile.getOwner(), mobilesFromUpdateRequest.get(userMobileNo).getOwner() )) {
                    userMobile.setOwner(mobilesFromUpdateRequest.get(userMobileNo).getOwner());
                    userMobile.setUpdatedAt(now);
                    detailsChanged = true;
                }

                mobilesFromUser.put(userMobileNo, userMobile);
            }
            else
            {
                detailsChanged = true;
                userMobile.setPrimary(false);
                userMobile.setStoppedAt(now);
            }
        }

        for(UserMobileRequest userMobileRequest: updateMobileRequest.getMobileList())
        {
            if(!mobilesFromUser.containsKey(userMobileRequest.getNumber()))
            {
                detailsChanged = true;

                mobiles.add(new UserMobile(userMobileRequest.getNumber(), updateMobileRequest.getUserId(), userMobileRequest.isPrimary(), now, now, userMobileRequest.isVerified(), userMobileRequest.getOwner()));
            }
        }

        if(detailsChanged)
        {
            userMobileRepository.saveAll(mobiles);
            publishEventForEpisode(new UserResponse(user.getId(), mobiles, null), clientId);
        }
    }

    public void updateEmails(UpdateEmailRequest updateEmailRequest, Long clientId)
    {
        updateEmailRequest.validate();

        User user = userRepository.findByIdAndIsDeletedAndClientId(updateEmailRequest.getUserId(), false, clientId);
        if(null == user)
        {
            throw new NotFoundException("User with given id not found");
        }

        Date now = new Date();

        Map<String, UserEmailRequest> emailsFromUpdateRequest = new HashMap<>();
        Map<String, UserEmail> emailsFromUser = new HashMap<>();

        String primaryEmail = "";
        for(UserEmailRequest userEmailRequest: updateEmailRequest.getEmailList())
        {
            if(userEmailRequest.isPrimary())
            {
                primaryEmail = userEmailRequest.getEmailId();
            }
            emailsFromUpdateRequest.put(userEmailRequest.getEmailId(), userEmailRequest);
        }

        boolean detailsChanged = false;

        List<UserEmail> emails = userEmailRepository.findAllByUserIdAndStoppedAtNull(updateEmailRequest.getUserId());
        for(UserEmail userEmail: emails)
        {
            if(emailsFromUpdateRequest.containsKey(userEmail.getEmailId()))
            {
                boolean isPrimary = userEmail.getEmailId().equals(primaryEmail);
                detailsChanged = (detailsChanged || (userEmail.isPrimary() != isPrimary));
                if(userEmail.isPrimary() != isPrimary)
                {
                    userEmail.setPrimary(isPrimary);
                    userEmail.setUpdatedAt(now);
                }
                emailsFromUser.put(userEmail.getEmailId(), userEmail);
            }
            else
            {
                detailsChanged = true;
                userEmail.setPrimary(false);
                userEmail.setStoppedAt(now);
            }
        }

        for(UserEmailRequest userEmailRequest: updateEmailRequest.getEmailList())
        {
            if(!emailsFromUser.containsKey(userEmailRequest.getEmailId()))
            {
                detailsChanged = true;

                emails.add(new UserEmail(userEmailRequest.getEmailId(), updateEmailRequest.getUserId(), userEmailRequest.isPrimary(), now, now));
            }
        }

        if(detailsChanged)
        {
            userEmailRepository.saveAll(emails);
            publishEventForEpisode(new UserResponse(user.getId(), null, emails), clientId);
        }
    }

    void publishEventForEpisode(Object objectToEmit, Long clientId) {
        Map<String, Object> headers = new HashMap<>();
        headers.put(Constants.CLIENT_ID, clientId);
        rabbitMQPublisherService.send(new EventStreamingDto<>(
                Constants.ELASTIC_EVENT_NAME,
                new UserResponseForEpisode(objectToEmit, Constants.MY_MODULE)
        ), Constants.DIRECT_EXCHANGE, Constants.Q_EPISODE_TRACKER, headers);
    }

    public List<UserResponse> fetchDuplicates(FetchDuplicatesRequest fetchDuplicatesRequest, Long clientId) throws Exception
    {
        fetchDuplicatesRequest.validate();

        DeduplicationHandler deduplicationHandler = deduplicationHandlerMap.getHandler(fetchDuplicatesRequest.getScheme());

        Set<String> requiredFields = fetchDuplicatesRequest.getRequiredFields();
        requiredFields.addAll(deduplicationHandler.getDefaultRequiredFields());

        return getUserResponseListFromUsers(deduplicationHandler.fetchDuplicates(fetchDuplicatesRequest, clientId), requiredFields);
    }

    @Override
    public List<UserResponse> getUserRelations(Long userId, String status) throws Exception {
        List<UserRelation> userRelations = userRelationService.getUserRelations(userId, status);
        Map<Long, UserRelation> secondaryProfileToRelation =
                userRelations.stream().collect(Collectors.toMap(
                        UserRelation::getSecondaryUserId,
                        v -> v
                ));
        List<Long> userIds = userRelations.stream().map(UserRelation::getSecondaryUserId).collect(Collectors.toList());
        List<User> activeUsers = userRepository.findAllByIdInAndIsDeletedAndClientId(userIds, false, clientService.getClientByToken().getId());
        List<UserResponse> userResponseList = getUserResponseListFromUsers(activeUsers, null);
        userResponseList.forEach(f -> {
            UserRelation userRelation = secondaryProfileToRelation.get(f.getId());
            UserRelationDto userRelationDto = new UserRelationDto(userRelation);
            f.setUserRelation(userRelationDto);
        });
        return userResponseList;
    }

    @Override
    public void addUserRelation(UserRelationDto userRelationDto) {
        userRelationService.addUserRelation(userRelationDto);
    }

    @Override
    public void updateUserRelation(UserRelationDto userRelationDto) {
        userRelationService.updateUserRelation(userRelationDto);
    }

    @Override
    public void deleteUserRelation(UserRelationDto userRelationDto) {
        userRelationService.deleteUserRelation(userRelationDto);
    }

    @Override
    public List<UserResponse> getUserRelationsPrimaryPhone(String phoneNumber, Long primaryUserId) throws Exception {
        List<UserResponse> userList = getByPrimaryPhone(phoneNumber, clientService.getClientByToken().getId());
        List<UserRelation> userRelations = userRelationService.getUserRelations(primaryUserId, UserRelationStatus.ALL.getName());
        Map<Long, UserRelation> secondaryProfileToRelation =
                userRelations.stream().collect(Collectors.toMap(
                        UserRelation::getSecondaryUserId,
                        v -> v
                ));
        userList.forEach(u -> {
            if (secondaryProfileToRelation.containsKey(u.getId())) {
                UserRelation userRelation = secondaryProfileToRelation.get(u.getId());
                u.setUserRelation(new UserRelationDto(userRelation));
            }
        });
        return userList;
    }


    @Override
    public UserResponse search(FilterUserRequest filterUserRequest, Long clientId) {

        try {
            String healthAddress = filterUserRequest.healthAddress;
            String phone = filterUserRequest.mobile;
            String healthId = filterUserRequest.healthId;

            List<UserMobile> userMobiles = StringUtils.hasText(phone) ? userMobileRepository.findAllByNumber(phone) : null;
            if(userMobiles != null && !userMobiles.isEmpty()) {
                List<Long> userIds = userMobiles.stream().map(UserMobile::getUserId).collect(Collectors.toList());
                if(!userIds.isEmpty()) {
                    return fuzzyMatchOnNameAndGender(userIds,filterUserRequest,clientId);
                }
            }

            List<ExternalId> phrAddresses = StringUtils.hasText(healthAddress) ? externalIdRepository.findAllByValueAndType(healthAddress,"HEALTH_ADDRESS") : null;
            if(phrAddresses != null && !phrAddresses.isEmpty()) {
                List<Long> userIds = phrAddresses.stream().map(ExternalId::getUserId).collect(Collectors.toList());
                if(!userIds.isEmpty()) {
                    return fuzzyMatchOnNameAndGender(userIds,filterUserRequest,clientId);
                }
            }

            List<ExternalId> healthIds = StringUtils.hasText(healthId) ? externalIdRepository.findAllByValueAndType(healthId,"HEALTH_ID") : null;
            if(healthIds != null && !healthIds.isEmpty()) {
                List<Long> userIds = healthIds.stream().map(ExternalId::getUserId).collect(Collectors.toList());
                if(!userIds.isEmpty()) {
                    return fuzzyMatchOnNameAndGender(userIds,filterUserRequest,clientId);
                }
            }

        } catch (Exception e) {
            Sentry.capture(e);
            return null;
        }
        return null;
    }

    public UserResponse fuzzyMatchOnNameAndGender(List<Long> userIds,FilterUserRequest filterUserRequest, Long clientId) throws Exception {
        String gender = filterUserRequest.gender;
        List<User> users = userRepository.findAllByIdInAndIsDeletedAndClientId(userIds,false,clientId);
        List<User> userFilterOnGender = StringUtils.hasText(gender) ? users.stream().filter(user -> user.getGender().equals(gender)).collect(Collectors.toList()) : null;
        List<User> userFilterOnDOB = new ArrayList<>();

        if(userFilterOnGender != null && !userFilterOnGender.isEmpty() && !filterUserRequest.getYearOfBirth().isEmpty()) {
            String yearOfBirth = filterUserRequest.getYearOfBirth();
            userFilterOnDOB = userFilterOnGender.stream().filter(user -> Math.abs(Utils.getYearFromDate(user.getDateOfBirth()) - Integer.parseInt(yearOfBirth)) <= ABDM_DISCOVERY_AGE_BAND).collect(Collectors.toList());
        }

        if(!userFilterOnDOB.isEmpty() && !filterUserRequest.getName().isEmpty()) {
            Map<Long,String> userNames = userFilterOnDOB.stream().collect(Collectors.toMap(User::getId, user -> user.getFirstName() + user.getLastName()));
            List<Long> userIdsByName = Utils.levenshteinMatchOnName(userNames,filterUserRequest.name);
            if(userIdsByName.size() == 1) {
                return get(userIdsByName.get(0),clientId);
            }
        }
        return null;
    }
}

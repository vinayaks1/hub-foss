package com.everwell.userservice.constants;

public class RMQConstants
{
    public static final String USER_EXCHANGE = "userservice.user_exchange";
    public static final String DATA_GATEWAY_EXCHANGE = "direct-incoming";

    public static final String ADD_USER_QUEUE = "userservice.add_user";
    public static final String DELETE_USER_QUEUE = "userservice.delete_user";
    public static final String UPDATE_USER_QUEUE = "userservice.update_user";
    public static final String UPDATE_USER_MOBILE_QUEUE = "userservice.update_user_mobile";
    public static final String UPDATE_USER_EMAIL_QUEUE = "userservice.update_user_email";
    public static final String ABDM_ADD_UPDATE_CONSENT_CONSUMER_QUEUE = "q.dg.abdm_add_update_consent";
    public static final String ABDM_ADD_UPDATE_CONSENT_RESPONSE_QUEUE = "abdm_add_update_consent";

    public static final String ADD_USER_ROUTING_KEY = "add-user";
    public static final String DELETE_USER_ROUTING_KEY = "delete-user";
    public static final String UPDATE_USER_ROUTING_KEY = "update-user";
    public static final String UPDATE_USER_MOBILE_ROUTING_KEY = "update-user-mobile";
    public static final String UPDATE_USER_EMAIL_ROUTING_KEY = "update-user-email";
    public static final String ABDM_ADD_UPDATE_CONSENT_CONSUMER_ROUTING_KEY = "q.dg.abdm_add_update_consent";
    public static final String ABDM_ADD_UPDATE_CONSENT_RESPONSE_ROUTING_KEY = "abdm-add-update-consent";

    public static final String ADD_USER_ACK_ROUTING_KEY_PREFIX = "add-user-";
    public static final String ABDM_ADD_CONSENT_REQUEST_ROUTING_KEY = "abdm-hiu-consent";
    public static final String ABDM_UPDATE_HIU_CONSENT_REQUEST_QUEUE = "userservice.update_hiu_consent";
}

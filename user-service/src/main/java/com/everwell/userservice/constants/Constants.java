package com.everwell.userservice.constants;

public class Constants
{
    public static final String ID = "id";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String GENDER = "gender";
    public static final String LANGUAGE = "language";
    public static final String DATE_OF_BIRTH = "dateOfBirth";
    public static final String ADDRESS = "address";
    public static final String ADDED_BY = "addedBy";
    public static final String ADDED_BY_TYPE = "addedByType";
    public static final String FATHER_NAME = "fatherName";
    public static final String PINCODE = "pincode";
    public static final String CITY = "city";
    public static final String MARITAL_STATUS = "maritalStatus";
    public static final String SOCIO_ECONOMIC_STATUS = "socioEconomicStatus";
    public static final String KEY_POPULATION = "keyPopulation";
    public static final String HEIGHT = "height";
    public static final String WEIGHT = "weight";
    public static final String CREATED_AT = "createdAt";
    public static final String UPDATED_AT = "updatedAt";
    public static final String DELETED_AT = "deletedAt";
    public static final String IS_DELETED = "isDeleted";
    public static final String DELETED_BY = "deletedBy";
    public static final String DELETED_BY_TYPE = "deletedByType";
    public static final String MOBILE_LIST = "mobileList";
    public static final String EMAIL_LIST = "emailList";
    public static final String PRIMARY_PHONE_NUMBER = "primaryPhoneNumber";
    public static final String MY_MODULE = "person";
    public static final String ELASTIC_EVENT_NAME = "person-update-event";
    public static final String DIRECT_EXCHANGE = "direct";
    public static final String Q_EPISODE_TRACKER = "q.episode.update_tracker";
    public static final String CLIENT_ID = "clientId";
    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String TIMESCALE_JDBC_BEAN_NAME = "timescaleNamedParameterJdbcTemplate";
    public static final String ABDM_CONSENT_SUCCESS_STATUS = "OK";
    public static final String ABDM_CONSENT_FAILURE_STATUS = "UNKNOWN";
    public static final int ABDM_EXCEPTION_CODE = 1000;
    public static final Integer ABDM_DISCOVERY_AGE_BAND = 2;

    public static final String X_US_CLIENT_ID = "X-US-Client-Id";

    public static final String X_US_AUTH_TOKEN = "X-US-Access-Token";



    public static Integer REFRESH_EXPIRY = 2 * 24 * 60 * 60 * 1000;

    public static final String[] AUTH_WHITELIST = {
            "/v1/client",
            "/v2/api-docs",
            "/configuration/ui",
            "/swagger-resources/**",
            "/configuration/security",
            "/swagger-ui.html",
            "/webjars/**",
            "/v2/api-docs/**",
            "/swagger-ui/**",
            "/actuator/**"
    };

    public static final String ABDM_CONSENT_GRANTED = "GRANTED";
    public static final String ABDM_CONSENT_EXPIRED = "EXPIRED";
    public static final String MOBILE = "mobile";
    public static final String MIDDLE_NAME = "middleName";
    public static final String DISTRICT_NAME = "districtName";
    public static final String YEAR_OF_BIRTH = "yearOfBirth";
    public static final String MONTH_OF_BIRTH = "monthOfBirth";
    public static final String DAY_OF_BIRTH = "dayOfBirth";
    public static final String AADHAR_OTP = "AADHAR_OTP";
    public static final String PRIMARY_PHONE = "PRIMARY_PHONE";
    public static final String HEALTH_ID_NUMBER = "healthIdNumber";
    public static final String HEALTH_ID = "healthId";
    public static final String HEALTH_ADDRESS = "HEALTH_ADDRESS";
    public static final String HEALTH_ID_CAPS = "HEALTH_ID";
    public static final String CLIENT_SECRET = "clientSecret";
    public static final String ACCESS_TOKEN = "accessToken";
    public static final String AUTHORIZATION = "Authorization";
    public static final String BEARER = "Bearer ";
    public static final String HIU_CONSENT_TYPE = "HIU";
    public static final String HIP_CONSENT_TYPE = "HIP";
    public static final String CONSENT_GRANTED = "GRANTED";
    public static final String CONSENT_EXPIRED = "EXPIRED";
}

package com.everwell.userservice.handlers;

import com.everwell.userservice.enums.DeduplicationScheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DeduplicationHandlerMap
{
    private final Map<DeduplicationScheme, DeduplicationHandler> handlerMap = new HashMap<>();

    @Autowired
    public DeduplicationHandlerMap(List<DeduplicationHandler> deduplicationHandlerList)
    {
        if(!CollectionUtils.isEmpty(deduplicationHandlerList)) {
            for(DeduplicationHandler deduplicationHandler: deduplicationHandlerList) {
                handlerMap.put(deduplicationHandler.getDeduplicationScheme(), deduplicationHandler);
            }
        }
    }

    public DeduplicationHandler getHandler(DeduplicationScheme deduplicationScheme)
    {
        return handlerMap.get(deduplicationScheme);
    }
}

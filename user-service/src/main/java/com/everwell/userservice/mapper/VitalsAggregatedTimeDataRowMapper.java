package com.everwell.userservice.mapper;

import com.everwell.userservice.models.dtos.vitals.VitalsAggregatedTimeData;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;


@Component
public class VitalsAggregatedTimeDataRowMapper implements RowMapper<VitalsAggregatedTimeData> {

    @Override
    public VitalsAggregatedTimeData mapRow(final ResultSet resultSet, final int rowNum) throws SQLException {
        VitalsAggregatedTimeData vitalsAggregatedTimeData = new VitalsAggregatedTimeData();
        vitalsAggregatedTimeData.setTime(resultSet.getTimestamp(1).toString());
        vitalsAggregatedTimeData.setUserId(resultSet.getLong(2));
        vitalsAggregatedTimeData.setVitalId(resultSet.getLong(3));
        vitalsAggregatedTimeData.setTotal(resultSet.getFloat(4));
        vitalsAggregatedTimeData.setAverage(resultSet.getFloat(5));
        return vitalsAggregatedTimeData;
    }
}
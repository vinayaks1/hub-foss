package com.everwell.userservice.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

import static springfox.documentation.builders.PathSelectors.regex;

@EnableSwagger2
@Profile({"dev","beta"})
@Configuration
public class SwaggerConfig {

    private final String BASE_PACKAGE = "com.everwell.userservice";

    @Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage(BASE_PACKAGE))
                .paths(regex("/.*"))
                .build()
                .apiInfo(getApiInfo());
    }

    private ApiInfo getApiInfo() {
        return new ApiInfo(
                "User Service - Api Documentation",
                "User Service is created to abstract user management features across all Everwell services. Documentation Link: [User Service](https://sites.google.com/a/everwell.org/engineering/tech-integrations/user-service)",
                "V1",
                "",
                new Contact("Everwell", "https://www.everwell.org/", "contact@everwell.org"),
                "",
                "",
                Collections.emptyList());
    }

}

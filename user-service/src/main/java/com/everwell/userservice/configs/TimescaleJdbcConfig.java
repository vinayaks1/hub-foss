package com.everwell.userservice.configs;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.sql.SQLException;

import static com.everwell.userservice.constants.Constants.TIMESCALE_JDBC_BEAN_NAME;

@Configuration
public class TimescaleJdbcConfig {

    @Value("${timescale.validateCredentialsOnStartup}")
    private boolean validateTimeScaleCredentials;

    @Value("${timescale.datasource.driverClassName}")
    private String driverName;

    @Value("${timescale.datasource.url}")
    private String url;

    @Value("${timescale.datasource.username}")
    private String username;

    @Value("${timescale.datasource.password}")
    private String password;

    @Bean
    @Qualifier(TIMESCALE_JDBC_BEAN_NAME)
    public NamedParameterJdbcTemplate namedParameterJdbcTemplate() throws SQLException {
        DriverManagerDataSource timeScaleDataSource = new DriverManagerDataSource();
        timeScaleDataSource.setDriverClassName(driverName);
        timeScaleDataSource.setUrl(url);
        timeScaleDataSource.setUsername(username);
        timeScaleDataSource.setPassword(password);
        if (validateTimeScaleCredentials) {
            timeScaleDataSource.getConnection();
        }
        return new NamedParameterJdbcTemplate(timeScaleDataSource);
    }
}

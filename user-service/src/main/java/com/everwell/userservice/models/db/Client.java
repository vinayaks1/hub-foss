package com.everwell.userservice.models.db;

import com.everwell.userservice.utils.Utils;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "user_client")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Client
{
    @Id
    @Column
    @SequenceGenerator(name="pk_sequence_user_client",sequenceName="user_client_id_seq", allocationSize=1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="pk_sequence_user_client")
    private Long id;

    private String name;

    private String password;

    private Date createdDate;

    private Long eventFlowId;

    public Client(String name, String password) {
        this.name = name;
        this.password = password;
        this.createdDate = Utils.getCurrentDate();
        this.eventFlowId = null;
    }

    public Client(Long id, String name, String password, Date createdDate) {
        this(name, password);
        this.id = id;
        this.createdDate = createdDate;
        this.eventFlowId = null;
    }
}

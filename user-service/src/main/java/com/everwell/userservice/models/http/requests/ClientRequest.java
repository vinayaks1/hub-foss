package com.everwell.userservice.models.http.requests;

import com.everwell.userservice.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;


@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ClientRequest {
    private String name;

    private String password;

    public void validate() {
        if(StringUtils.isEmpty(name)) {
            throw new ValidationException("client name cannot be null or empty");
        } else if(name.length() > 255) {
            throw new ValidationException("client name length should be less than 255 characters");
        }
        if(StringUtils.isEmpty(password)) {
            throw new ValidationException("client password cannot be null or empty");
        } else if (password.length() > 255) {
            throw new ValidationException("client password length should be less than 255 characters");
        }
    }

}

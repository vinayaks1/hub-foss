package com.everwell.userservice.models.http.responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VeriftyOTPForMobileLoginResponse {
    private String txnId;
    private String token;
    private List<MobileLinkedHid>mobileLinkedHid;
}

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
class MobileLinkedHid{
    private String healthIdNumber;
    private String healthId;
    private String name;
    private String profilePhoto;
    private String phrAddress;
}

package com.everwell.userservice.models.db;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "user_emails")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserEmail
{
    @Id
    @Column
    @SequenceGenerator(name="pk_sequence_user_emails",sequenceName="user_emails_id_seq", allocationSize=1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="pk_sequence_user_emails")
    private Long id;

    @Column
    private Long userId;

    @Column
    private String emailId;

    @Column
    private boolean isPrimary;

    @Column
    private boolean isVerified;

    @Column
    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    private Date createdAt;

    @Column
    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    private Date updatedAt;

    @Column
    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    private Date stoppedAt;

    public UserEmail(String emailId, Long userId, boolean isPrimary, Date createdAt, Date updatedAt)
    {
        this.emailId = emailId;
        this.userId = userId;
        this.isPrimary = isPrimary;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }
}

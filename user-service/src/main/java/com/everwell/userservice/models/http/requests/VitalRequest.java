package com.everwell.userservice.models.http.requests;


import com.everwell.userservice.enums.VitalValidation;
import com.everwell.userservice.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class VitalRequest {
    private String name;

    private Float lowValue;

    private Float highValue;

    private Float criticalLowValue;

    private Float criticalHighValue;

    private String unitOfMeasurement;

    private boolean containsAssociations;

    public void validate() {
        if (StringUtils.isEmpty(name)) {
            throw new ValidationException(VitalValidation.VITAL_NAME_INVALID.getMessage());
        }
    }
}

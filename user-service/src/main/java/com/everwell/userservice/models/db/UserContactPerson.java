package com.everwell.userservice.models.db;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "user_contact_person")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserContactPerson {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column
    private Long userId;

    @Column
    private String address;

    @Column
    private String name;

    @Column
    private String phone;

    public UserContactPerson(Long userId, String address, String name, String phone) {
        this.userId = userId;
        this.address = address;
        this.name = name;
        this.phone = phone;
    }

}

package com.everwell.userservice.models.http.responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ABHAProfileResponse extends ABHACreateResponse {
    private String address;
    private Boolean emailVerified;
    private Boolean kycVerified;
    private List<String> phrAddress;
    private String pincode;
    private String subDistrictCode;
    private String subdistrictName;
    private String townCode;
    private String townName;
    private String verificationStatus;
    private String verificationType;
    private String villageCode;
    private String villageName;
    private String wardCode;
    private String wardName;
}
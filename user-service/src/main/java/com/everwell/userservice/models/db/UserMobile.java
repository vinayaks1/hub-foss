package com.everwell.userservice.models.db;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "user_mobiles")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserMobile
{
    @Id
    @Column
    @SequenceGenerator(name="pk_sequence_user_mobiles",sequenceName="user_mobiles_id_seq", allocationSize=1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="pk_sequence_user_mobiles")
    private Long id;

    @Column
    private Long userId;

    @Column(length = 15)
    private String number;

    @Column
    private boolean isPrimary;

    @Column
    private boolean isVerified;

    @Column
    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    private Date createdAt;

    @Column
    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    private Date updatedAt;

    @Column
    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    private Date stoppedAt;

    @Column
    private String owner;

    public UserMobile(String number, Long userId, boolean isPrimary, Date createdAt, Date updatedAt, boolean isVerified, String owner)
    {
        this.number = number;
        this.userId = userId;
        this.isPrimary = isPrimary;
        this.isVerified = isVerified;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.owner = owner;
    }
}

package com.everwell.userservice.models.dtos.abdm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateConsentDto {
    String userId;
    String consentRequestId;
    String consentId;
    ConsentNotificationDto abdmConsentNotification;
}

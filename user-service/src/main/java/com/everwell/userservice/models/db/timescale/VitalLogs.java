package com.everwell.userservice.models.db.timescale;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class VitalLogs {
    private LocalDateTime time;

    private Long userId;

    private Long vitalId;

    private Float vitalValue;

    public VitalLogs(Long userId, Long vitalId, Float vitalValue) {
        this.userId = userId;
        this.vitalId = vitalId;
        this.vitalValue = vitalValue;
    }
}

package com.everwell.userservice.models.http.requests;

import com.everwell.userservice.exceptions.ValidationException;
import lombok.*;
import org.apache.commons.lang3.StringUtils;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class MobileEmailOTPRequest {
    private String value;
    private String purpose;
    private String authMode;
    private Long userId;
    private MobileEmailOTPRequester requester;

    public void validate() {
        if(StringUtils.isEmpty(value)) {
            throw new ValidationException("value cannot be null or empty");
        }else if(StringUtils.isEmpty(purpose)) {
            throw new ValidationException("purpose cannot be null or empty");
        }else if(userId==null) {
            throw new ValidationException("userId cannot be null or empty");
        }else if(StringUtils.isEmpty(authMode)) {
            throw new ValidationException("authMode cannot be null or empty");
        }
    }
}

package com.everwell.userservice.models.http.requests;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Intent{
    private String type;
}

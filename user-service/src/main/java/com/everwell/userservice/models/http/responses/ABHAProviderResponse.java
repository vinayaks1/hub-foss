package com.everwell.userservice.models.http.responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ABHAProviderResponse{
    private ABHAProviderIdentifier identifier;
    private String telephone;
    private String city;
    private String latitude;
    private String longitude;
    private String address;
    private String districtCode;
    private String stateCode;
    private String pinCode;
    private List<String> facilityType;
    private String isHIP;
    private String attributes;
}


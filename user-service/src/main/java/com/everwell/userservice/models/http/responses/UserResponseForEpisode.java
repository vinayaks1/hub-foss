package com.everwell.userservice.models.http.responses;

import com.everwell.userservice.models.dtos.UserResponse;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserResponseForEpisode {
    Object fields;
    String module;
}

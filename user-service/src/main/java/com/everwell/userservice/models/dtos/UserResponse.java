package com.everwell.userservice.models.dtos;

import com.everwell.userservice.constants.Constants;
import com.everwell.userservice.models.db.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class UserResponse
{
    private Long id;

    private String firstName;

    private String lastName;

    private String gender;

    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    private Date dateOfBirth;

    private String language;

    private String address;

    private Long addedBy;

    private String addedByType;

    private String fatherName;

    private Integer pincode;

    private String city;

    private String maritalStatus;

    private String socioEconomicStatus;

    private String keyPopulation;

    private Integer height;

    private Integer weight;

    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    private Date createdAt;

    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    private Date updatedAt;

    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    private Date deletedAt;

    private boolean isDeleted;

    private Long deletedBy;

    private String deletedByType;

    private Long clientId;

    private List<UserMobile> mobileList;

    private List<UserEmail> emailList;

    private String primaryPhoneNumber;

    private List<VerificationResponse> verifications;

    private List<ExternalIdResponse> externalIds;

    private boolean dataConsent;

    private String occupation;

    private String taluka;

    private String town;

    private String ward;

    private String landmark;

    private String area;

    private String contactPersonAddress;

    private String contactPersonName;

    private String contactPersonPhone;

    private Long age;

    private UserRelationDto userRelation;

    public UserResponse(UserResponse referenceUser, Set<String> requiredFields)
    {
        if((null == requiredFields) || requiredFields.contains(Constants.ID))
        {
            this.id = referenceUser.getId();
        }

        if((null == requiredFields) || requiredFields.contains(Constants.FIRST_NAME))
        {
            this.firstName = referenceUser.getFirstName();
        }

        if((null == requiredFields) || requiredFields.contains(Constants.LAST_NAME))
        {
            this.lastName = referenceUser.getLastName();
        }

        if((null == requiredFields) || requiredFields.contains(Constants.GENDER))
        {
            this.gender = referenceUser.getGender();
        }

        if((null == requiredFields) || requiredFields.contains(Constants.LANGUAGE))
        {
            this.language = referenceUser.getLanguage();
        }

        if((null == requiredFields) || requiredFields.contains(Constants.DATE_OF_BIRTH))
        {
            this.dateOfBirth = referenceUser.getDateOfBirth();
        }

        if((null == requiredFields) || requiredFields.contains(Constants.ADDRESS))
        {
            this.address = referenceUser.getAddress();
        }

        if((null == requiredFields) || requiredFields.contains(Constants.ADDED_BY))
        {
            this.addedBy = referenceUser.getAddedBy();
        }

        if((null == requiredFields) || requiredFields.contains(Constants.ADDED_BY_TYPE))
        {
            this.addedByType = referenceUser.getAddedByType();
        }

        if((null == requiredFields) || requiredFields.contains(Constants.FATHER_NAME))
        {
            this.fatherName = referenceUser.getFatherName();
        }

        if((null == requiredFields) || requiredFields.contains(Constants.PINCODE))
        {
            this.pincode = referenceUser.getPincode();
        }

        if((null == requiredFields) || requiredFields.contains(Constants.CITY))
        {
            this.city = referenceUser.getCity();
        }

        if((null == requiredFields) || requiredFields.contains(Constants.MARITAL_STATUS))
        {
            this.maritalStatus = referenceUser.getMaritalStatus();
        }

        if((null == requiredFields) || requiredFields.contains(Constants.SOCIO_ECONOMIC_STATUS))
        {
            this.socioEconomicStatus = referenceUser.getSocioEconomicStatus();
        }

        if((null == requiredFields) || requiredFields.contains(Constants.KEY_POPULATION))
        {
            this.keyPopulation = referenceUser.getKeyPopulation();
        }

        if((null == requiredFields) || requiredFields.contains(Constants.HEIGHT))
        {
            this.height = referenceUser.getHeight();
        }

        if((null == requiredFields) || requiredFields.contains(Constants.WEIGHT))
        {
            this.weight = referenceUser.getWeight();
        }

        if((null == requiredFields) || requiredFields.contains(Constants.CREATED_AT))
        {
            this.createdAt = referenceUser.getCreatedAt();
        }

        if((null == requiredFields) || requiredFields.contains(Constants.UPDATED_AT))
        {
            this.updatedAt = referenceUser.getUpdatedAt();
        }

        if((null == requiredFields) || requiredFields.contains(Constants.DELETED_AT))
        {
            this.deletedAt = referenceUser.getDeletedAt();
        }

        if((null == requiredFields) || requiredFields.contains(Constants.IS_DELETED))
        {
            this.isDeleted = referenceUser.isDeleted();
        }

        if((null == requiredFields) || requiredFields.contains(Constants.DELETED_BY))
        {
            this.deletedBy = referenceUser.getDeletedBy();
        }

        if((null == requiredFields) || requiredFields.contains(Constants.DELETED_BY_TYPE))
        {
            this.deletedByType = referenceUser.getDeletedByType();
        }

        if((null == requiredFields) || requiredFields.contains(Constants.MOBILE_LIST))
        {
            this.mobileList = referenceUser.getMobileList();
        }

        if((null == requiredFields) || requiredFields.contains(Constants.EMAIL_LIST))
        {
            this.emailList = referenceUser.getEmailList();
        }

        if((null == requiredFields) || requiredFields.contains(Constants.PRIMARY_PHONE_NUMBER))
        {
            this.primaryPhoneNumber = referenceUser.primaryPhone();
        }
    }

    public String primaryPhone()
    {
        String primaryPhone = "";

        if(null != this.getMobileList())
        {
            for(UserMobile userMobile: this.getMobileList())
            {
                if(userMobile.isPrimary())
                {
                    primaryPhone = userMobile.getNumber();
                    break;
                }
            }
        }

        return primaryPhone;
    }

    public UserResponse(Long id, List<UserMobile> mobileList, List<UserEmail> emailList) {
        this.id = id;
        this.mobileList = mobileList;
        this.emailList = emailList;
    }

    public void setUserAddress(UserAddress userAddress) {
        this.taluka = userAddress.getTaluka();
        this.town = userAddress.getTown();
        this.ward = userAddress.getWard();
        this.landmark = userAddress.getLandmark();
        this.area = userAddress.getArea();
    }

    public void setUserContactPerson(UserContactPerson userContactPerson) {
        this.contactPersonAddress = userContactPerson.getAddress();
        this.contactPersonName = userContactPerson.getName();
        this.contactPersonPhone = userContactPerson.getPhone();
    }

    public void setAgeFromDateOfBirth(Date dateOfBirth) {
        if (dateOfBirth == null)
            this.age = null;
        else {
            LocalDateTime dateOfBirthV2 = LocalDateTime.ofInstant(Instant.ofEpochMilli(dateOfBirth.getTime()), ZoneId.systemDefault());
            this.age = ChronoUnit.YEARS.between(dateOfBirthV2, LocalDateTime.now());
        }
    }

}

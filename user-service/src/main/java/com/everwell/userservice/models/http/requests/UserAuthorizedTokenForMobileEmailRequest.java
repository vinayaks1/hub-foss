package com.everwell.userservice.models.http.requests;

import com.everwell.userservice.exceptions.ValidationException;
import lombok.*;
import org.apache.commons.lang3.StringUtils;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UserAuthorizedTokenForMobileEmailRequest {
    private String patientId;
    private String transactionId;
    private String requesterId;
    private Long userId;

    public void validate() {
        if(StringUtils.isEmpty(patientId)) {
            throw new ValidationException("patientId cannot be null or empty");
        }else if(userId==null) {
            throw new ValidationException("userId cannot be null or empty");
        }
    }
}

package com.everwell.userservice.models.http.responses;

import com.everwell.userservice.models.db.Vital;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class VitalResponse {
    private Long id;

    private String name;

    private Float lowValue;

    private Float highValue;

    private Float criticalLowValue;

    private Float criticalHighValue;

    private String unitOfMeasurement;

    private Boolean containsAssociations;

    public VitalResponse(Vital vital) {
        this.id = vital.getId();
        this.name = vital.getName();
        this.lowValue = vital.getLowValue();
        this.highValue = vital.getHighValue();
        this.criticalLowValue = vital.getCriticalLowValue();
        this.criticalHighValue = vital.getCriticalHighValue();
        this.unitOfMeasurement = vital.getUnitOfMeasurement();
        this.containsAssociations= vital.isContainsAssociations();
    }
}

package com.everwell.userservice.models.dtos.abdm;

import lombok.Data;

import java.util.List;

@Data
public class ConsentNotificationDto {
    public String requestId;
    public String timestamp;
    public Notification notification;

    @Data
    public static class ConsentArtefact {
        public String id;
    }

    @Data
    public static class Notification{
        public String consentRequestId;
        public String status;
        public List<ConsentArtefact> consentArtefacts;
    }
}

package com.everwell.userservice.models.db;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "user_address")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserAddress {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column
    private Long userId;

    @Column
    private String taluka;

    @Column
    private String town;

    @Column
    private String ward;

    @Column
    private String landmark;

    @Column
    private String area;

    public UserAddress(Long userId, String taluka, String town, String ward, String landmark, String area) {
        this.userId = userId;
        this.taluka = taluka;
        this.town = town;
        this.ward = ward;
        this.landmark = landmark;
        this.area = area;
    }
}

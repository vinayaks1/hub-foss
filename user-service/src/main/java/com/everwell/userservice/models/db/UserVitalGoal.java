package com.everwell.userservice.models.db;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class UserVitalGoal {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column
    private Long userId;

    @Column
    private Long vitalId;

    @Column
    private Float value;

    @Column
    private LocalDateTime startDate;

    @Setter
    @Column
    private LocalDateTime endDate;

    public UserVitalGoal(Long userId, Long vitalId, Float value, LocalDateTime startDate, LocalDateTime endDate){
        this.userId = userId;
        this.vitalId = vitalId;
        this.value = value;
        this.startDate = startDate;
        this.endDate = endDate;
    }

}

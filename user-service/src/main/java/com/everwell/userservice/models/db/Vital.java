package com.everwell.userservice.models.db;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class Vital {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private Float lowValue;

    @Column
    private Float highValue;

    @Column
    private Float criticalLowValue;

    @Column
    private Float  criticalHighValue;

    @Column
    private String unitOfMeasurement;

    @Column
    private boolean containsAssociations;

    public Vital(String name, Float lowValue, Float highValue, Float criticalLowValue, Float criticalHighValue, String unitOfMeasurement, boolean containsAssociations){
        this.name = name;
        this.lowValue = lowValue;
        this.highValue = highValue;
        this.criticalLowValue = criticalLowValue;
        this.criticalHighValue = criticalHighValue;
        this.unitOfMeasurement = unitOfMeasurement;
        this.containsAssociations = containsAssociations;
    }

}

package com.everwell.userservice.models.dtos.vitals;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class VitalsTimeData {
    private String time;
    private Long userId;
    private Long vitalId;
    private String value;
}

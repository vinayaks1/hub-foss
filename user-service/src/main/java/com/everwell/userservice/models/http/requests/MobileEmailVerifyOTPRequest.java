package com.everwell.userservice.models.http.requests;

import com.everwell.userservice.exceptions.ValidationException;
import lombok.*;
import org.apache.commons.lang3.StringUtils;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class MobileEmailVerifyOTPRequest {
    private String transactionId;
    private String authCode;
    private String requesterId;
    private Long userId;

    public void validate() {
        if(userId==null) {
            throw new ValidationException("userId cannot be null or empty");
        }else if(StringUtils.isEmpty(authCode)) {
            throw new ValidationException("authCode cannot be null or empty");
        }
    }
}

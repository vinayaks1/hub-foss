package com.everwell.userservice.models.http.requests;

import com.everwell.userservice.exceptions.ValidationException;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.apache.commons.lang3.StringUtils;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResendOTPRequest {
    private String txnId;
    private String authMethod;

    public void validate() {
        if(StringUtils.isEmpty(txnId)) {
            throw new ValidationException("txnId cannot be null or empty");
        }
    }

    public void validateForAuth() {
        if(StringUtils.isEmpty(txnId)) {
            throw new ValidationException("txnId cannot be null or empty");
        } else if(StringUtils.isEmpty(authMethod)) {
            throw new ValidationException("authMethod cannot be null or empty");
        }
    }
}

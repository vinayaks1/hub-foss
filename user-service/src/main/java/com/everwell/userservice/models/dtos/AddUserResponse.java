package com.everwell.userservice.models.dtos;

import com.everwell.userservice.enums.UserType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AddUserResponse
{
    private Long userId;
    private String entityId;
    private UserType userType;
}

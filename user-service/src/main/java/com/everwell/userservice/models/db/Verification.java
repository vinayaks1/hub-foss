package com.everwell.userservice.models.db;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "verifications")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Verification
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Long userId;

    @Column(length = 25)
    private String type;

    @Column(length = 25)
    private String entityType;

    @Column(length = 25)
    private String entityValue;

    @Column(length = 50)
    private String transactionId;

    @Column
    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    private Date createdAt;

    @Column
    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    private Date stoppedAt;
}

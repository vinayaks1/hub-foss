package com.everwell.userservice.models.db;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "abdm_consent_details")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ABDMConsent
{
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "consent_id")
    private String consentId;

    @Column(name = "json_data", length = 5000)
    private String data;

    @Column
    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    private Date createdAt;

    @Column
    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    private Date updatedAt;

    @Column
    private Long userId;

    @Column(name = "consent_type")
    private String consentType;

    public ABDMConsent(String consentId, Date createdAt,String consentType){
        this.consentId = consentId;
        this.createdAt = createdAt;
        this.consentType = consentType;
    }

    public ABDMConsent(Long userId,String consentType){
        this.userId = userId;
        this.createdAt = new Date();
        this.consentType = consentType;
    }

}

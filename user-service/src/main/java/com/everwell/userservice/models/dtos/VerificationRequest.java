package com.everwell.userservice.models.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class VerificationRequest
{
    private String type;
    private String entityType;
    private String entityValue;
    private String transactionId;
}

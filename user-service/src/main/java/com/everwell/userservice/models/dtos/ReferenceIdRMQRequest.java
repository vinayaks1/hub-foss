package com.everwell.userservice.models.dtos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReferenceIdRMQRequest {
    public Long referenceId;
    public String userName;
    public String data;
    public Long userId;
}

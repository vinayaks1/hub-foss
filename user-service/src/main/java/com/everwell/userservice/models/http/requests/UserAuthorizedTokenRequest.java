package com.everwell.userservice.models.http.requests;

import com.everwell.userservice.exceptions.ValidationException;
import lombok.*;
import org.apache.commons.lang3.StringUtils;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UserAuthorizedTokenRequest {
    private String healthId;
    private String txnId;

    public void validate() {
        if(StringUtils.isEmpty(healthId)) {
            throw new ValidationException("healthId cannot be null or empty");
        } else if(StringUtils.isEmpty(txnId)) {
            throw new ValidationException("txnId cannot be null or empty");
        }
    }

}

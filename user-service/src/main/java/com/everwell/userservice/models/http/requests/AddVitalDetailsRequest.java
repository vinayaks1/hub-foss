package com.everwell.userservice.models.http.requests;


import com.everwell.userservice.enums.VitalValidation;
import com.everwell.userservice.exceptions.ValidationException;
import com.everwell.userservice.models.dtos.vitals.VitalDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class AddVitalDetailsRequest {
    private List<VitalDto> vitalDetails;

    public void validate() {
        if (CollectionUtils.isEmpty(vitalDetails)) {
            throw new ValidationException(VitalValidation.VITAL_DETAILS_INVALID.getMessage());
        }
        vitalDetails.forEach(vitalDto -> {
            if (null == vitalDto.getVitalId() && StringUtils.isEmpty(vitalDto.getVitalName())) {
                throw new ValidationException(VitalValidation.VITAL_ID_OR_VITAL_NAME.getMessage());
            }
            if (StringUtils.isEmpty(vitalDto.getValue())) {
                throw new ValidationException(VitalValidation.VITAL_VALUE_INVALID.getMessage());
            }
        });
    }

}

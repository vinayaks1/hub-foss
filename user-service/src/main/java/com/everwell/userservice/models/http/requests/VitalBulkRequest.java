package com.everwell.userservice.models.http.requests;

import com.everwell.userservice.enums.VitalValidation;
import com.everwell.userservice.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class VitalBulkRequest {
    List<VitalRequest> vitalsDetails;

    public void validate() {
        if (CollectionUtils.isEmpty(vitalsDetails)) {
            throw new ValidationException(VitalValidation.VITAL_LIST_INVALID.getMessage());
        }
        vitalsDetails.forEach(VitalRequest::validate);
    }
}

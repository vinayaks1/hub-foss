package com.everwell.userservice.models.dtos;

import com.everwell.userservice.exceptions.ValidationException;
import com.everwell.userservice.utils.Utils;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown=true)
public class UpdateMobileRequest
{
    private Long userId;
    private List<UserMobileRequest> mobileList = new ArrayList<>();

    public void validate()
    {
        if((null == mobileList) || mobileList.isEmpty())
        {
            throw new ValidationException("Must have at least one mobile number");
        }

        for(UserMobileRequest userMobileRequest: mobileList)
        {
            userMobileRequest.validate();
        }

        //ensuring there exists only 1 primary number
        Utils.ensureSinglePrimaryMobile(mobileList);
        mobileList = Utils.removeDuplicateNumbers(mobileList);
    }
}

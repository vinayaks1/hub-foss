package com.everwell.userservice.models.http.responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ABHAAuthResponse {
    private Long expiresIn;
    private Long refreshExpiresIn;
    private String refreshToken;
    private String token;
}
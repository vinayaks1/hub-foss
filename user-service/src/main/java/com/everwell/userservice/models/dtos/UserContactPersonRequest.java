package com.everwell.userservice.models.dtos;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserContactPersonRequest
{
    private String contactPersonAddress;

    private String contactPersonName;

    private String contactPersonPhone;

}

package com.everwell.userservice.models.http.responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ABHACreateResponse {
    private List<String> authMethods;
    private String dayOfBirth;
    private String districtCode;
    private String districtName;
    private String email;
    private String firstName;
    private String gender;
    private String healthId;
    private String healthIdNumber;
    private String kycPhoto;
    private String lastName;
    private String middleName;
    private String mobile;
    private String monthOfBirth;
    private String name;
    private String profilePhoto;
    private String refreshToken;
    private String stateCode;
    private String stateName;
    private String token;
    private String yearOfBirth;
    private Map<String, String> tags;
}
package com.everwell.userservice.models.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown=true)
public class UpdateUserRequest
{
    private Long userId;

    private String firstName;

    private String lastName;

    private String gender;

    private String dateOfBirth;

    private String language;

    private String address;

    private String fatherName;

    private Integer pincode;

    private String city;

    private String maritalStatus;

    private String socioEconomicStatus;

    private String keyPopulation;

    private Integer height;

    private Integer weight;

    private List<VerificationRequest> verifications;

    private List<ExternalIdRequest> externalIds;

    private Boolean dataConsent;

    private String occupation;

    private String taluka;

    private String town;

    private String ward;

    private String landmark;

    private String area;

    private String contactPersonAddress;

    private String contactPersonName;

    private String contactPersonPhone;

    private List<UserMobileRequest> mobileList = new ArrayList<>();

    private List<UserEmailRequest> emailList = new ArrayList<>();
  
    public void validate()
    {

    }
}
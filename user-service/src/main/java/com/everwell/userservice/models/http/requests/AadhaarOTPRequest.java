package com.everwell.userservice.models.http.requests;

import com.everwell.userservice.exceptions.ValidationException;
import com.everwell.userservice.utils.EncryptionUtils;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class AadhaarOTPRequest {
    private String aadhaar;

    public void validate() {
        if(StringUtils.isEmpty(aadhaar)) {
            throw new ValidationException("aadhaar cannot be null or empty");
        }
    }

    public void decryptData() {
        this.aadhaar = EncryptionUtils.decrypt(aadhaar);
    }
}

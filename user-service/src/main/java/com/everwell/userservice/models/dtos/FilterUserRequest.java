package com.everwell.userservice.models.dtos;

import com.everwell.userservice.exceptions.ValidationException;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.StringUtils;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown=true)
public class FilterUserRequest {

    public String healthAddress;
    public String healthId;
    public String mobile;
    public String name;
    public String gender;
    public String yearOfBirth;

    public void validate() {

        if(!StringUtils.hasText(name)) {
            throw new ValidationException("name cannot be empty");
        }

        if(!StringUtils.hasText(gender)) {
            throw new ValidationException("gender cannot be empty");
        }

        if(!StringUtils.hasText(yearOfBirth)) {
            throw new ValidationException("yearOfBirth cannot be empty");
        }
    }
}

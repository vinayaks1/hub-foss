# Registry Service

Registry Service is created to abstract out the functionality, staff, triggers and roles of different facilities based on hierarchy and deployment codes across all Everwell Services.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Things to be installed to run Registry Service

* Java 11
* IntelliJ IDEA / Eclipse or any editor capable of running Java
* Install Maven
* Add Lombok Plugin

For running pure development environment

* Install Postgres
* Install Redis
* Install Rabbitmq

While running the project for the first time locally, register default client [Swagger](https://registryservice.dev.everwell.org/swagger-ui/index.html)

### Installing and Deployment

A step by step series of examples that tell you how to get a development env running

Use the deployment commands for easy startup options.
The options for env are among dev, oncall, beta, nikshay and hub.
```
mvn clean compile -P$env package
java -jar registryservice/target/registryservice-0.0.1-SNAPSHOT.jar
```
The deployment commands builds a new package of the java project and runs the unit tests.
Once the deployment is done, we can look at the log to ascertain the application has started
```
2022-10-25 20:10:32.759  INFO 21656 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 9210 (http) with context path ''
2022-10-25 20:10:33.599  INFO 21656 --- [           main] c.everwell.registryservice.Application   : Started Application in 36.452 seconds (JVM running for 37.583)
```

### Building, Compilation and Packaging

For individual compiling of the project, we run the maven commands to build and compile

```
mvn clean compile
```

For packaging of the project in an executable format.
This would execute all the compilation build and test commands before packaging
```
mvn clean compile package
```

For individual running of the project, we run the maven commands

```
mvn spring-boot:run
```

## Running the tests

Registry Service uses JUnit to run tests

Run the following command to evaluate the entire test suite
```
mvn test
```

Run the following command to evaluate individual unit tests
```
mvn test -Dtest=<testfilename>
```

## Built With

* [Springboot](https://spring.io/projects/spring-boot) - The Java framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [Mockito](https://github.com/mockito/mockito) - Used to generate mocks for testing independently

## License

This project is licensed under the MIT License
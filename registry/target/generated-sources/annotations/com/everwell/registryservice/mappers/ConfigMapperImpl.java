package com.everwell.registryservice.mappers;

import com.everwell.registryservice.models.db.Config;
import com.everwell.registryservice.models.db.HierarchyConfigMap;
import com.everwell.registryservice.models.http.requests.AddConfigRequest;
import com.everwell.registryservice.models.http.response.ConfigHierarchyMappingBaseResponse;
import com.everwell.registryservice.models.http.response.ConfigHierarchyMappingRecordResponse;
import com.everwell.registryservice.models.http.response.ConfigResponse;
import javax.annotation.processing.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-10-25T14:15:57+0530",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 11.0.2 (Oracle Corporation)"
)
public class ConfigMapperImpl implements ConfigMapper {

    @Override
    public ConfigResponse configToConfigResponse(Config config) {
        if ( config == null ) {
            return null;
        }

        ConfigResponse configResponse = new ConfigResponse();

        configResponse.setId( config.getId() );
        configResponse.setName( config.getName() );
        configResponse.setDefaultValue( config.getDefaultValue() );
        configResponse.setType( config.getType() );
        configResponse.setMultiMapped( config.isMultiMapped() );

        return configResponse;
    }

    @Override
    public Config addConfigRequestToConfig(AddConfigRequest addConfigRequest) {
        if ( addConfigRequest == null ) {
            return null;
        }

        Config config = new Config();

        config.setName( addConfigRequest.getName() );
        config.setDefaultValue( addConfigRequest.getDefaultValue() );
        config.setType( addConfigRequest.getType() );

        return config;
    }

    @Override
    public ConfigHierarchyMappingRecordResponse hierarchyConfigMapToConfigHierarchyMappingRecordResponse(HierarchyConfigMap hierarchyConfigMap) {
        if ( hierarchyConfigMap == null ) {
            return null;
        }

        ConfigHierarchyMappingRecordResponse configHierarchyMappingRecordResponse = new ConfigHierarchyMappingRecordResponse();

        configHierarchyMappingRecordResponse.setHierarchyId( hierarchyConfigMap.getHierarchyId() );
        configHierarchyMappingRecordResponse.setValue( hierarchyConfigMap.getValue() );
        configHierarchyMappingRecordResponse.setActive( hierarchyConfigMap.isActive() );

        return configHierarchyMappingRecordResponse;
    }

    @Override
    public ConfigHierarchyMappingBaseResponse hierarchyConfigMapToConfigHierarchyMappingBaseResponse(HierarchyConfigMap hierarchyConfigMap) {
        if ( hierarchyConfigMap == null ) {
            return null;
        }

        ConfigHierarchyMappingBaseResponse configHierarchyMappingBaseResponse = new ConfigHierarchyMappingBaseResponse();

        configHierarchyMappingBaseResponse.setConfigId( hierarchyConfigMap.getConfigMappingId() );
        configHierarchyMappingBaseResponse.setId( hierarchyConfigMap.getId() );
        configHierarchyMappingBaseResponse.setHierarchyId( hierarchyConfigMap.getHierarchyId() );
        configHierarchyMappingBaseResponse.setValue( hierarchyConfigMap.getValue() );
        configHierarchyMappingBaseResponse.setActive( hierarchyConfigMap.isActive() );

        return configHierarchyMappingBaseResponse;
    }
}

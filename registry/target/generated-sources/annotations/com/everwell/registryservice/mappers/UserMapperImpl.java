package com.everwell.registryservice.mappers;

import com.everwell.registryservice.models.db.UserAccess;
import com.everwell.registryservice.models.dto.UserDetails;
import com.everwell.registryservice.models.http.requests.CreateUserRequest;
import javax.annotation.processing.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-10-25T14:15:57+0530",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 11.0.2 (Oracle Corporation)"
)
public class UserMapperImpl implements UserMapper {

    @Override
    public UserAccess createUserRequestToUserAccess(CreateUserRequest createUserRequest) {
        if ( createUserRequest == null ) {
            return null;
        }

        UserAccess userAccess = new UserAccess();

        userAccess.setUsername( createUserRequest.getUsername() );
        if ( createUserRequest.getHierarchyId() != null ) {
            userAccess.setHierarchyId( createUserRequest.getHierarchyId() );
        }
        if ( createUserRequest.getSelectiveHierarchyMapping() != null ) {
            userAccess.setSelectiveHierarchyMapping( createUserRequest.getSelectiveHierarchyMapping() );
        }
        if ( createUserRequest.getSsoId() != null ) {
            userAccess.setSsoId( createUserRequest.getSsoId() );
        }

        return userAccess;
    }

    @Override
    public UserDetails userAccessToUserDetails(UserAccess userAccess) {
        if ( userAccess == null ) {
            return null;
        }

        UserDetails userDetails = new UserDetails();

        userDetails.setUsername( userAccess.getUsername() );
        userDetails.setHierarchyId( userAccess.getHierarchyId() );
        userDetails.setSelectiveHierarchyMapping( userAccess.isSelectiveHierarchyMapping() );
        userDetails.setSsoId( userAccess.getSsoId() );
        userDetails.setActive( userAccess.isActive() );

        return userDetails;
    }
}

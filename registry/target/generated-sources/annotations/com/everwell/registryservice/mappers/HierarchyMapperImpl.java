package com.everwell.registryservice.mappers;

import com.everwell.registryservice.models.db.Hierarchy;
import com.everwell.registryservice.models.dto.HierarchyRequestData;
import com.everwell.registryservice.models.dto.HierarchyResponseData;
import com.everwell.registryservice.models.dto.HierarchyUpdateData;
import com.everwell.registryservice.models.dto.MinHierarchyDetails;
import com.everwell.registryservice.models.http.requests.UserUpdateHierarchyRequest;
import com.everwell.registryservice.models.http.response.HierarchyResponse;
import com.everwell.registryservice.models.http.response.HierarchySummaryResponseData;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-10-25T14:15:56+0530",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 11.0.2 (Oracle Corporation)"
)
public class HierarchyMapperImpl implements HierarchyMapper {

    @Override
    public Hierarchy hierarchyRequestDataToHierarchy(HierarchyRequestData hierarchyRequestData) {
        if ( hierarchyRequestData == null ) {
            return null;
        }

        Hierarchy hierarchy = new Hierarchy();

        hierarchy.setLevel( hierarchyRequestData.getLevel() );
        hierarchy.setName( hierarchyRequestData.getName() );
        hierarchy.setType( hierarchyRequestData.getType() );
        hierarchy.setCode( hierarchyRequestData.getCode() );
        hierarchy.setParentId( hierarchyRequestData.getParentId() );
        hierarchy.setMergeStatus( hierarchyRequestData.getMergeStatus() );

        return hierarchy;
    }

    @Override
    public HierarchyResponseData hierarchyToHierarchyResponseData(Hierarchy hierarchy) {
        if ( hierarchy == null ) {
            return null;
        }

        HierarchyResponseData hierarchyResponseData = new HierarchyResponseData();

        hierarchyResponseData.setId( hierarchy.getId() );
        hierarchyResponseData.setLevel( hierarchy.getLevel() );
        hierarchyResponseData.setName( hierarchy.getName() );
        hierarchyResponseData.setType( hierarchy.getType() );
        hierarchyResponseData.setCode( hierarchy.getCode() );
        hierarchyResponseData.setParentId( hierarchy.getParentId() );
        hierarchyResponseData.setLevel1Id( hierarchy.getLevel1Id() );
        hierarchyResponseData.setLevel1Name( hierarchy.getLevel1Name() );
        hierarchyResponseData.setLevel1Type( hierarchy.getLevel1Type() );
        hierarchyResponseData.setLevel1Code( hierarchy.getLevel1Code() );
        hierarchyResponseData.setLevel2Id( hierarchy.getLevel2Id() );
        hierarchyResponseData.setLevel2Name( hierarchy.getLevel2Name() );
        hierarchyResponseData.setLevel2Type( hierarchy.getLevel2Type() );
        hierarchyResponseData.setLevel2Code( hierarchy.getLevel2Code() );
        hierarchyResponseData.setLevel3Id( hierarchy.getLevel3Id() );
        hierarchyResponseData.setLevel3Name( hierarchy.getLevel3Name() );
        hierarchyResponseData.setLevel3Type( hierarchy.getLevel3Type() );
        hierarchyResponseData.setLevel3Code( hierarchy.getLevel3Code() );
        hierarchyResponseData.setLevel4Id( hierarchy.getLevel4Id() );
        hierarchyResponseData.setLevel4Name( hierarchy.getLevel4Name() );
        hierarchyResponseData.setLevel4Type( hierarchy.getLevel4Type() );
        hierarchyResponseData.setLevel4Code( hierarchy.getLevel4Code() );
        hierarchyResponseData.setLevel5Id( hierarchy.getLevel5Id() );
        hierarchyResponseData.setLevel5Name( hierarchy.getLevel5Name() );
        hierarchyResponseData.setLevel5Type( hierarchy.getLevel5Type() );
        hierarchyResponseData.setLevel5Code( hierarchy.getLevel5Code() );
        hierarchyResponseData.setLevel6Id( hierarchy.getLevel6Id() );
        hierarchyResponseData.setLevel6Name( hierarchy.getLevel6Name() );
        hierarchyResponseData.setLevel6Type( hierarchy.getLevel6Type() );
        hierarchyResponseData.setLevel6Code( hierarchy.getLevel6Code() );
        hierarchyResponseData.setHasChildren( hierarchy.isHasChildren() );
        hierarchyResponseData.setStartDate( hierarchy.getStartDate() );
        hierarchyResponseData.setEndDate( hierarchy.getEndDate() );
        hierarchyResponseData.setActive( hierarchy.isActive() );
        hierarchyResponseData.setCreatedAt( hierarchy.getCreatedAt() );
        hierarchyResponseData.setUpdatedAt( hierarchy.getUpdatedAt() );
        hierarchyResponseData.setMergeStatus( hierarchy.getMergeStatus() );
        hierarchyResponseData.setClientId( hierarchy.getClientId() );
        if ( hierarchy.getDeploymentId() != null ) {
            hierarchyResponseData.setDeploymentId( String.valueOf( hierarchy.getDeploymentId() ) );
        }

        return hierarchyResponseData;
    }

    @Override
    public HierarchySummaryResponseData hierarchyToHierarchySummaryResponseData(Hierarchy hierarchy) {
        if ( hierarchy == null ) {
            return null;
        }

        HierarchySummaryResponseData hierarchySummaryResponseData = new HierarchySummaryResponseData();

        hierarchySummaryResponseData.setId( hierarchy.getId() );
        hierarchySummaryResponseData.setLevel( hierarchy.getLevel() );
        hierarchySummaryResponseData.setName( hierarchy.getName() );
        hierarchySummaryResponseData.setType( hierarchy.getType() );
        hierarchySummaryResponseData.setCode( hierarchy.getCode() );
        hierarchySummaryResponseData.setParentId( hierarchy.getParentId() );

        return hierarchySummaryResponseData;
    }

    @Override
    public HierarchyUpdateData userUpdateHierarchyRequestToHierarchyUpdateData(UserUpdateHierarchyRequest updateHierarchyRequest) {
        if ( updateHierarchyRequest == null ) {
            return null;
        }

        HierarchyUpdateData hierarchyUpdateData = new HierarchyUpdateData();

        hierarchyUpdateData.setHierarchyId( updateHierarchyRequest.getHierarchyId() );
        hierarchyUpdateData.setName( updateHierarchyRequest.getName() );
        hierarchyUpdateData.setActive( updateHierarchyRequest.getActive() );
        hierarchyUpdateData.setParentId( updateHierarchyRequest.getParentId() );
        hierarchyUpdateData.setCode( updateHierarchyRequest.getCode() );

        return hierarchyUpdateData;
    }

    @Override
    public HierarchySummaryResponseData hierarchyResponseDataToHierarchySummaryResponseData(HierarchyResponseData hierarchyResponseData) {
        if ( hierarchyResponseData == null ) {
            return null;
        }

        HierarchySummaryResponseData hierarchySummaryResponseData = new HierarchySummaryResponseData();

        hierarchySummaryResponseData.setId( hierarchyResponseData.getId() );
        hierarchySummaryResponseData.setLevel( hierarchyResponseData.getLevel() );
        hierarchySummaryResponseData.setName( hierarchyResponseData.getName() );
        hierarchySummaryResponseData.setType( hierarchyResponseData.getType() );
        hierarchySummaryResponseData.setCode( hierarchyResponseData.getCode() );
        hierarchySummaryResponseData.setParentId( hierarchyResponseData.getParentId() );

        return hierarchySummaryResponseData;
    }

    @Override
    public List<MinHierarchyDetails> hierarchyResponseListToMinHierarchyDetailsList(List<HierarchyResponse> hierarchyResponseList) {
        if ( hierarchyResponseList == null ) {
            return null;
        }

        List<MinHierarchyDetails> list = new ArrayList<MinHierarchyDetails>( hierarchyResponseList.size() );
        for ( HierarchyResponse hierarchyResponse : hierarchyResponseList ) {
            list.add( hierarchyResponseToMinHierarchyDetails( hierarchyResponse ) );
        }

        return list;
    }

    @Override
    public MinHierarchyDetails hierarchyResponseToMinHierarchyDetails(HierarchyResponse hierarchyResponse) {
        if ( hierarchyResponse == null ) {
            return null;
        }

        MinHierarchyDetails minHierarchyDetails = new MinHierarchyDetails();

        minHierarchyDetails.setId( hierarchyResponseHierarchyId( hierarchyResponse ) );
        minHierarchyDetails.setName( hierarchyResponseHierarchyName( hierarchyResponse ) );
        minHierarchyDetails.setCode( hierarchyResponseHierarchyCode( hierarchyResponse ) );
        minHierarchyDetails.setType( hierarchyResponseHierarchyType( hierarchyResponse ) );
        minHierarchyDetails.setLevel( hierarchyResponseHierarchyLevel( hierarchyResponse ) );
        minHierarchyDetails.setParentId( hierarchyResponseHierarchyParentId( hierarchyResponse ) );

        return minHierarchyDetails;
    }

    private Long hierarchyResponseHierarchyId(HierarchyResponse hierarchyResponse) {
        if ( hierarchyResponse == null ) {
            return null;
        }
        HierarchyResponseData hierarchy = hierarchyResponse.getHierarchy();
        if ( hierarchy == null ) {
            return null;
        }
        Long id = hierarchy.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }

    private String hierarchyResponseHierarchyName(HierarchyResponse hierarchyResponse) {
        if ( hierarchyResponse == null ) {
            return null;
        }
        HierarchyResponseData hierarchy = hierarchyResponse.getHierarchy();
        if ( hierarchy == null ) {
            return null;
        }
        String name = hierarchy.getName();
        if ( name == null ) {
            return null;
        }
        return name;
    }

    private String hierarchyResponseHierarchyCode(HierarchyResponse hierarchyResponse) {
        if ( hierarchyResponse == null ) {
            return null;
        }
        HierarchyResponseData hierarchy = hierarchyResponse.getHierarchy();
        if ( hierarchy == null ) {
            return null;
        }
        String code = hierarchy.getCode();
        if ( code == null ) {
            return null;
        }
        return code;
    }

    private String hierarchyResponseHierarchyType(HierarchyResponse hierarchyResponse) {
        if ( hierarchyResponse == null ) {
            return null;
        }
        HierarchyResponseData hierarchy = hierarchyResponse.getHierarchy();
        if ( hierarchy == null ) {
            return null;
        }
        String type = hierarchy.getType();
        if ( type == null ) {
            return null;
        }
        return type;
    }

    private Long hierarchyResponseHierarchyLevel(HierarchyResponse hierarchyResponse) {
        if ( hierarchyResponse == null ) {
            return null;
        }
        HierarchyResponseData hierarchy = hierarchyResponse.getHierarchy();
        if ( hierarchy == null ) {
            return null;
        }
        Long level = hierarchy.getLevel();
        if ( level == null ) {
            return null;
        }
        return level;
    }

    private Long hierarchyResponseHierarchyParentId(HierarchyResponse hierarchyResponse) {
        if ( hierarchyResponse == null ) {
            return null;
        }
        HierarchyResponseData hierarchy = hierarchyResponse.getHierarchy();
        if ( hierarchy == null ) {
            return null;
        }
        Long parentId = hierarchy.getParentId();
        if ( parentId == null ) {
            return null;
        }
        return parentId;
    }
}

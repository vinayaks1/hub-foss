package com.everwell.registryservice.mappers;

import com.everwell.registryservice.models.db.SidebarItem;
import com.everwell.registryservice.models.dto.SidebarItemResponse;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-10-25T14:15:57+0530",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 11.0.2 (Oracle Corporation)"
)
public class SidebarItemMapperImpl implements SidebarItemMapper {

    @Override
    public List<SidebarItemResponse> sidebarItemListToSidebarItemResponseList(List<SidebarItem> sidebarItem) {
        if ( sidebarItem == null ) {
            return null;
        }

        List<SidebarItemResponse> list = new ArrayList<SidebarItemResponse>( sidebarItem.size() );
        for ( SidebarItem sidebarItem1 : sidebarItem ) {
            list.add( sidebarItemToSidebarItemResponse( sidebarItem1 ) );
        }

        return list;
    }

    @Override
    public SidebarItemResponse sidebarItemToSidebarItemResponse(SidebarItem sidebarItem) {
        if ( sidebarItem == null ) {
            return null;
        }

        SidebarItemResponse sidebarItemResponse = new SidebarItemResponse();

        sidebarItemResponse.setId( sidebarItem.getId() );
        sidebarItemResponse.setName( sidebarItem.getName() );
        sidebarItemResponse.setIcon( sidebarItem.getIcon() );
        sidebarItemResponse.setLink( sidebarItem.getLink() );
        sidebarItemResponse.setRelativePath( sidebarItem.getRelativePath() );

        return sidebarItemResponse;
    }
}

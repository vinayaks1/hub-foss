package com.everwell.registryservice.handler;

import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.handlers.JobHandler;
import com.everwell.registryservice.service.TriggerService;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;

import static org.mockito.Mockito.*;

public class JobHandlerTest extends BaseTest {

    @Spy
    @InjectMocks
    JobHandler jobHandler;

    @Mock
    TriggerService triggerService;

    @Test
    public void testMethodCallToReadTable()
    {
        doNothing().when(triggerService).readTriggerTable();
        jobHandler.EverydayJobScheduler();
        verify(triggerService, times(1)).readTriggerTable();
    }

}

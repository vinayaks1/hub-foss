package com.everwell.registryservice.controller;

import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.constants.FieldValidationMessages;
import com.everwell.registryservice.constants.ValidationStatusEnum;
import com.everwell.registryservice.exceptions.CustomExceptionHandler;
import com.everwell.registryservice.exceptions.NotFoundException;
import com.everwell.registryservice.exceptions.UnauthorizedException;
import com.everwell.registryservice.models.db.Client;
import com.everwell.registryservice.models.db.Deployment;
import com.everwell.registryservice.models.db.UserAccess;
import com.everwell.registryservice.models.dto.*;
import com.everwell.registryservice.models.http.requests.AddHierarchyRequest;
import com.everwell.registryservice.models.http.requests.UpdateHierarchyRequest;
import com.everwell.registryservice.models.http.requests.UserUpdateHierarchyRequest;
import com.everwell.registryservice.models.http.response.HierarchyResponse;
import com.everwell.registryservice.models.http.response.HierarchySummaryResponseData;
import com.everwell.registryservice.models.http.response.Response;
import com.everwell.registryservice.service.*;
import com.everwell.registryservice.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.util.Pair;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class HierarchyControllerTest extends BaseTest {
    private final String hierarchyURI = "/v1/hierarchy";
    private final String hierarchyWithAncestorsURI = "/v1/hierarchy/with-ancestors";
    private final String hierarchyWithDescendantsURI = "/v1/hierarchy/with-descendants";
    private final Long testHierarchyId = 34L;
    private final Long testDeploymentId = 36L;
    private final String testHierarchyName = "testHierarchyName";
    private final String testHierarchyCode = "testHierarchyCode";
    private final String testHierarchyType = "testHierarchyType";
    private final String testUserName = "testUser";
    private final Long testSsoId = 123L;
    private MockMvc mockMvc;
    @InjectMocks
    private HierarchyController hierarchyController;
    @Mock
    private HierarchyService hierarchyService;
    @Mock
    private ConfigService configService;
    @Mock
    private AssociationsService associationsService;
    @Mock
    private DeploymentService deploymentService;
    @Mock
    private UserService userService;

    @Before
    public void setup() {
        mockClient();
        mockMvc = MockMvcBuilders.standaloneSetup(hierarchyController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();

    }

    @Test
    public void addNewHierarchy_invalidRequestMissingHierarchyData_Error() throws Exception {
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(hierarchyURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getInvalidAddHierarchyRequestMissingHierarchyData()))
                )
                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertEquals(FieldValidationMessages.HIERARCHY_DATA_MANDATORY, response.getMessage());
                });
    }

    @Test
    public void addNewHierarchy_invalidRequestMissingHierarchyName_Error() throws Exception {
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(hierarchyURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getInvalidAddHierarchyRequestMissingHierarchyName()))
                )
                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.getMessage().contains(FieldValidationMessages.HIERARCHY_NAME_MANDATORY));
                });
    }

    @Test
    public void addNewHierarchy_invalidRequestMissingHierarchyCode_Error() throws Exception {
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(hierarchyURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getInvalidAddHierarchyRequestMissingHierarchyCode()))
                )
                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.getMessage().contains(FieldValidationMessages.HIERARCHY_CODE_MANDATORY));
                });
    }

    @Test
    public void addNewHierarchy_invalidRequestMissingHierarchyLevel_Error() throws Exception {
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(hierarchyURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getInvalidAddHierarchyRequestMissingHierarchyLevel()))
                )
                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.getMessage().contains(FieldValidationMessages.HIERARCHY_LEVEL_MANDATORY));
                });
    }

    @Test
    public void addNewHierarchy_invalidRequestHierarchyLevelLessThanSupported_Error() throws Exception {
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(hierarchyURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getInvalidAddHierarchyRequestHierarchyLevelLessThan1()))
                )
                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.getMessage().contains(FieldValidationMessages.HIERARCHY_LEVEL_LESSER_THAN_SUPPORTED));
                });
    }

    @Test
    public void addNewHierarchy_invalidRequestHierarchyLevelGreaterThanSupported_Error() throws Exception {
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(hierarchyURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getInvalidAddHierarchyRequestHierarchyLevelMoreThan6()))
                )
                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.getMessage().contains(FieldValidationMessages.HIERARCHY_LEVEL_GREATER_THAN_SUPPORTED));
                });
    }

    @Test
    public void addNewHierarchy_invalidRequestMissingHierarchyType_Error() throws Exception {
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(hierarchyURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getInvalidAddHierarchyRequestMissingHierarchyType()))
                )
                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.getMessage().contains(FieldValidationMessages.HIERARCHY_TYPE_MANDATORY));
                });
    }

    @Test
    public void addNewHierarchy_validRequestNoAssociationAndConfig_Success() throws Exception {
        when(deploymentService.getByCode(testHierarchyCode)).thenReturn(getValidDeployment());
        when(hierarchyService.addNewHierarchy(any(HierarchyRequestData.class), any(Long.class), any(Long.class))).thenReturn(getValidHierarchy());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(hierarchyURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getValidAddHierarchyRequestWithoutAssociationsAndConfigs()))
                )
                .andExpect(status().isCreated())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());
                    HierarchyResponse hierarchyResponse = Utils.convertObject(Utils.asJsonString(response.getData()), HierarchyResponse.class);
                    assertNotNull(hierarchyResponse);
                    assertNotNull(hierarchyResponse.getHierarchy());
                });
    }

    @Test
    public void addNewHierarchy_validRequestNoConfig_Success() throws Exception {
        when(deploymentService.getByCode(testHierarchyCode)).thenReturn(getValidDeployment());
        when(hierarchyService.addNewHierarchy(any(HierarchyRequestData.class), any(Long.class), any(Long.class))).thenReturn(getValidHierarchy());
        when(associationsService.addHierarchyAssociations(any(Long.class), any(Map.class))).thenReturn(new HashMap());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(hierarchyURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getValidAddHierarchyRequestWithoutConfigs()))
                )
                .andExpect(status().isCreated())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());
                    HierarchyResponse hierarchyResponse = Utils.convertObject(Utils.asJsonString(response.getData()), HierarchyResponse.class);
                    assertNotNull(hierarchyResponse);
                    assertNotNull(hierarchyResponse.getHierarchy());
                    assertNotNull(hierarchyResponse.getAssociations());
                });
    }

    @Test
    public void addNewHierarchy_validRequestAllData_Success() throws Exception {
        when(deploymentService.getByCode(testHierarchyCode)).thenReturn(getValidDeployment());
        when(hierarchyService.addNewHierarchy(any(HierarchyRequestData.class), any(Long.class), any(Long.class))).thenReturn(getValidHierarchy());
        when(associationsService.fetchHierarchyAssociations(testHierarchyId)).thenReturn(new HashMap<>());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(hierarchyURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getValidAddHierarchyRequest()))
                )
                .andExpect(status().isCreated())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());
                    HierarchyResponse hierarchyResponse = Utils.convertObject(Utils.asJsonString(response.getData()), HierarchyResponse.class);
                    assertNotNull(hierarchyResponse);
                    assertNotNull(hierarchyResponse.getHierarchy());
                    assertNotNull(hierarchyResponse.getAssociations());
                });
    }

    @Test
    public void getHierarchyDetails_onlyHierarchyData() throws Exception {
        when(hierarchyService.fetchHierarchyById(testHierarchyId, clientId_Nikshay)).thenReturn(getValidHierarchy());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(hierarchyURI + "/" + testHierarchyId + "?includeAssociations=false")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getValidAddHierarchyRequest()))
                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());
                    HierarchyResponse hierarchyResponse = Utils.convertObject(Utils.asJsonString(response.getData()), HierarchyResponse.class);
                    assertNotNull(hierarchyResponse);
                    HierarchyResponseData hierarchy = hierarchyResponse.getHierarchy();
                    assertNotNull(hierarchy);
                });
    }

    @Test
    public void getHierarchyDetails_allData() throws Exception {
        when(hierarchyService.fetchHierarchyById(testHierarchyId, clientId_Nikshay)).thenReturn(getValidHierarchy());
        when(associationsService.fetchHierarchyAssociations(testHierarchyId)).thenReturn(new HashMap<>());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(hierarchyURI + "/" + testHierarchyId + "?includeAssociations=true")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getValidAddHierarchyRequest()))
                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());
                    HierarchyResponse hierarchyResponse = Utils.convertObject(Utils.asJsonString(response.getData()), HierarchyResponse.class);
                    assertNotNull(hierarchyResponse);
                    HierarchyResponseData hierarchy = hierarchyResponse.getHierarchy();
                    assertNotNull(hierarchy);
                    assertNotNull(hierarchyResponse.getAssociations());
                });
    }

    @Test
    public void getHierarchyDetailsWithAncestors_onlyHierarchyData() throws Exception {
        when(hierarchyService.fetchHierarchyByIdWithAncestors(testHierarchyId, clientId_Nikshay)).thenReturn(Collections.singletonList(getValidHierarchyResponse()));
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(hierarchyWithAncestorsURI + "/" + testHierarchyId + "?includeAssociations=false")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getValidAddHierarchyRequest()))
                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());
                });
    }

    @Test
    public void getHierarchyDetailsWithAncestors_allData() throws Exception {
        when(hierarchyService.fetchHierarchyByIdWithAncestors(testHierarchyId, clientId_Nikshay)).thenReturn(Collections.singletonList(getValidHierarchyResponseWithAssociations()));
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(hierarchyWithAncestorsURI + "/" + testHierarchyId + "?includeAssociations=true")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getValidAddHierarchyRequest()))
                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());
                });
    }

    @Test
    public void getHierarchyDetailsWithDescendants_onlyHierarchyData() throws Exception {
        when(hierarchyService.fetchHierarchyByIdWithDescendants(testHierarchyId, true, clientId_Nikshay)).thenReturn(Collections.singletonList(getValidHierarchyResponse()));
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(hierarchyWithDescendantsURI + "/" + testHierarchyId + "?includeAssociations=false&fetchFullTree=true")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getValidAddHierarchyRequest()))
                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());
                });
    }

    @Test
    public void getHierarchyDetailsWithDescendants_allData() throws Exception {
        when(hierarchyService.fetchHierarchyByIdWithDescendants(testHierarchyId, true, clientId_Nikshay)).thenReturn(Collections.singletonList(getValidHierarchyResponseWithAssociations()));
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(hierarchyWithDescendantsURI + "/" + testHierarchyId + "?includeAssociations=true&fetchFullTree=true")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getValidAddHierarchyRequest()))
                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());
                });
    }

    @Test
    public void updateHierarchy_invalidRequestMissingHierarchyData_Error() throws Exception {
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(hierarchyURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getInvalidUpdateHierarchyRequestMissingUpdateHierarchyData()))
                )
                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertEquals(FieldValidationMessages.HIERARCHY_DATA_MANDATORY, response.getMessage());
                });
    }

    @Test
    public void updateHierarchy_invalidRequestMissingHierarchyId_Error() throws Exception {
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(hierarchyURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getInvalidUpdateHierarchyRequestMissingUpdateHierarchyId()))
                )
                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.getMessage().contains(FieldValidationMessages.HIERARCHY_ID_MANDATORY));
                });
    }

    @Test
    public void updateHierarchy_validRequestNoConfigAndAssociations_Success() throws Exception {
        when(hierarchyService.updateHierarchy(any(HierarchyUpdateData.class), any(Long.class))).thenReturn(getValidHierarchy());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(hierarchyURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getValidUpdateHierarchyRequestNoAssociationsAndConfigs()))
                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());
                    HierarchyResponse hierarchyResponse = Utils.convertObject(Utils.asJsonString(response.getData()), HierarchyResponse.class);
                    assertNotNull(hierarchyResponse);
                    HierarchyResponseData hierarchy = hierarchyResponse.getHierarchy();
                    assertNotNull(hierarchy);
                });
    }

    @Test
    public void updateHierarchy_validRequestNoConfigs_Success() throws Exception {
        when(hierarchyService.updateHierarchy(any(HierarchyUpdateData.class), any(Long.class))).thenReturn(getValidHierarchy());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(hierarchyURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getValidUpdateHierarchyRequestNoConfigs()))
                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());
                    HierarchyResponse hierarchyResponse = Utils.convertObject(Utils.asJsonString(response.getData()), HierarchyResponse.class);
                    assertNotNull(hierarchyResponse);
                    assertNotNull(hierarchyResponse.getHierarchy());
                    assertNotNull(hierarchyResponse.getAssociations());
                });
    }

    @Test
    public void updateHierarchy_validRequest_Success() throws Exception {
        when(hierarchyService.updateHierarchy(any(HierarchyUpdateData.class), any(Long.class))).thenReturn(getValidHierarchy());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(hierarchyURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getValidUpdateHierarchyRequest()))
                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());
                    HierarchyResponse hierarchyResponse = Utils.convertObject(Utils.asJsonString(response.getData()), HierarchyResponse.class);
                    assertNotNull(hierarchyResponse);
                    assertNotNull(hierarchyResponse.getHierarchy());
                    assertNotNull(hierarchyResponse.getAssociations());
                });
    }

    @Test
    public void deleteHierarchy_deleteUnsuccessful_Failure() throws Exception {
        when(hierarchyService.deleteHierarchy(testHierarchyId, clientId_Nikshay)).thenReturn(false);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .delete(hierarchyURI + "/" + testHierarchyId)
                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertFalse(response.isSuccess());
                });
    }

    @Test
    public void deleteHierarchy_deleteSuccessful_Success() throws Exception {
        when(hierarchyService.deleteHierarchy(testHierarchyId, clientId_Nikshay)).thenReturn(true);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .delete(hierarchyURI + "/" + testHierarchyId)
                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                });
    }

    @Test
    public void testGetHierarchies_Success() throws Exception {
        when(userService.getUserClientByToken()).thenReturn(getClientUserAccessData());
        when(hierarchyService.fetchHierarchyByIdWithDescendants(eq(testHierarchyId), eq(true), eq(clientId_Nikshay))).thenReturn(getHierarchyResponseList());
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get("/v1/hierarchies/user")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());
                    List<MinHierarchyDetails> hierarchyResponseList = (List<MinHierarchyDetails>) Utils.convertObject(Utils.asJsonString(response.getData()), (new TypeReference<List<MinHierarchyDetails>>() {}));
                    assertNotNull(hierarchyResponseList);
                    assertEquals(3, hierarchyResponseList.size());
                    assertNotNull(hierarchyResponseList.get(0).getId());
                    assertNotNull(hierarchyResponseList.get(0).getName());
                    assertNotNull(hierarchyResponseList.get(0).getCode());
                    assertNotNull(hierarchyResponseList.get(0).getLevel());
                    assertNotNull(hierarchyResponseList.get(0).getType());
                });
    }


    @Test
    public void testGetHierarchies_UserUnauthorized_Failure() throws Exception {
        when(userService.getUserClientByToken()).thenThrow(new UnauthorizedException("User session not found"));
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get("/v1/hierarchies/user")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertFalse(response.isSuccess());
                    assertNotNull(response.getMessage());
                    assertEquals(response.getMessage(), "User session not found");
                });
    }

    @Test
    public void testGetHierarchies_Hierarchy_NotFound() throws Exception {
        when(userService.getUserClientByToken()).thenReturn(getClientUserAccessData());
        when(hierarchyService.fetchHierarchyByIdWithDescendants(eq(testHierarchyId), eq(true), eq(clientId_Nikshay))).thenThrow(new NotFoundException(ValidationStatusEnum.HIERARCHY_NOT_FOUND));
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get("/v1/hierarchies/user")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertFalse(response.isSuccess());
                    assertNotNull(response.getMessage());
                    assertEquals(response.getMessage(), ValidationStatusEnum.HIERARCHY_NOT_FOUND.getMessage());
                });
    }

    @Test
    public void testGetUserHierarchySummary_UserUnauthorized_Failure() throws Exception {
        when(userService.getUserClientByToken()).thenThrow(new UnauthorizedException("User session not found"));
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get("/v1/hierarchy/summary/user")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertFalse(response.isSuccess());
                    assertNotNull(response.getMessage());
                    assertEquals(response.getMessage(), "User session not found");
                });
    }

    @Test
    public void testGetUserHierarchySummary_UnauthorizedHierarchySuccess() throws Exception {
        when(userService.getUserClientByToken()).thenReturn(getClientUserAccessData());
        when(hierarchyService.validateIsDescendant(anyLong(), eq(clientId_Nikshay), anyLong())).thenReturn(false);
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get("/v1/hierarchy/summary/user?hierarchyId=" + testHierarchyId)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertFalse(response.isSuccess());
                    assertNotNull(response.getMessage());
                    assertEquals(response.getMessage(), "The user does not have access to the requested hierarchy");
                });
    }

    @Test
    public void testGetUserHierarchySummary_HierarchyNotFound() throws Exception {
        when(userService.getUserClientByToken()).thenReturn(getClientUserAccessData());
        when(hierarchyService.validateIsDescendant(anyLong(), eq(clientId_Nikshay), anyLong())).thenReturn(true);
        when(hierarchyService.fetchHierarchySummaryById(eq(testHierarchyId), eq(clientId_Nikshay))).thenThrow(new NotFoundException(ValidationStatusEnum.HIERARCHY_NOT_FOUND));
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get("/v1/hierarchy/summary/user?hierarchyId=" + testHierarchyId)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertFalse(response.isSuccess());
                    assertNotNull(response.getMessage());
                    assertEquals(response.getMessage(), "The Hierarchy does not exist");
                });
    }

    @Test
    public void testGetUserHierarchySummary_EmptyLogins_Success() throws Exception {
        when(userService.getUserClientByToken()).thenReturn(getClientUserAccessData());
        when(hierarchyService.validateIsDescendant(anyLong(), eq(clientId_Nikshay), anyLong())).thenReturn(true);
        when(hierarchyService.fetchHierarchySummaryById(eq(testHierarchyId), eq(clientId_Nikshay))).thenReturn(getValidHierarchySummaryResponseData());
        when(userService.getUserSummaryResponseByHierarchyId(eq(testHierarchyId))).thenReturn(new ArrayList<>());
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get("/v1/hierarchy/summary/user?hierarchyId=" + testHierarchyId)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());
                    UserHierarchySummaryResponse summaryResponse = Utils.convertObject(Utils.asJsonString(response.getData()), UserHierarchySummaryResponse.class);
                    assertEquals(0, summaryResponse.getLoginDetails().size());
                    assertNotNull(summaryResponse.getBasicDetails());
                    assertEquals(testHierarchyId, summaryResponse.getBasicDetails().getId());
                });
    }

    @Test
    public void testGetUserHierarchySummary_FilledLogins_Success() throws Exception {
        when(userService.getUserClientByToken()).thenReturn(getClientUserAccessData());
        when(hierarchyService.validateIsDescendant(anyLong(), eq(clientId_Nikshay), anyLong())).thenReturn(true);
        when(hierarchyService.fetchHierarchySummaryById(eq(testHierarchyId), eq(clientId_Nikshay))).thenReturn(getValidHierarchySummaryResponseData());
        when(userService.getUserSummaryResponseByHierarchyId(eq(testHierarchyId))).thenReturn(Arrays.asList(new UserSummaryDto(), new UserSummaryDto()));
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get("/v1/hierarchy/summary/user?hierarchyId=" + testHierarchyId)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());
                    UserHierarchySummaryResponse summaryResponse = Utils.convertObject(Utils.asJsonString(response.getData()), UserHierarchySummaryResponse.class);
                    assertEquals(2, summaryResponse.getLoginDetails().size());
                    assertNotNull(summaryResponse.getBasicDetails());
                    assertEquals(testHierarchyId, summaryResponse.getBasicDetails().getId());
                });
    }

    @Test
    public void testUserUpdateHierarchy_Success() throws Exception {
        when(userService.getUserClientByToken()).thenReturn(getClientUserAccessData());
        when(hierarchyService.validateIsDescendant(anyLong(), eq(clientId_Nikshay), anyLong())).thenReturn(true);
        when(hierarchyService.userUpdateHierarchy(any(UserUpdateHierarchyRequest.class), eq(clientId_Nikshay))).thenReturn(getValidHierarchySummaryResponseData());
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .put("/v1/hierarchy/user")
                                .contentType(MediaType.APPLICATION_JSON)
                        .content(Utils.asJsonString(getValidUserUpdateHierarchyRequest()))
                )
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());
                    HierarchySummaryResponseData summaryResponse = Utils.convertObject(Utils.asJsonString(response.getData()), HierarchySummaryResponseData.class);
                    assertEquals(testHierarchyId, summaryResponse.getId());
                    assertNotNull(summaryResponse.getLevel());
                    assertNotNull(summaryResponse.getCode());
                    assertNotNull(summaryResponse.getType());
                });
    }

    @Test
    public void testuserUpdateHierarchy_UnauthorizedHierarchySuccess() throws Exception {
        when(userService.getUserClientByToken()).thenReturn(getClientUserAccessData());
        when(hierarchyService.validateIsDescendant(anyLong(), eq(clientId_Nikshay), anyLong())).thenReturn(false);
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .put("/v1/hierarchy/user")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getValidUserUpdateHierarchyRequest()))
                )
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertFalse(response.isSuccess());
                    assertNotNull(response.getMessage());
                    assertEquals(response.getMessage(), "The user does not have access to the requested hierarchy");
                });
    }


    private AddHierarchyRequest getInvalidAddHierarchyRequestMissingHierarchyData() {
        return new AddHierarchyRequest();
    }

    private AddHierarchyRequest getInvalidAddHierarchyRequestMissingHierarchyName() {
        AddHierarchyRequest addHierarchyRequest = getInvalidAddHierarchyRequestMissingHierarchyData();
        HierarchyRequestData hierarchyRequestData = new HierarchyRequestData();
        addHierarchyRequest.setHierarchy(hierarchyRequestData);
        return addHierarchyRequest;
    }

    private AddHierarchyRequest getInvalidAddHierarchyRequestMissingHierarchyCode() {
        AddHierarchyRequest addHierarchyRequest = getInvalidAddHierarchyRequestMissingHierarchyName();
        HierarchyRequestData hierarchyRequestData = addHierarchyRequest.getHierarchy();
        hierarchyRequestData.setName(testHierarchyName);
        return addHierarchyRequest;
    }

    private AddHierarchyRequest getInvalidAddHierarchyRequestMissingHierarchyLevel() {
        AddHierarchyRequest addHierarchyRequest = getInvalidAddHierarchyRequestMissingHierarchyCode();
        HierarchyRequestData hierarchyRequestData = addHierarchyRequest.getHierarchy();
        hierarchyRequestData.setCode(testHierarchyCode);
        return addHierarchyRequest;
    }

    private AddHierarchyRequest getInvalidAddHierarchyRequestHierarchyLevelLessThan1() {
        AddHierarchyRequest addHierarchyRequest = getInvalidAddHierarchyRequestMissingHierarchyLevel();
        HierarchyRequestData hierarchyRequestData = addHierarchyRequest.getHierarchy();
        hierarchyRequestData.setLevel(0L);
        return addHierarchyRequest;
    }

    private AddHierarchyRequest getInvalidAddHierarchyRequestHierarchyLevelMoreThan6() {
        AddHierarchyRequest addHierarchyRequest = getInvalidAddHierarchyRequestMissingHierarchyLevel();
        HierarchyRequestData hierarchyRequestData = addHierarchyRequest.getHierarchy();
        hierarchyRequestData.setLevel(7L);
        return addHierarchyRequest;
    }

    private AddHierarchyRequest getInvalidAddHierarchyRequestMissingHierarchyType() {
        AddHierarchyRequest addHierarchyRequest = getInvalidAddHierarchyRequestMissingHierarchyLevel();
        HierarchyRequestData hierarchyRequestData = addHierarchyRequest.getHierarchy();
        hierarchyRequestData.setLevel(1L);
        return addHierarchyRequest;
    }

    private AddHierarchyRequest getValidAddHierarchyRequestWithoutAssociationsAndConfigs() {
        AddHierarchyRequest addHierarchyRequest = getInvalidAddHierarchyRequestMissingHierarchyType();
        HierarchyRequestData hierarchyRequestData = addHierarchyRequest.getHierarchy();
        hierarchyRequestData.setType(testHierarchyType);
        hierarchyRequestData.setParentId(1L);
        return addHierarchyRequest;
    }

    private AddHierarchyRequest getValidAddHierarchyRequestWithoutConfigs() {
        AddHierarchyRequest addHierarchyRequest = getValidAddHierarchyRequestWithoutAssociationsAndConfigs();
        HashMap<String, Object> inputAssociations = new HashMap<>();
        inputAssociations.put(testHierarchyCode, testHierarchyName);
        addHierarchyRequest.setAssociations(inputAssociations);
        return addHierarchyRequest;
    }

    private AddHierarchyRequest getValidAddHierarchyRequest() {
        AddHierarchyRequest addHierarchyRequest = getValidAddHierarchyRequestWithoutConfigs();
        HashMap<String, Object> inputConfigs = new HashMap<>();
        inputConfigs.put(testHierarchyCode, testHierarchyName);
        addHierarchyRequest.setConfigs(inputConfigs);
        return addHierarchyRequest;
    }

    private HierarchyResponse getValidHierarchyResponse() {
        HierarchyResponse hierarchyResponse = new HierarchyResponse();
        HierarchyResponseData hierarchyResponseData = new HierarchyResponseData();
        hierarchyResponse.setHierarchy(hierarchyResponseData);
        return hierarchyResponse;
    }

    private HierarchyResponse getValidHierarchyResponseWithAssociations() {
        HierarchyResponse hierarchyResponse = getValidHierarchyResponse();
        hierarchyResponse.setAssociations(new HashMap<>());
        return hierarchyResponse;
    }

    private HierarchyResponseData getValidHierarchy() {
        HierarchyResponseData hierarchy = new HierarchyResponseData();
        hierarchy.setId(testHierarchyId);
        return hierarchy;
    }

    private UpdateHierarchyRequest getInvalidUpdateHierarchyRequestMissingUpdateHierarchyData() {
        return new UpdateHierarchyRequest();
    }

    private UpdateHierarchyRequest getInvalidUpdateHierarchyRequestMissingUpdateHierarchyId() {
        UpdateHierarchyRequest updateHierarchyRequest = getInvalidUpdateHierarchyRequestMissingUpdateHierarchyData();
        HierarchyUpdateData hierarchyUpdateData = new HierarchyUpdateData();
        updateHierarchyRequest.setHierarchy(hierarchyUpdateData);
        return updateHierarchyRequest;
    }

    private UpdateHierarchyRequest getValidUpdateHierarchyRequestNoAssociationsAndConfigs() {
        UpdateHierarchyRequest updateHierarchyRequest = getInvalidUpdateHierarchyRequestMissingUpdateHierarchyId();
        HierarchyUpdateData hierarchyUpdateData = updateHierarchyRequest.getHierarchy();
        hierarchyUpdateData.setHierarchyId(testHierarchyId);
        return updateHierarchyRequest;
    }

    private UpdateHierarchyRequest getValidUpdateHierarchyRequestNoConfigs() {
        UpdateHierarchyRequest updateHierarchyRequest = getValidUpdateHierarchyRequestNoAssociationsAndConfigs();
        HashMap<String, Object> inputAssociations = new HashMap<>();
        inputAssociations.put(testHierarchyCode, testHierarchyName);
        updateHierarchyRequest.setAssociations(inputAssociations);
        return updateHierarchyRequest;
    }

    private UpdateHierarchyRequest getValidUpdateHierarchyRequest() {
        UpdateHierarchyRequest updateHierarchyRequest = getValidUpdateHierarchyRequestNoConfigs();
        HashMap<String, Object> inputConfigs = new HashMap<>();
        inputConfigs.put(testHierarchyCode, testHierarchyName);
        updateHierarchyRequest.setConfigs(inputConfigs);
        return updateHierarchyRequest;
    }

    private Deployment getValidDeployment() {
        Deployment deployment = new Deployment();
        deployment.setId(testDeploymentId);
        return deployment;
    }

    private HierarchySummaryResponseData getValidHierarchySummaryResponseData() {
        HierarchySummaryResponseData summaryResponseData = new HierarchySummaryResponseData();
        summaryResponseData.setId(testHierarchyId);
        summaryResponseData.setName("hierarchy 1");
        summaryResponseData.setLevel(1L);
        summaryResponseData.setCode("H1");
        summaryResponseData.setType("COUNTRY");
        summaryResponseData.setParentId(null);
        return summaryResponseData;
    }

    private Pair<Client, UserAccess> getClientUserAccessData() {
        UserAccess definedUser = new UserAccess(1L, testUserName, testHierarchyId, true, clientId_Nikshay, testSsoId, true);
        definedUser.setClientId(clientId_Nikshay);
        return Pair.of( getClient(), definedUser);
    }

    private UserUpdateHierarchyRequest getValidUserUpdateHierarchyRequest() {
        return new UserUpdateHierarchyRequest(testHierarchyId, testHierarchyName, true, null, "testDeploymentCode", false, false, false, false, false);
    }

    private List<HierarchyResponse> getHierarchyResponseList() {
        // Add 1st hierarchy data
        HierarchyResponseData hierarchy1Response = new HierarchyResponseData();
        hierarchy1Response.setId(testHierarchyId);
        hierarchy1Response.setName("hierarchy 1");
        hierarchy1Response.setLevel(1L);
        hierarchy1Response.setCode("H1");
        hierarchy1Response.setType("COUNTRY");
        hierarchy1Response.setParentId(null);

        // Add 2nd hierarchy data
        HierarchyResponseData hierarchy2Response = new HierarchyResponseData();
        hierarchy2Response.setId(123L);
        hierarchy2Response.setName("hierarchy 2");
        hierarchy2Response.setLevel(2L);
        hierarchy2Response.setCode("H2");
        hierarchy2Response.setType("STATE");
        hierarchy2Response.setParentId(testHierarchyId);

        // Add 3rd hierarchy data
        HierarchyResponseData hierarchy3Response = new HierarchyResponseData();
        hierarchy1Response.setId(1234l);
        hierarchy1Response.setName("hierarchy 3");
        hierarchy1Response.setLevel(2L);
        hierarchy1Response.setCode("H2");
        hierarchy1Response.setType("STATE");
        hierarchy1Response.setParentId(testHierarchyId);
        return Arrays.asList(
                new HierarchyResponse(hierarchy1Response, new HashMap<>()),
                new HierarchyResponse(hierarchy2Response, new HashMap<>()),
                new HierarchyResponse(hierarchy3Response, new HashMap<>())
        );
    }
}

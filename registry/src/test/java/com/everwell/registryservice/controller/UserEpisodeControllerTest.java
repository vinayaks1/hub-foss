package com.everwell.registryservice.controller;

import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.exceptions.CustomExceptionHandler;
import com.everwell.registryservice.models.http.requests.SearchRequest;
import com.everwell.registryservice.models.http.requests.episode.EpisodeHeaderSearchRequest;
import com.everwell.registryservice.models.http.response.Response;
import com.everwell.registryservice.models.http.response.episode.HeaderSearchResponse;
import com.everwell.registryservice.service.EpisodeService;
import com.everwell.registryservice.service.UserService;
import com.everwell.registryservice.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserEpisodeControllerTest extends BaseTest {

    private MockMvc mockMvc;

    @InjectMocks
    private UserEpisodeController userEpisodeController;

    @Mock
    private UserService userService;

    @Mock
    private EpisodeService episodeService;

    @Before
    public void setup() {
        mockClient();
        mockMvc = MockMvcBuilders.standaloneSetup(userEpisodeController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void searchEpisode_InvalidRequest() throws Exception {
        SearchRequest request = new SearchRequest();
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post("/v1/user/episodes")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(request))
                )
                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertFalse(response.isSuccess());
                    assertNotNull(response.getMessage());
                });
    }

    @Test
    public void searchEpisode_Success() throws Exception {
        SearchRequest request = new SearchRequest("searchKey", "searchValue");
        when(episodeService.headerSearch(any(EpisodeHeaderSearchRequest.class))).thenReturn(new HeaderSearchResponse(1L, Arrays.asList(Map.of("id", "value"))));
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post("/v1/user/episodes")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(request))
                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());
                    HeaderSearchResponse searchResponse = Utils.convertObject(Utils.asJsonString(response.getData()), HeaderSearchResponse.class);
                    assertEquals(1L, searchResponse.getTotalHits().longValue());
                    assertNotNull(searchResponse.getEpisodeIndexList());
                    assertEquals(1L, searchResponse.getEpisodeIndexList().size());
                });
    }

}

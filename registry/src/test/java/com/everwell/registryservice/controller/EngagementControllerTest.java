package com.everwell.registryservice.controller;

import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.exceptions.CustomExceptionHandler;
import com.everwell.registryservice.models.dto.EngagementDetails;
import com.everwell.registryservice.models.dto.LanguageMapping;
import com.everwell.registryservice.models.http.requests.AddEngagementRequest;
import com.everwell.registryservice.models.http.requests.UpdateLanguageRequest;
import com.everwell.registryservice.models.http.response.LanguagesResponse;
import com.everwell.registryservice.service.EngagementService;
import com.everwell.registryservice.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class EngagementControllerTest extends BaseTest {

    private MockMvc mockMvc;

    @Mock
    EngagementService engagementService;

    @InjectMocks
    EngagementController engagementController;

    private static Long id = 1L;
    private static String name = "Dummy Client";
    private static String password = "Dummy Password";



    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(engagementController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void getEngagement_success() throws Exception {
        String uri = "/v1/engagement?hierarchyId=1";

        EngagementDetails engagementDetails = new EngagementDetails
                (1L, 1L, "", "", "", true,
                        "", "");

        when(engagementService.getEngagementByHierarchy(any(), anyBoolean())).thenReturn(engagementDetails);
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .header("x-client-id", id)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"));
    }

    @Test
    public void getEngagement_failure() throws Exception {
        String uri = "/v1/engagement?hierarchyId=1";

        when(engagementService.getEngagementByHierarchy(any(), anyBoolean())).thenReturn(null);
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .header("x-client-id", id)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("The Hierarchy Engagement Mapping does not exist"));
    }
    @Test
    public void addEngagement_success() throws Exception {
        EngagementDetails engagementDetails = new EngagementDetails
                (1L, 1L, "09:00:00", "1,2", "09:00:00", true,
                        "sms", "1");
        AddEngagementRequest addEngagementRequest = new AddEngagementRequest(Collections.singletonList(engagementDetails));

        String uri = "/v1/engagement";
        when(engagementService.addEngagement(any())).thenReturn(Collections.emptyList());

        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(addEngagementRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(""));
    }
    @Test
    public void addEngagement_partialFailure() throws Exception {
        EngagementDetails engagementDetails = new EngagementDetails
                (1L, 1L, "09:00:00", "1,2", "09:00:00", true,
                        "sms", "1");
        AddEngagementRequest addEngagementRequest = new AddEngagementRequest(Collections.singletonList(engagementDetails));

        String uri = "/v1/engagement";
        when(engagementService.addEngagement(any())).thenReturn(Collections.singletonList(engagementDetails));

        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(addEngagementRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"));
    }
    @Test
    public void addEngagement_defaultTimeEmpty() throws Exception {
        EngagementDetails engagementDetails = new EngagementDetails
                (1L, 1L, "", "1,2", "09:00:00", true,
                        "sms", "1");
        AddEngagementRequest addEngagementRequest = new AddEngagementRequest(Collections.singletonList(engagementDetails));

        String uri = "/v1/engagement";
        when(engagementService.addEngagement(any())).thenReturn(Collections.emptyList());

        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(addEngagementRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().is4xxClientError())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Default Time can not be empty"));
    }
    @Test
    public void addEngagement_dosingTimeNotEmpty() throws Exception {
        EngagementDetails engagementDetails = new EngagementDetails
                (1L, 1L, "09:00:00", "", "", true,
                        "sms", "1");
        AddEngagementRequest addEngagementRequest = new AddEngagementRequest(Collections.singletonList(engagementDetails));

        String uri = "/v1/engagement";
        when(engagementService.addEngagement(any())).thenReturn(Collections.emptyList());

        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(addEngagementRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().is4xxClientError())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Dose Timings can not be empty"));
    }
    @Test
    public void addEngagement_consentMandatoryNull() throws Exception {
        EngagementDetails engagementDetails = new EngagementDetails
                (1L, 1L, "09:00:00", "1,2", "09:00:00", null,
                        "sms", "1");
        AddEngagementRequest addEngagementRequest = new AddEngagementRequest(Collections.singletonList(engagementDetails));

        String uri = "/v1/engagement";
        when(engagementService.addEngagement(any())).thenReturn(Collections.emptyList());

        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(addEngagementRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().is4xxClientError())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Consent Mandatory can not be empty"));
    }
    @Test
    public void addEngagement_notificationTypeEmpty() throws Exception {
        EngagementDetails engagementDetails = new EngagementDetails
                (1L, 1L, "09:00:00", "1,2", "09:00:00", true,
                        "", "1");
        AddEngagementRequest addEngagementRequest = new AddEngagementRequest(Collections.singletonList(engagementDetails));

        String uri = "/v1/engagement";
        when(engagementService.addEngagement(any())).thenReturn(Collections.emptyList());

        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(addEngagementRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().is4xxClientError())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Notification Type can not be empty"));
    }
    @Test
    public void addEngagement_hierarchyId() throws Exception {
        EngagementDetails engagementDetails = new EngagementDetails
                (null, 1L, "09:00:00", "1,2", "09:00:00", true,
                        "sms", "1");
        AddEngagementRequest addEngagementRequest = new AddEngagementRequest(Collections.singletonList(engagementDetails));

        String uri = "/v1/engagement";
        when(engagementService.addEngagement(any())).thenReturn(Collections.emptyList());

        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(addEngagementRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().is4xxClientError())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Hierarchy Id can not be empty"));
    }
    @Test
    public void addEngagement_defaultLangNull() throws Exception {
        EngagementDetails engagementDetails = new EngagementDetails
                (1L, null, "09:00:00", "1,2", "09:00:00", true,
                        "sms", "1");
        AddEngagementRequest addEngagementRequest = new AddEngagementRequest(Collections.singletonList(engagementDetails));

        String uri = "/v1/engagement";
        when(engagementService.addEngagement(any())).thenReturn(Collections.emptyList());

        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(addEngagementRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().is4xxClientError())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Default Language can be not empty"));
    }

    @Test
    public void updateLanguagesSuccess() throws Exception{
        UpdateLanguageRequest updateLanguageRequest = getUpdateLanguageRequest(1L);

        String uri = "/v1/engagement/languages";

        when(engagementService.updateLanguages(any())).thenReturn(true);

        mockMvc.perform(
                MockMvcRequestBuilders
                        .put(uri)
                        .content(Utils.asJsonString(updateLanguageRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value(true));
        verify(engagementService, Mockito.times(1)).updateLanguages(any());
    }

    @Test
    public void updateLanguagesFailure() throws Exception{
        UpdateLanguageRequest updateLanguageRequest = getUpdateLanguageRequest(1L);

        String uri = "/v1/engagement/languages";

        when(engagementService.updateLanguages(updateLanguageRequest)).thenReturn(false);

        mockMvc.perform(
                        MockMvcRequestBuilders
                                .put(uri)
                                .content(Utils.asJsonString(updateLanguageRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value(false))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Something went wrong"));
        verify(engagementService, Mockito.times(1)).updateLanguages(any());
    }

    @Test
    public void updateLanguagesHierarchyIdNull() throws Exception {
        UpdateLanguageRequest updateLanguageRequest = getUpdateLanguageRequest(null);

        String uri = "/v1/engagement/languages";

        when(engagementService.updateLanguages(any())).thenReturn(true);

        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .put(uri)
                                .content(Utils.asJsonString(updateLanguageRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().is4xxClientError())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Hierarchy Id must be provided"));
    }

    @Test
    public void updateLanguagesLanguageIdsNull() throws Exception {
        UpdateLanguageRequest updateLanguageRequest = getUpdateLanguageRequestLanguageIdNull();

        String uri = "/v1/engagement/languages";

        when(engagementService.updateLanguages(any())).thenReturn(true);

        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .put(uri)
                                .content(Utils.asJsonString(updateLanguageRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().is4xxClientError())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Language Id's must be provided"));
    }

    @Test
    public void getLanguagesSuccess() throws Exception {
        LanguagesResponse languagesResponse = getLanguagesResponse();
        String uri = "/v1/engagement/languages?hierarchyId=1";

        when(engagementService.getLanguages(any())).thenReturn(languagesResponse);


        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.selectedLanguages[0].languageId").value(languagesResponse.getSelectedLanguages().get(0).getLanguageId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.selectedLanguages[0].language").value(languagesResponse.getSelectedLanguages().get(0).getLanguage()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.availableLanguages[0].languageId").value(languagesResponse.getAvailableLanguages().get(0).getLanguageId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.availableLanguages[0].language").value(languagesResponse.getAvailableLanguages().get(0).getLanguage()));
    }

    @Test
    public void getLanguagesFailure() throws Exception {
        String uri = "/v1/engagement/languages?hierarchyId=1";

        when(engagementService.getLanguages(any())).thenReturn(null);
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Something went wrong"));
    }

    private UpdateLanguageRequest getUpdateLanguageRequest(Long hierarchyId){
        List<Long> languageIds = Arrays.asList(1L,2L,3L);
        List<Long> disabledLanguageIds = Arrays.asList(4L,5L);
        UpdateLanguageRequest updateLanguageRequest = new UpdateLanguageRequest(hierarchyId, languageIds, disabledLanguageIds);
        return updateLanguageRequest;
    }

    private UpdateLanguageRequest getUpdateLanguageRequestLanguageIdNull(){
        List<Long> disabledLanguageIds = Arrays.asList(4L,5L);
        UpdateLanguageRequest updateLanguageRequest = new UpdateLanguageRequest(1L, null, disabledLanguageIds);
        return updateLanguageRequest;
    }

    private LanguagesResponse getLanguagesResponse(){
        LanguageMapping languageMapping = new LanguageMapping(1L, "telugu");
        List<LanguageMapping> availableLanguages = Arrays.asList(languageMapping);
        List<LanguageMapping> selectedLanguages = Arrays.asList(languageMapping);
        LanguagesResponse languagesResponse = new LanguagesResponse(availableLanguages, selectedLanguages);
        return languagesResponse;
    }
}

package com.everwell.registryservice.controller;

import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.constants.FieldValidationMessages;
import com.everwell.registryservice.constants.ValidationStatusEnum;
import com.everwell.registryservice.exceptions.CustomExceptionHandler;
import com.everwell.registryservice.exceptions.NotFoundException;
import com.everwell.registryservice.exceptions.UnauthorizedException;
import com.everwell.registryservice.models.db.*;
import com.everwell.registryservice.models.dto.HierarchyResponseData;
import com.everwell.registryservice.models.dto.MinHierarchyDetails;
import com.everwell.registryservice.models.dto.SidebarItemResponse;
import com.everwell.registryservice.models.dto.UnifiedListFilterResponse;
import com.everwell.registryservice.models.http.requests.CreateUserRequest;
import com.everwell.registryservice.models.http.requests.RedirectionRequest;
import com.everwell.registryservice.models.http.requests.sso.RegistrationRequest;
import com.everwell.registryservice.models.http.response.*;
import com.everwell.registryservice.models.http.response.adherence.AdherenceTechnologyDto;
import com.everwell.registryservice.models.http.response.sso.UserSSOResponse;
import com.everwell.registryservice.service.*;
import com.everwell.registryservice.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.util.Pair;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserControllerTest extends BaseTest {

    private final String usersURI = "/v1/users";
    private final String userURI = "/v1/user";
    private final String testUserName = "testUser";
    private final String testPassword = "testPassword";
    private final Long testSsoId = 123L;
    private final Long testHierarchyMappingId = 123456L;
    private final String testDeploymentId = "1234567";

    private MockMvc mockMvc;
    @InjectMocks
    private UserController userController;
    @Mock
    private UserService userService;
    @Mock
    private HierarchyService hierarchyService;
    @Mock
    private AssociationsService associationsService;
    @Mock
    private SSOService ssoService;
    @Mock
    private DeploymentService deploymentService;
    @Mock
    private ClientService clientService;
    @Mock
    private LanguageService languageService;
    @Mock
    private AdherenceService adherenceService;
    @Mock
    private TaskListService taskListService;

    @Before
    public void setup() {
        mockClient();
        mockMvc = MockMvcBuilders.standaloneSetup(userController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void addNewUser_InvalidRequestMissingUsername_Error() throws Exception {
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(usersURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getInValidCreateUserRequestMissingUserName()))
                )
                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertEquals(FieldValidationMessages.USER_NAME_MANDATORY, response.getMessage());
                });
    }

    @Test
    public void addNewUser_InvalidRequestMissingHierarchyMapping_Error() throws Exception {
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(usersURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getInValidCreateUserRequestMissingHierarchyMapping()))
                )
                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertEquals(FieldValidationMessages.HIERARCHY_ID_MANDATORY, response.getMessage());
                });
    }

    @Test
    public void addNewUser_InvalidRequestMissingPassword_Error() throws Exception {
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(usersURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getInValidCreateUserRequestMissingPassword()))
                )
                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertEquals(FieldValidationMessages.PASSWORD_MANDATORY, response.getMessage());
                });
    }

    @Test
    public void addNewUser_InvalidRequestMissingSsoId_Error() throws Exception {
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(usersURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getInValidCreateUserRequestMissingSsoId()))
                )
                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertEquals(FieldValidationMessages.SSO_ID_MANDATORY, response.getMessage());
                });
    }

    @Test
    public void addNewUser_ValidRequest_SsoCreation_Success() throws Exception {
        when(hierarchyService.fetchHierarchyById(testHierarchyMappingId, clientId_Nikshay)).thenReturn(new HierarchyResponseData());
        when(userService.addNewUser(any(CreateUserRequest.class), anyLong())).thenReturn(getUserAccessData());
        when(associationsService.fetchHierarchyAssociations(testHierarchyMappingId)).thenReturn(new HashMap<>());
        when(ssoService.registration(eq(clientId_Nikshay.toString()), any(RegistrationRequest.class))).thenReturn(ssoResponse());
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(usersURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getValidCreateSsoAndUserRequest()))
                )
                .andExpect(status().isCreated())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());
                    UserDetailsResponse userDetailsResponse = Utils.convertObject(Utils.asJsonString(response.getData()), UserDetailsResponse.class);
                    assertNotNull(userDetailsResponse.getUser());
                    assertEquals(testUserName, userDetailsResponse.getUser().getUsername());
                    assertNotNull(userDetailsResponse.getHierarchy());
                    assertEquals(1, userDetailsResponse.getHierarchy().size());
                });
    }

    @Test
    public void addNewUser_ValidRequest_Success() throws Exception {
        when(hierarchyService.fetchHierarchyById(testHierarchyMappingId, clientId_Nikshay)).thenReturn(new HierarchyResponseData());
        when(userService.addNewUser(any(CreateUserRequest.class), anyLong())).thenReturn(getUserAccessData());
        when(associationsService.fetchHierarchyAssociations(testHierarchyMappingId)).thenReturn(new HashMap<>());
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(usersURI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getValidCreateUserRequest()))
                )
                .andExpect(status().isCreated())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());
                    UserDetailsResponse userDetailsResponse = Utils.convertObject(Utils.asJsonString(response.getData()), UserDetailsResponse.class);
                    assertNotNull(userDetailsResponse.getUser());
                    assertEquals(testUserName, userDetailsResponse.getUser().getUsername());
                    assertNotNull(userDetailsResponse.getHierarchy());
                    assertEquals(1, userDetailsResponse.getHierarchy().size());
                });
    }

    @Test
    public void getUserDetails_OnlyUserData_Success() throws Exception {
        when(userService.fetchUserData(testSsoId)).thenReturn(getUserAccessData());
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(usersURI + "/" + testSsoId)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getValidCreateUserRequest()))
                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());
                    UserDetailsResponse userDetailsResponse = Utils.convertObject(Utils.asJsonString(response.getData()), UserDetailsResponse.class);
                    assertNotNull(userDetailsResponse.getUser());
                    assertNull(userDetailsResponse.getHierarchy());
                    assertEquals(testUserName, userDetailsResponse.getUser().getUsername());
                });
    }

    @Test
    public void getUserDetails_AllDataNoHierarchyMapped_Success() throws Exception {
        when(userService.fetchUserData(testSsoId)).thenReturn(getUserAccessData());
        when(userService.fetchHierarchiesAssociatedWithUser(1L)).thenReturn(Collections.singletonList(testHierarchyMappingId));
        when(hierarchyService.fetchAllHierarchies(any(List.class), any(Long.class))).thenReturn(Collections.emptyList());
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(usersURI + "/" + testSsoId + "?includeAllDetails=true")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getValidCreateUserRequest()))
                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());
                    UserDetailsResponse userDetailsResponse = Utils.convertObject(Utils.asJsonString(response.getData()), UserDetailsResponse.class);
                    assertNotNull(userDetailsResponse.getUser());
                    assertNull(userDetailsResponse.getHierarchy());
                    assertEquals(testUserName, userDetailsResponse.getUser().getUsername());
                });
    }

    @Test
    public void getUserDetails_NoUserMapped_Success() throws Exception {
        when(userService.fetchUserData(testSsoId)).thenReturn(null);
        when(userService.fetchHierarchiesAssociatedWithUser(1L)).thenReturn(null);
        when(hierarchyService.fetchAllHierarchies(any(List.class), any(Long.class))).thenReturn(Collections.emptyList());
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(usersURI + "/" + testSsoId + "?includeAllDetails=true")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getValidCreateUserRequest()))
                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());
                    UserDetailsResponse userDetailsResponse = Utils.convertObject(Utils.asJsonString(response.getData()), UserDetailsResponse.class);
                    assertNull(userDetailsResponse.getUser());
                    assertNull(userDetailsResponse.getHierarchy());
                });
    }

    @Test
    public void validateAuthStatus_LoggedInData_Success() throws Exception {
        when(userService.getUserClientByToken()).thenReturn(getClientUserAccessData());
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(userURI + "/" + "auth-status")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());

                    UserAuthStatusResponse authStatusResponse = Utils.convertObject(Utils.asJsonString(response.getData()), UserAuthStatusResponse.class);
                    assertNotNull(authStatusResponse.getUserName());
                    assertTrue(authStatusResponse.isUserAuthenticated());
                });
    }

    @Test
    public void validateAuthStatus_LoggedInButNoMatchingUser_Success() throws Exception {
        when(userService.getUserClientByToken()).thenThrow(new NotFoundException("not imp exception"));
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(userURI + "/" + "auth-status")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());

                    UserAuthStatusResponse authStatusResponse = Utils.convertObject(Utils.asJsonString(response.getData()), UserAuthStatusResponse.class);
                    assertNull(authStatusResponse.getUserName());
                    assertFalse(authStatusResponse.isUserAuthenticated());
                });
    }

    @Test
    public void validateAuthStatus_LoggedInButUnauthorized_Success() throws Exception {
        when(userService.getUserClientByToken()).thenThrow(new UnauthorizedException("not imp exception"));
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(userURI + "/" + "auth-status")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());

                    UserAuthStatusResponse authStatusResponse = Utils.convertObject(Utils.asJsonString(response.getData()), UserAuthStatusResponse.class);
                    assertNull(authStatusResponse.getUserName());
                    assertFalse(authStatusResponse.isUserAuthenticated());
                });
    }

    @Test
    public void validateAuthStatus_LoggedOutData_Success() throws Exception {
        when(userService.getUserClientByToken()).thenReturn(null);
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(userURI + "/" + "auth-status")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());

                    UserAuthStatusResponse authStatusResponse = Utils.convertObject(Utils.asJsonString(response.getData()), UserAuthStatusResponse.class);
                    assertNull(authStatusResponse.getUserName());
                    assertFalse(authStatusResponse.isUserAuthenticated());
                });
    }

    @Test
    public void deleteUser_DeletionUnsuccessful_Failure() throws Exception {
        when(userService.deleteUser(testSsoId)).thenReturn(false);
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .delete(usersURI + "/" + testSsoId)
                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertFalse(response.isSuccess());
                });
    }

    @Test
    public void deleteUser_DeletionSuccessful_success() throws Exception {
        when(userService.deleteUser(testSsoId)).thenReturn(true);
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .delete(usersURI + "/" + testSsoId)
                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                });
    }

    @Test
    public void fetchUserDetails_AllData_Success() throws Exception {
        when(userService.fetchUserData(testSsoId)).thenReturn(getUserAccessData());
        when(userService.fetchHierarchiesAssociatedWithUser(1L)).thenReturn(Collections.singletonList(testHierarchyMappingId));
        when(hierarchyService.fetchAllHierarchies(any(List.class), any(Long.class))).thenReturn(Collections.singletonList(getValidHierarchyResponse()));
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(usersURI + "/" + testSsoId + "?includeAllDetails=true")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getValidCreateUserRequest()))
                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());
                    UserDetailsResponse userDetailsResponse = Utils.convertObject(Utils.asJsonString(response.getData()), UserDetailsResponse.class);
                    assertNotNull(userDetailsResponse.getUser());
                    assertNotNull(userDetailsResponse.getHierarchy());
                    assertEquals(testUserName, userDetailsResponse.getUser().getUsername());
                    assertEquals(1, userDetailsResponse.getHierarchy().size());
                });
    }

    @Test
    public void getHeaderDetails_LoggedInData_EmptyUrl_Success() throws Exception {
        when(userService.getUserClientByToken()).thenReturn(getClientUserAccessData());
        when(hierarchyService.fetchHierarchyById(anyLong(), any())).thenReturn(getValidHierarchyResponseData());
        when(deploymentService.get(anyLong())).thenReturn(getEmptyUrlDeployment());
        when(userService.buildUserHeaderDetails(any(UserAccess.class), any())).thenReturn(getAuthenticatedHeaderResponse());
        when(clientService.getDefaultAskForUrl()).thenReturn(getClient().getAskforhelpurl());
        when(clientService.getFaqUrl()).thenReturn("dummy-url");
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(userURI + "/header")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());

                    HeaderResponse headerResponse = Utils.convertObject(Utils.asJsonString(response.getData()), HeaderResponse.class);
                    assertTrue(headerResponse.isUserLoggedIn());
                    assertEquals(headerResponse.getHeaderLinks().get("1").get("Value"), getClient().getAskforhelpurl());
                    assertNotNull(headerResponse.getDropdownItems());
                    assertNotNull(headerResponse.getSearchCriteria());
                    assertNotNull(headerResponse.getHeaderLinks());

                });
    }

    @Test
    public void getHeaderDetails_UnauthData_EmptyUrl_Success() throws Exception {
        when(userService.getUserClientByToken()).thenThrow(new UnauthorizedException("not imp exception"));
        when(hierarchyService.fetchHierarchyById(anyLong(), any())).thenReturn(getValidHierarchyResponseData());
        when(deploymentService.get(anyLong())).thenReturn(getEmptyUrlDeployment());
        when(userService.buildUserHeaderDetails(any(UserAccess.class), any())).thenReturn(getAuthenticatedHeaderResponse());
        when(clientService.getDefaultAskForUrl()).thenReturn(getClient().getAskforhelpurl());
        when(clientService.getFaqUrl()).thenReturn("dummy-url");
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(userURI + "/header")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());

                    HeaderResponse headerResponse = Utils.convertObject(Utils.asJsonString(response.getData()), HeaderResponse.class);
                    assertFalse(headerResponse.isUserLoggedIn());
                    assertEquals(headerResponse.getHeaderLinks().get("1").get("Value"), getClient().getAskforhelpurl());
                    assertNull(headerResponse.getDropdownItems());
                    assertNull(headerResponse.getSearchCriteria());
                    assertNotNull(headerResponse.getHeaderLinks());

                });
    }

    @Test
    public void getHeaderDetails_LoggedInData_Success() throws Exception {
        when(userService.getUserClientByToken()).thenReturn(getClientUserAccessData());
        when(hierarchyService.fetchHierarchyById(anyLong(), any())).thenReturn(getValidHierarchyResponseData());
        when(deploymentService.get(anyLong())).thenReturn(getFilledUrlDeployment());
        when(userService.buildUserHeaderDetails(any(UserAccess.class), any())).thenReturn(getAuthenticatedHeaderResponse());
        when(clientService.getDefaultAskForUrl()).thenReturn(getClient().getAskforhelpurl());
        when(clientService.getFaqUrl()).thenReturn("dummy-url");
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(userURI + "/header")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());

                    HeaderResponse headerResponse = Utils.convertObject(Utils.asJsonString(response.getData()), HeaderResponse.class);
                    assertTrue(headerResponse.isUserLoggedIn());
                    assertEquals(headerResponse.getHeaderLinks().get("1").get("Value"), getFilledUrlDeployment().getAskForHelpUrl());
                    assertNotNull(headerResponse.getDropdownItems());
                    assertNotNull(headerResponse.getSearchCriteria());
                    assertNotNull(headerResponse.getHeaderLinks());
                });
    }

    @Test
    public void testLoginRedirection_Success() throws Exception {
        when(userService.getUserClientByToken()).thenReturn(null);
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(userURI + "/login")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getValidRedirectionRequest()))

                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());

                    RedirectionResponse redResponse = Utils.convertObject(Utils.asJsonString(response.getData()), RedirectionResponse.class);
                    assertNull(redResponse.getRedirectUrl());
                });
    }

    @Test
    public void testLoginRedirection_Client_Success() throws Exception {
        when(userService.getUserClientByToken()).thenThrow(new UnauthorizedException(ValidationStatusEnum.USER_SESSION_NOT_FOUND));
        when(clientService.getRedirectUrl(any())).thenReturn(askForHelpUrl);
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(userURI + "/login")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getValidRedirectionRequest()))

                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());

                    RedirectionResponse redResponse = Utils.convertObject(Utils.asJsonString(response.getData()), RedirectionResponse.class);
                    assertNotNull(redResponse.getRedirectUrl());
                });
    }

    @Test
    public void testLoginRedirection_LoggedIn_Failure() throws Exception {
        when(userService.getUserClientByToken()).thenReturn(getClientUserAccessData());
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(userURI + "/login")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getValidRedirectionRequest()))

                )
                .andExpect(status().isConflict())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertFalse(response.isSuccess());
                    assertNotNull(response.getMessage());
                    assertEquals(response.getMessage(), "User is already logged in");
                });
    }

    @Test
    public void testLoginRedirection_UserNotFound_Failure() throws Exception {
        when(userService.getUserClientByToken()).thenThrow(new NotFoundException(ValidationStatusEnum.USER_NOT_FOUND));
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(userURI + "/login")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getValidRedirectionRequest()))

                )
                .andExpect(status().isNotFound())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertFalse(response.isSuccess());
                    assertNotNull(response.getMessage());
                    assertEquals(response.getMessage(), "The user does not exist");
                });
    }

    @Test
    public void testLoginRedirection_RedirectionEmptyRequest_Failure() throws Exception {
        when(userService.getUserClientByToken()).thenReturn(null);
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(userURI + "/login")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getInvalidRedirectionRequest()))

                )
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertFalse(response.isSuccess());
                    assertNotNull(response.getMessage());
                    assertEquals(response.getMessage(), FieldValidationMessages.INVALID_REQUEST_BODY);
                });
    }

    @Test
    public void testLanguageByUser_Success() throws Exception {
        when(userService.getUserClientByToken()).thenReturn(getClientUserAccessData());
        when(hierarchyService.fetchHierarchyById(eq(testHierarchyMappingId), eq(clientId_Nikshay))).thenReturn(getValidHierarchyResponseData());
        when(deploymentService.get(eq(Long.parseLong(testDeploymentId)))).thenReturn(getFilledUrlDeployment());
        when(languageService.getAll()).thenReturn(getAllLanguages());
        when(languageService.getMappings(eq(Long.parseLong(testDeploymentId)))).thenReturn(getDeploymentLanguages());
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(userURI + "/languages")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());

                    Map<String, String> langResponse = Utils.convertObject(Utils.asJsonString(response.getData()), Map.class);
                    assertNotNull(langResponse);
                    assertEquals(langResponse.size(), 1L);
                });
    }

    @Test
    public void testLanguageByUser_UserNotFound_Failure() throws Exception {
        when(userService.getUserClientByToken()).thenThrow(new NotFoundException(ValidationStatusEnum.USER_NOT_FOUND));
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(userURI + "/languages")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertFalse(response.isSuccess());
                    assertNotNull(response.getMessage());
                    assertEquals(response.getMessage(), "The user does not exist");
                });
    }

    @Test
    public void testLanguageByUser_UserUnauthorized_Failure() throws Exception {
        when(userService.getUserClientByToken()).thenThrow(new UnauthorizedException(ValidationStatusEnum.USER_SESSION_NOT_FOUND));
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(userURI + "/languages")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertFalse(response.isSuccess());
                    assertNotNull(response.getMessage());
                    assertEquals(response.getMessage(), ValidationStatusEnum.USER_SESSION_NOT_FOUND.getMessage());
                });
    }

    @Test
    public void testLanguageByUser_HierarchyNotFound_Failure() throws Exception {
        when(userService.getUserClientByToken()).thenReturn(getClientUserAccessData());
        when(hierarchyService.fetchHierarchyById(eq(testHierarchyMappingId), eq(clientId_Nikshay))).thenThrow(new NotFoundException(ValidationStatusEnum.HIERARCHY_NOT_FOUND));
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(userURI + "/languages")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertFalse(response.isSuccess());
                    assertNotNull(response.getMessage());
                    assertEquals(response.getMessage(), ValidationStatusEnum.HIERARCHY_NOT_FOUND.getMessage());
                });
    }

    @Test
    public void testSidebarItem_Success() throws Exception {
        when(userService.getUserClientByToken()).thenReturn(getClientUserAccessData());
        when(hierarchyService.fetchHierarchyById(eq(testHierarchyMappingId), eq(clientId_Nikshay))).thenReturn(getValidHierarchyResponseData());
        when(deploymentService.get(eq(Long.parseLong(testDeploymentId)))).thenReturn(getFilledUrlDeployment());
        when(userService.getSidebarItems(any(UserAccess.class), any(Deployment.class))).thenReturn(getSidebarItemResponses());

        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(userURI + "/sidebar-items")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());

                    SidebarResponse sidebarResponse = Utils.convertObject(Utils.asJsonString(response.getData()), SidebarResponse.class);
                    assertEquals(2, sidebarResponse.getSidebarItemsList().size());
                });
    }

    @Test
    public void testSidebarItem_HierarchyNotFound() throws Exception {
        when(userService.getUserClientByToken()).thenReturn(getClientUserAccessData());
        when(hierarchyService.fetchHierarchyById(eq(testHierarchyMappingId), eq(clientId_Nikshay))).thenThrow(new NotFoundException(ValidationStatusEnum.HIERARCHY_NOT_FOUND));
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(userURI + "/sidebar-items")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertFalse(response.isSuccess());
                    assertNotNull(response.getMessage());
                    assertEquals(response.getMessage(), ValidationStatusEnum.HIERARCHY_NOT_FOUND.getMessage());
                });
    }

    @Test
    public void testGetSidebarItems_UserUnauthorized_Failure() throws Exception {
        when(userService.getUserClientByToken()).thenThrow(new UnauthorizedException(ValidationStatusEnum.USER_SESSION_NOT_FOUND));
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(userURI + "/sidebar-items")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertFalse(response.isSuccess());
                    assertNotNull(response.getMessage());
                    assertEquals(response.getMessage(), ValidationStatusEnum.USER_SESSION_NOT_FOUND.getMessage());
                });
    }

    @Test
    public void testGetUppFilters_UserUnauthorized_Failure() throws Exception {
        when(userService.getUserClientByToken()).thenThrow(new UnauthorizedException(ValidationStatusEnum.USER_SESSION_NOT_FOUND));
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(userURI + "/upp-filters")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertFalse(response.isSuccess());
                    assertNotNull(response.getMessage());
                    assertEquals(response.getMessage(), ValidationStatusEnum.USER_SESSION_NOT_FOUND.getMessage());
                });
    }

    @Test
    public void testGetUppFilters_HierarchyNotFound_Failure() throws Exception {
        when(userService.getUserClientByToken()).thenReturn(getClientUserAccessData());
        when(adherenceService.getAllTechnologyOptions()).thenReturn(new ArrayList<>());
        when(userService.getUppFilterOptions(any(UserAccess.class), eq(new ArrayList<>()))).thenReturn(getUnifiedListResponse());
        when(hierarchyService.addHierarchyOptionsToUppFilter(eq(testHierarchyMappingId), eq(clientId_Nikshay), any(UnifiedListFilterResponse.class))).thenThrow(new NotFoundException(ValidationStatusEnum.HIERARCHY_NOT_FOUND));
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(userURI + "/upp-filters")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertFalse(response.isSuccess());
                    assertNotNull(response.getMessage());
                    assertEquals(response.getMessage(), ValidationStatusEnum.HIERARCHY_NOT_FOUND.getMessage());
                });
    }

    @Test
    public void testGetUppFilters_Success() throws Exception {
        when(userService.getUserClientByToken()).thenReturn(getClientUserAccessData());
        when(adherenceService.getAllTechnologyOptions()).thenReturn(Arrays.asList(new AdherenceTechnologyDto(), new AdherenceTechnologyDto(), new AdherenceTechnologyDto()));
        when(userService.getUppFilterOptions(any(UserAccess.class), any(List.class))).thenReturn(getUnifiedListResponse());
        when(hierarchyService.addHierarchyOptionsToUppFilter(eq(testHierarchyMappingId), eq(clientId_Nikshay), any(UnifiedListFilterResponse.class))).thenReturn(getValidUnifiedListResponse());
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(userURI + "/upp-filters")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());
                    UnifiedListFilterResponse unifiedListFilterResponse = Utils.convertObject(Utils.asJsonString(response.getData()), UnifiedListFilterResponse.class);
                    assertEquals(2, unifiedListFilterResponse.getStageOptions().size());
                    assertEquals(3, unifiedListFilterResponse.getTechnologyOptions().size());
                    assertNull(unifiedListFilterResponse.getEarliestEnrollment());
                    assertEquals(2, unifiedListFilterResponse.getHierarchyOptions().size());
                });
    }

    @Test
    public void testGetTaskListDetails_Success() throws Exception {
        when(userService.getUserClientByToken()).thenReturn(getClientUserAccessData());
        when(hierarchyService.fetchHierarchyById(eq(testHierarchyMappingId), eq(clientId_Nikshay))).thenReturn(getValidHierarchyResponseData());
        when(deploymentService.get(eq(Long.parseLong(testDeploymentId)))).thenReturn(getFilledUrlDeployment());
        when(taskListService.getTaskListFilterResponseByDeploymentId(eq(Long.parseLong(testDeploymentId)), eq(false))).thenReturn(Arrays.asList(new TaskListItemResponse()));
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(userURI + "/tasklist/details")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());
                    List<TaskListItemResponse> taskListItemResponses = (List<TaskListItemResponse>) Utils.convertObject(Utils.asJsonString(response.getData()), (new TypeReference<List<TaskListItemResponse>>() {}));
                    assertEquals(1, taskListItemResponses.size());
                });
    }

    @Test
    public void testGetTaskListDetails_UserUnauthorized_Failure() throws Exception {
        when(userService.getUserClientByToken()).thenThrow(new UnauthorizedException(ValidationStatusEnum.USER_SESSION_NOT_FOUND));
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(userURI + "/tasklist/details")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertFalse(response.isSuccess());
                    assertNotNull(response.getMessage());
                    assertEquals(response.getMessage(), ValidationStatusEnum.USER_SESSION_NOT_FOUND.getMessage());
                });
    }

    @Test
    public void testGetTaskListDetails_HierarchyNotFound_Failure() throws Exception {
        when(userService.getUserClientByToken()).thenReturn(getClientUserAccessData());
        when(hierarchyService.fetchHierarchyById(eq(testHierarchyMappingId), eq(clientId_Nikshay))).thenThrow(new NotFoundException(ValidationStatusEnum.HIERARCHY_NOT_FOUND));
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .get(userURI + "/tasklist/details")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertFalse(response.isSuccess());
                    assertNotNull(response.getMessage());
                    assertEquals(response.getMessage(), ValidationStatusEnum.HIERARCHY_NOT_FOUND.getMessage());
                });
    }

    @Test
    public void testLogoutRedirection_Client_Success() throws Exception {
        when(userService.getUserClientByToken()).thenThrow(new UnauthorizedException(ValidationStatusEnum.USER_SESSION_NOT_FOUND));
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(userURI + "/logout")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getValidRedirectionRequest()))

                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());
                    RedirectionResponse redResponse = Utils.convertObject(Utils.asJsonString(response.getData()), RedirectionResponse.class);
                    assertNotNull(redResponse.getRedirectUrl());
                });
    }

    @Test
    public void testLogoutRedirection_LoggedIn_Success() throws Exception {
        when(userService.getUserClientByToken()).thenReturn(getClientUserAccessData());
        when(clientService.getLogoutUrl(any())).thenReturn(askForHelpUrl);
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(userURI + "/logout")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getValidRedirectionRequest()))

                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());
                    RedirectionResponse redResponse = Utils.convertObject(Utils.asJsonString(response.getData()), RedirectionResponse.class);
                    assertNotNull(redResponse.getRedirectUrl());
                    assertEquals(askForHelpUrl,redResponse.getRedirectUrl());
                });
    }

    @Test
    public void testLogoutRedirection_UserNotFound_Failure() throws Exception {
        when(userService.getUserClientByToken()).thenThrow(new NotFoundException(ValidationStatusEnum.USER_NOT_FOUND));
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(userURI + "/logout")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getValidRedirectionRequest()))

                )
                .andExpect(status().isOk())
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertTrue(response.isSuccess());
                    assertNotNull(response.getData());
                    RedirectionResponse redResponse = Utils.convertObject(Utils.asJsonString(response.getData()), RedirectionResponse.class);
                    assertNotNull(redResponse.getRedirectUrl());
                });
    }

    @Test
    public void testLogoutRedirection_RedirectionEmptyRequest_Failure() throws Exception {
        when(userService.getUserClientByToken()).thenReturn(null);
        mockMvc.
                perform(
                        MockMvcRequestBuilders
                                .post(userURI + "/logout")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(getInvalidRedirectionRequest()))

                )
                .andExpect(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Response response = Utils.convertObject(json, Response.class);
                    assertFalse(response.isSuccess());
                    assertNotNull(response.getMessage());
                    assertEquals(response.getMessage(), FieldValidationMessages.INVALID_REQUEST_BODY);
                });
    }

    private CreateUserRequest getValidCreateUserRequest() {
        CreateUserRequest createUserRequest = new CreateUserRequest();
        createUserRequest.setUsername(testUserName);
        createUserRequest.setHierarchyId(testHierarchyMappingId);
        createUserRequest.setSsoId(testSsoId);
        return createUserRequest;
    }

    private CreateUserRequest getValidCreateSsoAndUserRequest() {
        CreateUserRequest createUserRequest = new CreateUserRequest();
        createUserRequest.setUsername(testUserName);
        createUserRequest.setHierarchyId(testHierarchyMappingId);
        createUserRequest.setSsoUserToBeCreated(true);
        createUserRequest.setPassword(testPassword);
        return createUserRequest;
    }

    private CreateUserRequest getInValidCreateUserRequestMissingUserName() {
        CreateUserRequest createUserRequest = new CreateUserRequest();
        createUserRequest.setHierarchyId(testHierarchyMappingId);
        createUserRequest.setSsoId(testSsoId);
        return createUserRequest;
    }

    private CreateUserRequest getInValidCreateUserRequestMissingHierarchyMapping() {
        CreateUserRequest createUserRequest = new CreateUserRequest();
        createUserRequest.setUsername(testUserName);
        createUserRequest.setSsoId(testSsoId);
        return createUserRequest;
    }

    private RedirectionRequest getValidRedirectionRequest() {
        RedirectionRequest redirectionRequest = new RedirectionRequest();
        redirectionRequest.setReturnUrl(askForHelpUrl);
        return redirectionRequest;
    }

    private RedirectionRequest getInvalidRedirectionRequest() {
        RedirectionRequest redirectionRequest = new RedirectionRequest();
        return redirectionRequest;
    }

    private CreateUserRequest getInValidCreateUserRequestMissingSsoId() {
        CreateUserRequest createUserRequest = new CreateUserRequest();
        createUserRequest.setUsername(testUserName);
        createUserRequest.setHierarchyId(testHierarchyMappingId);
        return createUserRequest;
    }

    private CreateUserRequest getInValidCreateUserRequestMissingPassword() {
        CreateUserRequest createUserRequest = new CreateUserRequest();
        createUserRequest.setUsername(testUserName);
        createUserRequest.setHierarchyId(testHierarchyMappingId);
        createUserRequest.setSsoUserToBeCreated(true);
        return createUserRequest;
    }

    private UserAccess getUserAccessData() {
        return new UserAccess(1L, testUserName, testHierarchyMappingId, true, clientId_Nikshay, testSsoId, true);
    }

    private Pair<Client, UserAccess> getClientUserAccessData() {
        UserAccess definedUser = new UserAccess(1L, testUserName, testHierarchyMappingId, true, clientId_Nikshay, testSsoId, true);
        definedUser.setClientId(clientId_Nikshay);
        return Pair.of( getClient(), definedUser);
    }

    private HierarchyResponse getValidHierarchyResponse() {
        HierarchyResponse hierarchyResponse = new HierarchyResponse();
        hierarchyResponse.setHierarchy(new HierarchyResponseData());
        hierarchyResponse.setAssociations(new HashMap<>());
        return hierarchyResponse;
    }

    private HierarchyResponseData getValidHierarchyResponseData() {
        HierarchyResponseData hierarchyResponse = new HierarchyResponseData();
        hierarchyResponse.setClientId(clientId_Nikshay);
        hierarchyResponse.setDeploymentId(testDeploymentId);
        return hierarchyResponse;
    }

    private Deployment getEmptyUrlDeployment() {
        return new Deployment();
    }

    private Deployment getFilledUrlDeployment() {
        Deployment deployment = new Deployment();
        deployment.setId(Long.parseLong(testDeploymentId));
        deployment.setAskForHelpUrl("http://dummy-url-2.com");
        return deployment;
    }

    private UnifiedListFilterResponse getUnifiedListResponse() {
        UnifiedListFilterResponse response = new UnifiedListFilterResponse();
        response.setStageOptions(Arrays.asList(Map.of("On Treatment", "ON_TREATMENT"), Map.of("Off Treatment", "OFF_TREATMENT")));
        response.setTechnologyOptions(Arrays.asList(new AdherenceTechnologyDto(), new AdherenceTechnologyDto(), new AdherenceTechnologyDto()));
        return response;
    }

    private UnifiedListFilterResponse getValidUnifiedListResponse() {
        UnifiedListFilterResponse response =  getUnifiedListResponse();
        response.setHierarchyOptions(Arrays.asList(
                Arrays.asList(
                new MinHierarchyDetails(testHierarchyMappingId, "testHierarchyName", "testHierarchyCode", "testHierarchyType", 1L, null)),
                Arrays.asList(
                new MinHierarchyDetails(123L, "testHierarchyName1", "testHierarchyCode1", "testHierarchyType1", 2L, testHierarchyMappingId))));
        return response;
    }

    private HeaderResponse getAuthenticatedHeaderResponse() {
        HeaderResponse headerResponse = new HeaderResponse();
        headerResponse.addDropdownOption(Pair.of("firstDO", "secondDO"));
        headerResponse.addSearchCriteriaOption(Pair.of("firstSC", "secondSC"));
        headerResponse.setUserLoggedIn(true);
        return headerResponse;
    }

    private UserSSOResponse ssoResponse() {
        return new UserSSOResponse(1L,testUserName, null, null, null);
    }

    private List<Language> getAllLanguages() {
        return Arrays.asList(new Language(1L, "English", "en", "English"), new Language(2L, "Hindi", "hi", "हिन्दी (Hindi)"));

    }

    private List<DeploymentLanguageMap> getDeploymentLanguages() {
        return Arrays.asList(new DeploymentLanguageMap(Long.parseLong(testDeploymentId), 1L, null));

    }

    private List<SidebarItemResponse> getSidebarItemResponses() {
        return Arrays.asList(new SidebarItemResponse(1L, "overview", "fa-fa-1", "http://imei1.org", false), new SidebarItemResponse(2L, "hierarchy_management", "fa-fa-2", "http://imei2.org", true));
    }
}

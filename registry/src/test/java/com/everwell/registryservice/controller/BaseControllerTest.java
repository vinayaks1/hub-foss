package com.everwell.registryservice.controller;

import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.constants.ValidationStatusEnum;
import com.everwell.registryservice.exceptions.NotFoundException;
import com.everwell.registryservice.exceptions.UnauthorizedException;
import com.everwell.registryservice.models.db.Client;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

public class BaseControllerTest extends BaseTest {

    @Mock
    private Authentication authentication;

    @Mock
    private SecurityContext securityContext;

    @Spy
    @InjectMocks
    private BaseController baseController;

    @Before
    public void setup() {
        SecurityContextHolder.setContext(securityContext);
    }

    @Test
    public void getClientId_AuthenticationFailure_Error() {
        UnauthorizedException unauthorizedException = assertThrows(UnauthorizedException.class, () -> baseController.getClientId());
        assertEquals(ValidationStatusEnum.AUTH_INVALID.getMessage(), unauthorizedException.getMessage());
    }

    @Test
    public void getClientId_Failure_Error() {
        when(securityContext.getAuthentication()).thenReturn(authentication);
        NotFoundException notFoundException = assertThrows(NotFoundException.class, () -> baseController.getClientId());
        assertEquals(ValidationStatusEnum.CLIENT_ID_UNAVAILABLE.getMessage(), notFoundException.getMessage());
    }

    @Test
    public void getClientId_Success() {
        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.getPrincipal()).thenReturn(getClient());
        Long clientId = baseController.getClientId();
        assertEquals(Optional.of(clientId_Nikshay), Optional.of(clientId));
    }

    protected Client getClient() {
        Client client = new Client(clientId_Nikshay, "test", "testP", LocalDateTime.now(), 1L, askForHelpUrl, ssoUrl, null);
        return client;
    }
}

package com.everwell.registryservice.controller;

import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.exceptions.CustomExceptionHandler;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TestControllerTests extends BaseTest {

    private MockMvc mockMvc;

    @InjectMocks
    private TestController testController;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(testController)
            .setControllerAdvice(new CustomExceptionHandler())
            .build();
    }


    @Test
    public void testGetClientSuccess() throws Exception {
        String uri = "/v1/test";
        mockMvc.
            perform(
                MockMvcRequestBuilders
                    .get(uri)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.content().string("Test controller working!"));
    }

}

package com.everwell.registryservice.mappers;

import com.everwell.registryservice.models.db.Config;
import com.everwell.registryservice.models.http.response.ConfigHierarchyMappingBaseResponse;
import com.everwell.registryservice.models.http.response.ConfigHierarchyMappingRecordResponse;
import com.everwell.registryservice.models.http.response.ConfigResponse;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNull;

public class ConfigMapperTest {

    private ConfigMapper configMapper;

    @Before
    public void before() {
        configMapper = new ConfigMapperImpl();
    }

    @Test
    public void configToConfigResponse_Null() {
        ConfigResponse configResponse = configMapper.configToConfigResponse(null);
        assertNull(configResponse);
    }

    @Test
    public void addConfigRequestToConfig_Null() {
        Config config = configMapper.addConfigRequestToConfig(null);
        assertNull(config);
    }

    @Test
    public void hierarchyConfigMapToConfigHierarchyMappingRecordResponse_Null() {
        ConfigHierarchyMappingRecordResponse configResponse = configMapper.hierarchyConfigMapToConfigHierarchyMappingRecordResponse(null);
        assertNull(configResponse);
    }

    @Test
    public void hierarchyConfigMapToConfigHierarchyMappingBaseResponse_Null() {
        ConfigHierarchyMappingBaseResponse configResponse = configMapper.hierarchyConfigMapToConfigHierarchyMappingBaseResponse(null);
        assertNull(configResponse);
    }
}

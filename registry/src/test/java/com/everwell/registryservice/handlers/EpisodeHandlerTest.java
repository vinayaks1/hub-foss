package com.everwell.registryservice.handlers;

import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.binders.EpisodeBinder;
import com.everwell.registryservice.constants.NotificationConstants;
import com.everwell.registryservice.exceptions.HandlerException;
import com.everwell.registryservice.models.dto.GenericEvent;
import com.everwell.registryservice.models.http.requests.ins.TriggerDetailsRequest;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.messaging.MessageChannel;

import java.util.*;

import static org.mockito.ArgumentMatchers.any;

public class EpisodeHandlerTest extends BaseTest {

    @Spy
    @InjectMocks
    EpisodeHandler episodeHandler;

    @Mock
    EpisodeBinder episodeBinder;

    @Mock
    MessageChannel episodeNotificationOutput;

    @Test
    public void sendTriggersToEpisodeTest() {

        Long clientId = 1L;
        List<Long> templateIds = new ArrayList<>();
        templateIds.add(1L);
        TriggerDetailsRequest trigger = new TriggerDetailsRequest(1L, 1L, 1L, templateIds,true,true,true, "test", 1L, "test", "test");
        GenericEvent<TriggerDetailsRequest> event = new GenericEvent<>(trigger, NotificationConstants.EPISODE_TRIGGER);
        List<GenericEvent<TriggerDetailsRequest>> triggers = new ArrayList<>();
        triggers.add(event);

        Mockito.when(episodeBinder.episodeNotificationOutput()).thenReturn(episodeNotificationOutput);
        episodeHandler.sendTriggersToEpisode(triggers, clientId);

        Mockito.verify(episodeNotificationOutput, Mockito.times(1)).send(any());
    }

    @Test(expected = HandlerException.class)
    public void sendTriggersToEpisodeTestException() {

        Long clientId = 1L;
        List<Long> templateIds = new ArrayList<>();
        templateIds.add(1L);
        TriggerDetailsRequest trigger = new TriggerDetailsRequest(1L, 1L, 1L, templateIds,true,true,true, "test", 1L, "test", "test");
        GenericEvent<TriggerDetailsRequest> event = new GenericEvent<>(trigger, NotificationConstants.EPISODE_TRIGGER);
        List<GenericEvent<TriggerDetailsRequest>> triggers = new ArrayList<>();
        triggers.add(event);

        Mockito.when(episodeBinder.episodeNotificationOutput()).thenReturn(null);
        episodeHandler.sendTriggersToEpisode(triggers, clientId);
    }

}

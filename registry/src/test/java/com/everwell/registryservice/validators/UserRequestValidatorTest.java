package com.everwell.registryservice.validators;

import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.constants.ValidationStatusEnum;
import com.everwell.registryservice.exceptions.ConflictException;
import com.everwell.registryservice.models.db.UserAccess;
import com.everwell.registryservice.models.http.requests.CreateUserRequest;
import com.everwell.registryservice.repository.UserAccessRepository;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.when;


public class UserRequestValidatorTest extends BaseTest {

    private final String testUserName = "testUser";
    private final Long testSsoId = 123L;
    private final Long testHierarchyMappingId = 123456L;
    @Spy
    @InjectMocks
    private UserRequestValidator userRequestValidator;
    @Mock
    private UserAccessRepository userAccessRepository;

    @Test
    public void validateCreateUserRequest_userAlreadyExists_Error() {
        when(userAccessRepository.findBySsoIdOrUsername(testSsoId, testUserName)).thenReturn(Optional.of(new UserAccess()));
        ConflictException conflictException = assertThrows(ConflictException.class, () -> userRequestValidator.validateCreateUserRequest(getValidCreateUserRequest()));
        assertEquals(ValidationStatusEnum.USER_ALREADY_EXISTS.getMessage(), conflictException.getMessage());
    }

    @Test
    public void validateCreateUserRequest_userNotExists_Success() {
        when(userAccessRepository.findBySsoIdOrUsername(testSsoId, testUserName)).thenReturn(Optional.empty());
        userRequestValidator.validateCreateUserRequest(getValidCreateUserRequest());
    }

    private CreateUserRequest getValidCreateUserRequest() {
        CreateUserRequest createUserRequest = new CreateUserRequest();
        createUserRequest.setUsername(testUserName);
        createUserRequest.setHierarchyId(testHierarchyMappingId);
        createUserRequest.setSsoId(testSsoId);
        return createUserRequest;
    }
}

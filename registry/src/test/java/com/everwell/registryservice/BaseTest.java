package com.everwell.registryservice;

import com.everwell.registryservice.models.db.Client;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;

import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@AutoConfigureMockMvc
public abstract class BaseTest {
    protected final Long clientId_Nikshay = 29L;
    protected final Long clientId_Hub = 63L;
    protected final String askForHelpUrl = "www.google.com";
    protected final String ssoUrl = "www.sso.com";

    protected Client getClient() {
        Client client = new Client(clientId_Nikshay, "test", "testP", LocalDateTime.now());
        client.setAskforhelpurl("http://dummy-url.com");
        return client;
    }

    protected void mockClient(){
        Authentication authentication = Mockito.mock(Authentication.class);
        when(authentication.getPrincipal()).thenReturn(getClient());
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
    }
}

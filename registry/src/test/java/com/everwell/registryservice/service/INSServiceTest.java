package com.everwell.registryservice.service;

import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.models.dto.LanguageMapping;
import com.everwell.registryservice.models.dto.ParameterExchange;
import com.everwell.registryservice.models.http.requests.ins.TemplateRequest;
import com.everwell.registryservice.models.http.response.Response;
import com.everwell.registryservice.models.http.response.ins.LanguageResponse;
import com.everwell.registryservice.models.http.response.ins.TemplateResponse;
import com.everwell.registryservice.service.impl.INSServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class INSServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    INSServiceImpl insHelperService;

    private static String clientId = "1";

    @Before
    public void init() {
        ReflectionTestUtils.setField(insHelperService, "dataGatewayURL", "http://test:8080");
        ReflectionTestUtils.setField(insHelperService, "username", "test");
        ReflectionTestUtils.setField(insHelperService, "password", "test");
    }

    @Test
    public void getAllTemplatesTest() {
        TemplateResponse templateResponse = new TemplateResponse(1L, "test", "test", "test", "test");
        ResponseEntity<Response<List<TemplateResponse>>> response = new ResponseEntity<>(new Response<>(true, Arrays.asList(templateResponse)), HttpStatus.OK);

        doReturn(response).when(insHelperService).getExchange(any());

        ResponseEntity<Response<List<TemplateResponse>>> apiResponse = insHelperService.getAllTemplates(clientId);

        assertEquals(templateResponse.getTemplateId(), apiResponse.getBody().getData().get(0).getTemplateId());
        assertEquals(1, apiResponse.getBody().getData().size());
    }

    @Test
    public void saveTemplateTest() {
        TemplateRequest templateRequest = new TemplateRequest("test", 1L, 1L, "test", true);

        doReturn(new ResponseEntity<>(new Response<>(true, 1), HttpStatus.OK)).when(insHelperService).postExchange(any());

        ResponseEntity<Response<Long>> apiResponse = insHelperService.saveTemplate(clientId, templateRequest);

        assertEquals(true, apiResponse.getBody().isSuccess());
    }

    @Test(expected = Exception.class)
    public void saveTemplateTestContentNotFound() {
        TemplateRequest templateRequest = new TemplateRequest(null, 1L, 1L, "test", true);

        insHelperService.saveTemplate(clientId, templateRequest);
    }

    @Test(expected = Exception.class)
    public void saveTemplateTestLanguageIdNotFound() {
        TemplateRequest templateRequest = new TemplateRequest("test", null, 1L, "test", true);

        insHelperService.saveTemplate(clientId, templateRequest);
    }

    @Test(expected = Exception.class)
    public void saveTemplateTestTypeIdNotFound() {
        TemplateRequest templateRequest = new TemplateRequest("test", 1L, null, "test", true);

        insHelperService.saveTemplate(clientId, templateRequest);
    }

    @Test
    public void getAllLanguagesTest() {
        LanguageResponse languageResponse = getLanguageResponse();
        ResponseEntity<Response<LanguageResponse>> response = new ResponseEntity<>(new Response<>(true, languageResponse), HttpStatus.OK);

        doReturn(response).when(insHelperService).getExchange(any());

        ResponseEntity<Response<LanguageResponse>> apiResponse = insHelperService.getAllLanguages(clientId);

        assertEquals(languageResponse.getLanguageMappings().size(), apiResponse.getBody().getData().getLanguageMappings().size());
    }

    private LanguageResponse getLanguageResponse(){
        LanguageMapping englishMap = new LanguageMapping(1L, "English");
        LanguageMapping amharicMap = new LanguageMapping(2L, "Amharic");
        LanguageMapping bengaliMap = new LanguageMapping(3L, "Bengali");
        LanguageMapping kirghizMap = new LanguageMapping(4L, "Kirghiz");
        LanguageResponse languageResponse = new LanguageResponse(Arrays.asList(englishMap, amharicMap, bengaliMap, kirghizMap));
        return languageResponse;
    }
}

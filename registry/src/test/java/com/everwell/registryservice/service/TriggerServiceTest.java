package com.everwell.registryservice.service;

import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.constants.ValidationStatusEnum;
import com.everwell.registryservice.exceptions.NotFoundException;
import com.everwell.registryservice.models.db.*;
import com.everwell.registryservice.models.dto.EngagementDetails;
import com.everwell.registryservice.models.http.requests.ins.TriggerDetailsRequest;
import com.everwell.registryservice.exceptions.ValidationException;
import com.everwell.registryservice.models.dto.HierarchyResponseData;
import com.everwell.registryservice.repository.TriggerRepository;
import com.everwell.registryservice.models.http.response.Response;
import com.everwell.registryservice.models.http.response.TriggersAndTemplatesResponse;
import com.everwell.registryservice.models.http.response.ins.TemplateResponse;
import com.everwell.registryservice.models.http.requests.UpdateTriggerDetailsRequest;
import com.everwell.registryservice.models.dto.AddTriggerDetails;
import com.everwell.registryservice.models.http.requests.AddTriggerDetailsRequest;
import com.everwell.registryservice.models.http.requests.ins.TemplateRequest;
import com.everwell.registryservice.models.http.response.AddTriggerResponse;
import com.everwell.registryservice.repository.VendorMappingRepository;
import com.everwell.registryservice.service.impl.TriggerServiceImpl;
import org.jobrunr.jobs.lambdas.JobLambda;
import org.jobrunr.scheduling.JobScheduler;
import org.junit.Test;
import org.mockito.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.util.*;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TriggerServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    TriggerServiceImpl triggerService;

    @Mock
    TriggerRepository triggerRepository;

    @Mock
    VendorMappingRepository vendorMappingRepository;

    @Mock
    ClientService clientService;

    @Mock
    INSService insService;

    @Mock
    HierarchyService hierarchyService;

    @Mock
    EngagementService engagementService;

    @Mock
    DeploymentService deploymentService;

    @Mock
    VendorService vendorService;

    @Mock
    JobScheduler jobScheduler;

    @Test
    public void readTriggerTableTest_registryTrigger()
    {
        Trigger trigger = new Trigger(1L, 1L, 1L, 1L, 1L, "",
                "", "SampleJob", "", false, "",
                "Registry", false, false);

        when(triggerRepository.getAllByCronTimeNotNullAndActiveTrue()).thenReturn(Collections.singletonList(trigger));
        doNothing().when(jobScheduler).delete(eq(""));
        triggerService.readTriggerTable();

        verify(jobScheduler, Mockito.times(1)).delete(eq(""));
    }
    @Test
    public void readTriggerTableTest_episodeTrigger()
    {
        Trigger trigger = new Trigger(1L, 1L, 1L, 1L, 1L, "",
                "", "SampleJob", "", false, "",
                "Episode", false, false);

        when(triggerRepository.getAllByCronTimeNotNullAndActiveTrue()).thenReturn(Collections.singletonList(trigger));
        doNothing().when(jobScheduler).delete(eq(""));
        when(jobScheduler.scheduleRecurrently(anyString(), anyString(), (JobLambda) any())).thenReturn("");
        triggerService.readTriggerTable();

        verify(jobScheduler, Mockito.times(0)).delete(eq(""));
    }
    @Test
    public void invokeFunctionTest()
    {
        Trigger trigger = new Trigger(1L, 1L, 1L, 1L, 1L, "",
                "", "SampleJob", "", false, "",
                "Registry", false, false);
        triggerService.invokeFuncFromFunctionName(trigger);
        verify(triggerService, times(1)).SampleJob();
    }
    @Test(expected = NotFoundException.class)
    public void invokeFunctionTest_Failure()
    {
        Trigger trigger = new Trigger(1L, 1L, 1L, 1L, 1L, "",
                "", "SampleJob1", "", false, "",
                "Registry", false, false);

        triggerService.invokeFuncFromFunctionName(trigger);
    }
    @Test
    public void getAllTriggers()
    {
        Client client = new Client(1L, "", "", LocalDateTime.now(), 1L, "www.google.com", ssoUrl, null);
        when(clientService.getClientByToken()).thenReturn(client);
        Deployment deployment = new Deployment(
                1L,
                "IND",
                123,
                "India Standard Time",
                "IST",
                "Asia/Kolkata",
                1L,
                null,
                "",
                "India",
                "-",
                "https://help.url",
                "10:00 AM"
        );
        List<Trigger> trigger = new ArrayList<>(Arrays.asList(
                new Trigger(1L, 1L, 1L, 1L, 1L, "1",
                        "", "SampleJob1", "", false, "",
                        "Registry", false, false),
                new Trigger(1L, 1L, 1L, 1L, 2L, "",
                        "", "SampleJob1", "", false, "",
                        "Registry", false, false)
                ));

        when(triggerRepository.findAll()).thenReturn(trigger);
        HierarchyResponseData hierarchy = new HierarchyResponseData();
        hierarchy.setDeploymentId("1");

        when(deploymentService.get(any())).thenReturn(deployment);
        when(hierarchyService.fetchHierarchyById(any(), any())).thenReturn(hierarchy);
        ResponseEntity<Response<List<TemplateResponse>>> templateResponse = new ResponseEntity<>(
                        new Response<>(true,
                            Collections.singletonList(
                                new TemplateResponse(1L, "", "", "", ""))),
                        HttpStatus.OK);

        when(insService.getAllTemplates(any())).thenReturn(templateResponse);

        List<TriggersAndTemplatesResponse> output = triggerService.getAllTriggers();

        assertEquals(output.size(), 2);
        assertEquals(output.get(0).getTemplates().size(), 1);
        assertEquals(output.get(1).getTemplates().size(), 0);
    }

    @Test
    public void getAllTriggers_HierarchyNotFound()
    {
        Client client = new Client(1L, "", "", LocalDateTime.now(), 1L, "www.google.com", ssoUrl, null);
        when(clientService.getClientByToken()).thenReturn(client);
        Deployment deployment = new Deployment(
                1L,
                "IND",
                123,
                "India Standard Time",
                "IST",
                "Asia/Kolkata",
                1L,
                null,
                "",
                "India",
                "-",
                "https://help.url",
                "10:00 AM"
        );
        List<Trigger> trigger = new ArrayList<>(Arrays.asList(
                new Trigger(1L, 1L, 1L, 1L, 1L, "1",
                        "", "SampleJob1", "", false, "",
                        "Registry", false, false),
                new Trigger(1L, 1L, 1L, 1L, 2L, "",
                        "", "SampleJob1", "", false, "",
                        "Registry", false, false)
        ));

        when(triggerRepository.findAll()).thenReturn(trigger);
        HierarchyResponseData hierarchy = new HierarchyResponseData();
        hierarchy.setDeploymentId("1");

        when(deploymentService.get(any())).thenReturn(deployment);
        when(hierarchyService.fetchHierarchyById(any(), any())).thenThrow(new NotFoundException(ValidationStatusEnum.HIERARCHY_NOT_FOUND));
        ResponseEntity<Response<List<TemplateResponse>>> templateResponse = new ResponseEntity<>(
                new Response<>(true,
                        Collections.singletonList(
                                new TemplateResponse(1L, "", "", "", ""))),
                HttpStatus.OK);

        when(insService.getAllTemplates(any())).thenReturn(templateResponse);

        List<TriggersAndTemplatesResponse> output = triggerService.getAllTriggers();

        assertEquals(output.size(), 0);
    }

    @Test
    public void getTriggersByHierarchy_recurssiveCall()
    {
        HierarchyResponseData hierarchy = new HierarchyResponseData();
        hierarchy.setId(1L);
        hierarchy.setDeploymentId("1");
        hierarchy.setLevel(2L);
        hierarchy.setParentId(2L);

        HierarchyResponseData hierarchy2 = new HierarchyResponseData();
        hierarchy2.setId(2L);
        hierarchy2.setDeploymentId("1");

        when(clientService.getClientByToken()).thenReturn(getClient());
        when(hierarchyService.fetchHierarchyById(eq(1L), eq(getClient().getId()))).thenReturn(hierarchy);
        when(hierarchyService.fetchHierarchyById(eq(2L), eq(getClient().getId()))).thenReturn(hierarchy2);

        List<Trigger> trigger = new ArrayList<>(Arrays.asList(
                new Trigger(1L, 2L, 1L, 1L, 1L, "1",
                        "", "SampleJob1", "", false, "",
                        "Registry", false, true),
                new Trigger(1L, 2L, 1L, 1L, 2L, "",
                        "", "SampleJob1", "", false, "",
                        "Registry", false, true)
        ));
        when(triggerRepository.getAllByHierarchyIdAndFunctionNameAndClientIdAndActiveTrue(eq(1L), any(), eq(getClient().getId()))).thenReturn(Collections.emptyList());
        when(triggerRepository.getAllByHierarchyIdAndFunctionNameAndClientIdAndActiveTrue(eq(2L), any(), eq(getClient().getId()))).thenReturn(trigger);

        EngagementDetails engagementDetails = new EngagementDetails
                (1L, 1L, "10:00:00", "", "10:00:00", true,
                        "", "");

        when(engagementService.getEngagementByHierarchy(any(), eq(true))).thenReturn(engagementDetails);
        Deployment deployment = new Deployment(
                1L, "IND", 123, "India Standard Time", "IST",
                "Asia/Kolkata", 1L, null, "", "India",
                "-", "https://help.url", "10:00 AM"
        );
        when(deploymentService.get(any())).thenReturn(deployment);
        when(vendorService.getVendorIdByDeploymentIdAndTriggerId(eq(1L), eq(1L))).thenReturn(1L);

        List<TriggerDetailsRequest> output = triggerService.getActiveTriggersByFunctionName(1L, "SampleJob1");

        assertEquals(2L, output.size());
    }

    @Test(expected = NotFoundException.class)
    public void updateTriggerTest_TriggerNotFound(){
        UpdateTriggerDetailsRequest updateTriggerDetailsRequest = new UpdateTriggerDetailsRequest(1L,1L,1L,"2,3","* * * * *","event","function",true,true,true,"sms");

        Long Id = 1L;
        when(triggerRepository.findById(Id)).thenReturn(Optional.empty());

        triggerService.updateTrigger(updateTriggerDetailsRequest);
    }

    @Test
    public void updateTriggerDetailsTestSuccess(){
        UpdateTriggerDetailsRequest updateTriggerDetailsRequest = new UpdateTriggerDetailsRequest(1L,1L,1L,"2,3","* * * * *","newEvent","function",true,true,true,"sms");
        Trigger trigger = new Trigger(1L,1L,1L,1L,1L,"2,3","eventName","functionName","* * * * *",true,"sms","registry",true,true);
        Long Id = 1L;
        when(triggerRepository.findById(Id)).thenReturn(Optional.of(trigger));

        TemplateResponse templateResponse = new TemplateResponse(1L, "test", "test", "test", "test");
        ResponseEntity<Response<List<TemplateResponse>>> response = new ResponseEntity<>(new Response<>(true, Arrays.asList(templateResponse)), HttpStatus.OK);

        String text = "Test";
        LocalDateTime dateTime = LocalDateTime.now();
        Client client = new Client(1L, text, text, dateTime);

        when(clientService.getClientByToken()).thenReturn(client);
        when(insService.getAllTemplates("1")).thenReturn(response);

        triggerService.updateTrigger(updateTriggerDetailsRequest);
    }

    @Test(expected = NotFoundException.class)
    public void updateTriggerDetailsTest_DefaultTemplateNotFound(){
        UpdateTriggerDetailsRequest updateTriggerDetailsRequest = new UpdateTriggerDetailsRequest(1L,1L,1L,"2,3","* * * * *","newEvent","function",true,true,true,"sms");
        Trigger trigger = new Trigger(1L,1L,1L,1L,1L,"2,3","eventName","functionName","* * * * *",true,"sms","registry",true,true);
        Long Id = 1L;
        when(triggerRepository.findById(Id)).thenReturn(Optional.of(trigger));

        TemplateResponse templateResponse = new TemplateResponse(2L,"content","parameter","type","language" );
        ResponseEntity<Response<List<TemplateResponse>>> response = new ResponseEntity<>(new Response<>(true, Arrays.asList(templateResponse)), HttpStatus.OK);

        String text = "Test";
        LocalDateTime dateTime = LocalDateTime.now();
        Client client = new Client(1L, text, text, dateTime);

        when(clientService.getClientByToken()).thenReturn(client);
        when(insService.getAllTemplates("1")).thenReturn(response);

        triggerService.updateTrigger(updateTriggerDetailsRequest);
    }

    @Test
    public void addTriggerTestSuccess(){
        AddTriggerDetails addTriggerDetails = new AddTriggerDetails(1L,1L, 1L, 1L, "registry", null,"2,3","* * * * *","event","function",true,true,true,"sms",true,"content",1L,1L,"p1,p2",false);
        Trigger trigger = new Trigger(1L,1L,1L,1L,1L,"2,3","eventName","functionName","* * * * *",true,"sms","registry",true,true);
        List<AddTriggerDetails> triggerDetails = new ArrayList<>();
        triggerDetails.add(addTriggerDetails);
        AddTriggerDetailsRequest addTriggerDetailsRequest = new AddTriggerDetailsRequest(triggerDetails);
        TemplateRequest templateRequest = new TemplateRequest("content", 1L, 1L, "p1,p2", false);
        HierarchyResponseData hierarchy = new HierarchyResponseData();
        hierarchy.setDeploymentId("1");

        ResponseEntity<Response<Long>> insResponse = new ResponseEntity<>(new Response<>(true, 1L), HttpStatus.OK);

        when(hierarchyService.fetchHierarchyById(any(), any())).thenReturn(hierarchy);
        when(insService.saveTemplate("1",templateRequest)).thenReturn(insResponse);
        when(triggerRepository.save(any())).thenReturn(trigger);
        AddTriggerResponse response = triggerService.addTrigger(addTriggerDetailsRequest);
        assertEquals(0, response.getFailedEntries().size());
    }

    @Test
    public void addTriggerTestFailureHierarchyNotPresent(){
        AddTriggerDetails addTriggerDetails = new AddTriggerDetails(1L,1L, 1L, 1L, "registry", null,"2,3","* * * * *","event","function",true,true,true,"sms",true,"content",1L,1L,"p1,p2",false);
        List<AddTriggerDetails> triggerDetails = new ArrayList<>();
        triggerDetails.add(addTriggerDetails);
        AddTriggerDetailsRequest addTriggerDetailsRequest = new AddTriggerDetailsRequest(triggerDetails);
        TemplateRequest templateRequest = new TemplateRequest("content", 1L, 1L, "p1,p2", false);

        ResponseEntity<Response<Long>> insResponse = new ResponseEntity<>(new Response<>(true, 1L), HttpStatus.OK);

        when(hierarchyService.fetchHierarchyById(any(), any())).thenThrow(new ValidationException(ValidationStatusEnum.HIERARCHY_NOT_FOUND));
        when(insService.saveTemplate("1",templateRequest)).thenReturn(insResponse);
        AddTriggerResponse response = triggerService.addTrigger(addTriggerDetailsRequest);
        assertEquals(1, response.getFailedEntries().size());
    }

    @Test
    public void addTriggerTestFailureDefaultTemplateIdPresent(){
        AddTriggerDetails addTriggerDetails = new AddTriggerDetails(1L,1L, 1L, 1L, "registry", 1L,"2,3","* * * * *","event","function",true,true,true,"sms",true,"content",1L,1L,"p1,p2",false);
        List<AddTriggerDetails> triggerDetails = new ArrayList<>();
        triggerDetails.add(addTriggerDetails);
        AddTriggerDetailsRequest addTriggerDetailsRequest = new AddTriggerDetailsRequest(triggerDetails);
        TemplateRequest templateRequest = new TemplateRequest("content", 1L, 1L, "p1,p2", false);
        HierarchyResponseData hierarchy = new HierarchyResponseData();

        ResponseEntity<Response<Long>> insResponse = new ResponseEntity<>(new Response<>(true, 1L), HttpStatus.OK);

        when(hierarchyService.fetchHierarchyById(any(), any())).thenReturn(hierarchy);
        when(insService.saveTemplate("1",templateRequest)).thenReturn(insResponse);
        AddTriggerResponse response = triggerService.addTrigger(addTriggerDetailsRequest);
        assertEquals(1, response.getFailedEntries().size());
    }

    @Test
    public void addTriggerTestFailureInvalidModule(){
        AddTriggerDetails addTriggerDetails = new AddTriggerDetails(1L,1L, 1L, 1L, "module", null,"2,3","* * * * *","event","function",true,true,true,"sms",true,"content",1L,1L,"p1,p2",false);
        List<AddTriggerDetails> triggerDetails = new ArrayList<>();
        triggerDetails.add(addTriggerDetails);
        AddTriggerDetailsRequest addTriggerDetailsRequest = new AddTriggerDetailsRequest(triggerDetails);
        TemplateRequest templateRequest = new TemplateRequest("content", 1L, 1L, "p1,p2", false);
        HierarchyResponseData hierarchy = new HierarchyResponseData();

        ResponseEntity<Response<Long>> insResponse = new ResponseEntity<>(new Response<>(true, 1L), HttpStatus.OK);

        when(hierarchyService.fetchHierarchyById(any(), any())).thenReturn(hierarchy);
        when(insService.saveTemplate("1",templateRequest)).thenReturn(insResponse);
        AddTriggerResponse response = triggerService.addTrigger(addTriggerDetailsRequest);
        assertEquals(1, response.getFailedEntries().size());
    }

    @Test
    public void addTriggerTestFailureDefaultTemplateIdNotProvided(){
        AddTriggerDetails addTriggerDetails = new AddTriggerDetails(1L,1L, 1L, 1L, "registry", null,"2,3","* * * * *","event","function",true,true,true,"sms",false,null,null,null,null,null);
        List<AddTriggerDetails> triggerDetails = new ArrayList<>();
        triggerDetails.add(addTriggerDetails);
        AddTriggerDetailsRequest addTriggerDetailsRequest = new AddTriggerDetailsRequest(triggerDetails);
        TemplateRequest templateRequest = new TemplateRequest("content", 1L, 1L, "p1,p2", false);
        HierarchyResponseData hierarchy = new HierarchyResponseData();

        ResponseEntity<Response<Long>> insResponse = new ResponseEntity<>(new Response<>(true, 1L), HttpStatus.OK);

        when(hierarchyService.fetchHierarchyById(any(), any())).thenReturn(hierarchy);
        when(insService.saveTemplate("1",templateRequest)).thenReturn(insResponse);
        AddTriggerResponse response = triggerService.addTrigger(addTriggerDetailsRequest);
        assertEquals(1, response.getFailedEntries().size());
    }

    @Test
    public void addTriggerTestFailureDefaultTemplateIdAbsent(){
        AddTriggerDetails addTriggerDetails = new AddTriggerDetails(1L,1L, 1L, 1L, "registry", 2L,"2,3","* * * * *","event","function",true,true,true,"sms",false,null,null,null,null,null);
        List<AddTriggerDetails> triggerDetails = new ArrayList<>();
        triggerDetails.add(addTriggerDetails);
        AddTriggerDetailsRequest addTriggerDetailsRequest = new AddTriggerDetailsRequest(triggerDetails);
        TemplateRequest templateRequest = new TemplateRequest("content", 1L, 1L, "p1,p2", false);
        HierarchyResponseData hierarchy = new HierarchyResponseData();
        TemplateResponse templateResponse = new TemplateResponse(1L,"content","parameter","type","language" );
        ResponseEntity<Response<List<TemplateResponse>>> insResponse = new ResponseEntity<>(new Response<>(true, Arrays.asList(templateResponse)), HttpStatus.OK);
        ResponseEntity<Response<Long>> insResponse1 = new ResponseEntity<>(new Response<>(true, 1L), HttpStatus.OK);

        when(hierarchyService.fetchHierarchyById(any(), any())).thenReturn(hierarchy);
        when(insService.getAllTemplates("1")).thenReturn(insResponse);
        when(insService.saveTemplate("1",templateRequest)).thenReturn(insResponse1);
        AddTriggerResponse response = triggerService.addTrigger(addTriggerDetailsRequest);
        assertEquals(1, response.getFailedEntries().size());
    }

    @Test(expected = NotFoundException.class)
    public void updateTriggerDetailsTest_ResponseBodySuccessFalse(){
        UpdateTriggerDetailsRequest updateTriggerDetailsRequest = new UpdateTriggerDetailsRequest(1L,1L,1L,"2,3","* * * * *","newEvent","function",true,true,true,"sms");
        Trigger trigger = new Trigger(1L,1L,1L,1L,1L,"2,3","eventName","functionName","* * * * *",true,"sms","registry",true,true);
        Long Id = 1L;
        when(triggerRepository.findById(Id)).thenReturn(Optional.of(trigger));
        ResponseEntity<Response<List<TemplateResponse>>> response = new ResponseEntity<>(new Response<>(false, null,"Templates does not exist!"), HttpStatus.BAD_REQUEST);
        String text = "Test";
        LocalDateTime dateTime = LocalDateTime.now();
        Client client = new Client(1L, text, text, dateTime);

        when(clientService.getClientByToken()).thenReturn(client);
        when(insService.getAllTemplates("1")).thenReturn(response);
        triggerService.updateTrigger(updateTriggerDetailsRequest);
    }

    @Test(expected = NotFoundException.class)
    public void updateTriggerDetailsTest_ResponseBodyNull(){
        UpdateTriggerDetailsRequest updateTriggerDetailsRequest = new UpdateTriggerDetailsRequest(1L,1L,1L,"2,3","* * * * *","newEvent","function",true,true,true,"sms");
        Trigger trigger = new Trigger(1L,1L,1L,1L,1L,"2,3","eventName","functionName","* * * * *",true,"sms","registry",true,true);
        Long Id = 1L;
        when(triggerRepository.findById(Id)).thenReturn(Optional.of(trigger));
        ResponseEntity<Response<List<TemplateResponse>>> response = new ResponseEntity<>(new Response<>(null), HttpStatus.OK);
        String text = "Test";
        LocalDateTime dateTime = LocalDateTime.now();
        Client client = new Client(1L, text, text, dateTime);

        when(clientService.getClientByToken()).thenReturn(client);
        when(insService.getAllTemplates("1")).thenReturn(response);
        triggerService.updateTrigger(updateTriggerDetailsRequest);
    }

    @Test
    public void addTriggerTestGetTemplateResponseBodySuccessFalse(){
        AddTriggerDetails addTriggerDetails = new AddTriggerDetails(1L,1L, 1L, 1L, "registry", 2L,"2,3","* * * * *","event","function",true,true,true,"sms",false,null,null,null,null,null);
        List<AddTriggerDetails> triggerDetails = new ArrayList<>();
        triggerDetails.add(addTriggerDetails);
        AddTriggerDetailsRequest addTriggerDetailsRequest = new AddTriggerDetailsRequest(triggerDetails);
        TemplateRequest templateRequest = new TemplateRequest("content", 1L, 1L, "p1,p2", false);
        HierarchyResponseData hierarchy = new HierarchyResponseData();
        TemplateResponse templateResponse = new TemplateResponse(1L,"content","parameter","type","language" );
        ResponseEntity<Response<List<TemplateResponse>>> insResponse = new ResponseEntity<>(new Response<>(false, null,"Templates does not exist!"), HttpStatus.BAD_REQUEST);
        ResponseEntity<Response<Long>> insResponse1 = new ResponseEntity<>(new Response<>(true, 1L), HttpStatus.OK);

        when(hierarchyService.fetchHierarchyById(any(), any())).thenReturn(hierarchy);
        when(insService.getAllTemplates("1")).thenReturn(insResponse);
        when(insService.saveTemplate("1",templateRequest)).thenReturn(insResponse1);
        AddTriggerResponse response = triggerService.addTrigger(addTriggerDetailsRequest);
        assertEquals(1, response.getFailedEntries().size());
    }

    @Test
    public void addTriggerTestGetTemplateResponseNull(){
        AddTriggerDetails addTriggerDetails = new AddTriggerDetails(1L,1L, 1L, 1L, "registry", 2L,"2,3","* * * * *","event","function",true,true,true,"sms",false,null,null,null,null,null);
        List<AddTriggerDetails> triggerDetails = new ArrayList<>();
        triggerDetails.add(addTriggerDetails);
        AddTriggerDetailsRequest addTriggerDetailsRequest = new AddTriggerDetailsRequest(triggerDetails);
        TemplateRequest templateRequest = new TemplateRequest("content", 1L, 1L, "p1,p2", false);
        HierarchyResponseData hierarchy = new HierarchyResponseData();
        TemplateResponse templateResponse = new TemplateResponse(1L,"content","parameter","type","language" );
        ResponseEntity<Response<List<TemplateResponse>>> insResponse = new ResponseEntity<>(new Response<>(true, null), HttpStatus.OK);
        ResponseEntity<Response<Long>> insResponse1 = new ResponseEntity<>(new Response<>(true, 1L), HttpStatus.OK);

        when(hierarchyService.fetchHierarchyById(any(), any())).thenReturn(hierarchy);
        when(insService.getAllTemplates("1")).thenReturn(insResponse);
        when(insService.saveTemplate("1",templateRequest)).thenReturn(insResponse1);
        AddTriggerResponse response = triggerService.addTrigger(addTriggerDetailsRequest);
        assertEquals(1, response.getFailedEntries().size());
    }

    @Test
    public void addTriggerTestSaveTemplateResponseFalse(){
        AddTriggerDetails addTriggerDetails = new AddTriggerDetails(1L,1L, 1L, 1L, "registry", null,"2,3","* * * * *","event","function",true,true,true,"sms",true,"content",1L,1L,"p1,p2",false);
        List<AddTriggerDetails> triggerDetails = new ArrayList<>();
        triggerDetails.add(addTriggerDetails);
        AddTriggerDetailsRequest addTriggerDetailsRequest = new AddTriggerDetailsRequest(triggerDetails);
        TemplateRequest templateRequest = new TemplateRequest("content", 1L, 1L, "p1,p2", false);
        HierarchyResponseData hierarchy = new HierarchyResponseData();

        ResponseEntity<Response<Long>> insResponse = new ResponseEntity<>(new Response<>(false, null, "Unable to save template"), HttpStatus.BAD_REQUEST);

        when(hierarchyService.fetchHierarchyById(any(), any())).thenReturn(hierarchy);
        when(insService.saveTemplate("1",templateRequest)).thenReturn(insResponse);
        AddTriggerResponse response = triggerService.addTrigger(addTriggerDetailsRequest);
        assertEquals(1, response.getFailedEntries().size());
    }

    @Test
    public void addTriggerTestSaveTemplateResponseNull(){
        AddTriggerDetails addTriggerDetails = new AddTriggerDetails(1L,1L, 1L, 1L, "registry", null,"2,3","* * * * *","event","function",true,true,true,"sms",true,"content",1L,1L,"p1,p2",false);
        List<AddTriggerDetails> triggerDetails = new ArrayList<>();
        triggerDetails.add(addTriggerDetails);
        AddTriggerDetailsRequest addTriggerDetailsRequest = new AddTriggerDetailsRequest(triggerDetails);
        TemplateRequest templateRequest = new TemplateRequest("content", 1L, 1L, "p1,p2", false);
        HierarchyResponseData hierarchy = new HierarchyResponseData();

        ResponseEntity<Response<Long>> insResponse = new ResponseEntity<>(new Response<>(false, null), HttpStatus.OK);

        when(hierarchyService.fetchHierarchyById(any(), any())).thenReturn(hierarchy);
        when(insService.saveTemplate("1",templateRequest)).thenReturn(insResponse);
        AddTriggerResponse response = triggerService.addTrigger(addTriggerDetailsRequest);
        assertEquals(1, response.getFailedEntries().size());
    }
}

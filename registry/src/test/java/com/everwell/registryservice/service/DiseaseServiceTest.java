package com.everwell.registryservice.service;

import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.constants.Constants;
import com.everwell.registryservice.exceptions.CustomExceptionHandler;
import com.everwell.registryservice.models.db.DeploymentDiseaseMap;
import com.everwell.registryservice.models.dto.DiseaseTemplate;
import com.everwell.registryservice.models.http.requests.episode.DiseaseTemplateRequest;
import com.everwell.registryservice.repository.*;
import com.everwell.registryservice.service.impl.DiseaseServiceImpl;
import com.everwell.registryservice.utils.RedisUtils;
import com.everwell.registryservice.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

public class DiseaseServiceTest extends BaseTest
{
    @Spy
    @InjectMocks
    private DiseaseServiceImpl diseaseService;

    @Mock
    private EpisodeService episodeService;

    @Mock
    private DeploymentDiseaseRepository deploymentDiseaseRepository;

    @Mock
    private ValueOperations<String, Object> valueOperations;

    @Mock
    RedisTemplate<String, Object> redisTemplate;

    RedisUtils redisUtils;

    @Before
    public void setup() {
        redisUtils = new RedisUtils(redisTemplate);
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
    }

    @Test
    public void testDiseaseByDeployment_withValidMappings()
    {
        DiseaseTemplateRequest episodeServiceResponse = new DiseaseTemplateRequest(
                Arrays.asList(
                    new DiseaseTemplate(1L, "TB1"),
                    new DiseaseTemplate(2L, "TB2")
                )
        );
        List <DeploymentDiseaseMap> deploymentDiseaseRepoResponse = Arrays.asList(
            new DeploymentDiseaseMap(1L, 1L, 1L, null, null),
            new DeploymentDiseaseMap(2L, 1L, 2L, null, null)
        );
        when(deploymentDiseaseRepository.getAllByDeploymentIdAndStoppedAtIsNull(1L)).thenReturn(deploymentDiseaseRepoResponse);
        when(episodeService.getAllDiseases()).thenReturn(episodeServiceResponse);
        when(redisUtils.hasKey(any())).thenReturn(true);


        List<DiseaseTemplate> response = diseaseService.getDiseaseByDeployment(1L, clientId_Nikshay);
        assertEquals(response.size(), 2);
    }
    @Test
    public void testDiseaseByDeployment_withValidAndInvalidMappings()
    {
        DiseaseTemplateRequest episodeServiceResponse = new DiseaseTemplateRequest(
                Arrays.asList(
                        new DiseaseTemplate(1L, "TB1"),
                        new DiseaseTemplate(2L, "TB2")
                        )
        );
        List <DeploymentDiseaseMap> deploymentDiseaseRepoResponse = Arrays.asList(
                new DeploymentDiseaseMap(1L, 1L, 1L, null, null),
                new DeploymentDiseaseMap(2L, 1L, 4L, null, null),
                new DeploymentDiseaseMap(2L, 1L, 3L, null, null)
        );
        when(deploymentDiseaseRepository.getAllByDeploymentIdAndStoppedAtIsNull(1L)).thenReturn(deploymentDiseaseRepoResponse);
        when(episodeService.getAllDiseases()).thenReturn(episodeServiceResponse);
        when(redisUtils.hasKey(any())).thenReturn(true);

        List<DiseaseTemplate> response = diseaseService.getDiseaseByDeployment(1L, clientId_Nikshay);
        assertEquals(response.size(), 1);
    }

    @Test
    public void testDiseaseIds_success()
    {
        DiseaseTemplateRequest episodeServiceResponse = new DiseaseTemplateRequest(
                Arrays.asList(
                        new DiseaseTemplate(1L, "TB1"),
                        new DiseaseTemplate(2L, "TB2")
                )
        );
        when(episodeService.getAllDiseases()).thenReturn(episodeServiceResponse);
        when(redisUtils.hasKey(any())).thenReturn(false);

        boolean output = diseaseService.verifyDiseaseIds(Arrays.asList(1L, 2L), clientId_Nikshay);
        assertEquals(output, true);
    }

    @Test
    public void testDiseaseIds_failure()
    {
        DiseaseTemplateRequest episodeServiceResponse = new DiseaseTemplateRequest(
                Arrays.asList(
                        new DiseaseTemplate(1L, "TB1"),
                        new DiseaseTemplate(2L, "TB2")
                )
        );
        when(episodeService.getAllDiseases()).thenReturn(episodeServiceResponse);
        when(redisUtils.hasKey(any())).thenReturn(false);

        boolean output = diseaseService.verifyDiseaseIds(Arrays.asList(3L, 2L), clientId_Nikshay);
        assertFalse(output);;
    }

    @Test(expected = NullPointerException.class)
    public void getDiseaseById_redisFailure()
    {
        String cacheKey = Constants.CLIENT_ID_CACHE_CONFIG + clientId_Nikshay + Constants.DISEASE_ID + 1L;

        when(episodeService.getAllDiseases()).thenReturn(null);
        when(redisUtils.hasKey(cacheKey)).thenReturn(false);

        when(valueOperations.get(cacheKey)).thenThrow(new NullPointerException());

        DiseaseTemplate output = diseaseService.getDiseaseById(2L, clientId_Nikshay);
        assertEquals(Optional.ofNullable(output.getId()), Optional.ofNullable(2L));
    }
    @Test
    public void getDiseaseById_redisSuccess()
    {
        DiseaseTemplate diseaseTemplate = new DiseaseTemplate(1L, "TB1");
        String cacheKey = Constants.DISEASE_ID + 1L + Constants.CLIENT_ID_CACHE_CONFIG + clientId_Nikshay;

        when(episodeService.getAllDiseases()).thenReturn(null);
        when(redisUtils.hasKey(cacheKey)).thenReturn(true);

        when(valueOperations.get(cacheKey)).thenReturn(Utils.asJsonString(diseaseTemplate));

        DiseaseTemplate output = diseaseService.getDiseaseById(diseaseTemplate.getId(), clientId_Nikshay);
        assertEquals(Optional.ofNullable(output.getId()), Optional.ofNullable(diseaseTemplate.getId()));
    }

}

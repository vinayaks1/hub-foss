package com.everwell.registryservice.service;

import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.exceptions.NotFoundException;
import com.everwell.registryservice.exceptions.ValidationException;
import com.everwell.registryservice.models.db.*;
import com.everwell.registryservice.models.dto.FilterDetails;
import com.everwell.registryservice.models.dto.TaskListItem;
import com.everwell.registryservice.models.http.requests.TaskListItemRequest;
import com.everwell.registryservice.models.http.response.TaskListItemResponse;
import com.everwell.registryservice.repository.*;
import com.everwell.registryservice.service.impl.TaskListServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;

import java.util.*;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class TaskListServiceTest extends BaseTest {
    @Spy
    @InjectMocks
    private TaskListServiceImpl taskListService;

    @Mock
    private DeploymentTaskListRepository deploymentTaskListRepository;

    @Mock
    private TaskListFilterMapRepository taskListFilterMapRepository;

    @Mock
    private TaskListColumnRepository taskListColumnRepository;

    @Mock
    private TaskListRepository taskListRepository;

    @Mock
    private FilterRepository filterRepository;

    @Mock
    private FilterValueRepository filterValueRepository;

    private static String name = "_end_date_passed";
    private static String webIcon = "fa fa-clock-";
    private static String appIcon = "{md-alarm}";
    private static String description = "tasklist_desc_end_date_passed";
    private static String displayColumns = "1,2,4";
    private static String extraData = "{extra_data}";
    private static Long displayOrder = 1L;
    private Long id = 1L;
    private Long deploymentId = 1L;
    private String labelName = "PatientType";
    private String label = "PatientType";
    private String filterName = "EndDatePassed";
    private String value = "true";
    private static String deploymentCode = "IND";

    @Test
    public void testGetTaskList() {
        DeploymentTaskList taskList = new DeploymentTaskList(id, deploymentId, 1L, displayColumns, displayOrder);
        when(deploymentTaskListRepository.findById(any())).thenReturn(Optional.of(taskList));
        taskListService.getTaskList(id);
        verify(deploymentTaskListRepository, Mockito.times(1)).findById(any());
    }

    @Test
    public void testGetTaskListWithTaskListIdAndDeploymentId() {
        List<DeploymentTaskList> deploymentTaskLists = new ArrayList<>();
        DeploymentTaskList taskList = new DeploymentTaskList(id, deploymentId, 1L, displayColumns, displayOrder);
        deploymentTaskLists.add(taskList);
        when(deploymentTaskListRepository.getDeploymentTaskListByIdAndDeploymentId(any(), any())).thenReturn(deploymentTaskLists);
        taskListService.getTaskList(id, 1L);
        verify(deploymentTaskListRepository, Mockito.times(1)).getDeploymentTaskListByIdAndDeploymentId(any(), any());
    }

    @Test
    public void testCheckDisplayColumns() {
        List<Long> displayColumnIds = new ArrayList<>();
        displayColumnIds.add(1L);
        displayColumnIds.add(2L);
        when(taskListColumnRepository.getAllTaskListColumnIds()).thenReturn(displayColumnIds);
        boolean validateDisplayColumns = taskListService.checkDisplayColumns("1,2");
        assertEquals(true, validateDisplayColumns);
    }

    @Test
    public void testCheckDisplayColumnsInvalid() {
        List<Long> displayColumnIds = new ArrayList<>();
        displayColumnIds.add(1L);
        displayColumnIds.add(2L);
        when(taskListColumnRepository.getAllTaskListColumnIds()).thenReturn(displayColumnIds);
        boolean validateDisplayColumns = taskListService.checkDisplayColumns("1,3");
        assertEquals(false, validateDisplayColumns);
    }

    @Test
    public void testValidateTaskList() {
        TaskListItem taskListItem = new TaskListItem(deploymentCode, 1L, displayColumns, displayOrder);
        List<Long> filterValueIds = Collections.singletonList(1L);
        TaskListItemRequest taskListItemRequest = new TaskListItemRequest(taskListItem, filterValueIds);

        when(deploymentTaskListRepository.getDeploymentTaskListByIdAndDeploymentId(1L, deploymentId)).thenReturn(new ArrayList<>());
        when(taskListRepository.findById(1L)).thenReturn(Optional.of(new TaskList(1L, name, webIcon, appIcon, description, extraData)));
        when(taskListColumnRepository.getAllTaskListColumnIds()).thenReturn(Arrays.asList(1L, 2L, 4L));
        when(filterValueRepository.findAllById(filterValueIds)).thenReturn(Collections.singletonList(new FilterValue(1L, 1L, "Value1")));

        boolean validateTaskList = taskListService.validateTaskList(taskListItemRequest, deploymentId);
        assertTrue(validateTaskList);
    }

    @Test
    public void testValidateTaskListInvalidFilters() {
        TaskListItem taskListItem = new TaskListItem(deploymentCode, 1L, displayColumns, displayOrder);
        List<Long> filterValueIds = Collections.singletonList(1L);
        TaskListItemRequest taskListItemRequest = new TaskListItemRequest(taskListItem, filterValueIds);

        when(deploymentTaskListRepository.getDeploymentTaskListByIdAndDeploymentId(1L, deploymentId)).thenReturn(new ArrayList<>());
        when(taskListRepository.findById(1L)).thenReturn(Optional.of(new TaskList(1L, name, webIcon, appIcon, description, extraData)));
        when(taskListColumnRepository.getAllTaskListColumnIds()).thenReturn(Arrays.asList(1L, 2L, 4L));
        when(filterValueRepository.findAllById(filterValueIds)).thenReturn(new ArrayList<>());

        boolean validateTaskList = taskListService.validateTaskList(taskListItemRequest, deploymentId);
        assertFalse(validateTaskList);
    }

    @Test
    public void testValidateTaskListInvalidTaskListItem() {
        TaskListItem taskListItem = new TaskListItem(deploymentCode, 1L, displayColumns, displayOrder);
        TaskListItemRequest taskListItemRequest = new TaskListItemRequest(taskListItem, Collections.singletonList(1L));

        when(deploymentTaskListRepository.getDeploymentTaskListByIdAndDeploymentId(1L, deploymentId)).thenReturn(new ArrayList<>());
        when(taskListRepository.findById(1L)).thenReturn(Optional.empty());

        boolean validateTaskList = taskListService.validateTaskList(taskListItemRequest, deploymentId);
        assertFalse(validateTaskList);
    }

    @Test
    public void testTaskListAlreadyMappedToDeployment() {
        Long taskListId = 1L;
        when(deploymentTaskListRepository.getDeploymentTaskListByIdAndDeploymentId(taskListId, deploymentId)).thenReturn(Collections.singletonList(new DeploymentTaskList(1L, deploymentId, 1L, displayColumns, displayOrder)));

        boolean validateTaskList = taskListService.taskListAlreadyMappedToDeployment(taskListId, deploymentId);
        assertTrue(validateTaskList);
    }

    @Test
    public void testTaskListNotMappedToDeployment() {
        Long taskListId = 1L;
        when(deploymentTaskListRepository.getDeploymentTaskListByIdAndDeploymentId(taskListId, deploymentId)).thenReturn(new ArrayList<>());

        boolean validateTaskList = taskListService.taskListAlreadyMappedToDeployment(taskListId, deploymentId);
        assertFalse(validateTaskList);
    }

    @Test
    public void testValidateTaskListInvalidDisplayColumns() {
        TaskListItem taskListItem = new TaskListItem(deploymentCode, 1L, "2", displayOrder);
        TaskListItemRequest taskListItemRequest = new TaskListItemRequest(taskListItem, Collections.singletonList(1L));

        when(deploymentTaskListRepository.getDeploymentTaskListByIdAndDeploymentId(1L, deploymentId)).thenReturn(new ArrayList<>());
        when(taskListRepository.findById(1L)).thenReturn(Optional.of(new TaskList(1L, name, webIcon, appIcon, description, extraData)));
        when(taskListColumnRepository.getAllTaskListColumnIds()).thenReturn(Collections.singletonList(1L));

        boolean validateTaskList = taskListService.validateTaskList(taskListItemRequest, deploymentId);
        assertFalse(validateTaskList);
    }

    @Test(expected = NotFoundException.class)
    public void testGetTaskListNotFound() {
        when(deploymentTaskListRepository.findById(any())).thenReturn(Optional.empty());
        taskListService.getTaskList(id);
    }

    @Test
    public void testGetDisplayColumns() {
        List<Long> displayColumnIds = new ArrayList<>();
        displayColumnIds.add(1L);
        displayColumnIds.add(2L);

        TaskListColumn column = new TaskListColumn(id, labelName, label, true);
        when(taskListColumnRepository.findAllById(Arrays.asList(1L, 2L))).thenReturn(Collections.singletonList(column));

        List<TaskListColumn> taskListColumns = taskListService.getDisplayColumns(displayColumnIds);
        verify(taskListColumnRepository, Mockito.times(1)).findAllById(any());
        assertEquals(1, taskListColumns.size());
    }

    @Test
    public void testGetDisplayColumnsWithoutIds() {
        List<Long> displayColumnIds = new ArrayList<>();
        List<TaskListColumn> taskListColumns = taskListService.getDisplayColumns(displayColumnIds);
        verify(taskListColumnRepository, Mockito.times(0)).findById(any());
        assertEquals(0, taskListColumns.size());
    }

    @Test
    public void testAddTaskListFilters() {
        List<Long> filterDetailIds = Arrays.asList(1L, 2L);
        when(taskListFilterMapRepository.save(any())).thenReturn(null);
        taskListService.addTaskListFilters(filterDetailIds, id);
        verify(taskListFilterMapRepository, Mockito.times(1)).saveAll(any());
    }

    @Test
    public void testAddTaskListFiltersWithoutFilterValueIds() {
        when(taskListFilterMapRepository.save(any())).thenReturn(null);
        taskListService.addTaskListFilters(null, id);
        verify(taskListFilterMapRepository, Mockito.times(0)).saveAll(any());
    }

    @Test
    public void testAddTaskListFiltersWithoutTaskListId() {
        when(taskListFilterMapRepository.save(any())).thenReturn(null);
        taskListService.addTaskListFilters(Collections.singletonList(1L), null);
        verify(taskListFilterMapRepository, Mockito.times(0)).saveAll(any());
    }

    @Test
    public void testAddTaskListFiltersWithoutFilterValueIdsAndTaskListId() {
        when(taskListFilterMapRepository.save(any())).thenReturn(null);
        taskListService.addTaskListFilters(new ArrayList<>(), null);
        verify(taskListFilterMapRepository, Mockito.times(0)).saveAll(any());
    }

    @Test
    public void testAddTaskList() {
        TaskListItem taskListItem = new TaskListItem(deploymentCode, 1L, displayColumns, displayOrder);
        TaskListItemRequest taskListItemRequest = new TaskListItemRequest(taskListItem, Collections.singletonList(1L));
        DeploymentTaskList taskList = new DeploymentTaskList(id, deploymentId, 1L, displayColumns, displayOrder);

        when(deploymentTaskListRepository.save(any())).thenReturn(taskList);
        doNothing().when(taskListService).addTaskListFilters(any(), any());

        Long taskListId = taskListService.addTaskList(taskListItemRequest, id);
        verify(deploymentTaskListRepository, Mockito.times(1)).save(any());
        assertEquals(id, taskListId);
    }

    @Test(expected = ValidationException.class)
    public void testUpdateTaskList_EmptyRequest() {
        taskListService.updateTaskList(new TaskListItemRequest(), 1L);
    }

    @Test(expected = NotFoundException.class)
    public void testUpdateTaskList_TaskListNotFound() {
        TaskListItem taskListItem = new TaskListItem(deploymentCode, 1L, displayColumns, displayOrder);
        TaskListItemRequest taskListItemRequest = new TaskListItemRequest(taskListItem, Collections.singletonList(1L));

        Long taskListId = 1L;
        when(deploymentTaskListRepository.findById(taskListId)).thenReturn(Optional.empty());

        taskListService.updateTaskList(taskListItemRequest, taskListId);
    }

    @Test
    public void testUpdateTaskList_TaskListItemSuccess() {
        TaskListItem taskListItem = new TaskListItem();
        taskListItem.setDisplayOrder(2L);

        TaskListItemRequest taskListItemRequest = new TaskListItemRequest(taskListItem, new ArrayList<>());

        Long taskListId = 1L;
        DeploymentTaskList taskList = new DeploymentTaskList(id, deploymentId, taskListId, displayColumns, displayOrder);

        when(deploymentTaskListRepository.findById(taskListId)).thenReturn(Optional.of(taskList));
        when(deploymentTaskListRepository.save(any())).thenReturn(null);

        taskListService.updateTaskList(taskListItemRequest, taskListId);
    }

    @Test(expected = NotFoundException.class)
    public void testUpdateTaskList_DisplayColumnsInvalid() {
        TaskListItem taskListItem = new TaskListItem();
        taskListItem.setDisplayColumns("1,3");

        when(taskListColumnRepository.findAllById(Arrays.asList(1L, 3L))).thenReturn(new ArrayList<>());

        TaskListItemRequest taskListItemRequest = new TaskListItemRequest(taskListItem, new ArrayList<>());

        DeploymentTaskList taskList = new DeploymentTaskList(id, deploymentId, 1L, displayColumns, displayOrder);

        Long taskListId = 1L;
        when(deploymentTaskListRepository.findById(taskListId)).thenReturn(Optional.of(taskList));

        taskListService.updateTaskList(taskListItemRequest, taskListId);
    }

    @Test
    public void testUpdateTaskList_DisplayColumns() {
        TaskListItem taskListItem = new TaskListItem();
        taskListItem.setDisplayColumns("1,3");

        TaskListColumn column1 = new TaskListColumn(id, labelName, label, true);
        TaskListColumn column3 = new TaskListColumn(3L, "labelName", "label", true);
        when(taskListColumnRepository.findAllById(Arrays.asList(1L, 3L))).thenReturn(Arrays.asList(column1, column3));

        TaskListItemRequest taskListItemRequest = new TaskListItemRequest(taskListItem, null);

        DeploymentTaskList taskList = new DeploymentTaskList(id, deploymentId, 1L, displayColumns, displayOrder);

        Long taskListId = 1L;
        when(deploymentTaskListRepository.findById(taskListId)).thenReturn(Optional.of(taskList));
        when(deploymentTaskListRepository.save(any())).thenReturn(null);

        taskListService.updateTaskList(taskListItemRequest, taskListId);
    }

    @Test
    public void testUpdateTaskList_UnchangedTaskList() {
        TaskListItem taskListItem = new TaskListItem(deploymentCode, 1L, displayColumns, displayOrder);
        TaskListItemRequest taskListItemRequest = new TaskListItemRequest(taskListItem, new ArrayList<>());

        DeploymentTaskList taskList = new DeploymentTaskList(id, deploymentId, 1L, displayColumns, displayOrder);

        Long taskListId = 1L;
        when(deploymentTaskListRepository.findById(taskListId)).thenReturn(Optional.of(taskList));

        taskListService.updateTaskList(taskListItemRequest, taskListId);
    }

    @Test(expected = ValidationException.class)
    public void testUpdateTaskList_InvalidFilters() {
        TaskListItem taskListItem = new TaskListItem(deploymentCode, 1L, displayColumns, displayOrder);
        List<Long> filterValueIds = Collections.singletonList(1L);
        TaskListItemRequest taskListItemRequest = new TaskListItemRequest(taskListItem, filterValueIds);

        DeploymentTaskList taskList = new DeploymentTaskList(id, deploymentId, 1L, displayColumns, displayOrder);

        Long taskListId = 1L;
        when(deploymentTaskListRepository.findById(taskListId)).thenReturn(Optional.of(taskList));

        when(filterValueRepository.findAllById(filterValueIds)).thenReturn(new ArrayList<>());

        taskListService.updateTaskList(taskListItemRequest, taskListId);
    }

    @Test
    public void testUpdateTaskList_FilterDetails() {
        List<Long> filterValueIds = Arrays.asList(1L, 3L);
        TaskListItemRequest taskListItemRequest = new TaskListItemRequest(null, filterValueIds);

        DeploymentTaskList taskList = new DeploymentTaskList(id, deploymentId, 1L, displayColumns, displayOrder);

        Long taskListId = 1L;
        when(deploymentTaskListRepository.findById(taskListId)).thenReturn(Optional.of(taskList));

        List<TaskListFilterMap> filterMaps = new ArrayList<>() {
            {
                add(new TaskListFilterMap(1L, taskListId, 1L));
                add(new TaskListFilterMap(1L, taskListId, 2L));
            }
        };

        when(taskListFilterMapRepository.getAllByDeploymentTaskListId(taskListId)).thenReturn(filterMaps);

        doNothing().when(taskListFilterMapRepository).deleteAll(any());
        when(taskListFilterMapRepository.saveAll(any())).thenReturn(new ArrayList<>());
        when(filterValueRepository.findAllById(filterValueIds)).thenReturn(Arrays.asList(
                new FilterValue(1L, 1L, "Value1"),
                new FilterValue(3L, 1L, "Value3")
        ));

        taskListService.updateTaskList(taskListItemRequest, taskListId);
    }

    @Test
    public void testUpdateTaskList_FilterDetailsUnchanged() {
        List<Long> filterValueIds = Arrays.asList(1L, 2L);
        TaskListItemRequest taskListItemRequest = new TaskListItemRequest(null, filterValueIds);

        DeploymentTaskList taskList = new DeploymentTaskList(id, deploymentId, 1L, displayColumns, displayOrder);

        Long taskListId = 1L;
        when(deploymentTaskListRepository.findById(taskListId)).thenReturn(Optional.of(taskList));

        List<TaskListFilterMap> filterMaps = new ArrayList<>() {
            {
                add(new TaskListFilterMap(1L, taskListId, 1L));
                add(new TaskListFilterMap(1L, taskListId, 2L));
            }
        };

        when(taskListFilterMapRepository.getAllByDeploymentTaskListId(taskListId)).thenReturn(filterMaps);

        doNothing().when(taskListFilterMapRepository).deleteAll(any());
        when(taskListFilterMapRepository.saveAll(any())).thenReturn(new ArrayList<>());
        when(filterValueRepository.findAllById(filterValueIds)).thenReturn(Arrays.asList(
                new FilterValue(1L, 1L, "Value1"),
                new FilterValue(2L, 1L, "Value2")
        ));

        taskListService.updateTaskList(taskListItemRequest, taskListId);
    }

    @Test
    public void testGetTaskListByDeployment() {
        List<DeploymentTaskList> taskLists = Collections.singletonList(
                new DeploymentTaskList(
                        1L,
                        deploymentId,
                        1L,
                        displayColumns,
                        displayOrder
                )
        );

        when(deploymentTaskListRepository.getAllByDeploymentIdOrderByDisplayOrderAscIdAsc(deploymentId)).thenReturn(taskLists);

        List<DeploymentTaskList> taskListsResponse = taskListService.getTaskListByDeployment(deploymentId);
        assertEquals(taskLists.size(), taskListsResponse.size());
        assertEquals(taskLists.get(0).getId(), taskListsResponse.get(0).getId());
    }

    @Test
    public void testDeleteTaskList() {
        DeploymentTaskList taskList = new DeploymentTaskList(id, deploymentId, 1L, displayColumns, displayOrder);

        when(deploymentTaskListRepository.findById(any())).thenReturn(Optional.of(taskList));
        doNothing().when(deploymentTaskListRepository).delete(any());

        taskListService.deleteTaskList(id);
        verify(deploymentTaskListRepository, Mockito.times(1)).findById(any());
        verify(deploymentTaskListRepository, Mockito.times(1)).delete(any());
    }

    @Test(expected = NotFoundException.class)
    public void testDeleteTaskListNotFound() {
        when(deploymentTaskListRepository.findById(any())).thenReturn(Optional.empty());
        taskListService.deleteTaskList(id);
    }

    @Test
    public void testGetTaskListFiltersByTaskListIds() {
        List<Long> taskListIds = Collections.singletonList(1L);

        List<TaskListFilterMap> filterMaps = Arrays.asList(
                new TaskListFilterMap(1L, deploymentId, 1L),
                new TaskListFilterMap(2L, deploymentId, 2L)
        );

        when(taskListFilterMapRepository.getAllByDeploymentTaskListIdIn(taskListIds)).thenReturn(filterMaps);

        List<TaskListFilterMap> filtersResponse = taskListService.getTaskListFiltersByTaskListIds(taskListIds);
        assertEquals(filterMaps.size(), filtersResponse.size());
        assertEquals(filterMaps.get(0).getId(), filtersResponse.get(0).getId());
        assertEquals(filterMaps.get(1).getId(), filtersResponse.get(1).getId());
    }

    @Test
    public void testGetFilterDetails() {
        Long filterValueId = 1L;
        Long filterId = 1L;
        String filterName = "Name1";
        String filterValue = "Value1";
        when(filterRepository.findAll()).thenReturn(Collections.singletonList(new Filter(filterId, filterName)));
        when(filterValueRepository.findAll()).thenReturn(Collections.singletonList(new FilterValue(filterValueId, filterId, filterValue)));

        List<Long> filterValueIds = Collections.singletonList(filterValueId);
        List<FilterDetails> details = taskListService.getFilterDetails(filterValueIds);
        assertEquals(filterValueIds.size(), details.size());
        assertEquals(filterValueId, details.get(0).getFilterValueId());
        assertEquals(filterName, details.get(0).getFilterName());
        assertEquals(filterValue, details.get(0).getValue());
    }

    @Test(expected = NotFoundException.class)
    public void testGetFilterDetailsFilterValueNotFound() {
        Long filterValueId = 1L;
        Long filterId = 1L;
        String filterName = "Name1";
        when(filterRepository.findAll()).thenReturn(Collections.singletonList(new Filter(filterId, filterName)));
        when(filterValueRepository.findAll()).thenReturn(new ArrayList<>());

        List<Long> filterValueIds = Collections.singletonList(filterValueId);
        taskListService.getFilterDetails(filterValueIds);
    }

    @Test(expected = NotFoundException.class)
    public void testGetFilterDetailsFilterNotFound() {
        Long filterValueId = 1L;
        Long filterId = 1L;
        String filterValue = "Value1";
        when(filterRepository.findAll()).thenReturn(new ArrayList<>());
        when(filterValueRepository.findAll()).thenReturn(Collections.singletonList(new FilterValue(filterValueId, filterId, filterValue)));

        List<Long> filterValueIds = Collections.singletonList(filterValueId);
        taskListService.getFilterDetails(filterValueIds);
    }

    @Test
    public void testGetAllTaskLists() {
        List<TaskList> list = Collections.singletonList(new TaskList(1L, name, webIcon, appIcon, description, extraData));

        when(taskListRepository.findAll()).thenReturn(list);

        List<TaskList> allTaskLists = taskListService.getAllTaskLists();
        assertEquals(list.size(), allTaskLists.size());
        assertEquals(list.get(0).getName(), allTaskLists.get(0).getName());
    }

    @Test
    public void testCheckTaskList() {
        TaskList taskList = new TaskList(1L, name, webIcon, appIcon, description, extraData);
        when(taskListRepository.findById(1L)).thenReturn(Optional.of(taskList));

        assertTrue(taskListService.checkTaskList(1L));
        assertFalse(taskListService.checkTaskList(2L));
    }

    @Test
    public void testCheckFilters() {
        Long filterValueId1 = 1L;
        Long filterValueId2 = 2L;

        List<FilterValue> filterValues = Arrays.asList(
                new FilterValue(filterValueId1, 1L, "Value1"),
                new FilterValue(filterValueId2, 1L, "Value2")
        );

        List<Long> filterValueIds = Arrays.asList(filterValueId1, filterValueId2);

        when(filterValueRepository.findAllById(filterValueIds)).thenReturn(filterValues);
        assertTrue(taskListService.checkFilters(filterValueIds));
    }

    @Test
    public void testCheckFiltersNullFilterIds() {
        assertTrue(taskListService.checkFilters(null));
    }

    @Test
    public void testCheckFiltersNotFound() {
        Long filterValueId1 = 1L;
        Long filterValueId2 = 2L;

        List<FilterValue> filterValues = Arrays.asList(
                new FilterValue(filterValueId1, 1L, "Value1"),
                new FilterValue(filterValueId2, 1L, "Value2")
        );

        List<Long> filterValueIds = Arrays.asList(filterValueId1, filterValueId2);

        when(filterValueRepository.findAllById(filterValueIds)).thenReturn(filterValues);
        assertFalse(taskListService.checkFilters(Arrays.asList(1L, 3L)));
    }

    @Test
    public void testGetTaskListItem() {
        Long taskListId = 1L;
        TaskList taskList = new TaskList(taskListId, name, webIcon, appIcon, description, extraData);

        when(taskListRepository.findById(taskListId)).thenReturn(Optional.of(taskList));

        TaskList response = taskListService.getTaskListItem(taskListId);
        assertNotNull(response);
        assertEquals(taskList.getName(), response.getName());
    }

    @Test
    public void testGetAllDisplayColumnDetails() {
        TaskListColumn column = new TaskListColumn(id, labelName, label, true);
        when(taskListColumnRepository.findAll()).thenReturn(Collections.singletonList(column));
        List<TaskListColumn> taskListColumns = taskListService.getAllDisplayColumns();
        verify(taskListColumnRepository, Mockito.times(1)).findAll();
        assertEquals(1, taskListColumns.size());
    }

    @Test(expected = NotFoundException.class)
    public void testGetAllDisplayColumnDetailsEmpty() {
        when(taskListColumnRepository.findAll()).thenReturn(null);
        taskListService.getAllDisplayColumns();
    }

    @Test
    public void testGetTaskListFilterResponseWithNoDisplayColumns() {
        List<DeploymentTaskList> taskLists = Collections.singletonList(
                new DeploymentTaskList(
                        1L,
                        deploymentId,
                        1L,
                        null,
                        displayOrder
                )
        );

        Long filterValueId = 1L;
        Long filterId = 1L;
        String filterName = "Name1";
        String filterValue = "Value1";
        when(filterRepository.findAll()).thenReturn(Collections.singletonList(new Filter(filterId, filterName)));
        when(filterValueRepository.findAll()).thenReturn(Collections.singletonList(new FilterValue(filterValueId, filterId, filterValue)));

        doReturn(taskLists).when(taskListService).getTaskListByDeployment(deploymentId);

        List<TaskListFilterMap> filterMaps = Arrays.asList(
                new TaskListFilterMap(1L, deploymentId, 1L),
                new TaskListFilterMap(2L, deploymentId, 2L)
        );
        doReturn(filterMaps).when(taskListService).getTaskListFiltersByTaskListIds(Collections.singletonList(1L));
        List<FilterDetails> filterDetails = Arrays.asList(
                new FilterDetails("Name1", "Value1", 1L),
                new FilterDetails("Name2", "Value2", 2L)
        );
        doReturn(filterDetails).when(taskListService).getFilterDetails(Arrays.asList(1L, 2L));
        doReturn(Collections.singletonList(new TaskList(1L, name, webIcon, appIcon, description, extraData))).when(taskListService).getAllTaskLists();

        List<TaskListItemResponse> responseList = taskListService.getTaskListFilterResponseByDeploymentId(deploymentId, true);

        assertNotNull(responseList);
        assertEquals(responseList.get(0).getId(), taskLists.get(0).getId());
        assertEquals(filterMaps.size(), responseList.get(0).getFilters().size());
        assertEquals(0, responseList.get(0).getDisplayColumns().size());

    }

    @Test
    public void testGetTaskListFilterResponseWithDisplayColumnsRequiredTrue() {
        List<DeploymentTaskList> taskLists = Collections.singletonList(
                new DeploymentTaskList(
                        1L,
                        deploymentId,
                        1L,
                        displayColumns,
                        displayOrder
                )
        );

        Long filterValueId = 1L;
        Long filterId = 1L;
        String filterName = "Name1";
        String filterValue = "Value1";
        TaskListColumn column = new TaskListColumn(id, labelName, label, true);
        when(filterRepository.findAll()).thenReturn(Collections.singletonList(new Filter(filterId, filterName)));
        when(filterValueRepository.findAll()).thenReturn(Collections.singletonList(new FilterValue(filterValueId, filterId, filterValue)));
        when(taskListColumnRepository.findAllById(anyList())).thenReturn(Arrays.asList(column));

        doReturn(taskLists).when(taskListService).getTaskListByDeployment(deploymentId);

        List<TaskListFilterMap> filterMaps = Arrays.asList(
                new TaskListFilterMap(1L, deploymentId, 1L),
                new TaskListFilterMap(2L, deploymentId, 2L)
        );
        doReturn(filterMaps).when(taskListService).getTaskListFiltersByTaskListIds(Collections.singletonList(1L));
        List<FilterDetails> filterDetails = Arrays.asList(
                new FilterDetails("Name1", "Value1", 1L),
                new FilterDetails("Name2", "Value2", 2L)
        );
        doReturn(filterDetails).when(taskListService).getFilterDetails(Arrays.asList(1L, 2L));
        doReturn(Collections.singletonList(new TaskList(1L, name, webIcon, appIcon, description, extraData))).when(taskListService).getAllTaskLists();

        List<TaskListItemResponse> responseList = taskListService.getTaskListFilterResponseByDeploymentId(deploymentId, true);

        assertNotNull(responseList);
        assertEquals(responseList.get(0).getId(), taskLists.get(0).getId());
        assertEquals(filterMaps.size(), responseList.get(0).getFilters().size());
        assertEquals(1, responseList.get(0).getDisplayColumns().size());

    }

    @Test
    public void testGetTaskListFilterResponseWithDisplayColumnsRequiredFalse() {
        List<DeploymentTaskList> taskLists = Collections.singletonList(
                new DeploymentTaskList(
                        1L,
                        deploymentId,
                        1L,
                        displayColumns,
                        displayOrder
                )
        );

        Long filterValueId = 1L;
        Long filterId = 1L;
        String filterName = "Name1";
        String filterValue = "Value1";
        TaskListColumn column = new TaskListColumn(id, labelName, label, true);
        when(filterRepository.findAll()).thenReturn(Collections.singletonList(new Filter(filterId, filterName)));
        when(filterValueRepository.findAll()).thenReturn(Collections.singletonList(new FilterValue(filterValueId, filterId, filterValue)));
        when(taskListColumnRepository.findAllById(anyList())).thenReturn(Arrays.asList(column));

        doReturn(taskLists).when(taskListService).getTaskListByDeployment(deploymentId);

        List<TaskListFilterMap> filterMaps = Arrays.asList(
                new TaskListFilterMap(1L, deploymentId, 1L),
                new TaskListFilterMap(2L, deploymentId, 2L)
        );
        doReturn(filterMaps).when(taskListService).getTaskListFiltersByTaskListIds(Collections.singletonList(1L));
        List<FilterDetails> filterDetails = Arrays.asList(
                new FilterDetails("Name1", "Value1", 1L),
                new FilterDetails("Name2", "Value2", 2L)
        );
        doReturn(filterDetails).when(taskListService).getFilterDetails(Arrays.asList(1L, 2L));
        doReturn(Collections.singletonList(new TaskList(1L, name, webIcon, appIcon, description, extraData))).when(taskListService).getAllTaskLists();

        List<TaskListItemResponse> responseList = taskListService.getTaskListFilterResponseByDeploymentId(deploymentId, false);

        assertNotNull(responseList);
        assertEquals(responseList.get(0).getId(), taskLists.get(0).getId());
        assertEquals(filterMaps.size(), responseList.get(0).getFilters().size());
        assertEquals(0, responseList.get(0).getDisplayColumns().size());

    }
}

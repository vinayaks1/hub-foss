package com.everwell.registryservice.service;

import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.exceptions.ValidationException;
import com.everwell.registryservice.models.db.VendorMapping;
import com.everwell.registryservice.repository.VendorRepository;
import com.everwell.registryservice.service.impl.VendorServiceImpl;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class VendorServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    VendorServiceImpl vendorService;

    @Mock
    VendorRepository vendorRepository;

    @Test
    public void getVendorId()
    {
        VendorMapping vendorMapping = new VendorMapping(1L, 1L, 1L, 1L);
        when(vendorRepository.findFirstByDeploymentIdAndTriggerRegistryId(any(), any())).thenReturn(vendorMapping);

        long output = vendorService.getVendorIdByDeploymentIdAndTriggerId(1L, 1L);

        assertEquals(1L, output);
    }

    @Test(expected = ValidationException.class)
    public void getVendorId_mappingDoseNotExist()
    {
        when(vendorRepository.findFirstByDeploymentIdAndTriggerRegistryId(any(), any())).thenReturn(null);

        vendorService.getVendorIdByDeploymentIdAndTriggerId(1L, 1L);
    }

}

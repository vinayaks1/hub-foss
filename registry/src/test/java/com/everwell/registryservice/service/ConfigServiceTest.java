package com.everwell.registryservice.service;

import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.constants.ValidationStatusEnum;
import com.everwell.registryservice.exceptions.ConflictException;
import com.everwell.registryservice.exceptions.NotFoundException;
import com.everwell.registryservice.exceptions.ValidationException;
import com.everwell.registryservice.models.db.Config;
import com.everwell.registryservice.models.db.HierarchyConfigMap;
import com.everwell.registryservice.models.dto.HierarchyLineage;
import com.everwell.registryservice.models.http.requests.AddConfigRequest;
import com.everwell.registryservice.models.http.requests.GenerateConfigHierarchyMappingRequest;
import com.everwell.registryservice.models.http.requests.GetConfigHierarchyMappingsRequest;
import com.everwell.registryservice.models.http.response.ConfigHierarchyMappingRecordResponse;
import com.everwell.registryservice.models.http.response.ConfigHierarchyMappingResponse;
import com.everwell.registryservice.models.http.response.ConfigHierarchyMappingBaseResponse;
import com.everwell.registryservice.repository.ConfigRepository;
import com.everwell.registryservice.repository.HierarchyConfigMapRepository;
import com.everwell.registryservice.service.impl.ConfigServiceImpl;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.dao.EmptyResultDataAccessException;

import java.time.LocalDateTime;
import java.util.*;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class ConfigServiceTest extends BaseTest {

    private final Long testConfigId = 1L;
    private final Long testHierarchyId = 34L;
    private final Long testHierarchyConfigId = 25L;
    private final String testConfigName = "testConfig";
    private final String testConfigValue = "testValue";
    private final String testConfigType = "testType";
    @Spy
    @InjectMocks
    private ConfigServiceImpl configService;
    @Mock
    private ConfigRepository configRepository;
    @Mock
    private HierarchyConfigMapRepository hierarchyConfigMapRepository;

    @Test
    public void addNewConfig_ConfigExists_Error() {
        when(configRepository.findByName(testConfigName)).thenReturn(Optional.of(getValidConfigDetails()));
        ConflictException conflictException = assertThrows(ConflictException.class, () -> configService.addNewConfig(getValidAddConfigRequest()));
        assertEquals(ValidationStatusEnum.CONFIG_ALREADY_EXISTS.getMessage(), conflictException.getMessage());
    }

    @Test
    public void addNewConfig_AddValidConfig_Success() {
        when(configRepository.save(any(Config.class))).thenReturn(getValidConfigDetails());
        Config config = configService.addNewConfig(getValidAddConfigRequest());
        assertNotNull(config);
        assertEquals(testConfigName, config.getName());
    }

    @Test
    public void fetchConfigDetails_ConfigNotFound_Error() {
        when(configRepository.findByName(testConfigName)).thenReturn(Optional.empty());
        NotFoundException notFoundException = assertThrows(NotFoundException.class, () -> configService.fetchConfigDetails(testConfigName));
        assertEquals(ValidationStatusEnum.CONFIG_NOT_FOUND.getMessage(), notFoundException.getMessage());
    }

    @Test
    public void fetchConfigDetails_ConfigFound_ValidConfigDetails() {
        when(configRepository.findByName(testConfigName)).thenReturn(Optional.of(getValidConfigDetails()));
        Config config = configService.fetchConfigDetails(testConfigName);
        assertNotNull(config);
        assertEquals(testConfigId, config.getId());
    }

    @Test
    public void getAllConfigs_NoConfigExist_Error() {
        when(configRepository.findAll()).thenReturn(Collections.emptyList());
        NotFoundException notFoundException = assertThrows(NotFoundException.class, () -> configService.getAllConfigs());
        assertEquals(ValidationStatusEnum.NO_CONFIGS_FOUND.getMessage(), notFoundException.getMessage());

    }

    @Test
    public void getAllConfigs_ConfigExist_ListOfConfigs() {
        when(configRepository.findAll()).thenReturn(Collections.singletonList(getValidConfigDetails()));
        List<Config> allConfigs = configService.getAllConfigs();
        assertEquals(1, allConfigs.size());
    }

    @Test
    public void deleteConfig_ConfigNotExists_Error() {
        when(configRepository.findByName(testConfigName)).thenReturn(Optional.empty());
        NotFoundException notFoundException = assertThrows(NotFoundException.class, () -> configService.deleteConfig(testConfigName));
        assertEquals(ValidationStatusEnum.CONFIG_NOT_FOUND.getMessage(), notFoundException.getMessage());
    }

    @Test
    public void deleteConfig_ConfigMapped_Error() {
        when(configRepository.findByName(testConfigName)).thenReturn(Optional.of(getValidConfigDetails()));
        when(hierarchyConfigMapRepository.countByConfigMappingId(testConfigId)).thenReturn(1);
        ConflictException conflictException = assertThrows(ConflictException.class, () -> configService.deleteConfig(testConfigName));
        assertEquals(ValidationStatusEnum.MAPPED_CONFIG_CANNOT_BE_DELETED.getMessage(), conflictException.getMessage());
    }

    @Test
    public void deleteConfig_RecordDeleted_True() {
        when(configRepository.findByName(testConfigName)).thenReturn(Optional.of(getValidConfigDetails()));
        when(hierarchyConfigMapRepository.countByConfigMappingId(testConfigId)).thenReturn(0);
        when(configRepository.deleteByName(testConfigName)).thenReturn(1);
        boolean configDeleted = configService.deleteConfig(testConfigName);
        assertTrue(configDeleted);
    }

    @Test
    public void deleteConfig_NoRecordDeleted_False() {
        when(configRepository.findByName(testConfigName)).thenReturn(Optional.of(getValidConfigDetails()));
        when(hierarchyConfigMapRepository.countByConfigMappingId(testConfigId)).thenReturn(0);
        when(configRepository.deleteByName(testConfigName)).thenReturn(0);
        boolean configDeleted = configService.deleteConfig(testConfigName);
        assertFalse(configDeleted);
    }

    @Test
    public void validateInputConfigs_configCountMismatch_Error() {
        when(configRepository.countByNameIn(any(List.class))).thenReturn(1);
        ValidationException validationException = assertThrows(ValidationException.class, () -> configService.validateInputConfigs(getInputConfigs()));
        assertEquals(ValidationStatusEnum.CONFIG_ASSOCIATED_WITH_HIERARCHY_DOES_NOT_EXIST.getMessage(), validationException.getMessage());
    }

    @Test
    public void validateInputConfigs_configCountMatch_Success() {
        when(configRepository.countByNameIn(any(List.class))).thenReturn(2);
        configService.validateInputConfigs(getInputConfigs());
    }

    @Test
    public void addHierarchyConfig_ConfigNotExist_Error() {
        when(configRepository.findByName(testConfigName)).thenReturn(Optional.empty());
        when(hierarchyConfigMapRepository.getByHierarchyIdAndConfigMappingId(testHierarchyId, testConfigId)).thenReturn(Optional.of(getValidHierarchyConfigMap()));
        NotFoundException notFoundException = assertThrows(NotFoundException.class, () -> configService.addConfigHierarchyMapping(getValidAddHierarchyConfigRequest(), testHierarchyId));
        assertEquals(ValidationStatusEnum.CONFIG_NOT_FOUND.getMessage(), notFoundException.getMessage());
    }

    @Test
    public void addHierarchyConfig_MappingAlreadyPresent_Error() {
        when(configRepository.findByName(testConfigName)).thenReturn(Optional.of(getValidConfigDetails()));
        when(hierarchyConfigMapRepository.getByHierarchyIdAndConfigMappingId(testHierarchyId, testConfigId)).thenReturn(Optional.of(getValidHierarchyConfigMap()));
        ConflictException conflictException = assertThrows(ConflictException.class, () -> configService.addConfigHierarchyMapping(getValidAddHierarchyConfigRequest(), testHierarchyId));
        assertEquals(ValidationStatusEnum.HIERARCHY_CONFIG_MAPPING_REDUNDANT.getMessage(), conflictException.getMessage());
    }

    @Test
    public void addHierarchyConfig_NewMapping_Success() {
        when(configRepository.findByName(testConfigName)).thenReturn(Optional.of(getValidConfigDetails()));
        when(hierarchyConfigMapRepository.getByHierarchyIdAndConfigMappingId(testHierarchyId, testConfigId)).thenReturn(Optional.empty());
        ConfigHierarchyMappingRecordResponse configHierarchyMappingRecordResponse = configService.addConfigHierarchyMapping(getValidAddHierarchyConfigRequest(), testHierarchyId);
        assertNotNull(configHierarchyMappingRecordResponse);
        assertTrue(configHierarchyMappingRecordResponse.isActive());
        assertEquals(testConfigName, configHierarchyMappingRecordResponse.getConfigName());
        assertEquals(testHierarchyId, configHierarchyMappingRecordResponse.getHierarchyId());
        assertEquals(testConfigValue, configHierarchyMappingRecordResponse.getValue());
    }

    @Test
    public void mapHierarchyWithConfigs_existingConfigsOnly() {
        when(configRepository.findByNameIn(any(List.class))).thenReturn(getConfigList());
        configService.mapConfigsWithHierarchy(testHierarchyId, getInputConfigs());
    }

    @Test
    public void mapHierarchyWithConfigs_existingAndNewConfigs() {
        when(configRepository.findByNameIn(any(List.class))).thenReturn(getConfigList());
        Map<String, Object> inputConfigs = getInputConfigs();
        inputConfigs.put(testConfigName, testConfigName);
        when(configRepository.saveAll(any(List.class))).thenReturn(getConfigList());
        configService.mapConfigsWithHierarchy(testHierarchyId, inputConfigs);
    }

    @Test
    public void deleteHierarchyConfig_mappingNotExists_Error() {
        when(configRepository.findByName(testConfigName)).thenReturn(Optional.of(getValidConfigDetails()));
        when(hierarchyConfigMapRepository.getByHierarchyIdAndConfigMappingId(testHierarchyId, testConfigId)).thenReturn(Optional.empty());
        NotFoundException notFoundException = assertThrows(NotFoundException.class, () -> configService.deleteConfigHierarchyMapping(testConfigName, testHierarchyId));
        assertEquals(ValidationStatusEnum.HIERARCHY_CONFIG_MAPPING_NOT_FOUND.getMessage(), notFoundException.getMessage());
    }

    @Test
    public void deleteHierarchyConfig_Exception_Failure() {
        when(configRepository.findByName(testConfigName)).thenReturn(Optional.of(getValidConfigDetails()));
        when(hierarchyConfigMapRepository.getByHierarchyIdAndConfigMappingId(testHierarchyId, testConfigId)).thenReturn(Optional.of(getValidHierarchyConfigMap()));
        doThrow(new EmptyResultDataAccessException(0)).when(hierarchyConfigMapRepository).deleteById(testHierarchyConfigId);
        assertFalse(configService.deleteConfigHierarchyMapping(testConfigName, testHierarchyId));
    }

    @Test
    public void deleteHierarchyConfig_MappingExist_Success() {
        when(configRepository.findByName(testConfigName)).thenReturn(Optional.of(getValidConfigDetails()));
        when(hierarchyConfigMapRepository.getByHierarchyIdAndConfigMappingId(testHierarchyId, testConfigId)).thenReturn(Optional.of(getValidHierarchyConfigMap()));
        assertTrue(configService.deleteConfigHierarchyMapping(testConfigName, testHierarchyId));
    }

    @Test
    public void updateHierarchyConfig_MappingNotPresent_Error() {
        when(configRepository.findByName(testConfigName)).thenReturn(Optional.of(getValidConfigDetails()));
        when(hierarchyConfigMapRepository.getByHierarchyIdAndConfigMappingId(testHierarchyId, testConfigId)).thenReturn(Optional.empty());
        NotFoundException notFoundException = assertThrows(NotFoundException.class, () -> configService.updateConfigHierarchyMapping(getValidAddHierarchyConfigRequest(), testHierarchyId));
        assertEquals(ValidationStatusEnum.HIERARCHY_CONFIG_MAPPING_NOT_FOUND.getMessage(), notFoundException.getMessage());
    }

    @Test
    public void updateHierarchyConfig_MultiMappingPresent_Error() {
        when(configRepository.findByName(testConfigName)).thenReturn(Optional.of(getValidConfigDetails()));
        when(hierarchyConfigMapRepository.getByHierarchyIdAndConfigMappingId(testHierarchyId, testConfigId)).thenReturn(Optional.of(getValidHierarchyConfigMapList()));
        ConflictException conflictException = assertThrows(ConflictException.class, () -> configService.updateConfigHierarchyMapping(getValidAddHierarchyConfigRequest(), testHierarchyId));
        assertEquals(ValidationStatusEnum.MULTIPLE_HIERARCHY_CONFIG_MAPPING_PRESENT.getMessage(), conflictException.getMessage());
    }

    @Test
    public void updateHierarchyConfig_NoChangeInPresentMapping_Success() {
        when(configRepository.findByName(testConfigName)).thenReturn(Optional.of(getValidConfigDetails()));
        when(hierarchyConfigMapRepository.getByHierarchyIdAndConfigMappingId(testHierarchyId, testConfigId)).thenReturn(Optional.of(getValidHierarchyConfigMap()));
        GenerateConfigHierarchyMappingRequest updateHierarchyConfigRequest = getValidAddHierarchyConfigRequest();
        ConfigHierarchyMappingRecordResponse configHierarchyMappingRecordResponse = configService.updateConfigHierarchyMapping(updateHierarchyConfigRequest, testHierarchyId);
        assertNotNull(configHierarchyMappingRecordResponse);
        assertEquals(updateHierarchyConfigRequest.getConfigName(), configHierarchyMappingRecordResponse.getConfigName());
        assertEquals(updateHierarchyConfigRequest.getHierarchyId(), configHierarchyMappingRecordResponse.getHierarchyId());
        assertEquals(updateHierarchyConfigRequest.getValue(), configHierarchyMappingRecordResponse.getValue());
        verify(hierarchyConfigMapRepository, times(0)).save(any(HierarchyConfigMap.class));
    }

    @Test
    public void updateHierarchyConfig_mappingUpdate_Success() {
        when(configRepository.findByName(testConfigName)).thenReturn(Optional.of(getValidConfigDetails()));
        when(hierarchyConfigMapRepository.getByHierarchyIdAndConfigMappingId(testHierarchyId, testConfigId)).thenReturn(Optional.of(getValidHierarchyConfigMap()));
        GenerateConfigHierarchyMappingRequest updateHierarchyConfigRequest = getValidUpdateHierarchyConfigRequest();
        ConfigHierarchyMappingRecordResponse configHierarchyMappingRecordResponse = configService.updateConfigHierarchyMapping(updateHierarchyConfigRequest, testHierarchyId);
        assertNotNull(configHierarchyMappingRecordResponse);
        assertEquals(updateHierarchyConfigRequest.getConfigName(), configHierarchyMappingRecordResponse.getConfigName());
        assertEquals(updateHierarchyConfigRequest.getHierarchyId(), configHierarchyMappingRecordResponse.getHierarchyId());
        assertEquals(updateHierarchyConfigRequest.getValue(), configHierarchyMappingRecordResponse.getValue());
        verify(hierarchyConfigMapRepository, times(1)).save(any(HierarchyConfigMap.class));
    }

    @Test
    public void fetchConfigHierarchyMappings_InvalidConfigName_Error() {
        when(configRepository.findAll()).thenReturn(Collections.singletonList(getValidConfigDetails()));
        when(hierarchyConfigMapRepository.getByHierarchyIdInAndConfigMappingIdIn(any(List.class), any(List.class))).thenReturn(getValidHierarchyConfigMapList());
        NotFoundException notFoundException = assertThrows(NotFoundException.class, () -> configService.fetchConfigHierarchyMappings(getValidConfigHierarchyMappingsRequest(), getValidHierarchyLineages()));
        assertEquals(ValidationStatusEnum.CONFIG_NOT_FOUND.getMessage(), notFoundException.getMessage());
    }

    @Test
    public void fetchConfigHierarchyMappings_allMappingsExist() {
        when(configRepository.findAll()).thenReturn(getConfigList());
        when(hierarchyConfigMapRepository.getByHierarchyIdInAndConfigMappingIdIn(any(List.class), any(List.class))).thenReturn(getValidHierarchyConfigMapList());
        List<ConfigHierarchyMappingResponse> configHierarchyMappingResponses = configService.fetchConfigHierarchyMappings(getValidConfigHierarchyMappingsRequest(), getValidHierarchyLineages());
        assertNotNull(configHierarchyMappingResponses);
        configHierarchyMappingResponses.forEach(response -> {
            assertNotNull(response.getHierarchyId());
            List<ConfigHierarchyMappingRecordResponse> configs = response.getConfigs();
            assertNotNull(configs);
            configs.forEach(config -> {
                assertNotNull(config.getHierarchyId());
                assertNotNull(config.getConfigName());
                assertNotNull(config.getValue());
            });
        });
    }

    @Test
    public void fetchConfigHierarchyMappings_defaultValuesUsed() {
        GetConfigHierarchyMappingsRequest validConfigHierarchyMappingsRequest = getValidConfigHierarchyMappingsRequest();
        validConfigHierarchyMappingsRequest.getHierarchies().put(testHierarchyId, null);
        List<HierarchyLineage> validHierarchyLineages = getValidHierarchyLineages();
        HierarchyLineage hierarchyLineage = new HierarchyLineage();
        hierarchyLineage.setId(testHierarchyId);
        Map<Long, Long> ancestralLevelMap = new HashMap<>();
        ancestralLevelMap.put(testHierarchyId, 1L);
        hierarchyLineage.setAncestralLevelMap(ancestralLevelMap);
        validHierarchyLineages.add(hierarchyLineage);
        when(configRepository.findAll()).thenReturn(getConfigList());
        when(hierarchyConfigMapRepository.getByHierarchyIdInAndConfigMappingIdIn(any(List.class), any(List.class))).thenReturn(getValidHierarchyConfigMapList());
        List<ConfigHierarchyMappingResponse> configHierarchyMappingResponses = configService.fetchConfigHierarchyMappings(validConfigHierarchyMappingsRequest, validHierarchyLineages);
        assertNotNull(configHierarchyMappingResponses);
        configHierarchyMappingResponses.forEach(response -> {
            assertNotNull(response.getHierarchyId());
            List<ConfigHierarchyMappingRecordResponse> configs = response.getConfigs();
            assertNotNull(configs);
            configs.forEach(config -> {
                assertNotNull(config.getHierarchyId());
                assertNotNull(config.getConfigName());
                assertNotNull(config.getValue());
            });
        });
    }

    @Test
    public void fetchConfigHierarchyMappings_mappingPresentAtMultipleLevel() {
        List<HierarchyLineage> validHierarchyLineages = new ArrayList<>();
        HierarchyLineage hierarchyLineage = new HierarchyLineage();
        hierarchyLineage.setId(testHierarchyId);
        Map<Long, Long> ancestralLevelMap = new HashMap<>();
        ancestralLevelMap.put(testHierarchyId, 1L);
        ancestralLevelMap.put(testHierarchyConfigId, 2L);
        hierarchyLineage.setAncestralLevelMap(ancestralLevelMap);
        validHierarchyLineages.add(hierarchyLineage);
        List<HierarchyConfigMap> validHierarchyConfigMapList = new ArrayList<>();
        HierarchyConfigMap hierarchyConfigMap = new HierarchyConfigMap();
        hierarchyConfigMap.setHierarchyId(testHierarchyConfigId);
        hierarchyConfigMap.setConfigMappingId(3L);
        hierarchyConfigMap.setValue(testConfigValue);
        validHierarchyConfigMapList.add(hierarchyConfigMap);
        HierarchyConfigMap hierarchyConfigMap2 = new HierarchyConfigMap();
        hierarchyConfigMap2.setHierarchyId(testHierarchyId);
        hierarchyConfigMap2.setConfigMappingId(3L);
        hierarchyConfigMap2.setValue(testConfigValue);
        validHierarchyConfigMapList.add(hierarchyConfigMap2);
        when(configRepository.findAll()).thenReturn(getConfigList());
        when(hierarchyConfigMapRepository.getByHierarchyIdInAndConfigMappingIdIn(any(List.class), any(List.class))).thenReturn(validHierarchyConfigMapList);
        List<ConfigHierarchyMappingResponse> configHierarchyMappingResponses = configService.fetchConfigHierarchyMappings(getValidConfigHierarchyMappingsRequest(), validHierarchyLineages);
        assertNotNull(configHierarchyMappingResponses);
        configHierarchyMappingResponses.forEach(response -> {
            assertNotNull(response.getHierarchyId());
            List<ConfigHierarchyMappingRecordResponse> configs = response.getConfigs();
            assertNotNull(configs);
            configs.forEach(config -> {
                assertNotNull(config.getHierarchyId());
                assertNotNull(config.getConfigName());
                assertNotNull(config.getValue());
            });
        });
    }

    @Test
    public void fetchConfigHierarchyMappings_multiMappingPresent() {
        List<HierarchyLineage> validHierarchyLineages = new ArrayList<>();
        HierarchyLineage hierarchyLineage = new HierarchyLineage();
        hierarchyLineage.setId(testHierarchyId);
        Map<Long, Long> ancestralLevelMap = new HashMap<>();
        ancestralLevelMap.put(testHierarchyId, 1L);
        ancestralLevelMap.put(testHierarchyConfigId, 2L);
        hierarchyLineage.setAncestralLevelMap(ancestralLevelMap);
        validHierarchyLineages.add(hierarchyLineage);
        List<HierarchyConfigMap> validHierarchyConfigMapList = new ArrayList<>();
        HierarchyConfigMap hierarchyConfigMap = new HierarchyConfigMap();
        hierarchyConfigMap.setHierarchyId(testHierarchyId);
        hierarchyConfigMap.setConfigMappingId(6L);
        hierarchyConfigMap.setValue(testConfigValue);
        validHierarchyConfigMapList.add(hierarchyConfigMap);
        HierarchyConfigMap hierarchyConfigMap2 = new HierarchyConfigMap();
        hierarchyConfigMap2.setHierarchyId(testHierarchyId);
        hierarchyConfigMap2.setConfigMappingId(6L);
        hierarchyConfigMap2.setValue(testConfigValue);
        validHierarchyConfigMapList.add(hierarchyConfigMap2);
        when(configRepository.findAll()).thenReturn(getConfigListWithMultiMapEligibleConfig());
        when(hierarchyConfigMapRepository.getByHierarchyIdInAndConfigMappingIdIn(any(List.class), any(List.class))).thenReturn(validHierarchyConfigMapList);
        List<ConfigHierarchyMappingResponse> configHierarchyMappingResponses = configService.fetchConfigHierarchyMappings(getValidConfigHierarchyMappingsRequest(), validHierarchyLineages);
        assertNotNull(configHierarchyMappingResponses);
        configHierarchyMappingResponses.forEach(response -> {
            assertNotNull(response.getHierarchyId());
            List<ConfigHierarchyMappingRecordResponse> configs = response.getConfigs();
            assertNotNull(configs);
            configs.forEach(config -> {
                assertNotNull(config.getHierarchyId());
                assertNotNull(config.getConfigName());
                if (config.isMultiMapped()) {
                    assertNotNull(config.getValues());
                } else {
                    assertNotNull(config.getValue());
                }
            });
        });
    }

    @Test
    public void fetchConfigHierarchyMappingsById_noMappingFound_Error() {
        when(hierarchyConfigMapRepository.findAllById(any(List.class))).thenReturn(null);
        NotFoundException notFoundException = assertThrows(NotFoundException.class, () -> configService.fetchConfigHierarchyMappingsById(Collections.singletonList(1L)));
        assertEquals(ValidationStatusEnum.HIERARCHY_CONFIG_MAPPING_NOT_FOUND.getMessage(), notFoundException.getMessage());
    }

    @Test
    public void fetchConfigHierarchyMappingsById_MappingFound_Success() {
        when(hierarchyConfigMapRepository.findAllById(any(List.class))).thenReturn(getValidHierarchyConfigMap());
        List<ConfigHierarchyMappingBaseResponse> configHierarchyMappingBaseResponses = configService.fetchConfigHierarchyMappingsById(Collections.singletonList(1L));
        assertNotNull(configHierarchyMappingBaseResponses);
        assertEquals(1, configHierarchyMappingBaseResponses.size());
    }

    private AddConfigRequest getValidAddConfigRequest() {
        return new AddConfigRequest(testConfigName, testConfigValue, testConfigType);
    }

    private Config getValidConfigDetails() {
        return new Config(testConfigId, testConfigName, testConfigValue, testConfigType, LocalDateTime.now(), false);
    }

    private Map<String, Object> getInputConfigs() {
        Map<String, Object> inputConfigs = new HashMap<>();
        inputConfigs.put(testConfigName, testConfigValue);
        inputConfigs.put(testConfigType, testConfigValue);
        return inputConfigs;
    }

    private GenerateConfigHierarchyMappingRequest getValidAddHierarchyConfigRequest() {
        return new GenerateConfigHierarchyMappingRequest(testHierarchyId, testConfigName, testConfigValue);
    }

    private List<Config> getConfigList() {
        List<Config> configs = new ArrayList<>();
        for (long i = 1; i <= 10; i++) {
            Config config = getValidConfigDetails();
            if (i > 1) {
                config.setName(testConfigName + (i - 1));
                config.setId(i);
            }
            configs.add(config);
        }
        return configs;
    }

    private List<Config> getConfigListWithMultiMapEligibleConfig() {
        List<Config> configs = new ArrayList<>();
        for (long i = 1; i <= 5; i++) {
            Config config = getValidConfigDetails();
            config.setId(i);
            configs.add(config);
        }
        Config config = getValidConfigDetails();
        config.setId(6L);
        config.setMultiMapped(true);
        configs.add(config);
        return configs;
    }

    private List<HierarchyConfigMap> getValidHierarchyConfigMap() {
        return Collections.singletonList(new HierarchyConfigMap(testHierarchyConfigId, testHierarchyId, testConfigId, testConfigValue, true, LocalDateTime.now()));
    }


    private List<HierarchyConfigMap> getValidHierarchyConfigMapList() {
        List<HierarchyConfigMap> hierarchyConfigMapList = new ArrayList<>();
        for (long i = 0; i < 5; i++) {
            hierarchyConfigMapList.add(new HierarchyConfigMap(testHierarchyConfigId + i, testHierarchyId + i, testConfigId + i, testConfigValue, true, LocalDateTime.now()));
        }
        return hierarchyConfigMapList;
    }

    private GenerateConfigHierarchyMappingRequest getValidUpdateHierarchyConfigRequest() {
        return new GenerateConfigHierarchyMappingRequest(testHierarchyId, testConfigName, testConfigValue + "2");
    }

    private GetConfigHierarchyMappingsRequest getValidConfigHierarchyMappingsRequest() {
        GetConfigHierarchyMappingsRequest getConfigHierarchyMappingsRequest = new GetConfigHierarchyMappingsRequest();
        Map<Long, List<String>> hierarchies = new HashMap<>();
        for (long i = 1; i <= 5L; i++) {
            List<String> configNames = new ArrayList<>();
            configNames.add(testConfigName);
            configNames.add(testConfigName + i);
            hierarchies.put(i, configNames);
        }
        getConfigHierarchyMappingsRequest.setHierarchies(hierarchies);
        return getConfigHierarchyMappingsRequest;
    }

    private List<HierarchyLineage> getValidHierarchyLineages() {
        List<HierarchyLineage> hierarchyLineageList = new ArrayList<>();
        for (long i = 1; i <= 5L; i++) {
            HierarchyLineage hierarchyLineage = new HierarchyLineage();
            hierarchyLineage.setId(i);
            Map<Long, Long> ancestralLevelMap = new HashMap<>();
            for (long j = 0; j < 5L; j++) {
                ancestralLevelMap.put(testHierarchyId + j, j + 1);
            }
            ancestralLevelMap.put(i, i + i);
            hierarchyLineage.setAncestralLevelMap(ancestralLevelMap);
            hierarchyLineageList.add(hierarchyLineage);
        }
        return hierarchyLineageList;
    }
}

package com.everwell.registryservice.service;

import com.everwell.registryservice.BaseTest;
import com.everwell.registryservice.exceptions.InternalServerErrorException;
import com.everwell.registryservice.exceptions.ValidationException;
import com.everwell.registryservice.models.http.requests.sso.RegistrationRequest;
import com.everwell.registryservice.models.http.response.Response;
import com.everwell.registryservice.models.http.response.sso.LoginResponse;
import com.everwell.registryservice.models.http.response.sso.UserSSOResponse;
import com.everwell.registryservice.service.impl.SSOServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doReturn;

public class SSOServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    SSOServiceImpl ssoService;

    @Before
    public void init() {
        ReflectionTestUtils.setField(ssoService, "dataGatewayURL", "http://test:8080");
        ReflectionTestUtils.setField(ssoService, "username", "test");
        ReflectionTestUtils.setField(ssoService, "password", "test");
    }

    @Test
    public void registrationTest() {
        RegistrationRequest registrationRequest = new RegistrationRequest("username", "password");
        UserSSOResponse userSSOResponse = new UserSSOResponse(1L, "username", null, null, null);
        ResponseEntity<Response<UserSSOResponse>> response = new ResponseEntity<>(new Response<>(true, userSSOResponse), HttpStatus.OK);

        doReturn(response).when(ssoService).postExchange(any());

        UserSSOResponse apiResponse = ssoService.registration(clientId_Nikshay.toString(), registrationRequest);

        assertEquals(userSSOResponse.getUsername(), apiResponse.getUsername());
    }

    @Test(expected = ValidationException.class)
    public void registrationTest_Failure() {
        RegistrationRequest registrationRequest = new RegistrationRequest("username", "password");
        UserSSOResponse userSSOResponse = new UserSSOResponse(1L, "username", null, null, null);
        ResponseEntity<Response<UserSSOResponse>> response = new ResponseEntity<>(new Response<>(false, userSSOResponse), HttpStatus.BAD_REQUEST);

        doReturn(response).when(ssoService).postExchange(any());

        UserSSOResponse apiResponse = ssoService.registration(clientId_Nikshay.toString(), registrationRequest);

        assertEquals(userSSOResponse.getUsername(), apiResponse.getUsername());
    }

    @Test(expected = InternalServerErrorException.class)
    public void registrationTest_Bodynull_Failure() {
        RegistrationRequest registrationRequest = new RegistrationRequest("username", "password");
        UserSSOResponse userSSOResponse = new UserSSOResponse(1L, "username", null, null, null);
        ResponseEntity<Response<UserSSOResponse>> response = new ResponseEntity<>(new Response<>(true, null), HttpStatus.OK);

        doReturn(response).when(ssoService).postExchange(any());

        UserSSOResponse apiResponse = ssoService.registration(clientId_Nikshay.toString(), registrationRequest);

        assertEquals(userSSOResponse.getUsername(), apiResponse.getUsername());
    }

    @Test
    public void loginTest() {
        Map<String, Object> loginRequest = new HashMap<>();
        LoginResponse loginResponse = new LoginResponse();
        loginResponse.setSessionId("1");
        ResponseEntity<Response<LoginResponse>> response = new ResponseEntity<>(new Response<>(true, loginResponse), HttpStatus.OK);

        doReturn(response).when(ssoService).postExchange(any());

        ResponseEntity<Response<LoginResponse>> responseEntity = ssoService.login(clientId_Nikshay.toString(), loginRequest);
        assertEquals(loginResponse.getSessionId(), responseEntity.getBody().getData().getSessionId());
    }

    @Test
    public void logoutTest() {
        Map<String, Object> logoutRequest = new HashMap<>();
        Map<String, Object> justaResponse = Map.of("key", "value");
        ResponseEntity<Response<Map<String, Object>>> response = new ResponseEntity<>(new Response<>(true, justaResponse), HttpStatus.OK);

        doReturn(response).when(ssoService).postExchange(any());

        Boolean apiResponse = ssoService.logout(clientId_Nikshay.toString(), logoutRequest);
        assertEquals(apiResponse, response.getBody().isSuccess());
    }
}

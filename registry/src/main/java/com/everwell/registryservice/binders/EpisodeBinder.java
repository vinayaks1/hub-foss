package com.everwell.registryservice.binders;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface EpisodeBinder {
    String EPISODE_NOTIFICATION_INPUT = "episode-notification-input";

    @Output(EPISODE_NOTIFICATION_INPUT)
    MessageChannel episodeNotificationOutput();
}

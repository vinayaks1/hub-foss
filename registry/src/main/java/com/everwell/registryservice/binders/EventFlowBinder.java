package com.everwell.registryservice.binders;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface EventFlowBinder {
    String EPISODE_NOTIFICATION_INPUT = "event-input";

    @Output(EPISODE_NOTIFICATION_INPUT)
    MessageChannel eventOutput();
}

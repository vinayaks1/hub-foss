package com.everwell.registryservice.handlers;

import com.everwell.registryservice.binders.EpisodeBinder;
import com.everwell.registryservice.constants.Constants;
import com.everwell.registryservice.exceptions.HandlerException;
import com.everwell.registryservice.models.dto.GenericEvent;
import com.everwell.registryservice.models.http.requests.ins.TriggerDetailsRequest;
import com.everwell.registryservice.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
@EnableBinding(EpisodeBinder.class)
public class EpisodeHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(EpisodeHandler.class);

    @Autowired
    private EpisodeBinder episodeBinder;

    public void sendTriggersToEpisode(List<GenericEvent<TriggerDetailsRequest>> triggers, Long clientId) {
        triggers.forEach(trigger -> {
                    try {
                        episodeBinder.episodeNotificationOutput().send(MessageBuilder.withPayload(Utils.asJsonString(trigger)).setHeader(Constants.CLIENT_ID, clientId).build());
                    } catch (Exception e) {
                        throw new HandlerException(e, Utils.asJsonString(trigger));
                    }
                }
        );
    }
}

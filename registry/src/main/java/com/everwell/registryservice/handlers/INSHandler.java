package com.everwell.registryservice.handlers;

import com.everwell.registryservice.binders.INSBinder;
import com.everwell.registryservice.constants.Constants;
import com.everwell.registryservice.exceptions.HandlerException;
import com.everwell.registryservice.models.dto.GenericEvent;
import com.everwell.registryservice.models.http.requests.ins.SmsDetailsRequest;
import com.everwell.registryservice.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@EnableBinding(INSBinder.class)
public class INSHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(INSHandler.class);

    @Autowired
    private INSBinder insBinder;

    public void sendSmsToINS(List<GenericEvent<SmsDetailsRequest>> smsDetails, Long clientId) {
        smsDetails.forEach(sms -> {
                    try {
                        insBinder.insSmsOutput().send(MessageBuilder.withPayload(Utils.asJsonString(sms)).setHeader(Constants.CLIENT_ID, clientId).build());
                    } catch (Exception e) {
                        throw new HandlerException(e, Utils.asJsonString(sms));
                    }
                }
        );
    }


}

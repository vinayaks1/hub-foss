package com.everwell.registryservice.handlers;

import com.everwell.registryservice.service.TriggerService;
import org.jobrunr.jobs.annotations.Job;
import org.jobrunr.spring.annotations.Recurring;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JobHandler {
    @Autowired
    TriggerService triggerService;

    // This Job will read trigger table and schedule all the jobs according to their cron times
    // Keeping it recurring, so that changes in trigger table can be synced with jobs if any.
    @Recurring(id = "everyday-job-scheduler", cron = "0 0 * * *")
    @Job
    public void EverydayJobScheduler()
    {
        triggerService.readTriggerTable();
    }


}

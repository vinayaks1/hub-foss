package com.everwell.registryservice.constants;

import lombok.Getter;

@Getter
public enum EventFlowConstants {
    GET_ALL_LANGUAGES("Language", "GetLanguages", "Get All Languages"),
    GET_DEPLOYMENT_BY_ID("Deployment", "DeploymentId- ", "Get By Id"),
    GET_DEPLOYMENT_BY_CODE("Deployment", "DeploymentCode- ", "Get By Code"),
    GET_ALL_DEPLOYMENTS("Deployment", "GetAllDeployments ", "Get All Deployments"),
    POST_CREATE_NEW_DEPLOYMENT("Deployment", "", "Create New Deployment"),
    GET_PREFIX("Deployment", "GetPrefix ", "Get Country Prefix"),
    GET_PROJECT_LIST("Deployment", "ProjectList", "Get Project List");

    private final String eventCategory;
    private final String eventName;
    private final String eventAction;
    EventFlowConstants(String eventCategory, String eventName, String eventAction) {
        this.eventAction = eventAction;
        this.eventName = eventName;
        this.eventCategory = eventCategory;
    }
}
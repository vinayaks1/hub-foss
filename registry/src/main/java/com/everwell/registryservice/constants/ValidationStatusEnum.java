package com.everwell.registryservice.constants;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ValidationStatusEnum {
    // Generic success response
    SUCCESS("Operation Successful"),
    // Authentication is invalid
    AUTH_INVALID("Authentication is not valid"),
    // Client ID is not available
    CLIENT_ID_UNAVAILABLE("Client Id is not available"),
    // The config to be added is already existing
    CONFIG_ALREADY_EXISTS("The Config already exists"),
    // The config searched does not exist
    CONFIG_NOT_FOUND("The Config does not exist"),
    // No Config present in DB
    NO_CONFIGS_FOUND("No Config exists currently"),
    // Config mapped to hierarchies cannot be deleted
    MAPPED_CONFIG_CANNOT_BE_DELETED("Config already mapped to hierarchies cannot be deleted"),
    // Level 1 Hierarchy for the deployment already exists
    LEVEL_1_HIERARCHY_EXISTS_FOR_DEPLOYMENT("A level 1 Hierarchy already exists for provided deployment"),
    // Hierarchy With Level greater than 1 should have Parent ID
    PARENT_ID_REQUIRED_FOR_HIERARCHY_NOT_LEVEL_ONE("Parent Id is required for hierarchy with level greater than one"),
    // Parent Hierarchy already at maximum supported level
    CANNOT_CREATE_CHILD_HIERARCHY("Child Hierarchy cannot be created for this parent"),
    // Parent hierarchy level smaller than child hierarchy level
    CHILD_HIERARCHY_LEVEL_GREATER_THAN_PARENT("Child Hierarchy level cannot be greater than that of parent"),
    // Association details provided with hierarchy request are not present in DB
    ASSOCIATIONS_ASSOCIATED_WITH_HIERARCHY_DOES_NOT_EXIST("The association details provided in request do not exist or are not unique"),
    // Config details provided with hierarchy request are not present in DB
    CONFIG_ASSOCIATED_WITH_HIERARCHY_DOES_NOT_EXIST("The config details provided in request do not exist or are not unique"),
    // The Hierarchy searched does not exist
    HIERARCHY_NOT_FOUND("The Hierarchy does not exist"),
    // The Hierarchy to be deleted is already inactive
    HIERARCHY_NOT_ACTIVE("The Hierarchy provided is not an active hierarchy"),
    // The Mapping for Hierarchy and Config does not exist
    HIERARCHY_CONFIG_MAPPING_NOT_FOUND("The Hierarchy Config Mapping does not exist"),
    // The Mapping for Hierarchy and Config already exists
    HIERARCHY_CONFIG_MAPPING_REDUNDANT("The Hierarchy Config Mapping already exists"),
    // Multiple Mappings for Hierarchy and Config already exists
    MULTIPLE_HIERARCHY_CONFIG_MAPPING_PRESENT("Multiple mappings found for Hierarchy Config combination"),
    // The user to be added is already existing
    USER_ALREADY_EXISTS("The user already exists"),
    // The User searched does not exist
    USER_NOT_FOUND("The user does not exist"),
    // The user to be deleted is already inactive
    USER_ALREADY_INACTIVE("The user is already inactive"),
    // Trigger Vendor mapping not found for selected trigger
    TRIGGER_VENDOR_MAPPING_NOT_FOUND("Trigger Vendor Mapping does not exist"),
    // Hierarchy Engagement mapping not found
    HIERARCHY_ENGAGEMENT_NOT_FOUND("The Hierarchy Engagement Mapping does not exist"),
    // Language Ids does not exist
    LANGUAGE_ID_NOT_FOUND("Language Ids does not exist"),
    // Invalid request body (generic)
    INVALID_REQUEST_BODY("Invalid request"),
    // User is already logged in
    USER_ALREADY_LOGGED_IN("User is already logged in"),
    // Token based user does not have a valid session
    USER_SESSION_NOT_FOUND("User session not found"),
    // All exceptions without causes
    SOMETHING_WENT_WRONG("Oops! Something went wrong"),
    //Unauthorized access to the hierarchy
    UNAUTHORIZED_HIERARCHY_ACCESS("The user does not have access to the requested hierarchy");

    private final String message;
}

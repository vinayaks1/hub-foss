package com.everwell.registryservice.constants;

public class Constants {
    public static final String X_CLIENT_ID = "X-Client-Id";
    public static final String ANNONYMOUS_USER = "anonymousUser";
    public static final String ASK_FOR_HELP = "ASK FOR HELP";
    public static final String FAQS = "FAQs";

    public static final String MALFORMED_JWT = "JWT Token Malformed";
    public static final String DEPLOYMENT_BY_ID_CACHE_PREFIX = "deployment:id:";
    public static final String DEPLOYMENT_BY_CODE_CACHE_PREFIX = "deployment:code:";
    public static final Long DAY_LONG_EXPIRY = (24L * 60 * 60);

    public static final String JOB_MODULE_REGISTRY = "Registry";
    public static final String DISEASE_ID = "disease:id:";
    public static final Long MAXIMUM_SUPPORTED_HIERARCHY_LEVEL = 6L;
    public static final String DEFAULT_CONFIG_TYPE = "String";
    public static final String DEFAULT_USER_HIERARCHY_RELATIONSHIP = "Direct";
    public static final String CLIENT_ID = "client-id";
    public static final String ACCEPT = "Accept";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String DATA = "data";
    public static final String KEY = "Key";
    public static final String VALUE = "Value";
    public static final String CLIENT_ID_CACHE_CONFIG = ":client:id:";
}

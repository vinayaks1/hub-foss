package com.everwell.registryservice.constants;

public class AuthConstants {
    public static final String AUTH_TOKEN_BEARER_PREFIX = "Bearer ";
    public static final String AUTH_HEADER_AUTHORIZATION = "Authorization";
    public static final String USERNAME = "username";
    public static final String SSO_AUTH_ISSUER = "everwell";
    public static final String SSO_AUTH_USER_ID = "ssoUserId";
    public static final String PASSWORD = "password";
    public static final String[] AUTH_WHITELIST = {
            "/v1/client",
            "/v1/projectList",
            "/actuator/**"
    };
    public static final String[] AUTH_WHITELIST_LOCAL = {
            "/v3/api-docs",
            "/v3/api-docs/*",
            "/swagger-resources/**",
            "/swagger-ui.html",
            "/swagger-ui/**"
    };
    public static final String DEFAULT_DESIGNATION = "default";
}

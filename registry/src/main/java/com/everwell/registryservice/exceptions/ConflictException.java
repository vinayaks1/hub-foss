package com.everwell.registryservice.exceptions;

import com.everwell.registryservice.constants.ValidationStatusEnum;

public class ConflictException extends RuntimeException {

    public ConflictException(String message) {
        super(message);
    }

    public ConflictException(ValidationStatusEnum validationError) {
        super(validationError.getMessage());
    }
}

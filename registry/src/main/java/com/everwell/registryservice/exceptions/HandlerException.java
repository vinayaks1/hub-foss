package com.everwell.registryservice.exceptions;

import lombok.Getter;

public class HandlerException extends RuntimeException {

    @Getter
    public String data;

    public HandlerException(Exception exception, String extraData) {
        super(exception);
        data = extraData;
    }
}

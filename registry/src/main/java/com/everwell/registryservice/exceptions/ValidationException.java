package com.everwell.registryservice.exceptions;

import com.everwell.registryservice.constants.ValidationStatusEnum;

public class ValidationException extends RuntimeException {

    public ValidationException(String exception) {
        super(exception);
    }

    public ValidationException(ValidationStatusEnum validationError) {
        super(validationError.getMessage());
    }
}

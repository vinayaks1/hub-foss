package com.everwell.registryservice.filters;

import com.everwell.registryservice.constants.AuthConstants;
import com.everwell.registryservice.constants.Constants;
import com.everwell.registryservice.exceptions.NotFoundException;
import com.everwell.registryservice.models.db.Client;
import com.everwell.registryservice.models.db.UserAccess;
import com.everwell.registryservice.models.http.response.Response;
import com.everwell.registryservice.service.ClientService;
import com.everwell.registryservice.service.UserService;
import com.everwell.registryservice.utils.JwtUtils;
import com.everwell.registryservice.utils.OutputStreamUtils;
import io.jsonwebtoken.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Component
public class JwtFilters extends OncePerRequestFilter {

    @Autowired
    ClientService clientService;

    @Autowired
    UserService userService;

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    OutputStreamUtils outputStreamUtils;

    @Value("${sso.authorizationcookie.name}")
    public String ssoAuthCookieName;

    @Override
    public void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {

        try {

            String jwt = getJwtFromHeaderOrCookie(httpServletRequest);
            doFilterLogic(httpServletRequest, jwt);
        } catch (NotFoundException e) {
            outputStreamUtils.writeCustomResponseToOutputStream(httpServletResponse, new Response<String>(e.getMessage()), HttpStatus.NOT_FOUND);
            return;
        } catch (IllegalArgumentException | UnsupportedJwtException e) {
            outputStreamUtils.writeCustomResponseToOutputStream(httpServletResponse, new Response<String>(e.getMessage()), HttpStatus.BAD_REQUEST);
            return;
        } catch (ExpiredJwtException | SignatureException | ArrayIndexOutOfBoundsException e) {
            outputStreamUtils.writeCustomResponseToOutputStream(httpServletResponse, new Response<String>(e.getMessage()), HttpStatus.UNAUTHORIZED);
            return;
        } catch (MalformedJwtException e) {
            outputStreamUtils.writeCustomResponseToOutputStream(httpServletResponse, new Response<String>(Constants.MALFORMED_JWT), HttpStatus.UNAUTHORIZED);
            return;
        } catch (Exception e) {
            outputStreamUtils.writeCustomResponseToOutputStream(httpServletResponse, new Response<String>(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
            return;
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

    private String getJwtFromCookie(Cookie[] cookies) {
        String jwt = null;
        if(null != cookies && cookies.length != 0) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(ssoAuthCookieName)) {
                    jwt = cookie.getValue();
                }
            }
        }
        return jwt;
    }

    private String getJwtFromHeader(String authorizationHeader) {
        String jwt = null;
        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ") && !authorizationHeader.equalsIgnoreCase("Bearer undefined")) {
            jwt = authorizationHeader.substring(7);
        }
        return jwt;
    }

    private String getJwtFromHeaderOrCookie(HttpServletRequest httpServletRequest) {
        String jwt = getJwtFromCookie(httpServletRequest.getCookies());
        if(StringUtils.isEmpty(jwt)) {
            jwt = getJwtFromHeader(httpServletRequest.getHeader("Authorization"));
        }
        return jwt;
    }

    private void doFilterLogic(HttpServletRequest httpServletRequest, String jwt) {
        if(StringUtils.isNotBlank(jwt)) {
            Claims claims = jwtUtils.getParsedClaims(jwt);
            Client client = null;
            UserAccess user = null;
            if (null != claims && AuthConstants.SSO_AUTH_ISSUER.equals(claims.getIssuer()) && null != claims.get(AuthConstants.SSO_AUTH_USER_ID)) {
                // The claims subject would be the username if issued by sso
                Long ssoUserId = Long.parseLong(claims.get(AuthConstants.SSO_AUTH_USER_ID).toString());
                user = userService.fetchUserData(ssoUserId);
                client = clientService.getClient(Long.valueOf(user.getClientId()));
            } else if(null != claims) {
                // The claims subject would the clientId if issued by registry
                client = clientService.getClient(Long.valueOf(claims.getSubject()));
            }
            if (null != client && jwtUtils.validateToken(jwt, user, client)) {
                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(client, user, Collections.emptyList());
                usernamePasswordAuthenticationToken
                        .setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            }
        }
    }

    @Override
    public boolean shouldNotFilter(HttpServletRequest request) {
        List<String> shouldNotFilterUrls = Arrays.asList(AuthConstants.AUTH_WHITELIST);
        return shouldNotFilterUrls.contains(request.getRequestURI());
    }

}

package com.everwell.registryservice;

import com.everwell.registryservice.handlers.JobHandler;
import org.jobrunr.scheduling.JobScheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class Application {

	@Autowired
	JobScheduler jobScheduler;

	@Autowired
	JobHandler jobHandler;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@PostConstruct
	public void triggerJobScheduler ()
	{
		// Every time, application is deployed, we want to trigger job scheduler
		// It will read trigger table and scheduler job accordingly
		jobScheduler.enqueue(() -> jobHandler.EverydayJobScheduler());
	}

}

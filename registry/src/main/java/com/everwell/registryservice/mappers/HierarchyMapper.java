package com.everwell.registryservice.mappers;

import com.everwell.registryservice.models.db.Hierarchy;
import com.everwell.registryservice.models.dto.HierarchyRequestData;
import com.everwell.registryservice.models.dto.HierarchyResponseData;
import com.everwell.registryservice.models.dto.MinHierarchyDetails;
import com.everwell.registryservice.models.http.response.HierarchyResponse;
import com.everwell.registryservice.models.http.response.HierarchySummaryResponseData;
import com.everwell.registryservice.models.dto.HierarchyUpdateData;
import com.everwell.registryservice.models.http.requests.UserUpdateHierarchyRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface HierarchyMapper {

    HierarchyMapper INSTANCE = Mappers.getMapper(HierarchyMapper.class);

    Hierarchy hierarchyRequestDataToHierarchy(HierarchyRequestData hierarchyRequestData);

    HierarchyResponseData hierarchyToHierarchyResponseData(Hierarchy hierarchy);

    HierarchySummaryResponseData hierarchyToHierarchySummaryResponseData(Hierarchy hierarchy);

    HierarchyUpdateData userUpdateHierarchyRequestToHierarchyUpdateData(UserUpdateHierarchyRequest updateHierarchyRequest);

    HierarchySummaryResponseData hierarchyResponseDataToHierarchySummaryResponseData(HierarchyResponseData hierarchyResponseData);

    List<MinHierarchyDetails> hierarchyResponseListToMinHierarchyDetailsList(List<HierarchyResponse> hierarchyResponseList);

    @Mapping(target="id", source="hierarchy.id")
    @Mapping(target="name", source="hierarchy.name")
    @Mapping(target="code", source="hierarchy.code")
    @Mapping(target="type", source="hierarchy.type")
    @Mapping(target="level", source="hierarchy.level")
    @Mapping(target="parentId", source="hierarchy.parentId")
    MinHierarchyDetails hierarchyResponseToMinHierarchyDetails(HierarchyResponse hierarchyResponse);
}

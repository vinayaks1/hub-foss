package com.everwell.registryservice.controller;

import com.everwell.registryservice.mappers.ConfigMapper;
import com.everwell.registryservice.models.db.Config;
import com.everwell.registryservice.models.dto.HierarchyLineage;
import com.everwell.registryservice.models.dto.HierarchyResponseData;
import com.everwell.registryservice.models.http.requests.*;
import com.everwell.registryservice.models.http.response.ConfigHierarchyMappingRecordResponse;
import com.everwell.registryservice.models.http.response.ConfigHierarchyMappingResponse;
import com.everwell.registryservice.models.http.response.ConfigHierarchyMappingBaseResponse;
import com.everwell.registryservice.models.http.response.ConfigResponse;
import com.everwell.registryservice.models.http.response.Response;
import com.everwell.registryservice.service.ConfigService;
import com.everwell.registryservice.service.HierarchyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Class Type - Rest Controller
 * Purpose - Provide endpoints and routes for config management
 * Scope - Modules interacting with configs through REST APIs
 *
 * @author Ashish Shrivastava
 */
@RestController
public class ConfigController extends BaseController {

    @Autowired
    private ConfigService configService;

    @Autowired
    private HierarchyService hierarchyService;

    /**
     * API to Add a new Config in DB
     *
     * @param addConfigRequest - Input details of config to be added
     * @return added config data
     */
    @RequestMapping(value = "v1/config", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response<ConfigResponse>> addNewConfig(@Valid @RequestBody AddConfigRequest addConfigRequest) {
        Config config = configService.addNewConfig(addConfigRequest);
        ConfigResponse configResponse = ConfigMapper.INSTANCE.configToConfigResponse(config);
        return new ResponseEntity<>(new Response<>(true, configResponse), HttpStatus.CREATED);
    }

    /**
     * API to search a config by name
     *
     * @param name - Input config name
     * @return searched config data
     */
    @RequestMapping(value = "v1/config/{name}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response<ConfigResponse>> getConfigByName(@PathVariable String name) {
        Config config = configService.fetchConfigDetails(name);
        return new ResponseEntity<>(new Response<>(true, ConfigMapper.INSTANCE.configToConfigResponse(config)), HttpStatus.OK);
    }

    /**
     * API to get all available configs from DB
     *
     * @return All available configs
     */
    @RequestMapping(value = "v1/config", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response<List<ConfigResponse>>> getAllConfigs() {
        List<Config> allConfigs = configService.getAllConfigs();
        List<ConfigResponse> allConfigDetails = allConfigs.stream().map(ConfigMapper.INSTANCE::configToConfigResponse).collect(Collectors.toList());
        return new ResponseEntity<>(new Response<>(true, allConfigDetails), HttpStatus.OK);
    }

    /**
     * API to delete a particular config
     *
     * @param name - Name of config to be deleted
     * @return Whether deletion is successful
     */
    @RequestMapping(value = "v1/config", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response<String>> deleteConfig(@RequestParam String name) {
        boolean configDeleted = configService.deleteConfig(name);
        ResponseEntity<Response<String>> response;
        if (configDeleted) {
            response = new ResponseEntity<>(new Response<>(true, null, "Deletion of config " + name + " Successful"), HttpStatus.OK);
        } else {
            response = new ResponseEntity<>(new Response<>("Deletion of config " + name + " Failed"), HttpStatus.OK);
        }
        return response;
    }

    /**
     * API to fetch config mappings for given hierarchies and their lineage
     *
     * @param getConfigHierarchyMappingsRequest - Input combination of hierarchies and mappings for which data is to be fetched
     * @return Config Hierarchy mappings list
     */
    @RequestMapping(value = "v1/config/hierarchy/mappings", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response<List<ConfigHierarchyMappingResponse>>> getConfigsForHierarchies(@Valid @RequestBody GetConfigHierarchyMappingsRequest getConfigHierarchyMappingsRequest) {
        // Get Map of Hierarchies and their corresponding configs sent in request
        Map<Long, List<String>> requestHierarchyConfigMappings = getConfigHierarchyMappingsRequest.getHierarchies();
        // Fetch Lineage Data of All Hierarchies passed in request
        List<HierarchyLineage> hierarchyLineages = hierarchyService.fetchAllHierarchiesLineages(new ArrayList<>(requestHierarchyConfigMappings.keySet()), getClientId());
        // Call Config Service to fetch Config Mappings as requested for each hierarchy
        List<ConfigHierarchyMappingResponse> configHierarchyMappingResponseList = configService.fetchConfigHierarchyMappings(getConfigHierarchyMappingsRequest, hierarchyLineages);
        // Send Response to Subscriber
        return new ResponseEntity<>(new Response<>(true, configHierarchyMappingResponseList), HttpStatus.OK);
    }


    /**
     * API to add a new Hierarchy - Config Mapping
     *
     * @param hierarchyConfigMappingRequest - Input Hierarchy Config mapping details
     * @return Saved Hierarchy Config Mapping
     */
    @RequestMapping(value = "v1/config/hierarchy", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response<ConfigHierarchyMappingRecordResponse>> addNewConfigHierarchyMapping(@Valid @RequestBody GenerateConfigHierarchyMappingRequest hierarchyConfigMappingRequest) {
        // Get Hierarchy details, internal validation to check if Hierarchy exists
        HierarchyResponseData hierarchy = hierarchyService.fetchHierarchyById(hierarchyConfigMappingRequest.getHierarchyId(), getClientId());
        // Call Config service to add hierarchy config mapping
        ConfigHierarchyMappingRecordResponse hierarchyConfigMapping = configService.addConfigHierarchyMapping(hierarchyConfigMappingRequest, hierarchy.getId());
        return new ResponseEntity<>(new Response<>(true, hierarchyConfigMapping), HttpStatus.CREATED);
    }

    @RequestMapping(value = "v1/config/hierarchy", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response<String>> deleteConfigHierarchyMapping(@Valid @RequestBody ConfigHierarchyMappingRequest deleteHierarchyConfigRequest) {
        // Get Hierarchy details, internal validation to check if Hierarchy exists
        HierarchyResponseData hierarchy = hierarchyService.fetchHierarchyById(deleteHierarchyConfigRequest.getHierarchyId(), getClientId());
        // Call Config Service to delete Hierarchy Config mapping
        boolean configDeleted = configService.deleteConfigHierarchyMapping(deleteHierarchyConfigRequest.getConfigName(), hierarchy.getId());
        ResponseEntity<Response<String>> response;
        // Check if deletion was successful and accordingly populate response
        if (configDeleted) {
            response = new ResponseEntity<>(new Response<>(true, null,
                    "Deletion of Config " + deleteHierarchyConfigRequest.getConfigName() + " for Hierarchy Id " + deleteHierarchyConfigRequest.getHierarchyId() + " Successful"), HttpStatus.OK);
        } else {
            response = new ResponseEntity<>(new Response<>(
                    "Deletion of Config " + deleteHierarchyConfigRequest.getConfigName() + " for Hierarchy Id " + deleteHierarchyConfigRequest.getHierarchyId() + " Failed"), HttpStatus.OK);
        }
        return response;
    }

    /**
     * API to update an existing Hierarchy - Config Mapping
     *
     * @param hierarchyConfigMappingRequest - Input Hierarchy Config mapping details
     * @return Saved Hierarchy Config Mapping
     */
    @RequestMapping(value = "v1/config/hierarchy", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response<ConfigHierarchyMappingRecordResponse>> updateConfigHierarchyMapping(@Valid @RequestBody GenerateConfigHierarchyMappingRequest hierarchyConfigMappingRequest) {
        // Get Hierarchy details, internal validation to check if Hierarchy exists
        HierarchyResponseData hierarchy = hierarchyService.fetchHierarchyById(hierarchyConfigMappingRequest.getHierarchyId(), getClientId());
        // Call Config Service to update Hierarchy Config mapping data
        ConfigHierarchyMappingRecordResponse hierarchyConfigMapping = configService.updateConfigHierarchyMapping(hierarchyConfigMappingRequest, hierarchy.getId());
        return new ResponseEntity<>(new Response<>(true, hierarchyConfigMapping), HttpStatus.OK);
    }

    /**
     * API to fetch config hierarchy mappings for given config hierarchy mapping Ids
     *
     * @param getConfigHierarchyMappingsByIdRequest - Input list of Config Hierarchy Mapping Ids
     * @return Config Hierarchy mappings list
     */
    @RequestMapping(value = "v1/config/hierarchy/mappingsList", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response<List<ConfigHierarchyMappingBaseResponse>>> getConfigHierarchyMappingsById(@Valid @RequestBody GetConfigHierarchyMappingsByIdRequest getConfigHierarchyMappingsByIdRequest) {
        // Call Config Service to fetch Config Hierarchy Mappings for input Ids
        List<ConfigHierarchyMappingBaseResponse> configHierarchyMappingResponseList = configService.fetchConfigHierarchyMappingsById(getConfigHierarchyMappingsByIdRequest.getMappingIds());
        // Send Response to Subscriber
        return new ResponseEntity<>(new Response<>(true, configHierarchyMappingResponseList), HttpStatus.OK);
    }

}

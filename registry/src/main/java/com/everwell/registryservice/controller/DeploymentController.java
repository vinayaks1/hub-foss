package com.everwell.registryservice.controller;

import com.everwell.registryservice.constants.Constants;
import com.everwell.registryservice.constants.EventFlowConstants;
import com.everwell.registryservice.exceptions.InternalServerErrorException;
import com.everwell.registryservice.exceptions.NotFoundException;
import com.everwell.registryservice.models.db.Deployment;
import com.everwell.registryservice.models.db.DeploymentLanguageMap;
import com.everwell.registryservice.models.db.Language;
import com.everwell.registryservice.models.dto.GenericEvents;
import com.everwell.registryservice.models.http.requests.CreateDeploymentRequest;
import com.everwell.registryservice.models.http.response.*;
import com.everwell.registryservice.service.DeploymentService;
import com.everwell.registryservice.service.DiseaseService;
import com.everwell.registryservice.service.LanguageService;
import com.everwell.registryservice.utils.RedisUtils;
import com.everwell.registryservice.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
public class DeploymentController extends BaseController{
    private static Logger LOGGER = LoggerFactory.getLogger(DeploymentController.class);

    @Autowired
    private DeploymentService deploymentService;

    @Autowired
    private LanguageService languageService;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    private DiseaseService diseaseService;

    private DeploymentResponse deploymentResponse(Deployment deployment)
    {
        DeploymentResponse response = new DeploymentResponse(deployment);
        Long defaultLanguageId = deployment.getDefaultLanguageId();
        List<Language> languages = this.languageService.getAll();
        List<Long> languageIds = languages.stream().map(Language::getId).collect(Collectors.toList());
        Map<Long, Language> languageByIdMap = languages.stream().collect(Collectors.toMap(Language::getId, language -> language));
        if(!languageIds.contains(defaultLanguageId))
        {
            throw new NotFoundException("Default language not found");
        }

        List<DeploymentLanguageMap> languageMaps = this.languageService.getMappings(deployment.getId());

        List<Language> allowedLanguages = new ArrayList<>();
        for(DeploymentLanguageMap languageMap: languageMaps)
        {
            if(!languageIds.contains(languageMap.getLanguageId()))
            {
                throw new NotFoundException("Language not found");
            }

            allowedLanguages.add(languageByIdMap.get(languageMap.getLanguageId()));
        }

        response.setDefaultLanguage(this.languageService.get(defaultLanguageId));
        response.setAllowedLanguages(allowedLanguages);
        response.setDiseases(diseaseService.getDiseaseByDeployment(deployment.getId(), getClientId()));

        return response;
    }

    @RequestMapping(value = "/v1/deployments/code/{code}", method = RequestMethod.GET)
    public ResponseEntity<Response<DeploymentResponse>> getByCode(@PathVariable String code) {
        applicationEventPublisher.publishEvent(new GenericEvents<String>(code, EventFlowConstants.GET_DEPLOYMENT_BY_CODE));
        String cacheKey = Constants.DEPLOYMENT_BY_CODE_CACHE_PREFIX + code + Constants.CLIENT_ID_CACHE_CONFIG + getClientId();
        DeploymentResponse deploymentResponse = null;
        boolean getFromDb = !RedisUtils.hasKey(cacheKey);
        if(RedisUtils.hasKey(cacheKey))
        {
            try
            {
                deploymentResponse = Utils.convertObject(RedisUtils.getFromCache(cacheKey), DeploymentResponse.class);
            }
            catch (Exception ex)
            {
                getFromDb = true;
            }
        }

        if(getFromDb)
        {
            deploymentResponse = deploymentResponse(this.deploymentService.getByCode(code));
            RedisUtils.putIntoCache(cacheKey, Utils.asJsonString(deploymentResponse), (Constants.DAY_LONG_EXPIRY));
        }

        return ResponseEntity.ok(new Response<>(true, deploymentResponse));
    }

    @RequestMapping(value = "/v1/deployments/id/{id}", method = RequestMethod.GET)
    public ResponseEntity<Response<DeploymentResponse>> get(@PathVariable Long id) {
        applicationEventPublisher.publishEvent(new GenericEvents<Long>(id, EventFlowConstants.GET_DEPLOYMENT_BY_ID));
        String cacheKey = Constants.DEPLOYMENT_BY_ID_CACHE_PREFIX + id;
        DeploymentResponse deploymentResponse = null;
        boolean getFromDb = !RedisUtils.hasKey(cacheKey);
        if(RedisUtils.hasKey(cacheKey))
        {
            try
            {
                deploymentResponse = Utils.convertObject(RedisUtils.getFromCache(cacheKey), DeploymentResponse.class);
            }
            catch (Exception ex)
            {
                getFromDb = true;
            }
        }

        if(getFromDb)
        {
            deploymentResponse = deploymentResponse(this.deploymentService.get(id));
            RedisUtils.putIntoCache(cacheKey, Utils.asJsonString(deploymentResponse), (Constants.DAY_LONG_EXPIRY));
        }

        return ResponseEntity.ok(new Response<>(true, deploymentResponse));
    }

    @RequestMapping(value = "/v1/deployments", method = RequestMethod.GET)
    public ResponseEntity<Response<List<DeploymentResponse>>> getAll()
    {
        applicationEventPublisher.publishEvent(new GenericEvents<String>(EventFlowConstants.GET_ALL_DEPLOYMENTS));
        return ResponseEntity.ok(new Response<>(true, deploymentService.getAll()
                .stream()
                .map(this::deploymentResponse)
                .collect(Collectors.toList()))
        );
    }

    @RequestMapping(value = "/v1/prefix", method = RequestMethod.GET)
    public ResponseEntity<Response<List<PrefixResponse>>> getPrefix()
    {
        applicationEventPublisher.publishEvent(new GenericEvents<String>(EventFlowConstants.GET_PREFIX));
        return ResponseEntity.ok(new Response<>(true, deploymentService.getPrefix(), ""));
    }

    @RequestMapping(value = "/v1/deployments", method = RequestMethod.POST)
    public ResponseEntity<Response<DeploymentResponse>> create(@RequestBody CreateDeploymentRequest request)
    {
        applicationEventPublisher.publishEvent(new GenericEvents<CreateDeploymentRequest>(request, EventFlowConstants.POST_CREATE_NEW_DEPLOYMENT));
        request.validate();
        List<Language> languages = this.languageService.getAll();
        List<Long> languageIds = languages.stream().map(Language::getId).collect(Collectors.toList());
        if(!languageIds.contains(request.getDefaultLanguageId()))
        {
            throw new NotFoundException("Default language not found");
        }

        for(Long languageId: request.getAllowedLanguages())
        {
            if(!languageIds.contains(languageId))
            {
                throw new NotFoundException("Language not found");
            }
        }
        if (!CollectionUtils.isEmpty(request.getDiseaseMappings()))
        {
            if (!diseaseService.verifyDiseaseIds(request.getDiseaseMappings(), getClientId())) {
                throw new NotFoundException("Disease not found");
            }
        }

        return new ResponseEntity<>(new Response<>(true, this.deploymentResponse(deploymentService.create(request))), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/v1/projectList", method = RequestMethod.GET)
    public ResponseEntity<ResponseUpperCase<List<ProjectListEntry>>> projectList()
    {
//        Commenting this out temporarily, as we need to figure out way to support publishing tracking events for public endpoints
//        applicationEventPublisher.publishEvent(new GenericEvents<String>(EventFlowConstants.GET_PROJECT_LIST));
        return ResponseEntity.ok(new ResponseUpperCase<>(true, deploymentService.projectList(), ""));
    }
}

package com.everwell.registryservice.controller;

import com.everwell.registryservice.constants.EventFlowConstants;
import com.everwell.registryservice.models.db.Language;
import com.everwell.registryservice.models.dto.GenericEvents;
import com.everwell.registryservice.models.http.response.Response;
import com.everwell.registryservice.service.LanguageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class LanguageController
{
    private static Logger LOGGER = LoggerFactory.getLogger(LanguageController.class);

    @Autowired
    private LanguageService languageService;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @RequestMapping(value = "/v1/languages", method = RequestMethod.GET)
    public ResponseEntity<Response<List<Language>>> getAll() {
        applicationEventPublisher.publishEvent(new GenericEvents<String>(EventFlowConstants.GET_ALL_LANGUAGES));
        return ResponseEntity.ok(new Response<>(true, languageService.getAll()));
    }
}

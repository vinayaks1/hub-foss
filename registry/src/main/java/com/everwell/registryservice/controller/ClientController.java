package com.everwell.registryservice.controller;

import com.everwell.registryservice.constants.Constants;
import com.everwell.registryservice.models.http.requests.RegisterClientRequest;
import com.everwell.registryservice.models.http.response.ClientResponse;
import com.everwell.registryservice.models.http.response.Response;
import com.everwell.registryservice.service.ClientService;
import com.everwell.registryservice.utils.JwtUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ClientController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientController.class);

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    ClientService clientService;

    @RequestMapping(value = "/v1/client", method = RequestMethod.GET)
    public ResponseEntity<Response<ClientResponse>> getClient(@RequestHeader(name = Constants.X_CLIENT_ID) Long id) {
        LOGGER.info("[getClient] get client request accepted");
        return ResponseEntity.ok(new Response<>(true, clientService.getClientWithToken(id)));
    }

    @RequestMapping(value = "/v1/client", method = RequestMethod.POST)
    public ResponseEntity<Response<ClientResponse>> registerClient(@RequestBody RegisterClientRequest registerClientRequest) {
        LOGGER.info("[registerClient] register client request accepted");
        registerClientRequest.validate();
        ClientResponse clientResponse = clientService.registerClient(registerClientRequest);
        Response<ClientResponse> response = new Response<>(clientResponse, "client registered successfully");
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

}

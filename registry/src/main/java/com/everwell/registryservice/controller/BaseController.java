package com.everwell.registryservice.controller;

import com.everwell.registryservice.constants.ValidationStatusEnum;
import com.everwell.registryservice.exceptions.NotFoundException;
import com.everwell.registryservice.exceptions.UnauthorizedException;
import com.everwell.registryservice.models.db.Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class BaseController {

    private static final Logger LOGGER = LoggerFactory.getLogger(BaseController.class);

    public Long getClientId() {
        Client client = getClient();
        if (null == client || null == client.getId()) {
            throw new NotFoundException(ValidationStatusEnum.CLIENT_ID_UNAVAILABLE);
        }
        return getClient().getId();
    }

    public Client getClient() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Client client = null;
        if (null != authentication) {
            client = (Client) authentication.getPrincipal();
        } else {
            throw new UnauthorizedException(ValidationStatusEnum.AUTH_INVALID);
        }
        return client;
    }
}

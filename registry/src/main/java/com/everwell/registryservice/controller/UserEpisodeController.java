package com.everwell.registryservice.controller;

import com.everwell.registryservice.models.http.requests.SearchRequest;
import com.everwell.registryservice.models.http.requests.episode.EpisodeHeaderSearchRequest;
import com.everwell.registryservice.models.http.response.Response;
import com.everwell.registryservice.models.http.response.episode.HeaderSearchResponse;
import com.everwell.registryservice.service.EpisodeService;
import com.everwell.registryservice.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Class Type - Rest Controller
 * Purpose - Provide endpoints and routes for episode search based on user login
 * Scope - Modules interacting with episode through user
 *
 */
@RestController
public class UserEpisodeController extends BaseController {

    @Autowired
    private UserService userService;

    @Autowired
    private EpisodeService episodeService;

    @Operation(summary = "Search Episodes mapped to user",
            responses = {
                    @ApiResponse(description = "a list of episodes with minimum details",
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = SearchRequest.class))),
                    @ApiResponse(responseCode = "404", description = "The user does not exist", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
                    @ApiResponse(responseCode = "401", description = "User session not found", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))})
    @RequestMapping(value = "v1/user/episodes", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response<HeaderSearchResponse>> searchEpisodes(@Valid @RequestBody SearchRequest requestBody) {
        requestBody.validate();
        HeaderSearchResponse response = episodeService.headerSearch(new EpisodeHeaderSearchRequest(requestBody));
        // Return Response
        return new ResponseEntity<>(new Response<>(true, response), HttpStatus.OK);
    }
}

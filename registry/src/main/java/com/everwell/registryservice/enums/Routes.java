package com.everwell.registryservice.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Routes {
    INS_TEMPLATE_V1("/ins/v1/template"),
    INS_LANGUAGES_V1("/ins/v1/allLanguages"),
    LOGIN("/sso/v1/ssoserver/validateUserCredentials"),
    REGISTRATION("/sso/v1/user/create"),
    LOGOUT("/sso/v1/ssoserver/logout"),
    UPDATE_DEVICE_ID("/sso/v1/ssoserver/androidSessionUpdate"),
    GET_DEVICE_ID("/sso/v1/ssoserver/deviceIdsUserMap"),
    TOKEN("/token");

    @Getter
    private final String route;
}

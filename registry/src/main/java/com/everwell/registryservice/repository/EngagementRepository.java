package com.everwell.registryservice.repository;

import com.everwell.registryservice.models.db.Engagement;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EngagementRepository extends JpaRepository<Engagement, Long> {
    Engagement findFirst1ByHierarchyId (Long HierarchyId);

    List<Engagement> findByHierarchyIdInAndDisabledLanguagesNotNull(List<Long> hierarchyLevelIdList);
}

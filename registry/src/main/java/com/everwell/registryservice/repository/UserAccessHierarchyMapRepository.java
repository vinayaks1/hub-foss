package com.everwell.registryservice.repository;

import com.everwell.registryservice.models.db.UserAccessHierarchyMap;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserAccessHierarchyMapRepository extends JpaRepository<UserAccessHierarchyMap, Long> {
    List<UserAccessHierarchyMap> findByUserId(Long userId);
}

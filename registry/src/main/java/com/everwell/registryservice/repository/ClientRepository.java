package com.everwell.registryservice.repository;

import com.everwell.registryservice.models.db.Client;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ClientRepository extends JpaRepository<Client, Long> {

    Client findByName(String name);

    Optional<Client> findBySsoAuthorizationCookieName(String cookieName);

}

package com.everwell.registryservice.repository;

import com.everwell.registryservice.models.db.HierarchyConfigMap;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface HierarchyConfigMapRepository extends JpaRepository<HierarchyConfigMap, Long> {

    List<HierarchyConfigMap> getByHierarchyIdInAndConfigMappingIdIn(List<Long> hierarchyIds, List<Long> configMappingIds);

    Integer countByConfigMappingId(Long configMappingId);

    Optional<List<HierarchyConfigMap>> getByHierarchyIdAndConfigMappingId(Long hierarchyId, Long configMappingId);

}

package com.everwell.registryservice.repository;

import com.everwell.registryservice.models.db.Filter;
import org.springframework.data.jpa.repository.JpaRepository;


public interface FilterRepository extends JpaRepository<Filter, Long> {
}

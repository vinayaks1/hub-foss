package com.everwell.registryservice.repository;

import com.everwell.registryservice.models.db.DeploymentDiseaseMap;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DeploymentDiseaseRepository extends JpaRepository<DeploymentDiseaseMap, Long> {
    List<DeploymentDiseaseMap> getAllByDeploymentIdAndStoppedAtIsNull(Long deploymentId);
}

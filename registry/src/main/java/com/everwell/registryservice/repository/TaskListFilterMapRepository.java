package com.everwell.registryservice.repository;

import com.everwell.registryservice.models.db.TaskListFilterMap;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TaskListFilterMapRepository extends JpaRepository<TaskListFilterMap, Long> {
    List<TaskListFilterMap> getAllByDeploymentTaskListId(Long taskListId);

    List<TaskListFilterMap> getAllByDeploymentTaskListIdIn(List<Long> taskListIds);
}

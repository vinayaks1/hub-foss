package com.everwell.registryservice.repository;

import com.everwell.registryservice.models.db.Hierarchy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface HierarchyRepository extends JpaRepository<Hierarchy, Long> {

    String CASE_LEVEL_ID = "\nCASE\n"
            + "    WHEN :level = 1 THEN level_1_id\n"
            + "    WHEN :level = 2 THEN level_2_id\n"
            + "    WHEN :level = 3 THEN level_3_id\n"
            + "    WHEN :level = 4 THEN level_4_id\n"
            + "    WHEN :level = 5 THEN level_5_id\n"
            + "END\n";

    Optional<Hierarchy> findByClientIdAndIdAndActiveTrue(Long clientId, Long hierarchyId);

    List<Hierarchy> findByClientIdAndParentId(Long clientId, Long parentId);

    @Query(value = "select * from hierarchy where " + CASE_LEVEL_ID + " = :parentId", nativeQuery = true)
    Optional<List<Hierarchy>>  findAllDescendants(Long level, Long parentId);

    List<Hierarchy> findByIdInAndClientIdAndActiveTrue(List<Long> hierarchyIdList, Long clientId);

    Hierarchy findByClientIdAndDeploymentIdAndLevelAndActiveTrue(Long clientId, Long deploymentId, Long Level);

    Long countByClientIdAndParentId(Long clientId, Long parentId);

}

package com.everwell.registryservice.repository;

import com.everwell.registryservice.models.db.Trigger;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TriggerRepository extends JpaRepository<Trigger, Long> {
    List<Trigger> getAllByCronTimeNotNullAndActiveTrue();
    List<Trigger> getAllByHierarchyIdAndFunctionNameAndClientIdAndActiveTrue(Long hierarchyId, String functionName, Long clientId);
}

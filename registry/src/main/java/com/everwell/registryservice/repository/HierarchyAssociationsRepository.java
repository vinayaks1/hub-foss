package com.everwell.registryservice.repository;

import com.everwell.registryservice.models.db.HierarchyAssociations;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface HierarchyAssociationsRepository extends JpaRepository<HierarchyAssociations, Long> {
    List<HierarchyAssociations> getByHierarchyId(Long hierarchyId);
}

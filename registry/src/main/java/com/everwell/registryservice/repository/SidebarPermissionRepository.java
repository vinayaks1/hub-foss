package com.everwell.registryservice.repository;

import com.everwell.registryservice.models.db.SidebarPermission;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SidebarPermissionRepository extends JpaRepository<SidebarPermission, Long> {

    List<SidebarPermission> findByDeploymentIdAndDesignationAndIsActiveTrue(Long deploymentId, String designation);

    List<SidebarPermission> findByDesignationAndIsActiveTrue(String designation);
}

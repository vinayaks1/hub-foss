package com.everwell.registryservice.repository;

import com.everwell.registryservice.models.db.FilterValue;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FilterValueRepository extends JpaRepository<FilterValue, Long> {
    List<FilterValue> getAllByFilterId(Long filterId);
}

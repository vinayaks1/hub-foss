package com.everwell.registryservice.service.impl;

import com.everwell.registryservice.exceptions.NotFoundException;
import com.everwell.registryservice.exceptions.ValidationException;
import com.everwell.registryservice.models.db.*;
import com.everwell.registryservice.models.dto.FilterDetails;
import com.everwell.registryservice.models.dto.TaskListItem;
import com.everwell.registryservice.models.http.requests.TaskListItemRequest;
import com.everwell.registryservice.models.http.response.TaskListItemResponse;
import com.everwell.registryservice.repository.*;
import com.everwell.registryservice.service.TaskListService;
import org.springframework.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class TaskListServiceImpl implements TaskListService {
    private static Logger LOGGER = LoggerFactory.getLogger(TaskListServiceImpl.class);

    @Autowired
    private DeploymentTaskListRepository deploymentTaskListRepository;

    @Autowired
    private TaskListFilterMapRepository taskListFilterMapRepository;

    @Autowired
    private TaskListColumnRepository taskListColumnRepository;

    @Autowired
    private TaskListRepository taskListRepository;

    @Autowired
    private FilterRepository filterRepository;

    @Autowired
    private FilterValueRepository filterValueRepository;

    public Long addTaskList(TaskListItemRequest taskListItemRequest, Long deploymentId) {
        DeploymentTaskList newTaskList = new DeploymentTaskList(deploymentId, taskListItemRequest.getTaskListItem());
        Long taskListId = deploymentTaskListRepository.save(newTaskList).getId();
        addTaskListFilters(taskListItemRequest.getFilterValueIds(), taskListId);
        return taskListId;
    }

    public boolean validateTaskList(TaskListItemRequest taskListItemRequest, Long deploymentId) {
        return (
                checkTaskList(taskListItemRequest.getTaskListItem().getTaskListId()) &&
                        checkDisplayColumns(taskListItemRequest.getTaskListItem().getDisplayColumns()) &&
                        checkFilters(taskListItemRequest.getFilterValueIds())
        );
    }

    public boolean taskListAlreadyMappedToDeployment(Long deploymentId, Long taskListId) {
        return !getTaskList(deploymentId, taskListId).isEmpty();
    }

    @Override
    public TaskList getTaskListItem(Long id) {
        return taskListRepository.findById(id).orElse(null);
    }

    @Override
    public List<TaskList> getAllTaskLists() {
        return taskListRepository.findAll();
    }

    public void addTaskListFilters(List<Long> filterValueIds, Long taskListId) {
        if (!CollectionUtils.isEmpty(filterValueIds) && null != taskListId) {
            taskListFilterMapRepository.saveAll(filterValueIds.stream().map(filterValueId -> (new TaskListFilterMap(taskListId, filterValueId))).collect(Collectors.toList()));
        }
    }

    public List<DeploymentTaskList> getTaskList(Long deploymentId, Long taskListId) {
        return deploymentTaskListRepository.getDeploymentTaskListByIdAndDeploymentId(taskListId, deploymentId);
    }

    public DeploymentTaskList getTaskList(Long taskListId) {
        DeploymentTaskList taskList = deploymentTaskListRepository.findById(taskListId).orElse(null);
        if (null == taskList) {
            throw new NotFoundException("Task list with task list id " + taskListId + " not found!");
        }
        return taskList;
    }

    public List<TaskListColumn> getDisplayColumns(List<Long> displayColumnIds) {
        List<TaskListColumn> taskListColumns = new ArrayList<>();
        if (!CollectionUtils.isEmpty(displayColumnIds)) {
            taskListColumns = taskListColumnRepository.findAllById(displayColumnIds);
        }
        return taskListColumns;
    }

    @Override
    @Transactional
    public void updateTaskList(TaskListItemRequest taskListItemRequest, Long taskListId)
    {
        if(
            null == taskListItemRequest.getTaskListItem()
            && null == taskListItemRequest.getFilterValueIds()
        )
        {
            throw new ValidationException("Request cannot be empty");
        }
        
        DeploymentTaskList existingTaskList = deploymentTaskListRepository.findById(taskListId).orElse(null);
        if (null == existingTaskList) {
            throw new NotFoundException("Task list with task list id " + taskListId + " not found!");
        }

        if (null != taskListItemRequest.getTaskListItem()) {
            TaskListItem taskListItem = taskListItemRequest.getTaskListItem();

            boolean isTaskListUpdateRequired = false;

            if (StringUtils.hasText(taskListItem.getDisplayColumns()) && !taskListItem.getDisplayColumns().equals(existingTaskList.getDisplayColumns())) {
                isTaskListUpdateRequired = true;
                String[] displayColumnIdStrings = taskListItem.getDisplayColumns().split(",");

                List<Long> displayColumnIds = Arrays.stream(displayColumnIdStrings).map(Long::parseLong).collect(Collectors.toList());

                List<TaskListColumn> displayColumns = getDisplayColumns(displayColumnIds);
                if (displayColumns.size() != displayColumnIds.size()) {
                    throw new NotFoundException("Display column not found");
                }

                existingTaskList.setDisplayColumns(taskListItem.getDisplayColumns());
            }

            if ((taskListItem.getDisplayOrder() != null) && !taskListItem.getDisplayOrder().equals(existingTaskList.getDisplayOrder())) {
                isTaskListUpdateRequired = true;
                existingTaskList.setDisplayOrder(taskListItem.getDisplayOrder());
            }

            if (isTaskListUpdateRequired) {
                deploymentTaskListRepository.save(existingTaskList);
            }
        }

        if (!CollectionUtils.isEmpty(taskListItemRequest.getFilterValueIds())) {
            if (!checkFilters(taskListItemRequest.getFilterValueIds())) {
                throw new ValidationException("Invalid Filters");
            }

            List<TaskListFilterMap> existingFilterMaps = taskListFilterMapRepository.getAllByDeploymentTaskListId(taskListId);

            List<TaskListFilterMap> filterMapsToDelete = new ArrayList<>();
            for (TaskListFilterMap filterMap : existingFilterMaps) {
                if (!taskListItemRequest.getFilterValueIds().contains(filterMap.getFilterValueId())) {
                    filterMapsToDelete.add(filterMap);
                }
            }

            Map<Long, TaskListFilterMap> existingFilterIdsMap = existingFilterMaps.stream().collect(Collectors.toMap(TaskListFilterMap::getFilterValueId, filterMap -> filterMap));
            List<TaskListFilterMap> filterMapsToAdd = new ArrayList<>();
            for (Long filterValueId : taskListItemRequest.getFilterValueIds()) {
                if (!existingFilterIdsMap.containsKey(filterValueId)) {
                    filterMapsToAdd.add(new TaskListFilterMap(taskListId, filterValueId));
                }
            }

            if (!filterMapsToDelete.isEmpty()) {
                taskListFilterMapRepository.deleteAll(filterMapsToDelete);
            }

            if (!filterMapsToAdd.isEmpty()) {
                taskListFilterMapRepository.saveAll(filterMapsToAdd);
            }
        }
    }

    @Override
    public List<DeploymentTaskList> getTaskListByDeployment(Long deploymentId) {
        return deploymentTaskListRepository.getAllByDeploymentIdOrderByDisplayOrderAscIdAsc(deploymentId);
    }

    public void deleteTaskList(Long taskListId) {
        DeploymentTaskList taskList = deploymentTaskListRepository.findById(taskListId).orElse(null);
        if (null == taskList) {
            throw new NotFoundException("Task list with task list id " + taskListId + " not found!");
        }
        deploymentTaskListRepository.delete(taskList);
    }

    public boolean checkDisplayColumns(String displayColumnIds) {
        List<Long> displayColumnIdList = Arrays.stream(displayColumnIds.split(",")).map(Long::parseLong).collect(Collectors.toList());
        List<Long> allDisplayColumnIds = taskListColumnRepository.getAllTaskListColumnIds();
        return allDisplayColumnIds.containsAll(displayColumnIdList);
    }

    public boolean checkFilters(List<Long> filterValueIds) {
        boolean isValid = true;

        if (null != filterValueIds) {
            List<FilterValue> filterValues = filterValueRepository.findAllById(filterValueIds);
            isValid = (filterValues.size() == filterValueIds.size());
        }

        return isValid;
    }

    public boolean checkTaskList(Long taskListId) {
        return taskListRepository.findById(taskListId).isPresent();
    }

    public List<Filter> getAllFilters() {
        return filterRepository.findAll();
    }

    public List<FilterValue> getAllFilterValues() {
        return filterValueRepository.findAll();
    }

    @Override
    public List<TaskListFilterMap> getTaskListFiltersByTaskListIds(List<Long> taskListIds) {
        return taskListFilterMapRepository.getAllByDeploymentTaskListIdIn(taskListIds);
    }

    public List<FilterDetails> getFilterDetails(List<Long> filterValueIds) {
        Map<Long, Filter> filters = getAllFilters().stream().collect(Collectors.toMap(Filter::getId, f -> f));

        Map<Long, FilterValue> filterValues = getAllFilterValues().stream().collect(Collectors.toMap(FilterValue::getId, fv -> fv));

        return filterValueIds.stream().map(filterValueId -> {
            FilterValue filterValue = filterValues.get(filterValueId);
            if (null == filterValue) {
                throw new NotFoundException("Filter Value not found");
            }
            Filter filter = filters.get(filterValue.getFilterId());
            if (null == filter) {
                throw new NotFoundException("Filter not found");
            }

            return new FilterDetails(filter.getName(), filterValue.getValue(), filterValue.getId());
        }).collect(Collectors.toList());
    }

    public List<TaskListColumn> getAllDisplayColumns() {
        List<TaskListColumn> taskListDisplayColumns = taskListColumnRepository.findAll();
        if (null == taskListDisplayColumns) {
            throw new NotFoundException("Display columns not found!");
        }
        return taskListDisplayColumns;
    }

    @Override
    public List<TaskListItemResponse> getTaskListFilterResponseByDeploymentId(Long deploymentId, boolean isDisplayColumnsRequired) {
        List<DeploymentTaskList> taskLists = getTaskListByDeployment(deploymentId);

        List<Long> taskListIds = taskLists.stream().map(DeploymentTaskList::getId).collect(Collectors.toList());
        List<TaskListFilterMap> filterMaps = getTaskListFiltersByTaskListIds(taskListIds);
        Map<Long, List<FilterDetails>> filterMapsByTaskListId = getFilterMapsByTaskListId(filterMaps);
        Map<Long, TaskList> taskListMap = getAllTaskLists().stream().collect(Collectors.toMap(TaskList::getId, t -> t));

        return getFormattedResponse(taskLists, taskListMap, isDisplayColumnsRequired, filterMapsByTaskListId);
    }

    private Map<Long, List<FilterDetails>> getFilterMapsByTaskListId(List<TaskListFilterMap> filterMaps) {
        Map<Long, List<FilterDetails>> filterMapsByTaskListId = new HashMap<>();

        List<FilterDetails> filterDetails = getFilterDetails(new ArrayList<>(filterMaps.stream().map(TaskListFilterMap::getFilterValueId).collect(Collectors.toSet())));
        Map<Long, FilterDetails> filterDetailsByFilterValueId = filterDetails.stream().collect(Collectors.toMap(FilterDetails::getFilterValueId, f -> f));

        for (TaskListFilterMap filterMap : filterMaps) {
            Long taskListId = filterMap.getDeploymentTaskListId();
            if (!filterMapsByTaskListId.containsKey(taskListId)) {
                filterMapsByTaskListId.put(taskListId, new ArrayList<>());
            }

            filterMapsByTaskListId.get(taskListId).add(filterDetailsByFilterValueId.get(filterMap.getFilterValueId()));
        }
        return filterMapsByTaskListId;
    }

    private List<TaskListItemResponse> getFormattedResponse(List<DeploymentTaskList> taskLists, Map<Long, TaskList> taskListMap, boolean isDisplayColumnsRequired, Map<Long, List<FilterDetails>> filterMapsByTaskListId) {
        return taskLists.stream().map(taskList -> {
            TaskList taskListItem = taskListMap.get(taskList.getTaskListId());
            if (taskListItem == null) {
                throw new NotFoundException("TaskList Item not found");
            }

            TaskListItemResponse taskListItemResponse = new TaskListItemResponse(taskList, taskListItem);

            List<TaskListColumn> displayColumns = new ArrayList<>();
            if (null != taskList.getDisplayColumns() && isDisplayColumnsRequired) {
                List<Long> displayColumnIds = Arrays.stream(taskList.getDisplayColumns().split(",")).map(Long::parseLong).collect(Collectors.toList());
                displayColumns = getDisplayColumns(displayColumnIds);
            }
            taskListItemResponse.setDisplayColumns(displayColumns);

            taskListItemResponse.setFilters(filterMapsByTaskListId.get(taskList.getId()));
            return taskListItemResponse;
        }).collect(Collectors.toList());
    }


}

package com.everwell.registryservice.service.impl;

import com.everwell.registryservice.constants.Constants;
import com.everwell.registryservice.constants.ModuleEnum;
import com.everwell.registryservice.exceptions.InternalServerErrorException;
import com.everwell.registryservice.exceptions.NotFoundException;
import com.everwell.registryservice.models.db.*;
import com.everwell.registryservice.models.dto.EngagementDetails;
import com.everwell.registryservice.models.dto.HierarchyResponseData;
import com.everwell.registryservice.models.http.requests.ins.TriggerDetailsRequest;
import com.everwell.registryservice.models.http.requests.UpdateTriggerDetailsRequest;
import com.everwell.registryservice.exceptions.ValidationException;
import com.everwell.registryservice.models.dto.AddTriggerDetails;
import com.everwell.registryservice.models.http.requests.AddTriggerDetailsRequest;
import com.everwell.registryservice.models.http.requests.ins.TemplateRequest;
import com.everwell.registryservice.models.http.response.AddTriggerResponse;
import com.everwell.registryservice.models.http.response.Response;
import com.everwell.registryservice.models.http.response.TriggersAndTemplatesResponse;
import com.everwell.registryservice.models.http.response.ins.TemplateResponse;
import com.everwell.registryservice.repository.TriggerRepository;
import com.everwell.registryservice.service.*;
import com.everwell.registryservice.repository.VendorMappingRepository;

import org.jobrunr.jobs.annotations.Job;
import org.jobrunr.scheduling.JobScheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TriggerServiceImpl implements TriggerService {

    @Autowired
    TriggerRepository triggerRepository;

    @Autowired
    JobScheduler jobScheduler;

    @Autowired
    INSService insService;

    @Autowired
    ClientService clientService;

    @Autowired
    DeploymentService deploymentService;

    @Autowired
    HierarchyService hierarchyService;

    @Autowired
    EngagementService engagementService;

    @Autowired
    VendorService vendorService;

    @Autowired
    VendorMappingRepository vendorMappingRepository;

    @Scheduled
    public void readTriggerTable()
    {
        List<Trigger> triggers = triggerRepository.getAllByCronTimeNotNullAndActiveTrue();
        triggers.forEach(trigger -> {
            if (Constants.JOB_MODULE_REGISTRY.equals(trigger.getModule()))
            {
                jobScheduler.delete(trigger.getEventName());
                jobScheduler.scheduleRecurrently(trigger.getEventName(), trigger.getCronTime(), () -> invokeFuncFromFunctionName(trigger));
            }
            else
            {
                // TO Do: Episode service Rabbit MQ implementation
            }
        });
    }
    public void invokeFuncFromFunctionName(Trigger trigger) {
        try {
            Class<?> classs = Class.forName(this.getClass().getName());
            // To pass arguments
            // c.getDeclaredMethod(trigger.getFunctionName, ArgumentType);
            // aa.invoke(this, arguments);
            Method method = classs.getDeclaredMethod(trigger.getFunctionName());
            method.invoke(this);
        }
        catch (Exception e)
        {
            // exception will be thrown if function does not exist
            throw new NotFoundException("Method not found " + trigger.getFunctionName());
        }
    }

    @Job
    public void SampleJob()
    {
        System.out.println("Starting Sample Job");
        System.out.println("Sample Job Ended");
    }

    @Override
    public List<TriggersAndTemplatesResponse> getAllTriggers() {
        Client client = clientService.getClientByToken();
        List<Trigger> triggersList = triggerRepository.findAll();
        List<TemplateResponse> templateResponseList = insService.getAllTemplates(client.getId().toString()).getBody().getData();
        List<TriggersAndTemplatesResponse> response = new ArrayList<>();

        triggersList.forEach(trigger -> {
            try {
                TemplateResponse defaultTemplate = templateResponseList.stream()
                        .filter(e -> e.getTemplateId() == trigger.getDefaultTemplateId()).findFirst().orElse(null);
                List<TemplateResponse> linkedTemplates = new ArrayList<>();
                if (!StringUtils.isEmpty(trigger.getTemplateIds())) {
                    Arrays.stream(trigger.getTemplateIds().split(",")).forEach(templateId -> {
                        TemplateResponse templateResponse = templateResponseList.stream()
                                .filter(e -> e.getTemplateId() == Long.valueOf(templateId)).findFirst().orElse(null);
                        if (null != templateResponse)
                            linkedTemplates.add(templateResponse);
                    });
                }
                String deploymentCodeId = hierarchyService.fetchHierarchyById(trigger.getHierarchyId(), client.getId()).getDeploymentId();
                Deployment deployment = deploymentService.get(Long.parseLong(deploymentCodeId));
                response.add(new TriggersAndTemplatesResponse(trigger, deployment.getCode(), defaultTemplate, linkedTemplates));
            } catch (NotFoundException e)
            {
                // Hierarchy Not found - possible reason is either hierarchy is deleted or is inactive
            }
        });
        return response;
    }

    @Override
    public List<TriggerDetailsRequest> getActiveTriggersByFunctionName(Long hierarchyId, String functionName) {
        Client client = clientService.getClientByToken();
        HierarchyResponseData hierarchy = hierarchyService.fetchHierarchyById(hierarchyId, client.getId());
        List<Trigger> triggersList = triggerRepository.getAllByHierarchyIdAndFunctionNameAndClientIdAndActiveTrue(hierarchy.getId(), functionName, client.getId());
        EngagementDetails engagementDetails = engagementService.getEngagementByHierarchy(hierarchy.getId(), true);
        Deployment deployment = deploymentService.get(Long.valueOf(hierarchy.getDeploymentId()));
        List<TriggerDetailsRequest> response = new ArrayList<>();

        for(Trigger trigger : triggersList) {
            long vendorId = vendorService.getVendorIdByDeploymentIdAndTriggerId(deployment.getId(), trigger.getId());

            List<Long> templateIds = new ArrayList<>();
            if (!StringUtils.isEmpty(trigger.getTemplateIds()))
                templateIds = Arrays.stream(trigger.getTemplateIds().split(","))
                                    .map(templateId -> Long.valueOf(templateId)).collect(Collectors.toList());
            response.add(
                new TriggerDetailsRequest(
                        vendorId, trigger.getTriggerId(), trigger.getDefaultTemplateId(),
                        templateIds, trigger.getMandatory(), engagementDetails.getConsentMandatory(),
                        true, engagementDetails.getDefaultTime(), hierarchy.getId(),
                        trigger.getFunctionName(), trigger.getNotificationType()
                )
            );
        }
        if (CollectionUtils.isEmpty(response) && hierarchy.getLevel() != 1)
        {
            return getActiveTriggersByFunctionName(hierarchy.getParentId(), functionName);
        }
        return response;
    }
    @Override
    public void updateTrigger(UpdateTriggerDetailsRequest updateTriggerDetailsRequest){
        Trigger existingTrigger = triggerRepository.findById(updateTriggerDetailsRequest.getId()).orElse(null);

        if(null == existingTrigger){
            throw new NotFoundException("Trigger with id " + updateTriggerDetailsRequest.getId() + " not found!");
        }

        existingTrigger.setTriggerId(updateTriggerDetailsRequest.getTriggerId());

        ResponseEntity<Response<List<TemplateResponse>>> response = insService.getAllTemplates(clientService.getClientByToken().getId().toString());
        if(response.getBody() == null || !response.getBody().isSuccess()){
            throw new NotFoundException(response.getBody().getMessage());
        }
        List<TemplateResponse> templateResponseList = response.getBody().getData();
        TemplateResponse templateResponse = templateResponseList.stream().filter(e -> e.getTemplateId().equals(updateTriggerDetailsRequest.getDefaultTemplateId())).findFirst().orElse(null);

        if(templateResponse == null){
            throw new NotFoundException("Default Template id " + updateTriggerDetailsRequest.getDefaultTemplateId() + " not found!");
        }
        existingTrigger.setDefaultTemplateId(updateTriggerDetailsRequest.getDefaultTemplateId());
        existingTrigger.setTemplateIds(updateTriggerDetailsRequest.getTemplateIds());
        existingTrigger.setCronTime(updateTriggerDetailsRequest.getCronTime());
        existingTrigger.setEventName(updateTriggerDetailsRequest.getEventName());
        existingTrigger.setFunctionName(updateTriggerDetailsRequest.getFunctionName());
        existingTrigger.setActive(updateTriggerDetailsRequest.getIsActive());
        existingTrigger.setMandatory(updateTriggerDetailsRequest.getMandatory());
        existingTrigger.setTimeRelated(updateTriggerDetailsRequest.getEntityTimeRelated());
        existingTrigger.setNotificationType(updateTriggerDetailsRequest.getNotificationType());

        triggerRepository.save(existingTrigger);
    }
    @Override
    public AddTriggerResponse addTrigger(AddTriggerDetailsRequest addTriggerDetailsRequest){
        List<AddTriggerDetails> failedEntries = new ArrayList<>();
        for(AddTriggerDetails addTriggerDetails : addTriggerDetailsRequest.getTriggerDetails()){
            try{
                boolean containsModule = false;
                for(ModuleEnum module: ModuleEnum.values()){
                    if(module.name().equals(addTriggerDetails.getModule())){
                        containsModule = true;
                        break;
                    }
                }
                if(!containsModule){
                    throw new NotFoundException("Module "+ addTriggerDetails.getModule() + " not found");
                }
                if(addTriggerDetails.getIsNewTemplate() && addTriggerDetails.getDefaultTemplateId() != null){
                    throw new ValidationException("DefaultTemplateId already present!");
                }
                if(!addTriggerDetails.getIsNewTemplate() && addTriggerDetails.getDefaultTemplateId() == null){
                    throw new ValidationException("DefaultTemplateId must be provided!");
                }
                if(addTriggerDetails.getDefaultTemplateId() != null){
                    ResponseEntity<Response<List<TemplateResponse>>> response = insService.getAllTemplates(addTriggerDetails.getClientId().toString());
                    if(response.getBody() == null || !response.getBody().isSuccess()){
                        throw new NotFoundException(response.getBody().getMessage());
                    }
                    List<TemplateResponse> templateResponseList = response.getBody().getData();
                    TemplateResponse templateResponse = templateResponseList.stream().filter(e -> e.getTemplateId().equals(addTriggerDetails.getDefaultTemplateId())).findFirst().orElse(null);
                    if(templateResponse == null){
                        throw new NotFoundException("Default Template id " + addTriggerDetails.getDefaultTemplateId() + " not found!");
                    }
                }
                // This will throw an exception if hierarchy is not present and current trigger details will be added in failedEntries
                HierarchyResponseData hierarchy = hierarchyService.fetchHierarchyById(addTriggerDetails.getHierarchyId(), addTriggerDetails.getClientId());
                if(addTriggerDetails.getIsNewTemplate()){
                    TemplateRequest templateRequest = new TemplateRequest(
                            addTriggerDetails.getContent(),
                            addTriggerDetails.getLanguageId(),
                            addTriggerDetails.getTypeId(),
                            addTriggerDetails.getParameters(),
                            addTriggerDetails.getUnicode()
                    );
                    ResponseEntity<Response<Long>> response = insService.saveTemplate(addTriggerDetails.getClientId().toString(),templateRequest);
                    if(response.getBody() == null || !response.getBody().isSuccess()){
                        throw new Exception(response.getBody().getMessage());
                    }
                    Long templateId = response.getBody().getData();
                    addTriggerDetails.setDefaultTemplateId(templateId);
                }
                Trigger trigger = new Trigger();
                trigger.setHierarchyId(addTriggerDetails.getHierarchyId());
                trigger.setClientId(addTriggerDetails.getClientId());
                trigger.setModule(addTriggerDetails.getModule());
                trigger.setTriggerId(addTriggerDetails.getTriggerId());
                trigger.setDefaultTemplateId(addTriggerDetails.getDefaultTemplateId());
                trigger.setTemplateIds(addTriggerDetails.getTemplateIds());
                trigger.setCronTime(addTriggerDetails.getCronTime());
                trigger.setEventName(addTriggerDetails.getEventName());
                trigger.setFunctionName(addTriggerDetails.getFunctionName());
                trigger.setActive(addTriggerDetails.getIsActive());
                trigger.setMandatory(addTriggerDetails.getMandatory());
                trigger.setTimeRelated(addTriggerDetails.getEntityTimeRelated());
                trigger.setNotificationType(addTriggerDetails.getNotificationType());
                Trigger savedTrigger = triggerRepository.save(trigger);

                VendorMapping vendor = new VendorMapping();
                vendor.setDeploymentId(Long.valueOf(hierarchy.getDeploymentId()));
                vendor.setVendorId(addTriggerDetails.getVendorId());
                vendor.setTriggerRegistryId(savedTrigger.getId());
                vendorMappingRepository.save(vendor);
            }
            catch (Exception e){
                failedEntries.add(addTriggerDetails);
            }
        }
        AddTriggerResponse triggerResponse = new AddTriggerResponse(failedEntries);
        return triggerResponse;
    }
}

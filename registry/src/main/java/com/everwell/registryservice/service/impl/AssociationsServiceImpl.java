package com.everwell.registryservice.service.impl;

import com.everwell.registryservice.constants.ValidationStatusEnum;
import com.everwell.registryservice.exceptions.ValidationException;
import com.everwell.registryservice.models.db.AssociationsMaster;
import com.everwell.registryservice.models.db.HierarchyAssociations;
import com.everwell.registryservice.models.dto.HierarchyResponseData;
import com.everwell.registryservice.models.http.response.HierarchyResponse;
import com.everwell.registryservice.repository.AssociationsMasterRepository;
import com.everwell.registryservice.repository.HierarchyAssociationsRepository;
import com.everwell.registryservice.service.AssociationsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class AssociationsServiceImpl implements AssociationsService {

    @Autowired
    private AssociationsMasterRepository associationsMasterRepository;

    @Autowired
    private HierarchyAssociationsRepository hierarchyAssociationsRepository;

    /**
     * API to validate Associations provided in request
     *
     * @param inputAssociations - Input Associations
     */
    @Override
    public void validateInputAssociations(Map<String, Object> inputAssociations) {
        if (!CollectionUtils.isEmpty(inputAssociations)) {
            // The total number of associations received in request
            Integer numberOfAssociationsFromRequest = inputAssociations.size();
            List<String> associationTypes = new ArrayList<>(inputAssociations.keySet());
            // Get count of associations from associations_master table matching with input association types
            Integer associationsInDb = associationsMasterRepository.countByTypeIn(associationTypes);
            // If the input and db association count has mismatched,
            // then either some association does not exist or there is duplication, throw validation error
            if (!Objects.equals(associationsInDb, numberOfAssociationsFromRequest)) {
                throw new ValidationException(ValidationStatusEnum.ASSOCIATIONS_ASSOCIATED_WITH_HIERARCHY_DOES_NOT_EXIST);
            }
        }
    }

    /**
     * API to add associations with provided new Hierarchy
     *
     * @param hierarchyId       - Input Hierarchy ID
     * @param inputAssociations - Associations to be mapped to Hierarchy
     */
    public Map<String, String> addHierarchyAssociations(Long hierarchyId, Map<String, Object> inputAssociations) {
        Map<String, String> mappedHierarchyAssociations = null;
        List<String> inputAssociationTypes = new ArrayList<>(inputAssociations.keySet());
        // Query Associations Master table to find All Associations corresponding to input associations
        List<AssociationsMaster> associationsMasterData = associationsMasterRepository.findByTypeIn(inputAssociationTypes);
        if (!CollectionUtils.isEmpty(associationsMasterData)) {
            // Prepare Hierarchy Associations
            List<HierarchyAssociations> hierarchyAssociations = associationsMasterData.stream().map(association -> {
                HierarchyAssociations hierarchyAssociation = new HierarchyAssociations();
                hierarchyAssociation.setHierarchyId(hierarchyId);
                hierarchyAssociation.setAssociationId(association.getId());
                hierarchyAssociation.setValue(String.valueOf(inputAssociations.get(association.getType())));
                return hierarchyAssociation;
            }).collect(Collectors.toList());
            // Add Hierarchy Associations to DB
            hierarchyAssociationsRepository.saveAll(hierarchyAssociations);
            // Call prepareHierarchyAssociations to prepare response
            mappedHierarchyAssociations = prepareHierarchyAssociations(hierarchyId, associationsMasterData, hierarchyAssociations);
        }
        return mappedHierarchyAssociations;
    }

    /**
     * API to update associations of provided Hierarchy
     *
     * @param hierarchyId       - Input Hierarchy ID
     * @param inputAssociations - Associations to be mapped to Hierarchy
     */
    public Map<String, String> updateHierarchyAssociations(Long hierarchyId, Map<String, Object> inputAssociations) {
        Map<String, String> mappedHierarchyAssociations = null;
        // Query Associations Master table to find All Associations
        List<AssociationsMaster> associationsMasterData = associationsMasterRepository.findAll();
        Set<String> inputAssociationNames = inputAssociations.keySet();
        List<AssociationsMaster> inputAssociationsMasterDataList = associationsMasterData.stream().filter(association -> inputAssociationNames.contains(association.getType())).collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(associationsMasterData)) {
            // Query Hierarchy Associations repository to retrieve currently mapped associations of the hierarchy
            List<HierarchyAssociations> currentHierarchyAssociations = hierarchyAssociationsRepository.getByHierarchyId(hierarchyId);
            Map<Long, HierarchyAssociations> currentHierarchyAssociationsMap = new HashMap<>();
            // Check if any current association present
            if (!CollectionUtils.isEmpty(currentHierarchyAssociations)) {
                // Prepare map of current hierarchy associations
                currentHierarchyAssociations.forEach(hierarchyAssociationMapping -> currentHierarchyAssociationsMap.put(hierarchyAssociationMapping.getAssociationId(), hierarchyAssociationMapping));
            }
            // Prepare Hierarchy Associations
            List<HierarchyAssociations> hierarchyAssociations = inputAssociationsMasterDataList.stream().map(association -> {
                String associationValue = String.valueOf(inputAssociations.get(association.getType()));
                HierarchyAssociations hierarchyAssociation;
                // If association mapping already present then use it
                if (null != currentHierarchyAssociationsMap.get(association.getId())) {
                    hierarchyAssociation = currentHierarchyAssociationsMap.get(association.getId());
                } else { // Create new association mapping
                    hierarchyAssociation = new HierarchyAssociations();
                    hierarchyAssociation.setHierarchyId(hierarchyId);
                    hierarchyAssociation.setAssociationId(association.getId());
                }
                // Set input value for hierarchy association mapping
                hierarchyAssociation.setValue(associationValue);
                return hierarchyAssociation;
            }).collect(Collectors.toList());
            // Add New/Modified Hierarchy Associations to DB
            hierarchyAssociationsRepository.saveAll(hierarchyAssociations);
            // Combine the added, modified and unchanged associations into single list
            List<HierarchyAssociations> hierarchyAssociationsTemp = new ArrayList<>(hierarchyAssociations);
            hierarchyAssociationsTemp.removeAll(currentHierarchyAssociations);
            currentHierarchyAssociations.addAll(hierarchyAssociationsTemp);
            // Call prepareHierarchyAssociations to prepare response
            mappedHierarchyAssociations = prepareHierarchyAssociations(hierarchyId, associationsMasterData, currentHierarchyAssociations);
        }
        return mappedHierarchyAssociations;
    }

    /**
     * API to retrieve Associations mapped to requested Hierarchy
     *
     * @param hierarchyId - Input Hierarchy ID
     * @return Map of associations type and value associated with Hierarchy
     */
    @Override
    public Map<String, String> fetchHierarchyAssociations(Long hierarchyId) {
        return prepareHierarchyAssociations(hierarchyId, null, null);
    }

    /**
     * API to populate provided Hierarchy Response List with Associations mapped to each hierarchy
     *
     * @param hierarchyResponseList - List of Hierarchy Responses containing hierarchies data
     */
    @Override
    public void populateAssociationsForHierarchies(List<HierarchyResponse> hierarchyResponseList) {
        // Fetch All associations Ids and Types from Associations Master in one go instead of fetching for each hierarchy
        List<AssociationsMaster> associationsMasterData = associationsMasterRepository.findAll();
        // Loop over Hierarchy response list for populating associations of each hierarchy response
        hierarchyResponseList.forEach(hierarchyResponse -> {
            // Check if Hierarchy Data present for hierarchyResponse record
            HierarchyResponseData hierarchyData = hierarchyResponse.getHierarchy();
            if (null != hierarchyData) {
                // Fetch and Add Associations mapped to Hierarchy in hierarchyResponse record
                hierarchyResponse.setAssociations(prepareHierarchyAssociations(hierarchyData.getId(), associationsMasterData, null));
            }
        });
    }

    /**
     * Method to retrieve Associations mapped to requested Hierarchy
     *
     * @param hierarchyId            - Input Hierarchy ID
     * @param associationsMasterData - Data of all associations present in associations master
     * @param hierarchyAssociations  - List of all associations mapped to Hierarchy
     * @return Map of associations type and value associated with Hierarchy
     */
    private Map<String, String> prepareHierarchyAssociations(Long hierarchyId, List<AssociationsMaster> associationsMasterData, List<HierarchyAssociations> hierarchyAssociations) {
        Map<String, String> associations = new HashMap<>();
        // Check if Hierarchy Association list passed in request
        if (CollectionUtils.isEmpty(hierarchyAssociations)) {
            // Fetch all associations mapped to hierarchy
            hierarchyAssociations = hierarchyAssociationsRepository.getByHierarchyId(hierarchyId);
        }
        // Check if associations mapped to hierarchy
        if (!CollectionUtils.isEmpty(hierarchyAssociations)) {
            // Check if Associations master data passed in request
            if (CollectionUtils.isEmpty(associationsMasterData)) {
                // Prepare list of hierarchy associations to query master table for type
                List<Long> hierarchyAssociationIds = hierarchyAssociations.stream().map(HierarchyAssociations::getAssociationId).collect(Collectors.toList());
                // Query master table to get types of each association
                associationsMasterData = associationsMasterRepository.findByIdIn(hierarchyAssociationIds);
            }
            // Prepare map of master table records for easy access
            Map<Long, String> hierarchyAssociationMasterMap = new HashMap<>();
            associationsMasterData.forEach(masterRecord -> hierarchyAssociationMasterMap.put(masterRecord.getId(), masterRecord.getType()));
            // Prepare map of hierarchy associations
            hierarchyAssociations.forEach(hierarchyAssociation -> associations.put(hierarchyAssociationMasterMap.get(hierarchyAssociation.getAssociationId()), hierarchyAssociation.getValue()));
        }
        // Return map of associations mapped to hierarchy
        return associations;
    }


}


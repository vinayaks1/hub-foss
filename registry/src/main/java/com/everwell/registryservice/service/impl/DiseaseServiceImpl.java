package com.everwell.registryservice.service.impl;

import com.everwell.registryservice.constants.Constants;
import com.everwell.registryservice.models.dto.DiseaseTemplate;
import com.everwell.registryservice.repository.DeploymentDiseaseRepository;
import com.everwell.registryservice.service.DiseaseService;
import com.everwell.registryservice.service.EpisodeService;
import com.everwell.registryservice.utils.RedisUtils;
import com.everwell.registryservice.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

@Service
public class DiseaseServiceImpl implements DiseaseService {

    @Autowired
    DeploymentDiseaseRepository deploymentDiseaseRepository;
    @Autowired
    EpisodeService episodeService;

    public List<DiseaseTemplate> getDiseaseByDeployment(Long deploymentId, Long clientId) {
        List<DiseaseTemplate> diseaseList = new ArrayList<>();
        deploymentDiseaseRepository.getAllByDeploymentIdAndStoppedAtIsNull(deploymentId)
            .stream().forEach(disease -> {
                DiseaseTemplate diseaseReponse = getDiseaseById(disease.getDiseaseId(), clientId);
                if (diseaseReponse != null && !diseaseList.contains(diseaseReponse)) {
                    diseaseList.add(diseaseReponse);
                }
            });
        return diseaseList;
    }

    public DiseaseTemplate getDiseaseById(Long diseaseId, Long clientId) {
        String cacheKey = Constants.DISEASE_ID + diseaseId + Constants.CLIENT_ID_CACHE_CONFIG + clientId;
        DiseaseTemplate response = null;
        boolean getFromDB = !RedisUtils.hasKey(cacheKey);
        try
        {
            response = Utils.convertObject(RedisUtils.getFromCache(cacheKey), DiseaseTemplate.class);
        }
        catch (Exception ex)
        {
            getFromDB = true;
        }
        if (getFromDB) {
            List<DiseaseTemplate> allDiseases = episodeService.getAllDiseases().getDiseases();
            allDiseases.stream().forEach(disease -> {
                String diseaseCacheKey = Constants.DISEASE_ID + disease.getId() + Constants.CLIENT_ID_CACHE_CONFIG + clientId;
                RedisUtils.putIntoCache(diseaseCacheKey, Utils.asJsonString(disease), (Constants.DAY_LONG_EXPIRY));
            });
            response = allDiseases.stream().filter(disease -> disease.getId() == diseaseId).findAny().orElse(null);
        }
        return response;
    }

    public boolean verifyDiseaseIds(List<Long> diseaseIds, Long clientId) {
        AtomicBoolean allIdsAreValid = new AtomicBoolean(true);
        diseaseIds.forEach(id -> {
            DiseaseTemplate diseaseReponse = getDiseaseById(id, clientId);
            if (diseaseReponse == null) {
                allIdsAreValid.set(false);
            }
        });
        return allIdsAreValid.get();
    }
}

package com.everwell.registryservice.service.impl;

import com.everwell.registryservice.models.http.response.adherence.AdherenceTechnologyDto;
import com.everwell.registryservice.service.AdherenceService;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class AdherenceServiceImpl implements AdherenceService {

    @Override
    public List<AdherenceTechnologyDto> getAllTechnologyOptions() {
        return Arrays.asList(
                new AdherenceTechnologyDto("MERM", "evriMED(MERM)", "MERM"),
                new AdherenceTechnologyDto("VOT", "_VOT", "VOT"),
                new AdherenceTechnologyDto("99D", "_99dots", "99DOTS"),
                new AdherenceTechnologyDto("99DL", "_99dotsLite", "99DOTSLite"),
                new AdherenceTechnologyDto("None", "FollowedWithoutTechnology", "None")
        );
    }
}

package com.everwell.registryservice.service;


import com.everwell.registryservice.models.dto.GenericExchange;
import com.everwell.registryservice.models.dto.ParameterExchange;
import com.everwell.registryservice.models.http.response.Response;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;

import java.util.Map;

public interface DataGatewayService {

    HttpHeaders headers(String clientId);

    void authenticate();

    <T> ResponseEntity<Response<T>> genericExchange(GenericExchange exchange);

    <T> ResponseEntity<Response<T>> getExchange(ParameterExchange exchange);

    <T> ResponseEntity<Response<T>> deleteExchange(ParameterExchange exchange);

    <T> ResponseEntity<Response<T>> postExchange(ParameterExchange exchange);

    <T> ResponseEntity<Response<T>> putExchange(ParameterExchange exchange);

}

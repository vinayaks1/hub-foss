package com.everwell.registryservice.service;

public interface VendorService {
    long getVendorIdByDeploymentIdAndTriggerId(long deploymentId, long triggerId);
}

package com.everwell.registryservice.service;

import com.everwell.registryservice.models.http.requests.ins.TriggerDetailsRequest;
import com.everwell.registryservice.models.http.requests.UpdateTriggerDetailsRequest;
import com.everwell.registryservice.models.http.requests.AddTriggerDetailsRequest;
import com.everwell.registryservice.models.http.response.AddTriggerResponse;
import com.everwell.registryservice.models.http.response.TriggersAndTemplatesResponse;

import java.util.List;

public interface TriggerService {
    void readTriggerTable();
    void SampleJob();

    List<TriggersAndTemplatesResponse> getAllTriggers();
    List<TriggerDetailsRequest> getActiveTriggersByFunctionName(Long hierarchyId, String functionName);

    void updateTrigger(UpdateTriggerDetailsRequest updateTriggerDetailsRequest);

    AddTriggerResponse addTrigger(AddTriggerDetailsRequest addTriggerDetailsRequest);
}

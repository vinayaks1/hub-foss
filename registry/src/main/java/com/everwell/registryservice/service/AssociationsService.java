package com.everwell.registryservice.service;

import com.everwell.registryservice.models.http.response.HierarchyResponse;

import java.util.List;
import java.util.Map;

public interface AssociationsService {

    /**
     * API to validate Associations provided in request
     *
     * @param inputAssociations - Input Associations
     */
    void validateInputAssociations(Map<String, Object> inputAssociations);


    /**
     * API to add associations with provided new Hierarchy
     *
     * @param hierarchyId       - Input Hierarchy ID
     * @param inputAssociations - Associations to be mapped to Hierarchy
     * @return Map of associations type and value associated with Hierarchy
     */
    Map<String, String> addHierarchyAssociations(Long hierarchyId, Map<String, Object> inputAssociations);

    /**
     * API to update associations of provided Hierarchy
     *
     * @param hierarchyId       - Input Hierarchy ID
     * @param inputAssociations - Associations to be mapped to Hierarchy
     * @return Map of associations type and value associated with Hierarchy
     */
    Map<String, String> updateHierarchyAssociations(Long hierarchyId, Map<String, Object> inputAssociations);

    /**
     * API to retrieve Associations mapped to requested Hierarchy
     *
     * @param hierarchyId - Input Hierarchy ID
     * @return Map of associations type and value associated with Hierarchy
     */
    Map<String, String> fetchHierarchyAssociations(Long hierarchyId);

    /**
     * API to populate provided Hierarchy Response List with Associations mapped to each hierarchy
     *
     * @param hierarchyResponseList - List of Hierarchy Responses containing hierarchies data
     */
    void populateAssociationsForHierarchies(List<HierarchyResponse> hierarchyResponseList);
}

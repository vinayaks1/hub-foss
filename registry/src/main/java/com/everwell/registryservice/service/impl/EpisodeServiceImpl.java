package com.everwell.registryservice.service.impl;


import com.everwell.registryservice.constants.EpisodeRoutes;
import com.everwell.registryservice.exceptions.InternalServerErrorException;
import com.everwell.registryservice.exceptions.UnauthorizedException;
import com.everwell.registryservice.exceptions.ValidationException;
import com.everwell.registryservice.models.http.requests.episode.DiseaseTemplateRequest;
import com.everwell.registryservice.models.http.requests.episode.EpisodeHeaderSearchRequest;
import com.everwell.registryservice.models.http.response.ClientResponse;
import com.everwell.registryservice.models.http.response.Response;
import com.everwell.registryservice.models.http.response.episode.HeaderSearchResponse;
import com.everwell.registryservice.service.ClientService;
import com.everwell.registryservice.service.EpisodeService;
import com.everwell.registryservice.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;

import java.io.IOException;
import java.util.Map;

@Service("EpisodeService")
public class EpisodeServiceImpl extends DataGatewayServiceImpl implements EpisodeService {

    public RestTemplate restTemplateForGateway = new RestTemplate();

    protected static HttpEntity httpEntity;

    public static String AUTH_TOKEN = "";

    public static final String AUTH_TOKEN_PREFIX = "Bearer ";

    public static final String AUTH_HEADER = "Authorization";

    public static final String CLIENT_ID_HEADER = "X-CLIENT-ID";

    @Value("${episodeService.url}")
    private String episodeServiceURL;

    @Autowired
    private ClientService clientService;

    @PostConstruct
    public void init() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(AUTH_HEADER, AUTH_TOKEN_PREFIX + AUTH_TOKEN);
        httpHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString());
        httpHeaders.add(CLIENT_ID_HEADER, clientService.getClientByTokenOrDefault().getId().toString());
        httpEntity = new HttpEntity<>(httpHeaders);
    }

    @Override
    public void authenticate() {
        try {
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString());
            httpHeaders.add(CLIENT_ID_HEADER, String.valueOf(clientService.getClientByToken().getId()));
            httpEntity = new HttpEntity<>(httpHeaders);

            ResponseEntity<Map> responseEntity = restTemplateForGateway
                    .exchange(episodeServiceURL + EpisodeRoutes.GET_TOKEN.getPath(), HttpMethod.GET, httpEntity, Map.class);
            Response<ClientResponse> response = (Response<ClientResponse>) Utils.convertObject(Utils.asJsonString(responseEntity.getBody()), (new TypeReference<Response<ClientResponse>>() {}));

            if (response.isSuccess())
            {
                AUTH_TOKEN = response.getData().getAuthToken();
                init();
            }
        }
        catch (Exception e)
        {
            throw new UnauthorizedException("Client Authentication Failed");
        }

    }

    @Override
    public DiseaseTemplateRequest getAllDiseases(){
        int retryCount = 1;
        boolean retry = false;
        ResponseEntity responseEntity = null;
        Response<DiseaseTemplateRequest> response = null;
        do {
            try{
                if(!httpEntity.getHeaders().get(CLIENT_ID_HEADER).get(0).equals(clientService.getClientByToken().getId().toString())){
                    httpEntity.getHeaders().get(AUTH_HEADER).remove(0);
                }
                responseEntity = restTemplateForGateway.exchange(
                        episodeServiceURL + EpisodeRoutes.GET_ALL_DISEASES.getPath(),
                        HttpMethod.GET, httpEntity, Map.class);
                response = (Response<DiseaseTemplateRequest>)Utils.convertObject(
                        Utils.asJsonString(responseEntity.getBody()),
                        (new TypeReference<Response<DiseaseTemplateRequest>>() {}));
                retry = false;
            } catch (Exception ex) {
                authenticate();
                retry = true;
            }
        } while (retry && retryCount-- > 0);
        return response.getData();
    }

    @Override
    public HeaderSearchResponse headerSearch(EpisodeHeaderSearchRequest headerSearchRequest) {
        int retryCount = 1;
        boolean retry = false;
        ResponseEntity responseEntity = null;
        Response<HeaderSearchResponse> response = null;
        do {
            try{
                responseEntity = restTemplateForGateway.exchange(
                        episodeServiceURL + EpisodeRoutes.POST_HEADER_SEARCH.getPath(),
                        HttpMethod.POST, new HttpEntity<>(headerSearchRequest, httpEntity.getHeaders()), Map.class);
                response = (Response<HeaderSearchResponse>)Utils.convertObject(
                        Utils.asJsonString(responseEntity.getBody()),
                        (new TypeReference<Response<HeaderSearchResponse>>() {}));
                retry = false;
            } catch (HttpClientErrorException ex) {
                if (ex.getStatusCode() == HttpStatus.UNAUTHORIZED) {
                    authenticate();
                    retry = true;
                }
            } catch (Exception e) {
                throw new InternalServerErrorException("Error with episode search api");
            }
        } while (retry && retryCount-- > 0);
        return response.getData();
    }
}
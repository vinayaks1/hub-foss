package com.everwell.registryservice.service;

import com.everwell.registryservice.models.db.Deployment;
import com.everwell.registryservice.models.http.requests.CreateDeploymentRequest;
import com.everwell.registryservice.models.http.response.PrefixResponse;
import com.everwell.registryservice.models.http.response.ProjectListEntry;

import java.util.List;

public interface DeploymentService
{
    Deployment getByCode(String code);

    Deployment get(Long id);

    List<Deployment> getAll();

    Deployment create(CreateDeploymentRequest request);

    List<ProjectListEntry> projectList();

    List<PrefixResponse> getPrefix();
}

package com.everwell.registryservice.service;

import com.everwell.registryservice.models.db.*;
import com.everwell.registryservice.models.dto.FilterDetails;
import com.everwell.registryservice.models.http.requests.TaskListItemRequest;
import com.everwell.registryservice.models.http.response.TaskListItemResponse;

import java.util.List;

public interface TaskListService {
    Long addTaskList(TaskListItemRequest taskListItemRequest, Long deploymentId);

    DeploymentTaskList getTaskList(Long taskListId);

    List<DeploymentTaskList> getTaskList(Long deploymentId, Long taskListId);

    List<TaskListColumn> getDisplayColumns(List<Long> displayColumnIds);

    void updateTaskList(TaskListItemRequest taskListItemRequest, Long taskListId);

    List<DeploymentTaskList> getTaskListByDeployment(Long deploymentId);

    void deleteTaskList(Long taskListId);

    List<TaskListFilterMap> getTaskListFiltersByTaskListIds(List<Long> taskListIds);

    boolean checkDisplayColumns(String displayColumnIds);

    boolean validateTaskList(TaskListItemRequest taskListItemRequest, Long deploymentId);

    boolean taskListAlreadyMappedToDeployment(Long deploymentId, Long taskListId);

    TaskList getTaskListItem(Long id);

    List<TaskList> getAllTaskLists();

    List<FilterDetails> getFilterDetails(List<Long> filterValueIds);

    List<Filter> getAllFilters();

    List<FilterValue> getAllFilterValues();

    List<TaskListColumn> getAllDisplayColumns();

    List<TaskListItemResponse> getTaskListFilterResponseByDeploymentId(Long deploymentId, boolean isDisplayColumnsRequired);

}

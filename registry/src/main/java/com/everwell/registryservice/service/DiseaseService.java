package com.everwell.registryservice.service;

import com.everwell.registryservice.models.dto.DiseaseTemplate;

import java.util.List;

public interface DiseaseService {
    // get all diseases mapped for any deployment using deployment id
    List<DiseaseTemplate> getDiseaseByDeployment(Long deploymentId, Long clientId);
    // check if input disease ids are valid or not
    boolean verifyDiseaseIds(List<Long> diseaseIds, Long clientId);
    // get disease details by id
    DiseaseTemplate getDiseaseById(Long diseaseId, Long clientId);
}

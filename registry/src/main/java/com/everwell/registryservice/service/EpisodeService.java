package com.everwell.registryservice.service;

import com.everwell.registryservice.models.http.requests.episode.DiseaseTemplateRequest;
import com.everwell.registryservice.models.http.requests.episode.EpisodeHeaderSearchRequest;
import com.everwell.registryservice.models.http.response.episode.HeaderSearchResponse;

public interface EpisodeService {

    // Api Call to Episode to get Auth Token
    void authenticate() throws Exception;
    // Api Call to Episode to get list of all diseases
    DiseaseTemplateRequest getAllDiseases();

    /**
     * Api to invoke episode header search
     * @param headerSearchRequest
     * @return search response including the minified details of the episodes matched
     */
    HeaderSearchResponse headerSearch(EpisodeHeaderSearchRequest headerSearchRequest);
}
package com.everwell.registryservice.service;

import com.everwell.registryservice.models.db.Client;
import com.everwell.registryservice.models.db.Deployment;
import com.everwell.registryservice.models.db.UserAccess;
import com.everwell.registryservice.models.dto.SidebarItemResponse;
import com.everwell.registryservice.models.dto.UserSummaryDto;
import com.everwell.registryservice.models.dto.UnifiedListFilterResponse;
import com.everwell.registryservice.models.http.requests.CreateUserRequest;
import com.everwell.registryservice.models.http.response.HeaderResponse;
import com.everwell.registryservice.models.http.response.adherence.AdherenceTechnologyDto;
import org.springframework.data.util.Pair;

import java.util.List;

/**
 * Class Type - Service Interface
 * Purpose - Provide business logic for user access management
 * Scope - Modules interacting with user access
 *
 * @author Ashish Shrivastava
 */
public interface UserService {

    /**
     * API to create new user in system
     *
     * @param createUserRequest - Details of user to be created
     * @param clientId - client against which the user exists
     * @return Created User Details
     */
    UserAccess addNewUser(CreateUserRequest createUserRequest, Long clientId);

    /**
     * API to fetch user data from DB
     *
     * @param ssoId - SSO ID to be searched
     * @return User Data
     */
    UserAccess fetchUserData(Long ssoId);

    /**
     * API to fetch all associated hierarchies of the user
     *
     * @param userid - Input user id
     * @return List of associated hierarchy Ids
     */
    List<Long> fetchHierarchiesAssociatedWithUser(Long userid);

    /**
     * API to soft delete a user from system
     *
     * @param ssoId - Input SSO ID to be deleted
     * @return Whether deletion is successful
     */
    Boolean deleteUser(Long ssoId);

    /**
     * Function to fetch the user and client based on the token
     *
     * @return An object containing both client and user
     */
    Pair<Client, UserAccess> getUserClientByToken();

    /**
     * Function to build the header details with user access dependent options
     *
     * @param userAccess - The user access object from the token
     * @param headerResponse - The header response object to be filled with logged in user data
     * @return An object with added user specific header details
     */
    HeaderResponse buildUserHeaderDetails(UserAccess userAccess, HeaderResponse headerResponse);

    /**
     * Function to fetch all the sidebar items mapped based on the user
     *
     * @param userAccess - User access object from the token
     * @param deployment - Deployment object of the logged in user
     * @return A list of sidebar items mapped based on the role
     */
    List<SidebarItemResponse> getSidebarItems(UserAccess userAccess, Deployment deployment);

    /**
     * Function to fetch the user summary response based on the the hierarchyId
     *
     * @param hierarchyId - HierarchyId to be evaluated for the summary response
     * @return A list of sidebar items mapped based on the role
     */
    List<UserSummaryDto> getUserSummaryResponseByHierarchyId(Long hierarchyId);

    /**
     * Function to fetch the filter response options mapped based on the user
     *
     * @return An object of unified filters
     */
    UnifiedListFilterResponse getUppFilterOptions(UserAccess userAccess, List<AdherenceTechnologyDto> adherenceTechnologyDtoList);
}

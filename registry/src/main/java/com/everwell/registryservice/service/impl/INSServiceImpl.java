package com.everwell.registryservice.service.impl;

import com.everwell.registryservice.enums.Routes;
import com.everwell.registryservice.models.dto.GenericExchange;
import com.everwell.registryservice.models.dto.ParameterExchange;
import com.everwell.registryservice.models.http.requests.ins.TemplateRequest;
import com.everwell.registryservice.models.http.response.Response;
import com.everwell.registryservice.models.http.response.ins.LanguageResponse;
import com.everwell.registryservice.models.http.response.ins.TemplateResponse;
import com.everwell.registryservice.service.INSService;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.List;

@Service
public class INSServiceImpl extends DataGatewayServiceImpl implements INSService {

    @Override
    public ResponseEntity<Response<List<TemplateResponse>>> getAllTemplates(String clientId) {
        String endPoint = Routes.INS_TEMPLATE_V1.getRoute();
        ParameterExchange exchange = new ParameterExchange(clientId, endPoint, new ParameterizedTypeReference<Response<List<TemplateResponse>>>() {
        }, null, null);
        return getExchange(exchange);
    }

    @Override
    public ResponseEntity<Response<Long>> saveTemplate(String clientId, @Valid TemplateRequest templateRequest) {
        String endPoint = Routes.INS_TEMPLATE_V1.getRoute();
        ParameterExchange exchange = new ParameterExchange(clientId, endPoint, templateRequest, new ParameterizedTypeReference<Response<Long>>() {
        });
        return postExchange(exchange);
    }

    @Override
    public ResponseEntity<Response<LanguageResponse>> getAllLanguages(String clientId){
        String endPoint = Routes.INS_LANGUAGES_V1.getRoute();
        ParameterExchange exchange = new ParameterExchange(clientId, endPoint, new ParameterizedTypeReference<Response<LanguageResponse>>(){
        }, null, null);
        return getExchange(exchange);
    }
}

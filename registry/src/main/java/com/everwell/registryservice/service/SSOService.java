package com.everwell.registryservice.service;

import com.everwell.registryservice.models.http.requests.sso.RegistrationRequest;
import com.everwell.registryservice.models.http.response.Response;
import com.everwell.registryservice.models.http.response.sso.LoginResponse;
import com.everwell.registryservice.models.http.response.sso.UserSSOResponse;
import org.springframework.http.ResponseEntity;

import java.util.Map;

public interface SSOService {

    ResponseEntity<Response<LoginResponse>> login(String clientId, Map<String, Object> loginRequest);

    UserSSOResponse registration(String clientId, RegistrationRequest registrationRequest);

    Boolean logout(String clientId, Map<String, Object> logoutRequest);

}

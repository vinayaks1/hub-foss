package com.everwell.registryservice.service.impl;

import com.everwell.registryservice.exceptions.ConflictException;
import com.everwell.registryservice.exceptions.IllegalArgumentException;
import com.everwell.registryservice.exceptions.NotFoundException;
import com.everwell.registryservice.models.db.Deployment;
import com.everwell.registryservice.models.db.DeploymentDiseaseMap;
import com.everwell.registryservice.models.db.DeploymentLanguageMap;
import com.everwell.registryservice.models.http.requests.CreateDeploymentRequest;
import com.everwell.registryservice.models.http.response.PrefixResponse;
import com.everwell.registryservice.models.http.response.ProjectListEntry;
import com.everwell.registryservice.repository.DeploymentDiseaseRepository;
import com.everwell.registryservice.repository.DeploymentLanguageRepository;
import com.everwell.registryservice.repository.DeploymentRepository;
import com.everwell.registryservice.service.DeploymentService;
import com.everwell.registryservice.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DeploymentServiceImpl implements DeploymentService
{
    private static Logger LOGGER = LoggerFactory.getLogger(DeploymentServiceImpl.class);

    @Autowired
    private DeploymentRepository deploymentRepository;

    @Autowired
    private DeploymentLanguageRepository deploymentLanguageRepository;

    @Autowired
    private DeploymentDiseaseRepository deploymentDiseaseRepository;


    @Value("${video-server.url}")
    private String videoServerUrl;

    @Value("${client.base-url}")
    private String clientBaseUrl;

    public Deployment getByCode(String code)
    {
        if(!StringUtils.hasText(code))
        {
            throw new IllegalArgumentException("Code cannot be empty");
        }

        Deployment deployment = this.deploymentRepository.getDeploymentByCode(code);
        if(null == deployment)
        {
            throw new NotFoundException("Deployment code not found");
        }

        return deployment;
    }

    public Deployment get(Long id)
    {
        if(null == id)
        {
            throw new IllegalArgumentException("Id cannot be empty");
        }

        if(id <= 0)
        {
            throw new IllegalArgumentException("Invalid Id");
        }

        Deployment deployment = this.deploymentRepository.getDeploymentById(id);
        if(null == deployment)
        {
            throw new NotFoundException("Deployment code not found");
        }

        return deployment;
    }

    @Override
    public List<Deployment> getAll()
    {
        return this.deploymentRepository.findAll();
    }

    @Override
    public List<PrefixResponse> getPrefix() {
        List<PrefixResponse> prefixResponse = new ArrayList<>();
        getAll().forEach(deployment -> {
            String countryWithPrefix = String.format("%s (+%d)",deployment.getCountryName(), deployment.getPhonePrefix());
            if (prefixResponse.stream().noneMatch(m -> m.getValue().contains(countryWithPrefix)))
                prefixResponse.add(new PrefixResponse(deployment.getCode(), countryWithPrefix));
        });
        return prefixResponse;
    }

    @Override
    @Transactional
    public Deployment create(CreateDeploymentRequest request)
    {
        Boolean alreadyExists = this.deploymentRepository.existsAllByCode(request.getCode());
        if(alreadyExists)
        {
            throw new ConflictException("Deployment already exists");
        }

        Boolean alreadyExistsCountryProjectCombination = this.deploymentRepository.existsAllByCountryNameAndProjectName
                (request.getCountryName(), request.getProjectName());

        if(alreadyExistsCountryProjectCombination)
        {
            throw new ConflictException("Deployment already exists for given country project combination");
        }

        Deployment deployment = new Deployment(request);
        this.deploymentRepository.save(deployment);

        LocalDateTime now = Utils.getCurrentDate();
        this.deploymentLanguageRepository.saveAll(
            request.getAllowedLanguages()
                .stream()
                .distinct()
                .map(languageId -> new DeploymentLanguageMap(deployment.getId(), languageId, now))
                .collect(Collectors.toList())
        );

        this.deploymentDiseaseRepository.saveAll(
            request.getDiseaseMappings()
                .stream()
                .distinct()
                .map(diseaseId -> new DeploymentDiseaseMap(deployment.getId(), diseaseId))
                .collect(Collectors.toList())
        );

        return deployment;
    }

    @Override
    public List<ProjectListEntry> projectList()
    {
        List<Deployment> deployments = this.deploymentRepository.getAllByCountryNameNotNullAndProjectNameNotNull();

        return deployments
            .stream()
            .map(deployment -> new ProjectListEntry(deployment, videoServerUrl, clientBaseUrl))
            .collect(Collectors.toList());
    }
}

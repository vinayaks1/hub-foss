package com.everwell.registryservice.models.db;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "hierarchy_config_map")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class HierarchyConfigMap {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "hierarchy_id")
    private Long hierarchyId;
    @Column(name = "config_mapping_id")
    private Long configMappingId;
    private String value;
    private boolean active;
    @CreationTimestamp
    @Column(name = "created_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime createdAt;

    public HierarchyConfigMap(Long hierarchyId, Long configMappingId, String value) {
        this.hierarchyId = hierarchyId;
        this.configMappingId = configMappingId;
        this.value = value;
        this.active = true;
    }
}

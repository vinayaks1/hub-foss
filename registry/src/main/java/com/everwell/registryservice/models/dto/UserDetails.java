package com.everwell.registryservice.models.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDetails {
    private String username;
    private long hierarchyId;
    private boolean selectiveHierarchyMapping;
    private long ssoId;
    private boolean active;
}

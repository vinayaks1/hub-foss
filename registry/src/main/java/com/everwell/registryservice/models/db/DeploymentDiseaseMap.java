package com.everwell.registryservice.models.db;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "deployment_disease_map")
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class DeploymentDiseaseMap {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private Long deploymentId;

    private Long diseaseId;

    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    @CreationTimestamp
    private LocalDateTime createdAt;

    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    private LocalDateTime stoppedAt;

//    To-Do: Use Map Struct
    public DeploymentDiseaseMap(Long deploymentId, Long diseaseId)
    {
        this.deploymentId = deploymentId;
        this.diseaseId = diseaseId;
    }

}

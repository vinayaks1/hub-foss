package com.everwell.registryservice.models.http.requests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserUpdateHierarchyRequest {
    @NotNull(message = "Hierarchy Id must be provided")
    private Long hierarchyId;
    private String name;
    private Boolean active;
    private Long parentId;
    private String code;
    private boolean hasMERM;
    private boolean hasVOT;
    private boolean has99D;
    private boolean has99DL;
    private boolean hasNone;
}

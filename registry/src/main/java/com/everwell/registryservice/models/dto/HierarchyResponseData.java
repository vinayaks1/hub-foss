package com.everwell.registryservice.models.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class HierarchyResponseData {
    private Long id;
    private Long level;
    private String name;
    private String type;
    private String code;
    private Long parentId;
    private Long level1Id;
    private String level1Name;
    private String level1Type;
    private String level1Code;
    private Long level2Id;
    private String level2Name;
    private String level2Type;
    private String level2Code;
    private Long level3Id;
    private String level3Name;
    private String level3Type;
    private String level3Code;
    private Long level4Id;
    private String level4Name;
    private String level4Type;
    private String level4Code;
    private Long level5Id;
    private String level5Name;
    private String level5Type;
    private String level5Code;
    private Long level6Id;
    private String level6Name;
    private String level6Type;
    private String level6Code;
    private boolean hasChildren;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private boolean active;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private String mergeStatus;
    private Long clientId;
    private String deploymentId;
}

package com.everwell.registryservice.models.http.requests.episode;

import com.everwell.registryservice.models.dto.DiseaseTemplate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DiseaseTemplateRequest {
    List<DiseaseTemplate> diseases;
}

package com.everwell.registryservice.models.http.requests;

import com.everwell.registryservice.constants.FieldValidationMessages;
import com.everwell.registryservice.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateUserRequest {
    @NotBlank(message = FieldValidationMessages.USER_NAME_MANDATORY)
    private String username;
    @NotNull(message = FieldValidationMessages.HIERARCHY_ID_MANDATORY)
    private Long hierarchyId;
    private Long ssoId;
    private Boolean selectiveHierarchyMapping;
    private String password;
    private boolean ssoUserToBeCreated = false;

    public void validateSsoCreation() {
        if(ssoUserToBeCreated) {
            if(StringUtils.isEmpty(password)) {
                throw new ValidationException(FieldValidationMessages.PASSWORD_MANDATORY);
            }
        } else {
            if(null == ssoId) {
                throw new ValidationException(FieldValidationMessages.SSO_ID_MANDATORY);
            }
        }
    }
}

package com.everwell.registryservice.models.http.requests;

import com.everwell.registryservice.constants.FieldValidationMessages;
import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UpdateLanguageRequest {
    @Valid
    @NotNull(message = FieldValidationMessages.HIERARCHY_ID_MANDATORY)
    private Long hierarchyId;

    @Valid
    @NotEmpty(message = FieldValidationMessages.LANGUAGE_ID_MANDATORY)
    private List<Long> languageIds;

    private List<Long> disabledLanguageIds;
}

package com.everwell.registryservice.models.http.requests;

import com.everwell.registryservice.exceptions.ValidationException;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class SearchRequest {

    private String searchKey;

    private String searchValue;

    public void validate() {
        if (StringUtils.isEmpty(searchKey) || StringUtils.isEmpty(searchValue))
            throw new ValidationException("Search key and value must be present!");
    }
}

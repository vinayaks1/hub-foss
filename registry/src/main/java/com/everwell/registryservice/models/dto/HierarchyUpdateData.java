package com.everwell.registryservice.models.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HierarchyUpdateData {
    @NotNull(message = "Hierarchy Id must be provided")
    private Long hierarchyId;
    private String name;
    private Boolean active;
    private Long parentId;
    private String code;
}

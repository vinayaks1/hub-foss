package com.everwell.registryservice.models.db;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "task_list_filter_map")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TaskListFilterMap {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long deploymentTaskListId;
    private Long filterValueId;

    public TaskListFilterMap(Long deploymentTaskListId, Long filterValueId) {
        this.deploymentTaskListId = deploymentTaskListId;
        this.filterValueId = filterValueId;
    }
}

package com.everwell.registryservice.models.http.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ConfigHierarchyMappingRecordResponse {
    private Long hierarchyId;
    private String configName;
    private String value;
    private boolean active;
    private boolean multiMapped;
    private List<String> values;

}

package com.everwell.registryservice.models.db;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "hierarchy_associations")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class HierarchyAssociations {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "hierarchy_id")
    private Long hierarchyId;
    @Column(name = "association_id")
    private Long associationId;
    private String value;
}

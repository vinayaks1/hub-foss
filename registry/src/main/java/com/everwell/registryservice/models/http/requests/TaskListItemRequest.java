package com.everwell.registryservice.models.http.requests;

import com.everwell.registryservice.exceptions.ValidationException;
import com.everwell.registryservice.models.dto.FilterDetails;
import com.everwell.registryservice.models.dto.TaskListItem;
import lombok.*;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class TaskListItemRequest {

    private TaskListItem taskListItem;

    private List<Long> filterValueIds;

    public void validate(TaskListItemRequest taskListItemRequest) {
        if (null == taskListItemRequest.getTaskListItem())
            throw new ValidationException("task list item is required");
        taskListItemRequest.getTaskListItem().validate(taskListItemRequest.getTaskListItem());

        if((null == filterValueIds) || filterValueIds.isEmpty())
        {
            throw new ValidationException("filters are required");
        }
    }
}

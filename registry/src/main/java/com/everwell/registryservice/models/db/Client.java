package com.everwell.registryservice.models.db;

import com.everwell.registryservice.utils.Utils;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "client")
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String password;

    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    private LocalDateTime createdDate;

    private Long eventFlowId;

    @Setter
    private String askforhelpurl;

    @Setter
    private String ssoUrl;

    private String ssoAuthorizationCookieName;

    public Client(String name, String password, Long eventFlowId, String askforhelpurl) {
        this.name = name;
        this.password = password;
        this.createdDate = Utils.getCurrentDate();
        this.eventFlowId = eventFlowId;
        this.askforhelpurl = askforhelpurl;
    }

    public Client(String name, String password) {
        this.name = name;
        this.password = password;
        this.createdDate = Utils.getCurrentDate();
    }

    public Client(Long id, String name, String password, LocalDateTime createdDate) {
        this(name, password, null, null);
        this.id = id;
        this.createdDate = createdDate;
    }
}

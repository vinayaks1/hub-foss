package com.everwell.registryservice.models.db;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "task_list_column")
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class TaskListColumn {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String label;
    private Boolean sortable;

}

package com.everwell.registryservice.models.http.response.episode;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HeaderSearchResponse {
    private Long totalHits;
    private List<Map<String, Object>> episodeIndexList;
}

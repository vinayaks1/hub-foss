package com.everwell.registryservice.models.http.response;

import com.everwell.registryservice.models.dto.UserDetails;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDetailsResponse {
    private UserDetails user;
    private List<HierarchyResponse> hierarchy;
}

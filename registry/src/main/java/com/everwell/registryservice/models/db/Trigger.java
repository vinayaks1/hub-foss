package com.everwell.registryservice.models.db;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "trigger")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Trigger {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "hierarchy_id")
    private Long hierarchyId;
    @Column(name = "client_id")
    private Long clientId;
    @Column(name = "trigger_id")
    private Long triggerId;
    @Column(name = "default_template_id")
    private Long defaultTemplateId;
    @Column(name = "template_ids")
    private String templateIds;
    @Column(name = "event_name")
    private String eventName;
    @Column(name = "function_name")
    private String functionName;
    @Column(name = "cron_time")
    private String cronTime;
    @Column(name = "time_related")
    private Boolean timeRelated;
    @Column(name = "notification_type")
    private String notificationType;
    private String module;
    private Boolean mandatory;
    private Boolean active;
}

package com.everwell.registryservice.models.http.requests;

import com.everwell.registryservice.models.dto.EngagementDetails;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddEngagementRequest {
    @Valid
    @NotNull(message = "Engagement Details must be Provided")
    private List<EngagementDetails> engagementDetails;
}

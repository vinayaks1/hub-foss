package com.everwell.registryservice.models.http.response;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
@NoArgsConstructor
@Getter
public class Response<T> implements Serializable {
    boolean success;
    String message;
    T data;

    public Response(String message) {
        // All error responses
        success = false;
        this.message = message;
    }

    public Response(boolean success, T data, String message) {
        // All error responses
        this.success = success;
        this.message = message;
        this.data = data;
    }

    public Response(T data, String message) {
        // All success responses
        this(true, data, message);
    }

    public Response(boolean success, T data) {
        this(success, data, "");
    }
}

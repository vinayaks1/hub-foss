package com.everwell.registryservice.models.db;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "form_part")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FormPart {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "part_name")
    private String partName;
    @Column(name = "part_label")
    private String partLabel;
    @Column(name = "part_type")
    private String partType;
    private Long position;
    private Long rows;
    private Long columns;
    @Column(name = "allow_row_open")
    private boolean allowRowOpen;
    @Column(name = "allow_row_delete")
    private boolean allowRowDelete;
    @Column(name = "list_item_title")
    private String listItemTitle;
    @Column(name = "item_description_field")
    private String itemDescriptionField;
    @Column(name = "row_delete_text")
    private String rowDeleteText;
}

package com.everwell.registryservice.models.http.requests.episode;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EpisodeSearchRequest {
    private Integer pageSize;
    private Integer page;
    private List<String> fieldsToShow;
    private List<String> typeOfEpisode;
}

package com.everwell.registryservice.models.http.requests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public abstract class HierarchyRequest {
    private Map<String, Object> associations;
    private Map<String, Object> configs;
}

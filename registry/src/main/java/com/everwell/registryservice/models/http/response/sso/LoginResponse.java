package com.everwell.registryservice.models.http.response.sso;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Getter
public class LoginResponse {
    private Boolean isUserCredentialsValid;

    private Boolean pendingPasswordReset;

    @Setter
    private String sessionId;
}

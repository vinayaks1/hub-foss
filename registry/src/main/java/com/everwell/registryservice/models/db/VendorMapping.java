package com.everwell.registryservice.models.db;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "vendor_mapping")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class VendorMapping {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "deployment_id")
    private Long deploymentId;
    @Column(name = "vendor_id")
    private Long vendorId;
    @Column(name = "trigger_registry_id")
    private Long triggerRegistryId;
}

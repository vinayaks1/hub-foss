package com.everwell.registryservice.models.db;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "field")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Field {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "field_name")
    private String fieldName;
    private String label;
    private String component;
    @Column(name = "field_options")
    private String fieldOptions;
    private boolean disabled;
    private boolean visible;
    private boolean required;
    @Column(name = "default_value")
    private String defaultValue;
    @Column(name = "validation_list")
    private String validationList;
    @Column(name = "row_number")
    private Long rowNumber;
    @Column(name = "column_number")
    private Long columnNumber;
    @Column(name = "remote_url")
    private String remoteUrl;
    private String config;
    private String type;
}

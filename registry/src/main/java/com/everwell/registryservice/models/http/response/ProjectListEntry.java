package com.everwell.registryservice.models.http.response;

import com.everwell.registryservice.models.db.Deployment;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProjectListEntry
{
    @JsonProperty(value = "CountryName")
    private String countryName;
    @JsonProperty(value = "BaseUrl")
    private String baseUrl;
    @JsonProperty(value = "VideoServerUrl")
    private String videoServerUrl;
    @JsonProperty(value = "ProjectName")
    private String projectName;
    @JsonProperty(value = "AskForHelpUrl")
    private String askForHelpUrl;
    @JsonProperty(value = "DeploymentCode")
    private String deploymentCode;


    public ProjectListEntry(Deployment deployment, String videoServerUrl, String baseUrl)
    {
        this.countryName = deployment.getCountryName();
        this.projectName = deployment.getProjectName();
        this.askForHelpUrl = deployment.getAskForHelpUrl();
        this.videoServerUrl = videoServerUrl;
        this.baseUrl = baseUrl;
        this.deploymentCode = deployment.getCode();
    }
}

package com.everwell.registryservice.models.http.requests;

import com.everwell.registryservice.constants.FieldValidationMessages;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetConfigHierarchyMappingsByIdRequest {
    @NotEmpty(message = FieldValidationMessages.CONFIG_MAPPING_ID_MANDATORY)
    private List<Long> mappingIds;
}

package com.everwell.registryservice.models.http.response;

import com.everwell.registryservice.models.dto.SidebarItemResponse;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SidebarResponse {

    private List<SidebarItemResponse> sidebarItemsList;
}

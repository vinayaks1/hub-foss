package com.everwell.registryservice.models.dto;

import com.everwell.registryservice.constants.FieldValidationMessages;
import lombok.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HierarchyRequestData {
    @NotBlank(message = FieldValidationMessages.HIERARCHY_NAME_MANDATORY)
    private String name;
    @NotBlank(message = FieldValidationMessages.HIERARCHY_CODE_MANDATORY)
    private String code;
    @NotNull(message = FieldValidationMessages.HIERARCHY_LEVEL_MANDATORY)
    @Min(value = 1, message = FieldValidationMessages.HIERARCHY_LEVEL_LESSER_THAN_SUPPORTED)
    @Max(value = 6, message = FieldValidationMessages.HIERARCHY_LEVEL_GREATER_THAN_SUPPORTED)
    private Long level;
    @NotBlank(message = FieldValidationMessages.HIERARCHY_TYPE_MANDATORY)
    private String type;
    private Long parentId;
    @Getter(AccessLevel.NONE)
    private boolean hasChildren;
    private String mergeStatus;

    public Boolean hasChildren() {
        return this.hasChildren;
    }
}

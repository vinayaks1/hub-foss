package com.everwell.registryservice.models.http.requests.ins;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class TriggerDefaultRequest implements Serializable {
    Long vendorId;
    Long triggerId;
    Long templateId;
    List<Long> templateIds;
    Boolean isMandatory;
    Boolean defaultConsent;
    Boolean isDefaultTime;
    String timeOfSms;
}

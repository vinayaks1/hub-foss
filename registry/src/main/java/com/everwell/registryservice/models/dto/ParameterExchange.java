package com.everwell.registryservice.models.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.lang.Nullable;

import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ParameterExchange {
    private String clientId;
    private String endPoint;
    private ParameterizedTypeReference typeReference;
    @Nullable
    private Long pathVariable;
    @Nullable
    private Map<String, Object> queryParams;
    @Nullable
    private Object body;

    public ParameterExchange(String clientId, String endpoint, ParameterizedTypeReference typeReference, Map<String, Object> queryParams, Long pathVariable) {
        this.clientId = clientId;
        this.endPoint = endpoint;
        this.typeReference = typeReference;
        this.pathVariable = pathVariable;
        this.queryParams = queryParams;
    }

    public ParameterExchange(String clientId, String endpoint, ParameterizedTypeReference typeReference, Long pathVariable) {
        this.clientId = clientId;
        this.endPoint = endpoint;
        this.typeReference = typeReference;
        this.pathVariable = pathVariable;
    }

    public ParameterExchange(String clientId, String endpoint, Object body, ParameterizedTypeReference typeReference) {
        this.clientId = clientId;
        this.endPoint = endpoint;
        this.body = body;
        this.typeReference = typeReference;
    }
}

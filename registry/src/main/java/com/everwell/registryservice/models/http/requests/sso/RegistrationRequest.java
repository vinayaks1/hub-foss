package com.everwell.registryservice.models.http.requests.sso;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class RegistrationRequest {
    private String name;

    private String password;
}

package com.everwell.registryservice.models.http.response;

import com.everwell.registryservice.models.db.Deployment;
import com.everwell.registryservice.models.db.Language;
import com.everwell.registryservice.models.dto.DiseaseTemplate;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DeploymentResponse
{
    private Long id;
    private String code;
    private String countryName;
    private String projectName;
    private String timeZone;
    private String timeZoneAbbreviation;
    private String timeZoneDetails;
    private Integer phonePrefix;
    private String defaultTech;
    private String shortCode;
    private String askForHelpUrl;
    private String defaultTime;
    @Setter
    private Language defaultLanguage;
    @Setter
    private List<Language> allowedLanguages;
    @Setter
    private List<DiseaseTemplate> diseases;

    public DeploymentResponse(Deployment deployment)
    {
        this.id = deployment.getId();
        this.code = deployment.getCode();
        this.countryName = deployment.getCountryName();
        this.projectName = deployment.getProjectName();
        this.timeZone = deployment.getTimeZone();
        this.timeZoneAbbreviation = deployment.getTimeZoneAbbreviation();
        this.timeZoneDetails = deployment.getTimeZoneDetails();
        this.phonePrefix = deployment.getPhonePrefix();
        this.defaultTech = deployment.getDefaultTech();
        this.shortCode = deployment.getShortCode();
        this.askForHelpUrl = deployment.getAskForHelpUrl();
        this.defaultTime = deployment.getDefaultTime();
    }
}

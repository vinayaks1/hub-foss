package com.everwell.registryservice.models.http.response.ins;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TemplateResponse {
    Long templateId;
    String content;
    String parameters;
    String type;
    String language;
}

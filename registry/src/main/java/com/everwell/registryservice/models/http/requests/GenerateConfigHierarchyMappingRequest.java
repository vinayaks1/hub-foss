package com.everwell.registryservice.models.http.requests;

import com.everwell.registryservice.constants.FieldValidationMessages;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GenerateConfigHierarchyMappingRequest extends ConfigHierarchyMappingRequest {
    @NotBlank(message = FieldValidationMessages.CONFIG_VALUE_MANDATORY)
    private String value;

    public GenerateConfigHierarchyMappingRequest(Long hierarchyId, String configName, String value) {
        super(hierarchyId, configName);
        this.value = value;
    }
}

package com.everwell.registryservice.models.db;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "user_access")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class UserAccess {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String username;
    @Column(name = "hierarchy_id")
    private long hierarchyId;
    @Column(name = "selective_hierarchy_mapping")
    private boolean selectiveHierarchyMapping;
    @Column(name = "client_id")
    private Long clientId;
    private long ssoId;
    private boolean active;
}

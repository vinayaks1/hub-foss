package com.everwell.registryservice.models.http.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ConfigResponse {
    private Long id;
    private String name;
    private String defaultValue;
    private String type;
    private boolean multiMapped;
}

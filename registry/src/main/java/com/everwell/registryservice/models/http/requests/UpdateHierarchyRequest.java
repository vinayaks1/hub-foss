package com.everwell.registryservice.models.http.requests;

import com.everwell.registryservice.models.dto.HierarchyUpdateData;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateHierarchyRequest extends HierarchyRequest{
    @Valid
    @NotNull(message = "Hierarchy data must be provided")
    private HierarchyUpdateData hierarchy;
}

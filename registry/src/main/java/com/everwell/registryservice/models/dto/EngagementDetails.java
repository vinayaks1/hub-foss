package com.everwell.registryservice.models.dto;

import com.everwell.registryservice.models.db.Engagement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class EngagementDetails {
    @NotNull(message = "Hierarchy Id can not be empty")
    private Long hierarchyId;
    @NotNull(message = "Default Language can be not empty")
    private Long defaultLanguage;
    @NotEmpty(message = "Default Time can not be empty")
    private String defaultTime;
    private String languages;
    @NotEmpty(message = "Dose Timings can not be empty")
    private String doseTimings;
    @NotNull(message = "Consent Mandatory can not be empty")
    private Boolean consentMandatory;
    @NotEmpty(message = "Notification Type can not be empty")
    private String notificationType;
    private String disabledLanguages;

    public EngagementDetails(Engagement engagement)
    {
        this.hierarchyId = engagement.getHierarchyId();
        this.defaultLanguage = engagement.getDefaultLang();
        this.defaultTime = engagement.getDefaultTime();
        this.languages = engagement.getLanguages();
        this.doseTimings = engagement.getDoseTimings();
        this.consentMandatory = engagement.getConsentMandatory();
        this.notificationType = engagement.getNotificationType();
        this.disabledLanguages = engagement.getDisabledLanguages();
    }
}

package com.everwell.registryservice.models.db;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "field_dependency")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FieldDependency {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String type;
    @Column(name = "form_part_id")
    private Long formPartId;
    @Column(name = "field_id")
    private Long fieldId;
    private String lookups;
    @Column(name = "property_and_values")
    private String propertyAndValues;
}

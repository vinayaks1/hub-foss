package com.everwell.registryservice.models.db;

import com.everwell.registryservice.models.dto.TaskListItem;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "filter_values")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FilterValue {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long filterId;
    private String value;
}

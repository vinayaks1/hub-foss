package com.everwell.registryservice.models.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class DiseaseTemplate {

    @NotNull(message = "Disease Id can not be null")
    private Long id;

    @NotNull(message = "Disease Name can not be null")
    private String diseaseName;

    private Long clientId;

    public DiseaseTemplate(Long id, String diseaseName) {
        this.id = id;
        this.diseaseName = diseaseName;
    }
}

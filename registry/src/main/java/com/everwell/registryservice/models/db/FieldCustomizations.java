package com.everwell.registryservice.models.db;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "field_customizations")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FieldCustomizations {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "field_id")
    private Long fieldId;
    @Column(name = "attribute_name")
    private String attributeName;
    @Column(name = "attribute_value")
    private String attributeValue;
}

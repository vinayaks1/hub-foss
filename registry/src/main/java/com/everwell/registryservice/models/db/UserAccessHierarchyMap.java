package com.everwell.registryservice.models.db;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "user_access_hierarchy_map")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class UserAccessHierarchyMap {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "user_id")
    private long userId;
    @Column(name = "hierarchy_id")
    private long hierarchyId;
    private String relation;
    @Column(name = "last_updated_by")
    private long lastUpdatedBy;
    @CreationTimestamp
    @Column(name = "created_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime createdAt;
    @UpdateTimestamp
    @Column(name = "updated_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime updatedAt;

    public UserAccessHierarchyMap(UserAccess userAccess) {
        this.userId = userAccess.getId();
        this.hierarchyId = userAccess.getHierarchyId();
    }
}

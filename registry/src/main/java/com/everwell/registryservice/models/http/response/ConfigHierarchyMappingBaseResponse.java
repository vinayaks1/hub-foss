package com.everwell.registryservice.models.http.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ConfigHierarchyMappingBaseResponse {
    private long id;
    private Long hierarchyId;
    private Long configId;
    private String value;
    private boolean active;
}

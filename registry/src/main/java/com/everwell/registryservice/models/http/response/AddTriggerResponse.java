package com.everwell.registryservice.models.http.response;

import com.everwell.registryservice.models.dto.AddTriggerDetails;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class AddTriggerResponse {

    List<AddTriggerDetails> failedEntries;
}

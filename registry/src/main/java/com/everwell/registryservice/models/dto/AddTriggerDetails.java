package com.everwell.registryservice.models.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AddTriggerDetails implements Serializable {

    @NotNull(message = "HierarchyId must be provided")
    private Long hierarchyId;

    @NotNull(message = "Client Id must be provided")
    private Long clientId;

    @NotNull(message = "Trigger Id must be provided")
    private Long triggerId;

    @NotNull(message = "Vendor Id must be provided")
    private Long vendorId;

    @NotBlank(message = "Module must be provided")
    private String module;
    private Long defaultTemplateId;
    private String templateIds;

    @NotBlank(message = "Cron Time must be provided")
    private String cronTime;

    @NotBlank(message = "Event Name must be provided")
    private String eventName;

    @NotBlank(message = "Function Name must be provided")
    private String functionName;

    @NotNull(message = "Active status must be provided")
    private Boolean isActive;

    @NotNull(message = "Mandatory must be provided")
    private Boolean mandatory;

    @NotNull(message = "Entity time related must be provided")
    private Boolean entityTimeRelated;

    @NotBlank(message = "Notification type must be provided")
    private String notificationType;

    @NotNull(message = "Is New Template status must be provided")
    private Boolean isNewTemplate;
    private String content;
    private Long languageId;
    private Long typeId;
    private String parameters;
    private Boolean unicode;

}

package com.everwell.registryservice.models.http.requests.episode;

import com.everwell.registryservice.models.http.requests.SearchRequest;
import lombok.Data;

import java.util.Arrays;

@Data
public class EpisodeHeaderSearchRequest extends EpisodeSearchRequest {

    private String searchKey;
    private String searchValue;

    public EpisodeHeaderSearchRequest() {
        super(10, 0, Arrays.asList("id", "firstName", "lastName"), Arrays.asList("IndiaTbPrivate", "IndiaTbPublic"));
    }

    public EpisodeHeaderSearchRequest(String searchKey, String searchValue) {
        super(10, 0, Arrays.asList("id", "firstName", "lastName"), Arrays.asList("IndiaTbPrivate", "IndiaTbPublic"));
        this.searchKey = convertAndSanitise(searchKey);
        this.searchValue = searchValue;
    }

    public EpisodeHeaderSearchRequest(SearchRequest searchRequest) {
        this(searchRequest.getSearchKey(), searchRequest.getSearchValue());
    }

    public String convertAndSanitise(String key) {
        String returnKey = key;
        switch (key) {
            case "Patient Id":
                returnKey = "id";
                break;
            case "TB Number":
                returnKey = "TBNumber";
        }
        return returnKey;
    }
}

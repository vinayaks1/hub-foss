package com.everwell.registryservice.models.http.response;

import com.everwell.registryservice.models.dto.LanguageMapping;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class LanguagesResponse {
    private List<LanguageMapping> availableLanguages;
    private List<LanguageMapping> selectedLanguages;
}

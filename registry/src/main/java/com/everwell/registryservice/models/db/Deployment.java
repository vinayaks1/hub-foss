package com.everwell.registryservice.models.db;

import com.everwell.registryservice.models.http.requests.CreateDeploymentRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "deployment")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Deployment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String code;

    private Integer phonePrefix;

    private String timeZone;

    private String timeZoneAbbreviation;

    private String timeZoneDetails;

    private Long defaultLanguageId;

    private String defaultTech;

    private String shortCode;

    private String countryName;

    private String projectName;

    private String askForHelpUrl;

    private String defaultTime;

    public Deployment(CreateDeploymentRequest request)
    {
        this.code = request.getCode();
        this.phonePrefix = request.getPhonePrefix();
        this.timeZone = request.getTimeZone();
        this.timeZoneAbbreviation = request.getTimeZoneAbbreviation();
        this.timeZoneDetails = request.getTimeZoneDetails();
        this.defaultLanguageId = request.getDefaultLanguageId();
        this.defaultTech = request.getDefaultTech();
        this.shortCode = request.getShortCode();
        this.countryName = request.getCountryName();
        this.projectName = request.getProjectName();
        this.askForHelpUrl = request.getAskForHelpUrl();
        this.defaultTime = request.getDefaultTime();
    }
}

package com.everwell.registryservice.models.db;

import com.everwell.registryservice.models.dto.TaskListItem;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "deployment_task_list")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DeploymentTaskList {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long deploymentId;
    private Long taskListId;
    private String displayColumns;
    private Long displayOrder;

    public DeploymentTaskList(Long deploymentId, TaskListItem taskListItem) {
        this.deploymentId = deploymentId;
        this.taskListId = taskListItem.getTaskListId();
        this.displayColumns = taskListItem.getDisplayColumns();
        this.displayOrder = taskListItem.getDisplayOrder();
    }
}

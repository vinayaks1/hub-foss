package com.everwell.registryservice.models.http.requests.ins;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TriggerDetailsRequest extends TriggerDefaultRequest implements Serializable {
    Long hierarchyId;
    String functionName;
    String notificationType;

    public TriggerDetailsRequest(Long vendorId, Long triggerId, Long templateId, List<Long> templateIds, Boolean isMandatory, Boolean defaultConsent, Boolean isDefaultTime, String timeOfSms, Long hierarchyId, String functionName, String notificationType) {
        super(vendorId, triggerId, templateId, templateIds, isMandatory, defaultConsent, isDefaultTime, timeOfSms);
        this.hierarchyId = hierarchyId;
        this.functionName = functionName;
        this.notificationType = notificationType;
    }
}

package com.everwell.registryservice.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.concurrent.TimeUnit;

@Component
public class RedisUtils
{
    private static RedisTemplate<String, Object> redisTemplate;

    @Autowired
    public RedisUtils(RedisTemplate<String, Object> redisTemplate) {
        RedisUtils.redisTemplate = redisTemplate;
    }

    public static <T> T getFromCache(String key) {
        Object obj = redisTemplate.opsForValue().get(key);
        return (null == obj)?null:(T)(obj);
    }

    public static boolean hasKey(String key) {
        return redisTemplate.hasKey(key);
    }

    public static void putIntoCache(String key, Object value, Long ttl) {
        redisTemplate.opsForValue().set(key, value, ttl, TimeUnit.SECONDS);
    }
}

package com.everwell.registryservice.utils;

import com.everwell.registryservice.constants.AuthConstants;
import com.everwell.registryservice.constants.ValidationStatusEnum;
import com.everwell.registryservice.exceptions.UnauthorizedException;
import com.everwell.registryservice.models.db.Client;
import com.everwell.registryservice.models.db.UserAccess;
import io.jsonwebtoken.*;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Component
public class JwtUtils {
    private static final long serialVersionUID = 234234523523L;

    public static final long JWT_TOKEN_VALIDITY = 5 * 60 * 60;

    @Setter
    @Value("${jwt.secret}")
    private String secretKey;

    @Setter
    @Value("${sso.publickey}")
    private String ssoPublicKey;

    //retrieve expiration date from jwt token
    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        Claims claims = getParsedClaims(token);
        return claimsResolver.apply(claims);
    }

    public Claims getParsedClaims(String token) {
        Claims claims = null;
        try {
            claims = getAllClaimsFromToken(token, false);
        } catch (IllegalArgumentException | MalformedJwtException e) {
            try {
                claims = getAllClaimsFromToken(token, true);
            } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
                throw new UnauthorizedException(ValidationStatusEnum.AUTH_INVALID);
            }
        } catch (NoSuchAlgorithmException | InvalidKeySpecException ex){
          throw new UnauthorizedException(ValidationStatusEnum.AUTH_INVALID);
        }
        return claims;
    }

    //for retrieving any information from token we will need the secret key
    private Claims getAllClaimsFromToken(String token, boolean usePublicKey) throws NoSuchAlgorithmException, InvalidKeySpecException {
        JwtParser parser = Jwts
                .parser();
        if(usePublicKey) {
            parser.setSigningKey(readPublicKey(ssoPublicKey));
        } else{
            parser.setSigningKey(secretKey);
        }
        return parser.parseClaimsJws(token)
                .getBody();
    }

    //check if the token has expired
    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    //generate token for user
    public String generateToken(String username) {
        return generateToken(username, new HashMap<>());
    }

    public String generateToken(String username, Map<String, Object> claims) {
        return doGenerateToken(claims, username);
    }

    //while creating the token -
    //1. Define  claims of the token, like Issuer, Expiration, Subject, and the ID
    //2. Sign the JWT using the HS512 algorithm and secret key.
    private String doGenerateToken(Map<String, Object> claims, String subject) {
        return Jwts
            .builder()
            .setClaims(claims)
            .setSubject(subject)
            .setIssuedAt(new Date(System.currentTimeMillis()))
            .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY * 1000))
            .signWith(SignatureAlgorithm.HS512, secretKey)
            .compact();
    }

    //validate token
    public Boolean validateToken(String token, UserAccess userAccess, Client client) {
        final String subject = getClaimFromToken(token, Claims::getSubject);
        final String issuer = getClaimFromToken(token, Claims::getIssuer);
        // A token is valid if issued by sso and active user is available or if the token is the client id principal
        return ((subject.equals(String.valueOf(client.getId())) || (issuer.equals(AuthConstants.SSO_AUTH_ISSUER) && null != userAccess && userAccess.isActive())) && !isTokenExpired(token));
    }

    public static PublicKey readPublicKey(String publicKey)
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        Base64.Decoder decoder = Base64.getDecoder();
        X509EncodedKeySpec publicSpec = new X509EncodedKeySpec(decoder.decode(publicKey));
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        return keyFactory.generatePublic(publicSpec);
    }
}

package com.everwell.registryservice.validators;

import com.everwell.registryservice.constants.ValidationStatusEnum;
import com.everwell.registryservice.exceptions.ValidationException;
import com.everwell.registryservice.models.dto.HierarchyRequestData;
import org.springframework.stereotype.Component;

/**
 * Class Type - Validator
 * Purpose - Validate Request received by Hierarchy Controller
 * Scope - Hierarchy Module
 *
 * @author Ashish Shrivastava
 */
@Component
public class HierarchyRequestValidator {

    /**
     * Validator method to validate AddHierarchyRequest received by Controller
     *
     * @param hierarchyRequestData - Input Hierarchy Request Data
     */
    public void validateAddHierarchyRequest(HierarchyRequestData hierarchyRequestData) {
        // If input hierarchy level is greater than one then Parent id is mandatory to be passed in request
        if (hierarchyRequestData.getLevel() > 1 && null == hierarchyRequestData.getParentId()) {
            throw new ValidationException(ValidationStatusEnum.PARENT_ID_REQUIRED_FOR_HIERARCHY_NOT_LEVEL_ONE);
        }
    }
}

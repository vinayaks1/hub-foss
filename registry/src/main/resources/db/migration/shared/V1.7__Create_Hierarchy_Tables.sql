CREATE TABLE IF NOT EXISTS associations_master
(
    id bigserial NOT NULL,
    type character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT associations_master_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS config
(
    id bigserial NOT NULL,
    created_at timestamp without time zone,
    default_value character varying(255) COLLATE pg_catalog."default",
    name character varying(255) COLLATE pg_catalog."default",
    type character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT config_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS hierarchy
(
    id bigserial NOT NULL,
    active boolean NOT NULL,
    client_id bigint,
    code character varying(255) COLLATE pg_catalog."default",
    created_at timestamp without time zone,
    deployment_code character varying(255) COLLATE pg_catalog."default",
    end_date timestamp without time zone,
    has_children boolean NOT NULL,
    level bigint,
    level_1_code character varying(255) COLLATE pg_catalog."default",
    level_1_id bigint,
    level_1_name character varying(255) COLLATE pg_catalog."default",
    level_1_type character varying(255) COLLATE pg_catalog."default",
    level_2_code character varying(255) COLLATE pg_catalog."default",
    level_2_id bigint,
    level_2_name character varying(255) COLLATE pg_catalog."default",
    level_2_type character varying(255) COLLATE pg_catalog."default",
    level_3_code character varying(255) COLLATE pg_catalog."default",
    level_3_id bigint,
    level_3_name character varying(255) COLLATE pg_catalog."default",
    level_3_type character varying(255) COLLATE pg_catalog."default",
    level_4_code character varying(255) COLLATE pg_catalog."default",
    level_4_id bigint,
    level_4_name character varying(255) COLLATE pg_catalog."default",
    level_4_type character varying(255) COLLATE pg_catalog."default",
    level_5_code character varying(255) COLLATE pg_catalog."default",
    level_5_id bigint,
    level_5_name character varying(255) COLLATE pg_catalog."default",
    level_5_type character varying(255) COLLATE pg_catalog."default",
    level_6_code character varying(255) COLLATE pg_catalog."default",
    level_6_id bigint,
    level_6_name character varying(255) COLLATE pg_catalog."default",
    level_6_type character varying(255) COLLATE pg_catalog."default",
    merge_status character varying(255) COLLATE pg_catalog."default",
    merm_mapping_level bigint,
    mobile_number character varying(255) COLLATE pg_catalog."default",
    name character varying(255) COLLATE pg_catalog."default",
    parent_id bigint,
    start_date timestamp without time zone,
    type character varying(255) COLLATE pg_catalog."default",
    updated_at timestamp without time zone,
    CONSTRAINT hierarchy_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS hierarchy_associations
(
    id bigserial NOT NULL,
    association_id bigint,
    hierarchy_id bigint,
    value character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT hierarchy_associations_pkey PRIMARY KEY (id),
    CONSTRAINT fkb5envuphupjmrpa69v9t03lei FOREIGN KEY (association_id)
        REFERENCES public.associations_master (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fknc7bcp3pkvka06l3cuk50tjei FOREIGN KEY (hierarchy_id)
        REFERENCES public.hierarchy (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE IF NOT EXISTS hierarchy_config
(
    id bigserial NOT NULL,
    active boolean,
    config_mapping_id bigint,
    created_at timestamp without time zone,
    hierarchy_id bigint NOT NULL,
    value character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT hierarchy_config_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS hierarchy_config_map
(
    id bigserial NOT NULL,
    active boolean,
    created_at timestamp without time zone,
    value character varying(255) COLLATE pg_catalog."default",
    config_mapping_id bigint,
    hierarchy_id bigint,
    CONSTRAINT hierarchy_config_map_pkey PRIMARY KEY (id),
    CONSTRAINT fkequxk26jgvf4w7v0exvkg355k FOREIGN KEY (hierarchy_id)
        REFERENCES public.hierarchy (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fkpaf04dg0rva826ri4a2vqcpio FOREIGN KEY (config_mapping_id)
        REFERENCES public.config (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE IF NOT EXISTS user_access
(
    id bigserial NOT NULL,
    hierarchy_mapping bigint,
    selective_hierarchy_mapping boolean,
    sso_id bigint NOT NULL,
    username character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT user_access_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS user_access_hierarchy_map
(
    id bigserial NOT NULL,
    created_at timestamp without time zone,
    hierarchy_mapping bigint,
    last_updated_by bigint,
    relation character varying(255) COLLATE pg_catalog."default",
    updated_at timestamp without time zone,
    userid bigint NOT NULL,
    CONSTRAINT user_access_hierarchy_map_pkey PRIMARY KEY (id)
);
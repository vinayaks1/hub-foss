ALTER TABLE Language
DROP COLUMN IF EXISTS displayName;

ALTER TABLE Language
    ADD if not exists display_name varchar(255);

update Language set display_name = 'English' where name = 'English';
update Language set display_name = 'አማርኛ (Amharic)' where name = 'Amharic';
update Language set display_name = 'বাংলা (Bangla)' where name = 'Bengali';
update Language set display_name = 'qɯɾʁɯz (Kirghiz)' where name = 'Kirghiz';
update Language set display_name = 'Afaan Oromoo' where name = 'Oromo';
update Language set display_name = 'Português (Portuguese)' where name = 'Portuguese';
update Language set display_name = 'Русский язык (Russian)' where name = 'Russian';
update Language set display_name = 'Kiswahili (Swahili)' where name = 'Swahili';
update Language set display_name = 'Filipino (Tagalog and English)'where name = 'Tagalog';
update Language set display_name = 'Українська (Ukrainian)' where name = 'Ukrainian';
update Language set display_name = 'Français (French)' where name = 'French';
update Language set display_name = 'മലയാളം (Malayalam)' where name = 'Malayalam';
update Language set display_name = 'தமிழ் (Tamil)' where name = 'Tamil';
update Language set display_name = 'ગુજરાતી (Gujarati)' where name = 'Gujarati';
update Language set display_name = 'िन्दी (Hindi)' where name = 'Hindi';
update Language set display_name = 'मराठी (Marathi)' where name = 'Marathi';
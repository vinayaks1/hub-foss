ALTER TABLE client
    ADD if not exists sso_url varchar(255);

ALTER TABLE client
    ADD if not exists sso_authorization_cookie_name varchar(255);
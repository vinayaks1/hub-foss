DO
$do$
BEGIN

IF NOT EXISTS (SELECT * FROM form WHERE form_name='Registration') THEN
    INSERT INTO form (form_label, form_name, save_endpoint, save_text) VALUES ('_add_patient', 'Registration', '/api/Patients/Add?isPatientDetailsRequired=false', '_add_patient');
END IF;

IF NOT EXISTS (SELECT * FROM form WHERE form_name='BasicDetails') THEN
    INSERT INTO form (form_label, form_name, save_endpoint, save_text) VALUES ('_basic_details', 'BasicDetails', '/API/Patients/Update', 'submit');
END IF;

IF NOT EXISTS (SELECT * FROM form_part WHERE part_name='BasicDetails' AND part_label='_basic_details') THEN
    INSERT INTO form_part (part_name, part_label, part_type, rows, columns, position, allow_row_delete, allow_row_open, item_description_field, list_item_title, row_delete_text)
    VALUES ('BasicDetails','_basic_details','vertical-form-part',NULL,NULL,1,false,false,NULL,NULL,NULL);
END IF;

IF NOT EXISTS (SELECT * FROM form_part WHERE part_name='ContactDetails' AND part_label='contact_details') THEN
    INSERT INTO form_part (part_name, part_label, part_type, rows, columns, position, allow_row_delete, allow_row_open, item_description_field, list_item_title, row_delete_text)
    VALUES ('ContactDetails','contact_details','vertical-form-part',NULL,NULL,2,false,false,NULL,NULL,NULL);
END IF;

IF NOT EXISTS (SELECT * FROM form_part WHERE part_name='Residence' AND part_label='treatment_center') THEN
    INSERT INTO form_part (part_name, part_label, part_type, rows, columns, position, allow_row_delete, allow_row_open, item_description_field, list_item_title, row_delete_text)
    VALUES ('Residence','treatment_center','vertical-form-part',NULL,NULL,3,false,false,NULL,NULL,NULL);
END IF;

IF NOT EXISTS (SELECT * FROM form_part WHERE part_name='TreatmentDetails' AND part_label='Treatment Details') THEN
    INSERT INTO form_part (part_name, part_label, part_type, rows, columns, position, allow_row_delete, allow_row_open, item_description_field, list_item_title, row_delete_text)
    VALUES ('TreatmentDetails','Treatment Details','vertical-form-part',NULL,NULL,4,false,false,NULL,NULL,NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field WHERE field_name = 'HierarchyMapping_Residence' AND label = 'HierarchyMapping_Residence' AND component = 'app-hierarchy-selection-field') THEN
    INSERT INTO field (field_name,label,component,disabled,visible,required,default_value,field_options,validation_list,row_number,column_number,remote_url,config) VALUES
     ('HierarchyMapping_Residence','HierarchyMapping_Residence','app-hierarchy-selection-field',false,true,true,NULL,NULL,NULL,1,NULL,NULL,'{"HierarchySelectionConfig": [{"Types": ["STATE"], "Label": "Select STATE", "Level": 2, "Order": 1, "OptionDisplayKeys": ["Name", "Id", "Code"], "Options": [], "RemoteUrl": "/api/Hierarchy/GetHierarchiesByTypeParent"},  {"Types": ["DISTRICT"], "Label": "Select DISTRICT", "Level": 3, "Order": 2, "OptionDisplayKeys": ["Name", "Id", "Code"], "Options": [], "RemoteUrl": "/api/Hierarchy/GetHierarchiesByTypeParent"},  {"Types": ["FACILITY"], "Label": "Select FACILITY", "Level": 4, "Order": 3, "OptionDisplayKeys": ["Name", "Id", "Code"], "Options": [], "RemoteUrl": "/api/Hierarchy/GetHierarchiesByTypeParent"}]}');
END IF;

IF NOT EXISTS (SELECT * FROM field WHERE field_name = 'FirstName' AND label = '_first_name' AND component = 'app-input-field') THEN
    INSERT INTO field (field_name,label,component,disabled,visible,required,default_value,field_options,validation_list,row_number,column_number,remote_url,config) VALUES
	 ('FirstName','_first_name','app-input-field',false,true,true,NULL,NULL,'OnlyAlphabetAndSpaceAllowed',1,NULL,NULL,NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field WHERE field_name = 'LastName' AND label = '_surname' AND component = 'app-input-field') THEN
    INSERT INTO field (field_name,label,component,disabled,visible,required,default_value,field_options,validation_list,row_number,column_number,remote_url,config) VALUES
	 ('LastName','_surname','app-input-field',false,true,false,NULL,NULL,'OnlyAlphabetAndSpaceAllowed',2,NULL,NULL,NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field WHERE field_name = 'Address' AND label = '_address' AND component = 'app-input-field') THEN
    INSERT INTO field (field_name,label,component,disabled,visible,required,default_value,field_options,validation_list,row_number,column_number,remote_url,config) VALUES
    ('Address','_address','app-input-field',false,true,false,NULL,NULL,NULL,3,NULL,NULL,NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field WHERE field_name = 'Age' AND label = '_age' AND component = 'app-input-field') THEN
    INSERT INTO field (field_name,label,component,disabled,visible,required,default_value,field_options,validation_list,row_number,column_number,remote_url,config) VALUES
    ('Age','_age','app-input-field',false,true,false,NULL,NULL,'Age',4,NULL,NULL,NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field WHERE field_name = 'Gender' AND label = '_gender' AND component = 'app-select') THEN
    INSERT INTO field (field_name,label,component,disabled,visible,required,default_value,field_options,validation_list,row_number,column_number,remote_url,config) VALUES
    ('Gender','_gender','app-select',false,true,false,NULL,'[{"Value" : "_male", "Key" : "Male"}, {"Value":"_female", "Key":"Female"}, {"Value":"_unknown", "Key":"Unknown"}]',NULL,5,NULL,NULL,NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field WHERE field_name = 'PrimaryPhone' AND label = 'primary_phone' AND component = 'app-input-field-group') THEN
    INSERT INTO field (field_name,label,component,disabled,visible,required,default_value,field_options,validation_list,row_number,column_number,remote_url,config) VALUES
	 ('PrimaryPhone','primary_phone','app-input-field-group',false,true,true,NULL,NULL,'PhoneNumberNotStartingWithZero,PhoneNumberMaxLengthEleven',1,NULL,NULL,NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field WHERE field_name = 'PrimaryPhoneOwner' AND label = '_owner' AND component = 'app-input-field') THEN
    INSERT INTO field (field_name,label,component,disabled,visible,required,default_value,field_options,validation_list,row_number,column_number,remote_url,config) VALUES
	 ('PrimaryPhoneOwner','_owner','app-input-field',false,true,false,NULL,NULL,'OnlyAlphabetAndSpaceAllowed',2,NULL,NULL,NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field WHERE field_name = 'SecondaryPhone1' AND label = '_phone_1' AND component = 'app-input-field-group') THEN
    INSERT INTO field (field_name,label,component,disabled,visible,required,default_value,field_options,validation_list,row_number,column_number,remote_url,config) VALUES
	 ('SecondaryPhone1','_phone_1','app-input-field-group',false,true,false,NULL,NULL,'PhoneNumberStartingWithZero,PhoneNumberMaxLengthEleven',3,NULL,NULL,NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field WHERE field_name = 'SecondaryPhone1Owner' AND label = '_owner' AND component = 'app-input-field') THEN
    INSERT INTO field (field_name,label,component,disabled,visible,required,default_value,field_options,validation_list,row_number,column_number,remote_url,config) VALUES
	 ('SecondaryPhone1Owner','_owner','app-input-field',false,true,false,NULL,NULL,'OnlyAlphabetAndSpaceAllowed',4,NULL,NULL,NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field WHERE field_name = 'SecondaryPhone2' AND label = '_phone_3' AND component = 'app-input-field-group') THEN
    INSERT INTO field (field_name,label,component,disabled,visible,required,default_value,field_options,validation_list,row_number,column_number,remote_url,config) VALUES
	 ('SecondaryPhone2','_phone_3','app-input-field-group',false,true,false,NULL,NULL,'PhoneNumberStartingWithZero,PhoneNumberMaxLengthEleven',5,NULL,NULL,NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field WHERE field_name = 'SecondaryPhone2Owner' AND label = '_owner' AND component = 'app-input-field') THEN
    INSERT INTO field (field_name,label,component,disabled,visible,required,default_value,field_options,validation_list,row_number,column_number,remote_url,config) VALUES
	 ('SecondaryPhone2Owner','_owner','app-input-field',false,true,false,NULL,NULL,'OnlyAlphabetAndSpaceAllowed',6,NULL,NULL,NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field WHERE field_name = 'SecondaryPhone3' AND label = '_phone_4' AND component = 'app-input-field-group') THEN
    INSERT INTO field (field_name,label,component,disabled,visible,required,default_value,field_options,validation_list,row_number,column_number,remote_url,config) VALUES
	 ('SecondaryPhone3','_phone_4','app-input-field-group',false,true,false,NULL,NULL,'PhoneNumberStartingWithZero,PhoneNumberMaxLengthEleven',7,NULL,NULL,NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field WHERE field_name = 'SecondaryPhone3Owner' AND label = '_owner' AND component = 'app-input-field') THEN
    INSERT INTO field (field_name,label,component,disabled,visible,required,default_value,field_options,validation_list,row_number,column_number,remote_url,config) VALUES
	 ('SecondaryPhone3Owner','_owner','app-input-field',false,true,false,NULL,NULL,'OnlyAlphabetAndSpaceAllowed',8,NULL,NULL,NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field WHERE field_name = 'TBTreatmentStartDate' AND label = 'TBTreatmentStartDate' AND component = 'app-datepicker') THEN
    INSERT INTO field (field_name,label,component,disabled,visible,required,default_value,field_options,validation_list,row_number,column_number,remote_url,config) VALUES
	 ('TBTreatmentStartDate','TBTreatmentStartDate','app-datepicker',false,true,false,NULL,NULL,NULL,1,NULL,NULL,'{"DisabledDateConfig":{"AddDaysFromToday":"0"}}');
END IF;

IF NOT EXISTS (SELECT * FROM field WHERE field_name = 'EnrollmentDate' AND label = 'EnrollmentDate' AND component = 'app-datepicker') THEN
    INSERT INTO field (field_name,label,component,disabled,visible,required,default_value,field_options,validation_list,row_number,column_number,remote_url,config) VALUES
	 ('EnrollmentDate','EnrollmentDate','app-datepicker',false,true,false,NULL,NULL,NULL,2,NULL,NULL,'{"DisabledDateConfig":{"SubtractDaysFromToday":180, "AddDaysFromToday":"0"}}');
END IF;

IF NOT EXISTS (SELECT * FROM field WHERE field_name = 'TBCategory' AND label = '_tb_category' AND component = 'app-select') THEN
    INSERT INTO field (field_name,label,component,disabled,visible,required,default_value,field_options,validation_list,row_number,column_number,remote_url,config) VALUES
	 ('TBCategory','_tb_category','app-select',false,true,true,NULL,'[{"Value" : "_ds_tb", "Key" : "DS-TB"}, {"Value":"_dr_tb", "Key":"DR-TB"}]',NULL,3,NULL,NULL,NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field WHERE field_name = 'WeightBand' AND label = '_weight_band' AND component = 'app-select') THEN
    INSERT INTO field (field_name,label,component,disabled,visible,required,default_value,field_options,validation_list,row_number,column_number,remote_url,config) VALUES
	 ('WeightBand','_weight_band','app-select',false,true,true,NULL,'[{"Value" : "2 pills per day", "Key" : "2"}, {"Value":"3 pills per day", "Key":"3"}, {"Value":"4 pills per day", "Key":"4"}, {"Value":"5 pills per day", "Key":"5"}]',NULL,4,NULL,NULL,NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field WHERE field_name = 'NumberOfPills' AND label = 'NumberOfPills' AND component = 'app-select') THEN
    INSERT INTO field (field_name,label,component,disabled,visible,required,default_value,field_options,validation_list,row_number,column_number,remote_url,config) VALUES
	 ('NumberOfPills','NumberOfPills','app-select',false,true,true,NULL,'[{"Value" : "1", "Key" : "1"}, {"Value":"2", "Key":"2"}, {"Value" : "3", "Key" : "3"}, {"Value" : "4", "Key" : "4"}, {"Value" : "5", "Key" : "5"}, {"Value" : "6", "Key" : "6"}, {"Value" : "7", "Key" : "7"}, {"Value" : "8", "Key" : "8"}, {"Value" : "9", "Key" : "9"}, {"Value" : "10", "Key" : "10"}, {"Value" : "11", "Key" : "11"}, {"Value" : "12", "Key" : "12"}, {"Value" : "13", "Key" : "13"}, {"Value" : "14", "Key" : "14"}, {"Value" : "15", "Key" : "15"}, {"Value" : "16", "Key" : "16"}, {"Value" : "17", "Key" : "17"}, {"Value" : "18", "Key" : "18"}, {"Value" : "19", "Key" : "19"}, {"Value" : "20", "Key" : "20"}]',NULL,5,NULL,NULL,NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field WHERE field_name = 'MonitoringMethod' AND label = 'adherence_technology' AND component = 'app-select') THEN
    INSERT INTO field (field_name,label,component,disabled,visible,required,default_value,field_options,validation_list,row_number,column_number,remote_url,config) VALUES
	 ('MonitoringMethod','adherence_technology','app-select',false,true,true,NULL,'[{"Value" : "evriMED(MERM)", "Key" : "MERM", "AdherenceTechOptions": "MERM"}, {"Value":"_VOT", "Key":"VOT", "AdherenceTechOptions":"VOT"}, {"Value":"_99dots", "Key":"99DOTS", "AdherenceTechOptions": "99DOTS"}, {"Value":"_99dotsLite", "Key":"99DOTSLite", "AdherenceTechOptions": "99DOTSLite"}, {"Value":"FollowedWithoutTechnology", "Key":"NONE", "AdherenceTechOptions": "NONE"}]',NULL,6,NULL,NULL,NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field WHERE field_name = 'MermSerialNoObj' AND label = 'MermSerialNoObj' AND component = 'app-search-select') THEN
    INSERT INTO field (field_name,label,component,disabled,visible,required,default_value,field_options,validation_list,row_number,column_number,remote_url,config) VALUES
	 ('MermSerialNoObj','MermSerialNoObj','app-search-select',false,false,true,NULL,NULL,NULL,7,NULL,'/api/Merm/GetAvailableMermImeisNew', '{"RemoteUpdateConfig": [{    "Url": "/api/Merm/GetAvailableMermImeisNew",    "FormPart": "TreatmentDetails",    "Field": "MermSerialNoObj",    "ResponsePath": ["Data"],    "ConditionalCall": "true",    "ValueDependency": [		{			"ExpectedValue": "MERM",			"FormPart": "TreatmentDetails",			"Field": "MonitoringMethod"        },		{            "CheckNotNull": "true",            "FormPart": "TreatmentDetails",            "Field": "EnrollmentDate"        },        {        	"CheckNotNull": "true",            "FormPart": "Residence",            "Field": "HierarchyMapping_Residence",            "IsObject": "true",            "ObjectProperty": "Id"        }	],    "UrlVariables": {        "EnrollmentDate": ["TreatmentDetails", "EnrollmentDate"],        "HierarchyId": ["Residence", "HierarchyMapping_Residence", "Id"]    }}]}');
END IF;

IF NOT EXISTS (SELECT * FROM field WHERE field_name = 'MermAlarmEnabled' AND label = 'MermAlarmEnabled' AND component = 'app-checkbox-group') THEN
    INSERT INTO field (field_name,label,component,disabled,visible,required,default_value,field_options,validation_list,row_number,column_number,remote_url,config) VALUES
	 ('MermAlarmEnabled','MermAlarmEnabled','app-checkbox-group',false,false,false,NULL,'[{"Value" : "", "Key" : "true"}]',NULL,9,NULL,NULL,NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field WHERE field_name = 'MermAlarmTime' AND label = 'MermAlarmTime' AND component = 'app-select') THEN
    INSERT INTO field (field_name,label,component,disabled,visible,required,default_value,field_options,validation_list,row_number,column_number,remote_url,config) VALUES
	 ('MermAlarmTime','MermAlarmTime','app-select',false,false,true,NULL,'[{"Value" : "00:00", "Key" : "00:00"}, {"Value":"00:30", "Key":"00:30"}, {"Value" : "01:00", "Key" : "01:00"}, {"Value":"01:30", "Key":"01:30"}, {"Value" : "02:00", "Key" : "02:00"}, {"Value":"02:30", "Key":"02:30"}, {"Value" : "03:00", "Key" : "03:00"}, {"Value":"03:30", "Key":"03:30"}, {"Value" : "04:00", "Key" : "04:00"}, {"Value":"04:30", "Key":"04:30"}, {"Value" : "05:00", "Key" : "05:00"}, {"Value":"05:30", "Key":"05:30"}, {"Value" : "06:00", "Key" : "06:00"}, {"Value":"06:30", "Key":"06:30"}, {"Value" : "07:00", "Key" : "07:00"}, {"Value":"07:30", "Key":"07:30"}, {"Value" : "08:00", "Key" : "08:00"}, {"Value":"08:30", "Key":"08:30"}, {"Value" : "09:00", "Key" : "09:00"}, {"Value":"09:30", "Key":"09:30"}, {"Value" : "10:00", "Key" : "10:00"}, {"Value":"10:30", "Key":"10:30"}, {"Value" : "11:00", "Key" : "11:00"}, {"Value":"11:30", "Key":"11:30"}, {"Value" : "12:00", "Key" : "12:00"}, {"Value":"12:30", "Key":"12:30"},{"Value" : "13:00", "Key" : "13:00"}, {"Value":"13:30", "Key":"13:30"}, {"Value" : "14:00", "Key" : "14:00"}, {"Value":"14:30", "Key":"14:30"}, {"Value" : "15:00", "Key" : "15:00"}, {"Value":"15:30", "Key":"15:30"},{"Value" : "16:00", "Key" : "16:00"}, {"Value":"16:30", "Key":"16:30"},{"Value" : "17:00", "Key" : "17:00"}, {"Value":"17:30", "Key":"17:30"}, {"Value" : "18:00", "Key" : "18:00"}, {"Value":"18:30", "Key":"18:30"}, {"Value" : "19:00", "Key" : "19:00"}, {"Value":"19:30", "Key":"19:30"}, {"Value" : "20:00", "Key" : "20:00"}, {"Value":"20:30", "Key":"20:30"}, {"Value" : "21:00", "Key" : "21:00"}, {"Value":"21:30", "Key":"21:30"},{"Value" : "22:00", "Key" : "22:00"}, {"Value":"22:30", "Key":"22:30"}, {"Value" : "23:00", "Key" : "23:00"}, {"Value":"23:30", "Key":"23:30"}]',NULL,9,NULL,NULL,NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field WHERE field_name = 'MermRefillAlarmEnabled' AND label = 'MermRefillAlarmEnabled' AND component = 'app-checkbox-group') THEN
    INSERT INTO field (field_name,label,component,disabled,visible,required,default_value,field_options,validation_list,row_number,column_number,remote_url,config) VALUES
	 ('MermRefillAlarmEnabled','MermRefillAlarmEnabled','app-checkbox-group',false,false,false,NULL,'[{"Value" : "", "Key" : "true"}]',NULL,10,NULL,NULL,NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field WHERE field_name = 'MermRefillAlarmDate' AND label = 'MermRefillAlarmDate' AND component = 'app-datepicker') THEN
    INSERT INTO field (field_name,label,component,disabled,visible,required,default_value,field_options,validation_list,row_number,column_number,remote_url,config) VALUES
	 ('MermRefillAlarmDate','MermRefillAlarmDate','app-datepicker',false,false,true,NULL,NULL,NULL,11,NULL,NULL,NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field WHERE field_name = 'MermRefillAlarmTime' AND label = 'MermRefillAlarmTime' AND component = 'app-select') THEN
    INSERT INTO field (field_name,label,component,disabled,visible,required,default_value,field_options,validation_list,row_number,column_number,remote_url,config) VALUES
	 ('MermRefillAlarmTime','MermRefillAlarmTime','app-select',false,false,true,NULL,'[{"Value" : "00:00", "Key" : "00:00"}, {"Value":"00:30", "Key":"00:30"}, {"Value" : "01:00", "Key" : "01:00"}, {"Value":"01:30", "Key":"01:30"}, {"Value" : "02:00", "Key" : "02:00"}, {"Value":"02:30", "Key":"02:30"}, {"Value" : "03:00", "Key" : "03:00"}, {"Value":"03:30", "Key":"03:30"}, {"Value" : "04:00", "Key" : "04:00"}, {"Value":"04:30", "Key":"04:30"}, {"Value" : "05:00", "Key" : "05:00"}, {"Value":"05:30", "Key":"05:30"}, {"Value" : "06:00", "Key" : "06:00"}, {"Value":"06:30", "Key":"06:30"}, {"Value" : "07:00", "Key" : "07:00"}, {"Value":"07:30", "Key":"07:30"}, {"Value" : "08:00", "Key" : "08:00"}, {"Value":"08:30", "Key":"08:30"}, {"Value" : "09:00", "Key" : "09:00"}, {"Value":"09:30", "Key":"09:30"}, {"Value" : "10:00", "Key" : "10:00"}, {"Value":"10:30", "Key":"10:30"}, {"Value" : "11:00", "Key" : "11:00"}, {"Value":"11:30", "Key":"11:30"}, {"Value" : "12:00", "Key" : "12:00"}, {"Value":"12:30", "Key":"12:30"},{"Value" : "13:00", "Key" : "13:00"}, {"Value":"13:30", "Key":"13:30"}, {"Value" : "14:00", "Key" : "14:00"}, {"Value":"14:30", "Key":"14:30"}, {"Value" : "15:00", "Key" : "15:00"}, {"Value":"15:30", "Key":"15:30"},{"Value" : "16:00", "Key" : "16:00"}, {"Value":"16:30", "Key":"16:30"},{"Value" : "17:00", "Key" : "17:00"}, {"Value":"17:30", "Key":"17:30"}, {"Value" : "18:00", "Key" : "18:00"}, {"Value":"18:30", "Key":"18:30"}, {"Value" : "19:00", "Key" : "19:00"}, {"Value":"19:30", "Key":"19:30"}, {"Value" : "20:00", "Key" : "20:00"}, {"Value":"20:30", "Key":"20:30"}, {"Value" : "21:00", "Key" : "21:00"}, {"Value":"21:30", "Key":"21:30"},{"Value" : "22:00", "Key" : "22:00"}, {"Value":"22:30", "Key":"22:30"}, {"Value" : "23:00", "Key" : "23:00"}, {"Value":"23:30", "Key":"23:30"}]',NULL,12,NULL,NULL,NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field WHERE field_name = 'ExternalId1' AND label = '_tb_number' AND component = 'app-input-field') THEN
    INSERT INTO field (field_name,label,component,disabled,visible,required,default_value,field_options,validation_list,row_number,column_number,remote_url,config) VALUES
	 ('ExternalId1','_tb_number','app-input-field',false,true,false,NULL,NULL,NULL,16,NULL,NULL,NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field WHERE field_name = 'TreatmentLength' AND label = 'TreatmentLength' AND component = 'app-input-field') THEN
    INSERT INTO field (field_name,label,component,disabled,visible,required,default_value,field_options,validation_list,row_number,column_number,remote_url,config) VALUES
	 ('TreatmentLength','TreatmentLength','app-input-field',false,true,true,'168',NULL,NULL,20,NULL,NULL,NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field WHERE field_name = 'PhoneNetwork' AND label = 'PhoneNetwork' AND component = 'app-input-field') THEN
    INSERT INTO field (field_name,label,component,disabled,visible,required,default_value,field_options,validation_list,row_number,column_number,remote_url,config) VALUES
	 ('PhoneNetwork','PhoneNetwork','app-input-field',false,true,false,NULL,NULL,NULL,24,NULL,NULL,NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field WHERE field_name = 'AddedFrom' AND label = 'AddedFrom' AND component = 'app-input-field') THEN
    INSERT INTO field (field_name,label,component,disabled,visible,required,default_value,field_options,validation_list,row_number,column_number,remote_url,config) VALUES
	 ('AddedFrom','AddedFrom','app-input-field',false,false,false,'web',NULL,NULL,25,NULL,NULL,NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field WHERE field_name = 'LastSeen' AND label = 'last_seen' AND component = 'app-input-field') THEN
    INSERT INTO field (field_name,label,component,disabled,visible,required,default_value,field_options,validation_list,row_number,column_number,remote_url,config) VALUES
	 ('LastSeen','last_seen','app-input-field',true,false,false,NULL,NULL,NULL,7,NULL,NULL,NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field WHERE field_name = 'BatteryLevel' AND label = 'last_known_battery_level' AND component = 'app-input-field') THEN
    INSERT INTO field (field_name,label,component,disabled,visible,required,default_value,field_options,validation_list,row_number,column_number,remote_url,config) VALUES
	 ('BatteryLevel','last_known_battery_level','app-input-field',true,false,false,NULL,NULL,NULL,8,NULL,NULL,NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field_customizations WHERE field_id = ((SELECT Id FROM field WHERE field_name = 'PrimaryPhone')) AND attribute_name = 'AddOn' AND attribute_value = 'PHONE_PREFIX') THEN
    INSERT INTO field_customizations (field_id, attribute_name, attribute_value) VALUES
    ((SELECT Id FROM field WHERE field_name = 'PrimaryPhone'), 'AddOn', 'PHONE_PREFIX');
END IF;

IF NOT EXISTS (SELECT * FROM field_customizations WHERE field_id = ((SELECT Id FROM field WHERE field_name = 'HierarchyMapping_Residence')) AND attribute_name = 'Label' AND attribute_value = 'select_treatment_center') THEN
    INSERT INTO field_customizations (field_id, attribute_name, attribute_value) VALUES
    ((SELECT Id FROM field WHERE field_name = 'HierarchyMapping_Residence'), 'Label', 'select_treatment_center');
END IF;

IF NOT EXISTS (SELECT * FROM field_dependency WHERE field_id = (SELECT id from field WHERE field_name = 'TreatmentLength') AND lookups = '[{"FormPart": "TreatmentDetails", "Field": "TBCategory", "IsObject": true, "ObjectKeyAndValue": {"DR-TB": "336", "DS-TB": "168"}}]') THEN
    INSERT INTO field_dependency (type, form_part_id, field_id, lookups, property_and_values) VALUES
     ('VALUE',(SELECT id FROM form_part WHERE part_name = 'TreatmentDetails'),(SELECT id FROM field WHERE field_name = 'TreatmentLength'),'[{"FormPart": "TreatmentDetails", "Field": "TBCategory", "IsObject": true, "ObjectKeyAndValue": {"DR-TB": "336", "DS-TB": "168"}}]',NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field_dependency WHERE field_id = (SELECT id from field WHERE field_name = 'EnrollmentDate') AND lookups = '[{"FormPart": "TreatmentDetails", "Field": "TBTreatmentStartDate", "ConstraintName": "To"}]') THEN
    INSERT INTO field_dependency (type, form_part_id, field_id, lookups, property_and_values) VALUES
	 ('DATE_CONSTRAINT',(SELECT id FROM form_part WHERE part_name = 'TreatmentDetails'),(SELECT id FROM field WHERE field_name = 'EnrollmentDate'),'[{"FormPart": "TreatmentDetails", "Field": "TBTreatmentStartDate", "ConstraintName": "To"}]',NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field_dependency WHERE field_id = (SELECT id from field WHERE field_name = 'MermSerialNoObj') AND lookups = '[{"FormPart": "TreatmentDetails", "Field": "MonitoringMethod", "ExpectedValues": ["MERM"]}]') THEN
    INSERT INTO field_dependency (type, form_part_id, field_id, lookups, property_and_values) VALUES
	 ('VISIBILITY',(SELECT id FROM form_part WHERE part_name = 'TreatmentDetails'),(SELECT id FROM field WHERE field_name = 'MermSerialNoObj'),'[{"FormPart": "TreatmentDetails", "Field": "MonitoringMethod", "ExpectedValues": ["MERM"]}]',NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field_dependency WHERE field_id = (SELECT id from field WHERE field_name = 'MermAlarmEnabled') AND lookups = '[{"FormPart": "TreatmentDetails", "Field": "MonitoringMethod", "ExpectedValues": ["MERM"]}]') THEN
    INSERT INTO field_dependency (type, form_part_id, field_id, lookups, property_and_values) VALUES
	 ('VISIBILITY',(SELECT id FROM form_part WHERE part_name = 'TreatmentDetails'),(SELECT id FROM field WHERE field_name = 'MermAlarmEnabled'),'[{"FormPart": "TreatmentDetails", "Field": "MonitoringMethod", "ExpectedValues": ["MERM"]}]',NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field_dependency WHERE field_id = (SELECT id from field WHERE field_name = 'MermAlarmTime') AND lookups = '[{"FormPart": "TreatmentDetails", "Field": "MermAlarmEnabled", "ExpectedValues": ["true"]}, {"FormPart": "TreatmentDetails", "Field": "MonitoringMethod", "ExpectedValues": ["MERM"]}]') THEN
    INSERT INTO field_dependency (type, form_part_id, field_id, lookups, property_and_values) VALUES
	 ('VISIBILITY',(SELECT id FROM form_part WHERE part_name = 'TreatmentDetails'),(SELECT id FROM field WHERE field_name = 'MermAlarmTime'),'[{"FormPart": "TreatmentDetails", "Field": "MermAlarmEnabled", "ExpectedValues": ["true"]}, {"FormPart": "TreatmentDetails", "Field": "MonitoringMethod", "ExpectedValues": ["MERM"]}]',NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field_dependency WHERE field_id = (SELECT id from field WHERE field_name = 'MermRefillAlarmEnabled') AND lookups = '[{"FormPart": "TreatmentDetails", "Field": "MonitoringMethod", "ExpectedValues": ["MERM"]}]') THEN
    INSERT INTO field_dependency (type, form_part_id, field_id, lookups, property_and_values) VALUES
	 ('VISIBILITY',(SELECT id FROM form_part WHERE part_name = 'TreatmentDetails'),(SELECT id FROM field WHERE field_name = 'MermRefillAlarmEnabled'),'[{"FormPart": "TreatmentDetails", "Field": "MonitoringMethod", "ExpectedValues": ["MERM"]}]',NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field_dependency WHERE field_id = (SELECT id from field WHERE field_name = 'MermRefillAlarmDate') AND lookups = '[{"FormPart": "TreatmentDetails", "Field": "MermRefillAlarmEnabled", "ExpectedValues": ["true"]}, {"FormPart": "TreatmentDetails", "Field": "MonitoringMethod", "ExpectedValues": ["MERM"]}]') THEN
    INSERT INTO field_dependency (type, form_part_id, field_id, lookups, property_and_values) VALUES
	 ('VISIBILITY',(SELECT id FROM form_part WHERE part_name = 'TreatmentDetails'),(SELECT id FROM field WHERE field_name = 'MermRefillAlarmDate'),'[{"FormPart": "TreatmentDetails", "Field": "MermRefillAlarmEnabled", "ExpectedValues": ["true"]}, {"FormPart": "TreatmentDetails", "Field": "MonitoringMethod", "ExpectedValues": ["MERM"]}]',NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field_dependency WHERE field_id = (SELECT id from field WHERE field_name = 'WeightBand') AND lookups = '[{"FormPart": "TreatmentDetails", "Field": "TBCategory", "ExpectedValues": ["DS-TB"]}]') THEN
    INSERT INTO field_dependency (type, form_part_id, field_id, lookups, property_and_values) VALUES
	 ('VISIBILITY',(SELECT id FROM form_part WHERE part_name = 'TreatmentDetails'),(SELECT id FROM field WHERE field_name = 'WeightBand'),'[{"FormPart": "TreatmentDetails", "Field": "TBCategory", "ExpectedValues": ["DS-TB"]}]',NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field_dependency WHERE field_id = (SELECT id from field WHERE field_name = 'NumberOfPills') AND lookups = '[{"FormPart": "TreatmentDetails", "Field": "TBCategory", "ExpectedValues": ["DR-TB"]}]') THEN
    INSERT INTO field_dependency (type, form_part_id, field_id, lookups, property_and_values) VALUES
	 ('VISIBILITY',(SELECT id FROM form_part WHERE part_name = 'TreatmentDetails'),(SELECT id FROM field WHERE field_name = 'NumberOfPills'),'[{"FormPart": "TreatmentDetails", "Field": "TBCategory", "ExpectedValues": ["DR-TB"]}]',NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field_dependency WHERE field_id = (SELECT id from field WHERE field_name = 'MermRefillAlarmTime') AND lookups = '[{"FormPart": "TreatmentDetails", "Field": "MermRefillAlarmEnabled", "ExpectedValues": ["true"]}, {"FormPart": "TreatmentDetails", "Field": "MonitoringMethod", "ExpectedValues": ["MERM"]}]') THEN
    INSERT INTO field_dependency (type, form_part_id, field_id, lookups, property_and_values) VALUES
	 ('VISIBILITY',(SELECT id FROM form_part WHERE part_name = 'TreatmentDetails'),(SELECT id FROM field WHERE field_name = 'MermRefillAlarmTime'),'[{"FormPart": "TreatmentDetails", "Field": "MermRefillAlarmEnabled", "ExpectedValues": ["true"]}, {"FormPart": "TreatmentDetails", "Field": "MonitoringMethod", "ExpectedValues": ["MERM"]}]',NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field_dependency WHERE field_id = (SELECT id from field WHERE field_name = 'MonitoringMethod') AND lookups = '[{"FormPart": "Residence", "Field": "HierarchyMapping_Residence", "AttributeName": "AdherenceTechOptions"}]') THEN
    INSERT INTO field_dependency (type, form_part_id, field_id, lookups, property_and_values) VALUES
	 ('FILTER',(SELECT id FROM form_part WHERE part_name = 'TreatmentDetails'),(SELECT id FROM field WHERE field_name = 'MonitoringMethod'),'[{"FormPart": "Residence", "Field": "HierarchyMapping_Residence", "AttributeName": "AdherenceTechOptions"}]',NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field_dependency WHERE field_id = (SELECT id from field WHERE field_name = 'LastSeen') AND lookups = '[{"FormPart": "TreatmentDetails", "Field": "MermSerialNoObj", "AttributeName":"LastSeen"}]') THEN
    INSERT INTO field_dependency (type, form_part_id, field_id, lookups, property_and_values) VALUES
	 ('VALUE',(SELECT id FROM form_part WHERE part_name = 'TreatmentDetails'),(SELECT id FROM field WHERE field_name = 'LastSeen'),'[{"FormPart": "TreatmentDetails", "Field": "MermSerialNoObj", "AttributeName":"LastSeen"}]',NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field_dependency WHERE field_id = (SELECT id from field WHERE field_name = 'BatteryLevel') AND lookups = '[{"FormPart": "TreatmentDetails", "Field": "MermSerialNoObj", "AttributeName":"BatteryLevel"}]') THEN
    INSERT INTO field_dependency (type, form_part_id, field_id, lookups, property_and_values) VALUES
	 ('VALUE',(SELECT id FROM form_part WHERE part_name = 'TreatmentDetails'),(SELECT id FROM field WHERE field_name = 'BatteryLevel'),'[{"FormPart": "TreatmentDetails", "Field": "MermSerialNoObj", "AttributeName":"BatteryLevel"}]',NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field_dependency WHERE field_id = (SELECT id from field WHERE field_name = 'LastSeen') AND lookups = '[{"FormPart": "TreatmentDetails", "Field": "MermSerialNoObj", "CheckNotNull":"true"},{"FormPart": "TreatmentDetails", "Field": "MonitoringMethod", "ExpectedValues": ["MERM"]}]') THEN
    INSERT INTO field_dependency (type, form_part_id, field_id, lookups, property_and_values) VALUES
	 ('VISIBILITY',(SELECT id FROM form_part WHERE part_name = 'TreatmentDetails'),(SELECT id FROM field WHERE field_name = 'LastSeen'),'[{"FormPart": "TreatmentDetails", "Field": "MermSerialNoObj", "CheckNotNull":"true"},{"FormPart": "TreatmentDetails", "Field": "MonitoringMethod", "ExpectedValues": ["MERM"]}]',NULL);
END IF;

IF NOT EXISTS (SELECT * FROM field_dependency WHERE field_id = (SELECT id from field WHERE field_name = 'BatteryLevel') AND lookups = '[{"FormPart": "TreatmentDetails", "Field": "MermSerialNoObj", "CheckNotNull":"true"},{"FormPart": "TreatmentDetails", "Field": "MonitoringMethod", "ExpectedValues": ["MERM"]}]') THEN
    INSERT INTO field_dependency (type, form_part_id, field_id, lookups, property_and_values) VALUES
	 ('VISIBILITY',(SELECT id FROM form_part WHERE part_name = 'TreatmentDetails'),(SELECT id FROM field WHERE field_name = 'BatteryLevel'),'[{"FormPart": "TreatmentDetails", "Field": "MermSerialNoObj", "CheckNotNull":"true"},{"FormPart": "TreatmentDetails", "Field": "MonitoringMethod", "ExpectedValues": ["MERM"]}]',NULL);
END IF;

IF NOT EXISTS (SELECT * FROM form_items_map WHERE deployment_id = 0 AND form_name = 'Registration') THEN
    INSERT INTO form_items_map (deployment_id, form_name, item_type, item_id, parent_type, parent_id) VALUES
     (0,'Registration','PART',(SELECT id from form_part WHERE part_name = 'BasicDetails'),'FORM',(SELECT id from form WHERE form_name = 'Registration')),
	 (0,'Registration','PART',(SELECT id from form_part WHERE part_name = 'ContactDetails'),'FORM',(SELECT id from form WHERE form_name = 'Registration')),
	 (0,'Registration','PART',(SELECT id from form_part WHERE part_name = 'Residence'),'FORM',(SELECT id from form WHERE form_name = 'Registration')),
	 (0,'Registration','PART',(SELECT id from form_part WHERE part_name = 'TreatmentDetails'),'FORM',(SELECT id from form WHERE form_name = 'Registration')),
	 (0,'Registration','FIELD',(SELECT id from field WHERE field_name = 'FirstName'),'PART',(SELECT id from form_part WHERE part_name = 'BasicDetails')),
	 (0,'Registration','FIELD',(SELECT id from field WHERE field_name = 'LastName'),'PART',(SELECT id from form_part WHERE part_name = 'BasicDetails')),
	 (0,'Registration','FIELD',(SELECT id from field WHERE field_name = 'Address'),'PART',(SELECT id from form_part WHERE part_name = 'BasicDetails')),
	 (0,'Registration','FIELD',(SELECT id from field WHERE field_name = 'Age'),'PART',(SELECT id from form_part WHERE part_name = 'BasicDetails')),
	 (0,'Registration','FIELD',(SELECT id from field WHERE field_name = 'Gender'),'PART',(SELECT id from form_part WHERE part_name = 'BasicDetails')),
	 (0,'Registration','FIELD',(SELECT id from field WHERE field_name = 'PrimaryPhone'),'PART',(SELECT id from form_part WHERE part_name = 'ContactDetails')),
	 (0,'Registration','FIELD',(SELECT id from field WHERE field_name = 'PrimaryPhoneOwner'),'PART',(SELECT id from form_part WHERE part_name = 'ContactDetails')),
	 (0,'Registration','FIELD',(SELECT id from field WHERE field_name = 'SecondaryPhone1'),'PART',(SELECT id from form_part WHERE part_name = 'ContactDetails')),
	 (0,'Registration','FIELD',(SELECT id from field WHERE field_name = 'SecondaryPhone1Owner'),'PART',(SELECT id from form_part WHERE part_name = 'ContactDetails')),
	 (0,'Registration','FIELD',(SELECT id from field WHERE field_name = 'SecondaryPhone2'),'PART',(SELECT id from form_part WHERE part_name = 'ContactDetails')),
	 (0,'Registration','FIELD',(SELECT id from field WHERE field_name = 'SecondaryPhone2Owner'),'PART',(SELECT id from form_part WHERE part_name = 'ContactDetails')),
	 (0,'Registration','FIELD',(SELECT id from field WHERE field_name = 'SecondaryPhone3'),'PART',(SELECT id from form_part WHERE part_name = 'ContactDetails')),
	 (0,'Registration','FIELD',(SELECT id from field WHERE field_name = 'SecondaryPhone3Owner'),'PART',(SELECT id from form_part WHERE part_name = 'ContactDetails')),
	 (0,'Registration','FIELD',(SELECT id from field WHERE field_name = 'HierarchyMapping_Residence'),'PART',(SELECT id from form_part WHERE part_name = 'Residence')),
	 (0,'Registration','FIELD',(SELECT id from field WHERE field_name = 'TBTreatmentStartDate'),'PART',(SELECT id from form_part WHERE part_name = 'TreatmentDetails')),
	 (0,'Registration','FIELD',(SELECT id from field WHERE field_name = 'EnrollmentDate'),'PART',(SELECT id from form_part WHERE part_name = 'TreatmentDetails')),
	 (0,'Registration','FIELD',(SELECT id from field WHERE field_name = 'TBCategory'),'PART',(SELECT id from form_part WHERE part_name = 'TreatmentDetails')),
	 (0,'Registration','FIELD',(SELECT id from field WHERE field_name = 'WeightBand'),'PART',(SELECT id from form_part WHERE part_name = 'TreatmentDetails')),
	 (0,'Registration','FIELD',(SELECT id from field WHERE field_name = 'MonitoringMethod'),'PART',(SELECT id from form_part WHERE part_name = 'TreatmentDetails')),
	 (0,'Registration','FIELD',(SELECT id from field WHERE field_name = 'MermSerialNoObj'),'PART',(SELECT id from form_part WHERE part_name = 'TreatmentDetails')),
	 (0,'Registration','FIELD',(SELECT id from field WHERE field_name = 'MermAlarmEnabled'),'PART',(SELECT id from form_part WHERE part_name = 'TreatmentDetails')),
	 (0,'Registration','FIELD',(SELECT id from field WHERE field_name = 'MermRefillAlarmEnabled'),'PART',(SELECT id from form_part WHERE part_name = 'TreatmentDetails')),
	 (0,'Registration','FIELD',(SELECT id from field WHERE field_name = 'MermRefillAlarmDate'),'PART',(SELECT id from form_part WHERE part_name = 'TreatmentDetails')),
	 (0,'Registration','FIELD',(SELECT id from field WHERE field_name = 'ExternalId1'),'PART',(SELECT id from form_part WHERE part_name = 'TreatmentDetails')),
	 (0,'Registration','FIELD',(SELECT id from field WHERE field_name = 'TreatmentLength'),'PART',(SELECT id from form_part WHERE part_name = 'TreatmentDetails')),
	 (0,'Registration','FIELD',(SELECT id from field WHERE field_name = 'PhoneNetwork'),'PART',(SELECT id from form_part WHERE part_name = 'TreatmentDetails')),
	 (0,'Registration','FIELD',(SELECT id from field WHERE field_name = 'MermAlarmTime'),'PART',(SELECT id from form_part WHERE part_name = 'TreatmentDetails')),
	 (0,'Registration','FIELD',(SELECT id from field WHERE field_name = 'MermRefillAlarmTime'),'PART',(SELECT id from form_part WHERE part_name = 'TreatmentDetails')),
	 (0,'Registration','FIELD',(SELECT id from field WHERE field_name = 'NumberOfPills'),'PART',(SELECT id from form_part WHERE part_name = 'TreatmentDetails')),
	 (0,'Registration','FIELD',(SELECT id from field WHERE field_name = 'AddedFrom'),'PART',(SELECT id from form_part WHERE part_name = 'TreatmentDetails')),
	 (0,'Registration','DEPENDENCY',(SELECT id from field_dependency WHERE field_id = ((SELECT id from field WHERE field_name = 'TreatmentLength'))),'FIELD',(SELECT id from field WHERE field_name = 'TreatmentLength')),
	 (0,'Registration','DEPENDENCY',(SELECT id from field_dependency WHERE field_id = ((SELECT id from field WHERE field_name = 'EnrollmentDate')) AND type = 'DATE_CONSTRAINT'),'FIELD',(SELECT id from field WHERE field_name = 'EnrollmentDate')),
	 (0,'Registration','DEPENDENCY',(SELECT id from field_dependency WHERE field_id = ((SELECT id from field WHERE field_name = 'MermSerialNoObj'))),'FIELD',(SELECT id from field WHERE field_name = 'MermSerialNoObj')),
	 (0,'Registration','DEPENDENCY',(SELECT id from field_dependency WHERE field_id = ((SELECT id from field WHERE field_name = 'MermAlarmEnabled'))),'FIELD',(SELECT id from field WHERE field_name = 'MermAlarmEnabled')),
	 (0,'Registration','DEPENDENCY',(SELECT id from field_dependency WHERE field_id = ((SELECT id from field WHERE field_name = 'MermAlarmTime'))),'FIELD',(SELECT id from field WHERE field_name = 'MermAlarmTime')),
	 (0,'Registration','DEPENDENCY',(SELECT id from field_dependency WHERE field_id = ((SELECT id from field WHERE field_name = 'MermRefillAlarmEnabled'))),'FIELD',(SELECT id from field WHERE field_name = 'MermRefillAlarmEnabled')),
	 (0,'Registration','DEPENDENCY',(SELECT id from field_dependency WHERE field_id = ((SELECT id from field WHERE field_name = 'MermRefillAlarmDate'))),'FIELD',(SELECT id from field WHERE field_name = 'MermRefillAlarmDate')),
	 (0,'Registration','DEPENDENCY',(SELECT id from field_dependency WHERE field_id = ((SELECT id from field WHERE field_name = 'MermRefillAlarmTime'))),'FIELD',(SELECT id from field WHERE field_name = 'MermRefillAlarmTime')),
	 (0,'Registration','DEPENDENCY',(SELECT id from field_dependency WHERE field_id = ((SELECT id from field WHERE field_name = 'WeightBand'))),'FIELD',(SELECT id from field WHERE field_name = 'WeightBand')),
	 (0,'Registration','DEPENDENCY',(SELECT id from field_dependency WHERE field_id = ((SELECT id from field WHERE field_name = 'NumberOfPills'))),'FIELD',(SELECT id from field WHERE field_name = 'NumberOfPills')),
	 (0,'Registration','DEPENDENCY',(SELECT id from field_dependency WHERE field_id = ((SELECT id from field WHERE field_name = 'MonitoringMethod'))),'FIELD',(SELECT id from field WHERE field_name = 'MonitoringMethod')),
	 (0,'Registration','CUSTOMIZATION',(SELECT id FROM field_customizations WHERE attribute_value = 'PHONE_PREFIX'),'FIELD',(SELECT id from field WHERE field_name = 'PrimaryPhone')),
	 (0,'Registration','CUSTOMIZATION',(SELECT id FROM field_customizations WHERE attribute_value = 'PHONE_PREFIX'),'FIELD',(SELECT id from field WHERE field_name = 'SecondaryPhone1')),
	 (0,'Registration','CUSTOMIZATION',(SELECT id FROM field_customizations WHERE attribute_value = 'PHONE_PREFIX'),'FIELD',(SELECT id from field WHERE field_name = 'SecondaryPhone2')),
	 (0,'Registration','CUSTOMIZATION',(SELECT id FROM field_customizations WHERE attribute_value = 'PHONE_PREFIX'),'FIELD',(SELECT id from field WHERE field_name = 'SecondaryPhone3')),
	 (0,'Registration','CUSTOMIZATION',(SELECT id FROM field_customizations WHERE attribute_value = 'select_treatment_center'),'FIELD',(SELECT id from field WHERE field_name = 'HierarchyMapping_Residence')),
	 (0,'Registration','FIELD',(SELECT id from field WHERE field_name = 'LastSeen'),'PART',(SELECT id from form_part WHERE part_name = 'TreatmentDetails')),
	 (0,'Registration','FIELD',(SELECT id from field WHERE field_name = 'BatteryLevel'),'PART',(SELECT id from form_part WHERE part_name = 'TreatmentDetails')),
	 (0,'Registration','DEPENDENCY',(SELECT id from field_dependency WHERE field_id = (SELECT id from field WHERE field_name = 'LastSeen') AND type = 'VISIBILITY'),'FORM',(SELECT id from form WHERE form_name = 'Registration')),
	 (0,'Registration','DEPENDENCY',(SELECT id from field_dependency WHERE field_id = (SELECT id from field WHERE field_name = 'BatteryLevel') AND type = 'VISIBILITY'),'FORM',(SELECT id from form WHERE form_name = 'Registration')),
	 (0,'Registration','DEPENDENCY',(SELECT id from field_dependency WHERE field_id = (SELECT id from field WHERE field_name = 'LastSeen') AND type = 'VALUE'),'FORM',(SELECT id from form WHERE form_name = 'Registration')),
	 (0,'Registration','DEPENDENCY',(SELECT id from field_dependency WHERE field_id = (SELECT id from field WHERE field_name = 'BatteryLevel') AND type = 'VALUE'),'FORM',(SELECT id from form WHERE form_name = 'Registration'));
END IF;

END;
$do$
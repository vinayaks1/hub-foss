create table client (
    id bigserial not null,
    name varchar(255),
    password varchar(255),
    event_flow_id bigint,
    created_date timestamp,
    primary key (id)
);

create table deployment (
    id bigserial not null,
    code varchar(255),
    country_name varchar(100),
    project_name varchar(100),
    default_language_id bigint,
    time_zone varchar(255),
    time_zone_abbreviation varchar(255),
    time_zone_details varchar(255),
    default_tech varchar(50),
    phone_prefix integer,
    short_code varchar(10),
    ask_for_help_url text,
    default_time varchar(10),
    primary key (id)
);

create table language (
    id  bigserial not null,
    name varchar(255),
    translation_key varchar(255),
    primary key (id)
);

create table deployment_language_map (
    id  bigserial not null,
    deployment_id bigint,
    language_id bigint,
    created_at timestamp,
    stopped_at timestamp,
    primary key (id)
);
create index if not exists associations_master_type on associations_master(type);

create index if not exists config_name on config(name);

create index if not exists hierarchy_associations_hierarchy_id on hierarchy_associations(hierarchy_id);

create index if not exists hierarchy_config_map_config_mapping_id on hierarchy_config_map(config_mapping_id);

create index if not exists hierarchy_config_map_hierarchy_id_and_config_mapping_id on hierarchy_config_map(hierarchy_id,config_mapping_id);

create index if not exists hierarchy_client_id_and_id_and_active on hierarchy(client_id, id, active);

create index if not exists hierarchy_client_id_and_id_and_active on hierarchy(client_id, id, active);

create index if not exists hierarchy_client_id_parent_id on hierarchy(client_id, parent_id);

create index if not exists hierarchy_level_1_id on hierarchy(level_1_id);

create index if not exists hierarchy_level_2_id on hierarchy(level_2_id);

create index if not exists hierarchy_level_3_id on hierarchy(level_3_id);

create index if not exists hierarchy_level_4_id on hierarchy(level_4_id);

create index if not exists hierarchy_level_5_id on hierarchy(level_5_id);

create index if not exists user_access_ssoId on user_access(sso_id);

create index if not exists user_access_hierarchy_map_user_id on user_access_hierarchy_map(user_id);
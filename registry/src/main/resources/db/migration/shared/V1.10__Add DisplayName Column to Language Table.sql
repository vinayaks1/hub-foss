ALTER TABLE Language
    ADD if not exists displayName varchar(255);

update Language set displayName = 'English' where name = 'English';
update Language set displayName = 'አማርኛ (Amharic)' where name = 'Amharic';
update Language set displayName = 'বাংলা (Bangla)' where name = 'Bengali';
update Language set displayName = 'qɯɾʁɯz (Kirghiz)' where name = 'Kirghiz';
update Language set displayName = 'Afaan Oromoo' where name = 'Oromo';
update Language set displayName = 'Português (Portuguese)' where name = 'Portuguese';
update Language set displayName = 'Русский язык (Russian)' where name = 'Russian';
update Language set displayName = 'Kiswahili (Swahili)' where name = 'Swahili';
update Language set displayName = 'Filipino (Tagalog and English)'where name = 'Tagalog';
update Language set displayName = 'Українська (Ukrainian)' where name = 'Ukrainian';
update Language set displayName = 'Français (French)' where name = 'French';
update Language set displayName = 'മലയാളം (Malayalam)' where name = 'Malayalam';
update Language set displayName = 'தமிழ் (Tamil)' where name = 'Tamil';
update Language set displayName = 'ગુજરાતી (Gujarati)' where name = 'Gujarati';
update Language set displayName = 'िन्दी (Hindi)' where name = 'Hindi';
update Language set displayName = 'मराठी (Marathi)' where name = 'Marathi';
-- Add new languages

INSERT INTO language (name, translation_key) VALUES ('French', 'fr');

INSERT INTO language (name, translation_key) VALUES ('Malayalam', 'ml');

INSERT INTO language (name, translation_key) VALUES ('Tamil', 'ta');

INSERT INTO language (name, translation_key) VALUES ('Gujarati', 'gu');

INSERT INTO language (name, translation_key) VALUES ('Hindi', 'hi');

INSERT INTO language (name, translation_key) VALUES ('Marathi', 'mr');

INSERT INTO language (name, translation_key) VALUES ('Telugu', 'te');


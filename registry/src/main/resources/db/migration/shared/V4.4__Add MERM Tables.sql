DO
$do$
BEGIN

create table if not exists merm_management
(
    id                   bigint not null,
    imei                 varchar(255),
    last_battery         varchar(255),
    last_seen            timestamp,
    last_opened          timestamp,
    active               boolean,
    deactivated_on       timestamp,
    alarm_enabled        boolean,
    alarm_time           timestamp,
    refill_alarm_enabled boolean,
    refill_date          timestamp,
    rt_hours             bigint,
    hierarchy_mapping    bigint,
    lid_feedback         bigint,
    current_status       varchar(50),
    primary key (id)
);

if not exists (select * from config where name = 'MermMappingLevel') then
    insert into config(name, default_value, type, created_at, multi_mapped)
    values('MermMappingLevel', '1', 'string', NOW(), false);
end if;

END;
$do$
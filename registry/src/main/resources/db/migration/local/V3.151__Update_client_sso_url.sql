-- SSO for Hub runs on port 9099
update client set sso_url = 'http://localhost:9099' where name = 'Hub India';
update client set sso_url = 'http://localhost:9099' where name = 'Hub Tanzania';
update client set sso_url = 'http://localhost:9099' where name = 'Hub South Africa';

-- SSO for Nikshay runs on port 9098
update client set sso_url = 'http://localhost:9098' where name = 'Nikshay';

-- SSO for Aaro flavour runs on port 9097
update client set sso_url = 'http://localhost:9097' where name = 'AaroHealth';
 # E Hub FOSS

Patient Management with Adherence and Communication Support as designed for disease tracking.

## Installation and Setup

The Everwell Hub system is a host of independent business modules based patient care and tracking software application. Keeping in mind the different needs of support across countries, the scale should support all realtime, near-realtime and asynchronous data updates and inserts. 

Depending on each individual business component, the scale can vary and should thus be designed as micro services across the backend technical support. Adopting creation of micro services, also allows smaller, independently managed and updated components
easily separating and helping in plug and play of the business logic.

These are following business modules which make up the Everwell Hub FOSS

### Data Gateway:
The Gateway system supports multi format structures to allow for ease of integration and is the one source of traffic for all internal and external requests to / from the Hub. 

To read more on installation, setup and deployment [README.md](https://gitlab.com/everwell/hub-foss/-/blob/master/data-gateway/README.md) It is coded in Java using the Springboot framework.

### Integrated Adherence Management: 
The Adherence system manages the medication tracking for each entity within the Everwell Hub.

To read more on installation, setup and deployment [README.md](https://gitlab.com/everwell/hub-foss/-/blob/master/iam-backend/README.md) It is coded in Java using the Springboot framework.

### Integrated Notification System: 
The Notification system manages the consent, specific schedules and ways of notifiyinh each entity within the Everwell Hub.

To read more on installation, setup and deployment [README.md](https://gitlab.com/everwell/hub-foss/-/blob/master/ins/README.md) It is coded in Java using the Springboot framework.

### Dispensation:
The Dispensation manages the medicine inventory and prescription management for each entity within the Everwell Hub. 

To read more on installation, setup and deployment [README.md](https://gitlab.com/everwell/hub-foss/-/blob/master/dispensation-service/README.md) It is coded in Java using the Springboot framework.

### Registry: 
The Registry system manages the deployment, facility and related staff configuration within the Everwell Hub. It uses a system of Rabbit MQ for asynchronous communication,Redis for caching and Cloud Config for configuration management. The service also auto initialises the Job Runner scheduler. It is coded in Java using the Springboot framework.

To read more on installation, setup and deployment [README.md](https://gitlab.com/everwell/hub-foss/-/blob/master/registry/README.md) 

### Unified Web UI: 
The unified web ui has the latest and modern designs and flavour based pages and components to allow to dynamic server configured behaviour. It is coded in Vue.js.

To read more on installation, setup and deployment [README.md](https://gitlab.com/everwell/hub-foss/-/blob/master/unified-web-ui/README.md)

### User Service:
User Service is created to abstract user management features across all Everwell services.

To read more on installation, setup and deployment [README.md](https://gitlab.com/everwell/hub-foss/-/blob/master/UserService/README.md)

### Analytics driven by Superset:
The analytics code is modelled atop the Apache Superset and allows for visualizing, analysing and reporting the data captured through the Hub platform.

To read more on installation, setup and deployment [README.md](https://gitlab.com/everwell/superset/-/blob/master/README.md)

## License
The software is open source and released under the [MIT License](https://opensource.org/licenses/MIT)

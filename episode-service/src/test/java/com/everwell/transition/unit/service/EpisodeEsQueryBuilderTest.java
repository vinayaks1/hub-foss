package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.elasticsearch.ElasticConstants;
import com.everwell.transition.elasticsearch.service.Impl.EpisodeEsQueryBuilder;
import com.everwell.transition.model.dto.AggregationDto;
import com.everwell.transition.model.request.episodesearch.EpisodeSearchRequest;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.search.aggregations.AbstractAggregationBuilder;
import org.junit.Assert;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.junit.Test;

import java.io.IOException;
import java.util.*;


public class EpisodeEsQueryBuilderTest extends BaseTest {

    @Spy
    @InjectMocks
    EpisodeEsQueryBuilder episodeEsQueryBuilder;

    @Test
    public void buildMustTest () throws IOException {
        Map<String, Object> must = new HashMap<>();
        must.put("firstName", "Ram");
        must.put("treatmentOutcome", "Passed");
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest(
                new EpisodeSearchRequest.SupportedSearchMethods(
                        must, new HashMap<>(), new HashMap<>()
                ),
                new HashMap<>()
        );
        episodeSearchRequest.setKeywordFields(Collections.singletonList("treatmentOutcome"));
        Map<String, String> map = new HashMap<>();
        map.put("treatmentOutcome", "stageData");
        map.put("firstName", "stageData");
        map.put("lastName", "stageData");
        BoolQueryBuilder boolQueryBuilder = episodeEsQueryBuilder.build(episodeSearchRequest, map);
        Map<String, Object> queryMap = Utils.jsonToObject(boolQueryBuilder.toString(), new TypeReference<Map<String, Object>>() {});
        Map<String, Object> boolMap = (Map<String, Object>) queryMap.get("bool");
        Assert.assertTrue(boolMap.containsKey("filter"));
        List<Map<String, Object>> filterMapList = ((List<Map<String, Object>>) boolMap.get("filter"));
        Assert.assertTrue(filterMapList.get(0).containsKey("term"));
        Map<String, Object> clauseValueMap = (Map<String, Object>) filterMapList.get(0).get("term");
        Assert.assertTrue(clauseValueMap.containsKey("stageData.firstName"));
        Map<String, Object> clauseValueMapTreatmentOutcome = (Map<String, Object>) filterMapList.get(1).get("term");
        Assert.assertTrue(clauseValueMapTreatmentOutcome.containsKey("stageData.treatmentOutcome.keyword"));
    }

    @Test
    public void buildMustNotTest () throws IOException {
        Map<String, Object> mustNot = new HashMap<>();
        mustNot.put("firstName", "Ram");
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest(
                new EpisodeSearchRequest.SupportedSearchMethods(
                        new HashMap<>(), mustNot, new HashMap<>()
                        ),
                new HashMap<>()
        );
        Map<String, String> map = new HashMap<>();
        map.put("treatmentOutcome", "stageData");
        map.put("firstName", "stageData");
        map.put("lastName", "stageData");
        BoolQueryBuilder boolQueryBuilder = episodeEsQueryBuilder.build(episodeSearchRequest, map);
        Map<String, Object> queryMap = Utils.jsonToObject(boolQueryBuilder.toString(), new TypeReference<Map<String, Object>>() {});
        Map<String, Object> boolMap = (Map<String, Object>) queryMap.get("bool");
        Assert.assertTrue(boolMap.containsKey("must_not"));
        Map<String, Object> filterMapList = ((List<Map<String, Object>>) boolMap.get("must_not")).get(0);
        Assert.assertTrue(filterMapList.containsKey("term"));
        Map<String, Object> clauseValueMap = (Map<String, Object>) filterMapList.get("term");
        Assert.assertTrue(clauseValueMap.containsKey("stageData.firstName"));
    }

    @Test
    public void buildMustExcludeKeywordTest () throws IOException {
        Map<String, Object> must = new HashMap<>();
        must.put("id", "1234");
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest(
                new EpisodeSearchRequest.SupportedSearchMethods(
                        must, new HashMap<>(), new HashMap<>()
                ),
                new HashMap<>()
        );
        Map<String, String> map = new HashMap<>();
        map.put("TreatmentOutcome", "stageData");
        map.put("FirstName", "stageData");
        map.put("LastName", "stageData");
        BoolQueryBuilder boolQueryBuilder = episodeEsQueryBuilder.build(episodeSearchRequest, map);
        Map<String, Object> queryMap = Utils.jsonToObject(boolQueryBuilder.toString(), new TypeReference<Map<String, Object>>() {});
        Map<String, Object> boolMap = (Map<String, Object>) queryMap.get("bool");
        Assert.assertTrue(boolMap.containsKey("filter"));
        Map<String, Object> filterMapList = ((List<Map<String, Object>>) boolMap.get("filter")).get(0);
        Assert.assertTrue(filterMapList.containsKey("term"));
        Map<String, Object> clauseValueMap = (Map<String, Object>) filterMapList.get("term");
        Assert.assertTrue(clauseValueMap.containsKey("id"));
    }

    @Test
    public void buildMustMatchTest () throws IOException {
        Map<String, Object> must = new HashMap<>();
        must.put(FieldConstants.FIELD_STAGE_NAME, "name");
        must.put(FieldConstants.EPISODE_FIELD_DISEASE_ID_OPTIONS, "1,2");
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest(
                new EpisodeSearchRequest.SupportedSearchMethods(
                        must, new HashMap<>(), new HashMap<>()
                ),
                new HashMap<>()
        );
        Map<String, String> map = new HashMap<>();
        map.put("TreatmentOutcome", "stageData");
        map.put("FirstName", "stageData");
        map.put("LastName", "stageData");
        BoolQueryBuilder boolQueryBuilder = episodeEsQueryBuilder.build(episodeSearchRequest, map);
        Map<String, Object> queryMap = Utils.jsonToObject(boolQueryBuilder.toString(), new TypeReference<Map<String, Object>>() {});
        Map<String, Object> boolMap = (Map<String, Object>) queryMap.get("bool");
        Assert.assertTrue(boolMap.containsKey("filter"));
        Map<String, Object> matchPhrasePrefixFilterMapList = ((List<Map<String, Object>>) boolMap.get("filter")).get(0);
        Assert.assertTrue(matchPhrasePrefixFilterMapList.containsKey("match_phrase_prefix"));
        Map<String, Object> matchPhrasePrefixClauseValueMap = (Map<String, Object>) matchPhrasePrefixFilterMapList.get("match_phrase_prefix");
        Assert.assertTrue(matchPhrasePrefixClauseValueMap.containsKey(FieldConstants.FIELD_STAGE_NAME));
        Map<String, Object> matchFilterMapList = ((List<Map<String, Object>>) boolMap.get("filter")).get(1);
        Assert.assertTrue(matchFilterMapList.containsKey("match"));
        Map<String, Object> matchClauseValueMap = (Map<String, Object>) matchFilterMapList.get("match");
        Assert.assertTrue(matchClauseValueMap.containsKey(FieldConstants.EPISODE_FIELD_DISEASE_ID_OPTIONS));
    }

    @Test
    public void buildMustTermsMatchTest () throws IOException {
        Map<String, Object> must = new HashMap<>();
        must.put("current_all", Arrays.asList(1L, 2L, 3L));
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest(
                new EpisodeSearchRequest.SupportedSearchMethods(
                        must, new HashMap<>(), new HashMap<>()
                ),
                new HashMap<>()
        );
        Map<String, String> map = new HashMap<>();
        map.put("TreatmentOutcome", "stageData");
        map.put("FirstName", "stageData");
        map.put("LastName", "stageData");
        map.put("current_all", "hierarchyMappings");
        BoolQueryBuilder boolQueryBuilder = episodeEsQueryBuilder.build(episodeSearchRequest, map);
        Map<String, Object> queryMap = Utils.jsonToObject(boolQueryBuilder.toString(), new TypeReference<Map<String, Object>>() {});
        Map<String, Object> boolMap = (Map<String, Object>) queryMap.get("bool");
        Assert.assertTrue(boolMap.containsKey("filter"));
        Map<String, Object> filterMapList = ((List<Map<String, Object>>) boolMap.get("filter")).get(0);
        Assert.assertTrue(filterMapList.containsKey("terms"));
        Map<String, Object> clauseValueMap = (Map<String, Object>) filterMapList.get("terms");
        Assert.assertTrue(clauseValueMap.containsKey("hierarchyMappings.current_all"));
    }

    @Test
    public void buildRangeTest () throws IOException {
        Map<String, Object> range = new HashMap<>();
        Map<String, String> rangeVal = new HashMap<>();
        rangeVal.put("gt", "2010");
        rangeVal.put("lt", "2015");
        range.put("startDate", rangeVal);
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest(
                new EpisodeSearchRequest.SupportedSearchMethods(
                        new HashMap<>(), new HashMap<>(), new HashMap<>()
                ),
                range
        );
        Map<String, String> map = new HashMap<>();
        map.put("TreatmentOutcome", "stageData");
        map.put("FirstName", "stageData");
        map.put("LastName", "stageData");
        BoolQueryBuilder boolQueryBuilder = episodeEsQueryBuilder.build(episodeSearchRequest, map);
        Map<String, Object> queryMap = Utils.jsonToObject(boolQueryBuilder.toString(), new TypeReference<Map<String, Object>>() {});
        Map<String, Object> boolMap = (Map<String, Object>) queryMap.get("bool");
        Assert.assertTrue(boolMap.containsKey("filter"));
        Map<String, Object> filterMapList = ((List<Map<String, Object>>) boolMap.get("filter")).get(0);
        Assert.assertTrue(filterMapList.containsKey("range"));
        Map<String, Object> clauseValueMap = (Map<String, Object>) filterMapList.get("range");
        Assert.assertTrue(clauseValueMap.containsKey("startDate"));
        Map<String, Object> clauseStartDateValueMap = (Map<String, Object>) clauseValueMap.get("startDate");
        Assert.assertFalse((boolean) clauseStartDateValueMap.get("include_lower"));
        Assert.assertFalse((boolean) clauseStartDateValueMap.get("include_upper"));
    }

    @Test
    public void buildRange2Test () throws IOException {
        Map<String, Object> range = new HashMap<>();
        Map<String, String> rangeVal = new HashMap<>();
        rangeVal.put(ElasticConstants.ELASTIC_SEARCH_RANGE_GREATER_THAN_EQUAL, "2010");
        rangeVal.put(ElasticConstants.ELASTIC_SEARCH_RANGE_LOWER_THAN_EQUAL, "2015");
        range.put("startDate", rangeVal);
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest(
                new EpisodeSearchRequest.SupportedSearchMethods(
                        new HashMap<>(), new HashMap<>(), new HashMap<>()
                ),
                range
        );
        Map<String, String> map = new HashMap<>();
        map.put("TreatmentOutcome", "stageData");
        map.put("FirstName", "stageData");
        map.put("LastName", "stageData");
        BoolQueryBuilder boolQueryBuilder = episodeEsQueryBuilder.build(episodeSearchRequest, map);
        Map<String, Object> queryMap = Utils.jsonToObject(boolQueryBuilder.toString(), new TypeReference<Map<String, Object>>() {});
        Map<String, Object> boolMap = (Map<String, Object>) queryMap.get("bool");
        Assert.assertTrue(boolMap.containsKey("filter"));
        Map<String, Object> filterMapList = ((List<Map<String, Object>>) boolMap.get("filter")).get(0);
        Assert.assertTrue(filterMapList.containsKey("range"));
        Map<String, Object> clauseValueMap = (Map<String, Object>) filterMapList.get("range");
        Assert.assertTrue(clauseValueMap.containsKey("startDate"));
        Map<String, Object> clauseStartDateValueMap = (Map<String, Object>) clauseValueMap.get("startDate");
        Assert.assertTrue((boolean) clauseStartDateValueMap.get("include_lower"));
        Assert.assertTrue((boolean) clauseStartDateValueMap.get("include_upper"));
    }

    @Test
    public void buildAggregationTest() {
        EpisodeSearchRequest.SupportedAggMethods supportedAggMethods = new EpisodeSearchRequest.SupportedAggMethods(Collections.singletonList(new AggregationDto(10, "stageCount", "stageName")), null);
        List<AbstractAggregationBuilder<?>> aggregationBuilderList = new ArrayList<>();

        List<AbstractAggregationBuilder<?>> response = episodeEsQueryBuilder.buildAggregation(supportedAggMethods);
        Assert.assertEquals(1, response.size());
        Assert.assertEquals("stageCount", response.get(0).getName());
    }

    @Test
    public void buildAggregationTest_DateAggregation() {
        EpisodeSearchRequest.SupportedAggMethods supportedAggMethods = new EpisodeSearchRequest.SupportedAggMethods(null, Collections.singletonList(new AggregationDto(10, "enrollmentCount", "startDate")));

        List<AbstractAggregationBuilder<?>> response = episodeEsQueryBuilder.buildAggregation(supportedAggMethods);
        Assert.assertEquals(1, response.size());
        Assert.assertEquals("enrollmentCount", response.get(0).getName());
    }

    @Test
    public void buildContainsTest () throws IOException {
        Map<String, Object> contains = new HashMap<>();
        contains.put("firstName", "Ram");
        contains.put("treatmentOutcome", "Passed");
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest(
                new EpisodeSearchRequest.SupportedSearchMethods(
                        new HashMap<>(), new HashMap<>(), contains
                ),
                new HashMap<>()
        );
        episodeSearchRequest.setKeywordFields(Collections.singletonList("treatmentOutcome"));
        Map<String, String> map = new HashMap<>();
        map.put("treatmentOutcome", "stageData");
        map.put("firstName", "stageData");
        map.put("lastName", "stageData");
        BoolQueryBuilder boolQueryBuilder = episodeEsQueryBuilder.build(episodeSearchRequest, map);
        Map<String, Object> queryMap = Utils.jsonToObject(boolQueryBuilder.toString(), new TypeReference<Map<String, Object>>() {});
        Map<String, Object> boolMap = (Map<String, Object>) queryMap.get("bool");
        Assert.assertTrue(boolMap.containsKey("filter"));
        List<Map<String, Object>> filterMapList = ((List<Map<String, Object>>) boolMap.get("filter"));
        Assert.assertTrue(filterMapList.get(0).containsKey("wildcard"));
        Map<String, Object> clauseValueMap = (Map<String, Object>) filterMapList.get(0).get("wildcard");
        Assert.assertTrue(clauseValueMap.containsKey("stageData.firstName"));
        Map<String, Object> clauseValueMapTreatmentOutcome = (Map<String, Object>) filterMapList.get(1).get("wildcard");
        Assert.assertTrue(clauseValueMapTreatmentOutcome.containsKey("stageData.treatmentOutcome.keyword"));
    }

}

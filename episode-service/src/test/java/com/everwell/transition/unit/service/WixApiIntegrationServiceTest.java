package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.db.VendorConfig;
import com.everwell.transition.model.dto.wix.WixPostFilter;
import com.everwell.transition.model.request.wix.WixPostResponse;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.repositories.VendorConfigRepository;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.impl.UserServiceImpl;
import com.everwell.transition.service.impl.WixApiIntegrationServiceImpl;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import org.checkerframework.checker.units.qual.C;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThatCode;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static springfox.documentation.builders.PathSelectors.any;

public class WixApiIntegrationServiceTest extends BaseTest {

    @InjectMocks
    @Spy
    WixApiIntegrationServiceImpl wixApiIntegrationService;

    @Mock
    VendorConfigRepository vendorConfigRepository;

    @Mock
    ClientService clientService;

    @Mock
    RestTemplate restTemplate;

    Map<String, String> credentialsMap = new HashMap<>();

    @Before
    public void init () {
        credentialsMap.put("client_id", "client_id");
        credentialsMap.put("client_secret", "client_secret");
        credentialsMap.put("refresh_token", "refresh_token");
        credentialsMap.put("code", "code");
        credentialsMap.put("base_site_url", "base_site_url");
    }

    @Test
    public void listPostsTest() throws IOException {

        ResponseEntity<Map<String, Object>> response = new ResponseEntity<>(postDate(), HttpStatus.OK);;

        when(restTemplate.exchange(anyString(), eq(HttpMethod.GET), ArgumentMatchers.any(HttpEntity.class), ArgumentMatchers.any(ParameterizedTypeReference.class)))
                .thenReturn(response);

        List<WixPostResponse> wixPostResponses = wixApiIntegrationService.listPosts(new WixPostFilter(true, Arrays.asList("1")));

        Assert.assertEquals(1, wixPostResponses.size());
        Assert.assertEquals("UnderstandAsthma", wixPostResponses.get(0).getTitle());

    }

    @Test
    public void listPosts_ExceptionTest() throws IOException {
        HttpClientErrorException forbiddenException = HttpClientErrorException.Forbidden.create(HttpStatus.FORBIDDEN, null, null, null, null);

        when(restTemplate.exchange(anyString(), eq(HttpMethod.GET), ArgumentMatchers.any(HttpEntity.class), ArgumentMatchers.any(ParameterizedTypeReference.class)))
                .thenThrow(forbiddenException);

        doNothing().when(wixApiIntegrationService).setAuthToken();

        List<WixPostResponse> wixPostResponses = wixApiIntegrationService.listPosts(new WixPostFilter(true, Arrays.asList("1")));

        Mockito.verify(wixApiIntegrationService, Mockito.times(3)).setAuthToken();

    }

    @Test
    public void listCategoriesFilterTest() throws IOException {

        ResponseEntity<Map<String, Object>> response = new ResponseEntity<>(categoryData(), HttpStatus.OK);;

        when(restTemplate.exchange(anyString(), eq(HttpMethod.POST), ArgumentMatchers.any(HttpEntity.class), ArgumentMatchers.any(ParameterizedTypeReference.class)))
                .thenReturn(response);

        Map<String, String> filters = new HashMap<>();
        filters.put("language", "en");

        Map<String, WixPostResponse> wixCategoryResponseMap = wixApiIntegrationService.listCategoriesFilter(filters);

        Assert.assertEquals(1, wixCategoryResponseMap.size());
        Assert.assertEquals("ashtma", wixCategoryResponseMap.get("bbd4d8a9-a645-447e-bc45-88545febe90f").getCategory());

    }

    @Test
    public void listCategoriesFilter_ExceptionTest() throws IOException {

        HttpClientErrorException forbiddenException = HttpClientErrorException.Forbidden.create(HttpStatus.FORBIDDEN, null, null, null, null);

        when(restTemplate.exchange(anyString(), eq(HttpMethod.POST), ArgumentMatchers.any(HttpEntity.class), ArgumentMatchers.any(ParameterizedTypeReference.class)))
                .thenThrow(forbiddenException);

        Map<String, String> filters = new HashMap<>();
        filters.put("language", "en");

        doNothing().when(wixApiIntegrationService).setAuthToken();

        Map<String, WixPostResponse> wixCategoryResponseMap = wixApiIntegrationService.listCategoriesFilter(filters);

        Mockito.verify(wixApiIntegrationService, Mockito.times(3)).setAuthToken();

    }

    @Test
    public void refreshTokenCalledFromAuthTokenTest() {

        Map<String, Object> authResponse = new HashMap<>();
        authResponse.put("access_token", "access_token");
        authResponse.put("refresh_token", "refresh_token");

        ResponseEntity<Map<String, Object>> response = new ResponseEntity<>(authResponse, HttpStatus.OK);;

        when(restTemplate.exchange(anyString(), eq(HttpMethod.POST), ArgumentMatchers.any(HttpEntity.class), ArgumentMatchers.any(ParameterizedTypeReference.class)))
                .thenReturn(response);

        when(vendorConfigRepository.findByClientIdAndVendor(anyString(), anyString())).thenReturn(
                new VendorConfig(1L, "client_id", "{}", "{\"client_id\":\"kdsmksda\",\"client_secret\":\"5aee92a2e643\",\"refresh_token\":\"OAUTH2\",\"code\":\"OAUTH2\",\"base_site_url\":\"https://www.aaro.health\"}", "wix")
        );

        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(1L, "name", "pass", new Date()));

        assertThatCode(() -> wixApiIntegrationService.setAuthToken()).doesNotThrowAnyException(); // this will call refresh token flow
        Mockito.verify(wixApiIntegrationService, Mockito.times(1)).refreshAuthToken();
    }

    @Test
    public void refreshTokenCalledFromAuthToken_ExceptionTest() {
        HttpClientErrorException forbiddenException = HttpClientErrorException.Forbidden.create(HttpStatus.FORBIDDEN, null, null, null, null);

        when(restTemplate.exchange(anyString(), eq(HttpMethod.POST), ArgumentMatchers.any(HttpEntity.class), ArgumentMatchers.any(ParameterizedTypeReference.class)))
                .thenThrow(forbiddenException);

        doNothing().when(wixApiIntegrationService).setAuthToken();

        wixApiIntegrationService.refreshAuthToken();

        Mockito.verify(wixApiIntegrationService, Mockito.times(3)).setAuthToken();
    }

    @Test
    public void setAuthTokenTest() {

        Map<String, Object> authResponse = new HashMap<>();
        authResponse.put("access_token", "access_token");
        authResponse.put("refresh_token", "refresh_token");

        ResponseEntity<Map<String, Object>> response = new ResponseEntity<>(authResponse, HttpStatus.OK);;

        when(vendorConfigRepository.findByClientIdAndVendor(anyString(), anyString())).thenReturn(
                new VendorConfig(1L, "client_id", "{}", "{\"client_id\":\"kdsmksda\",\"client_secret\":\"5aee92a2e643\",\"refresh_token\":\"\",\"code\":\"OAUTH2\",\"base_site_url\":\"https://www.aaro.health\"}", "wix")
        );

        when(restTemplate.exchange(anyString(), eq(HttpMethod.POST), ArgumentMatchers.any(HttpEntity.class), ArgumentMatchers.any(ParameterizedTypeReference.class)))
                .thenReturn(response);

        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(1L, "name", "pass", new Date()));

        assertThatCode(() -> wixApiIntegrationService.setAuthToken()).doesNotThrowAnyException(); // this will call refresh token flow
        Mockito.verify(wixApiIntegrationService, Mockito.times(1)).setAuthToken();

    }

    Map<String, Object> postDate() throws IOException {
        return Utils.jsonToObject("{\"posts\":[{\"id\":\"79bf9c19-77a8-4966-a68b-d874c2061325\",\"title\":\"UnderstandAsthma\",\"excerpt\":\"“Achronicdiseasecharacterizedbychronicairwayinflammation,atleastpartiallyirreversibleairwayobstruction,andincreasedairway...\",\"firstPublishedDate\":\"2023-04-12T13:16:01.712Z\",\"lastPublishedDate\":\"2023-04-12T13:22:16.591Z\",\"slug\":\"understand-asthma\",\"featured\":true,\"pinned\":false,\"categoryIds\":[\"72bfab4a-fc2a-4f45-8661-8b570a777825\"],\"coverMedia\":{\"enabled\":true,\"image\":{\"id\":\"f50a3d_db7dd873becd4db7a9ea7ae3d1d81ae5~mv2.jpg\",\"url\":\"https://static.wixstatic.com/media/f50a3d_db7dd873becd4db7a9ea7ae3d1d81ae5~mv2.jpg\",\"height\":1237,\"width\":3000},\"displayed\":true,\"custom\":false},\"memberId\":\"06f4bff4-2c91-4c4f-9582-e8c956b5f5ff\",\"hashtags\":[],\"commentingEnabled\":false,\"minutesToRead\":2,\"tagIds\":[],\"relatedPostIds\":[\"733be5ad-621b-408c-a2de-d5f1bea65af6\"],\"pricingPlanIds\":[],\"translationId\":\"9c1111b5-44e1-42d7-b1bf-e4bc49049df0\",\"language\":\"en\",\"contentId\":\"6436b088925288b0e98a4f97\",\"mostRecentContributorId\":\"06f4bff4-2c91-4c4f-9582-e8c956b5f5ff\",\"media\":{\"wixMedia\":{\"image\":{\"id\":\"f50a3d_db7dd873becd4db7a9ea7ae3d1d81ae5~mv2.jpg\",\"url\":\"https://static.wixstatic.com/media/f50a3d_db7dd873becd4db7a9ea7ae3d1d81ae5~mv2.jpg\",\"height\":1237,\"width\":3000}},\"displayed\":true,\"custom\":false},\"internalCategoryIds\":[],\"internalRelatedPostIds\":[]}]}",
                new TypeReference<Map<String, Object>>() {});
    }

    Map<String, Object> categoryData() throws IOException {
        return Utils.jsonToObject("{\"categories\":[{\"id\":\"bbd4d8a9-a645-447e-bc45-88545febe90f\",\"label\":\"ashtma\",\"postCount\":2,\"title\":\"\",\"coverMedia\":{\"enabled\":true,\"displayed\":true,\"custom\":true},\"oldRank\":0,\"rank\":0,\"displayPosition\":0,\"translationId\":\"06bee160-4a8b-4d63-b445-445339e6d5c2\",\"language\":\"hi\",\"slug\":\"asthma-6-1\"}]}"
                , new TypeReference<Map<String, Object>>() {});
    }

}

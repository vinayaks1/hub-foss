package com.everwell.transition.unit;

import com.everwell.transition.BaseTest;
import com.everwell.transition.model.response.Response;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;

public class ResponseModelTest extends BaseTest {

    @Test
    public void response_constructorData() {
        Integer data = 20;
        Response<Integer> s = new Response<>(data);
        Assert.assertTrue(s.isSuccess());
        assertThat(data, instanceOf(Integer.class));
    }

    @Test
    public void response_constructorErrorMessage() {
        String error = "error message";
        Response<Integer> s = new Response<>(error);
        Assert.assertFalse(s.isSuccess());
        assertThat(error, instanceOf(String.class));
    }

    @Test
    public void response_constructorDataSuccess() {
        Response<String> s = new Response<>(true, "");
        Assert.assertTrue(s.isSuccess());
        assertThat(s.getData(), instanceOf(String.class));
    }

}

package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.utils.MultiPartFileUtils;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

public class MultiPartFileUtilsTest extends BaseTest {

    @Test
    public void validateFileExtension_ValidExtension () {
        Assertions.assertThatCode(() -> MultiPartFileUtils.validateFileExtension("jpg"))
                .doesNotThrowAnyException();
    }

    @Test
    public void validateFileExtension_ValidExtensionUppercase () {
        Assertions.assertThatCode(() -> MultiPartFileUtils.validateFileExtension("JPEG"))
                .doesNotThrowAnyException();
    }

    @Test(expected = ValidationException.class)
    public void validateFileExtension_InvalidExtension () {
        MultiPartFileUtils.validateFileExtension("WhatsApp 2010 at 22.00.00.jpg.jpg");
    }

    @Test(expected = ValidationException.class)
    public void validateFileName_LongFileName () {
        MultiPartFileUtils.validateFileName("WhatsApp_WhatsAppWhatsAppWhatsAppWhatsAppWhatsApp_WhatsApp_WhatsApp_WhatsApp 2010 at 22-00-00.jpg.exe");
    }

    @Test
    public void validateFileName_IncorrectFileNameCharacters () {
        Assertions.assertThatThrownBy(
                () -> MultiPartFileUtils.validateFileName("!scr.jpg"))
            .isInstanceOf(ValidationException.class);
        Assertions.assertThatThrownBy(
                        () -> MultiPartFileUtils.validateFileName("a..cr.jpg"))
                .isInstanceOf(ValidationException.class);
        Assertions.assertThatCode(
                        () -> MultiPartFileUtils.validateFileName("WhatsApp 2010 at 22-00-00.jpg"))
                .doesNotThrowAnyException();
    }

    @Test
    public void validateFileName_IncorrectFileNameCharacters2 () {
        Assertions.assertThatThrownBy(
                () -> MultiPartFileUtils.validateFileName("name!.jpg"))
            .isInstanceOf(ValidationException.class);
        Assertions.assertThatThrownBy(
                        () -> MultiPartFileUtils.validateFileName("name@.jpg"))
                .isInstanceOf(ValidationException.class);
        Assertions.assertThatThrownBy(
                        () -> MultiPartFileUtils.validateFileName("name#.jpg"))
                .isInstanceOf(ValidationException.class);
        Assertions.assertThatThrownBy(
                        () -> MultiPartFileUtils.validateFileName("name$.jpg"))
                .isInstanceOf(ValidationException.class);
        Assertions.assertThatThrownBy(
                        () -> MultiPartFileUtils.validateFileName("name%.jpg"))
                .isInstanceOf(ValidationException.class);
        Assertions.assertThatThrownBy(
                        () -> MultiPartFileUtils.validateFileName("name^.jpg"))
                .isInstanceOf(ValidationException.class);
        Assertions.assertThatThrownBy(
                        () -> MultiPartFileUtils.validateFileName("name&.jpg"))
                .isInstanceOf(ValidationException.class);
        Assertions.assertThatThrownBy(
                        () -> MultiPartFileUtils.validateFileName("name*.jpg"))
                .isInstanceOf(ValidationException.class);
        Assertions.assertThatThrownBy(
                        () -> MultiPartFileUtils.validateFileName("name(.jpg"))
                .isInstanceOf(ValidationException.class);
        Assertions.assertThatThrownBy(
                        () -> MultiPartFileUtils.validateFileName("name{.jpg"))
                .isInstanceOf(ValidationException.class);
        Assertions.assertThatThrownBy(
                        () -> MultiPartFileUtils.validateFileName("name\\.jpg"))
                .isInstanceOf(ValidationException.class);
        Assertions.assertThatThrownBy(
                        () -> MultiPartFileUtils.validateFileName("name|.jpg"))
                .isInstanceOf(ValidationException.class);
        Assertions.assertThatThrownBy(
                        () -> MultiPartFileUtils.validateFileName("name[.jpg"))
                .isInstanceOf(ValidationException.class);
        Assertions.assertThatThrownBy(
                        () -> MultiPartFileUtils.validateFileName("name?.jpg"))
                .isInstanceOf(ValidationException.class);
        Assertions.assertThatThrownBy(
                        () -> MultiPartFileUtils.validateFileName("name<.jpg"))
                .isInstanceOf(ValidationException.class);
    }

    @Test
    public void validateMultipartFiles_EmptyFile() {
        List<MultipartFile> multipartFiles = Arrays.asList(
                new MultipartFile() {
                    @Override
                    public String getName() {
                        return null;
                    }

                    @Override
                    public String getOriginalFilename() {
                        return null;
                    }

                    @Override
                    public String getContentType() {
                        return null;
                    }

                    @Override
                    public boolean isEmpty() {
                        return true;
                    }

                    @Override
                    public long getSize() {
                        return 0;
                    }

                    @Override
                    public byte[] getBytes() throws IOException {
                        return new byte[0];
                    }

                    @Override
                    public InputStream getInputStream() throws IOException {
                        return null;
                    }

                    @Override
                    public void transferTo(File dest) throws IOException, IllegalStateException {

                    }
                }
        );
        Assertions.assertThatThrownBy(
                        () -> MultiPartFileUtils.validateMultipartFiles(multipartFiles))
                .isInstanceOf(ValidationException.class);
    }

    @Test
    public void validateMultipartFiles_ValidFile() {
        List<MultipartFile> multipartFiles = Arrays.asList(
                new MultipartFile() {
                    @Override
                    public String getName() {
                        return "episode";
                    }

                    @Override
                    public String getOriginalFilename() {
                        return "episode.jpg";
                    }

                    @Override
                    public String getContentType() {
                        return "application/jpg";
                    }

                    @Override
                    public boolean isEmpty() {
                        return false;
                    }

                    @Override
                    public long getSize() {
                        return 110;
                    }

                    @Override
                    public byte[] getBytes() throws IOException {
                        return new byte[0];
                    }

                    @Override
                    public InputStream getInputStream() throws IOException {
                        BufferedImage result = new BufferedImage(600, 400, BufferedImage.TYPE_INT_RGB); // create a BufferedImage object
                        ByteArrayOutputStream os = new ByteArrayOutputStream();
                        ImageIO.write(result, "jpg", os);
                        return new ByteArrayInputStream(os.toByteArray());
                    }

                    @Override
                    public void transferTo(File dest) throws IOException, IllegalStateException {

                    }
                }
        );
        Assertions.assertThatCode(
                        () -> MultiPartFileUtils.validateMultipartFiles(multipartFiles))
                .doesNotThrowAnyException();
    }

    @Test
    public void validateMultipartFiles_ValidFileNotImage() {
        List<MultipartFile> multipartFiles = Arrays.asList(
                new MultipartFile() {
                    @Override
                    public String getName() {
                        return "episode";
                    }

                    @Override
                    public String getOriginalFilename() {
                        return "episode.pdf";
                    }

                    @Override
                    public String getContentType() {
                        return "application/pdf";
                    }

                    @Override
                    public boolean isEmpty() {
                        return false;
                    }

                    @Override
                    public long getSize() {
                        return 110;
                    }

                    @Override
                    public byte[] getBytes() throws IOException {
                        return new byte[0];
                    }

                    @Override
                    public InputStream getInputStream() throws IOException {
                        String html = "</Page Something 0 0 /Page>";
                        return new ByteArrayInputStream(html.getBytes(StandardCharsets.UTF_8));
                    }

                    @Override
                    public void transferTo(File dest) throws IOException, IllegalStateException {

                    }
                }
        );
        Assertions.assertThatCode(
                        () -> MultiPartFileUtils.validateMultipartFiles(multipartFiles))
                .doesNotThrowAnyException();
    }

    @Test
    public void validateMultipartFiles_HtmlContent() {
        List<MultipartFile> multipartFiles = Arrays.asList(
                new MultipartFile() {
                    @Override
                    public String getName() {
                        return null;
                    }

                    @Override
                    public String getOriginalFilename() {
                        return null;
                    }

                    @Override
                    public String getContentType() {
                        return null;
                    }

                    @Override
                    public boolean isEmpty() {
                        return true;
                    }

                    @Override
                    public long getSize() {
                        return 0;
                    }

                    @Override
                    public byte[] getBytes() throws IOException {
                        return new byte[0];
                    }

                    @Override
                    public InputStream getInputStream() throws IOException {
                        String html = "<html><script></html></script>";
                        return new ByteArrayInputStream(html.getBytes(StandardCharsets.UTF_8));
                    }

                    @Override
                    public void transferTo(File dest) throws IOException, IllegalStateException {

                    }
                }
        );
        Assertions.assertThatThrownBy(
                        () -> MultiPartFileUtils.validateMultipartFiles(multipartFiles))
                .isInstanceOf(ValidationException.class);
    }

}

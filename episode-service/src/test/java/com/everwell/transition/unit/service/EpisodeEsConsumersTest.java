package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.elasticsearch.service.Impl.EpisodeEsConsumers;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.dto.EpisodeEsInsertUpdateDto;
import com.everwell.transition.model.dto.EventStreamingDto;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.EpisodeService;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;

import java.io.IOException;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doThrow;

public class EpisodeEsConsumersTest extends BaseTest {

    @InjectMocks
    EpisodeEsConsumers episodeEsConsumers;

    @Mock
    EpisodeService episodeService;

    @Mock
    ClientService clientService;

    @Test
    public void addUpdateElasticDocumentHandler_UpdateTrueTest () {
        EventStreamingDto<EpisodeEsInsertUpdateDto> eventStreamingDto = new EventStreamingDto<>(
        "name", new EpisodeEsInsertUpdateDto(1L, 1L, true, false)
        );
        episodeEsConsumers.addUpdateElasticDocumentHandler(eventStreamingDto);
        Mockito.verify(episodeService, Mockito.times(1)).updateElasticDocument(eq(1L), eq(1L), eq(false));
    }

    @Test
    public void addUpdateElasticDocumentHandler_UpdateFalse () {
        EventStreamingDto<EpisodeEsInsertUpdateDto> eventStreamingDto = new EventStreamingDto<>(
                "name", new EpisodeEsInsertUpdateDto(1L, 1L, false, true)
        );
        episodeEsConsumers.addUpdateElasticDocumentHandler(eventStreamingDto);
        Mockito.verify(episodeService, Mockito.times(1)).saveToElastic(eq(1L), eq(1L));
    }

    @Test
    public void addClientEpisodeToElasticTest () throws IOException {
        String jsonPayload = "{\"EventName\":\"patient_enrollment\",\"Field\":{\"Id\":10712725,\"UserId\":123223,\"clientId\":29,\"Deleted\":false,\"AddedTimestamp\":\"2021-02-03T20:36:02.9471744\",\"FirstName\":\"Testing\"}}";
        MessageProperties messageProperties = new MessageProperties();
        messageProperties.setHeader("clientId", "29");
        Message message = new Message(jsonPayload.getBytes(), messageProperties);
        episodeEsConsumers.addClientEpisodeToElastic(message);
        Mockito.verify(episodeService, Mockito.times(1)).convertAndSavetoElastic(anyMap(), eq(29L));
    }

    @Test (expected = AmqpRejectAndDontRequeueException.class)
    public void addClientEpisodeToElasticTest_Exception () throws IOException {
        String jsonPayload = "{\"EventName\":\"patient_enrollment\",\"Field\":{\"Id\":10712725,\"UserId\":123223,\"clientId\":29,\"Deleted\":false,\"AddedTimestamp\":\"2021-02-03T20:36:02.9471744\",\"FirstName\":\"Testing\"}}";
        MessageProperties messageProperties = new MessageProperties();
        messageProperties.setHeader("clientId", "29");
        Message message = new Message(jsonPayload.getBytes(), messageProperties);
        doThrow (new ValidationException("")).when(episodeService).convertAndSavetoElastic(anyMap(), eq(29L));
        episodeEsConsumers.addClientEpisodeToElastic(message);
        Mockito.verify(episodeService, Mockito.times(1)).convertAndSavetoElastic(anyMap(), eq(29L));
    }

    @Test
    public void updateElasticDetails () throws IOException {
        String jsonPayload = "{\"eventName\":\"episode-person\",\"field\":{\"module\":\"iam\",\"fields\":{\"firstName\":\"newname\"}}}";
        MessageProperties messageProperties = new MessageProperties();
        messageProperties.setHeader("clientId", "29");
        Message message = new Message(jsonPayload.getBytes(), messageProperties);
        episodeEsConsumers.updateElasticDetails(message);
        Mockito.verify(episodeService, Mockito.times(1)).updateClientEpisodeToElastic(anyMap(), eq(29L), eq("iam"));
    }

    @Test
    public void updateElasticDetails_Person () throws IOException {
        String jsonPayload = "{\"eventName\":\"episode-person\",\"field\":{\"module\":\"person\",\"fields\":{\"firstName\":\"newname\", \"id\":\"1\"}}}";
        MessageProperties messageProperties = new MessageProperties();
        messageProperties.setHeader("clientId", "29");
        Message message = new Message(jsonPayload.getBytes(), messageProperties);
        episodeEsConsumers.updateElasticDetails(message);
        Mockito.verify(episodeService, Mockito.times(1)).updateClientEpisodeToElastic(anyMap(), eq(29L), eq("person"));
    }

    @Test
    public void updateElasticDetails_PersonWithMobile () throws IOException {
        String jsonPayload = "{\"eventName\":\"episode-person\",\"field\":{\"module\":\"person\",\"fields\":{\"firstName\":\"newname\", \"id\":\"1\", \"mobileList\":[{\"number\":\"7475657558\",\"createdAt\":1665062874445,\"stoppedAt\":1665063845455,\"verified\":false,\"id\":113,\"userId\":91,\"updatedAt\":1665062874445,\"primary\":false},{\"number\":\"7383748472\",\"createdAt\":1665063845455,\"stoppedAt\":null,\"verified\":false,\"id\":114,\"userId\":91,\"updatedAt\":1665063845455,\"primary\":true}]}}}";
        MessageProperties messageProperties = new MessageProperties();
        messageProperties.setHeader("clientId", "29");
        Message message = new Message(jsonPayload.getBytes(), messageProperties);
        episodeEsConsumers.updateElasticDetails(message);
        Mockito.verify(episodeService, Mockito.times(1)).updateClientEpisodeToElastic(anyMap(), eq(29L), eq("person"));
    }

    @Test (expected = AmqpRejectAndDontRequeueException.class)
    public void updateElasticDetails_PersonWithMobile_Exception () throws IOException {
        String jsonPayload = "{\"eventName\":\"episode-person\",\"field\":{\"module\":\"person\",\"fields\":{\"firstName\":\"newname\", \"id\":\"1\", \"mobileList\":[{\"number\":\"7475657558\",\"createdAt\":1665062874445,\"stoppedAt\":1665063845455,\"verified\":false,\"id\":113,\"userId\":91,\"updatedAt\":1665062874445,\"primary\":false},{\"number\":\"7383748472\",\"createdAt\":1665063845455,\"stoppedAt\":null,\"verified\":false,\"id\":114,\"userId\":91,\"updatedAt\":1665063845455,\"primary\":true}]}}}";
        MessageProperties messageProperties = new MessageProperties();
        messageProperties.setHeader("clientId", "29");
        Message message = new Message(jsonPayload.getBytes(), messageProperties);
        doThrow (new ValidationException("")).when(episodeService).updateClientEpisodeToElastic(anyMap(), eq(29L), eq("person"));
        episodeEsConsumers.updateElasticDetails(message);
        Mockito.verify(episodeService, Mockito.times(1)).updateClientEpisodeToElastic(anyMap(), eq(29L), eq("person"));
    }
}

package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.constants.Constants;
import com.everwell.transition.enums.episode.EpisodeValidation;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.db.EpisodeLog;
import com.everwell.transition.model.db.EpisodeLogStore;
import com.everwell.transition.model.request.adherence.EditDosesRequest;
import com.everwell.transition.model.request.episode.DeleteRequest;
import com.everwell.transition.model.request.episodelogs.EpisodeLogDataRequest;
import com.everwell.transition.model.request.episodelogs.EpisodeLogDeletionRequest;
import com.everwell.transition.model.request.episodelogs.SearchLogsRequest;
import com.everwell.transition.model.request.episodelogs.*;
import com.everwell.transition.model.response.EpisodeLogResponse;
import com.everwell.transition.model.response.LogsConfigResponse;
import com.everwell.transition.postconstruct.EpisodeLogStoreCreation;
import com.everwell.transition.repositories.EpisodeLogRepository;
import com.everwell.transition.service.impl.EpisodeLogServiceImpl;
import com.everwell.transition.utils.Utils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.data.util.Pair;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;


import static org.mockito.ArgumentMatchers.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class EpisodeLogServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    EpisodeLogServiceImpl episodeLogService;
    
    @Mock
    EpisodeLogRepository episodeLogRepository;

    
    @Mock
    EpisodeLogStoreCreation episodeLogStoreCreation;

    private final List<LocalDateTime> dates = Collections.singletonList(Utils.toLocalDateTimeWithNull("06-03-2022 18:30:00"));
    private final ZoneId zoneId = ZoneId.of("IST", ZoneId.SHORT_IDS);

    @Test
    public void getEpisodeLogsTest() {
        when(episodeLogRepository.getAllByEpisodeIdOrderByAddedOnDesc(anyLong())).thenReturn(getEpisodeLogs());
        EpisodeLogResponse episodeLogs = episodeLogService.getEpisodeLogs(1234L);
        for (int i = 0; i < episodeLogs.getLogs().size(); i++) {
            Assertions.assertEquals(episodeLogs.getLogs().get(i).getCategory(), getEpisodeLogs().get(i).getCategory());
        }
    }

    @Test
    public void saveTest() {
        when(episodeLogRepository.saveAll(any())).thenReturn(getEpisodeLogs());
        List<EpisodeLogDataRequest> episodeLogRequest = Arrays.asList(
            new EpisodeLogDataRequest(12345L, "Manual_Doses_Marked", 7945L, "Marked dose","Marked dose")
        );
        List<EpisodeLogResponse> episodeLogResponses = episodeLogService.save(episodeLogRequest);
        for (int i = 0; i < episodeLogResponses.get(0).getLogs().size(); i++) {
            Assertions.assertEquals(episodeLogResponses.get(0).getLogs().get(i).getCategory(), getEpisodeLogs().get(i).getCategory());
        }
    }

    @Test
    public void search_Logs_Test() {
        List<String> categories = new ArrayList<>();
        categories.add("Manual_Doses_Marked");
        SearchLogsRequest searchLogsRequestAll = new SearchLogsRequest(
            Arrays.asList(1L, 2L, 3L), 7945L, categories
        );
        SearchLogsRequest searchLogsRequestNoCategory = new SearchLogsRequest(
            Arrays.asList(1L, 2L, 3L), 7945L, null
        );
        SearchLogsRequest searchLogsRequestNoAddedBy = new SearchLogsRequest(
            Arrays.asList(1L, 2L, 3L), null, categories
        );
        SearchLogsRequest searchLogsRequestNull = new SearchLogsRequest(
            Arrays.asList(1L, 2L, 3L), null, null
        );
        episodeLogService.search(searchLogsRequestAll);
        episodeLogService.search(searchLogsRequestNoCategory);
        episodeLogService.search(searchLogsRequestNoAddedBy);
        episodeLogService.search(searchLogsRequestNull);
        Mockito.verify(episodeLogRepository, Mockito.times(1)).getAllByEpisodeIdInOrderByAddedOnDesc(any());
        Mockito.verify(episodeLogRepository, Mockito.times(1)).getAllByEpisodeIdInAndAddedByOrderByAddedOnDesc(any(),eq(7945L));
        Mockito.verify(episodeLogRepository, Mockito.times(1)).getAllByEpisodeIdInAndCategoryInOrderByAddedOnDesc(any(),eq(categories));
        Mockito.verify(episodeLogRepository, Mockito.times(1)).getAllByEpisodeIdInAndCategoryInAndAddedByOrderByAddedOnDesc(any(),eq(categories),eq(7945L));

    }

    @Test
    public void deleteLogs() {
        List<String> categories = Arrays.asList(
            "A", "B", "C"
        );
        String deleteAllAfter = "11-10-2021 18:30:00";
        EpisodeLogDeletionRequest episodeLogDeletionRequestNoTime = new EpisodeLogDeletionRequest(1L, categories, null);
        EpisodeLogDeletionRequest episodeLogDeletionRequestAll = new EpisodeLogDeletionRequest(1L, categories, deleteAllAfter);

        episodeLogService.delete(episodeLogDeletionRequestAll);
        episodeLogService.delete(episodeLogDeletionRequestNoTime);

        LocalDateTime time = Utils.toLocalDateTimeWithNull(deleteAllAfter);

        Mockito.verify(episodeLogRepository, Mockito.times(1))
            .deleteLogs(eq(1L), eq(categories));
        Mockito.verify(episodeLogRepository, Mockito.times(1))
            .deleteLogsAfterDate(eq(1L), eq(categories), eq(time));
    }


    @Test
    public void groupLogsTest() {
        List<EpisodeLog> logList = new ArrayList<>(getEpisodeLogs());
        //add more to check function correctness
        logList.add(new EpisodeLog(1L, 123456L, LocalDateTime.now(), 7946L,"Case_Reopened", null, null, null, null));
        logList.add(new EpisodeLog(1L, 123457L, LocalDateTime.now(), 7946L,"Case_Reopened", null, null, null, null));

        List<EpisodeLogResponse> response = episodeLogService.groupLogs(logList);
        Assertions.assertEquals(response.size(), 3);
    }

    @Test
    public void getEpisodeConfigTest() {
        Map<String, EpisodeLogStore> episodeLogStoreMap = new HashMap<>();
        episodeLogStoreMap.put("A", getStore().get(0));
        when(episodeLogStoreCreation.getEpisodeLogStoreMap()).thenReturn(episodeLogStoreMap);
        List<LogsConfigResponse> config = episodeLogService.getEpisodeLogsConfig();
        Assertions.assertEquals(config.get(0).getCategory(), getStore().get(0).getCategory());
        Assertions.assertEquals(config.get(0).getCategoryGroup(), getStore().get(0).getCategoryGroup());
    }

    @Test
    public void testGetDatesFromLogAllDatesBefore() {
        String actionTaken = "Manual doses marked for 4/11/2021, 25/11/2021";
        LocalDateTime endDateTime = Utils.toLocalDateTimeWithNull("28-11-2021 14:30:00");

        Pair<String, List<LocalDate>> dates = episodeLogService.getActionTakenWithDatesFromLog(actionTaken, endDateTime);

        Assertions.assertEquals("Manual doses marked for", dates.getFirst());
        Assertions.assertEquals(2, dates.getSecond().size());
        Assertions.assertEquals(dates.getSecond().get(0), Utils.toLocalDate("4/11/2021","d/M/yyyy"));
        Assertions.assertEquals(dates.getSecond().get(1), Utils.toLocalDate("25/11/2021","d/M/yyyy"));
    }

    @Test
    public void testGetDatesFromLogBothDates() {
        String actionTaken = "Patient manual doses marked for 25/11/2021, 26/11/2021, 28/11/2021";
        LocalDateTime endDateTime = Utils.toLocalDateTimeWithNull("27-11-2021 14:30:00");

        Pair<String, List<LocalDate>> dates = episodeLogService.getActionTakenWithDatesFromLog(actionTaken, endDateTime);

        Assertions.assertEquals("Patient manual doses marked for", dates.getFirst());
        Assertions.assertEquals(2, dates.getSecond().size());
        Assertions.assertEquals(dates.getSecond().get(0), Utils.toLocalDate("25/11/2021", "d/M/yyyy"));
        Assertions.assertEquals(dates.getSecond().get(1), Utils.toLocalDate("26/11/2021", "d/M/yyyy"));
    }

    @Test
    public void testGetDatesFromLogAfterEndDate() {
        String actionTaken = "Manual doses marked for";
        LocalDateTime endDateTime = Utils.toLocalDateTimeWithNull("22-11-2021 14:30:00");

        Pair<String, List<LocalDate>> dates = episodeLogService.getActionTakenWithDatesFromLog(actionTaken, endDateTime);

        Assertions.assertEquals(0, dates.getSecond().size());
    }

    @Test
    public void testSync() {
        Map<String, EpisodeLogStore> episodeLogStoreMap = new HashMap<>();
        episodeLogStoreMap.put("Monitoring_Method_Changed", getStore().get(1));
        episodeLogStoreMap.put("Manual_Doses_Added", getStore().get(2));
        episodeLogStoreMap.put("Patient_Manual_Doses_Added", getStore().get(3));
        episodeLogStoreMap.put("Tags_Added_For_Past_Doses", getStore().get(4));
        List<EpisodeLog> episodeLogsMonitoringMethod = new ArrayList<>();
        List<EpisodeLog> episodeLogs = new ArrayList<>();
        episodeLogsMonitoringMethod.add(new EpisodeLog(1L, 7L, Utils.toLocalDateTimeWithNull("02-12-2021 18:30:00"), 7945L, "Monitoring_Method_Changed", null, null, null, null));
        episodeLogs.add(new EpisodeLog(2L, 7L, LocalDateTime.now(), 7945L, "Manual_Doses_Added", null, "Manual doses marked for 4/11/2021, 25/11/2021", null, null));
        episodeLogs.add(new EpisodeLog(3L, 7L, LocalDateTime.now(), 7945L, "Manual_Doses_Removed", null, "Unmarked manual doses for 26/11/2021, 28/11/2021", null, null));
        episodeLogs.add(new EpisodeLog(4L, 7L, LocalDateTime.now(), 7945L, "Missed_Doses_Removed", null, "Unmarked missed doses for 29/11/2021, 1/12/2021", null, null));
        episodeLogs.add(new EpisodeLog(5L, 7L, LocalDateTime.now(), 7945L, "Missed_Doses_Added", null, "Missed doses marked for 3/12/2021", null, null));

        List<LocalDate> dateBefore = Arrays.asList(LocalDate.of(2021,11,4), LocalDate.of(2021,11,25));
        List<LocalDate> dateBoth = Collections.singletonList(LocalDate.of(2021, 11, 26));
        when(episodeLogRepository.getAllByEpisodeIdAndCategoryInAndAddedOnAfterOrderByAddedOnAsc(eq(7L),any(),any())).thenReturn(episodeLogsMonitoringMethod);
        when(episodeLogRepository.getAllByEpisodeIdAndCategoryInAndAddedOnAfterOrderByAddedOnAsc(eq(7L),any(),eq(Utils.toLocalDateTimeWithNull("02-12-2021 18:30:00")))).thenReturn(episodeLogs);
        when(episodeLogStoreCreation.getEpisodeLogStoreMap()).thenReturn(episodeLogStoreMap);

        doReturn(Pair.of("Manual doses marked for",dateBefore)).when(episodeLogService).getActionTakenWithDatesFromLog(eq("Manual doses marked for 4/11/2021, 25/11/2021"),any());
        doReturn(Pair.of("Unmarked manual doses for",dateBoth)).when(episodeLogService).getActionTakenWithDatesFromLog(eq("Unmarked manual doses for 26/11/2021, 28/11/2021"),any());
        doReturn(Pair.of("Unmarked missed doses for",Collections.emptyList())).when(episodeLogService).getActionTakenWithDatesFromLog(eq("Unmarked missed doses for 29/11/2021, 1/12/2021"),any());
        doReturn(Pair.of("Unmarked missed doses for",Collections.emptyList())).when(episodeLogService).getActionTakenWithDatesFromLog(eq("Unmarked missed doses for 3/12/2021"),any());


        episodeLogService.syncLogs(7L, "27-11-2021 14:30:00");

        List<EpisodeLog> logsToDelete = episodeLogs.subList(2,4);
        logsToDelete.add(episodeLogsMonitoringMethod.get(0));


        verify(episodeLogRepository,Mockito.times(1)).deleteAll(eq(logsToDelete));
        verify(episodeLogRepository,Mockito.times(1)).saveAll(eq(episodeLogs));
    }

    @Test
    public void testSyncEmpty() {
        when(episodeLogRepository.getAllByEpisodeIdAndCategoryInAndAddedOnAfterOrderByAddedOnAsc(eq(7L),any(),any())).thenReturn(null);
        episodeLogService.syncLogs(7L, "27-11-2021 14:30:00");

        verify(episodeLogRepository,Mockito.times(0)).deleteAll(any());
        verify(episodeLogRepository,Mockito.times(0)).saveAll(any());
    }

    @Test
    public void addAdherenceLogsPatientManual() {
        EditDosesRequest editDosesRequest = new EditDosesRequest();
        editDosesRequest.setAddDoses(true);
        editDosesRequest.setCode("PATIENT_MANUAL");

       EpisodeLog episodeLog = episodeLogService.addAdherenceLogs(editDosesRequest, dates, zoneId);

       Assert.assertEquals("Patient_Manual_Doses_Added", episodeLog.getCategory());
       Assert.assertEquals("Patient manual doses marked for 7/3/2022 00:00:00", episodeLog.getActionTaken());

    }

    @Test
    public void addAdherenceLogsManual() {
        EditDosesRequest editDosesRequest = new EditDosesRequest();
        editDosesRequest.setAddDoses(true);
        editDosesRequest.setCode("MANUAL");

        EpisodeLog episodeLog = episodeLogService.addAdherenceLogs(editDosesRequest, dates, zoneId);

        Assert.assertEquals("Manual_Doses_Added", episodeLog.getCategory());
        Assert.assertEquals("Manual doses marked for 7/3/2022 00:00:00", episodeLog.getActionTaken());

    }

    @Test
    public void addAdherenceLogsPatientMissed() {
        EditDosesRequest editDosesRequest = new EditDosesRequest();
        editDosesRequest.setAddDoses(true);
        editDosesRequest.setCode("PATIENT_MISSED");

        EpisodeLog episodeLog = episodeLogService.addAdherenceLogs(editDosesRequest, dates, zoneId);

        Assert.assertEquals("Patient_Missed_Doses_Added", episodeLog.getCategory());
        Assert.assertEquals("Patient missed doses marked for 7/3/2022 00:00:00", episodeLog.getActionTaken());

    }

    @Test
    public void addAdherenceLogsMissed() {
        EditDosesRequest editDosesRequest = new EditDosesRequest();
        editDosesRequest.setAddDoses(true);
        editDosesRequest.setCode("MISSED");

        EpisodeLog episodeLog = episodeLogService.addAdherenceLogs(editDosesRequest, dates, zoneId);

        Assert.assertEquals("Missed_Doses_Added", episodeLog.getCategory());
        Assert.assertEquals("Missed doses marked for 7/3/2022 00:00:00", episodeLog.getActionTaken());

    }

    @Test
    public void removeAdherenceLogsPatientManual() {
        EditDosesRequest editDosesRequest = new EditDosesRequest();
        editDosesRequest.setAddDoses(false);
        editDosesRequest.setCode("PATIENT_MANUAL");

        EpisodeLog episodeLog = episodeLogService.addAdherenceLogs(editDosesRequest, dates, zoneId);

        Assert.assertEquals("Patient_Manual_Doses_Removed", episodeLog.getCategory());
        Assert.assertEquals("Patient Unmarked manual doses for 7/3/2022 00:00:00", episodeLog.getActionTaken());

    }

    @Test
    public void removeAdherenceLogsManual() {
        EditDosesRequest editDosesRequest = new EditDosesRequest();
        editDosesRequest.setAddDoses(false);
        editDosesRequest.setCode("MANUAL");

        EpisodeLog episodeLog = episodeLogService.addAdherenceLogs(editDosesRequest, dates, zoneId);

        Assert.assertEquals("Manual_Doses_Removed", episodeLog.getCategory());
        Assert.assertEquals("Unmarked manual doses for 7/3/2022 00:00:00", episodeLog.getActionTaken());

    }

    @Test
    public void removeAdherenceLogsMissed() {
        EditDosesRequest editDosesRequest = new EditDosesRequest();
        editDosesRequest.setAddDoses(false);
        editDosesRequest.setCode("MISSED");

        EpisodeLog episodeLog = episodeLogService.addAdherenceLogs(editDosesRequest, dates, zoneId);

        Assert.assertEquals("Missed_Doses_Removed", episodeLog.getCategory());
        Assert.assertEquals("Unmarked missed doses for 7/3/2022 00:00:00", episodeLog.getActionTaken());

    }

    @Test
    public void removeAdherenceLogsPatientMissed() {
        EditDosesRequest editDosesRequest = new EditDosesRequest();
        editDosesRequest.setAddDoses(false);
        editDosesRequest.setCode("PATIENT_MISSED");

        EpisodeLog episodeLog = episodeLogService.addAdherenceLogs(editDosesRequest, dates, zoneId);

        Assert.assertEquals("Patient_Missed_Doses_Removed", episodeLog.getCategory());
        Assert.assertEquals("Patient Unmarked missed doses for 7/3/2022 00:00:00", episodeLog.getActionTaken());

    }

    @Test
    public void addEpisodeDeletionLog() {
        DeleteRequest deleteEpisodeRequest = new DeleteRequest(1L, 1L, "reason", "notes");

        EpisodeLog episodeLog = new EpisodeLog(1L, 1L, "Patient_Deleted", "Reason for Deletion: reason", "notes");

        when(episodeLogRepository.save(any(EpisodeLog.class))).thenReturn(episodeLog);

        episodeLogService.addEpisodeDeletionLog(deleteEpisodeRequest);

        Mockito.verify(episodeLogRepository, times(1)).save(any(EpisodeLog.class));
    }

    @Test
    public void updateSupportActions()
    {
        Map<String, EpisodeLogStore> episodeLogStoreMap = new HashMap<>();
        episodeLogStoreMap.put("A", new EpisodeLogStore(1L, "Support_Action_Call", Constants.SUPPORT_ACTION_LOGS));
        when(episodeLogStoreCreation.getEpisodeLogStoreMap()).thenReturn(episodeLogStoreMap);
        List<EpisodeLog> logs = Collections.singletonList(new EpisodeLog(1L, 1L, LocalDateTime.now(), 1L, "oldCategory", "oldSubCategory", "oldAction", "oldComments", LocalDateTime.now()));
        when(episodeLogRepository.getAllByEpisodeIdAndCategoryIn(1L, Collections.singletonList("Support_Action_Call"))).thenReturn(logs);
        UpdateSupportActionsRequest request = new UpdateSupportActionsRequest(1L, Collections.singletonList(new UpdateSupportActionsDataRequest(1L, 1L, "category", "subCategory", 1L, "action", "comments", LocalDateTime.now())));

        doNothing().when(episodeLogRepository).deleteAll(any());
        when(episodeLogRepository.saveAll(any())).thenReturn(new ArrayList<>());

        episodeLogService.updateSupportActions(request);

        Mockito.verify(episodeLogRepository, Mockito.times(1)).deleteAll(any());
        Mockito.verify(episodeLogRepository, Mockito.times(1)).saveAll(any());
    }

    @Test
    public void updateSupportActions_Unchanged()
    {
        Map<String, EpisodeLogStore> episodeLogStoreMap = new HashMap<>();
        episodeLogStoreMap.put("A", new EpisodeLogStore(1L, "Support_Action_Call", Constants.SUPPORT_ACTION_LOGS));
        when(episodeLogStoreCreation.getEpisodeLogStoreMap()).thenReturn(episodeLogStoreMap);
        LocalDateTime dateTime = LocalDateTime.now();
        List<EpisodeLog> logs = Collections.singletonList(new EpisodeLog(1L, 1L, dateTime, 1L, "category", "subCategory", "action", "comments", dateTime));
        when(episodeLogRepository.getAllByEpisodeIdAndCategoryIn(1L, Collections.singletonList("Support_Action_Call"))).thenReturn(logs);
        UpdateSupportActionsRequest request = new UpdateSupportActionsRequest(1L, Collections.singletonList(new UpdateSupportActionsDataRequest(1L, 1L, "category", "subCategory", 1L, "action", "comments", dateTime)));

        doNothing().when(episodeLogRepository).deleteAll(any());
        when(episodeLogRepository.saveAll(any())).thenReturn(new ArrayList<>());

        episodeLogService.updateSupportActions(request);

        Mockito.verify(episodeLogRepository, Mockito.times(1)).deleteAll(new ArrayList<>());
        Mockito.verify(episodeLogRepository, Mockito.times(1)).saveAll(new ArrayList<>());
    }

    @Test
    public void updateSupportActions_delete()
    {
        Map<String, EpisodeLogStore> episodeLogStoreMap = new HashMap<>();
        episodeLogStoreMap.put("A", new EpisodeLogStore(1L, "Support_Action_Call", Constants.SUPPORT_ACTION_LOGS));
        when(episodeLogStoreCreation.getEpisodeLogStoreMap()).thenReturn(episodeLogStoreMap);
        List<EpisodeLog> logs = Collections.singletonList(new EpisodeLog(1L, 1L, LocalDateTime.now(), 1L, "oldCategory", "oldSubCategory", "oldAction", "oldComments", LocalDateTime.now()));
        when(episodeLogRepository.getAllByEpisodeIdAndCategoryIn(1L, Collections.singletonList("Support_Action_Call"))).thenReturn(logs);
        UpdateSupportActionsRequest request = new UpdateSupportActionsRequest(1L, new ArrayList<>());

        doNothing().when(episodeLogRepository).deleteAll(any());
        when(episodeLogRepository.saveAll(any())).thenReturn(new ArrayList<>());

        episodeLogService.updateSupportActions(request);

        Mockito.verify(episodeLogRepository, Mockito.times(1)).deleteAll(any());
        Mockito.verify(episodeLogRepository, Mockito.times(1)).saveAll(any());
    }

    @Test
    public void updateSupportActions_IdNull()
    {
        UpdateSupportActionsRequest request = new UpdateSupportActionsRequest(1L, Collections.singletonList(new UpdateSupportActionsDataRequest(null, 1L, "category", "subCategory", 1L, "action", "comments", LocalDateTime.now())));

        ValidationException exception = Assert.assertThrows(ValidationException.class, () -> {
            episodeLogService.updateSupportActions(request);
        });

        Assert.assertEquals("Id cannot be empty!", exception.getMessage());
    }

    @Test
    public void updateSupportActions_CategoryEmpty()
    {
        UpdateSupportActionsRequest request = new UpdateSupportActionsRequest(1L, Collections.singletonList(new UpdateSupportActionsDataRequest(1L, 1L, "", "subCategory", 1L, "action", "comments", LocalDateTime.now())));

        ValidationException exception = Assert.assertThrows(ValidationException.class, () -> {
            episodeLogService.updateSupportActions(request);
        });

        Assert.assertEquals("Category cannot be empty!", exception.getMessage());
    }

    @Test
    public void updateSupportActions_CategoryNull()
    {
        UpdateSupportActionsRequest request = new UpdateSupportActionsRequest(1L, Collections.singletonList(new UpdateSupportActionsDataRequest(1L, 1L, null, "subCategory", 1L, "action", "comments", LocalDateTime.now())));

        ValidationException exception = Assert.assertThrows(ValidationException.class, () -> {
            episodeLogService.updateSupportActions(request);
        });

        Assert.assertEquals("Category cannot be empty!", exception.getMessage());
    }

    @Test
    public void updateSupportActions_EpisodeIdNull()
    {
        UpdateSupportActionsRequest request = new UpdateSupportActionsRequest(1L, Collections.singletonList(new UpdateSupportActionsDataRequest(1L, null, "category", "subCategory", 1L, "action", "comments", LocalDateTime.now())));

        ValidationException exception = Assert.assertThrows(ValidationException.class, () -> {
            episodeLogService.updateSupportActions(request);
        });

        Assert.assertEquals("Episode Id cannot be empty!", exception.getMessage());
    }

    @Test
    public void updateSupportActions_EpisodeIdNull_caseTwo() {

        UpdateSupportActionsRequest request = new UpdateSupportActionsRequest(null, Collections.singletonList(new UpdateSupportActionsDataRequest(1L, 1L, "category", "subCategory", 1L, "action", "comments", LocalDateTime.now())));

        ValidationException exception = Assert.assertThrows(ValidationException.class, () -> {
            episodeLogService.updateSupportActions(request);
        });

        Assert.assertEquals(EpisodeValidation.INVALID_EPISODE_ID.getMessage(), exception.getMessage());
    }

    @Test
    public void updateSupportActions_AddedByNull()
    {
        UpdateSupportActionsRequest request = new UpdateSupportActionsRequest(1L, Collections.singletonList(new UpdateSupportActionsDataRequest(1L, 1L, "category", "subCategory", null, "action", "comments", LocalDateTime.now())));

        ValidationException exception = Assert.assertThrows(ValidationException.class, () -> {
            episodeLogService.updateSupportActions(request);
        });

        Assert.assertEquals("Added By cannot be null", exception.getMessage());
    }

    List<EpisodeLogStore> getStore() {
        List<EpisodeLogStore> config = Arrays.asList(
            new EpisodeLogStore(1L, "Manual_Doses_Marked", "Dosage Level changes"),
            new EpisodeLogStore(2L,"Monitoring_Method_Changes","Adherence changes"),
            new EpisodeLogStore(3L,"Manual_Doses_Added","Dosage level changes"),
            new EpisodeLogStore(4L,"Patient_Manual_Doses_Added","Dosage level changes"),
            new EpisodeLogStore(5L,"Tags_Added_For_Past_Doses","Dosage level changes"));
        return config;
    }

    List<EpisodeLog> getEpisodeLogs() {
        long episodeId = 12345;
        return Arrays.asList(
            new EpisodeLog(1L, episodeId, LocalDateTime.now(), 7945L,"Case_Closed", null, null, null, null),
            new EpisodeLog(1L, episodeId, LocalDateTime.now(), 7946L, null,"Case_Reopened", null, null, null)
            );
    }
}

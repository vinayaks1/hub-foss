package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.dto.notification.EpisodeListDto;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.impl.DispensationServiceImpl;
import com.everwell.transition.service.impl.NikshayServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

public class NikshayServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    NikshayServiceImpl nikshayService;

    @Mock
    ClientService clientService;

    @Before
    public void init() {
        ReflectionTestUtils.setField(nikshayService, "dataGatewayURL", "http://test:8080");
        ReflectionTestUtils.setField(nikshayService, "username", "test");
        ReflectionTestUtils.setField(nikshayService, "password", "test");
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(29L,"test", "test", null ));
        nikshayService.init();
    }

    @Test
    public void testGetEpisodesWithContactTracingFilterSuccess() {
        EpisodeListDto episodeListDto = new EpisodeListDto(Collections.singletonList(123L));
        ResponseEntity<Response<EpisodeListDto>> response = new ResponseEntity<>(
                new Response<>(episodeListDto),
                HttpStatus.OK
        );

        doReturn(response).when(nikshayService).postExchange(ArgumentMatchers.anyString(), ArgumentMatchers.any(), ArgumentMatchers.any());

        List<Long> actualResponse = nikshayService.getEpisodesWithContactTracingFilter(Collections.singletonList(123L));
        Assert.assertEquals(123L, (long) actualResponse.get(0));
    }

    @Test(expected = ValidationException.class)
    public void testGetEpisodesWithContactTracingFilterException() {

        doReturn(null).when(nikshayService).postExchange(ArgumentMatchers.anyString(), ArgumentMatchers.any(), ArgumentMatchers.any());

        nikshayService.getEpisodesWithContactTracingFilter(Collections.singletonList(123L));
    }
}

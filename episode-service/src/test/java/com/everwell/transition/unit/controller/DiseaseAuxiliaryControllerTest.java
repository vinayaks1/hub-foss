package com.everwell.transition.unit.controller;

import com.everwell.transition.BaseTest;
import com.everwell.transition.constants.Constants;
import com.everwell.transition.controller.DiseaseAuxiliaryController;
import com.everwell.transition.exceptions.CustomExceptionHandler;
import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.db.Regimen;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.DiseaseTemplateService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class DiseaseAuxiliaryControllerTest extends BaseTest {

    private MockMvc mockMvc;

    @Mock
    DiseaseTemplateService diseaseTemplateService;

    @InjectMocks
    DiseaseAuxiliaryController diseaseAuxiliaryController;

    @Mock
    ClientService clientService;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(diseaseAuxiliaryController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void test_getRegimenForDisease_success_for_diseaseId() throws Exception {
        Long diseaseId = 1L;
        Long clientId = 1L;
        when(diseaseTemplateService.getRegimensForDisease(any(), any(), any())).thenReturn(getRegimens_testCase1());
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(clientId, "test", "test", null));
        String uri = "/v1/regimen/disease?diseaseId=" + diseaseId;

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .header(Constants.X_CLIENT_ID, clientId)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"));
    }

    @Test
    public void test_getRegimenForDisease_success_for_diseaseName() throws Exception {
        String diseaseName = "dstb";
        Long clientId = 1L;
        when(diseaseTemplateService.getRegimensForDisease(any(), any(), any())).thenReturn(getRegimens_testCase1());
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(clientId, "test", "test", null));
        String uri = "/v1/regimen/disease?diseaseName=" + diseaseName;

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .header(Constants.X_CLIENT_ID, clientId)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("true"));
    }

    @Test
    public void test_getRegimenForDisease_throws_validationExceptions() throws Exception {
        Long clientId = 1L;
        when(diseaseTemplateService.getRegimensForDisease(any(), any(), any())).thenReturn(getRegimens_testCase1());
        String uri = "/v1/regimen/disease";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .header(Constants.X_CLIENT_ID, clientId)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is4xxClientError())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"));
    }



    private List<Regimen> getRegimens_testCase1() {
        List<Regimen> regimens = new ArrayList<>();
        regimens.add(new Regimen(1L, "Regimen1", 180L, 1L));
        regimens.add(new Regimen(2L, "Regimen2", 170L, 1L));
        regimens.add(new Regimen(3L, "Regimen3", 160L, 1L));
        regimens.add(new Regimen(4L, "Regimen4", 150L, 1L));
        return regimens;
    }
}

package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.db.AdverseEvent;
import com.everwell.transition.model.db.AdverseReaction;
import com.everwell.transition.model.db.CausalityManagement;
import com.everwell.transition.model.db.AersOutcome;
import com.everwell.transition.model.dto.CausalityMedicineDto;
import com.everwell.transition.model.dto.KeyValuePair;
import com.everwell.transition.model.dto.SuspectedDrugsDto;
import com.everwell.transition.model.request.aers.AdverseReactionRequest;
import com.everwell.transition.model.request.aers.CausalityManagementRequest;
import com.everwell.transition.model.request.aers.OutcomeRequest;
import com.everwell.transition.model.response.AdverseReactionResponse;
import com.everwell.transition.model.response.CausalityManagementResponse;
import com.everwell.transition.model.response.FormConfigResponse;
import com.everwell.transition.model.response.OutcomeResponse;
import com.everwell.transition.repositories.AdverseEventRepository;
import com.everwell.transition.repositories.AdverseReactionRepository;
import com.everwell.transition.repositories.CausalityManagementRepository;
import com.everwell.transition.repositories.OutcomeRepository;
import com.everwell.transition.service.impl.AERSServiceImpl;
import com.everwell.transition.utils.Utils;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.*;
import static org.junit.Assert.assertEquals;

public class AERSServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    private AERSServiceImpl aersService;

    @Mock
    private AdverseEventRepository adverseEventRepository;

    @Mock
    private AdverseReactionRepository adverseReactionRepository;

    @Mock
    private CausalityManagementRepository causalityManagementRepository;

    @Mock
    private OutcomeRepository outcomeRepository;

    @Mock
    private AdverseReaction mockAdverseReaction;

    @Mock
    private CausalityManagement causalityManagement;

    @Mock
    private AersOutcome outcome;


    @Test
    public void testAddAdverseReaction()
    {
        AdverseReactionRequest adverseReactionRequest = new AdverseReactionRequest(null,1L);
        AdverseReaction testAdverseReaction = new AdverseReaction(1L);

        List<KeyValuePair> predominantCondition = new ArrayList<>();
        predominantCondition.add(new KeyValuePair());

        adverseReactionRequest.setPredominantCondition(predominantCondition);
        adverseReactionRequest.setOnsetDate("10-10-2021 18:30:30");

        when(adverseReactionRepository.save(any())).thenReturn(testAdverseReaction);

        aersService.addAdverseReaction(adverseReactionRequest);

        verify(adverseReactionRepository,Mockito.times(1)).save(any());
        verify(adverseEventRepository,Mockito.times(1)).save(any());

    }

    @Test
    public void updateAdverseReaction() {
        AdverseReactionRequest adverseReactionRequest = new AdverseReactionRequest(1L,1L);
        AdverseEvent adverseEvent = new AdverseEvent(1L,1L);
        AdverseReaction adverseReaction = new AdverseReaction();

        when(adverseEventRepository.findById(1L)).thenReturn(Optional.of(adverseEvent));
        when(adverseReactionRepository.findById(1L)).thenReturn(Optional.of(adverseReaction));

        aersService.updateAdverseReaction(adverseReactionRequest);

        verify(adverseEventRepository, Mockito.times(1)).findById(1L);
        verify(adverseReactionRepository, Mockito.times(1)).findById(1L);
        verify(adverseReactionRepository, Mockito.times(1)).save(adverseReaction);
        verify(adverseEventRepository,Mockito.times(1)).save(adverseEvent);

    }

    @Test
    public void updateAdverseReaction_after_add() {
        AdverseReactionRequest adverseReactionRequest = new AdverseReactionRequest(1L,1L);
        AdverseEvent adverseEvent = new AdverseEvent(1L,1L);
        AdverseReaction adverseReaction = new AdverseReaction();

        when(adverseEventRepository.findById(1L)).thenReturn(Optional.of(adverseEvent));
        when(adverseReactionRepository.findById(1L)).thenReturn(Optional.of(adverseReaction));

        aersService.addAdverseReaction(adverseReactionRequest);

        verify(adverseEventRepository, Mockito.times(1)).findById(1L);
        verify(adverseReactionRepository, Mockito.times(1)).findById(1L);
        verify(adverseReactionRepository, Mockito.times(1)).save(adverseReaction);
        verify(adverseEventRepository,Mockito.times(1)).save(adverseEvent);

    }

    @Test(expected = ValidationException.class)
    public void updateAdverseReaction_Exception() {
        AdverseReactionRequest adverseReactionRequest = new AdverseReactionRequest(null,1L);
        aersService.updateAdverseReaction(adverseReactionRequest);
    }

    @Test
    public void testCausalityManagementAdd() {
        CausalityManagementRequest causalityManagementRequest = new CausalityManagementRequest(1L,1L);
        AdverseEvent adverseEvent = new AdverseEvent(1L,1L);
        CausalityManagement causalityManagement = new CausalityManagement(1L);

        when(adverseEventRepository.findById(1L)).thenReturn(Optional.of(adverseEvent));
        when(causalityManagementRepository.save(any())).thenReturn(causalityManagement);

        aersService.addCausalityManagement(causalityManagementRequest);

        verify(adverseEventRepository, Mockito.times(1)).findById(1L);
        verify(causalityManagementRepository, Mockito.times(1)).save(any());
        verify(adverseEventRepository, Mockito.times(1)).save(any());

    }

    @Test
    public void testCausalityManagementUpdate() {
        CausalityManagementRequest causalityManagementRequest = new CausalityManagementRequest(1L,1L,"yes");
        AdverseEvent adverseEvent = new AdverseEvent(1L,1L,1L);
        CausalityManagement causalityManagement = new CausalityManagement();

        List<CausalityMedicineDto> medicinesDto = new ArrayList<>();
        medicinesDto.add(new CausalityMedicineDto());
        List<KeyValuePair> actionTaken = new ArrayList<>();
        actionTaken.add(new KeyValuePair());

        causalityManagementRequest.setActionTaken(actionTaken);
        causalityManagementRequest.setOtherMedicines(medicinesDto);
        causalityManagementRequest.setAntiTbMedicines(medicinesDto);
        causalityManagementRequest.setNewMedicines(medicinesDto);
        causalityManagementRequest.setAmikacin("amikacin");

        when(adverseEventRepository.findById(1L)).thenReturn(Optional.of(adverseEvent));
        when(causalityManagementRepository.findById(1L)).thenReturn(Optional.of(causalityManagement));

        aersService.updateCausalityManagement(causalityManagementRequest);

        verify(adverseEventRepository, Mockito.times(1)).findById(1L);
        verify(causalityManagementRepository, Mockito.times(1)).findById(1L);
        verify(causalityManagementRepository, Mockito.times(1)).save(any());

    }

    @Test
    public void testCausalityManagementUpdate_after_add() {
        CausalityManagementRequest causalityManagementRequest = new CausalityManagementRequest(1L,1L,"No");
        AdverseEvent adverseEvent = new AdverseEvent(1L,1L,1L);
        CausalityManagement causalityManagement = new CausalityManagement();

        when(adverseEventRepository.findById(1L)).thenReturn(Optional.of(adverseEvent));
        when(causalityManagementRepository.findById(1L)).thenReturn(Optional.of(causalityManagement));

        aersService.addCausalityManagement(causalityManagementRequest);

        verify(adverseEventRepository, Mockito.times(2)).findById(1L);
        verify(causalityManagementRepository, Mockito.times(1)).findById(1L);
        verify(causalityManagementRepository, Mockito.times(1)).save(any());

    }

    @Test
    public void testOutcomeAdd() {

        OutcomeRequest outcomeRequest = new OutcomeRequest(1L,1L, "no");
        AdverseEvent adverseEvent = new AdverseEvent(1L,1L,"11-10-2021 18:30:00");
        AersOutcome outcome = new AersOutcome(1L,"resolved");

        when(adverseEventRepository.findById(1L)).thenReturn(Optional.of(adverseEvent));
        when(outcomeRepository.save(any())).thenReturn(outcome);

        aersService.addOutcome(outcomeRequest);

        verify(adverseEventRepository, Mockito.times(1)).findById(1L);
        verify(outcomeRepository, Mockito.times(1)).save(any());
        verify(adverseEventRepository, Mockito.times(1)).save(any());

    }

    @Test(expected = ValidationException.class)
    public void testOutcomeAddDateOfDeathException() {

        OutcomeRequest outcomeRequest = new OutcomeRequest(1L,1L, "no","10-10-2021 18:30:00");
        AdverseEvent adverseEvent = new AdverseEvent(1L,1L,"11-10-2021 18:30:00");
        AersOutcome outcome = new AersOutcome(1L,"resolved");

        when(adverseEventRepository.findById(1L)).thenReturn(Optional.of(adverseEvent));

        aersService.addOutcome(outcomeRequest);

        verify(adverseEventRepository, Mockito.times(1)).findById(1L);
        verify(outcomeRepository, Mockito.times(0)).save(any());
        verify(adverseEventRepository, Mockito.times(0)).save(any());
    }

    @Test
    public void testOutcomeAddDateOfDeath_After() {

        OutcomeRequest outcomeRequest = new OutcomeRequest(1L,1L, "no","12-10-2021 18:30:00");
        AdverseEvent adverseEvent = new AdverseEvent(1L,1L,"11-10-2021 18:30:00");
        AersOutcome outcome = new AersOutcome(1L,"resolved");

        when(adverseEventRepository.findById(1L)).thenReturn(Optional.of(adverseEvent));
        when(outcomeRepository.save(any())).thenReturn(outcome);

        aersService.addOutcome(outcomeRequest);

        verify(adverseEventRepository, Mockito.times(1)).findById(1L);
        verify(outcomeRepository, Mockito.times(1)).save(any());
        verify(adverseEventRepository, Mockito.times(1)).save(any());
    }

    @Test(expected = ValidationException.class)
    public void testOutcomeAddDateOfResolutionException() {

        OutcomeRequest outcomeRequest = new OutcomeRequest(1L,1L, "no","10-10-2021 18:30:00");
        outcomeRequest.setDateOfResolution("10-10-2021 18:30:00");
        AdverseEvent adverseEvent = new AdverseEvent(1L,1L,"11-10-2021 18:30:00");

        when(adverseEventRepository.findById(1L)).thenReturn(Optional.of(adverseEvent));

        aersService.addOutcome(outcomeRequest);

        verify(adverseEventRepository, Mockito.times(1)).findById(1L);
        verify(outcomeRepository, Mockito.times(0)).save(any());
        verify(adverseEventRepository, Mockito.times(0)).save(any());
    }

    @Test
    public void testOutcomeUpdate() {
        OutcomeRequest outcomeRequest = new OutcomeRequest(1L,1L,"fatal non");
        AdverseEvent adverseEvent = new AdverseEvent(1L,1L,1L,1L);
        AersOutcome outcome = new AersOutcome("Fatal","dont know");

        when(adverseEventRepository.findById(1L)).thenReturn(Optional.of(adverseEvent));
        when(outcomeRepository.findById(1L)).thenReturn(Optional.of(outcome));

        aersService.updateOutcome(outcomeRequest);

        verify(adverseEventRepository, Mockito.times(1)).findById(1L);
        verify(outcomeRepository, Mockito.times(1)).findById(1L);
        verify(outcomeRepository, Mockito.times(1)).save(outcome);
        verify(adverseEventRepository,Mockito.times(1)).save(adverseEvent);

    }

    @Test
    public void testOutcomeUpdateRecovered() {
        OutcomeRequest outcomeRequest = new OutcomeRequest(1L,1L,"Recovered / Resolved");
        outcomeRequest.setDateOfResolution("10-10-2021 18:30:00");
        AdverseEvent adverseEvent = new AdverseEvent(1L,1L,1L,1L);
        AersOutcome outcome = new AersOutcome("Fatal","dont know");

        when(adverseEventRepository.findById(1L)).thenReturn(Optional.of(adverseEvent));
        when(outcomeRepository.findById(1L)).thenReturn(Optional.of(outcome));

        aersService.updateOutcome(outcomeRequest);

        verify(adverseEventRepository, Mockito.times(1)).findById(1L);
        verify(outcomeRepository, Mockito.times(1)).findById(1L);
        verify(outcomeRepository, Mockito.times(1)).save(outcome);
        verify(adverseEventRepository,Mockito.times(1)).save(adverseEvent);

    }

    @Test
    public void testOutcomeUpdateRecoveredSequel() {
        OutcomeRequest outcomeRequest = new OutcomeRequest(1L,1L,"Recovered / Resolved with sequel");
        AdverseEvent adverseEvent = new AdverseEvent(1L,1L,1L,1L);
        AersOutcome outcome = new AersOutcome("Fatal","dont know");

        when(adverseEventRepository.findById(1L)).thenReturn(Optional.of(adverseEvent));
        when(outcomeRepository.findById(1L)).thenReturn(Optional.of(outcome));

        aersService.updateOutcome(outcomeRequest);

        verify(adverseEventRepository, Mockito.times(1)).findById(1L);
        verify(outcomeRepository, Mockito.times(1)).findById(1L);
        verify(outcomeRepository, Mockito.times(1)).save(outcome);
        verify(adverseEventRepository,Mockito.times(1)).save(adverseEvent);

    }

    @Test
    public void testOutcomeUpdate_after_add() {
        OutcomeRequest outcomeRequest = new OutcomeRequest(1L,1L, "Fatal");
        outcomeRequest.setDateOfResolution("18-10-2021 18:30:00");
        AdverseEvent adverseEvent = new AdverseEvent(1L,1L,1L,1L);
        adverseEvent.setOnsetDate(Utils.toLocalDateTimeWithNull("16-10-2021 18:30:00"));
        AersOutcome outcome = new AersOutcome("Non fatal","yes");

        when(adverseEventRepository.findById(1L)).thenReturn(Optional.of(adverseEvent));
        when(outcomeRepository.findById(1L)).thenReturn(Optional.of(outcome));

        aersService.addOutcome(outcomeRequest);

        verify(adverseEventRepository, Mockito.times(2)).findById(1L);
        verify(outcomeRepository, Mockito.times(1)).findById(1L);
        verify(outcomeRepository, Mockito.times(1)).save(outcome);
        verify(adverseEventRepository,Mockito.times(1)).save(adverseEvent);

    }

    @Test
    public void getAdverseReaction() throws IOException {
        AdverseEvent adverseEvent = new AdverseEvent(1L,1L,1L,1L);
        List<KeyValuePair> predominantCondition = new ArrayList<>();
        predominantCondition.add(new KeyValuePair());
        String predominantConditionString = Utils.asJsonString(predominantCondition);
        AdverseReaction adverseReaction = new AdverseReaction("10","18-10-2021 18:30:00", "10-10-2021 18:30:00", predominantConditionString);

        when(adverseEventRepository.findById(any())).thenReturn(Optional.of(adverseEvent));
        when(adverseReactionRepository.findById(any())).thenReturn(Optional.of(adverseReaction));

        AdverseReactionResponse response = aersService.getAdverseReaction(1L);
        assertEquals(1L,(long)response.getEpisodeId());
        assertEquals("10",response.getCurrentWeight());
        assertEquals(("2021-10-18T18:30"),response.getDateOfDeath());
    }

    @Test
    public void getAdverseReactionAllNullDates() throws IOException {
        AdverseEvent adverseEvent = new AdverseEvent(1L,1L,1L,1L);
        AdverseReaction adverseReaction = new AdverseReaction("10",null);

        when(adverseEventRepository.findById(any())).thenReturn(Optional.of(adverseEvent));
        when(adverseReactionRepository.findById(any())).thenReturn(Optional.of(adverseReaction));

        AdverseReactionResponse response = aersService.getAdverseReaction(1L);
        assertEquals(1L,(long)response.getEpisodeId());
        assertEquals("10",response.getCurrentWeight());
    }

    @Test
    public void getCausalityManagement() throws IOException {
        AdverseEvent adverseEvent = new AdverseEvent(1L,1L,1L,1L);
        CausalityManagement causalityManagement = new CausalityManagement("test1","test2");

        when(adverseEventRepository.findById(any())).thenReturn(Optional.of(adverseEvent));
        when(causalityManagementRepository.findById(any())).thenReturn(Optional.of(causalityManagement));

        CausalityManagementResponse response = aersService.getCausalityManagement(1L);
        assertEquals(1L,(long)response.getEpisodeId());
        assertEquals("test1",response.getCauseMedicineLinkage());
        assertEquals("test2",response.getDechallenge());
    }

    @Test
    public void getCausalityManagementWithDtos() throws IOException {
        AdverseEvent adverseEvent = new AdverseEvent(1L,1L,1L,1L);
        SuspectedDrugsDto suspectedDrugsDto = new SuspectedDrugsDto();
        suspectedDrugsDto.setAmikacin("amikacin");
        String suspectedDrugs = Utils.asJsonString(suspectedDrugsDto);
        List<CausalityMedicineDto> medicinesDto = new ArrayList<>();
        medicinesDto.add(new CausalityMedicineDto());
        String medicines = Utils.asJsonString(medicinesDto);
        List<KeyValuePair> actionTaken = new ArrayList<>();
        actionTaken.add(new KeyValuePair());
        String actionTakenOrNot = "Yes";
        String actionTakenString = Utils.asJsonString(actionTaken);
        CausalityManagement causalityManagement = new CausalityManagement("test1","test2", suspectedDrugs,actionTakenOrNot,actionTakenString, medicines);

        when(adverseEventRepository.findById(any())).thenReturn(Optional.of(adverseEvent));
        when(causalityManagementRepository.findById(any())).thenReturn(Optional.of(causalityManagement));

        CausalityManagementResponse response = aersService.getCausalityManagement(1L);
        assertEquals(1L,(long)response.getEpisodeId());
        assertEquals("test1",response.getCauseMedicineLinkage());
        assertEquals("test2",response.getDechallenge());
    }

    @Test
    public void testGetOutcome() {
        AdverseEvent adverseEvent = new AdverseEvent(1L,1L,1L,1L);
        AersOutcome outcome = new AersOutcome("test1","test2");

        when(adverseEventRepository.findById(any())).thenReturn(Optional.of(adverseEvent));
        when(outcomeRepository.findById(any())).thenReturn(Optional.of(outcome));

        OutcomeResponse response = aersService.getOutcome(1L);
        assertEquals(1L,(long)response.getEpisodeId());
        assertEquals("test1",response.getOutcome());
        assertEquals("test2",response.getCauseOfDeath());
    }

    @Test
    public void testGetOutcomeWithDates() {
        AdverseEvent adverseEvent = new AdverseEvent(1L,1L,1L,1L);
        AersOutcome outcome = new AersOutcome("test1","test2", "10-10-2021 18:30:00");

        when(adverseEventRepository.findById(any())).thenReturn(Optional.of(adverseEvent));
        when(outcomeRepository.findById(any())).thenReturn(Optional.of(outcome));

        OutcomeResponse response = aersService.getOutcome(1L);
        assertEquals(1L,(long)response.getEpisodeId());
        assertEquals("test1",response.getOutcome());
        assertEquals("test2",response.getCauseOfDeath());
    }

    @Test(expected = ValidationException.class)
    public void testAdverseEventException() {
        AdverseReactionRequest adverseReactionRequest = new AdverseReactionRequest();

        aersService.updateAdverseReaction(adverseReactionRequest);
    }

    @Test(expected = ValidationException.class)
    public void testAdverseReactionException() {
        AdverseReactionRequest adverseReactionRequest = new AdverseReactionRequest(1L,1L);
        AdverseEvent adverseEvent = new AdverseEvent(1L,1L);

        when(adverseEventRepository.findById(any())).thenReturn(Optional.of(adverseEvent));

        aersService.updateAdverseReaction(adverseReactionRequest);

    }

    @Test(expected = ValidationException.class)
    public void testSaveCausalityException() {
        CausalityManagementRequest causalityManagementRequest = new CausalityManagementRequest(1L,1L);
        AdverseEvent adverseEvent = new AdverseEvent(1L,1L,1L);

        when(adverseEventRepository.findById(any())).thenReturn(Optional.of(adverseEvent));

        aersService.updateCausalityManagement(causalityManagementRequest);
    }

    @Test(expected = ValidationException.class)
    public void testSaveOutcomeException() {
        OutcomeRequest outcomeRequest = new OutcomeRequest(1L,1L);
        AdverseEvent adverseEvent = new AdverseEvent(1L,1L,1L,1L);

        when(adverseEventRepository.findById(any())).thenReturn(Optional.of(adverseEvent));

        aersService.updateOutcome(outcomeRequest);
    }

    @Test(expected = ValidationException.class)
    public void testGetOutcomeException() {
        OutcomeRequest outcomeRequest = new OutcomeRequest(1L,1L);
        AdverseEvent adverseEvent = new AdverseEvent(1L,1L,1L,1L);

        when(adverseEventRepository.findById(any())).thenReturn(Optional.of(adverseEvent));

        aersService.getOutcome(1L);
    }

    @Test(expected = ValidationException.class)
    public void testGetAdverseReactionException() throws IOException {

        AdverseEvent adverseEvent = new AdverseEvent(1L,1L,1L,1L);

        when(adverseEventRepository.findById(any())).thenReturn(Optional.of(adverseEvent));

        aersService.getAdverseReaction(1L);
    }

    @Test(expected = ValidationException.class)
    public void testGetCausalityException() throws IOException {
        AdverseEvent adverseEvent = new AdverseEvent(1L,1L,1L,1L);

        when(adverseEventRepository.findById(any())).thenReturn(Optional.of(adverseEvent));

        aersService.getCausalityManagement(1L);
    }

    @Test
    public void testGetAersStatus() {
        List<AdverseEvent> adverseEventList = new ArrayList<>();
        adverseEventList.add(new AdverseEvent(1L,1L,2L,3L,"18-10-2021 18:30:00","18-11-2021 18:30:00"));
        adverseEventList.add(new AdverseEvent(2L,4L,5L,null,"21-12-2021 18:30:00"));
        adverseEventList.add(new AdverseEvent(2L,4L,5L,null,null));

        when(adverseEventRepository.getAllByEpisodeId(1L)).thenReturn(adverseEventList);

        List<FormConfigResponse> aersStatusList = aersService.getStatus(1L);

        assertEquals("2021-10-18T18:30",aersStatusList.get(0).getOnsetDate());
        assertEquals("2021-11-18T18:30",aersStatusList.get(0).getOutcomeDate());
        assertEquals("2021-12-21T18:30",aersStatusList.get(1).getOnsetDate());
        assertNull(aersStatusList.get(1).getOutcomeDate());
        assertEquals(5L,(long)aersStatusList.get(1).getFormConfigs().get(1).getId());
        assertNull(aersStatusList.get(1).getFormConfigs().get(2).getId());
    }

    @Test(expected = ValidationException.class)
    public void testGetAdverseEventException() throws IOException {

        when(adverseEventRepository.findById(1L)).thenReturn(Optional.empty());

        aersService.getAdverseReaction(1L);
    }

    @Test
    public void getCausalityManagementNullId() throws IOException {
        AdverseEvent adverseEvent = new AdverseEvent(1L, 1L);

        when(adverseEventRepository.findById(any())).thenReturn(Optional.of(adverseEvent));

        CausalityManagementResponse response = aersService.getCausalityManagement(1L);
        assertNull(response);
    }

    @Test
    public void testGetOutcomeNullId() {
        AdverseEvent adverseEvent = new AdverseEvent(1L,1L);

        when(adverseEventRepository.findById(any())).thenReturn(Optional.of(adverseEvent));

        OutcomeResponse response = aersService.getOutcome(1L);
        assertNull(response);
    }

    @Test
    public void testValidateDatesSuccess() {
        String hospitalAdmissionDate = "14-12-2021 18:30:00";
        String hospitalDischargeDate = "16-12-2021 18:30:00";
        String dateOfDeath = "17-12-2021 18:30:00";

        aersService.validateDates(dateOfDeath, hospitalAdmissionDate, hospitalDischargeDate);
    }

    @Test
    public void testValidateDeathDateVsOnsetSuccess() {
        String dateOfOnset = "16-12-2021 18:30:00";
        String dateOfDeath = "17-12-2021 18:30:00";

        aersService.validateDeathDateVsOnset(dateOfDeath, dateOfOnset);
    }

    @Test(expected = ValidationException.class)
    public void testValidateDatesAdmissionDateBefore() {
        String hospitalAdmissionDate = "14-12-2021 18:30:00";
        String hospitalDischargeDate = "13-12-2021 18:30:00";

        aersService.validateDates(null, hospitalAdmissionDate, hospitalDischargeDate);
    }


    @Test(expected = ValidationException.class)
    public void testValidateDateDeathBeforeHospitalAdmission() {
        String hospitalAdmissionDate = "14-12-2021 18:30:00";
        String dateOfDeath = "13-12-2021 18:30:00";

        aersService.validateDates(dateOfDeath, hospitalAdmissionDate, null);
    }

    @Test(expected = ValidationException.class)
    public void testValidateDateDeathBeforeHospitalDischarge() {
        String hospitalAdmissionDate = "14-12-2021 18:30:00";
        String hospitalDischargeDate = "16-12-2021 18:30:00";
        String dateOfDeath = "15-12-2021 18:30:00";

        aersService.validateDates(dateOfDeath, hospitalAdmissionDate, hospitalDischargeDate);
    }

    @Test
    public void testValidateDatesMedicineSuccess() {
        aersService.validateDates(null);

        List<CausalityMedicineDto> medicines = new ArrayList<>();
        CausalityMedicineDto medicine = new CausalityMedicineDto();
        medicine.setStartDate("10-10-2021 18:30:00");
        medicine.setStopDate("11-10-2021 18:30:00");
        medicines.add(medicine);
        aersService.validateDates(medicines);
    }

    @Test(expected = ValidationException.class)
    public void testValidateDatesMedicineFailure() {
        List<CausalityMedicineDto> medicines = new ArrayList<>();
        CausalityMedicineDto medicine = new CausalityMedicineDto();
        medicine.setStartDate("11-10-2021 18:30:00");
        medicine.setStopDate("9-10-2021 18:30:00");
        medicines.add(medicine);

        aersService.validateDates(medicines);
    }

    @Test(expected = ValidationException.class)
    public void testValidateDeathDateVsOnsetFailure() {
        String dateOfOnset = "17-12-2021 18:30:00";
        String dateOfDeath = "16-12-2021 18:30:00";

        aersService.validateDeathDateVsOnset(dateOfDeath, dateOfOnset);
    }

}

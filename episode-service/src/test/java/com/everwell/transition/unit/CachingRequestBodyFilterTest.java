package com.everwell.transition.unit;

import com.everwell.transition.BaseTest;
import com.everwell.transition.filters.CachingRequestBodyFilter;
import com.everwell.transition.utils.MultipleReadHttpRequest;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;

import javax.servlet.FilterChain;
import javax.servlet.ReadListener;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static reactor.core.publisher.Mono.when;

public class CachingRequestBodyFilterTest extends BaseTest {

    @Mock
    HttpServletRequest httpServletRequest;

    @Mock
    HttpServletResponse httpServletResponse;

    @Mock
    FilterChain filterChain;

    @Mock
    MultipleReadHttpRequest multipleReadHttpRequest;

    @Spy
    CachingRequestBodyFilter cachingRequestBodyFilter;

    @Test
    public void doFilterTest() throws ServletException, IOException {
        doReturn(multipleReadHttpRequest).when(cachingRequestBodyFilter).getMultipleReadHttpRequest(httpServletRequest);
        cachingRequestBodyFilter.doFilter(httpServletRequest, httpServletResponse, filterChain);
        Mockito.verify(cachingRequestBodyFilter, Mockito.times(1)).getMultipleReadHttpRequest(httpServletRequest);
        Mockito.verify(filterChain, Mockito.times(1)).doFilter(multipleReadHttpRequest, httpServletResponse);
    }

    @Test
    public void getMultipleReadHttpRequestTest() throws IOException {
        Mockito.when(httpServletRequest.getInputStream()).thenReturn(MultipleReadHttpRequestTest.getServletInputStream("temp_body"));
        MultipleReadHttpRequest requestTest = cachingRequestBodyFilter.getMultipleReadHttpRequest(httpServletRequest);
        Assert.assertEquals(new String(requestTest.getCacheData()), "temp_body");
    }
}

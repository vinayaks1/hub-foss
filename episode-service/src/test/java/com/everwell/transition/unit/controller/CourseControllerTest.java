package com.everwell.transition.unit.controller;

import com.everwell.transition.BaseTest;
import com.everwell.transition.controller.CourseController;
import com.everwell.transition.exceptions.CustomExceptionHandler;
import com.everwell.transition.model.dto.CourseDto;
import com.everwell.transition.model.request.course.MarkCourseRequest;
import com.everwell.transition.model.response.CourseResponse;
import com.everwell.transition.model.response.MarkCourseResponse;
import com.everwell.transition.service.CourseService;
import com.everwell.transition.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CourseControllerTest extends BaseTest{
    private MockMvc mockMvc;

    @InjectMocks
    private CourseController courseController;

    @Mock
    CourseService courseService;


    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(courseController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void getCoursesTest() throws Exception {
        long episodeId = 12345L;
        String uri = "/v1/course/" + episodeId;
        when(courseService.getAllCourses(episodeId)).thenReturn(getCourses());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.episodeId").value(episodeId))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.courses[0].language").value("English"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.courses[1].language").value("Hindi"));
    }

    @Test
    public void getCoursesTest_ZeroEpisodeId() throws Exception {
        long episodeId = 12345L;
        String uri = "/v1/course/0";
        when(courseService.getAllCourses(episodeId)).thenReturn(getCourses());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("episodeId must not be null!"));
    }

    @Test
    public void MarkCourseTest() throws Exception {
        String uri = "/v1/course/status";

        MarkCourseRequest markCourseRequest = new MarkCourseRequest(1L,1L);
        MarkCourseResponse markCourseResponse = new MarkCourseResponse(1L,1L,LocalDateTime.now());
        when(courseService.markCourseAsComplete(markCourseRequest)).thenReturn(markCourseResponse);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(markCourseRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.episodeId").value(1L))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.courseId").value(1L));
    }

    @Test
    public void MarkCourseTest_NullEpisodeIdTest() throws Exception {
        String uri = "/v1/course/status";

        MarkCourseRequest markCourseRequest = new MarkCourseRequest(null,1L);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(markCourseRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("episodeId must not be null!"));
    }

    CourseResponse getCourses() {
        long episodeId = 12345;
        List<CourseDto> courses = Arrays.asList(
                new CourseDto(1L,"English","1. Introduction to TB","https://igot.gov.in/explore-course/course/do_3134313464520212481444",true),
                new CourseDto(2L, "Hindi","5. टीबी चैंपियंस की भूमिका","https://igot.gov.in/explore-course/course/do_31343981751268147213108",false)
        );

        return new CourseResponse(episodeId, courses);
    }

}

package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.constants.AppointmentConstants;
import com.everwell.transition.exceptions.InternalServerErrorException;
import com.everwell.transition.exceptions.NotFoundException;
import com.everwell.transition.model.db.Appointment;
import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.dto.appointment.AppointmentData;
import com.everwell.transition.model.dto.appointment.AppointmentDetails;
import com.everwell.transition.model.dto.appointment.Document;
import com.everwell.transition.model.dto.appointment.QuestionAnswers;
import com.everwell.transition.model.request.appointment.*;
import com.everwell.transition.model.response.appointment.AppointmentStaffMap;
import com.everwell.transition.model.response.appointment.GetAppointmentResponse;
import com.everwell.transition.repositories.AppointmentAnswerRepository;
import com.everwell.transition.repositories.AppointmentRepository;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.EpisodeService;
import com.everwell.transition.service.RegistryService;
import com.everwell.transition.service.impl.AppointmentServiceImpl;
import com.everwell.transition.utils.Utils;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;

import java.time.LocalDateTime;
import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

public class AppointmentServiceTest extends BaseTest {
    @Spy
    @InjectMocks
    AppointmentServiceImpl appointmentService;

    @Mock
    private AppointmentRepository appointmentRepository;

    @Mock
    private AppointmentAnswerRepository appointmentAnswerRepository;

    @Mock
    private EpisodeService episodeService;

    @Mock
    private ClientService clientService;

    @Mock
    private RegistryService registryService;

    private EpisodeDto episodeDetailsBasic() {
        EpisodeDto episodeDto = new EpisodeDto(1L, 1L, 1L, true, "DSTB", 1L, null, Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, null, 1L, "PRESUMPTIVE_OPEN", "Presumptive Open", "IndiaTbPublic", 1L, null, null, null, null, null);
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("name", "Test Name");
        episodeDto.setStageData(stageData);
        return episodeDto;
    }

    private Appointment getSampleAppointment() {
        return new Appointment(1, null, 1L, null, null, null, 1, null, AppointmentConstants.DEFAULT_STATUS);
    }

    @Test
    public void testAddAppointment() {
        AppointmentData sampleAppointmentData = new AppointmentData(
                1L,
                "STAFF",
                new AppointmentRequest(null, 1L, LocalDateTime.now()),
                new ArrayList<QuestionAnswers>(),
                new ArrayList<Document>()
        );
        ArrayList<QuestionAnswers> sampleQuestionnaire = new ArrayList<QuestionAnswers>();
        sampleQuestionnaire.add(new QuestionAnswers(1L, "sample answer"));
        sampleAppointmentData.setQuestionnaire(sampleQuestionnaire);

        Appointment savedAppointment = getSampleAppointment();

        when(appointmentRepository.save(any())).thenReturn(savedAppointment);

        AppointmentData response = appointmentService.addAppointment(sampleAppointmentData);
        Assert.assertNotNull(response.getAppointment().getId());
    }

    @Test
    public void testAddAppointmentByPatient() {
        AppointmentData sampleAppointmentData = new AppointmentData(
                1L,
                "PATIENT",
                new AppointmentRequest(null, 1L, LocalDateTime.now()),
                new ArrayList<QuestionAnswers>(),
                new ArrayList<Document>()
        );
        ArrayList<QuestionAnswers> sampleQuestionnaire = new ArrayList<QuestionAnswers>();
        sampleQuestionnaire.add(new QuestionAnswers(1L, "sample answer"));
        sampleAppointmentData.setQuestionnaire(sampleQuestionnaire);

        Appointment savedAppointment = getSampleAppointment();

        when(appointmentRepository.save(any())).thenReturn(savedAppointment);
        when(registryService.addAppointmentStaffMap(any())).thenReturn(new AppointmentStaffMap(1, 1L, 1));

        AppointmentData response = appointmentService.addAppointment(sampleAppointmentData);
        Assert.assertNotNull(response.getAppointment().getId());
    }

    @Test(expected = InternalServerErrorException.class)
    public void testAddAppointmentByPatient_ThrowsErrorIfNotSavedAtRegistry() throws InternalServerErrorException {
        AppointmentData sampleAppointmentData = new AppointmentData(
                1L,
                "PATIENT",
                new AppointmentRequest(null, 1L, LocalDateTime.now()),
                new ArrayList<QuestionAnswers>(),
                new ArrayList<Document>()
        );
        ArrayList<QuestionAnswers> sampleQuestionnaire = new ArrayList<QuestionAnswers>();
        sampleQuestionnaire.add(new QuestionAnswers(1L, "sample answer"));
        sampleAppointmentData.setQuestionnaire(sampleQuestionnaire);

        Appointment savedAppointment = getSampleAppointment();

        when(appointmentRepository.save(any())).thenReturn(savedAppointment);
        when(registryService.addAppointmentStaffMap(any())).thenReturn(null);

        AppointmentData response = appointmentService.addAppointment(sampleAppointmentData);
        Assert.assertNotNull(response.getAppointment().getId());
    }

    @Test
    public void testAddAppointmentWithoutQuestions() {
        AppointmentData sampleAppointmentData = new AppointmentData(
                1L,
                "STAFF",
                new AppointmentRequest(null, 1L, LocalDateTime.now()),
                null,
                new ArrayList<Document>()
        );
        when(appointmentRepository.save(any())).thenReturn(getSampleAppointment());

        AppointmentData response = appointmentService.addAppointment(sampleAppointmentData);
        Assert.assertNotNull(response.getAppointment().getId());
    }

    @Test
    public void testGetAppointment() throws Exception {
        Integer sampleAppointmentId = 1;

        doReturn(episodeDetailsBasic()).when(episodeService)
                .getEpisodeDetails(ArgumentMatchers.any(), ArgumentMatchers.any());
        when(appointmentRepository.findByIdAndDeletedAtIsNull(sampleAppointmentId)).thenReturn(getSampleAppointment());
        when(appointmentAnswerRepository.getAllByAppointmentId(any())).thenReturn(null);
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(1L, "client", "client", new Date()));

        GetAppointmentResponse response = appointmentService.getAppointmentResponse(sampleAppointmentId);
        Assert.assertNotNull(response.getAppointment().getId());
    }

    @Test(expected = NotFoundException.class)
    public void testGetAppointmentFailure() {
        Integer sampleAppointmentId = 1;
        doReturn(episodeDetailsBasic()).when(episodeService)
                .getEpisodeDetails(ArgumentMatchers.any(), ArgumentMatchers.any());
        when(appointmentRepository.findById(sampleAppointmentId)).thenReturn(Optional.empty());
        when(appointmentAnswerRepository.getAllByAppointmentId(any())).thenReturn(null);
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(1L, "client", "client", new Date()));

        appointmentService.getAppointmentResponse(sampleAppointmentId);
    }

    @Test
    public void testGetAppointmentsListByStatus() throws Exception {
        List<Integer> appointmentIds = new ArrayList<>();
        appointmentIds.add(1);
        appointmentIds.add(2);
        AppointmentListRequest request = new AppointmentListRequest(appointmentIds, AppointmentConstants.DEFAULT_STATUS, null, null, null);


        Appointment savedAppointment1 = new Appointment(1, null, 1L, null, null, LocalDateTime.now(), 1, null, AppointmentConstants.DEFAULT_STATUS);
        Appointment savedAppointment2 = new Appointment(2, null, 1L, null, null, LocalDateTime.now(), 1, null, AppointmentConstants.DEFAULT_STATUS);
        List<Appointment> findAllByIdResponse = new ArrayList<>();
        findAllByIdResponse.add(savedAppointment1);
        findAllByIdResponse.add(savedAppointment2);

        doReturn(episodeDetailsBasic()).when(episodeService)
                .getEpisodeDetails(ArgumentMatchers.any(), ArgumentMatchers.any());
        when(appointmentRepository.findAllByIdInAndDeletedAtIsNull(appointmentIds)).thenReturn(findAllByIdResponse);
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(1L, "client", "client", new Date()));

        List<AppointmentDetails> response = appointmentService.getAppointments(request);
        Assert.assertNotNull(response);
        Assert.assertTrue(response.stream().count() > 0);
    }

    @Test
    public void testGetAppointmentsListWithDatesFilter() throws Exception {
        List<Integer> appointmentIds = new ArrayList<>();
        appointmentIds.add(1);
        appointmentIds.add(2);
        AppointmentListRequest request = new AppointmentListRequest(appointmentIds, AppointmentConstants.DEFAULT_STATUS, null, LocalDateTime.now().minusYears(1), LocalDateTime.now());


        Appointment savedAppointment1 = new Appointment(1, null, 1L, null, null, LocalDateTime.now(), 1, null, AppointmentConstants.DEFAULT_STATUS);
        Appointment savedAppointment2 = new Appointment(2, null, 1L, null, null, LocalDateTime.now().minusDays(2), 1, null, AppointmentConstants.DEFAULT_STATUS);
        List<Appointment> findAllByIdResponse = new ArrayList<>();
        findAllByIdResponse.add(savedAppointment1);
        findAllByIdResponse.add(savedAppointment2);

        doReturn(episodeDetailsBasic()).when(episodeService)
                .getEpisodeDetails(ArgumentMatchers.any(), ArgumentMatchers.any());
        when(appointmentRepository.findAllByIdInAndDeletedAtIsNull(appointmentIds)).thenReturn(findAllByIdResponse);
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(1L, "client", "client", new Date()));

        List<AppointmentDetails> response = appointmentService.getAppointments(request);
        Assert.assertNotNull(response);
        Assert.assertEquals(1, response.stream().count());
    }

    @Test
    public void testGetAppointmentsListByEpisodeId() throws Exception {
        Long episodeId = 1L;
        AppointmentListRequest request = new AppointmentListRequest(null, null, episodeId, null, null);

        Appointment savedAppointment1 = new Appointment(1, null, 1L, null, null, LocalDateTime.now(), 1, null, AppointmentConstants.DEFAULT_STATUS);
        Appointment savedAppointment2 = new Appointment(2, null, 1L, null, null, LocalDateTime.now(), 1, null, AppointmentConstants.DEFAULT_STATUS);
        List<Appointment> findAllByIdResponse = new ArrayList<>();
        findAllByIdResponse.add(savedAppointment1);
        findAllByIdResponse.add(savedAppointment2);

        doReturn(episodeDetailsBasic()).when(episodeService)
                .getEpisodeDetails(ArgumentMatchers.any(), ArgumentMatchers.any());
        when(appointmentRepository.findAllByEpisodeIdAndDeletedAtIsNull(episodeId)).thenReturn(findAllByIdResponse);
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(1L, "client", "client", new Date()));

        List<AppointmentDetails> response = appointmentService.getAppointments(request);
        Assert.assertNotNull(response);
        Assert.assertTrue(response.stream().count() > 0);
    }

    @Test
    public void testGetAppointmentsListWithoutStatus() throws Exception {
        List<Integer> appointmentIds = new ArrayList<>();
        appointmentIds.add(1);
        appointmentIds.add(2);
        AppointmentListRequest request = new AppointmentListRequest(appointmentIds, null, null, null, null);
        Appointment savedAppointment1 = new Appointment(1, null, 1L, null, null, LocalDateTime.now(), 1, null, AppointmentConstants.DEFAULT_STATUS);
        Appointment savedAppointment2 = new Appointment(2, null, 1L, null, null, LocalDateTime.now(), 1, null, AppointmentConstants.DEFAULT_STATUS);
        List<Appointment> findAllByIdResponse = new ArrayList<>();
        findAllByIdResponse.add(savedAppointment1);
        findAllByIdResponse.add(savedAppointment2);

        doReturn(episodeDetailsBasic()).when(episodeService)
                .getEpisodeDetails(ArgumentMatchers.any(), ArgumentMatchers.any());
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(1L, "client", "client", new Date()));
        when(appointmentRepository.findAllByIdInAndDeletedAtIsNull(appointmentIds)).thenReturn(findAllByIdResponse);

        List<AppointmentDetails> response = appointmentService.getAppointments(request);
        Assert.assertNotNull(response);
        Assert.assertTrue(response.stream().count() > 0);
    }

    @Test
    public void testEndAppointment() {
        Integer sampleAppointmentId = 1;

        when(appointmentRepository.findByIdAndDeletedAtIsNull(any())).thenReturn(getSampleAppointment());
        when(appointmentRepository.save(any())).thenReturn(getSampleAppointment());

        String response = appointmentService.endAppointment(sampleAppointmentId);
        Assert.assertEquals(AppointmentConstants.END_SUCCESS, response);
    }

    @Test
    public void testConfirmAppointment() {
        Integer sampleAppointmentId = 1;
        Appointment savedAppointment = new Appointment(1, null, 1L, null, null, null, 1, null, AppointmentConstants.DEFAULT_STATUS);

        when(appointmentRepository.findByIdAndDeletedAtIsNull(any())).thenReturn(savedAppointment);
        when(appointmentRepository.save(any())).thenReturn(savedAppointment);

        Appointment response = appointmentService.confirmAppointment(sampleAppointmentId);
        Assert.assertEquals(savedAppointment.getId(), response.getId());
    }

    @Test(expected = NotFoundException.class)
    public void testConfirmAppointmentFailureForId() {
        Integer sampleAppointmentId = 1;
        when(appointmentRepository.findByIdAndDeletedAtIsNull(any())).thenReturn(null);
        appointmentService.confirmAppointment(sampleAppointmentId);
    }

    @Test
    public void testFollowUpAppointment() {
        Appointment savedAppointment = getSampleAppointment();
        Appointment followUpAppointment = new Appointment(2, 1, 1L, null, null, LocalDateTime.now(), 2, null, AppointmentConstants.STATUS_CONFIRMED);

        when(appointmentRepository.findByIdAndDeletedAtIsNull(any())).thenReturn(savedAppointment);
        when(appointmentRepository.save(any())).thenReturn(followUpAppointment);

        Appointment response = appointmentService.createFollowUpAppointment(new FollowUpRequest(1, LocalDateTime.now()));
        Assert.assertNotNull(response);
    }

    @Test
    public void testRescheduleAppointment() {
        when(appointmentRepository.findByIdAndDeletedAtIsNull(any())).thenReturn(getSampleAppointment());
        Appointment response = appointmentService.rescheduleAppointment(new RescheduleRequest(1, LocalDateTime.now()));
        Assert.assertNotNull(response);
    }

    @Test
    public void testDeleteAppointment() {
        when(appointmentRepository.findByIdAndDeletedAtIsNull(any())).thenReturn(getSampleAppointment());
        when(episodeService.getEpisodeDetails(ArgumentMatchers.any(), ArgumentMatchers.any()))
                .thenReturn(episodeDetailsBasic());
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(1L, "client", "client", new Date()));

        appointmentService.deleteAppointment(new DeleteAppointmentRequest(1L, 1));
    }

}

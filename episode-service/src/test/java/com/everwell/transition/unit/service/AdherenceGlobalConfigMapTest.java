package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.response.AdherenceCodeConfigResponse;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.response.adherence.AdherenceGlobalConfigResponse;
import com.everwell.transition.model.response.adherence.GenericConfigResponse;
import com.everwell.transition.postconstruct.AdherenceGlobalConfigMap;
import com.everwell.transition.service.IAMService;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

public class AdherenceGlobalConfigMapTest extends BaseTest {

    @InjectMocks
    AdherenceGlobalConfigMap adherenceGlobalConfigMap;

    @Mock
    IAMService iamService;

    @Test
    public void createAdherenceCodeMapTest() {
        List<AdherenceCodeConfigResponse> adherenceCodeConfigResponseList =  Arrays.asList(
                new AdherenceCodeConfigResponse('6', "NO_INFO", null, "No Info")
        );
        List<GenericConfigResponse> adherenceTechConfigResponseList =  Arrays.asList(
                new GenericConfigResponse("merm", 3)
        );
        List<GenericConfigResponse> scheduleTypeConfigResponseList =  Arrays.asList(
                new GenericConfigResponse("daily", 1)
        );
        AdherenceGlobalConfigResponse adherenceGlobalConfigResponse = new AdherenceGlobalConfigResponse(adherenceCodeConfigResponseList,
                adherenceTechConfigResponseList, scheduleTypeConfigResponseList);
        ResponseEntity<Response<AdherenceGlobalConfigResponse>> responseEntity = new ResponseEntity<>(new Response<>(adherenceGlobalConfigResponse), HttpStatus.OK);
        when(iamService.getAdherenceGlobalConfig(any())).thenReturn(responseEntity);
        adherenceGlobalConfigMap.createConfigMaps();
        Assert.assertEquals('6', adherenceGlobalConfigMap.getCodeToConfigMap().get("NO_INFO").getCode());
        Assert.assertEquals(3, adherenceGlobalConfigMap.getMonitoringMethodToIdMap().get("merm").intValue());
        Assert.assertEquals(1, adherenceGlobalConfigMap.getScheduleTypeToIdMap().get("daily").intValue());
    }

    @Test
    public void createAdherenceCodeMapTestNullResponse() {
        ResponseEntity<Response<AdherenceGlobalConfigResponse>> responseEntity = null;
        when(iamService.getAdherenceGlobalConfig(any())).thenReturn(responseEntity);
        adherenceGlobalConfigMap.createConfigMaps();
        Assert.assertTrue(adherenceGlobalConfigMap.getCodeToConfigMap().isEmpty());
        Assert.assertTrue(adherenceGlobalConfigMap.getMonitoringMethodToIdMap().isEmpty());
        Assert.assertTrue(adherenceGlobalConfigMap.getScheduleTypeToIdMap().isEmpty());
    }

    @Test
    public void createAdherenceCodeMapWithExceptionTest() {
        doThrow(ValidationException.class).when(iamService).getAdherenceGlobalConfig(any());
        adherenceGlobalConfigMap.createConfigMaps();
    }
}

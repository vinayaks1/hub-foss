package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.enums.iam.IAMEndpoint;
import com.everwell.transition.constants.IAMConstants;
import com.everwell.transition.exceptions.InternalServerErrorException;
import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.dto.IAMScheduleDto;
import com.everwell.transition.model.request.adherence.*;
import com.everwell.transition.model.response.AdherenceCodeConfigResponse;
import com.everwell.transition.model.response.AdherenceResponse;
import com.everwell.transition.model.response.AllAdherenceResponse;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.response.adherence.AllAvgAdherenceResponse;
import com.everwell.transition.model.response.adherence.PositiveCountResponse;
import com.everwell.transition.model.response.adherence.AdherenceGlobalConfigResponse;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.impl.IAMServiceImpl;
import com.everwell.transition.utils.Utils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.*;
import java.util.concurrent.Executors;

import static org.mockito.Mockito.*;

public class IAMServiceTest extends BaseTest  {

    @Spy
    @InjectMocks
    IAMServiceImpl iamHelperService;

    @Mock
    ClientService clientService;

    private ThreadPoolTaskExecutor spyTaskExecutor = new ThreadPoolTaskExecutor();

    @Before
    public void init(){
        ReflectionTestUtils.setField(iamHelperService, "dataGatewayURL", "http://test:8080");
        ReflectionTestUtils.setField(iamHelperService, "username", "test");
        ReflectionTestUtils.setField(iamHelperService, "password", "test");
        spyTaskExecutor.setCorePoolSize(1);
        spyTaskExecutor.setWaitForTasksToCompleteOnShutdown(true);
        spyTaskExecutor.initialize();
        ReflectionTestUtils.setField(iamHelperService, "taskExecutor", spyTaskExecutor);
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(29L,"test", "test", null ));
        iamHelperService.init();
    }

    @Test
    public void getAdherenceConfigTest() {
        ResponseEntity<Response<List<AdherenceCodeConfigResponse>>> response =
                new ResponseEntity<>(
                        new Response<>(
                                Arrays.asList(
                                        new AdherenceCodeConfigResponse('6', "NO_INFO", null, "No Info")
                                )
                        ),
                        HttpStatus.OK
                );
        doReturn(response).when(iamHelperService).getExchange(eq(IAMEndpoint.ADHERENCE_CONFIG.getEndpointUrl()), ArgumentMatchers.any(ParameterizedTypeReference.class), ArgumentMatchers.any(), ArgumentMatchers.any());

        ResponseEntity<Response<List<AdherenceCodeConfigResponse>>> apiResponse = iamHelperService.getAdherenceConfig();
        Assert.assertEquals('6', apiResponse.getBody().getData().get(0).getCode());
    }

    @Test
    public void getAdherenceGlobalConfig() {
        Map<String, Object> configRequest = new HashMap<>();
        AdherenceGlobalConfigResponse adherenceGlobalConfigResponse = new AdherenceGlobalConfigResponse(new ArrayList<>(),
                new ArrayList<>(), new ArrayList<>());
        ResponseEntity<Response<AdherenceGlobalConfigResponse>> response = new ResponseEntity<>(new Response<>(adherenceGlobalConfigResponse), HttpStatus.OK);

        doReturn(response).when(iamHelperService).postExchange(eq(IAMEndpoint.ADHERENCE_GLOBAL_CONFIG.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any());

        ResponseEntity<Response<AdherenceGlobalConfigResponse>> responseEntity = iamHelperService.getAdherenceGlobalConfig(configRequest);
        Assert.assertNotNull(responseEntity.getBody());
        Assert.assertTrue(responseEntity.getBody().isSuccess());
    }

    @Test
    public void getAdherenceTest() {
        AdherenceResponse adherenceResponse = new AdherenceResponse(
                Utils.toLocalDateTimeWithNull("02-01-2022 18:30:00"),
                Utils.toLocalDateTimeWithNull("06-01-2022 18:30:00"),
                "654A9"
        );
        doReturn(new ResponseEntity<>(
                new Response<>(
                        adherenceResponse
                ),
                HttpStatus.OK
        )).when(iamHelperService).getExchange(eq(IAMEndpoint.GET_ADHERENCE.getEndpointUrl()), ArgumentMatchers.any(ParameterizedTypeReference.class), ArgumentMatchers.any(), ArgumentMatchers.any());

        ResponseEntity<Response<AdherenceResponse>> apiResponse = iamHelperService.getAdherence(1L, false);
        Assert.assertEquals("654A9", apiResponse.getBody().getData().getAdherenceString());
    }

    @Test
    public void getAdherenceBulkTest() {
        AdherenceResponse adherenceResponse = new AdherenceResponse(
                Utils.toLocalDateTimeWithNull("02-01-2022 18:30:00"),
                Utils.toLocalDateTimeWithNull("06-01-2022 18:30:00"),
                "654A9"
        );
        SearchAdherenceRequest searchAdherenceRequest = new SearchAdherenceRequest(Arrays.asList("1L"), null);
        doReturn(new ResponseEntity<>(
                new Response<>(
                        new AllAdherenceResponse(
                                Arrays.asList(adherenceResponse)
                        )
                ),
                HttpStatus.OK
        )).when(iamHelperService).postExchange(eq(IAMEndpoint.GET_ADHERENCE_BULK.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any());

        ResponseEntity<Response<AllAdherenceResponse>> apiResponse = iamHelperService.getAdherenceBulk(searchAdherenceRequest);
        Assert.assertEquals("654A9", apiResponse.getBody().getData().getAdherenceResponseList().get(0).getAdherenceString());
    }

    @Test
    public void recordAdherenceForMultipleDates() {
        RecordAdherenceRequest recordAdherenceRequest = new RecordAdherenceRequest();
        doReturn(new ResponseEntity<>(
                new Response<>(
                        Void.class
                ),
                HttpStatus.OK
        )).when(iamHelperService).postExchange(eq(IAMEndpoint.RECORD_ADHERENCE.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any());


        ResponseEntity<Response<Void>> response = iamHelperService.recordAdherenceForMultipleDates(recordAdherenceRequest);
        Assert.assertTrue(response.getBody().isSuccess());
    }

    @Test
    public void registerEntity() {
        doReturn(new ResponseEntity<>(
                new Response<>(
                        Void.class
                ),
                HttpStatus.OK
        )).when(iamHelperService).postExchange(ArgumentMatchers.anyString(), ArgumentMatchers.any(), ArgumentMatchers.any());

        IAMScheduleDto iamScheduleDto = new IAMScheduleDto(IAMConstants.DAILY, null, IAMConstants.FIRST_DOSE_OFFSET, IAMConstants.DEFAULT_POSITIVE_SENSITIVITY, IAMConstants.DEFAULT_NEGATIVE_SENSITIVITY, IAMConstants.SCHEDULE_TYPE_ID);
        boolean response = iamHelperService.registerEntity(String.valueOf(1L), "NONE", "1", "date", iamScheduleDto, new ArrayList<>());
        Assert.assertTrue(response);
    }


    @Test
    public void updateEntityTest() {
        ResponseEntity<Response<Map<String, Object>>> response = new ResponseEntity<>(
                new Response<>(new HashMap<>()),
                HttpStatus.OK
        );
        doReturn(response).when(iamHelperService).putExchange(eq(IAMEndpoint.UPDATE_ENTITY.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any());

        ResponseEntity<Response<Map<String, Object>>> responseEntity = iamHelperService.updateEntity(new EntityRequest());
        Assert.assertNotNull(responseEntity.getBody());
        Assert.assertTrue(responseEntity.getBody().isSuccess());
        Mockito.verify(iamHelperService, Mockito.times(1)).putExchange(eq(IAMEndpoint.UPDATE_ENTITY.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any());
    }
    
    @Test
    public void deleteEntity() {
        ResponseEntity<Response<String>> responseEntity = new ResponseEntity<>(new Response<>(true, "Success"), HttpStatus.OK);
        doReturn(responseEntity).when(iamHelperService).deleteExchange(anyString(), any(), any(), any(), any());

        Long entityId = 1L;
        iamHelperService.deleteEntity(String.valueOf(entityId));

        verify(iamHelperService, times(1)).deleteEntity(eq(String.valueOf(entityId)));
    }

    @Test
    public void deleteEntity_IAMError() {
        ResponseEntity<Response<String>> responseEntity = new ResponseEntity<>(new Response<>(false, "Error"), HttpStatus.BAD_REQUEST);
        doReturn(responseEntity).when(iamHelperService).deleteExchange(anyString(), any(), any(), any(), any());

        Long entityId = 1L;

        InternalServerErrorException exception = Assert.assertThrows(InternalServerErrorException.class, () -> {
            iamHelperService.deleteEntity(String.valueOf(entityId));
        });

        Assert.assertEquals("Error while deleting entity", exception.getMessage());
    }

    @Test
    public void getPositiveEventCountTest() {
        PositiveCountResponse positiveCountResponse = new PositiveCountResponse(10L, 20L, 30L);
        PositiveAdherenceRequest positiveAdherenceRequest = new PositiveAdherenceRequest(Arrays.asList("1L"));
        doReturn(new ResponseEntity<>(
                new Response<>(
                       positiveCountResponse
                ),
                HttpStatus.OK
        )).when(iamHelperService).postExchange(eq(IAMEndpoint.POSITIVE_EVENT.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any());

        ResponseEntity<Response<PositiveCountResponse>> apiResponse = iamHelperService.getPositiveEventCount(positiveAdherenceRequest);
        Assert.assertEquals(positiveCountResponse.getManualCount(), apiResponse.getBody().getData().getManualCount());
        Assert.assertEquals(positiveCountResponse.getDigitalCount(), apiResponse.getBody().getData().getDigitalCount());
        Assert.assertEquals(positiveCountResponse.getMissedCount(), apiResponse.getBody().getData().getMissedCount());
    }

    @Test
    public void getAverageAdherenceTest() {
        AllAvgAdherenceResponse allAvgAdherenceResponse = new AllAvgAdherenceResponse(10d, 20d, new HashMap<>(), new ArrayList<>());
        AverageAdherenceRequest averageAdherenceRequest = new AverageAdherenceRequest(Arrays.asList("1L"), true, new ArrayList<>());
        doReturn(new ResponseEntity<>(
                new Response<>(
                        allAvgAdherenceResponse
                ),
                HttpStatus.OK
        )).when(iamHelperService).postExchange(eq(IAMEndpoint.AVERAGE_ADHERENCE.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any());

        ResponseEntity<Response<AllAvgAdherenceResponse>> apiResponse = iamHelperService.getAverageAdherence(averageAdherenceRequest);
        Assert.assertEquals(allAvgAdherenceResponse.getDigitalAdherence(), apiResponse.getBody().getData().getDigitalAdherence(), 0);
        Assert.assertEquals(allAvgAdherenceResponse.getTotalAdherence(), apiResponse.getBody().getData().getTotalAdherence(), 0);
    }

    @Test
    public void getImeiTest() {
        ResponseEntity<Response<List<Map<String, Object>>>> response = new ResponseEntity<>(
                new Response<>(new ArrayList<>()),
                HttpStatus.OK
        );
        doReturn(response).when(iamHelperService).getExchange(eq(IAMEndpoint.IMEI.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any());

        ResponseEntity<Response<List<Map<String, Object>>>> responseEntity = iamHelperService.getImei("1L", true);
        Assert.assertNotNull(responseEntity.getBody());
        Assert.assertTrue(responseEntity.getBody().isSuccess());
    }

    @Test
    public void getImeiBulkTes() {
        Map<String, Object> imeiRequest = new HashMap<>();
        ResponseEntity<Response<List<Map<String, Object>>>> response = new ResponseEntity<>(
                new Response<>(new ArrayList<>()),
                HttpStatus.OK
        );
        doReturn(response).when(iamHelperService).postExchange(eq(IAMEndpoint.IMEI_BULK.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any());

        ResponseEntity<Response<List<Map<String, Object>>>> responseEntity = iamHelperService.getImeiBulk(imeiRequest);
        Assert.assertNotNull(responseEntity.getBody());
        Assert.assertTrue(responseEntity.getBody().isSuccess());
    }

    @Test
    public void getActiveEntityFromImeiTest() {
        ResponseEntity<Response<List<Map<String, Object>>>> response = new ResponseEntity<>(
                new Response<>(new ArrayList<>()),
                HttpStatus.OK
        );
        doReturn(response).when(iamHelperService).getExchange(eq(IAMEndpoint.IMEI_ENTITY.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any());

        ResponseEntity<Response<List<Map<String, Object>>>> responseEntity = iamHelperService.getActiveEntityFromImei("1");
        Assert.assertNotNull(responseEntity.getBody());
        Assert.assertTrue(responseEntity.getBody().isSuccess());
    }

    @Test
    public void updateVideoStatusTest() {
        Map<String, Object> reviewAdherenceRequest = new HashMap<>();
        ResponseEntity<Response<List<Map<String, Object>>>> response = new ResponseEntity<>(
                new Response<>(new ArrayList<>()),
                HttpStatus.OK
        );
        doReturn(response).when(iamHelperService).putExchange(eq(IAMEndpoint.VOT_VIDEO_STATUS.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any());

        ResponseEntity<Response<Map<String, Object>>> responseEntity = iamHelperService.updateVideoStatus(reviewAdherenceRequest);
        Assert.assertNotNull(responseEntity.getBody());
        Assert.assertTrue(responseEntity.getBody().isSuccess());
    }

    @Test
    public void reallocateImei() {
        Map<String, Object> imeiEditRequest = new HashMap<>();
        ResponseEntity<Response<Long>>  response = new ResponseEntity<>(
                new Response<>(1L),
                HttpStatus.OK
        );
        doReturn(response).when(iamHelperService).putExchange(eq(IAMEndpoint.IMEI.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any());

        ResponseEntity<Response<Long>>  responseEntity = iamHelperService.reallocateIMEI(imeiEditRequest);
        Assert.assertNotNull(responseEntity.getBody());
        Assert.assertTrue(responseEntity.getBody().isSuccess());
    }

    @Test
    public void editPhone() {
        Map<String, Object> phoneEditDtoRequest = new HashMap<>();
        ResponseEntity<Response<String>>  response = new ResponseEntity<>(
                new Response<>("response", true),
                HttpStatus.OK
        );
        doReturn(response).when(iamHelperService).putExchange(eq(IAMEndpoint.EDIT_PHONE.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any());

        ResponseEntity<Response<String>> responseEntity = iamHelperService.editPhone(phoneEditDtoRequest);
        Assert.assertNotNull(responseEntity.getBody());
        Assert.assertTrue(responseEntity.getBody().isSuccess());
    }

    @Test
    public void closeCase() {
        ResponseEntity<Response<String>> responseEntity = new ResponseEntity<>(new Response<>(true, "Success"), HttpStatus.OK);
        doReturn(responseEntity).when(iamHelperService).deleteExchange(anyString(), any(), any(), any(), any());

        Long entityId = 1L;
        iamHelperService.closeCase(entityId, "2022-03-10 00:00:00", true, true);

        verify(iamHelperService, times(1)).deleteExchange(eq(IAMEndpoint.CLOSE_ENTITY.getEndpointUrl()), any(), any(), any(), any());
    }

    @Test
    public void reopenEntity() {
        ResponseEntity<Response<String>> responseEntity = new ResponseEntity<>(new Response<>(true, "Success"), HttpStatus.OK);
        doReturn(responseEntity).when(iamHelperService).putExchange(anyString(), any(), any());

        iamHelperService.reopenEntity(new EntityRequest());

        verify(iamHelperService, times(1)).putExchange(eq(IAMEndpoint.ENTITY_REOPEN.getEndpointUrl()), any(), any());
    }

    @Test
    public void registerEntityTest() {
        ResponseEntity<Response<Map<String, Object>>> response = new ResponseEntity<>(
                new Response<>(new HashMap<>()),
                HttpStatus.OK
        );
        doReturn(response).when(iamHelperService).postExchange(eq(IAMEndpoint.REGISTER_ENTITY.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any());

        ResponseEntity<Response<Map<String, Object>>> responseEntity = iamHelperService.registerEntity(new EntityRequest());
        Assert.assertNotNull(responseEntity.getBody());
        Assert.assertTrue(responseEntity.getBody().isSuccess());
        Mockito.verify(iamHelperService, Mockito.times(1)).postExchange(eq(IAMEndpoint.REGISTER_ENTITY.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any());
    }
}

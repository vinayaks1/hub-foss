package com.everwell.transition.unit.controller;

import com.everwell.transition.BaseTest;
import com.everwell.transition.constants.AppointmentConstants;
import com.everwell.transition.controller.AppointmentController;
import com.everwell.transition.exceptions.CustomExceptionHandler;
import com.everwell.transition.model.db.Appointment;
import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.dto.appointment.AppointmentData;
import com.everwell.transition.model.dto.appointment.AppointmentDetails;
import com.everwell.transition.model.dto.appointment.Document;
import com.everwell.transition.model.dto.appointment.QuestionAnswers;
import com.everwell.transition.model.request.appointment.*;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.response.appointment.GetAppointmentResponse;
import com.everwell.transition.service.AppointmentService;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.EpisodeService;
import com.everwell.transition.service.NotificationService;
import com.everwell.transition.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDateTime;
import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AppointmentControllerTest extends BaseTest {

    private MockMvc mockMvc;

    @Spy
    @InjectMocks
    private AppointmentController appointmentController;

    @Mock
    private AppointmentService appointmentService;

    @Mock
    private EpisodeService episodeService;

    @Mock
    private ClientService clientService;

    @Mock
    private NotificationService notificationService;

    @Mock
    ApplicationEventPublisher applicationEventPublisher;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(appointmentController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    private Appointment getSampleAppointment() {
        return new Appointment(1, null, 1L, null, null, null, 1, null, AppointmentConstants.DEFAULT_STATUS);
    }

    private EpisodeDto episodeDetailsBasic() {
        EpisodeDto episodeDto = new EpisodeDto(1L, 1L, 1L, true, "DSTB", 1L, null, Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, null, 1L, "PRESUMPTIVE_OPEN", "Presumptive Open", "IndiaTbPublic", 1L, null, null, null, null, null);
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("name", "Test Name");
        episodeDto.setStageData(stageData);
        return episodeDto;
    }

    @Test
    public void testAddAppointment() throws Exception {
        String uri = "/v1/appointment";
        AppointmentData appointment = new AppointmentData(
                1L,
                "STAFF",
                new AppointmentRequest(1, 1L, LocalDateTime.now()),
                new ArrayList<QuestionAnswers>(),
                new ArrayList<Document>()
        );
        EpisodeDto episodeDto = new EpisodeDto();
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("name", "Test Name");
        episodeDto.setStageData(stageData);
        ResponseEntity<Response<AppointmentData>> response
                = new ResponseEntity<>(
                new Response<>(appointment, true),
                HttpStatus.OK
        );

        doReturn(episodeDto).when(episodeService)
                .getEpisodeDetails(ArgumentMatchers.any(), ArgumentMatchers.any());
        when(appointmentService.addAppointment(appointment)).thenReturn(appointment);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(appointment))
                )
                .andExpect(status().isOk());
    }

    @Test
    public void testGetAppointment() throws Exception {
        String uri = "/v1/appointment";
        GetAppointmentResponse getAppointmentResponse = new GetAppointmentResponse();
        ResponseEntity<Response<GetAppointmentResponse>> response
                = new ResponseEntity<>(new Response<>(true, getAppointmentResponse), HttpStatus.OK);
        when(appointmentService.getAppointmentResponse(any())).thenReturn(getAppointmentResponse);
        mockMvc.
                perform(
                        MockMvcRequestBuilders.
                                get(uri)
                                .param("appointmentId", "1")
                )
                .andExpect(status().isOk());
    }

    @Test
    public void testGetAppointmentList() throws Exception {
        String uri = "/v1/appointment/list";
        List<AppointmentDetails> serviceResult = new ArrayList<>();
        AppointmentDetails appointment = new AppointmentDetails(
                1, 1L, "test", "surname", null, "Female", null, null, AppointmentConstants.DEFAULT_STATUS, "New", 1, LocalDateTime.now()
        );
        serviceResult.add(appointment);

        List<Integer> appointmentIds = new ArrayList<>();
        appointmentIds.add(1);
        AppointmentListRequest request = new AppointmentListRequest(appointmentIds, AppointmentConstants.DEFAULT_STATUS, null, null, null);

        when(appointmentService.getAppointments(any())).thenReturn(serviceResult);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(request))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk());
    }

    @Test
    public void testEndAppointment() throws Exception {
        String uri = "/v1/appointment/end";
        when(appointmentService.endAppointment(any())).thenReturn(AppointmentConstants.END_SUCCESS);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(uri)
                                .param("appointmentId", "1")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk());
    }

    @Test
    public void testConfirmAppointment() throws Exception {
        String uri = "/v1/appointment/confirm";
        Appointment savedAppointment = new Appointment(1, null, 1L, null, null, null, 1, null, AppointmentConstants.DEFAULT_STATUS);
        when(appointmentService.confirmAppointment(any())).thenReturn(savedAppointment);
        EpisodeDto episodeDto = new EpisodeDto(1L, 1L, 1L, true, "DSTB", 1L, null, Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, null, 1L,"PRESUMPTIVE_OPEN", "Presumptive Open", "IndiaTbPublic", 1L, null, null, null, null, new HashMap<>());
        when(episodeService.getEpisodeDetails(1L, 1L)).thenReturn(episodeDto);
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client());
        doNothing().when(applicationEventPublisher).publishEvent(any());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(uri)
                                .param("appointmentId", "1")
                                .param("staffName", "doctor")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk());
    }


    @Test
    public void testCreateFollowupAppointment() throws Exception {
        String uri = "/v1/appointment/follow-up";
        FollowUpRequest request = new FollowUpRequest(1, LocalDateTime.now());

        when(appointmentService.createFollowUpAppointment(any())).thenReturn(getSampleAppointment());
        when(episodeService.getEpisodeDetails(any(), any())).thenReturn(episodeDetailsBasic());
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(1L, "client", "client", new Date()));

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(request))
                )
                .andExpect(status().isOk());
    }

    @Test
    public void testRescheduleAppointment() throws Exception {
        String uri = "/v1/appointment/re-schedule";
        AppointmentDetails serviceResult = new AppointmentDetails(
                1, 1L, "test", "surname", null, "Female", null, null, AppointmentConstants.DEFAULT_STATUS, "New", 1, LocalDateTime.now()
        );
        RescheduleRequest request = new RescheduleRequest(1, LocalDateTime.now());

        when(appointmentService.rescheduleAppointment(any())).thenReturn(getSampleAppointment());
        when(episodeService.getEpisodeDetails(any(), any())).thenReturn(episodeDetailsBasic());
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(1L, "client", "client", new Date()));

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(request))
                )
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteAppointment() throws Exception {
        String uri = "/v1/appointment";
        DeleteAppointmentRequest request = new DeleteAppointmentRequest(1L, 1);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .delete(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(request))
                )
                .andExpect(status().isOk());
    }
}

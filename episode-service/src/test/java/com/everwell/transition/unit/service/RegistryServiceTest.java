package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.response.HierarchyData;
import com.everwell.transition.model.response.HierarchyResponseData;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.impl.RegistryServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.List;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

public class RegistryServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    RegistryServiceImpl registryService;

    @Mock
    ClientService clientService;

    @Before
    public void init() {
        ReflectionTestUtils.setField(registryService, "dataGatewayURL", "http://test:8080");
        ReflectionTestUtils.setField(registryService, "username", "test");
        ReflectionTestUtils.setField(registryService, "password", "test");
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(29L,"test", "test", null ));
        registryService.init();
    }

    @Test
    public void getHierarchyById() {
        HierarchyResponseData hierarchyResponseData = new HierarchyResponseData();
        hierarchyResponseData.setHierarchy(new HierarchyData());
        hierarchyResponseData.getHierarchy().setId(100L);
        ResponseEntity<Response<HierarchyResponseData>> response = new ResponseEntity<>(new Response<>(hierarchyResponseData, true), HttpStatus.OK);
        doReturn(response).when(registryService).getExchange(ArgumentMatchers.anyString(), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any());
        registryService.getHierarchyById(100L, false);
        Assert.assertEquals(100L, (long) response.getBody().getData().getHierarchy().getId());
    }

    @Test
    public void getAllLevelIdsForHierarchyTest() {
        HierarchyResponseData hierarchyResponseData = new HierarchyResponseData();
        hierarchyResponseData.setHierarchy(new HierarchyData());
        hierarchyResponseData.getHierarchy().setId(100L);
        hierarchyResponseData.getHierarchy().setLevel1Id(1L);
        hierarchyResponseData.getHierarchy().setLevel2Id(1000L);
        ResponseEntity<Response<HierarchyResponseData>> response = new ResponseEntity<>(new Response<>(hierarchyResponseData, true), HttpStatus.OK);
        doReturn(response).when(registryService).getHierarchyById(eq(10L), eq(false));
        List<Long> ids = registryService.getAllLevelIdsForHierarchy(10L, false);
        Assert.assertTrue(ids.contains(100L));
        Assert.assertTrue(ids.contains(1L));
        Assert.assertTrue(ids.contains(1000L));
    }

}

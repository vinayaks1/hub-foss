package com.everwell.transition.unit.controller;

import com.everwell.transition.BaseTest;
import com.everwell.transition.controller.AERSController;
import com.everwell.transition.exceptions.CustomExceptionHandler;
import com.everwell.transition.model.db.AdverseReaction;
import com.everwell.transition.model.db.CausalityManagement;
import com.everwell.transition.model.db.AersOutcome;
import com.everwell.transition.model.dto.FormConfigDto;
import com.everwell.transition.model.dto.KeyValuePair;
import com.everwell.transition.model.request.aers.AdverseReactionRequest;
import com.everwell.transition.model.request.aers.CausalityManagementRequest;
import com.everwell.transition.model.request.aers.OutcomeRequest;
import com.everwell.transition.model.response.AdverseReactionResponse;
import com.everwell.transition.model.response.CausalityManagementResponse;
import com.everwell.transition.model.response.FormConfigResponse;
import com.everwell.transition.model.response.OutcomeResponse;
import com.everwell.transition.service.AERSService;
import com.everwell.transition.utils.Utils;
import org.checkerframework.checker.units.qual.K;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.Assert.assertEquals;


import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AERSControllerTest extends BaseTest {

    private MockMvc mockMvc;

    @Spy
    @InjectMocks
    private AERSController aersController;

    @Mock
    private AERSService aersService;

    private static long adverseEventId = 1L;
    private static long episodeId = 123L;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(aersController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void testAdverseReactionAdd() throws Exception {
        String uri = "/v1/aers/adverse-reaction";
        List<KeyValuePair> predominantCondition = new ArrayList<>();
        predominantCondition.add(new KeyValuePair());
        AdverseReactionRequest adverseReactionRequest = new AdverseReactionRequest(1L,1L, "Initial","25-10-2021 18:30:00",predominantCondition,"severity");

        doNothing().when(aersService).addAdverseReaction(any());

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(adverseReactionRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isAccepted())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value("Successfully saved Adverse Reaction"));
        verify(aersService,Mockito.times(1)).addAdverseReaction(any());
    }

    @Test
    public void testAdverseReactionUpdate() throws Exception {
        String uri = "/v1/aers/adverse-reaction";
        List<KeyValuePair> predominantCondition = new ArrayList<>();
        predominantCondition.add(new KeyValuePair());
        AdverseReactionRequest adverseReactionRequest = new AdverseReactionRequest(1L,1L, "Initial","25-10-2021 18:30:00",predominantCondition,"severity");

        doNothing().when(aersService).updateAdverseReaction(any());

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(adverseReactionRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isAccepted())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value("Successfully saved Adverse Reaction"));
        verify(aersService,Mockito.times(1)).updateAdverseReaction(any());
    }

    @Test
    public void testAddCausalityManagement() throws Exception {
        String uri = "/v1/aers/causality-management";
        List<KeyValuePair> actionTaken = new ArrayList<>();
        actionTaken.add(new KeyValuePair());
        CausalityManagementRequest causalityManagementRequest = new CausalityManagementRequest(1L,1L);
        causalityManagementRequest.setActionTaken(actionTaken);

        doNothing().when(aersService).addCausalityManagement(causalityManagementRequest);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(causalityManagementRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isAccepted())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value("Successfully saved Causality and Management Details"));
        verify(aersService,Mockito.times(1)).addCausalityManagement(any());
    }

    @Test
    public void testUpdateCausalityManagement() throws Exception {
        String uri = "/v1/aers/causality-management";
        List<KeyValuePair> actionTaken = new ArrayList<>();
        actionTaken.add(new KeyValuePair());
        CausalityManagementRequest causalityManagementRequest = new CausalityManagementRequest(1L,1L);
        causalityManagementRequest.setActionTaken(actionTaken);

        doNothing().when(aersService).updateCausalityManagement(causalityManagementRequest);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(causalityManagementRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isAccepted())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value("Successfully saved Causality and Management Details"));
        verify(aersService,Mockito.times(1)).updateCausalityManagement(any());
    }

    @Test
    public void testAddOutcome() throws Exception {
        String uri = "/v1/aers/outcome";
        OutcomeRequest outcomeRequest = new OutcomeRequest(1L,1L);
        outcomeRequest.setOutcome("Fatal");
        doNothing().when(aersService).addOutcome(outcomeRequest);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(outcomeRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isAccepted())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value("Successfully saved Outcome Details"));
        verify(aersService,Mockito.times(1)).addOutcome(any());
    }

    @Test
    public void testUpdateOutcome() throws Exception {
        String uri = "/v1/aers/outcome";
        OutcomeRequest outcomeRequest = new OutcomeRequest(1L,1L);
        outcomeRequest.setOutcome("Resolved");
        doNothing().when(aersService).updateOutcome(outcomeRequest);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(outcomeRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isAccepted())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value("Successfully saved Outcome Details"));
        verify(aersService,Mockito.times(1)).updateOutcome(any());
    }

    @Test
    public void testGetAdverseReaction() throws Exception {
        String uri = "/v1/aers/adverse-reaction?adverseEventId=" + adverseEventId;
        AdverseReaction adverseReaction = new AdverseReaction("10","18-10-2021 18:30:00");
        AdverseReactionResponse response = new AdverseReactionResponse(adverseReaction,1L,1L);
        when(aersService.getAdverseReaction(1L)).thenReturn(response);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isAccepted())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.episodeId").value(1L))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.currentWeight").value("10"));
        verify(aersService,Mockito.times(1)).getAdverseReaction(any());

    }

    @Test
    public void testGetCausalityManagement() throws Exception {
        String uri = "/v1/aers/causality-management?adverseEventId=" + adverseEventId;
        CausalityManagement causalityManagement = new CausalityManagement("test1","test2");
        CausalityManagementResponse response = new CausalityManagementResponse(causalityManagement,1L,1L,null);
        when(aersService.getCausalityManagement(1L)).thenReturn(response);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isAccepted())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.episodeId").value(1L))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.dechallenge").value("test2"));
        verify(aersService,Mockito.times(1)).getCausalityManagement(any());

    }

    @Test
    public void testGetOutcome() throws Exception {
        String uri = "/v1/aers/outcome?adverseEventId=" + adverseEventId;
        AersOutcome outcome = new AersOutcome("test1","test2");
        OutcomeResponse response = new OutcomeResponse(outcome,1L,1L);
        when(aersService.getOutcome(1L)).thenReturn(response);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isAccepted())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.episodeId").value(1L))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.outcome").value("test1"));
        verify(aersService,Mockito.times(1)).getOutcome(any());

    }

    @Test
    public void testGetAdverseEvents() throws Exception {
        String uri = "/v1/aers/status?episodeId="+episodeId;
        List<FormConfigDto> formConfigDtos = new ArrayList<>();
        formConfigDtos.add(new FormConfigDto("test",1,1L));
        List<FormConfigResponse> aersStatusResponse = new ArrayList<>();
        aersStatusResponse.add(new FormConfigResponse(1L,formConfigDtos,"2021-10-21","2021-11-21"));

        when(aersService.getStatus(any())).thenReturn(aersStatusResponse);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isAccepted())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.aersStatusList[0].adverseEventId").value(1L));
        verify(aersService,Mockito.times(1)).getStatus(any());
    }

    @Test
    public void testAdverseReactionException() throws Exception {
        String uri = "/v1/aers/adverse-reaction";
        AdverseReactionRequest adverseReactionRequest = new AdverseReactionRequest(null,null);

        doNothing().when(aersService).addAdverseReaction(any());

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(adverseReactionRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCausalityExceptionForEpisodeId() throws Exception {
        String uri = "/v1/aers/causality-management";
        CausalityManagementRequest causalityManagementRequest = new CausalityManagementRequest(1L,null);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(causalityManagementRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testOutcomeExceptionForEpisodeId() throws Exception {
        String uri = "/v1/aers/outcome";
        OutcomeRequest outcomeRequest = new OutcomeRequest(1L,null);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(outcomeRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCausalityExceptionForAdverseEventId() throws Exception {
        String uri = "/v1/aers/causality-management";
        CausalityManagementRequest causalityManagementRequest = new CausalityManagementRequest(null,1L);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(causalityManagementRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testOutcomeExceptionForAdverseEventId() throws Exception {
        String uri = "/v1/aers/outcome";
        OutcomeRequest outcomeRequest = new OutcomeRequest(null,1L);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(outcomeRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testAdverseReactionExceptionMissingReportType() throws Exception {
        String uri = "/v1/aers/adverse-reaction";
        AdverseReactionRequest adverseReactionRequest = new AdverseReactionRequest(1L,1L);

        doNothing().when(aersService).addAdverseReaction(any());

        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .post(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(Utils.asJsonString(adverseReactionRequest))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isBadRequest());
    }

    @Test
    public void testAdverseReactionExceptionMissingOnsetDate() throws Exception {
        String uri = "/v1/aers/adverse-reaction";
        AdverseReactionRequest adverseReactionRequest = new AdverseReactionRequest(1L,1L, "report", null, null, "test");

        doNothing().when(aersService).addAdverseReaction(any());

        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .post(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(Utils.asJsonString(adverseReactionRequest))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isBadRequest());
    }

    @Test
    public void testAdverseReactionExceptionMissingSeverity() throws Exception {
        String uri = "/v1/aers/adverse-reaction";
        AdverseReactionRequest adverseReactionRequest = new AdverseReactionRequest(1L,1L, "report", "test", null, null);

        doNothing().when(aersService).addAdverseReaction(any());

        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .post(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(Utils.asJsonString(adverseReactionRequest))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isBadRequest());
    }

    @Test
    public void testAdverseReactionExceptionMissingPredominantCondition() throws Exception {
        String uri = "/v1/aers/adverse-reaction";
        AdverseReactionRequest adverseReactionRequest = new AdverseReactionRequest(1L,1L, "report", "test", null, "severity");

        doNothing().when(aersService).addAdverseReaction(any());

        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .post(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(Utils.asJsonString(adverseReactionRequest))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isBadRequest());
    }

    @Test
    public void testOutcomeExceptionForOutcome() throws Exception {
        String uri = "/v1/aers/outcome";
        OutcomeRequest outcomeRequest = new OutcomeRequest(1L,1L);

        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .post(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(Utils.asJsonString(outcomeRequest))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isBadRequest());
    }

}

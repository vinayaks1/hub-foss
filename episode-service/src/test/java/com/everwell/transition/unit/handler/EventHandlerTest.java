package com.everwell.transition.unit.handler;

import com.everwell.transition.BaseTest;
import com.everwell.transition.binders.EpisodeEventsBinder;
import com.everwell.transition.enums.Event;
import com.everwell.transition.handlers.EventHandler;
import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.dto.EventDto;
import com.everwell.transition.service.ClientService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.messaging.MessageChannel;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class EventHandlerTest extends BaseTest {

    @Spy
    @InjectMocks
    EventHandler eventHandler;

    @Mock
    MessageChannel eventOutput;

    @Mock
    EpisodeEventsBinder episodeEventsBinder;

    @Mock
    ClientService clientService;

    @Before
    public void init() {
        Mockito.when(episodeEventsBinder.userServiceEventOutput()).thenReturn(eventOutput);
    }

    @Test
    public <T> void emitEventTest() {
        Map<String, Object> updateUserMap = new HashMap<>();
        updateUserMap.put("userId", 1L);
        updateUserMap.put("firstName", "TestName");
        EventDto<T> eventDto = new EventDto(Event.UPDATE_USER, updateUserMap);
        Client testClient = new Client(1L,"test","test",new Date(),1L,"test");
        when(clientService.getClientByTokenOrDefault()).thenReturn(testClient);
        eventHandler.emitEvent(eventDto);
        Mockito.verify(eventOutput, Mockito.times(1)).send(any());
    }

    @Test
    public <T> void emitEventTWithNullClientTest() {
        Map<String, Object> updateUserMap = new HashMap<>();
        updateUserMap.put("userId", 1L);
        updateUserMap.put("firstName", "TestName");
        EventDto<T> eventDto = new EventDto(Event.UPDATE_USER, updateUserMap);
        when(clientService.getClientByTokenOrDefault()).thenReturn(null);
        eventHandler.emitEvent(eventDto);
        Mockito.verify(eventOutput, Mockito.times(0)).send(any());
    }

    @Test
    public <T> void emitEventTWithNullClientIdTest() {
        Map<String, Object> updateUserMap = new HashMap<>();
        updateUserMap.put("userId", 1L);
        updateUserMap.put("firstName", "TestName");
        EventDto<T> eventDto = new EventDto(Event.UPDATE_USER, updateUserMap);
        Client testClient = new Client(null,"test","test",new Date(),1L, "test");
        when(clientService.getClientByTokenOrDefault()).thenReturn(testClient);
        eventHandler.emitEvent(eventDto);
        Mockito.verify(eventOutput, Mockito.times(0)).send(any());
    }

}

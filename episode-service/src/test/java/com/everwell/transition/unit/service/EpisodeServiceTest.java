package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.constants.Constants;
import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.constants.TaskListFields;
import com.everwell.transition.constants.*;
import com.everwell.transition.constants.TaskListFields;
import com.everwell.transition.elasticsearch.indices.EpisodeIndex;
import com.everwell.transition.elasticsearch.service.ESEpisodeService;
import com.everwell.transition.constants.NotificationConstants;
import com.everwell.transition.elasticsearch.indices.EpisodeIndex;
import com.everwell.transition.elasticsearch.service.ESEpisodeService;
import com.everwell.transition.elasticsearch.service.Impl.ReportFiltersToEpisodeSearchFields;
import com.everwell.transition.elasticsearch.service.Impl.TaskListFiltersToEpisodeSearchFields;
import com.everwell.transition.elasticsearch.service.Impl.UppFiltersToEpisodeSearchFields;
import com.everwell.transition.enums.AdherenceCodeEnum;
import com.everwell.transition.enums.ScheduleTimeEnum;
import com.everwell.transition.enums.SupportedRelationType;
import com.everwell.transition.enums.user.UserServiceField;
import com.everwell.transition.enums.episode.EpisodeField;
import com.everwell.transition.exceptions.IllegalArgumentException;
import com.everwell.transition.exceptions.InternalServerErrorException;
import com.everwell.transition.exceptions.NotFoundException;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.EpisodeFieldIdValue;
import com.everwell.transition.model.db.*;
import com.everwell.transition.model.dto.*;
import com.everwell.transition.model.dto.notification.*;
import com.everwell.transition.model.request.episodesearch.EpisodeSearchRequest;
import com.everwell.transition.model.response.EpisodeTagResponse;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.dto.episode.EpisodeDetailsDto;
import com.everwell.transition.model.dto.dtoInterface.EpisodeFieldIdValueDtoInterface;
import com.everwell.transition.model.dto.treatmentPlan.TreatmentPlanDto;
import com.everwell.transition.model.dto.treatmentPlan.TreatmentPlanProductMapDto;
import com.everwell.transition.model.request.AddEpisodeDocumentRequest;
import com.everwell.transition.model.request.EpisodeDocumentSearchRequest;
import com.everwell.transition.model.request.episode.DeleteRequest;
import com.everwell.transition.model.request.episodesearch.EpisodeSearchRequest;
import com.everwell.transition.model.response.AdherenceResponse;
import com.everwell.transition.model.response.EpisodeTagResponse;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.dto.notification.PNDtoWithNotificationData;
import com.everwell.transition.model.request.tasklistfilters.EpisodeTasklistRequest;
import com.everwell.transition.model.request.tasklistfilters.TaskListFilterRequest;
import com.everwell.transition.model.response.*;
import com.everwell.transition.model.response.DiseaseTemplateResponse;
import com.everwell.transition.model.request.episode.EpisodeBulkRequest;
import com.everwell.transition.model.request.episode.SearchEpisodesRequest;
import com.everwell.transition.model.request.episode.UpdateRiskRequest;
import com.everwell.transition.model.response.SSO.DeviceIdsMapResponse;
import com.everwell.transition.model.response.dispensation.ProductResponse;
import com.everwell.transition.repositories.*;
import com.everwell.transition.service.DiseaseTemplateService;
import com.everwell.transition.service.EpisodeTagService;
import com.everwell.transition.service.RegistryService;
import com.everwell.transition.service.StageTransitionService;
import com.everwell.transition.service.UserService;
import com.everwell.transition.service.*;
import com.everwell.transition.service.impl.EpisodeServiceImpl;
import com.everwell.transition.utils.Utils;
import org.elasticsearch.client.RestHighLevelClient;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.http.MockHttpOutputMessage;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

public class EpisodeServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    private EpisodeServiceImpl episodeService;

    @Mock
    private EpisodeRepository episodeRepository;

    @Mock
    private EpisodeStageRepository episodeStageRepository;

    @Mock
    private EpisodeStageDataRepository episodeStageDataRepository;

    @Mock
    private EpisodeAssociationRepository episodeAssociationRepository;

    @Mock
    private EpisodeHierarchyLinkageRepository episodeHierarchyLinkageRepository;

    @Mock
    private TreatmentPlanProductMapRepository treatmentPlanProductMapRepository;

    @Mock
    private SupportedRelationRepository supportedRelationRepository;

    @Mock
    private StageRepository stageRepository;

    @Mock
    private DiseaseTemplateService diseaseTemplateService;

    @Mock
    private StageTransitionService stageTransitionService;

    @Mock
    private ApplicationEventPublisher applicationEventPublisher;

    @Mock
    private EpisodeTagService episodeTagService;

    @Mock
    private EpisodeLogService episodeLogService;

    @Mock
    private ESEpisodeService esEpisodeService;

    @Mock
    RegistryService registryService;

    @Mock
    private ClientService clientService;

    @Mock
    private UserService userService;

    @Mock
    RestHighLevelClient restHighLevelClient;

    @Mock
    private DispensationService dispensationService;

    @Mock
    private IAMService iamService;

    @Mock
    private SSOService ssoService;

    @Mock
    private TriggerRepository triggerRepository;

    @Mock
    private TaskListFiltersToEpisodeSearchFields taskListFiltersToEpisodeSearchFields;

    @Mock
    private UppFiltersToEpisodeSearchFields uppFiltersToEpisodeSearchFields;

    @Mock
    private EpisodeDocumentRepository episodeDocumentRepository;

    @Mock
    private NikshayService nikshayService;

    @Mock
    private DiagnosticsService diagnosticsService;

    @Mock
    private TreatmentPlanService treatmentPlanService;
    
    @Mock
    private ReportFiltersToEpisodeSearchFields reportFiltersToEpisodeSearchFields;

    @Mock
    private EpisodeDocumentDetailsRepository episodeDocumentDetailsRepository;

    @Mock
    private AdherenceService adherenceService;

    private EpisodeIndex getDefaultEpisodeIndex() {
        EpisodeIndex episodeIndex = new EpisodeIndex();
        episodeIndex.setId("1");
        episodeIndex.setPersonId(2L);
        return episodeIndex;
    }

    @Test(expected = NotFoundException.class)
    public void test_getEpisodeDetails_throws_notFoundException() {
        Long episodeId = 1L;
        Long clientId =1L;
        when(episodeRepository.findByIdAndClientIdAndDeletedIs(any(), any(), any())).thenReturn(null);
        episodeService.getEpisodeDetails(episodeId, clientId);
    }

    @Test
    public void test_getEpisodeDetails_success_testCase1() {
        Long episodeId = 1L;
        Long clientId = 1L;
        when(episodeRepository.findByIdAndClientIdAndDeletedIs(any(), any(), any())).thenReturn(getEpisode_testCase1());
        when(episodeStageRepository.getEpisodeStagesByEpisodeIdInOrderByStartDateAsc(any())).thenReturn(getEpisodeStageList_testCase1());
        when(stageRepository.findAllById(any())).thenReturn(getStageList_testCase1());
        when(episodeStageDataRepository.findAllByEpisodeStageIdIn(any())).thenReturn(getEpisodeStageDataList_testCase1());
        when(diseaseTemplateService.getDefinedStageIdKeyMappingsListMap(any(), any())).thenReturn(getDefinedStageIdKeyMappingsListMap_testCase1());
        when(supportedRelationRepository.findAll()).thenReturn(getAllSupportedRelation());
        when(episodeAssociationRepository.getAllByEpisodeIdIn(any())).thenReturn(getEpisodeAssociations_testCase1());
        when(episodeHierarchyLinkageRepository.getAllByEpisodeIdIn(any())).thenReturn(getEpisodeHierarchyLinkages_testCase1());
        when(diseaseTemplateService.getStageToTabPermissionsListMapForStageIds(any(), any(), any())).thenReturn(getStageToTabPermissions_testCase1());
        when(diseaseTemplateService.getDiseaseIdTemplateMap(Collections.singletonList(any()))).thenReturn(getDiseaseIdTemplateMap_testCase1());
        when(stageRepository.findById(any())).thenReturn(Optional.of(getStage0()));
        when(episodeTagService.getEpisodeTags(eq(episodeId))).thenReturn(new EpisodeTagResponse(episodeId, new ArrayList<>(), Arrays.asList("Hakuna", "Matata")));

        EpisodeDto episode = episodeService.getEpisodeDetails(episodeId, clientId);
        List<String> associationKeys = Arrays.asList("SR1", "SR2", "SR3");
        List<String> associationValues = Arrays.asList("AssociationValue1", "AssociationValue2", "AssociationValue3");
        for(String key : associationKeys) {
            assertTrue(episode.getAssociations().containsKey(key));
        }
        for(String value : associationValues) {
            assertTrue(episode.getAssociations().containsValue(value));
        }

        List<String> hierarchyMappingKeys = Arrays.asList("SR4", "SR5", "SR6");
        List<Long> hierarchyMappings = Arrays.asList(21L,22L,23L);
        for(String key : hierarchyMappingKeys) {
            assertTrue(episode.getHierarchyMappings().containsKey(key));
        }
        for(Long value : hierarchyMappings) {
            assertTrue(episode.getHierarchyMappings().containsValue(value));
        }

        HashSet<String> tabNames = new HashSet<>(Arrays.asList("Tab1", "Tab2", "Tab3", "Tab4", "Tab5"));
        Map<String, TabPermissionDto> tabNameToEntityMap = getTabPermissions_6Tabs()
                .stream().collect(Collectors.toMap(TabPermissionDto::getTabName, a -> a));
        for(TabPermissionDto tabPermissionDto : episode.getTabPermissions()) {
            assertTrue(tabNames.contains(tabPermissionDto.getTabName()));
            Assert.assertEquals(tabPermissionDto.isAdd(), tabNameToEntityMap.get(tabPermissionDto.getTabName()).isAdd());
            Assert.assertEquals(tabPermissionDto.isDelete(), tabNameToEntityMap.get(tabPermissionDto.getTabName()).isDelete());
            Assert.assertEquals(tabPermissionDto.isEdit(), tabNameToEntityMap.get(tabPermissionDto.getTabName()).isEdit());
            Assert.assertEquals(tabPermissionDto.isView(), tabNameToEntityMap.get(tabPermissionDto.getTabName()).isView());
        }

        Map<String, String> expectedFieldKeyValueMap = new HashMap<>();
        expectedFieldKeyValueMap.put("Field1", "Field1Value");
        expectedFieldKeyValueMap.put("Field2", "Field2Value");
        expectedFieldKeyValueMap.put("Field3", "Field3Value");
        expectedFieldKeyValueMap.put("Field4", "Field4Value");
        for(Map.Entry<String, String> entry : expectedFieldKeyValueMap.entrySet()) {
            assertEquals(episode.getStageData().get(entry.getKey()), entry.getValue());
        }
        assertEquals(episode.getCurrentTags().get(0), "Hakuna");
        assertEquals(episode.getCurrentTags().get(1), "Matata");
    }

    @Test
    public void test_getEpisodeDetails_success_EmptyTabPermissions() {
        Long episodeId = 1L;
        Long clientId = 1L;
        when(episodeRepository.findByIdAndClientIdAndDeletedIs(any(), any(), any())).thenReturn(getEpisode_testCase1());
        when(episodeStageRepository.getEpisodeStagesByEpisodeIdInOrderByStartDateAsc(any())).thenReturn(getEpisodeStageList_testCase1());
        when(stageRepository.findAllById(any())).thenReturn(getStageList_testCase1());
        when(episodeStageDataRepository.findAllByEpisodeStageIdIn(any())).thenReturn(getEpisodeStageDataList_testCase1());
        when(diseaseTemplateService.getDefinedStageIdKeyMappingsListMap(any(), any())).thenReturn(getDefinedStageIdKeyMappingsListMap_testCase1());
        when(supportedRelationRepository.findAll()).thenReturn(getAllSupportedRelation());
        when(episodeAssociationRepository.getAllByEpisodeIdIn(any())).thenReturn(getEpisodeAssociations_testCase1());
        when(episodeHierarchyLinkageRepository.getAllByEpisodeIdIn(any())).thenReturn(getEpisodeHierarchyLinkages_testCase1());
        when(diseaseTemplateService.getStageToTabPermissionsListMapForStageIds(anySet(), anyList(), any())).thenReturn(new HashMap<>());
        when(diseaseTemplateService.getDiseaseIdTemplateMap(Collections.singletonList(any()))).thenReturn(getDiseaseIdTemplateMap_testCase1());
        when(stageRepository.findById(any())).thenReturn(Optional.of(getStage0()));
        when(episodeTagService.getEpisodeTags(eq(episodeId))).thenReturn(new EpisodeTagResponse(episodeId, new ArrayList<>(), Arrays.asList("Hakuna", "Matata")));

        EpisodeDto episode = episodeService.getEpisodeDetails(episodeId, clientId);

        Assert.assertNotNull(episode.getTabPermissions());
        Assert.assertEquals(0, episode.getTabPermissions().size());
    }

    @Test
    public void test_getEpisodeDetails_success_multipleStages() {
        Long episodeId = 1L;
        Long clientId = 1L;
        when(episodeRepository.findByIdAndClientIdAndDeletedIs(any(), any(), any())).thenReturn(getEpisode_testCase2());
        when(episodeStageRepository.getEpisodeStagesByEpisodeIdInOrderByStartDateAsc(any())).thenReturn(getEpisodeStage_testCase2());
        when(stageRepository.findAllById(any())).thenReturn(getStageList_testCase2());
        when(episodeStageDataRepository.findAllByEpisodeStageIdIn(any())).thenReturn(getEpisodeStageDataList_testCase2());
        when(diseaseTemplateService.getDefinedStageIdKeyMappingsListMap(any(), any())).thenReturn(getDefinedStageIdKeyMappingsListMap_testCase2());
        when(supportedRelationRepository.findAll()).thenReturn(getAllSupportedRelation());
        when(episodeAssociationRepository.getAllByEpisodeIdIn(any())).thenReturn(getEpisodeAssociations_testCase1());
        when(episodeHierarchyLinkageRepository.getAllByEpisodeIdIn(any())).thenReturn(getEpisodeHierarchyLinkages_testCase1());
        when(diseaseTemplateService.getStageToTabPermissionsListMapForStageIds(any(), any(), any())).thenReturn(getStageToTabPermissions_testCase2());
        when(diseaseTemplateService.getDiseaseIdTemplateMap(Collections.singletonList(any()))).thenReturn(getDiseaseIdTemplateMap_testCase1());
        when(stageRepository.findById(any())).thenReturn(Optional.of(getStage0()));
        when(episodeTagService.getEpisodeTags(eq(episodeId))).thenReturn(new EpisodeTagResponse(episodeId, new ArrayList<>(), Arrays.asList("hakuna", "matata")));

        EpisodeDto episode = episodeService.getEpisodeDetails(episodeId, clientId);
        List<String> associationKeys = Arrays.asList("SR1", "SR2", "SR3");
        List<String> associationValues = Arrays.asList("AssociationValue1", "AssociationValue2", "AssociationValue3");
        for(String key : associationKeys) {
            assertTrue(episode.getAssociations().containsKey(key));
        }
        for(String value : associationValues) {
            assertTrue(episode.getAssociations().containsValue(value));
        }

        List<String> hierarchyMappingKeys = Arrays.asList("SR4", "SR5", "SR6");
        List<Long> hierarchyMappings = Arrays.asList(21L,22L,23L);
        for(String key : hierarchyMappingKeys) {
            assertTrue(episode.getHierarchyMappings().containsKey(key));
        }
        for(Long value : hierarchyMappings) {
            assertTrue(episode.getHierarchyMappings().containsValue(value));
        }

        HashSet<String> tabNames = new HashSet<>(Arrays.asList("Tab1", "Tab2", "Tab3", "Tab4", "Tab5"));
        Map<String, TabPermissionDto> tabNameToEntityMap = getStageToTabPermissions_testCase2().get(episode.getCurrentStageId())
                .stream().collect(Collectors.toMap(TabPermissionDto::getTabName, a -> a));
        for(TabPermissionDto tabPermissionDto : episode.getTabPermissions()) {
            assertTrue(tabNames.contains(tabPermissionDto.getTabName()));
            Assert.assertEquals(tabPermissionDto.isAdd(), tabNameToEntityMap.get(tabPermissionDto.getTabName()).isAdd());
            Assert.assertEquals(tabPermissionDto.isDelete(), tabNameToEntityMap.get(tabPermissionDto.getTabName()).isDelete());
            Assert.assertEquals(tabPermissionDto.isEdit(), tabNameToEntityMap.get(tabPermissionDto.getTabName()).isEdit());
            Assert.assertEquals(tabPermissionDto.isView(), tabNameToEntityMap.get(tabPermissionDto.getTabName()).isView());
        }

        Map<String, String> expectedFieldKeyValueMap = new HashMap<>();
        expectedFieldKeyValueMap.put("Field1", "Field1Value2");
        expectedFieldKeyValueMap.put("Field2", "Field2Value");
        expectedFieldKeyValueMap.put("Field3", "Field3Value2");
        expectedFieldKeyValueMap.put("Field4", "Field4Value2");
        for(Map.Entry<String, String> entry : expectedFieldKeyValueMap.entrySet()) {
            assertEquals(entry.getValue(), episode.getStageData().get(entry.getKey()));
        }
    }

    @Test(expected = ValidationException.class)
    public void test_processCreateEpisode_throws_validationExceptionForAddedBy(){
        Map<String, Object> input = new HashMap<>();
        input.put(FieldConstants.EPISODE_FIELD_PERSON_ID, 1L);
        Long clientId = 1L;
        episodeService.processCreateEpisode(clientId,input);
    }
    
    @Test(expected =  ValidationException.class)
    public void test_processCreateEpisode_throws_validationExceptionForNoTypeOfEpisode() {
        Map<String, Object> requestInput = new HashMap<>();
        Long clientId = 1L;
        requestInput.put(FieldConstants.EPISODE_FIELD_EPISODE_ID, 1L);
        requestInput.put(FieldConstants.EPISODE_FIELD_ADDED_BY, 1L);
        requestInput.put(FieldConstants.EPISODE_FIELD_DISEASE_ID, 1L);
        when(diseaseTemplateService.getDiseasesForClient(clientId)).thenReturn(getDiseases());
        episodeService.processCreateEpisode(clientId, requestInput);
    }

    @Test(expected = ValidationException.class)
    public void test_processCreateEpisode_throws_validationExceptionForStage(){
        Long clientId = 1L;
        Map<String, Object> input = new HashMap<>();
        Map<String, Object> stageTransitionFieldValueMap = new HashMap<>();
        stageTransitionFieldValueMap.put("ToStage", "Stage0");
        when(stageTransitionService.stageTransitionHandler(input, clientId)).thenReturn(new EventStreamingDto<>("eventName", stageTransitionFieldValueMap));
        when(stageRepository.findByStageName(any())).thenReturn(null);
        episodeService.processCreateEpisode(clientId,input);
    }

    @Test(expected = ValidationException.class)
    public void test_processCreateEpisode_throws_validationExceptionForDiseaseId(){
        Map<String, Object> input = new HashMap<>();
        input.put(FieldConstants.EPISODE_FIELD_ADDED_BY, 1L);
        input.put(FieldConstants.EPISODE_FIELD_DISEASE_ID, ",,");
        Long clientId = 1L;
        episodeService.processCreateEpisode(clientId,input);
    }


    @Test(expected = ValidationException.class)
    public void test_processCreateEpisode_throws_validationExceptionForEmptyDiseaseOptions(){
        Map<String, Object> input = new HashMap<>();
        input.put(FieldConstants.EPISODE_FIELD_ADDED_BY, 1L);
        input.put(FieldConstants.EPISODE_FIELD_DISEASE_ID, null);
        input.put(FieldConstants.EPISODE_FIELD_DISEASE_ID_OPTIONS, "");
        Long clientId = 1L;
        episodeService.processCreateEpisode(clientId,input);
    }

    @Test(expected = ValidationException.class)
    public void test_processCreateEpisode_throws_validationExceptionForWrongDiseaseOptions(){
        Map<String, Object> input = new HashMap<>();
        input.put(FieldConstants.EPISODE_FIELD_ADDED_BY, 1L);
        input.put(FieldConstants.EPISODE_FIELD_DISEASE_ID, null);
        input.put(FieldConstants.EPISODE_FIELD_DISEASE_ID_OPTIONS, ",,,");
        Long clientId = 12L;
        when(diseaseTemplateService.getDiseasesForClient(clientId)).thenReturn(getDiseases());
        episodeService.processCreateEpisode(clientId,input);
    }

    @Test(expected = ValidationException.class)
    public void test_processCreateEpisode_throws_validationExceptionForPersonId(){
        Map<String, Object> input = new HashMap<>();
        Long clientId = 1L;
        episodeService.processCreateEpisode(clientId,input);
    }

    @Test(expected =  ValidationException.class)
    public void test_createEpisode_throws_validationExceptionForCreateUser_testCase1() {
        Long clientId = 1L;
        Map<String, Object> requestInput = new HashMap<>();
        requestInput.put(FieldConstants.EPISODE_FIELD_PERSON_ID, 1L);
        requestInput.put(FieldConstants.EPISODE_FIELD_ADDED_BY, 1L);
        requestInput.put(FieldConstants.EPISODE_FIELD_TYPE_OF_EPISODE, "IndiaTb");
        requestInput.put(FieldConstants.EPISODE_FIELD_DISEASE_ID, 1L);

        Map<String, Object> stageTransitionFieldValueMap = new HashMap<>();
        stageTransitionFieldValueMap.put("ToStage", "Stage0");
        when(stageTransitionService.stageTransitionHandler(requestInput, clientId)).thenReturn(new EventStreamingDto<>("eventName", stageTransitionFieldValueMap));
        when(stageRepository.findByStageName(any())).thenReturn(getStage0());
        doThrow(ValidationException.class).when(userService).createUser(any());

        episodeService.processCreateEpisode(clientId, requestInput);

        Mockito.verify(episodeRepository, Mockito.times(0)).save(any());
        Mockito.verify(episodeStageRepository, Mockito.times(0)).save(any());
        Mockito.verify(episodeAssociationRepository, Mockito.times(0)).saveAll(any());
        Mockito.verify(episodeHierarchyLinkageRepository, Mockito.times(0)).saveAll(any());
        Mockito.verify(episodeStageDataRepository, Mockito.times(0)).saveAll(any());
        Mockito.verify(userService, Mockito.times(0)).createUser(any());
    }

    @Test(expected =  ValidationException.class)
    public void test_createEpisode_throws_validationExceptionForCreateUser_testCase2() {
        Long clientId = 1L;
        Map<String, Object> requestInput = new HashMap<>();
        requestInput.put(FieldConstants.EPISODE_FIELD_PERSON_ID, 1L);
        requestInput.put(FieldConstants.EPISODE_FIELD_ADDED_BY, 1L);
        requestInput.put(FieldConstants.EPISODE_FIELD_TYPE_OF_EPISODE, "IndiaTb");
        requestInput.put(FieldConstants.EPISODE_FIELD_DISEASE_ID_OPTIONS, "1");

        Map<String, Object> stageTransitionFieldValueMap = new HashMap<>();
        stageTransitionFieldValueMap.put("ToStage", "Stage0");
        when(stageTransitionService.stageTransitionHandler(requestInput, clientId)).thenReturn(new EventStreamingDto<>("eventName", stageTransitionFieldValueMap));
        when(stageRepository.findByStageName(any())).thenReturn(getStage0());
        doThrow(ValidationException.class).when(userService).createUser(any());

        episodeService.processCreateEpisode(clientId, requestInput);

        Mockito.verify(episodeRepository, Mockito.times(0)).save(any());
        Mockito.verify(episodeStageRepository, Mockito.times(0)).save(any());
        Mockito.verify(episodeAssociationRepository, Mockito.times(0)).saveAll(any());
        Mockito.verify(episodeHierarchyLinkageRepository, Mockito.times(0)).saveAll(any());
        Mockito.verify(episodeStageDataRepository, Mockito.times(0)).saveAll(any());
        Mockito.verify(userService, Mockito.times(0)).createUser(any());
    }

    @Test
    public void test_createEpisode_success_withoutHierarchyAndSupportedRelations() {
        Long clientId = 1L;
        Long personId = 2L;
        Map<String, Object> requestInput = new HashMap<>();
        requestInput.put(FieldConstants.EPISODE_FIELD_PERSON_ID, 1L);
        requestInput.put(FieldConstants.EPISODE_FIELD_ADDED_BY, 1L);
        requestInput.put(FieldConstants.EPISODE_FIELD_TYPE_OF_EPISODE, "IndiaTb");
        requestInput.put(FieldConstants.EPISODE_FIELD_DISEASE_ID, 1L);
        requestInput.put("SR1", "Association1");
        requestInput.put("SR4", 3L);
        requestInput.put("Field1", "Field1Value");
        requestInput.put("monitoringMethod", "NONE");
        requestInput.put("addedFrom", "mobile");
        requestInput.put("startDate","2022-08-20 00:00:00");

        Map<String, Object> stageTransitionFieldValueMap = new HashMap<>();
        stageTransitionFieldValueMap.put("ToStage", "Stage0");
        stageTransitionFieldValueMap.put("ssoRegistration", true);
        when(stageTransitionService.stageTransitionHandler(requestInput, clientId)).thenReturn(new EventStreamingDto<>("eventName", stageTransitionFieldValueMap));
        when(stageRepository.findByStageName(any())).thenReturn(getStage0());
        when(episodeRepository.save(any())).thenReturn(getEpisode_testCase1_withoutCurrentStageId());
        when(episodeStageRepository.save(any())).thenReturn(getEpisodeStage_testCase1());
        when(supportedRelationRepository.findAll()).thenReturn(new ArrayList<>()).thenReturn(new ArrayList<>());
        when(episodeAssociationRepository.getAllByEpisodeIdIn(any())).thenReturn(new ArrayList<>());
        when(episodeHierarchyLinkageRepository.getAllByEpisodeIdIn(any())).thenReturn(new ArrayList<>());
        when(diseaseTemplateService.getDefinedStageIdKeyMappingsListMap(any(), any())).thenReturn(getDefinedStageIdKeyMappingsListMap_testCase1());
        when(diseaseTemplateService.getDiseasesForClient(clientId)).thenReturn(getDiseases());
        when(episodeStageDataRepository.findAllByEpisodeStageIdIn(any())).thenReturn(new ArrayList<>());
        when(ssoService.registration(any())).thenReturn(true);
        when(iamService.registerEntity(eq("1"), eq("NONE"), eq("1"), any(), any(), any())).thenReturn(true);
        when(userService.createUser(any())).thenReturn(personId);
        doNothing().when(adherenceService).registerEntity(any(), any(), any(), any());
        AtomicInteger associationCount = new AtomicInteger();
        AtomicInteger hierarchyMapsCount = new AtomicInteger();
        AtomicInteger stageDataCount = new AtomicInteger();
        when(episodeAssociationRepository.saveAll(any())).thenAnswer(i -> {
            List<EpisodeAssociation> argument = i.getArgument(0);
            associationCount.set(argument.size());
            return argument;
        });
        when(episodeHierarchyLinkageRepository.saveAll(any())).thenAnswer(i -> {
            List<EpisodeHierarchyLinkage> argument = i.getArgument(0);
            hierarchyMapsCount.set(argument.size());
            return argument;
        });
        when(episodeStageDataRepository.saveAll(any())).thenAnswer(i -> {
            List<EpisodeStageData> argument = i.getArgument(0);
            stageDataCount.set(argument.size());
            return argument;
        });

        episodeService.processCreateEpisode(clientId, requestInput);
        Mockito.verify(episodeRepository, Mockito.times(1)).save(any());
        Mockito.verify(episodeStageRepository, Mockito.times(1)).save(any());
        Mockito.verify(episodeAssociationRepository, Mockito.times(1)).saveAll(any());
        Mockito.verify(episodeHierarchyLinkageRepository, Mockito.times(0)).saveAll(any());
        Mockito.verify(episodeStageDataRepository, Mockito.times(1)).saveAll(any());
        Mockito.verify(userService, Mockito.times(0)).createUser(any());
        Mockito.verify(adherenceService, Mockito.times(1)).registerEntity(any(), any(), any(), any());
        assertEquals(0, associationCount.get());
        assertEquals(0, hierarchyMapsCount.get());
        assertEquals(1, stageDataCount.get());
    }

    @Test
    public void test_processCreateEpisode_success_testCase1() {
        Long clientId = 1L;
        Long personId = 2L;
        Map<String, Object> requestInput = new HashMap<>();
        requestInput.put(FieldConstants.EPISODE_FIELD_PERSON_ID, 1L);
        requestInput.put(FieldConstants.EPISODE_FIELD_ADDED_BY, 1L);
        requestInput.put(FieldConstants.EPISODE_FIELD_TYPE_OF_EPISODE, "IndiaTb");
        requestInput.put(FieldConstants.EPISODE_FIELD_DISEASE_ID, 1L);
        requestInput.put("SR1", "Association1");
        requestInput.put("SR4", 3L);
        requestInput.put("Field1", "Field1Value");
        requestInput.put("monitoringMethod", "NONE");
        requestInput.put("addedFrom", "mobile");
        requestInput.put("startDate","2022-08-20 00:00:00");

        Map<String, Object> stageTransitionFieldValueMap = new HashMap<>();
        stageTransitionFieldValueMap.put("ToStage", "Stage0");
        stageTransitionFieldValueMap.put("ssoRegistration", true);
        when(stageTransitionService.stageTransitionHandler(requestInput, clientId)).thenReturn(new EventStreamingDto<>("eventName", stageTransitionFieldValueMap));
        when(stageRepository.findByStageName(any())).thenReturn(getStage0());
        when(episodeRepository.save(any())).thenReturn(getEpisode_testCase1_withoutCurrentStageId());
        when(episodeStageRepository.save(any())).thenReturn(getEpisodeStage_testCase1());
        when(supportedRelationRepository.findAll()).thenReturn(getAllSupportedRelation()).thenReturn(getAllSupportedRelation());
        when(episodeAssociationRepository.getAllByEpisodeIdIn(any())).thenReturn(new ArrayList<>());
        when(episodeHierarchyLinkageRepository.getAllByEpisodeIdIn(any())).thenReturn(new ArrayList<>());
        when(diseaseTemplateService.getDefinedStageIdKeyMappingsListMap(any(), any())).thenReturn(getDefinedStageIdKeyMappingsListMap_testCase1());
        when(diseaseTemplateService.getDiseasesForClient(clientId)).thenReturn(getDiseases());
        when(episodeStageDataRepository.findAllByEpisodeStageIdIn(any())).thenReturn(new ArrayList<>());
        when(ssoService.registration(any())).thenReturn(true);
        when(iamService.registerEntity(eq("1"), eq("NONE"), eq("1"), any(), any(), any())).thenReturn(true);
        when(userService.createUser(any())).thenReturn(personId);
        Mockito.verify(userService, Mockito.times(0)).createUser(any());
        AtomicInteger associationCount = new AtomicInteger();
        AtomicInteger hierarchyMapsCount = new AtomicInteger();
        AtomicInteger stageDataCount = new AtomicInteger();
        when(episodeAssociationRepository.saveAll(any())).thenAnswer(i -> {
            List<EpisodeAssociation> argument = i.getArgument(0);
            associationCount.set(argument.size());
            return argument;
        });
        when(episodeHierarchyLinkageRepository.saveAll(any())).thenAnswer(i -> {
            List<EpisodeHierarchyLinkage> argument = i.getArgument(0);
            hierarchyMapsCount.set(argument.size());
            return argument;
        });
        when(episodeStageDataRepository.saveAll(any())).thenAnswer(i -> {
            List<EpisodeStageData> argument = i.getArgument(0);
            stageDataCount.set(argument.size());
            return argument;
        });

        episodeService.processCreateEpisode(clientId, requestInput);
        Mockito.verify(episodeRepository, Mockito.times(1)).save(any());
        Mockito.verify(episodeStageRepository, Mockito.times(1)).save(any());
        Mockito.verify(episodeAssociationRepository, Mockito.times(1)).saveAll(any());
        Mockito.verify(episodeHierarchyLinkageRepository, Mockito.times(1)).saveAll(any());
        Mockito.verify(episodeStageDataRepository, Mockito.times(1)).saveAll(any());
        Mockito.verify(userService, Mockito.times(0)).createUser(any());
        Mockito.verify(adherenceService, Mockito.times(1)).registerEntity(any(), any(), any(), any());
        assertEquals(1, associationCount.get());
        assertEquals(1, hierarchyMapsCount.get());
        assertEquals(1, stageDataCount.get());
    }

    @Test
    public void test_processCreateEpisode_success_testCase1_without_personId() {
        Long clientId = 1L;
        Long personId = 2L;
        Map<String, Object> requestInput = new HashMap<>();
        requestInput.put(FieldConstants.EPISODE_FIELD_ADDED_BY, 1L);
        requestInput.put(FieldConstants.EPISODE_FIELD_TYPE_OF_EPISODE, "IndiaTb");
        requestInput.put(FieldConstants.EPISODE_FIELD_DISEASE_ID, 1L);
        requestInput.put("SR1", "Association1");
        requestInput.put("SR4", 3L);
        requestInput.put("Field1", "Field1Value");
        requestInput.put("monitoringMethod", "NONE");
        requestInput.put("addedFrom", "mobile");
        requestInput.put("startDate","2022-08-20 00:00:00");

        Map<String, Object> stageTransitionFieldValueMap = new HashMap<>();
        stageTransitionFieldValueMap.put("ToStage", "Stage0");
        stageTransitionFieldValueMap.put("ssoRegistration", true);
        when(stageTransitionService.stageTransitionHandler(requestInput, clientId)).thenReturn(new EventStreamingDto<>("eventName", stageTransitionFieldValueMap));
        when(stageRepository.findByStageName(any())).thenReturn(getStage0());
        when(episodeRepository.save(any())).thenReturn(getEpisode_testCase1_withoutCurrentStageId());
        when(episodeStageRepository.save(any())).thenReturn(getEpisodeStage_testCase1());
        when(supportedRelationRepository.findAll()).thenReturn(getAllSupportedRelation()).thenReturn(getAllSupportedRelation());
        when(episodeAssociationRepository.getAllByEpisodeIdIn(any())).thenReturn(new ArrayList<>());
        when(episodeHierarchyLinkageRepository.getAllByEpisodeIdIn(any())).thenReturn(new ArrayList<>());
        when(diseaseTemplateService.getDefinedStageIdKeyMappingsListMap(any(), any())).thenReturn(getDefinedStageIdKeyMappingsListMap_testCase1());
        when(diseaseTemplateService.getDiseasesForClient(clientId)).thenReturn(getDiseases());
        when(episodeStageDataRepository.findAllByEpisodeStageIdIn(any())).thenReturn(new ArrayList<>());
        when(ssoService.registration(any())).thenReturn(true);
        when(iamService.registerEntity(eq(String.valueOf(1L)), eq("NONE"), eq("1"), any(), any(), any())).thenReturn(true);
        when(userService.createUser(any())).thenReturn(personId);
        AtomicInteger associationCount = new AtomicInteger();
        AtomicInteger hierarchyMapsCount = new AtomicInteger();
        AtomicInteger stageDataCount = new AtomicInteger();
        when(episodeAssociationRepository.saveAll(any())).thenAnswer(i -> {
            List<EpisodeAssociation> argument = i.getArgument(0);
            associationCount.set(argument.size());
            return argument;
        });
        when(episodeHierarchyLinkageRepository.saveAll(any())).thenAnswer(i -> {
            List<EpisodeHierarchyLinkage> argument = i.getArgument(0);
            hierarchyMapsCount.set(argument.size());
            return argument;
        });
        when(episodeStageDataRepository.saveAll(any())).thenAnswer(i -> {
            List<EpisodeStageData> argument = i.getArgument(0);
            stageDataCount.set(argument.size());
            return argument;
        });

        episodeService.processCreateEpisode(clientId, requestInput);
        Mockito.verify(episodeRepository, Mockito.times(1)).save(any());
        Mockito.verify(episodeStageRepository, Mockito.times(1)).save(any());
        Mockito.verify(episodeAssociationRepository, Mockito.times(1)).saveAll(any());
        Mockito.verify(episodeHierarchyLinkageRepository, Mockito.times(1)).saveAll(any());
        Mockito.verify(episodeStageDataRepository, Mockito.times(1)).saveAll(any());
        Mockito.verify(userService, Mockito.times(1)).createUser(any());
        assertEquals(1, associationCount.get());
        assertEquals(1, hierarchyMapsCount.get());
        assertEquals(1, stageDataCount.get());
    }

    @Test(expected =  InternalServerErrorException.class)
    public void test_processCreateEpisode_throwsExceptionInSsoRegistration() {
        Long clientId = 1L;
        Long personId = 2L;
        Map<String, Object> requestInput = new HashMap<>();
        requestInput.put(FieldConstants.EPISODE_FIELD_PERSON_ID, 1L);
        requestInput.put(FieldConstants.EPISODE_FIELD_ADDED_BY, 1L);
        requestInput.put(FieldConstants.EPISODE_FIELD_TYPE_OF_EPISODE, "IndiaTb");
        requestInput.put(FieldConstants.EPISODE_FIELD_DISEASE_ID, 1L);
        requestInput.put("SR1", "Association1");
        requestInput.put("SR4", 3L);
        requestInput.put("Field1", "Field1Value");
        requestInput.put("monitoringMethod", "NONE");
        requestInput.put("addedFrom", "mobile");
        requestInput.put("startDate", "2022-10-11 00:00:00");

        Map<String, Object> stageTransitionFieldValueMap = new HashMap<>();
        stageTransitionFieldValueMap.put("ToStage", "Stage0");
        stageTransitionFieldValueMap.put("ssoRegistration", true);
        when(stageTransitionService.stageTransitionHandler(requestInput, clientId)).thenReturn(new EventStreamingDto<>("eventName", stageTransitionFieldValueMap));
        when(stageRepository.findByStageName(any())).thenReturn(getStage0());
        when(episodeRepository.save(any())).thenReturn(getEpisode_testCase1_withoutCurrentStageId());
        when(episodeStageRepository.save(any())).thenReturn(getEpisodeStage_testCase1());
        when(supportedRelationRepository.findAll()).thenReturn(getAllSupportedRelation()).thenReturn(getAllSupportedRelation());
        when(episodeAssociationRepository.getAllByEpisodeIdIn(any())).thenReturn(new ArrayList<>());
        when(episodeHierarchyLinkageRepository.getAllByEpisodeIdIn(any())).thenReturn(new ArrayList<>());
        when(diseaseTemplateService.getDefinedStageIdKeyMappingsListMap(any(), any())).thenReturn(getDefinedStageIdKeyMappingsListMap_testCase1());
        when(diseaseTemplateService.getDiseasesForClient(clientId)).thenReturn(getDiseases());
        when(episodeStageDataRepository.findAllByEpisodeStageIdIn(any())).thenReturn(new ArrayList<>());
        when(ssoService.registration(any())).thenReturn(false);
        when(iamService.registerEntity(eq(String.valueOf(1L)), eq("NONE"), eq("1"), any(), any(), any())).thenReturn(true);
        when(userService.createUser(any())).thenReturn(personId);
        doNothing().when(adherenceService).registerEntity(any(), any(), any(), any());
        AtomicInteger associationCount = new AtomicInteger();
        AtomicInteger hierarchyMapsCount = new AtomicInteger();
        AtomicInteger stageDataCount = new AtomicInteger();
        when(episodeAssociationRepository.saveAll(any())).thenAnswer(i -> {
            List<EpisodeAssociation> argument = i.getArgument(0);
            associationCount.set(argument.size());
            return argument;
        });
        when(episodeHierarchyLinkageRepository.saveAll(any())).thenAnswer(i -> {
            List<EpisodeHierarchyLinkage> argument = i.getArgument(0);
            hierarchyMapsCount.set(argument.size());
            return argument;
        });
        when(episodeStageDataRepository.saveAll(any())).thenAnswer(i -> {
            List<EpisodeStageData> argument = i.getArgument(0);
            stageDataCount.set(argument.size());
            return argument;
        });

        episodeService.processCreateEpisode(clientId, requestInput);
    }

    @Test(expected = ValidationException.class)
    public void test_processUpdateEpisode_throws_validationExceptionForEpisodeId(){
        Map<String, Object> input = new HashMap<>();
        Long clientId = 1L;
        episodeService.processUpdateEpisode(clientId,input);
    }

    @Test(expected =  ValidationException.class)
    public void test_processUpdateEpisode_throws_validationExceptionForEndDateEarlierThanStartDate(){
        Map<String, Object> requestInput = new HashMap<>();
        Long clientId = 1L;
        requestInput.put(FieldConstants.EPISODE_FIELD_EPISODE_ID, 1L);
        requestInput.put(FieldConstants.EPISODE_FIELD_END_DATE, Utils.getFormattedDateNew(Utils.getCurrentDateNew().minusDays(1)));
        when(diseaseTemplateService.getDefinedStageIdKeyMappingsListMap(any(), any())).thenReturn(getDefinedStageIdKeyMappingsListMap_testCase1());
        doReturn(getEpisodeDtoEntity_testCase1()).when(episodeService).getEpisodeDetails(any(), any());
        when(episodeRepository.findByIdAndClientIdAndDeletedIs(any(), any(),any())).thenReturn(getEpisode_testCase1());
        episodeService.processUpdateEpisode(clientId, requestInput);
    }

    @Test
    public void test_processUpdateEpisode_success_testCase1() {
        Long clientId = 1L;
        Map<String, Object> requestInput = new HashMap<>();
        requestInput.put(FieldConstants.EPISODE_FIELD_EPISODE_ID, 1L);
        requestInput.put(FieldConstants.EPISODE_FIELD_TYPE_OF_EPISODE, "type1");
        requestInput.put(FieldConstants.EPISODE_FIELD_DISEASE_ID, 1L);
        requestInput.put(FieldConstants.EPISODE_FIELD_START_DATE, "2021-11-21 00:00:00");
        requestInput.put(FieldConstants.EPISODE_FIELD_RISK_STATUS, "Status1");
        requestInput.put("SR1", "AssociationValue1New");
        requestInput.put("SR4", 33L);
        requestInput.put("Field1", "Field1ValueNew");

        doReturn(getEpisodeDtoEntity_testCase1()).when(episodeService).getEpisodeDetails(any(), any());
        when(stageRepository.findByStageName(any())).thenReturn(getStage0());
        when(episodeRepository.save(any())).thenReturn(getEpisode_testCase1());
        when(episodeStageRepository.getCurrentStageForEpisodeId(any())).thenReturn(getEpisodeStage_testCase1());
        when(supportedRelationRepository.findAll()).thenReturn(getAllSupportedRelation()).thenReturn(getAllSupportedRelation());
        when(episodeAssociationRepository.getAllByEpisodeIdIn(any())).thenReturn(getEpisodeAssociations_testCase1());
        when(episodeHierarchyLinkageRepository.getAllByEpisodeIdIn(any())).thenReturn(getEpisodeHierarchyLinkages_testCase1());
        when(diseaseTemplateService.getDefinedStageIdKeyMappingsListMap(any(), any())).thenReturn(getDefinedStageIdKeyMappingsListMap_testCase1());
        when(diseaseTemplateService.getDiseasesForClient(clientId)).thenReturn(getDiseases());
        when(episodeStageDataRepository.findAllByEpisodeStageIdIn(any())).thenReturn(getEpisodeStageDataList_testCase1());
        when(episodeRepository.findByIdAndClientIdAndDeletedIs(any(), any(), any())).thenReturn(getEpisode_testCase1());
        doNothing().when(episodeService).processEpisodeDataWithStageTransitionHandler(any(), any(), any(), any(), any());
        doNothing().when(userService).updateUser(any(), any());
        doNothing().when(episodeService).updateElasticDocument(any(), any(), eq(true));
        AtomicInteger associationCount = new AtomicInteger();
        AtomicInteger hierarchyMapsCount = new AtomicInteger();
        AtomicInteger stageDataCount = new AtomicInteger();
        AtomicReference<String> associationNewValue = new AtomicReference<>();
        AtomicLong hierarchyMappingNewValue = new AtomicLong();
        AtomicReference<String> stageDataNewValue = new AtomicReference<>();
        when(episodeAssociationRepository.saveAll(any())).thenAnswer(i -> {
            List<EpisodeAssociation> argument = i.getArgument(0);
            associationCount.set(argument.size());
            associationNewValue.set(argument.size() == 1 ? argument.get(0).getValue() : associationNewValue.get());
            return argument;
        });
        when(episodeHierarchyLinkageRepository.saveAll(any())).thenAnswer(i -> {
            List<EpisodeHierarchyLinkage> argument = i.getArgument(0);
            hierarchyMapsCount.set(argument.size());
            hierarchyMappingNewValue.set(argument.size() == 1 ? argument.get(0).getHierarchyId() : hierarchyMappingNewValue.get());
            return argument;
        });
        when(episodeStageDataRepository.saveAll(any())).thenAnswer(i -> {
            List<EpisodeStageData> argument = i.getArgument(0);
            stageDataCount.set(argument.size());
            stageDataNewValue.set(argument.size() == 1 ? argument.get(0).getValue(): stageDataNewValue.get());
            return argument;
        });

        episodeService.processUpdateEpisode(clientId, requestInput);
        Mockito.verify(episodeRepository, Mockito.times(1)).save(any());
        Mockito.verify(episodeAssociationRepository, Mockito.times(1)).saveAll(any());
        Mockito.verify(episodeHierarchyLinkageRepository, Mockito.times(1)).saveAll(any());
        Mockito.verify(episodeStageDataRepository, Mockito.times(1)).saveAll(any());
        Mockito.verify(userService, Mockito.times(1)).updateUser(any(), any());
        Mockito.verify(episodeService, Mockito.times(1)).updateElasticDocument(any(), any(), eq(true));
        assertEquals(1, associationCount.get());
        assertEquals(1, hierarchyMapsCount.get());
        assertEquals(1, stageDataCount.get());
        assertEquals(requestInput.get("SR1"),associationNewValue.get());
        assertEquals(requestInput.get("SR4"), hierarchyMappingNewValue.get());
        assertEquals(requestInput.get("Field1"), stageDataNewValue.get());
    }

    DiseaseTemplateResponse getDiseases() {
        List<DiseaseTemplate> diseases  = Collections.singletonList(new DiseaseTemplate(1L, "TB", 1L, false, "TB"));
        return new DiseaseTemplateResponse(diseases);
    }
    DiseaseTemplateResponse getDiseasesForClientId171() {
        List<DiseaseTemplate> diseases  = Collections.singletonList(new DiseaseTemplate(16L, "TB", 171L, false, "TB"));
        return new DiseaseTemplateResponse(diseases);
    }

    @Test
    public void test_processEpisodeDataWithStageTransitionHandler_with_deleteAsDuplicateStatus() {
        Map<String, Object> stageTransitionResult = getStageTransitionResult_withDeleteAndDuplicate();
        EpisodeDto updatedEpisodeDetails = getEpisodeDtoEntity_with_StageData_testCaseOne();
        Map<String, Object> requestInput = new HashMap<>();
        requestInput.put(EpisodeField.PROCESS_EPISODE_DUPLICATES.getFieldName(), true);
        doReturn(updatedEpisodeDetails).when(episodeService).getEpisodeDetails(any(), any());
        when(stageTransitionService.stageTransitionHandler(any(), any())).thenReturn(new EventStreamingDto<>("#", stageTransitionResult));
        doNothing().when(episodeService).processStageTransitionResult(any(), any());
        doReturn(getEpisode_testCase4()).when(episodeService).getEpisode(any(), any());
        doNothing().when(userService).deleteUser(any(), any());
        doNothing().when(adherenceService).updateEntity(any(), any(), any(), any());

        episodeService.processEpisodeDataWithStageTransitionHandler(getEpisode_testCase1(), new EpisodeDetailsDto(), requestInput, new HashMap<>(), 1L);

        Mockito.verify(episodeService, Mockito.times(1)).getEpisodeDetails(any(), any());
        Mockito.verify(stageTransitionService, Mockito.times(1)).stageTransitionHandler(any(), any());
        Mockito.verify(episodeService, Mockito.times(1)).processStageTransitionResult(any(), any());
        Mockito.verify(episodeService, Mockito.times(1)).getEpisode(any(), any());
        Mockito.verify(userService, Mockito.times(1)).deleteUser(any(), any());
        Mockito.verify(episodeRepository, Mockito.times(1)).save(any());
        Mockito.verify(adherenceService, Mockito.times(1)).updateEntity(any(), any(), any(), any());
    }

    @Test
    public void test_processEpisodeDataWithStageTransitionHandler_with_deleteAsDuplicateStatusForDeletedEpisode() {
        Map<String, Object> stageTransitionResult = getStageTransitionResult_withDeleteAndDuplicate();
        EpisodeDto updatedEpisodeDetails = getEpisodeDtoEntity_with_StageData_testCaseOne();
        Map<String, Object> requestInput = new HashMap<>();
        requestInput.put(EpisodeField.PROCESS_EPISODE_DUPLICATES.getFieldName(), true);
        doReturn(updatedEpisodeDetails).when(episodeService).getEpisodeDetails(any(), any());
        when(stageTransitionService.stageTransitionHandler(any(), any())).thenReturn(new EventStreamingDto<>("#", stageTransitionResult));
        doNothing().when(episodeService).processStageTransitionResult(any(), any());
        doNothing().when(adherenceService).updateEntity(any(), any(), any(), any());
        Episode inputEpisode = getEpisode_testCase1();
        inputEpisode.setDeleted(true);

        episodeService.processEpisodeDataWithStageTransitionHandler(inputEpisode, new EpisodeDetailsDto(), requestInput, new HashMap<>(), 1L);

        Mockito.verify(episodeService, Mockito.times(1)).getEpisodeDetails(any(), any());
        Mockito.verify(stageTransitionService, Mockito.times(1)).stageTransitionHandler(any(), any());
        Mockito.verify(episodeService, Mockito.times(1)).processStageTransitionResult(any(), any());
        Mockito.verify(episodeService, Mockito.times(0)).getEpisode(any(), any());
        Mockito.verify(userService, Mockito.times(0)).deleteUser(any(), any());
        Mockito.verify(episodeRepository, Mockito.times(0)).save(any());
        Mockito.verify(adherenceService, Mockito.times(1)).updateEntity(any(), any(), any(), any());
    }

    @Test
    public void test_processEpisodeDataWithStageTransitionHandler_with_userConfirmedDuplicateStatus() {
        Map<String, Object> stageTransitionResult = getStageTransitionResult();
        EpisodeDto updatedEpisodeDetails = getEpisodeDtoEntity_with_StageData_testCaseTwo();
        Map<String, Object> requestInput = new HashMap<>();
        requestInput.put(EpisodeField.PROCESS_EPISODE_DUPLICATES.getFieldName(), true);
        doReturn(updatedEpisodeDetails).when(episodeService).getEpisodeDetails(any(), any());
        when(stageTransitionService.stageTransitionHandler(any(), any())).thenReturn(new EventStreamingDto<>("#", stageTransitionResult));
        doNothing().when(episodeService).processStageTransitionResult(any(), any());
        doReturn(getEpisode_testCase4()).when(episodeService).getEpisode(any(), any());
        doNothing().when(userService).deleteUser(any(), any());
        doNothing().when(adherenceService).updateEntity(any(), any(), any(), any());

        episodeService.processEpisodeDataWithStageTransitionHandler(getEpisode_testCase1(), new EpisodeDetailsDto(), requestInput, new HashMap<>(),1L);

        verify(episodeService, times(1)).getEpisodeDetails(any(), any());
        verify(stageTransitionService, times(1)).stageTransitionHandler(any(), any());
        verify(episodeService, times(1)).processStageTransitionResult(any(), any());
        verify(episodeService, times(1)).getEpisode(any(), any());
        verify(userService, times(1)).deleteUser(any(), any());
        verify(episodeRepository, times(1)).save(any());
        Mockito.verify(adherenceService, Mockito.times(1)).updateEntity(any(), any(), any(), any());
    }

    @Test
    public void test_processEpisodeDataWithStageTransitionHandler_with_noDuplicateProcessing() {
        Map<String, Object> stageTransitionResult = getStageTransitionResult();
        EpisodeDto updatedEpisodeDetails = getEpisodeDtoEntity_with_StageData_testCaseThree();
        Map<String, Object> requestInput = new HashMap<>();
        requestInput.put(EpisodeField.PROCESS_EPISODE_DUPLICATES.getFieldName(), true);
        doReturn(updatedEpisodeDetails).when(episodeService).getEpisodeDetails(any(), any());
        when(stageTransitionService.stageTransitionHandler(any(), any())).thenReturn(new EventStreamingDto<>("#", stageTransitionResult));
        doNothing().when(episodeService).processStageTransitionResult(any(), any());
        doNothing().when(adherenceService).updateEntity(any(), any(), any(), any());

        episodeService.processEpisodeDataWithStageTransitionHandler(getEpisode_testCase1(), new EpisodeDetailsDto(), requestInput, new HashMap<>(),1L);

        verify(episodeService, times(1)).getEpisodeDetails(any(), any());
        verify(stageTransitionService, times(1)).stageTransitionHandler(any(), any());
        verify(episodeService, times(1)).processStageTransitionResult(any(), any());
        verify(episodeService, times(0)).getEpisode(any(), any());
        verify(userService, times(0)).deleteUser(any(), any());
        verify(episodeRepository, times(0)).save(any());
        Mockito.verify(adherenceService, Mockito.times(1)).updateEntity(any(), any(), any(), any());
    }

    @Test
    public void test_processEpisodeDataWithStageTransitionHandler_with_uniqueDuplicateStatus_caseOne() {
        Long clientId = 1L;
        Map<String, Object> stageTransitionResult = getStageTransitionResult_withDeleteAndDuplicate();
        EpisodeDto updatedEpisodeDetails = getEpisodeDtoEntity_with_StageData_testCaseThree();
        Map<String, Object> requestInput = new HashMap<>();
        requestInput.put(EpisodeField.PROCESS_EPISODE_DUPLICATES.getFieldName(), true);
        List<EpisodeIndex> duplicateEpisodeIndexList = new ArrayList<>();
        EpisodeDto duplicateEpisodeDtoOne = getEpisodeDtoEntity_testCase1();
        duplicateEpisodeIndexList.add(EpisodeIndex.convert(duplicateEpisodeDtoOne, clientId));
        EpisodeDto duplicateEpisodeDtoTwo= getEpisodeDtoEntity_testCase1();
        duplicateEpisodeDtoTwo.setId(3L);
        duplicateEpisodeIndexList.add(EpisodeIndex.convert(duplicateEpisodeDtoTwo, clientId));
        doReturn(updatedEpisodeDetails).when(episodeService).getEpisodeDetails(any(), any());
        when(stageTransitionService.stageTransitionHandler(any(), any())).thenReturn(new EventStreamingDto<>("#", stageTransitionResult));
        doNothing().when(episodeService).processStageTransitionResult(any(), any());
        doReturn(getEpisode_testCase4()).when(episodeService).getEpisode(any(), any());
        doNothing().when(userService).deleteUser(any(), any());
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client());
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(2, duplicateEpisodeIndexList, null);
        doReturn(episodeSearchResult).when(episodeService).elasticSearch(any());
        doNothing().when(adherenceService).updateEntity(any(), any(), any(), any());

        episodeService.processEpisodeDataWithStageTransitionHandler(getEpisode_testCase1(), new EpisodeDetailsDto(), requestInput, new HashMap<>(), clientId);

        Mockito.verify(episodeService, Mockito.times(1)).getEpisodeDetails(any(), any());
        Mockito.verify(stageTransitionService, Mockito.times(1)).stageTransitionHandler(any(), any());
        Mockito.verify(episodeService, Mockito.times(1)).processStageTransitionResult(any(), any());
        Mockito.verify(episodeService, Mockito.times(1)).getEpisode(any(), any());
        Mockito.verify(userService, Mockito.times(1)).deleteUser(any(), any());
        Mockito.verify(episodeRepository, Mockito.times(1)).save(any());
        Mockito.verify(episodeService, Mockito.times(1)).elasticSearch(any());
        Mockito.verify(adherenceService, Mockito.times(1)).updateEntity(any(), any(), any(), any());
    }

    @Test
    public void test_processEpisodeDataWithStageTransitionHandler_with_uniqueDuplicateStatus_caseTwo() {
        Long clientId = 1L;
        Map<String, Object> stageTransitionResult = getStageTransitionResult_withDeleteAndDuplicate();
        EpisodeDto updatedEpisodeDetails = getEpisodeDtoEntity_with_StageData_testCaseThree();
        List<EpisodeDto> allLinkedEpisodeList = getEpisodeDtoEntityList_with_samePersonId();
        Map<String, Object> requestInput = new HashMap<>();
        List<EpisodeIndex> duplicateEpisodeIndexList = new ArrayList<>();
        EpisodeDto duplicateEpisodeDtoOne = getEpisodeDtoEntity_with_StageData_testCaseThree();
        duplicateEpisodeIndexList.add(EpisodeIndex.convert(duplicateEpisodeDtoOne, clientId));
        EpisodeDto duplicateEpisodeDtoTwo= getEpisodeDtoEntity_with_StageData_testCaseThree();
        duplicateEpisodeDtoTwo.setId(3L);
        duplicateEpisodeIndexList.add(EpisodeIndex.convert(duplicateEpisodeDtoTwo, clientId));
        requestInput.put(EpisodeField.PROCESS_EPISODE_DUPLICATES.getFieldName(), true);
        doReturn(updatedEpisodeDetails).when(episodeService).getEpisodeDetails(any(), any());
        when(stageTransitionService.stageTransitionHandler(any(), any())).thenReturn(new EventStreamingDto<>("#", stageTransitionResult));
        doNothing().when(episodeService).processStageTransitionResult(any(), any());
        doReturn(getEpisode_testCase4()).when(episodeService).getEpisode(any(), any());
        doNothing().when(userService).deleteUser(any(), any());
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(2, duplicateEpisodeIndexList, null);
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client());
        doReturn(episodeSearchResult).when(episodeService).elasticSearch(any());
        doReturn(allLinkedEpisodeList).when(episodeService).getEpisodesForPersonList(any(), any(), anyBoolean());
        doNothing().when(adherenceService).registerEntity(any(), any(), any(), any());
        doNothing().when(adherenceService).updateEntity(any(), any(), any(), any());

        episodeService.processEpisodeDataWithStageTransitionHandler(getEpisode_testCase1(), new EpisodeDetailsDto(), requestInput, new HashMap<>(), clientId);

        Mockito.verify(episodeService, Mockito.times(1)).getEpisodeDetails(any(), any());
        Mockito.verify(stageTransitionService, Mockito.times(1)).stageTransitionHandler(any(), any());
        Mockito.verify(episodeService, Mockito.times(1)).processStageTransitionResult(any(), any());
        Mockito.verify(episodeService, Mockito.times(1)).getEpisode(any(), any());
        Mockito.verify(userService, Mockito.times(1)).deleteUser(any(), any());
        Mockito.verify(episodeRepository, Mockito.times(1)).save(any());
        Mockito.verify(episodeService, Mockito.times(1)).elasticSearch(any());
        Mockito.verify(episodeLogService, Mockito.times(0)).save(any());
        Mockito.verify(adherenceService, Mockito.times(1)).updateEntity(any(), any(), any(), any());
    }

    @Test
    public void test_processEpisodeDataWithStageTransitionHandler_with_uniqueDuplicateStatus_caseThree() {
        Long clientId = 1L;
        Map<String, Object> stageTransitionResult = getStageTransitionResult_withDeleteAndDuplicate();
        EpisodeDto updatedEpisodeDetails = getEpisodeDtoEntity_with_StageData_testCaseThree();
        List<EpisodeDto> allLinkedEpisodeList = getEpisodeDtoEntityList_with_samePersonId();
        Map<String, Object> requestInput = new HashMap<>();
        List<EpisodeIndex> duplicateEpisodeIndexList = new ArrayList<>();
        EpisodeDto duplicateEpisodeDto = getEpisodeDtoEntity_with_StageData_testCaseTwo();
        duplicateEpisodeDto.setId(3L);
        duplicateEpisodeIndexList.add(EpisodeIndex.convert(duplicateEpisodeDto, clientId));
        requestInput.put(EpisodeField.PROCESS_EPISODE_DUPLICATES.getFieldName(), true);
        doReturn(updatedEpisodeDetails).when(episodeService).getEpisodeDetails(any(), any());
        when(stageTransitionService.stageTransitionHandler(any(), any())).thenReturn(new EventStreamingDto<>("#", stageTransitionResult));
        doNothing().when(episodeService).processStageTransitionResult(any(), any());
        doReturn(getEpisode_testCase4()).when(episodeService).getEpisode(any(), any());
        doNothing().when(userService).deleteUser(any(), any());
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client());
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(2, duplicateEpisodeIndexList, null);
        doReturn(episodeSearchResult).when(episodeService).elasticSearch(any());
        doReturn(allLinkedEpisodeList).when(episodeService).getEpisodesForPersonList(any(), any(), anyBoolean());
        doNothing().when(adherenceService).updateEntity(any(), any(), any(), any());

        episodeService.processEpisodeDataWithStageTransitionHandler(getEpisode_testCase1(), new EpisodeDetailsDto(), requestInput, new HashMap<>(), clientId);

        Mockito.verify(episodeService, Mockito.times(1)).getEpisodeDetails(any(), any());
        Mockito.verify(stageTransitionService, Mockito.times(1)).stageTransitionHandler(any(), any());
        Mockito.verify(episodeService, Mockito.times(1)).processStageTransitionResult(any(), any());
        Mockito.verify(episodeService, Mockito.times(1)).getEpisode(any(), any());
        Mockito.verify(userService, Mockito.times(1)).deleteUser(any(), any());
        Mockito.verify(episodeRepository, Mockito.times(1)).save(any());
        Mockito.verify(episodeService, Mockito.times(1)).elasticSearch(any());
        Mockito.verify(episodeLogService, Mockito.times(1)).save(any());
        Mockito.verify(adherenceService, Mockito.times(1)).updateEntity(any(), any(), any(), any());
    }

    @Test
    public void test_processEpisodeDataWithStageTransitionHandler_with_uniqueDuplicateStatus_and_noDuplicates() {
        Long clientId = 1L;
        Map<String, Object> stageTransitionResult = getStageTransitionResult_withDeleteAndDuplicate();
        EpisodeDto updatedEpisodeDetails = getEpisodeDtoEntity_with_StageData_testCaseThree();
        Map<String, Object> requestInput = new HashMap<>();
        requestInput.put(EpisodeField.PROCESS_EPISODE_DUPLICATES.getFieldName(), true);
        doReturn(updatedEpisodeDetails).when(episodeService).getEpisodeDetails(any(), any());
        when(stageTransitionService.stageTransitionHandler(any(), any())).thenReturn(new EventStreamingDto<>("#", stageTransitionResult));
        doNothing().when(episodeService).processStageTransitionResult(any(), any());
        doReturn(getEpisode_testCase4()).when(episodeService).getEpisode(any(), any());
        doNothing().when(userService).deleteUser(any(), any());
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client());
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(0, new ArrayList<>(), null);
        doReturn(episodeSearchResult).when(episodeService).elasticSearch(any());
        doNothing().when(adherenceService).updateEntity(any(), any(), any(), any());

        episodeService.processEpisodeDataWithStageTransitionHandler(getEpisode_testCase1(), new EpisodeDetailsDto(), requestInput, new HashMap<>(), clientId);

        Mockito.verify(episodeService, Mockito.times(1)).getEpisodeDetails(any(), any());
        Mockito.verify(stageTransitionService, Mockito.times(1)).stageTransitionHandler(any(), any());
        Mockito.verify(episodeService, Mockito.times(1)).processStageTransitionResult(any(), any());
        Mockito.verify(episodeService, Mockito.times(1)).getEpisode(any(), any());
        Mockito.verify(userService, Mockito.times(1)).deleteUser(any(), any());
        Mockito.verify(episodeRepository, Mockito.times(1)).save(any());
        Mockito.verify(episodeService, Mockito.times(1)).elasticSearch(any());
        Mockito.verify(adherenceService, Mockito.times(1)).updateEntity(any(), any(), any(), any());
    }

    @Test
    public void test_processEpisodeDataWithStageTransitionHandler_with_checkAndUpdateDuplicate_caseOne() {
        Map<String, Object> stageTransitionResult = getStageTransitionResult_withCheckAndUpdateDuplicate();
        EpisodeDetailsDto episodeDetailsDto = new EpisodeDetailsDto(new EpisodeDto(), new ArrayList<>());
        Map<String, Object> requestInput = new HashMap<>();
        List<EpisodeDto> allLinkedEpisodeList = getEpisodeDtoEntityList_with_samePersonId();
        requestInput.put(EpisodeField.PROCESS_EPISODE_DUPLICATES.getFieldName(), true);
        doReturn(new EpisodeDto()).when(episodeService).getEpisodeDetails(any(), any());
        when(stageTransitionService.stageTransitionHandler(any(), any())).thenReturn(new EventStreamingDto<>("#", stageTransitionResult));
        doNothing().when(episodeService).processStageTransitionResult(any(), any());
        doReturn(allLinkedEpisodeList).when(episodeService).getEpisodesForPersonList(any(), any(), anyBoolean());
        doNothing().when(adherenceService).updateEntity(any(), any(), any(), any());

        episodeService.processEpisodeDataWithStageTransitionHandler(getEpisode_testCase1(), episodeDetailsDto, requestInput, new HashMap<>(),1L);

        Mockito.verify(episodeService, Mockito.times(1)).getEpisodeDetails(any(), any());
        Mockito.verify(stageTransitionService, Mockito.times(1)).stageTransitionHandler(any(), any());
        Mockito.verify(episodeService, Mockito.times(1)).processStageTransitionResult(any(), any());
        Mockito.verify(episodeLogService, Mockito.times(1)).save(any());
        Mockito.verify(adherenceService, Mockito.times(1)).updateEntity(any(), any(), any(), any());
    }

    @Test
    public void test_processEpisodeDataWithStageTransitionHandler_with_checkAndUpdateDuplicate_caseTwo() {
        Long clientId = 1L;
        Map<String, Object> stageTransitionResult = getStageTransitionResult_withCheckAndUpdateDuplicate();
        EpisodeDto episodeDto = getEpisodeDtoEntity_testCase1();
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("duplicateStatus", "SYSTEM_IDENTIFIED_DUPLICATE");
        episodeDto.setStageData(stageData);
        EpisodeDetailsDto episodeDetailsDto = new EpisodeDetailsDto(episodeDto, new ArrayList<>());
        Map<String, Object> requestInput = new HashMap<>();
        List<EpisodeDto> allLinkedEpisodeList = new ArrayList<>();
        allLinkedEpisodeList.add(getEpisodeDtoEntity_testCase1());
        List<EpisodeIndex> duplicateEpisodeIndexList = new ArrayList<>();
        EpisodeDto duplicateEpisodeDtoTwo = getEpisodeDtoEntity_with_StageData_testCaseTwo();
        duplicateEpisodeDtoTwo.setId(3L);
        duplicateEpisodeIndexList.add(EpisodeIndex.convert(duplicateEpisodeDtoTwo, clientId));
        requestInput.put(EpisodeField.PROCESS_EPISODE_DUPLICATES.getFieldName(), true);
        doReturn(getEpisodeDtoEntity_testCase1()).when(episodeService).getEpisodeDetails(any(), any());
        when(stageTransitionService.stageTransitionHandler(any(), any())).thenReturn(new EventStreamingDto<>("#", stageTransitionResult));
        doNothing().when(episodeService).processStageTransitionResult(any(), any());
        doReturn(allLinkedEpisodeList).when(episodeService).getEpisodesForPersonList(any(), any(), anyBoolean());
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client());
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(1, duplicateEpisodeIndexList, null);
        doReturn(episodeSearchResult).when(episodeService).elasticSearch(any());
        doNothing().when(adherenceService).updateEntity(any(), any(), any(), any());

        episodeService.processEpisodeDataWithStageTransitionHandler(getEpisode_testCase1(), episodeDetailsDto, requestInput, new HashMap<>(), clientId);

        Mockito.verify(episodeService, Mockito.times(1)).getEpisodeDetails(any(), any());
        Mockito.verify(stageTransitionService, Mockito.times(1)).stageTransitionHandler(any(), any());
        Mockito.verify(episodeService, Mockito.times(1)).processStageTransitionResult(any(), any());
        Mockito.verify(episodeLogService, Mockito.times(0)).save(any());
        Mockito.verify(episodeService, Mockito.times(1)).elasticSearch(any());
        Mockito.verify(adherenceService, Mockito.times(1)).updateEntity(any(), any(), any(), any());
    }

    @Test
    public void test_processEpisodeDataWithStageTransitionHandler_with_checkAndUpdateDuplicate_caseThree() {
        Long clientId = 1L;
        Map<String, Object> stageTransitionResult = getStageTransitionResult_withCheckAndUpdateDuplicate();
        EpisodeDetailsDto episodeDetailsDto = new EpisodeDetailsDto(getEpisodeDtoEntity_testCase1(), new ArrayList<>());
        Map<String, Object> requestInput = new HashMap<>();
        List<EpisodeDto> allLinkedEpisodeList = new ArrayList<>();
        allLinkedEpisodeList.add(getEpisodeDtoEntity_testCase1());
        requestInput.put(EpisodeField.PROCESS_EPISODE_DUPLICATES.getFieldName(), true);
        doReturn(getEpisodeDtoEntity_testCase1()).when(episodeService).getEpisodeDetails(any(), any());
        when(stageTransitionService.stageTransitionHandler(any(), any())).thenReturn(new EventStreamingDto<>("#", stageTransitionResult));
        doNothing().when(episodeService).processStageTransitionResult(any(), any());
        doReturn(allLinkedEpisodeList).when(episodeService).getEpisodesForPersonList(any(), any(), anyBoolean());
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client());
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(0, new ArrayList<>(), null);
        doReturn(episodeSearchResult).when(episodeService).elasticSearch(any());
        doNothing().when(adherenceService).updateEntity(any(), any(), any(), any());

        episodeService.processEpisodeDataWithStageTransitionHandler(getEpisode_testCase1(), episodeDetailsDto, requestInput, new HashMap<>(), clientId);

        Mockito.verify(episodeService, Mockito.times(1)).getEpisodeDetails(any(), any());
        Mockito.verify(stageTransitionService, Mockito.times(1)).stageTransitionHandler(any(), any());
        Mockito.verify(episodeService, Mockito.times(1)).processStageTransitionResult(any(), any());
        Mockito.verify(episodeLogService, Mockito.times(1)).save(any());
        Mockito.verify(episodeService, Mockito.times(1)).elasticSearch(any());
        Mockito.verify(adherenceService, Mockito.times(1)).updateEntity(any(), any(), any(), any());
    }

    @Test
    public void test_processEpisodeDataWithStageTransitionHandler_with_checkAndUpdateDuplicate_caseFour() {
        Long clientId = 1L;
        Map<String, Object> stageTransitionResult = new HashMap<>();
        stageTransitionResult.put("ToStage", "Stage1");
        stageTransitionResult.put("duplicationScheme", "PRIMARYPHONE_GENDER");
        stageTransitionResult.put(RuleConstants.CHECK_DUPLICATES_AND_UPDATE, true);
        EpisodeDetailsDto episodeDetailsDto = new EpisodeDetailsDto(getEpisodeDtoEntity_testCase1(), new ArrayList<>());
        Map<String, Object> requestInput = new HashMap<>();
        List<EpisodeDto> allLinkedEpisodeList = new ArrayList<>();
        allLinkedEpisodeList.add(getEpisodeDtoEntity_testCase1());
        requestInput.put(EpisodeField.PROCESS_EPISODE_DUPLICATES.getFieldName(), true);
        doReturn(getEpisodeDtoEntity_testCase1()).when(episodeService).getEpisodeDetails(any(), any());
        when(stageTransitionService.stageTransitionHandler(any(), any())).thenReturn(new EventStreamingDto<>("#", stageTransitionResult));
        doNothing().when(episodeService).processStageTransitionResult(any(), any());
        doReturn(allLinkedEpisodeList).when(episodeService).getEpisodesForPersonList(any(), any(), anyBoolean());
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client());
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(0, new ArrayList<>(), null);
        doReturn(episodeSearchResult).when(episodeService).elasticSearch(any());
        doNothing().when(adherenceService).updateEntity(any(), any(), any(), any());

        episodeService.processEpisodeDataWithStageTransitionHandler(getEpisode_testCase1(), episodeDetailsDto, requestInput, new HashMap<>(), clientId);

        Mockito.verify(episodeService, Mockito.times(1)).getEpisodeDetails(any(), any());
        Mockito.verify(stageTransitionService, Mockito.times(1)).stageTransitionHandler(any(), any());
        Mockito.verify(episodeService, Mockito.times(1)).processStageTransitionResult(any(), any());
        Mockito.verify(episodeLogService, Mockito.times(1)).save(any());
        Mockito.verify(episodeService, Mockito.times(1)).elasticSearch(any());
        Mockito.verify(adherenceService, Mockito.times(1)).updateEntity(any(), any(), any(), any());
    }

    @Test
    public void test_processEpisodeDataWithStageTransitionHandler_with_checkAndUpdateDuplicate_caseFive() {
        Long clientId = 1L;
        Map<String, Object> stageTransitionResult = new HashMap<>();
        stageTransitionResult.put("ToStage", "Stage1");
        stageTransitionResult.put(RuleConstants.CHECK_DUPLICATES_AND_UPDATE, true);
        EpisodeDetailsDto episodeDetailsDto = new EpisodeDetailsDto(getEpisodeDtoEntity_testCase1(), new ArrayList<>());
        Map<String, Object> requestInput = new HashMap<>();
        List<EpisodeDto> allLinkedEpisodeList = new ArrayList<>();
        allLinkedEpisodeList.add(getEpisodeDtoEntity_testCase1());
        requestInput.put(EpisodeField.PROCESS_EPISODE_DUPLICATES.getFieldName(), true);
        doReturn(getEpisodeDtoEntity_testCase1()).when(episodeService).getEpisodeDetails(any(), any());
        when(stageTransitionService.stageTransitionHandler(any(), any())).thenReturn(new EventStreamingDto<>("#", stageTransitionResult));
        doNothing().when(episodeService).processStageTransitionResult(any(), any());
        doReturn(allLinkedEpisodeList).when(episodeService).getEpisodesForPersonList(any(), any(), anyBoolean());
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client());
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(0, new ArrayList<>(), null);
        doReturn(episodeSearchResult).when(episodeService).elasticSearch(any());
        doNothing().when(adherenceService).updateEntity(any(), any(), any(), any());

        episodeService.processEpisodeDataWithStageTransitionHandler(getEpisode_testCase1(), episodeDetailsDto, requestInput, new HashMap<>(), clientId);

        Mockito.verify(episodeService, Mockito.times(1)).getEpisodeDetails(any(), any());
        Mockito.verify(stageTransitionService, Mockito.times(1)).stageTransitionHandler(any(), any());
        Mockito.verify(episodeService, Mockito.times(1)).processStageTransitionResult(any(), any());
        Mockito.verify(episodeLogService, Mockito.times(1)).save(any());
        Mockito.verify(episodeService, Mockito.times(0)).elasticSearch(any());
        Mockito.verify(adherenceService, Mockito.times(1)).updateEntity(any(), any(), any(), any());
    }

    @Test
    public void test_updateEpisodeStageForEntity_doesNothing() {
        doReturn(new EpisodeDto()).when(episodeService).getEpisodeDetails(any(), any());
        when(stageTransitionService.stageTransitionHandler(any(), any())).thenReturn(new EventStreamingDto<>("#", null));
        doNothing().when(episodeService).processStageTransitionResult(any(), any());
        doNothing().when(adherenceService).updateEntity(any(), any(), any(), any());

        episodeService.processEpisodeDataWithStageTransitionHandler(getEpisode_testCase1(), new EpisodeDetailsDto(), new HashMap<>(), new HashMap<>(), 1L);

        Mockito.verify(episodeService, Mockito.times(1)).getEpisodeDetails(any(), any());
        Mockito.verify(stageTransitionService, Mockito.times(1)).stageTransitionHandler(any(), any());
        Mockito.verify(episodeService, Mockito.times(0)).processStageTransitionResult(any(), any());
        Mockito.verify(adherenceService, Mockito.times(1)).updateEntity(any(), any(), any(), any());
    }

    @Test
    public void test_updateEpisodeStageForEntity_withoutProcessDuplicate() {
        Map<String, Object> requestInput = new HashMap<>();
        requestInput.put(EpisodeField.PROCESS_EPISODE_DUPLICATES.getFieldName(), false);
        doReturn(new EpisodeDto()).when(episodeService).getEpisodeDetails(any(), any());
        when(stageTransitionService.stageTransitionHandler(any(), any())).thenReturn(new EventStreamingDto<>("#", getStageTransitionResult()));
        doNothing().when(episodeService).processStageTransitionResult(any(), any());
        doNothing().when(adherenceService).updateEntity(any(), any(), any(), any());

        episodeService.processEpisodeDataWithStageTransitionHandler(getEpisode_testCase1(), new EpisodeDetailsDto(), requestInput, new HashMap<>(),1L);

        Mockito.verify(episodeService, Mockito.times(1)).getEpisodeDetails(any(), any());
        Mockito.verify(stageTransitionService, Mockito.times(1)).stageTransitionHandler(any(), any());
        Mockito.verify(episodeService, Mockito.times(1)).processStageTransitionResult(any(), any());
        Mockito.verify(adherenceService, Mockito.times(1)).updateEntity(any(), any(), any(), any());
    }

    @Test
    public void test_processStageTransitionResultWithNullStageTransitionResult() {
        when(stageRepository.findByStageName(any())).thenReturn(null);
        when(episodeStageRepository.getCurrentStageForEpisodeId(any())).thenReturn(new EpisodeStage(1L, 1L));

        episodeService.processStageTransitionResult(getEpisode_testCase2(), null);

        Mockito.verify(stageRepository, Mockito.times(1)).findByStageName(any());
        Mockito.verify(episodeStageRepository, Mockito.times(1)).getCurrentStageForEpisodeId(any());
        Mockito.verify(episodeStageRepository, Mockito.times(0)).save(any());
        Mockito.verify(episodeRepository, Mockito.times(0)).save(any());
    }

    @Test
    public void test_processStageTransitionResultWithSameNewStage() {
        Map<String, Object> stageTransitionResult = new HashMap<>();
        stageTransitionResult.put("ToStage", "Stage1");
        when(stageRepository.findByStageName(any())).thenReturn(getStage1());
        when(episodeStageRepository.getCurrentStageForEpisodeId(any())).thenReturn(new EpisodeStage(1L, 2L));
        episodeService.processStageTransitionResult(getEpisode_testCase2(), stageTransitionResult);

        Mockito.verify(stageRepository, Mockito.times(1)).findByStageName(any());
        Mockito.verify(episodeStageRepository, Mockito.times(1)).getCurrentStageForEpisodeId(any());
        Mockito.verify(episodeStageRepository, Mockito.times(0)).save(any());
        Mockito.verify(episodeRepository, Mockito.times(0)).save(any());
    }

    @Test
    public void test_processStageTransitionResult() {
        Map<String, Object> stageTransitionResult = new HashMap<>();
        stageTransitionResult.put("ToStage", "Stage1");
        when(stageRepository.findByStageName(any())).thenReturn(getStage1());
        when(episodeStageRepository.getCurrentStageForEpisodeId(any())).thenReturn(new EpisodeStage(1L, 1L));
        AtomicReference<Long> actualStageId = new AtomicReference<>();
        when(episodeRepository.save(any())).thenAnswer(i -> {
            Episode episode = i.getArgument(0);
            actualStageId.set(episode.getCurrentStageId());
            return episode;
        });

        episodeService.processStageTransitionResult(getEpisode_testCase2(), stageTransitionResult);

        Mockito.verify(stageRepository, Mockito.times(1)).findByStageName(any());
        Mockito.verify(episodeStageRepository, Mockito.times(1)).getCurrentStageForEpisodeId(any());
        Mockito.verify(episodeStageRepository, Mockito.times(2)).save(any());
        Mockito.verify(episodeRepository, Mockito.times(1)).save(any());
        assertEquals(getStage1().getId(), actualStageId.get());
    }

    @Test
    public void processStageTransitionResult_WithMoreData() {
        Map<String, Object> map = new HashMap<>();
        map.put("diagnosisBasis", "Chest X Ray");
        map.put("ToStage", "DIAGNOSED");

        when(episodeStageRepository.getCurrentStageForEpisodeId(any())).thenReturn(new EpisodeStage(1L, 1L));
        doReturn(new EpisodeDto()).when(episodeService).getEpisodeDetails(any(), any());
        when(stageRepository.findByStageName(any())).thenReturn(getStage1());

        doNothing().when(episodeService).processEpisodeStageDataForEpisodeAndRequestInput(anyMap(), any(), anyMap(), anyBoolean());
        episodeService.processStageTransitionResult(getEpisode_testCase2(), map);
    }

    private EpisodeDto getEpisodeDtoEntity_with_StageData_testCaseOne() {
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("duplicateStatus", "DELETED_AS_DUPLICATE");
        stageData.put("duplicateOf", "2");
        return new EpisodeDto(1L, 1L, 1L, true, "DSTB", 1L, null, Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, null, 1L,"PRESUMPTIVE_OPEN", "Presumptive Open", "IndiaTbPublic", 1L, null, null, null, null, stageData);
    }

    private EpisodeDto getEpisodeDtoEntity_with_StageData_testCaseTwo() {
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("duplicateStatus", "USER_CONFIRMED_DUPLICATE");
        stageData.put("duplicateOf", "2");
        return new EpisodeDto(1L, 1L, 1l, true, "DSTB", 1L, null, Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, null, 1L,"PRESUMPTIVE_OPEN", "Presumptive Open", "IndiaTbPublic", 1L, null, null, null, null, stageData);
    }

    private EpisodeDto getEpisodeDtoEntity_with_StageData_testCaseThree() {
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("duplicateStatus", "SYSTEM_IDENTIFIED_UNIQUE");
        stageData.put("duplicateOf", "2");
        return new EpisodeDto(1L, 1L, 1L, true, "DSTB", 1L, null, Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, null, 1L,"PRESUMPTIVE_OPEN", "Presumptive Open", "IndiaTbPublic", 1L, null, null, null, null, stageData);
    }

    private List<EpisodeDto> getEpisodeDtoEntityList_with_samePersonId() {
        Map<String, Object> stageData = new HashMap<>();
        List<EpisodeDto> episodeDtoList = new ArrayList<>();
        episodeDtoList.add(new EpisodeDto(1L, 1L, 1L, true, "DSTB", 1L, null, Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, null, 1L,"PRESUMPTIVE_OPEN", "Presumptive Open", "IndiaTbPublic", 1L, null, null, null, null, stageData));
        episodeDtoList.add(new EpisodeDto(2L, 1L, 1L, true, "DSTB", 1L, null, Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, null, 1L,"PRESUMPTIVE_OPEN", "Presumptive Open", "IndiaTbPublic", 1L, null, null, null, null, stageData));
        return episodeDtoList;
    }

    private Map<String, Object> getStageTransitionResult() {
        Map<String, Object> stageTransitionResult = new HashMap<>();
        stageTransitionResult.put("ToStage", "Stage1");
        return stageTransitionResult;
    }

    private Map<String, Object> getStageTransitionResult_withDeleteAndDuplicate() {
        Map<String, Object> stageTransitionResult = new HashMap<>();
        stageTransitionResult.put("ToStage", "Stage1");
        stageTransitionResult.put("notifiedStageIdList", "4,8,5,6,7");
        stageTransitionResult.put("duplicationScheme", "PRIMARYPHONE_GENDER");
        stageTransitionResult.put(RuleConstants.CHECK_DUPLICATES_AND_DELETE, true);
        return stageTransitionResult;
    }

    private Map<String, Object> getStageTransitionResult_withCheckAndUpdateDuplicate() {
        Map<String, Object> stageTransitionResult = new HashMap<>();
        stageTransitionResult.put("ToStage", "Stage1");
        stageTransitionResult.put("notifiedStageIdList", "4,8,5,6,7");
        stageTransitionResult.put("duplicationScheme", "PRIMARYPHONE_GENDER");
        stageTransitionResult.put(RuleConstants.CHECK_DUPLICATES_AND_UPDATE, true);
        return stageTransitionResult;
    }

    private Episode getEpisode_testCase1() {
        return new Episode(1L, 1L, 1L, false, null, "1", Utils.getCurrentDateNew(), Utils.getCurrentDateNew(),
                null, Utils.getCurrentDateNew(), null, 1L, null, 1L);
    }

    private Episode getEpisode_testCase1_withoutCurrentStageId() {
        return new Episode(1L, 1L, 1L, false, 1L, "1,2,3", Utils.getCurrentDateNew(), Utils.getCurrentDateNew(),
                null, Utils.getCurrentDateNew(), null, null, null, 1L);
    }

    private Episode getEpisode_testCase2() {
        return new Episode(1L, 1L, 1L, false, 1L, "1,2,3", Utils.getCurrentDateNew(), Utils.getCurrentDateNew(),
                null, Utils.getCurrentDateNew(), null, 2L, null, 1L);
    }

    private Episode getEpisode_testCase4() {
        return new Episode(2L, 1L, 2L, false, 1L, "1,2,3", Utils.getCurrentDateNew(), Utils.getCurrentDateNew(),
                null, Utils.getCurrentDateNew(), null, 2L, null, 1L);
    }

    private EpisodeStage getEpisodeStage_testCase1() {
        return new EpisodeStage(1L, 1L, 1L, Utils.getCurrentDateNew(), null);
    }

    private List<EpisodeStage> getEpisodeStageList_testCase1() {
        return Collections.singletonList(getEpisodeStage_testCase1());
    }

    private Stage getStage0() {
        return new Stage(1L, "Stage0", null, "Stage0");
    }

    private Stage getStage1() {
        return new Stage(2L, "Stage1", null, "Stage1");
    }

    private List<EpisodeStage> getEpisodeStage_testCase2() {
        EpisodeStage episodeStage = new EpisodeStage(1L, 1L, 1L, Utils.getCurrentDateNew().minusDays(2L), Utils.getCurrentDateNew());
        EpisodeStage episodeStage2 = new EpisodeStage(2L, 1L, 2L, Utils.getCurrentDateNew(), null);
        return Arrays.asList(episodeStage, episodeStage2);
    }

    private List<Stage> getStageList_testCase1() {
        return Collections.singletonList(getStage0());
    }

    private List<Stage> getStageList_testCase2() {
        Stage stage = new Stage(1L, "Stage1", null, "Stage1");
        Stage stage2 = new Stage(2L, "Stage2", null, "Stage2");
        return Arrays.asList(stage, stage2);
    }

    private List<EpisodeStageData> getEpisodeStageDataList_testCase1() {
        List<EpisodeStageData> episodeStageDataList = new ArrayList<>();
        episodeStageDataList.add(new EpisodeStageData(1L, 1L, 1L, "Field1Value"));
        episodeStageDataList.add(new EpisodeStageData(2L, 1L, 2L, "Field2Value"));
        episodeStageDataList.add(new EpisodeStageData(3L, 1L, 3L, "Field3Value"));
        episodeStageDataList.add(new EpisodeStageData(4L, 1L, 4L, "Field4Value"));
        return episodeStageDataList;
    }

    private List<EpisodeStageData> getEpisodeStageDataList_testCase2() {
        List<EpisodeStageData> episodeStageDataList = new ArrayList<>();
        episodeStageDataList.add(new EpisodeStageData(1L, 1L, 1L, "Field1Value"));
        episodeStageDataList.add(new EpisodeStageData(2L, 1L, 2L, "Field2Value"));
        episodeStageDataList.add(new EpisodeStageData(3L, 1L, 3L, "Field3Value"));
        episodeStageDataList.add(new EpisodeStageData(4L, 1L, 4L, "Field4Value"));

        episodeStageDataList.add(new EpisodeStageData(5L, 2L, 1L, "Field1Value2"));
        episodeStageDataList.add(new EpisodeStageData(6L, 2L, 3L, "Field3Value2"));
        episodeStageDataList.add(new EpisodeStageData(7L, 2L, 4L, "Field4Value2"));
        return episodeStageDataList;
    }

    private Map<Long, List<DiseaseStageFieldMapDto>> getDefinedStageIdKeyMappingsListMap_testCase1() {
        Map<Long, List<DiseaseStageFieldMapDto>> longListMap = new HashMap<>();
        List<DiseaseStageFieldMapDto> list = new ArrayList<>();
        list.add(new DiseaseStageFieldMapDto(1L, 1L, 1L, 1L, 1L, "Field1", "Module1", true, false, "DefaultValue1"));
        list.add(new DiseaseStageFieldMapDto(2L, 1L, 1L, 1L, 2L, "Field2", "IAM", true, false, "DefaultValue2"));
        list.add(new DiseaseStageFieldMapDto(3L, 1L, 1L, 1L, 3L, "Field3", "Module3", true, false, "DefaultValue3"));
        list.add(new DiseaseStageFieldMapDto(4L, 1L, 1L, 1L, 4L, "Field4", "Module4", true, false, "DefaultValue4"));
        longListMap.put(1L, list);
        return longListMap;
    }

    private Map<Long, List<DiseaseStageFieldMapDto>> getDefinedStageIdKeyMappingsListMap_testCase2() {
        Map<Long, List<DiseaseStageFieldMapDto>> longListMap = new HashMap<>();
        List<DiseaseStageFieldMapDto> list = new ArrayList<>();
        list.add(new DiseaseStageFieldMapDto(1L, 1L, 1L, 1L, 1L, "Field1", "Module1", true, false, "DefaultValue1"));
        list.add(new DiseaseStageFieldMapDto(2L, 1L, 1L, 1L, 2L, "Field2", "Module2", true, false, "DefaultValue2"));
        list.add(new DiseaseStageFieldMapDto(3L, 1L, 1L, 1L, 3L, "Field3", "Module3", true, false, "DefaultValue3"));
        list.add(new DiseaseStageFieldMapDto(4L, 1L, 1L, 1L, 4L, "Field4", "Module4", true, false, "DefaultValue4"));
        longListMap.put(1L, list);

        list = new ArrayList<>();
        list.add(new DiseaseStageFieldMapDto(1L, 2L, 1L, 2L, 5L, "Field1", "Module1", true, false, "DefaultValue1"));
        list.add(new DiseaseStageFieldMapDto(3L, 2L, 1L, 2L, 7L, "Field3", "Module3", true, false, "DefaultValue3"));
        list.add(new DiseaseStageFieldMapDto(4L, 2L, 1L, 2L, 8L, "Field4", "Module4", true, false, "DefaultValue4"));
        longListMap.put(2L, list);

        return longListMap;
    }

    private List<SupportedRelation> getAllSupportedRelation() {
        List<SupportedRelation> supportedRelations = new ArrayList<>();
        supportedRelations.add(new SupportedRelation(1L, "SR1", "SRDescription1", SupportedRelationType.ASSOCIATION));
        supportedRelations.add(new SupportedRelation(2L, "SR2", "SRDescription2", SupportedRelationType.ASSOCIATION));
        supportedRelations.add(new SupportedRelation(3L, "SR3", "SRDescription3", SupportedRelationType.ASSOCIATION));
        supportedRelations.add(new SupportedRelation(4L, "SR4", "SRDescription4", SupportedRelationType.HIERARCHY));
        supportedRelations.add(new SupportedRelation(5L, "SR5", "SRDescription5", SupportedRelationType.HIERARCHY));
        supportedRelations.add(new SupportedRelation(6L, "SR6", "SRDescription6", SupportedRelationType.HIERARCHY));
        supportedRelations.add(new SupportedRelation(7L, "current", "current", SupportedRelationType.HIERARCHY));
        return supportedRelations;
    }

    private List<SupportedRelation> getAllSupportedRelationLowerCase() {
        List<SupportedRelation> supportedRelations = new ArrayList<>();
        supportedRelations.add(new SupportedRelation(1L, "sR1", "SRDescription1", SupportedRelationType.ASSOCIATION));
        supportedRelations.add(new SupportedRelation(2L, "sR2", "SRDescription2", SupportedRelationType.ASSOCIATION));
        supportedRelations.add(new SupportedRelation(3L, "sR3", "SRDescription3", SupportedRelationType.ASSOCIATION));
        supportedRelations.add(new SupportedRelation(4L, "sR4", "SRDescription4", SupportedRelationType.HIERARCHY));
        supportedRelations.add(new SupportedRelation(5L, "sR5", "SRDescription5", SupportedRelationType.HIERARCHY));
        supportedRelations.add(new SupportedRelation(6L, "sR6", "SRDescription6", SupportedRelationType.HIERARCHY));
        supportedRelations.add(new SupportedRelation(7L, "current", "current", SupportedRelationType.HIERARCHY));
        supportedRelations.add(new SupportedRelation(8L, "enrollment", "enrollment", SupportedRelationType.HIERARCHY));
        return supportedRelations;
    }

    private List<EpisodeAssociation> getEpisodeAssociations_testCase1() {
        List<EpisodeAssociation> episodeAssociations = new ArrayList<>();
        episodeAssociations.add(new EpisodeAssociation(1L, 1L, 1L, "AssociationValue1"));
        episodeAssociations.add(new EpisodeAssociation(2L, 1L, 2L, "AssociationValue2"));
        episodeAssociations.add(new EpisodeAssociation(3L, 1L, 3L, "AssociationValue3"));
        return episodeAssociations;
    }

    private List<EpisodeHierarchyLinkage> getEpisodeHierarchyLinkages_testCase1() {
        List<EpisodeHierarchyLinkage> episodeHierarchyLinkages = new ArrayList<>();
        episodeHierarchyLinkages.add(new EpisodeHierarchyLinkage(1L, 1L, 4L, 21L));
        episodeHierarchyLinkages.add(new EpisodeHierarchyLinkage(2L, 1L, 5L, 22L));
        episodeHierarchyLinkages.add(new EpisodeHierarchyLinkage(3L, 1L, 6L, 23L));
        return episodeHierarchyLinkages;
    }

    private Map<Long, List<TabPermissionDto>> getStageToTabPermissions_testCase1() {
        Map<Long, List<TabPermissionDto>> map = new HashMap<>();
        map.put(1L, getTabPermissions_6Tabs());
        return map;
    }

    private Map<Long, List<TabPermissionDto>> getStageToTabPermissions_testCase2() {
        Map<Long, List<TabPermissionDto>> map = new HashMap<>();
        map.put(1L, getTabPermissions_6Tabs());
        map.put(2L, getTabPermissions_6Tabs());
        return map;
    }

    private List<TabPermissionDto> getTabPermissions_6Tabs() {
        List<TabPermissionDto> list = new ArrayList<>();
        list.add(new TabPermissionDto(1L, 1L, "Tab1", "_tab1", true, true, true, true, 1L));
        list.add(new TabPermissionDto(1L, 2L, "Tab2", "_tab2", true, true, true, true, 2L));
        list.add(new TabPermissionDto(1L, 3L, "Tab3", "_tab3", true, true, true, true, 3L));
        list.add(new TabPermissionDto(1L, 4L, "Tab4", "_tab4", true, true, true, true, 4L));
        list.add(new TabPermissionDto(1L, 5L, "Tab5", "_tab5", true, true, true, true, 5L));
        return list;
    }

    private Map<String, Object> getFetchDuplicateRequestOne() {
        Map<String, Object> fetchDuplicateMap = new HashMap<>();
        fetchDuplicateMap.put("offset", 0L);
        fetchDuplicateMap.put("count", 10L);
        fetchDuplicateMap.put("scheme", "PRIMARYPHONE_GENDER");
        fetchDuplicateMap.put("primaryPhoneNumber", "9988799887" );
        fetchDuplicateMap.put("gender", "Male");
        return fetchDuplicateMap;
    }

    private Map<Long, DiseaseTemplate> getDiseaseIdTemplateMap_testCase1() {
        Map<Long, DiseaseTemplate> map = new HashMap<>();
        map.put(1L, new DiseaseTemplate(1L, "DSTB", 29L, false, "TB"));
        map.put(2L, new DiseaseTemplate(2L, "DRTB", 29L, false, "TB"));
        map.put(3L, new DiseaseTemplate(3L, "LTBI", 29L, false, "TB"));
        return map;
    }

    @Test
    public void test_getEpisodeDetails_stageDataValueForMultipleRepeatedStages() {
        Long episodeId = 1L;
        Long clientId = 1L;
        when(episodeRepository.findByIdAndClientIdAndDeletedIs(any(), any(), any())).thenReturn(getEpisode_testCase3());
        when(episodeStageRepository.getEpisodeStagesByEpisodeIdInOrderByStartDateAsc(any())).thenReturn(getEpisodeStage_testCase3());
        when(stageRepository.findAllById(any())).thenReturn(getStageList_testCase2());
        when(episodeStageDataRepository.findAllByEpisodeStageIdIn(any())).thenReturn(getEpisodeStageDataList_testCase3());
        when(diseaseTemplateService.getDefinedStageIdKeyMappingsListMap(any(), any())).thenReturn(getDefinedStageIdKeyMappingsListMap_testCase2());
        when(supportedRelationRepository.findAll()).thenReturn(getAllSupportedRelation());
        when(episodeAssociationRepository.getAllByEpisodeIdIn(any())).thenReturn(getEpisodeAssociations_testCase1());
        when(episodeHierarchyLinkageRepository.getAllByEpisodeIdIn(any())).thenReturn(getEpisodeHierarchyLinkages_testCase1());
        when(diseaseTemplateService.getStageToTabPermissionsListMapForStageIds(any(), any(), any())).thenReturn(getStageToTabPermissions_testCase2());
        when(diseaseTemplateService.getDiseaseIdTemplateMap(Collections.singletonList(any()))).thenReturn(getDiseaseIdTemplateMap_testCase1());
        when(stageRepository.findById(any())).thenReturn(Optional.of(getStage0()));
        when(episodeTagService.getEpisodeTags(eq(episodeId))).thenReturn(new EpisodeTagResponse(episodeId, new ArrayList<>(), Arrays.asList("hakuna", "matata")));
        EpisodeDto episode = episodeService.getEpisodeDetails(episodeId, clientId);


        Map<String, String> expectedFieldKeyValueMap = new HashMap<>();
        expectedFieldKeyValueMap.put("Field1", "Field1Value3");
        expectedFieldKeyValueMap.put("Field2", "Field2Value3");
        expectedFieldKeyValueMap.put("Field3", "Field3Value2");
        expectedFieldKeyValueMap.put("Field4", null);
        for(Map.Entry<String, String> entry : expectedFieldKeyValueMap.entrySet()) {
            assertEquals(entry.getValue(), episode.getStageData().get(entry.getKey()));
        }
    }

    private Episode getEpisode_testCase3() {
        return new Episode(1L, 1L, 1L, false, 1L, "1,2,3", Utils.convertStringToDateNew(Utils.getFormattedDateNew(Utils.getCurrentDateNew())), Utils.getCurrentDateNew(),
                null, Utils.getCurrentDateNew(), null, 1L, null, 1L);
    }

    private List<EpisodeStage> getEpisodeStage_testCase3() {
        EpisodeStage episodeStage  = new EpisodeStage(1L, 1L, 1L, Utils.getCurrentDateNew().minusDays(4L), Utils.getCurrentDateNew().minusDays(3));
        EpisodeStage episodeStage2 = new EpisodeStage(2L, 1L, 2L, Utils.getCurrentDateNew().minusDays(3), Utils.getCurrentDateNew().minusDays(2));
        EpisodeStage episodeStage3 = new EpisodeStage(3L, 1L, 1L, Utils.getCurrentDateNew().minusDays(2), null);
        return Arrays.asList(episodeStage, episodeStage2, episodeStage3);
    }

    private List<EpisodeStageData> getEpisodeStageDataList_testCase3() {
        List<EpisodeStageData> episodeStageDataList = new ArrayList<>();
        episodeStageDataList.add(new EpisodeStageData(1L, 1L, 1L, "Field1Value"));
        episodeStageDataList.add(new EpisodeStageData(2L, 1L, 2L, "Field2Value"));
        episodeStageDataList.add(new EpisodeStageData(3L, 1L, 3L, "Field3Value"));
        episodeStageDataList.add(new EpisodeStageData(4L, 1L, 4L, "Field4Value"));

        episodeStageDataList.add(new EpisodeStageData(5L, 2L, 1L, "Field1Value2"));
        episodeStageDataList.add(new EpisodeStageData(6L, 2L, 3L, "Field3Value2"));
        episodeStageDataList.add(new EpisodeStageData(7L, 2L, 4L, "Field4Value2"));

        episodeStageDataList.add(new EpisodeStageData(5L, 3L, 2L, "Field2Value3"));
        episodeStageDataList.add(new EpisodeStageData(6L, 3L, 1L, "Field1Value3"));
        episodeStageDataList.add(new EpisodeStageData(7L, 3L, 4L, null));
        return episodeStageDataList;
    }

    @Test(expected = NotFoundException.class)
    public void test_getEpisodesForPerson_throwsNotFoundException() {
        Long personId = 1L;
        Long clientId = 1L;
        List<Long> personIdList = new ArrayList<>();
        personIdList.add(personId);
        when(episodeRepository.findAllByPersonIdInAndClientIdAndDeletedFalse(eq(personIdList), eq(clientId))).thenReturn(new ArrayList<>());
        episodeService.getEpisodesForPersonList(personIdList, clientId, true);
    }

    @Test
    public void test_getEpisodesForPerson_withTwoEpisodes() {
        Long personId = 1L;
        Long clientId = 1L;
        List<Long> personIdList = new ArrayList<>();
        personIdList.add(personId);
        List<Episode> episodes = getEpisodesList_countTwo();
        when(episodeRepository.findAllByPersonIdInAndClientIdAndDeletedFalse(eq(personIdList), eq(clientId))).thenReturn(getEpisodesList_countTwo());
        when(stageRepository.findAllById(any())).thenReturn(getStageList_testCase2()).thenReturn(getStageList_testCase2());
        when(episodeStageRepository.getEpisodeStagesByEpisodeIdInOrderByStartDateAsc(any())).thenReturn(getEpisodeStageMappingsForTwoEpisodes());
        when(diseaseTemplateService.getDefinedStageIdKeyMappingsListMap(any(), any())).thenReturn(getDefinedStageIdKeyMappingsListMap_testCase3());
        when(episodeStageDataRepository.findAllByEpisodeStageIdIn(any())).thenReturn(getEpisodeStageDataListForTwoEpisodes());
        when(episodeAssociationRepository.getAllByEpisodeIdIn(any())).thenReturn(getEpisodeAssociationsForTwoEpisodes());
        when(episodeHierarchyLinkageRepository.getAllByEpisodeIdIn(any())).thenReturn(getEpisodeHierarchyLinkagesForTwoEpisodes());
        when(diseaseTemplateService.getStageToTabPermissionsListMapForStageIds(any(), any(), any())).thenReturn(getStageToTabPermissions_testCase2());
        when(diseaseTemplateService.getDiseaseIdTemplateMap(Collections.singletonList(any()))).thenReturn(getDiseaseIdTemplateMap_testCase1());
        when(episodeTagService.getEpisodeTags(eq(episodes.get(0).getId()))).thenReturn(new EpisodeTagResponse(episodes.get(0).getId()
                , new ArrayList<>(), Arrays.asList("hakuna", "matata")));
        when(episodeTagService.getEpisodeTags(eq(episodes.get(1).getId()))).thenReturn(new EpisodeTagResponse(episodes.get(1).getId()
                , new ArrayList<>(), Arrays.asList("hakuna1", "matata2")));
        when(supportedRelationRepository.findAll()).thenReturn(getAllSupportedRelation());

        List<EpisodeDto> episodeDtos = episodeService.getEpisodesForPersonList(personIdList, clientId, true);

        List<String> associationKeys = Arrays.asList("SR1", "SR2", "SR3");
        Map<Long, List<String>> episodeIdToAssociationKeysMap = new HashMap<>();
        episodeIdToAssociationKeysMap.put(1L, Arrays.asList("AssociationValue1", "AssociationValue1", "AssociationValue1"));
        episodeIdToAssociationKeysMap.put(2L, Arrays.asList("AssociationValue1E2", "AssociationValue1E2", "AssociationValue1E2"));

        List<String> hierarchyMappingKeys = Arrays.asList("SR4", "SR5", "SR6");
        Map<Long, List<Long>> episodeIdToHierarchyMappings = new HashMap<>();
        episodeIdToHierarchyMappings.put(1L, Arrays.asList(11L, 12L, 13L));
        episodeIdToHierarchyMappings.put(2L, Arrays.asList(21L, 22L, 23L));

        HashSet<String> tabNames = new HashSet<>(Arrays.asList("Tab1", "Tab2", "Tab3", "Tab4", "Tab5"));

        Map<Long, Map<String, String>> episodeIdToExpectedFieldKeyValueMap = new HashMap<>();
        Map<String, String> expectedFieldKeyValueMapE1 = new HashMap<>();
        expectedFieldKeyValueMapE1.put("Field1", "Field1Value3");
        expectedFieldKeyValueMapE1.put("Field2", "Field2Value3");
        expectedFieldKeyValueMapE1.put("Field3", "Field3Value2");
        expectedFieldKeyValueMapE1.put("Field4", "Field4Value3");
        //Expect default value for Field5 when Field5 is not part of existing Episode Stage data.
        expectedFieldKeyValueMapE1.put("Field5", "DefaultValue5");
        expectedFieldKeyValueMapE1.put("Field6", "DefaultValue6");
        expectedFieldKeyValueMapE1.put("Field7", "DefaultValue7V2");
        episodeIdToExpectedFieldKeyValueMap.put(1L, expectedFieldKeyValueMapE1);

        Map<String, String> expectedFieldKeyValueMapE2 = new HashMap<>();
        expectedFieldKeyValueMapE2.put("Field1", "Field1Value3E2");
        expectedFieldKeyValueMapE2.put("Field2", "Field2Value2E2");
        expectedFieldKeyValueMapE2.put("Field3", "Field3Value3E2");
        expectedFieldKeyValueMapE2.put("Field4", "Field4ValueE2");
        episodeIdToExpectedFieldKeyValueMap.put(2L, expectedFieldKeyValueMapE2);

        for(EpisodeDto episode : episodeDtos) {
            for(String key : associationKeys) {
                assertTrue(episode.getAssociations().containsKey(key));
            }
            for(String value : episodeIdToAssociationKeysMap.get(episode.getId())) {
                assertTrue(episode.getAssociations().containsValue(value));
            }
            for(String key : hierarchyMappingKeys) {
                assertTrue(episode.getHierarchyMappings().containsKey(key));
            }
            for(Long value : episodeIdToHierarchyMappings.get(episode.getId())) {
                assertTrue(episode.getHierarchyMappings().containsValue(value));
            }
            Map<String, TabPermissionDto> tabNameToEntityMap = getStageToTabPermissions_testCase2().get(episode.getCurrentStageId())
                    .stream().collect(Collectors.toMap(TabPermissionDto::getTabName, a -> a));
            for(TabPermissionDto tabPermissionDto : episode.getTabPermissions()) {
                assertTrue(tabNames.contains(tabPermissionDto.getTabName()));
                Assert.assertEquals(tabPermissionDto.isAdd(), tabNameToEntityMap.get(tabPermissionDto.getTabName()).isAdd());
                Assert.assertEquals(tabPermissionDto.isDelete(), tabNameToEntityMap.get(tabPermissionDto.getTabName()).isDelete());
                Assert.assertEquals(tabPermissionDto.isEdit(), tabNameToEntityMap.get(tabPermissionDto.getTabName()).isEdit());
                Assert.assertEquals(tabPermissionDto.isView(), tabNameToEntityMap.get(tabPermissionDto.getTabName()).isView());
            }
            for(Map.Entry<String, String> entry : episodeIdToExpectedFieldKeyValueMap.get(episode.getId()).entrySet()) {
                assertEquals(entry.getValue(), episode.getStageData().get(entry.getKey()));
            }
        }
    }

    private List<Episode> getEpisodesList_countTwo() {
        Episode episode1 = new Episode(1L, 1L, 1L, false, 1L, "1,2,3", Utils.getCurrentDateNew(), Utils.getCurrentDateNew(),
                null, Utils.getCurrentDateNew(), null, 1L, null, 1L);
        Episode episode2 = new Episode(2L, 1L, 1L, false, 2L, "1,2,3", Utils.getCurrentDateNew(), Utils.getCurrentDateNew(),
                null, Utils.getCurrentDateNew(), null, 2L, null, 1L);
        return Arrays.asList(episode1, episode2);
    }

    private List<EpisodeStage> getEpisodeStageMappingsForTwoEpisodes() {
        EpisodeStage episodeStage  = new EpisodeStage(1L, 1L, 1L, Utils.getCurrentDateNew().minusDays(4L), Utils.getCurrentDateNew().minusDays(3));
        EpisodeStage episodeStage2 = new EpisodeStage(3L, 1L, 2L, Utils.getCurrentDateNew().minusDays(3), Utils.getCurrentDateNew().minusDays(2));
        EpisodeStage episodeStage3 = new EpisodeStage(5L, 1L, 1L, Utils.getCurrentDateNew().minusDays(2), null);

        EpisodeStage episodeStage4  = new EpisodeStage(2L, 2L, 2L, Utils.getCurrentDateNew().minusDays(4L), Utils.getCurrentDateNew().minusDays(3));
        EpisodeStage episodeStage5  = new EpisodeStage(4L, 2L, 1L, Utils.getCurrentDateNew().minusDays(3), Utils.getCurrentDateNew().minusDays(2));
        EpisodeStage episodeStage6  = new EpisodeStage(6L, 2L, 2L, Utils.getCurrentDateNew().minusDays(2), null);
        return Arrays.asList(episodeStage, episodeStage2, episodeStage3, episodeStage4, episodeStage5, episodeStage6);
    }

    private Map<Long, List<DiseaseStageFieldMapDto>> getDefinedStageIdKeyMappingsListMap_testCase3() {
        Map<Long, List<DiseaseStageFieldMapDto>> longListMap = new HashMap<>();
        List<DiseaseStageFieldMapDto> list = new ArrayList<>();
        list.add(new DiseaseStageFieldMapDto(1L, 1L, 1L, 1L, 1L, "Field1", "Module1", true, false, "DefaultValue1"));
        list.add(new DiseaseStageFieldMapDto(2L, 1L, 1L, 1L, 2L, "Field2", "Module2", true, false, "DefaultValue2"));
        list.add(new DiseaseStageFieldMapDto(3L, 1L, 1L, 1L, 3L, "Field3", "Module3", true, false, "DefaultValue3"));
        list.add(new DiseaseStageFieldMapDto(4L, 1L, 1L, 1L, 4L, "Field4", "Module4", true, false, "DefaultValue4"));
        list.add(new DiseaseStageFieldMapDto(5L, 1L, 1L, 1L, 5L, "Field5", "Module5", true, false, "DefaultValue5"));
        list.add(new DiseaseStageFieldMapDto(6L, 1L, 1L, 1L, 6L, "Field6", "Module6", true, false, "DefaultValue6"));

        list.add(new DiseaseStageFieldMapDto(1L, 1L, 2L, 3L, 7L, "Field1", "Module1", true, false, "DefaultValue1E2"));
        list.add(new DiseaseStageFieldMapDto(2L, 1L, 2L, 3L, 8L, "Field2", "Module2", true, false, "DefaultValue2E2"));
        list.add(new DiseaseStageFieldMapDto(3L, 1L, 2L, 3L, 9L, "Field3", "Module3", true, false, "DefaultValue3E2"));
        longListMap.put(1L, list);

        list = new ArrayList<>();
        list.add(new DiseaseStageFieldMapDto(1L, 2L, 1L, 2L, 10L, "Field1", "Module1", true, false, "DefaultValue1"));
        list.add(new DiseaseStageFieldMapDto(3L, 2L, 1L, 2L, 11L, "Field3", "Module3", true, false, "DefaultValue3"));
        list.add(new DiseaseStageFieldMapDto(4L, 2L, 1L, 2L, 12L, "Field4", "Module4", true, false, "DefaultValue4"));
        list.add(new DiseaseStageFieldMapDto(5L, 2L, 1L, 2L, 13L, "Field5", "Module5", true, false, "DefaultValue5V2"));
        list.add(new DiseaseStageFieldMapDto(7L, 2L, 1L, 2L, 14L, "Field7", "Module7", true, false, "DefaultValue7V2"));

        list.add(new DiseaseStageFieldMapDto(1L, 2L, 2L, 4L, 15L, "Field1", "Module1", true, false, "DefaultValue1E2"));
        list.add(new DiseaseStageFieldMapDto(2L, 2L, 2L, 3L, 16L, "Field2", "Module2", true, false, "DefaultValue2E2"));
        list.add(new DiseaseStageFieldMapDto(3L, 2L, 2L, 4L, 17L, "Field3", "Module3", true, false, "DefaultValue3E2"));
        list.add(new DiseaseStageFieldMapDto(4L, 2L, 2L, 4L, 18L, "Field4", "Module4", true, false, "DefaultValue4E2"));
        longListMap.put(2L, list);

        return longListMap;
    }

    private List<EpisodeStageData> getEpisodeStageDataListForTwoEpisodes() {
        List<EpisodeStageData> episodeStageDataList = new ArrayList<>();
        episodeStageDataList.add(new EpisodeStageData(1L, 1L, 1L, "Field1Value"));
        episodeStageDataList.add(new EpisodeStageData(2L, 1L, 2L, "Field2Value"));
        episodeStageDataList.add(new EpisodeStageData(3L, 1L, 3L, "Field3Value"));
        episodeStageDataList.add(new EpisodeStageData(4L, 1L, 4L, "Field4Value"));

        episodeStageDataList.add(new EpisodeStageData(5L, 3L, 1L, "Field1Value2"));
        episodeStageDataList.add(new EpisodeStageData(6L, 3L, 3L, "Field3Value2"));
        episodeStageDataList.add(new EpisodeStageData(7L, 3L, 4L, "Field4Value2"));

        episodeStageDataList.add(new EpisodeStageData( 8L, 5L, 2L, "Field2Value3"));
        episodeStageDataList.add(new EpisodeStageData( 9L, 5L, 1L, "Field1Value3"));
        episodeStageDataList.add(new EpisodeStageData(10L, 5L, 4L, "Field4Value3"));

        episodeStageDataList.add(new EpisodeStageData(11L, 2L, 1L, "Field1ValueE2"));
        episodeStageDataList.add(new EpisodeStageData(12L, 2L, 2L, "Field2ValueE2"));
        episodeStageDataList.add(new EpisodeStageData(13L, 2L, 4L, "Field4ValueE2"));

        episodeStageDataList.add(new EpisodeStageData(14L, 4L, 1L, "Field1Value2E2"));
        episodeStageDataList.add(new EpisodeStageData(15L, 4L, 2L, "Field2Value2E2"));
        episodeStageDataList.add(new EpisodeStageData(16L, 4L, 3L, "Field3Value2E2"));

        episodeStageDataList.add(new EpisodeStageData(17L, 6L, 1L, "Field1Value3E2"));
        episodeStageDataList.add(new EpisodeStageData(18L, 6L, 3L, "Field3Value3E2"));
        return episodeStageDataList;
    }

    private List<EpisodeAssociation> getEpisodeAssociationsForTwoEpisodes() {
        List<EpisodeAssociation> episodeAssociations = new ArrayList<>();
        episodeAssociations.add(new EpisodeAssociation(1L, 1L, 1L, "AssociationValue1"));
        episodeAssociations.add(new EpisodeAssociation(2L, 1L, 2L, "AssociationValue2"));
        episodeAssociations.add(new EpisodeAssociation(3L, 1L, 3L, "AssociationValue3"));

        episodeAssociations.add(new EpisodeAssociation(4L, 2L, 1L, "AssociationValue1E2"));
        episodeAssociations.add(new EpisodeAssociation(5L, 2L, 2L, "AssociationValue2E2"));
        episodeAssociations.add(new EpisodeAssociation(6L, 2L, 3L, "AssociationValue3E2"));
        return episodeAssociations;
    }

    private List<EpisodeHierarchyLinkage> getEpisodeHierarchyLinkagesForTwoEpisodes() {
        List<EpisodeHierarchyLinkage> episodeHierarchyLinkages = new ArrayList<>();
        episodeHierarchyLinkages.add(new EpisodeHierarchyLinkage(1L, 1L, 4L, 11L));
        episodeHierarchyLinkages.add(new EpisodeHierarchyLinkage(2L, 1L, 5L, 12L));
        episodeHierarchyLinkages.add(new EpisodeHierarchyLinkage(3L, 1L, 6L, 13L));

        episodeHierarchyLinkages.add(new EpisodeHierarchyLinkage(4L, 2L, 4L, 21L));
        episodeHierarchyLinkages.add(new EpisodeHierarchyLinkage(5L, 2L, 5L, 22L));
        episodeHierarchyLinkages.add(new EpisodeHierarchyLinkage(6L, 2L, 6L, 23L));
        return episodeHierarchyLinkages;
    }

    @Test
    public void test_getEpisodesForPersonIncludeDetailsFalse() {
        Long personId = 1L;
        Long clientId = 1L;
        List<Long> personIdList = new ArrayList<>();
        personIdList.add(personId);
        List<Episode> episodes = getEpisodesList_countOne();
        when(episodeRepository.findAllByPersonIdInAndClientIdAndDeletedFalse(eq(personIdList), eq(clientId))).thenReturn(episodes);
        when(episodeTagService.getEpisodeTags(eq(episodes.get(0).getId()))).thenReturn(new EpisodeTagResponse(episodes.get(0).getId(), new ArrayList<>(), Arrays.asList("hakuna", "matata")));
        List<EpisodeDto> episodeDtos = episodeService.getEpisodesForPersonList(personIdList, clientId, false);

        Mockito.verify(stageRepository, Mockito.times(1)).findAllById(any());
        Mockito.verify(episodeStageRepository, Mockito.times(0)).getEpisodeStagesByEpisodeIdInOrderByStartDateAsc(any());
        Mockito.verify(diseaseTemplateService, Mockito.times(0)).getDefinedStageIdKeyMappingsListMap(any(), any());
        Mockito.verify(episodeStageDataRepository, Mockito.times(0)).findAllByEpisodeStageIdIn(any());
        Mockito.verify(episodeAssociationRepository, Mockito.times(0)).getAllByEpisodeIdIn(any());
        Mockito.verify(episodeHierarchyLinkageRepository, Mockito.times(0)).getAllByEpisodeIdIn(any());
        Mockito.verify(diseaseTemplateService, Mockito.times(0)).getStageToTabPermissionsListMapForStageIds(any(), any(), any());
        Mockito.verify(diseaseTemplateService, Mockito.times(0)).getDiseaseIdTemplateMap(Collections.singletonList(any()));
        Mockito.verify(supportedRelationRepository, Mockito.times(0)).findAll();
    }

    private List<Episode> getEpisodesList_countOne() {
        Episode episode1 = new Episode(1L, 1L, 1L, false, 1L, "1,2,3", Utils.getCurrentDateNew(),
                null, Utils.getCurrentDateNew(), null, null, 1L, null, 1L);
        return Collections.singletonList(episode1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_dateTimeParseExxceptionThrowForDateParsing() {
        Episode testEpisode = new Episode(1L, 1L, 1L, false, 1L, "1,2,3", Utils.convertStringToDateNew("12-12-2022"), Utils.getCurrentDateNew(),
                null, Utils.getCurrentDateNew(), null, 1L, null, 1L);
    }

    @Test
    public void test_processEpisodeRequestInputForStageTransition_processWithoutExceptionWithWriteToElastic() {
        Long episodeId = 1L;
        Long clientId = 1L;
        Map<String, Object> requestInput = new HashMap<>();
        requestInput.put(FieldConstants.EPISODE_FIELD_EPISODE_ID, episodeId);
        requestInput.put(FieldConstants.EPISODE_FIELD_TYPE_OF_EPISODE, "type1");
        requestInput.put(FieldConstants.EPISODE_FIELD_START_DATE, "2021-11-21 00:00:00");
        requestInput.put(FieldConstants.EPISODE_FIELD_RISK_STATUS, "Status1");
        requestInput.put("SR1", "AssociationValue1New");
        requestInput.put("SR4", 33L);
        requestInput.put("Field1", "Field1ValueNew");

        Map<String, Object> stageTransitionOutput = new HashMap<>();
        stageTransitionOutput.put("ToStage", "Diagnosed_Not_On_Treatment");
        doReturn(getEpisodeDtoEntity_testCase1()).when(episodeService).getEpisodeDetails(any(), any());
        doReturn(getEpisode_testCase2()).when(episodeService).processUpdateEpisode(any(), any(),anyBoolean());
        when(stageTransitionService.stageTransitionHandler(any(), any())).thenReturn(new EventStreamingDto<>("#",stageTransitionOutput));
        doNothing().when(adherenceService).updateEntity(any(), any(), any(), any());
        episodeService.processEpisodeRequestInputForStageTransition(clientId, requestInput);
    }
    @Test
    public void test_processEpisodeRequestInputForStageTransition_processWithoutExceptionWithoutWriteToElastic() {
        Long episodeId = 1L;
        Long clientId = 1L;
        Map<String, Object> requestInput = new HashMap<>();
        requestInput.put(FieldConstants.EPISODE_FIELD_EPISODE_ID, episodeId);
        requestInput.put(FieldConstants.EPISODE_FIELD_TYPE_OF_EPISODE, "type1");
        requestInput.put(FieldConstants.EPISODE_FIELD_START_DATE, "2021-11-21 00:00:00");
        requestInput.put(FieldConstants.EPISODE_FIELD_RISK_STATUS, "Status1");
        requestInput.put("SR1", "AssociationValue1New");
        requestInput.put("SR4", 33L);
        requestInput.put("Field1", "Field1ValueNew");

        Map<String, Object> stageTransitionOutput = new HashMap<>();
        stageTransitionOutput.put("ToStage", "Diagnosed_Not_On_Treatment");
        doReturn(getEpisodeDtoEntity_testCase1()).when(episodeService).getEpisodeDetails(any(), any());
        doReturn(getEpisode_testCase2()).when(episodeService).processUpdateEpisode(any(), any(),anyBoolean());
        when(stageTransitionService.stageTransitionHandler(any(), any())).thenReturn(new EventStreamingDto<>("#",stageTransitionOutput));
        doNothing().when(adherenceService).updateEntity(any(), any(), any(), any());
        episodeService.processEpisodeRequestInputForStageTransition(clientId, requestInput, false);
    }

    @Test(expected = ValidationException.class)
    public void test_processEpisoderequestInputForStageTransition_throwsValidationException() throws Exception {
        Long episodeId = 1L;
        Long clientId = 1L;
        Map<String, Object> requestInput = new HashMap<>();
        requestInput.put(FieldConstants.EPISODE_FIELD_EPISODE_ID, episodeId);
        doReturn(getEpisodeDtoEntity_testCase1()).when(episodeService).getEpisodeDetails(any(), any());
        when(stageTransitionService.stageTransitionHandler(any(), eq(clientId))).thenReturn(new EventStreamingDto<>("#", null));
        doNothing().when(adherenceService).updateEntity(any(), any(), any(), any());
        episodeService.processEpisodeRequestInputForStageTransition(clientId, requestInput);
    }

    private EpisodeDto getEpisodeDtoEntity_testCase1() {
        return new EpisodeDto(1L, 1L, 1L, true, "DSTB", 1L, null, Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, null, 1L,"PRESUMPTIVE_OPEN", "Presumptive Open", "IndiaTbPublic", 1L, null, null, null, null, new HashMap<>());
    }

    private List<EpisodeDto> getEpisodeDtoEntity_testCase2() {
        List<EpisodeDto> episodeDtoList = new ArrayList<>();
        episodeDtoList.add(new EpisodeDto(1L, 1L, 1L, true, "DSTB", 1L, null, Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, null, 1L,"PRESUMPTIVE_OPEN", "Presumptive Open", "IndiaTbPublic", 1L, null, null, null, null, new HashMap<>()));
        episodeDtoList.add(new EpisodeDto(2L, 2L, 1L, true, "DSTB", 1L, null, Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, null, 1L,"PRESUMPTIVE_OPEN", "Presumptive Open", "IndiaTbPublic", 1L, null, null, null, null, new HashMap<>()));
        episodeDtoList.add(new EpisodeDto(2L, 3L, 1L, true, "DSTB", 1L, null, Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, null, 1L,"PRESUMPTIVE_OPEN", "Presumptive Open", "IndiaTbPublic", 1L, null, null, null, null, new HashMap<>()));
        return episodeDtoList;
    }

    @Test
    public void test_deleteEpisode_deletesEpisodeSuccess() {
        Long episodeId = 1L;
        Long clientId = 1L;
        TabPermissionDto tabPermissionDto = new TabPermissionDto(1L, 1L, "Adherence", null, true, true, true, true, 1L);
        List<TabPermissionDto> tabPermissionDtoList = new ArrayList<>();
        tabPermissionDtoList.add(tabPermissionDto);
        Map<Long, List<TabPermissionDto>> tabPermissionMap= new HashMap<>();
        tabPermissionMap.put(1L, tabPermissionDtoList);
        when(episodeRepository.findByIdAndClientIdAndDeletedIs(eq(episodeId), eq(clientId), eq(false))).thenReturn(getEpisode_testCase1());
        doReturn(getEpisode_testCase1()).when(episodeService).processUpdateEpisode(any(), any());
        when(diseaseTemplateService.getStageToTabPermissionsListMapForStageIds(anySet(), anyList(), any())).thenReturn(tabPermissionMap);
        DeleteRequest deleteEpisodeRequest = new DeleteRequest(episodeId, 1L, "reason", "notes");
        Episode deletedEpisode = episodeService.deleteEpisode(clientId, deleteEpisodeRequest);
        Assert.assertTrue(deletedEpisode.isDeleted());
        Mockito.verify(applicationEventPublisher, Mockito.times(1)).publishEvent(any(EventStreamingAnalyticsDto.class));
    }

    @Test(expected = NotFoundException.class)
    public void test_deleteEpisode_throwsNotFoundException()  {
        Long episodeId = 1L;
        Long clientId = 1L;
        when(episodeRepository.findByIdAndClientIdAndDeletedIs(eq(episodeId), eq(clientId), eq(false))).thenReturn(null);
        DeleteRequest deleteEpisodeRequest = new DeleteRequest(episodeId, 1L, "reason", "notes");
        episodeService.deleteEpisode(clientId, deleteEpisodeRequest);
    }

    @Test
    public void elasticSearchTest() {
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest();
        episodeSearchRequest.setFieldsToShow(Arrays.asList("id"));
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(10, new ArrayList<>(), new HashMap<>());
        doReturn (episodeSearchResult).when(episodeService).elasticSearch(eq(episodeSearchRequest), eq(false));
        EpisodeSearchResult episodeSearchResult1 = episodeService.elasticSearch(episodeSearchRequest);
        assertEquals(10, episodeSearchResult1.getTotalHits());
    }

    @Test
    public void elasticSearch_2_Test() {
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest();
        episodeSearchRequest.setFieldsToShow(Arrays.asList("id"));
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(10, new ArrayList<>(), new HashMap<>());

        doReturn (new HashMap<>()).when(episodeService).getFieldToTableTypeMap();
        when (esEpisodeService.search(eq(episodeSearchRequest), anyMap(), eq(false))).thenReturn(episodeSearchResult);

        EpisodeSearchResult episodeSearchResult1 = episodeService.elasticSearch(episodeSearchRequest, false);
        assertEquals(10, episodeSearchResult1.getTotalHits());
    }

    @Test
    public void getFieldToTableTypeMap_Test() {
        when(supportedRelationRepository.findAll()).thenReturn(getAllSupportedRelationLowerCase());
        when(diseaseTemplateService.getAllFields()).thenReturn(getFields());
        Map<String, String> fieldToTableMap = episodeService.getFieldToTableTypeMap();
        assertEquals("stageData", fieldToTableMap.get("firstName"));
        assertEquals("stageData", fieldToTableMap.get("treatmentOutcome"));
        assertEquals("hierarchyMappings", fieldToTableMap.get("sR4"));
        assertEquals("hierarchyMappings", fieldToTableMap.get("sR4_all"));
        assertEquals("hierarchyMappings", fieldToTableMap.get("hierarchyMapping_SR4"));
        assertEquals("associations", fieldToTableMap.get("sR1"));
    }

    @Test
    public void clientObjToElasticDoc_Test() {
        Map<String, Object> nikshayPatientObj = new HashMap<>();
        nikshayPatientObj.put("FirstName", "Some Name");
        nikshayPatientObj.put("LastName", "Some Name");
        nikshayPatientObj.put("TreatmentOutcome", "Some Outcome");
        nikshayPatientObj.put("HierarchyMapping_Current", 123);
        nikshayPatientObj.put("enrollment", 123);
        nikshayPatientObj.put("SR1", "SR1");
        nikshayPatientObj.put("typeOfEpisode", "Private");

        when(supportedRelationRepository.findAll()).thenReturn(getAllSupportedRelationLowerCase());
        when(diseaseTemplateService.getAllFields()).thenReturn(getFields());
        doReturn(Arrays.asList(1L, 2L, 3L)).when(registryService).getAllLevelIdsForHierarchy(anyLong(), anyBoolean());

        Map<String, Object> elasticDoc = episodeService.clientObjToElasticDoc(nikshayPatientObj);
        assertTrue(((Map<String, String>)elasticDoc.get("associations")).containsKey("sR1"));
        assertTrue(((Map<String, String>)elasticDoc.get("hierarchyMappings")).containsKey("current_all"));
        assertTrue(((Map<String, String>)elasticDoc.get("hierarchyMappings")).containsKey("enrollment"));
        assertTrue(((Map<String, String>)elasticDoc.get("stageData")).containsKey("firstName"));
        assertTrue(elasticDoc.containsKey("typeOfEpisode"));
    }

    @Test
    public void covertAndSavetoElastic_Public_Test() {
        EpisodeIndex episodeIndex = new EpisodeIndex();
        when (esEpisodeService.save(any())).thenReturn(episodeIndex);
        Map<String, Object> map = new HashMap<>();
        map.put("typeOfPatient", "IndiaTbPublic");
        map.put("id", "1234");
        map.put("personId", "1234");
        map.put("stage", "PRESUMPTIVE_OPEN_PUBLIC");
        map.put("stageData", new HashMap<>());
        doReturn(map).when(episodeService).clientObjToElasticDoc(any());
        when(stageRepository.findByStageName(eq("PRESUMPTIVE_OPEN_PUBLIC"))).thenReturn(new Stage(1L, "PRESUMPTIVE_OPEN_PUBLIC", "PRESUMPTIVE_OPEN_PUBLIC", "PRESUMPTIVE_OPEN_PUBLIC"));
        when(episodeTagService.getEpisodeTags(any())).thenReturn(new EpisodeTagResponse(
                1L, null, null
        ));
        episodeService.convertAndSavetoElastic(new HashMap<>(), 1L);
        Mockito.verify(esEpisodeService, Mockito.times(1)).save(any());
    }

    @Test
    public void covertAndSavetoElastic_Private_Test() {
        EpisodeIndex episodeIndex = new EpisodeIndex();
        when (esEpisodeService.save(any())).thenReturn(episodeIndex);
        Map<String, Object> map = new HashMap<>();
        map.put("typeOfPatient", "IndiaTbPrivate");
        map.put("id", "1234");
        map.put("personId", "1234");
        map.put("stage", "PRESUMPTIVE_OPEN_PRIVATE");
        map.put("stageData", new HashMap<>());
        doReturn(map).when(episodeService).clientObjToElasticDoc(any());
        when(stageRepository.findByStageName(eq("PRESUMPTIVE_OPEN_PRIVATE"))).thenReturn(new Stage(1L, "PRESUMPTIVE_OPEN_PUBLIC", "PRESUMPTIVE_OPEN_PUBLIC", "PRESUMPTIVE_OPEN_PUBLIC"));
        when(episodeTagService.getEpisodeTags(any())).thenReturn(new EpisodeTagResponse(
                1L, null, null
        ));
        episodeService.convertAndSavetoElastic(new HashMap<>(), 1L);
        Mockito.verify(esEpisodeService, Mockito.times(1)).save(any());
    }

    @Test
    public void saveToElasticEmptyDtoTest() {
        EpisodeDto episodeDto = new EpisodeDto();
        doReturn(episodeDto).when(episodeService).getEpisodeDetails(eq(1L), eq(1L));
        episodeService.saveToElastic(1L, 1L);
        Mockito.verify(esEpisodeService, Mockito.times(1)).save(any());
    }

    @Test
    public void saveToElasticTest() {
        EpisodeDto episodeDto = new EpisodeDto();
        Map<String, Object> hMappings = new HashMap<>();
        hMappings.put("current", 104);
        episodeDto.setHierarchyMappings(hMappings);
        doReturn(episodeDto).when(episodeService).getEpisodeDetails(eq(1L), eq(1L));
        doReturn(Arrays.asList(1L, 2L)).when(registryService).getAllLevelIdsForHierarchy(anyLong(), anyBoolean());
        episodeService.saveToElastic(1L, 1L);
        Assert.assertTrue(hMappings.containsKey("current_all"));
        Mockito.verify(esEpisodeService, Mockito.times(1)).save(any());
    }

    @Test
    public void getEpisodeFromElastic() {
        EpisodeIndex episodeIndex = new EpisodeIndex("1", 1L, 1L, false, 1L, "1,2,3", "2022-01-01 13:00:00", "LOW", 1L, 100L, "", "", "", "2022-01-01 13:00:00", "2022-01-01 13:00:00", "2022-01-01 13:00:00", null, null, null, new ArrayList<>());
        when(esEpisodeService.findByIdAndClientId(eq(1L), eq(29L))).thenReturn(episodeIndex);
        episodeService.getEpisodeDetails(1L, 29L, false, false, false, true);
        Mockito.verify(esEpisodeService, Mockito.times(1)).findByIdAndClientId(any(), any());
    }

    @Test
    public void getEpisodeDocumentTest() {
        when(esEpisodeService.findByIdAndClientId(eq(1L), eq(1L))).thenReturn(new EpisodeIndex());
        episodeService.getEpisodeDocument(1L, 1L);
        Mockito.verify(esEpisodeService, Mockito.times(1)).findByIdAndClientId(eq(1L), eq(1L));
    }

    private List<Field> getFields () {
        return Arrays.asList(
                new Field(1L, "firstName", "Person", ""),
                new Field(2L, "lastName", "Person", ""),
                new Field(3L, "treatmentOutcome", "Episode", "")
        );
    }

    @Test(expected = ValidationException.class)
    public void getDuplicatesEpisodeForInvalidNumberOfEpisodeRecords() {
        Map<String, Object> fetchDuplicatesRequest = new HashMap<>();

        episodeService.getDuplicateEpisodesForUser(1L, fetchDuplicatesRequest);

        Mockito.verify(userService, Mockito.times(0)).fetchDuplicates(any());
        Mockito.verify(episodeService, Mockito.times(0)).getEpisodesForPersonList(any(), any(), anyBoolean());
    }

    @Test
    public void getDuplicatesEpisodeForUserTest() {
        List<Long> stageIdList = Arrays.asList(1L, 2L);
        List<Long> personIdList = Arrays.asList(1L, 2L, 3L);
        List<EpisodeDto> episodeDtos = getEpisodeDtoEntity_testCase2();
        int numberOfRecords = 1;
        Map<String, Object> fetchDuplicatesRequest = getFetchDuplicateRequestOne();
        fetchDuplicatesRequest.put(EpisodeField.STAGE_ID_LIST.getFieldName(), stageIdList);
        fetchDuplicatesRequest.put(EpisodeField.START_INDEX.getFieldName(), 0);
        fetchDuplicatesRequest.put(EpisodeField.NUMBER_OF_EPISODE_RECORDS.getFieldName(), numberOfRecords);

        when(userService.fetchDuplicates(any())).thenReturn(personIdList);
        doReturn(episodeDtos).when(episodeService).getEpisodesForPersonListFromElastic(any(), any());

        List<EpisodeDto> episodeDtoList = episodeService.getDuplicateEpisodesForUser(1L, fetchDuplicatesRequest);
        List<Long> episodeStageIdList = episodeDtoList.stream().map(EpisodeDto::getCurrentStageId).collect(Collectors.toList());

        Assert.assertEquals(numberOfRecords, episodeDtoList.size());
        Assert.assertTrue(stageIdList.containsAll(episodeStageIdList));
        Assert.assertFalse(Collections.disjoint(episodeStageIdList, stageIdList));
        Mockito.verify(userService, Mockito.times(1)).fetchDuplicates(any());
        Mockito.verify(episodeService, Mockito.times(1)).getEpisodesForPersonListFromElastic(any(), any());
    }

    @Test
    public void getDuplicatesEpisodeForNullStartIndexAndNumberOfEpisodeRecords() {
        List<Long> stageIdList = Arrays.asList(1L, 2L);
        List<Long> personIdList = Arrays.asList(1L, 2L, 3L);
        List<EpisodeDto> episodeDtos = getEpisodeDtoEntity_testCase2();
        Map<String, Object> fetchDuplicatesRequest = getFetchDuplicateRequestOne();
        fetchDuplicatesRequest.put(EpisodeField.STAGE_ID_LIST.getFieldName(), stageIdList);

        when(userService.fetchDuplicates(any())).thenReturn(personIdList);
        doReturn(episodeDtos).when(episodeService).getEpisodesForPersonListFromElastic(any(), any());

        List<EpisodeDto> episodeDtoList = episodeService.getDuplicateEpisodesForUser(1L, fetchDuplicatesRequest);
        List<Long> episodeStageIdList = episodeDtoList.stream().map(EpisodeDto::getCurrentStageId).collect(Collectors.toList());

        Assert.assertEquals(episodeDtos.size(), episodeDtoList.size());
        Assert.assertTrue(stageIdList.containsAll(episodeStageIdList));
        Mockito.verify(userService, Mockito.times(1)).fetchDuplicates(any());
        Mockito.verify(episodeService, Mockito.times(1)).getEpisodesForPersonListFromElastic(any(), any());
    }

    @Test
    public void getDuplicatesEpisodeForEmptyStageList() {
        List<Long> personIdList = Arrays.asList(1L, 2L, 3L);
        List<EpisodeDto> episodeDtos = getEpisodeDtoEntity_testCase2();
        Map<String, Object> fetchDuplicatesRequest = getFetchDuplicateRequestOne();

        when(userService.fetchDuplicates(any())).thenReturn(personIdList);
        doReturn(episodeDtos).when(episodeService).getEpisodesForPersonListFromElastic(any(), any());

        List<EpisodeDto> episodeDtoList = episodeService.getDuplicateEpisodesForUser(1L, fetchDuplicatesRequest);

        Assert.assertEquals(episodeDtos.size(), episodeDtoList.size());
        Mockito.verify(userService, Mockito.times(1)).fetchDuplicates(any());
        Mockito.verify(episodeService, Mockito.times(1)).getEpisodesForPersonListFromElastic(any(), any());
    }

    @Test
    public void getDuplicatesEpisodeForUserWithEmptyPersonIdListTest() {
        Map<String, Object> fetchDuplicatesRequest = getFetchDuplicateRequestOne();
        List<Long> personIdList = new ArrayList<>();
        when(userService.fetchDuplicates(any())).thenReturn(personIdList);

        List<EpisodeDto> episodeDtoList = episodeService.getDuplicateEpisodesForUser(1L, fetchDuplicatesRequest);

        Assert.assertEquals(0L, episodeDtoList.size());
        Mockito.verify(userService, Mockito.times(1)).fetchDuplicates(any());
        Mockito.verify(episodeService, Mockito.times(0)).getEpisodesForPersonListFromElastic(any(), any());
    }

    @Test
    public void updateClientEpisodeToElasticTest_PersonModule() {
        Long clientId = 29L;
        String module = "Person";

        List<EpisodeIndex> episodeIndices = new ArrayList<>();

        EpisodeIndex episodeIndex = new EpisodeIndex();
        episodeIndex.setClientId(29L);
        episodeIndex.setId("1");

        EpisodeIndex episodeIndex2 = new EpisodeIndex();
        episodeIndex2.setClientId(29L);
        episodeIndex2.setId("2");

        episodeIndices.add(episodeIndex);
        episodeIndices.add(episodeIndex2);

        Map<String, String> map = new HashMap<>();
        map.put("id", "1234");
        map.put("personId", "1234");

        Map<String, Object> episodeObj = new HashMap<>();
        episodeObj.put("personId", "12344");

        doReturn(map).when(episodeService).clientObjToElasticDoc(eq(episodeObj), eq(true), eq("Person"));
        when (esEpisodeService.findAllByPersonId(eq(12344L), eq(29L))).thenReturn(episodeIndices);
        doNothing().when(esEpisodeService).updateToElastic(any());
        doReturn (new HashMap<>()).when(episodeService).getFieldToTableTypeMap();

        episodeService.updateClientEpisodeToElastic(episodeObj, clientId, module);

        Mockito.verify(esEpisodeService, Mockito.times(1)).updateBulkElastic(any());
    }

    @Test
    public void updateClientEpisodeToElasticTest_EpisodeModule() {
        Long clientId = 29L;
        String module = "episode";

        List<EpisodeIndex> episodeIndices = new ArrayList<>();

        EpisodeIndex episodeIndex = new EpisodeIndex();
        episodeIndex.setClientId(29L);
        episodeIndex.setId("1");

        episodeIndices.add(episodeIndex);

        Map<String, Object> map = new HashMap<>();
        map.put("id", "1234");

        Map<String, Object> episodeObj = new HashMap<>();
        episodeObj.put("id", "12344");
        episodeObj.put("Stage", "DIAGNOSED_ON_TREATMENT_PUBLIC");
        episodeObj.put("TypeOfPatient", "IndiaTbPublic");
        episodeObj.put("TBTreatmentStartDate", "2022-01-01 14:00:00");
        episodeObj.put("EndDate", "2022-10-10 18:00:00");

        when(stageRepository.findByStageName(eq("DIAGNOSED_ON_TREATMENT_PUBLIC"))).thenReturn(getStage0());
        doReturn(map).when(episodeService).clientObjToElasticDoc(any(), eq(true), eq("episode"));
        when (esEpisodeService.findAllByPersonId(eq(12344L), eq(29L))).thenReturn(episodeIndices);
        doNothing().when(esEpisodeService).updateBulkElastic(any());
        doReturn (new HashMap<>()).when(episodeService).getFieldToTableTypeMap();

        episodeService.updateClientEpisodeToElastic(episodeObj, clientId, module);

        Mockito.verify(esEpisodeService, Mockito.times(1)).updateBulkElastic(any());
        assertTrue(map.containsKey("stageKey"));
        assertEquals(map.get("startDate"), "2022-01-01 14:00:00");
        assertEquals(map.get("endDate"), "2022-10-10 18:00:00");
    }

    @Test
    public void updateClientEpisodeToElasticTest_EpisodeModule_Stage() {
        Long clientId = 29L;
        String module = "episode";

        List<EpisodeIndex> episodeIndices = new ArrayList<>();

        EpisodeIndex episodeIndex = new EpisodeIndex();
        episodeIndex.setClientId(29L);
        episodeIndex.setId("1");

        episodeIndices.add(episodeIndex);

        Map<String, Object> map = new HashMap<>();
        map.put("id", "1234");

        Map<String, Object> episodeObj = new HashMap<>();
        episodeObj.put("id", "12344");
        episodeObj.put("Stage", "DIAGNOSED_ON_TREATMENT_PRIVATE");
        episodeObj.put("TypeOfPatient", "IndiaTbPrivate");

        when(stageRepository.findByStageName(eq("DIAGNOSED_ON_TREATMENT_PRIVATE"))).thenReturn(getStage0());
        doReturn(map).when(episodeService).clientObjToElasticDoc(any(), eq(true), eq("episode"));
        when (esEpisodeService.findAllByPersonId(eq(12344L), eq(29L))).thenReturn(episodeIndices);
        doNothing().when(esEpisodeService).updateToElastic(any());
        doReturn (new HashMap<>()).when(episodeService).getFieldToTableTypeMap();

        episodeService.updateClientEpisodeToElastic(episodeObj, clientId, module);

        Mockito.verify(esEpisodeService, Mockito.times(1)).updateBulkElastic(any());
        assertTrue(map.containsKey("stageKey"));
    }

    @Test
    public void updateClientEpisodeToElasticTest_Episode_Delete() {
        Long clientId = 29L;
        String module = "episode";

        List<EpisodeIndex> episodeIndices = new ArrayList<>();

        EpisodeIndex episodeIndex = new EpisodeIndex();
        episodeIndex.setClientId(29L);
        episodeIndex.setId("1");

        episodeIndices.add(episodeIndex);

        Map<String, Object> map = new HashMap<>();
        map.put("id", "1234");

        Map<String, Object> episodeObj = new HashMap<>();
        episodeObj.put("id", "12344");
        episodeObj.put("Stage", "DIAGNOSED_OUTCOME_ASSIGNED");
        episodeObj.put("TypeOfPatient", "IndiaTbPublic");
        episodeObj.put("Deleted", true);
        episodeObj.put("DuplicateStatus", "DELETED_AS_DUPLICATE");

        when(stageRepository.findByStageName(eq("DIAGNOSED_OUTCOME_ASSIGNED"))).thenReturn(getStage0());
        doReturn(map).when(episodeService).clientObjToElasticDoc(any(), eq(true), eq("episode"));
        when (esEpisodeService.findAllByPersonId(eq(12344L), eq(29L))).thenReturn(episodeIndices);
        doNothing().when(esEpisodeService).updateToElastic(any());
        doReturn (new HashMap<>()).when(episodeService).getFieldToTableTypeMap();

        episodeService.updateClientEpisodeToElastic(episodeObj, clientId, module);

        Mockito.verify(esEpisodeService, Mockito.times(1)).delete(any(), any());
    }

    @Test
    public void updateElasticDocument_withHierarchyUpdateAsTrue() {
        EpisodeDto episodeDto = new EpisodeDto();
        episodeDto.setStageData(new HashMap<>());
        Map<String, Object> hierarchyMappings = new HashMap<>();
        hierarchyMappings.put("current", 1);
        episodeDto.setHierarchyMappings(hierarchyMappings);
        doReturn(episodeDto).when(episodeService).getEpisodeDetails(eq(1L), eq(1L), eq(false), eq(true), eq(false), eq(true));

        Map<String, Object> map = new HashMap<>();
        map.put("typeOfPatient", "IndiaTbPublic");
        map.put("id", "1234");
        map.put("personId", "1234");
        map.put("hierarchyMappings", hierarchyMappings);

        doReturn(map).when(episodeService).clientObjToElasticDoc(any(), eq(true), eq(Constants.MY_MODULE));
        doNothing().when(esEpisodeService).updateToElastic(any());

        episodeService.updateElasticDocument(1L, 1L, true);
        Mockito.verify(esEpisodeService, Mockito.times(1)).updateToElastic(any());
    }

    @Test
    public void updateElasticDocument_withHierarchyUpdateAsTrueAndHierarchyDataAsNull() {
        EpisodeDto episodeDto = new EpisodeDto();
        episodeDto.setStageData(new HashMap<>());
        doReturn(episodeDto).when(episodeService).getEpisodeDetails(eq(1L), eq(1L), eq(false), eq(true), eq(false), eq(true));

        Map<String, Object> map = new HashMap<>();
        map.put("typeOfPatient", "IndiaTbPublic");
        map.put("id", "1234");
        map.put("personId", "1234");

        doReturn(map).when(episodeService).clientObjToElasticDoc(any(), eq(true), eq(Constants.MY_MODULE));
        doNothing().when(esEpisodeService).updateToElastic(any());

        episodeService.updateElasticDocument(1L, 1L, true);
        Mockito.verify(esEpisodeService, Mockito.times(1)).updateToElastic(any());
    }


    @Test
    public void updateElasticDocument_withHierarchyUpdateAsFalse() {
        EpisodeDto episodeDto = new EpisodeDto();
        episodeDto.setStageData(new HashMap<>());
        Map<String, Object> hierarchyMappings = new HashMap<>();
        hierarchyMappings.put("current", 1);
        episodeDto.setHierarchyMappings(hierarchyMappings);
        doReturn(episodeDto).when(episodeService).getEpisodeDetails(eq(1L), eq(1L), eq(false), eq(true), eq(false), eq(true));

        Map<String, Object> map = new HashMap<>();
        map.put("typeOfPatient", "IndiaTbPublic");
        map.put("id", "1234");
        map.put("personId", "1234");

        doReturn(map).when(episodeService).clientObjToElasticDoc(any());
        doNothing().when(esEpisodeService).updateToElastic(any());

        episodeService.updateElasticDocument(1L, 1L, false);
        Mockito.verify(esEpisodeService, Mockito.times(1)).updateToElastic(any());
    }


    @Test
    public void updateElasticDocument_Deleted() {
        Episode episode = new Episode(1L, 1L, 1L, true, null, "1,2,3", Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, 1L, "IndiaTbPublic", 1L);
        Stage stage = new Stage(1L, "PRESUMPTIVE_OPEN_PUBLIC", null, "Presumptive Open");
        EpisodeDto episodeDto = new EpisodeDto(episode, new ArrayList<>(), stage, null, new ArrayList<>(), new HashMap<>(), new HashMap<>(), new ArrayList<>(), new HashMap<>());

        episodeDto.setStageData(new HashMap<>());
        doReturn(episodeDto).when(episodeService).getEpisodeDetails(eq(1L), eq(1L), eq(false), eq(true), eq(false), eq(true));

        Map<String, Object> map = new HashMap<>();
        map.put("typeOfPatient", "IndiaTbPublic");
        map.put("id", "1234");
        map.put("personId", "1234");

        doReturn(map).when(episodeService).clientObjToElasticDoc(any());
        doNothing().when(esEpisodeService).updateToElastic(any());

        episodeService.updateElasticDocument(1L, 1L, false);
        Mockito.verify(esEpisodeService, Mockito.times(1)).delete(any(), any());
    }

    public void getEpisodeByPhoneNumberServiceTest() {
        String phoneNumber = "9999999999";
        Client client = new Client();
        Episode episode = new Episode(1L, 1L, 1L, false, null, "1", Utils.getCurrentDateNew(), Utils.getCurrentDateNew(),
                null, Utils.getCurrentDateNew(), null, 1L, null, 1L);
        List<Episode> episodeList = new ArrayList<>();
        episodeList.add(episode);
        List<Map<String, Object>> personIdList = new ArrayList<>();
        Map<String, Object> personIdmap = new HashMap<>();
        personIdmap.put("id", 1L);
        personIdList.add(personIdmap);
        ResponseEntity<Response<List<Map<String, Object>>>> user = new ResponseEntity<>(new Response<>(true, personIdList), HttpStatus.OK);
        List<Long> personIds= new ArrayList<>();
        for( Map<String, Object> personId: personIdList){
            personIds.add(Long.parseLong(String.valueOf(personId.get("id"))));
        }
        when(clientService.getClientByTokenOrDefault()).thenReturn(client);
        when(userService.getByPrimaryPhone(eq(phoneNumber))).thenReturn(user);
        when(episodeRepository.findAllByPersonIdInAndClientIdAndDeletedFalse(eq(personIds), eq(client.getId()))).thenReturn(episodeList);
        when(userService.processResponseEntity(any(), any())).thenReturn(user.getBody().getData());
        List<Episode> episodeIdResponse = episodeService.getEpisodeByPhoneNumber(phoneNumber);
        Long ans = 1L;
        Assert.assertEquals(ans, episodeIdResponse.get(0).getId());
    }

    @Test(expected = NotFoundException.class)
    public void getEpisodeByPhoneNumberServiceTestThrowsNotFoundException() {
        String phoneNumber = "9999999999";
        Long episodeId = 1L;
        Client client = new Client();
        Episode episode = new Episode(1L, 1L, 1L, false, null, "1", Utils.getCurrentDateNew(), Utils.getCurrentDateNew(),
                null, Utils.getCurrentDateNew(), null, 1L, null, 1L);
        List<Episode> episodeList = new ArrayList<>();
        episodeList.add(episode);
        List<Map<String, Object>> personIdList = new ArrayList<>();
        Map<String, Object> personIdmap = new HashMap<>();
        personIdmap.put("id", 1L);
        personIdList.add(personIdmap);
        ResponseEntity<Response<List<Map<String, Object>>>> user = new ResponseEntity<>(new Response<>(true, personIdList), HttpStatus.OK);
        List<Long> personIds= new ArrayList<>();
        for( Map<String, Object> personId: personIdList){
            personIds.add(Long.parseLong(String.valueOf(personId.get("id"))));
        }
        when(clientService.getClientByTokenOrDefault()).thenReturn(client);
        when(userService.getByPrimaryPhone(eq(phoneNumber))).thenReturn(user);
        when(episodeRepository.findAllByPersonIdInAndClientIdAndDeletedFalse(eq(personIds), eq(client.getId()))).thenReturn(null);
        when(userService.processResponseEntity(any(), any())).thenReturn(user.getBody().getData());
        List<Episode> episodeIdResponse = episodeService.getEpisodeByPhoneNumber(phoneNumber);
    }

    @Test
    public void test_searchEpisode() {
        SearchEpisodesRequest searchEpisodesRequest = new SearchEpisodesRequest(Arrays.asList(1L,2L),"2022-06-08 00:00:00","2022-06-09 00:00:00",true,new ArrayList<>(),"current",new ArrayList<>());
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client());
        episodeService.search(searchEpisodesRequest);
        Mockito.verify(episodeStageRepository, Mockito.times(1)).getEpisodeIdsByStartDateAndStage(any(),any(),any());
        Mockito.verify(episodeRepository, Mockito.times(1)).getAllEpisodes(any(), any());
    }

    @Test
    public void test_updateRiskStatus() {
        UpdateRiskRequest updateRiskRequest = new UpdateRiskRequest(Collections.singletonList(1L),Collections.singletonList(1L),2L);
        episodeService.updateRiskStatus(updateRiskRequest);
        Mockito.verify(episodeRepository,Mockito.times(2)).updateRiskStatusForEpisodeIdWithStatus(eq(updateRiskRequest.getHighRiskIdList()),any());
        Mockito.verify(esEpisodeService,Mockito.times(2)).updateBulkElastic(any());
    }

    @Test
    public void test_getEpisodeBulk() {
        EpisodeBulkRequest episodeBulkRequest = new EpisodeBulkRequest(Arrays.asList(1L,2L));
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client());
        episodeService.getEpisodesBulk(episodeBulkRequest);
        Mockito.verify(episodeRepository, Mockito.times(1)).getAllEpisodes(any(), any());
    }

    @Test
    public void validateDates() {
        Map<String, Object> map = new HashMap<>();
        map.put("endDate", "2022-01-10 01:01:00");
        map.put("startDate", "2022-01-01 01:01:00");
        episodeService.validateDates(map);
    }

    @Test(expected = ValidationException.class)
    public void validateDates_ValidationException() {
        Map<String, Object> map = new HashMap<>();
        map.put("endDate", "2021-01-10 01:01:00");
        map.put("startDate", "2022-01-01 01:01:00");
        episodeService.validateDates(map);
    }

    @Test
    public void processDoseReminderPNDtosTest() {
        Client client = new Client();
        List<Episode> episodeList = new ArrayList<>();
        Episode episode = new Episode(1L, 1L, 1L, false, null, "1", Utils.getCurrentDateNew(), Utils.getCurrentDateNew(),
                null, Utils.getCurrentDateNew(), null, 1L, null, 1L);
        episodeList.add(episode);
        AdherenceResponse adherenceResponse = new AdherenceResponse(
                Utils.toLocalDateTimeWithNull("02-01-2022 18:30:00"),
                Utils.toLocalDateTimeWithNull("06-01-2022 18:30:00"),
                "654A9"
        );
        List<AdherenceResponse> adherenceResponseList = new ArrayList<>();
        adherenceResponseList.add(adherenceResponse);
        Map<String, List<String>> userDeviceIdsMap = new HashMap<>();
        List<String> deviceIdList = new ArrayList<>();
        deviceIdList.add("1");
        userDeviceIdsMap.put("userNames", deviceIdList);
        AllAdherenceResponse allAdherenceResponse = new AllAdherenceResponse(adherenceResponseList);
        DeviceIdsMapResponse deviceIdsMapResponse = new DeviceIdsMapResponse(userDeviceIdsMap);
        ResponseEntity<Response<AllAdherenceResponse>> AdherenceResponse = new ResponseEntity<>(new Response<>(true, allAdherenceResponse), HttpStatus.OK);
        ResponseEntity<Response<DeviceIdsMapResponse>> deviceIdResponse = new ResponseEntity<>(new Response<>(true, deviceIdsMapResponse), HttpStatus.OK);
        when(clientService.getClientByTokenOrDefault()).thenReturn(client);
        when(episodeRepository.findAllByClientIdAndDeletedFalse(eq(client.getId()))).thenReturn(episodeList);
        when(iamService.getAdherenceBulk(any())).thenReturn(AdherenceResponse);
        when(ssoService.fetchDeviceIdUserMap(any())).thenReturn(deviceIdResponse);
        PNDtoWithNotificationData pushNotificationTemplateDtoList = episodeService.getDoseReminderPNDtosForClientId(29L, "Asia/Kolkata");
        assertEquals("1", pushNotificationTemplateDtoList.getPushNotificationTemplateDtoList().get(0).getDeviceId());
    }

    @Test
    public void processDoseReminderPNDtosTest_episode_with_treatment_plan() {
        Client client = new Client();
        List<Episode> episodeList = new ArrayList<>();
        Episode episode = new Episode(1L, 1L, 1L, false, null, "1", Utils.getCurrentDateNew(), Utils.getCurrentDateNew(),
                null, Utils.getCurrentDateNew(), null, 1L, null, 1L);
        episodeList.add(episode);
        AdherenceResponse adherenceResponse = new AdherenceResponse(
                Utils.toLocalDateTimeWithNull("02-01-2022 18:30:00"),
                Utils.toLocalDateTimeWithNull("06-01-2022 18:30:00"),
                "654A9"
        );
        List<AdherenceResponse> adherenceResponseList = new ArrayList<>();
        adherenceResponseList.add(adherenceResponse);
        Map<String, List<String>> userDeviceIdsMap = new HashMap<>();
        List<String> deviceIdList = new ArrayList<>();
        deviceIdList.add("1");
        userDeviceIdsMap.put("userNames", deviceIdList);
        AllAdherenceResponse allAdherenceResponse = new AllAdherenceResponse(adherenceResponseList);
        DeviceIdsMapResponse deviceIdsMapResponse = new DeviceIdsMapResponse(userDeviceIdsMap);
        ResponseEntity<Response<AllAdherenceResponse>> AdherenceResponse = new ResponseEntity<>(new Response<>(true, allAdherenceResponse), HttpStatus.OK);
        ResponseEntity<Response<DeviceIdsMapResponse>> deviceIdResponse = new ResponseEntity<>(new Response<>(true, deviceIdsMapResponse), HttpStatus.OK);
        when(clientService.getClientByTokenOrDefault()).thenReturn(client);
        when(episodeRepository.findAllByClientIdAndDeletedFalse(eq(client.getId()))).thenReturn(episodeList);
        when(iamService.getAdherenceBulk(any())).thenReturn(AdherenceResponse);
        when(ssoService.fetchDeviceIdUserMap(any())).thenReturn(deviceIdResponse);
        when(diseaseTemplateService.getAllFieldsByNameIn(any())).thenReturn(Arrays.asList(new Field(1L, "treatmentPlanId", "Episode", null)));
        when(episodeStageDataRepository.getEpisodeFieldIdValueMapForFieldIdsInAndClientId(any(), any())).thenReturn(getEpisodeFieldIdValueDtos());
        PNDtoWithNotificationData pushNotificationTemplateDtoList = episodeService.getDoseReminderPNDtosForClientId(29L, "Asia/Kolkata");
        assertEquals("1", pushNotificationTemplateDtoList.getPushNotificationTemplateDtoList().get(0).getDeviceId());
    }

    @Test
    public void getDeviceIdListTest() {
        List<String> episodeIdList = new ArrayList<>();
        episodeIdList.add("1");
        Map<String, List<String>> userDeviceIdsMap = new HashMap<>();
        List<String> deviceIdList = new ArrayList<>();
        deviceIdList.add("1");
        userDeviceIdsMap.put("userNames", deviceIdList);
        DeviceIdsMapResponse deviceIdsMapResponse = new DeviceIdsMapResponse(userDeviceIdsMap);
        ResponseEntity<Response<DeviceIdsMapResponse>> deviceIdResponse = new ResponseEntity<>(new Response<>(true, deviceIdsMapResponse), HttpStatus.OK);
        when(ssoService.fetchDeviceIdUserMap(any())).thenReturn(deviceIdResponse);
        Map<String , List<String>> deviceTokenMap = episodeService.getDeviceIdList(episodeIdList);
        Mockito.verify(ssoService, Mockito.times(1)).fetchDeviceIdUserMap(any());
    }

    @Test
    public void processRefillReminderPNDtosTest() {
        Client client = new Client();
        Map<Long, List<DiseaseStageFieldMapDto>> longListMap = new HashMap<>();
        List<EpisodeStageData> episodeStageDataList = new ArrayList<>();
        episodeStageDataList.add(new EpisodeStageData(1L, 1L, 1L, "0"));
        List list = new ArrayList();
        list.add(new DiseaseStageFieldMapDto(1L, 1L, 1L, 1L, 1L, "numberOfDaysOfMedication", "Module1", true, false, "0"));
        longListMap.put(1L, list);
        List<Episode> episodes = getEpisodesList_countTwo();
        EpisodeStage episodeStage  = new EpisodeStage(1L, 1L, 1L, Utils.getCurrentDateNew().minusDays(4L), Utils.getCurrentDateNew().minusDays(3));
        List<Episode> episodeList = new ArrayList<>();
        Episode episode = new Episode(1L, 29L, 1L, false, null, "1", Utils.getCurrentDateNew(), Utils.getCurrentDateNew(),
                null, Utils.getCurrentDateNew(), null, 1L, null, 1L);
        episodeList.add(episode);
        Map<String, List<String>> userDeviceIdsMap = new HashMap<>();
        List<String> deviceIdList = new ArrayList<>();
        deviceIdList.add("1");
        userDeviceIdsMap.put("userNames", deviceIdList);
        DeviceIdsMapResponse deviceIdsMapResponse = new DeviceIdsMapResponse(userDeviceIdsMap);
        ResponseEntity<Response<DeviceIdsMapResponse>> deviceIdResponse = new ResponseEntity<>(new Response<>(true, deviceIdsMapResponse), HttpStatus.OK);
        when(clientService.getClientByTokenOrDefault()).thenReturn(client);
        when(episodeRepository.findAllByClientIdAndDeletedFalse(eq(29L))).thenReturn(episodeList);
        when(ssoService.fetchDeviceIdUserMap(any())).thenReturn(deviceIdResponse);
        when(stageRepository.findAllById(any())).thenReturn(getStageList_testCase2()).thenReturn(getStageList_testCase2());
        when(diseaseTemplateService.getStageToTabPermissionsListMapForStageIds(any(), any(), any())).thenReturn(getStageToTabPermissions_testCase2());
        when(diseaseTemplateService.getDiseaseIdTemplateMap(Collections.singletonList(any()))).thenReturn(getDiseaseIdTemplateMap_testCase1());
        when(episodeStageRepository.getEpisodeStagesByEpisodeIdInOrderByStartDateAsc(any())).thenReturn(Collections.singletonList(episodeStage));
        when(diseaseTemplateService.getDefinedStageIdKeyMappingsListMap(any(), any())).thenReturn(longListMap);
        when(episodeStageDataRepository.findAllByEpisodeStageIdIn(any())).thenReturn(episodeStageDataList);
        when(episodeAssociationRepository.getAllByEpisodeIdIn(any())).thenReturn(getEpisodeAssociationsForTwoEpisodes());
        when(episodeHierarchyLinkageRepository.getAllByEpisodeIdIn(any())).thenReturn(getEpisodeHierarchyLinkagesForTwoEpisodes());
        when(episodeTagService.getEpisodeTags(eq(episodes.get(0).getId()))).thenReturn(new EpisodeTagResponse(episodes.get(0).getId()
                , new ArrayList<>(), Arrays.asList("hakuna", "matata")));
        when(episodeTagService.getEpisodeTags(eq(episodes.get(1).getId()))).thenReturn(new EpisodeTagResponse(episodes.get(1).getId()
                , new ArrayList<>(), Arrays.asList("hakuna1", "matata2")));
        when(supportedRelationRepository.findAll()).thenReturn(getAllSupportedRelation());
        HashSet<String> tabNames = new HashSet<>(Arrays.asList("Tab1", "Tab2", "Tab3", "Tab4", "Tab5"));
        Map<Long, Map<String, String>> episodeIdToExpectedFieldKeyValueMap = new HashMap<>();
        Map<String, String> expectedFieldKeyValueMapE1 = new HashMap<>();
        expectedFieldKeyValueMapE1.put("numberOfDaysOfMedication", "5");
        episodeIdToExpectedFieldKeyValueMap.put(1L, expectedFieldKeyValueMapE1);
        PNDtoWithNotificationData pushNotificationTemplateDtoList = episodeService.getRefillReminderPNDtosForClientId(29L, "Asia/Kolkata");
        Mockito.verify(episodeRepository, Mockito.times(1)).findAllByClientIdAndDeletedFalse(29L);
        assertEquals("1", pushNotificationTemplateDtoList.getPushNotificationTemplateDtoList().get(0).getDeviceId());
        assertEquals(true, pushNotificationTemplateDtoList.getSendNotificationData());
    }

    @Test
    public void testTaskListRefillDue() {
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(1L, "client", "client", new Date()));
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest();
        episodeSearchRequest.setSortKey("id");
        episodeSearchRequest.setSortDirection(Sort.Direction.ASC);
        episodeSearchRequest.setSize(20);
        episodeSearchRequest.setPage(0);
        List<TaskListFilterRequest> taskListFilterRequestList = Collections.singletonList(new TaskListFilterRequest(TaskListFields.TASK_LIST_REFILL_DUE, "True", 36L));
        EpisodeTasklistRequest episodeTasklistRequest = new EpisodeTasklistRequest(episodeSearchRequest, taskListFilterRequestList);

        EpisodeIndex episodeIndex = new EpisodeIndex();
        Map<String, Object> stageDataMap = new HashMap<>();
        episodeIndex.setId("23");
        episodeIndex.setStartDate("2022-10-10 18:30:00");
        episodeIndex.setStageData(stageDataMap);
        EpisodeIndex episodeIndex2 = new EpisodeIndex();
        episodeIndex2.setId("24");
        episodeIndex2.setStartDate("2022-10-11 18:30:00");
        episodeIndex2.setStageData(stageDataMap);
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(20, Arrays.asList(episodeIndex2, episodeIndex), null);
        doReturn(episodeSearchResult).when(episodeService).elasticSearch(any());
        when(taskListFiltersToEpisodeSearchFields.transform(any())).thenReturn(episodeSearchRequest);
        Map<Long, String> entityIdRefillMap = new HashMap<>();
        entityIdRefillMap.put(23L, "2022-10-14 18:30:00");
        entityIdRefillMap.put(24L, "");
        when(dispensationService.getEpisodeIdsWithRefillDueDate(any())).thenReturn(entityIdRefillMap);

        EpisodeSearchResult response = episodeService.taskListSearch(episodeTasklistRequest);
        Assert.assertEquals(2L, response.getTotalHits());
    }

    @Test
    public void testTaskListMissedDose() {
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(1L, "client", "client", new Date()));
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest();
        episodeSearchRequest.setSortKey("id");
        episodeSearchRequest.setSortDirection(Sort.Direction.ASC);
        episodeSearchRequest.setSize(20);
        episodeSearchRequest.setPage(0);
        List<TaskListFilterRequest> taskListFilterRequestList = Collections.singletonList(new TaskListFilterRequest(TaskListFields.TASK_LIST_ALLOWED_RANGE_JSON, "[{\"LowerBound\":1,\"UpperBound\":3}]", 36L));
        EpisodeTasklistRequest episodeTasklistRequest = new EpisodeTasklistRequest(episodeSearchRequest, taskListFilterRequestList);

        EpisodeIndex episodeIndex = new EpisodeIndex();
        Map<String, Object> stageDataMap = new HashMap<>();
        episodeIndex.setId("23");
        episodeIndex.setStartDate("2022-10-10 18:30:00");
        episodeIndex.setStageData(stageDataMap);
        EpisodeIndex episodeIndex2 = new EpisodeIndex();
        episodeIndex2.setId("24");
        episodeIndex2.setStartDate("2022-10-11 18:30:00");
        episodeIndex2.setStageData(stageDataMap);
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(20, Arrays.asList(episodeIndex2, episodeIndex), null);
        doReturn(episodeSearchResult).when(episodeService).elasticSearch(any());
        when(taskListFiltersToEpisodeSearchFields.transform(any())).thenReturn(episodeSearchRequest);
        AdherenceResponse adherenceResponse = new AdherenceResponse();
        adherenceResponse.setEntityId("23");
        List<AdherenceResponse> adherenceResponseList = Collections.singletonList(adherenceResponse);
        AllAdherenceResponse allAdherenceResponse = new AllAdherenceResponse(adherenceResponseList);
        ResponseEntity<Response<AllAdherenceResponse>> iamResponse
                = new ResponseEntity<>(
                new Response<>(allAdherenceResponse),
                HttpStatus.OK
        );
        when(iamService.getAdherenceBulk(any())).thenReturn(iamResponse);

        EpisodeSearchResult response = episodeService.taskListSearch(episodeTasklistRequest);
        Assert.assertEquals(1L, response.getTotalHits());
    }

    @Test(expected = ValidationException.class)
    public void testTaskListMissedDoseException() {
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(1L, "client", "client", new Date()));
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest();
        episodeSearchRequest.setSortKey("id");
        episodeSearchRequest.setSortDirection(Sort.Direction.ASC);
        episodeSearchRequest.setSize(20);
        episodeSearchRequest.setPage(0);
        List<TaskListFilterRequest> taskListFilterRequestList = Collections.singletonList(new TaskListFilterRequest(TaskListFields.TASK_LIST_ALLOWED_RANGE_JSON, "{", 36L));
        EpisodeTasklistRequest episodeTasklistRequest = new EpisodeTasklistRequest(episodeSearchRequest, taskListFilterRequestList);

        EpisodeIndex episodeIndex = new EpisodeIndex();
        Map<String, Object> stageDataMap = new HashMap<>();
        episodeIndex.setId("23");
        episodeIndex.setStartDate("2022-10-10 18:30:00");
        episodeIndex.setStageData(stageDataMap);
        EpisodeIndex episodeIndex2 = new EpisodeIndex();
        episodeIndex2.setId("24");
        episodeIndex2.setStartDate("2022-10-11 18:30:00");
        episodeIndex2.setStageData(stageDataMap);
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(20, Arrays.asList(episodeIndex2, episodeIndex), null);
        doReturn(episodeSearchResult).when(episodeService).elasticSearch(any());
        when(taskListFiltersToEpisodeSearchFields.transform(any())).thenReturn(episodeSearchRequest);
        AdherenceResponse adherenceResponse = new AdherenceResponse();
        adherenceResponse.setEntityId("23");
        List<AdherenceResponse> adherenceResponseList = Collections.singletonList(adherenceResponse);
        AllAdherenceResponse allAdherenceResponse = new AllAdherenceResponse(adherenceResponseList);
        ResponseEntity<Response<AllAdherenceResponse>> iamResponse
                = new ResponseEntity<>(
                new Response<>(allAdherenceResponse),
                HttpStatus.OK
        );
        when(iamService.getAdherenceBulk(any())).thenReturn(iamResponse);

        episodeService.taskListSearch(episodeTasklistRequest);
    }

    @Test
    public void testTaskListTagList() {
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(1L, "client", "client", new Date()));
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest();
        episodeSearchRequest.setSortKey("id");
        episodeSearchRequest.setSortDirection(Sort.Direction.ASC);
        episodeSearchRequest.setSize(20);
        episodeSearchRequest.setPage(0);
        List<TaskListFilterRequest> taskListFilterRequestList = Collections.singletonList(new TaskListFilterRequest(TaskListFields.TASK_LIST_TAG_LIST, "[\"No_visit_recorded\",\"Follow_up_done_by_STS\"]", 36L));
        EpisodeTasklistRequest episodeTasklistRequest = new EpisodeTasklistRequest(episodeSearchRequest, taskListFilterRequestList);

        EpisodeIndex episodeIndex = new EpisodeIndex();
        Map<String, Object> stageDataMap = new HashMap<>();
        episodeIndex.setId("23");
        episodeIndex.setStartDate("2022-10-10 18:30:00");
        episodeIndex.setStageData(stageDataMap);
        EpisodeIndex episodeIndex2 = new EpisodeIndex();
        episodeIndex2.setId("24");
        episodeIndex2.setStartDate("2022-10-11 18:30:00");
        episodeIndex2.setStageData(stageDataMap);
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(20, Arrays.asList(episodeIndex2, episodeIndex), null);
        doReturn(episodeSearchResult).when(episodeService).elasticSearch(any());
        when(taskListFiltersToEpisodeSearchFields.transform(any())).thenReturn(episodeSearchRequest);
        EpisodeTagResponse episodeTagResponse = new EpisodeTagResponse();
        episodeTagResponse.setEpisodeId(23L);
        when(episodeTagService.search(any())).thenReturn(Collections.singletonList(episodeTagResponse));

        EpisodeSearchResult response = episodeService.taskListSearch(episodeTasklistRequest);
        Assert.assertEquals(1L, response.getTotalHits());
    }

    @Test(expected = ValidationException.class)
    public void testTaskListTagListException() {
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(1L, "client", "client", new Date()));
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest();
        episodeSearchRequest.setSortKey("id");
        episodeSearchRequest.setSortDirection(Sort.Direction.ASC);
        episodeSearchRequest.setSize(20);
        episodeSearchRequest.setPage(0);
        List<TaskListFilterRequest> taskListFilterRequestList = Collections.singletonList(new TaskListFilterRequest(TaskListFields.TASK_LIST_TAG_LIST, "{", 36L));
        EpisodeTasklistRequest episodeTasklistRequest = new EpisodeTasklistRequest(episodeSearchRequest, taskListFilterRequestList);

        EpisodeIndex episodeIndex = new EpisodeIndex();
        Map<String, Object> stageDataMap = new HashMap<>();
        episodeIndex.setId("23");
        episodeIndex.setStartDate("2022-10-10 18:30:00");
        episodeIndex.setStageData(stageDataMap);
        EpisodeIndex episodeIndex2 = new EpisodeIndex();
        episodeIndex2.setId("24");
        episodeIndex2.setStartDate("2022-10-11 18:30:00");
        episodeIndex2.setStageData(stageDataMap);
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(20, Arrays.asList(episodeIndex2, episodeIndex), null);
        doReturn(episodeSearchResult).when(episodeService).elasticSearch(any());
        when(taskListFiltersToEpisodeSearchFields.transform(any())).thenReturn(episodeSearchRequest);
        EpisodeTagResponse episodeTagResponse = new EpisodeTagResponse();
        episodeTagResponse.setEpisodeId(23L);
        when(episodeTagService.search(any())).thenReturn(Collections.singletonList(episodeTagResponse));

        EpisodeSearchResult response = episodeService.taskListSearch(episodeTasklistRequest);
        Assert.assertEquals(1L, response.getTotalHits());
    }

    @Test
    public void testTaskListPositiveDosingDays() {
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(1L, "client", "client", new Date()));

        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest();
        episodeSearchRequest.setSortKey("id");
        episodeSearchRequest.setSortDirection(Sort.Direction.ASC);
        episodeSearchRequest.setSize(20);
        episodeSearchRequest.setPage(0);
        List<TaskListFilterRequest> taskListFilterRequestList = Arrays.asList(
                new TaskListFilterRequest(TaskListFields.TASK_LIST_POSITIVE_DOSING_DAYS, "3", 1L),
                new TaskListFilterRequest(TaskListFields.TASK_LIST_MIN_DAYS_SINCE_TREATMENT_START, "3", 2L)
        );
        EpisodeTasklistRequest episodeTasklistRequest = new EpisodeTasklistRequest(episodeSearchRequest, taskListFilterRequestList);

        EpisodeIndex episodeIndex = new EpisodeIndex();
        Map<String, Object> stageDataMap = new HashMap<>();
        episodeIndex.setId("1");
        episodeIndex.setStartDate(Utils.getFormattedDateNew(LocalDateTime.now().minusDays(5)));
        episodeIndex.setStageData(stageDataMap);

        EpisodeIndex episodeIndex2 = new EpisodeIndex();
        episodeIndex2.setId("2");
        episodeIndex2.setStartDate(Utils.getFormattedDateNew(LocalDateTime.now().minusDays(2)));
        episodeIndex2.setStageData(stageDataMap);

        EpisodeIndex episodeIndex3 = new EpisodeIndex();
        episodeIndex3.setId("3");
        episodeIndex3.setStartDate(Utils.getFormattedDateNew(LocalDateTime.now().minusDays(5)));
        episodeIndex3.setStageData(stageDataMap);

        EpisodeIndex episodeIndex4 = new EpisodeIndex();
        episodeIndex4.setId("4");
        episodeIndex4.setStartDate(Utils.getFormattedDateNew(LocalDateTime.now().minusDays(2)));
        episodeIndex4.setStageData(stageDataMap);

        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(4, Arrays.asList(episodeIndex, episodeIndex2, episodeIndex3, episodeIndex4), null);
        doReturn(episodeSearchResult).when(episodeService).elasticSearch(any());

        AdherenceResponse adherenceResponse2 = new AdherenceResponse(LocalDateTime.now().minusDays(2), LocalDateTime.now().plusDays(60), "8494");
        adherenceResponse2.setEntityId("2");

        AdherenceResponse adherenceResponse3 = new AdherenceResponse(LocalDateTime.now().minusDays(2), LocalDateTime.now().plusDays(60), "84");
        adherenceResponse3.setEntityId("3");

        AdherenceResponse adherenceResponse4 = new AdherenceResponse(LocalDateTime.now().minusDays(2), LocalDateTime.now().plusDays(60), "6666");
        adherenceResponse4.setEntityId("4");
        when(iamService.getAdherenceBulk(any())).thenReturn(new ResponseEntity<>(new Response<>(true, new AllAdherenceResponse(Arrays.asList(adherenceResponse2, adherenceResponse3, adherenceResponse4))), HttpStatus.OK));

        when(taskListFiltersToEpisodeSearchFields.transform(any())).thenReturn(episodeSearchRequest);

        EpisodeSearchResult response = episodeService.taskListSearch(episodeTasklistRequest);
        Assert.assertEquals(3, response.getEpisodeIndexList().size());
        Assert.assertEquals(3, response.getTotalHits());
        Assert.assertTrue(response.getEpisodeIndexList().stream().anyMatch(episode -> episode.getId().equals("1")));
        Assert.assertTrue(response.getEpisodeIndexList().stream().anyMatch(episode -> episode.getId().equals("2")));
    }

    @Test
    public void testUpp() {
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest();
        episodeSearchRequest.setSortKey("id");
        episodeSearchRequest.setSortDirection(Sort.Direction.ASC);
        episodeSearchRequest.setSize(20);
        EpisodeUnifiedSearchRequest episodeUnifiedSearchRequest = new EpisodeUnifiedSearchRequest(episodeSearchRequest, new EpisodeUnifiedFilters());
        when(uppFiltersToEpisodeSearchFields.convert(any())).thenReturn(episodeSearchRequest);
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(1L, "client", "client", new Date()));

        EpisodeIndex episodeIndex = new EpisodeIndex();
        Map<String, Object> stageDataMap = new HashMap<>();
        episodeIndex.setId("23");
        episodeIndex.setStartDate("2022-10-10 18:30:00");
        episodeIndex.setStageData(stageDataMap);
        EpisodeIndex episodeIndex2 = new EpisodeIndex();
        episodeIndex2.setId("24");
        episodeIndex2.setStartDate("2022-10-11 18:30:00");
        episodeIndex2.setStageData(stageDataMap);
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(2, Arrays.asList(episodeIndex2, episodeIndex), null);
        doReturn(episodeSearchResult).when(episodeService).elasticSearch(any());

        AdherenceResponse adherenceResponse = new AdherenceResponse();
        adherenceResponse.setEntityId("23");
        adherenceResponse.setAdherenceString("99");
        List<AdherenceResponse> adherenceResponseList = Collections.singletonList(adherenceResponse);
        AllAdherenceResponse allAdherenceResponse = new AllAdherenceResponse(adherenceResponseList);
        ResponseEntity<Response<AllAdherenceResponse>> iamResponse
                = new ResponseEntity<>(
                new Response<>(allAdherenceResponse),
                HttpStatus.OK
        );
        when(iamService.getAdherenceBulk(any())).thenReturn(iamResponse);

        EpisodeTagResponse episodeTagResponse = new EpisodeTagResponse();
        episodeTagResponse.setEpisodeId(24L);
        when(episodeTagService.search(any())).thenReturn(Collections.singletonList(episodeTagResponse));

        EpisodeSearchResult response = episodeService.episodeUnifiedSearch(episodeUnifiedSearchRequest);
        Assert.assertEquals(2, response.getEpisodeIndexList().size());
        Assert.assertEquals("99", response.getEpisodeIndexList().get(0).getStageData().get(FieldConstants.FIELD_ADHERENCE_STRING));
    }
    

    @Test
    public void uploadEpisodeDocumentTest() throws IOException {
        String type = "type";
        String dateOfRecord = "2022-10-09 18:30:00";
        AddEpisodeDocumentRequest addEpisodeDocumentRequest = new AddEpisodeDocumentRequest(1L, 1L, Collections.singletonList(type), "test", "1111111111", Collections.singletonList(dateOfRecord),"notes", "Asia/Kolkata", "2022-10-19 00:00:00", null, null);
        EpisodeDocument episodeDocument = new EpisodeDocument(addEpisodeDocumentRequest);
        EpisodeDocumentDetails episodeDocumentDetails = new EpisodeDocumentDetails(addEpisodeDocumentRequest, 0, 1L, "test");
        String name = "file.txt";
        String originalFileName = "file.txt";
        String contentType = "text/plain";
        byte[] content = null;
        MultipartFile result = new MockMultipartFile(name,
                originalFileName, contentType, content);
        when(episodeDocumentRepository.save(any())).thenReturn(episodeDocument);
        when(episodeDocumentDetailsRepository.saveAll(any())).thenReturn(Collections.singletonList(episodeDocumentDetails));
        ReflectionTestUtils.setField(episodeService, "accountName", "aaroeverwell");
        ReflectionTestUtils.setField(episodeService, "containerName", "dev");
        ReflectionTestUtils.setField(episodeService, "accountKey", "lAUc2/jkgY3O/VyvVM7ukXZwg28jxv26lHq232srzBr86EqnlK2penf9uns53BJ0/sBmQOJLQTsR+ASt59dURQ==");
        episodeService.uploadEpisodeDocument(Collections.singletonList(result), addEpisodeDocumentRequest);
        Mockito.verify(episodeDocumentRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void getPersonDocumentDetailsTest() {
        Long personId = 1L;
        Client client = new Client();
        String type = "type";
        String dateOfRecord = "2022-10-09 18:30:00";
        AddEpisodeDocumentRequest addEpisodeDocumentRequest = new AddEpisodeDocumentRequest(1L, 1L, Collections.singletonList(type), "test", "1111111111", Collections.singletonList(dateOfRecord),"notes", "Asia/Kolkata", "2022-10-19 00:00:00", null, null);
        Episode episode = new Episode(1L, 29L, 1L, false, null, "1", Utils.getCurrentDateNew(), Utils.getCurrentDateNew(),
                null, Utils.getCurrentDateNew(), null, 1L, null, 1L);
        EpisodeDocument episodeDocument = new EpisodeDocument(addEpisodeDocumentRequest);
        when(episodeRepository.findAllByPersonIdInAndClientIdAndDeletedFalse(any(), any())).thenReturn(Collections.singletonList(episode));
        when(clientService.getClientByTokenOrDefault()).thenReturn(client);
        when(episodeDocumentRepository.findAllByEpisodeIdCustom(any())).thenReturn(Collections.singletonList(episodeDocument));
        List<EpisodeDocumentResponseDto> response = episodeService.getPersonDocumentDetails(personId, false);
        assertEquals(1L, Long.parseLong(String.valueOf(response.get(0).getEpisodeId())));
    }

    @Test
    public void testGetEpisodesForTreatmentInitiation() {
        NotificationTriggerDto notificationTriggerDto = new NotificationTriggerDto();
        notificationTriggerDto.setHierarchyId(1L);
        Map<String, Object> stageDataMap = new HashMap<>();
        stageDataMap.put(FieldConstants.EPISODE_FIELD_KEY_POPULATION, "Not Applicable");
        stageDataMap.put(FieldConstants.PERSON_FIELD_NAME, "test1");
        stageDataMap.put(FieldConstants.PERSON_PRIMARY_PHONE, "123");
        stageDataMap.put(FieldConstants.EPISODE_FILED_HIV_TEST_STATUS, "Positive");
        EpisodeIndex episodeIndex = getDefaultEpisodeIndex();
        episodeIndex.setStageData(stageDataMap);
        NotificationTriggerEvent notificationTriggerEvent = new NotificationTriggerEvent(notificationTriggerDto, new EpisodeSearchRequest());

        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(1, Collections.singletonList(episodeIndex), null);
        doReturn(episodeSearchResult).when(episodeService).elasticSearch(any());
        when(diagnosticsService.episodesWithTbTest(any())).thenReturn(Collections.emptyList());

        List<UserSmsDetails> response = episodeService.getEpisodesForTreatmentInitiation(notificationTriggerEvent, "29");
        Assert.assertEquals("123", response.get(0).getPhone());
    }

    @Test
    public void testGetEpisodesForAdherenceReminder() {
        NotificationTriggerDto notificationTriggerDto = new NotificationTriggerDto();
        notificationTriggerDto.setHierarchyId(1L);
        EpisodeIndex episodeIndex = getDefaultEpisodeIndex();
        Map<String, Object> stageDataMap = new HashMap<>();
        stageDataMap.put(FieldConstants.PERSON_FIELD_NAME, "test1");
        stageDataMap.put(FieldConstants.PERSON_PRIMARY_PHONE, "123");
        episodeIndex.setStageData(stageDataMap);
        NotificationTriggerEvent notificationTriggerEvent = new NotificationTriggerEvent(notificationTriggerDto, new EpisodeSearchRequest());

        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(1, Collections.singletonList(episodeIndex), null);
        doReturn(episodeSearchResult).when(episodeService).elasticSearch(any());

        List<UserSmsDetails> response = episodeService.getEpisodesForAdherenceReminder(notificationTriggerEvent, "29");
        Assert.assertEquals("123", response.get(0).getPhone());
    }

    @Test
    public void testGetEpisodesForFollowUp() {
        NotificationTriggerDto notificationTriggerDto = new NotificationTriggerDto();
        notificationTriggerDto.setHierarchyId(1L);
        EpisodeIndex episodeIndex = getDefaultEpisodeIndex();
        episodeIndex.setStartDate("2021-10-10 18:30:00");
        Map<String, Object> stageDataMap = new HashMap<>();
        stageDataMap.put(FieldConstants.PERSON_FIELD_NAME, "test1");
        stageDataMap.put(FieldConstants.PERSON_PRIMARY_PHONE, "123");
        episodeIndex.setStageData(stageDataMap);
        NotificationTriggerEvent notificationTriggerEvent = new NotificationTriggerEvent(notificationTriggerDto, new EpisodeSearchRequest());

        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(1, Collections.singletonList(episodeIndex), null);
        doReturn(episodeSearchResult).when(episodeService).elasticSearch(any());

        List<UserSmsDetails> response = episodeService.getEpisodesForFollowUp(notificationTriggerEvent, "29");
        Assert.assertEquals(0, response.size());
    }

    @Test
    public void testGetEpisodesForPostTreatmentReminder() {
        NotificationTriggerDto notificationTriggerDto = new NotificationTriggerDto();
        notificationTriggerDto.setHierarchyId(1L);
        EpisodeIndex episodeIndex = getDefaultEpisodeIndex();
        episodeIndex.setEndDate("2021-10-10 18:30:00");
        Map<String, Object> stageDataMap = new HashMap<>();
        stageDataMap.put(FieldConstants.PERSON_FIELD_NAME, "test1");
        stageDataMap.put(FieldConstants.PERSON_PRIMARY_PHONE, "123");
        episodeIndex.setStageData(stageDataMap);
        NotificationTriggerEvent notificationTriggerEvent = new NotificationTriggerEvent(notificationTriggerDto, new EpisodeSearchRequest());

        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(1, Collections.singletonList(episodeIndex), null);
        doReturn(episodeSearchResult).when(episodeService).elasticSearch(any());

        List<UserSmsDetails> response = episodeService.getEpisodesForPostTreatmentReminder(notificationTriggerEvent, "29");
        Assert.assertEquals(0, response.size());
    }

    @Test
    public void testGetEpisodesForEmptyContactTracing() {
        NotificationTriggerDto notificationTriggerDto = new NotificationTriggerDto();
        notificationTriggerDto.setHierarchyId(1L);
        EpisodeIndex episodeIndex = getDefaultEpisodeIndex();
        EpisodeIndex episodeIndex2 = new EpisodeIndex();
        episodeIndex2.setId("2");
        Map<String, Object> stageDataMap = new HashMap<>();
        stageDataMap.put(FieldConstants.PERSON_FIELD_NAME, "test1");
        stageDataMap.put(FieldConstants.PERSON_PRIMARY_PHONE, "123");
        episodeIndex.setStageData(stageDataMap);
        episodeIndex2.setStageData(stageDataMap);
        NotificationTriggerEvent notificationTriggerEvent = new NotificationTriggerEvent(notificationTriggerDto, new EpisodeSearchRequest());

        when(nikshayService.getEpisodesWithContactTracingFilter(any())).thenReturn(Collections.singletonList(2L));
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(2, Arrays.asList(episodeIndex, episodeIndex2), null);
        doReturn(episodeSearchResult).when(episodeService).elasticSearch(any());

        List<UserSmsDetails> finalResponse = episodeService.getEpisodesForEmptyContactTracing(notificationTriggerEvent, "29");
        Assert.assertEquals(1, finalResponse.size());
        Assert.assertEquals("123", finalResponse.get(0).getPhone());
    }


    @Test
    public void testGetEpisodesForEmptyBankDetails() {
        LocalDateTime presentDay = LocalDateTime.now();
        LocalDateTime startDate = presentDay.minusDays(15);
        NotificationTriggerDto notificationTriggerDto = new NotificationTriggerDto();
        notificationTriggerDto.setHierarchyId(1L);
        EpisodeIndex episodeIndex = getDefaultEpisodeIndex();
        episodeIndex.setTypeOfEpisode(Constants.PRIVATE_EPISODE_TYPE);
        episodeIndex.setStageData(new HashMap<>());
        episodeIndex.setStartDate(Utils.getFormattedDateNew(startDate));
        Map<String, Object> stageDataMap = new HashMap<>();
        stageDataMap.put(FieldConstants.PERSON_FIELD_NAME, "test1");
        stageDataMap.put(FieldConstants.PERSON_PRIMARY_PHONE, "123");
        episodeIndex.setStageData(stageDataMap);
        Client client = new Client(29L, "test", "test", null);

        when(clientService.getClientByTokenOrDefault()).thenReturn(client);
        when(diagnosticsService.episodesWithTbTest(any())).thenReturn(Collections.emptyList());
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(1, Collections.singletonList(episodeIndex), null);
        doReturn(episodeSearchResult).when(episodeService).elasticSearch(any());

        EmptyBankDetailsDto response = episodeService.getEpisodesForEmptyBankDetails(new EpisodeSearchRequest());
        Assert.assertEquals(1, response.getPersonIdSmsDetailsMap().size());
    }

    @Test
    public void testGetEpisodesForBenefitsCreated() {
        NotificationTriggerDto notificationTriggerDto = new NotificationTriggerDto();
        notificationTriggerDto.setHierarchyId(1L);
        EpisodeIndex episodeIndex = getDefaultEpisodeIndex();
        Map<String, Object> stageDataMap = new HashMap<>();
        stageDataMap.put(FieldConstants.PERSON_FIELD_NAME, "test1");
        stageDataMap.put(FieldConstants.PERSON_PRIMARY_PHONE, "123");
        episodeIndex.setStageData(stageDataMap);

        EpisodeIndex episodeIndex2 = new EpisodeIndex();
        Map<String, Object> stageDataMap2 = new HashMap<>();
        stageDataMap2.put(FieldConstants.PERSON_FIELD_NAME, "test1");
        stageDataMap2.put(FieldConstants.PERSON_PRIMARY_PHONE, "");
        episodeIndex2.setStageData(stageDataMap2);
        Client client = new Client(29L, "test", "test", null);
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(2, Arrays.asList(episodeIndex, episodeIndex2), null);


        when(clientService.getClientByTokenOrDefault()).thenReturn(client);
        doReturn(episodeSearchResult).when(episodeService).elasticSearch(any());

        List<BeneficiaryDetailsDto> response = episodeService.getEpisodesForBenefitCreated(new EpisodeSearchRequest());
        Assert.assertEquals(1, response.size());
        Assert.assertEquals(1L, (long) response.get(0).getEpisodeId());
        Assert.assertEquals(2L, (long) response.get(0).getPersonId());
    }

    @Test
    public void testGetEpisodesForInvalidBank() {
        NotificationTriggerDto notificationTriggerDto = new NotificationTriggerDto();
        notificationTriggerDto.setHierarchyId(1L);
        EpisodeIndex episodeIndex = getDefaultEpisodeIndex();
        Map<String, Object> stageDataMap = new HashMap<>();
        stageDataMap.put(FieldConstants.PERSON_FIELD_NAME, "test1");
        stageDataMap.put(FieldConstants.PERSON_PRIMARY_PHONE, "123");
        episodeIndex.setStageData(stageDataMap);
        Client client = new Client(29L, "test", "test", null);

        when(clientService.getClientByTokenOrDefault()).thenReturn(client);
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(1, Collections.singletonList(episodeIndex), null);
        doReturn(episodeSearchResult).when(episodeService).elasticSearch(any());

        List<UserSmsDetails> response = episodeService.getEpisodesForInvalidBankDetails(new EpisodeSearchRequest());
        Assert.assertEquals(1, response.size());
        Assert.assertEquals(1L, (long) response.get(0).getId());
        Assert.assertEquals("123", response.get(0).getPhone());
    }

    @Test
    public void getPersonDocumentDetailsTestForAll() {
        Map<String, Object> users = new HashMap<>();
        users.put("id", 1L);
        Long personId = 1L;
        String type = "type";
        String dateOfRecord = "2022-10-09 18:30:00";
        Client client = new Client();
        Episode episode = new Episode(1L, 29L, 1L, false, null, "1", Utils.getCurrentDateNew(), Utils.getCurrentDateNew(),
                null, Utils.getCurrentDateNew(), null, 1L, null, 1L);
        AddEpisodeDocumentRequest request = new AddEpisodeDocumentRequest(1L, 1L, Collections.singletonList(type), "test", "1111111111", Collections.singletonList(dateOfRecord), "notes", "Asia/Kolkata", "19-10-2022", null, null);
        EpisodeDocument episodeDocument = new EpisodeDocument(request);
        when(episodeRepository.findAllByPersonIdInAndClientIdAndDeletedFalse(any(), any())).thenReturn(Collections.singletonList(episode));
        when(episodeDocumentRepository.findAllByEpisodeIdCustom(any())).thenReturn(Collections.singletonList(episodeDocument));
        when(clientService.getClientByTokenOrDefault()).thenReturn(client);
        doReturn(new ResponseEntity<>(new Response(true, Collections.singletonList(users)), HttpStatus.OK)).when(userService).getUserRelations(eq(1L), eq("ACCEPT"));
        List<EpisodeDocumentResponseDto> response = episodeService.getPersonDocumentDetails(personId, true);
        assertEquals(1L, Long.parseLong(String.valueOf(response.get(0).getEpisodeId())));
    }

    @Test
    public void getEpisodeIdsWithClientIdAndScheduleTimeMorningTest() {
        Long clientId = 1L;
        String timeZone = "Asia/Kolkata";
        when(diseaseTemplateService.getAllFieldsByNameIn(any())).thenReturn(Arrays.asList(new Field(1L, "treatmentPlanId", "Episode", null)));
        when(episodeStageDataRepository.getEpisodeFieldIdValueMapForFieldIdsInAndClientId(any(), any())).thenReturn(getEpisodeFieldIdValueDtos());
        when(treatmentPlanService.getTreatmentPlanAggregatedScheduleStringMapForTreatmentPlanIds(any(), any())).thenReturn(getTreatmentPlanToScheduleStringMap());
        List<String> episodesToReceiveNotification = episodeService.getEpisodeIdsWithClientIdAndScheduleTimeIndex(clientId, ScheduleTimeEnum.MORNING.getScheduleStringIndex(), timeZone);
        Assert.assertEquals(2, episodesToReceiveNotification.size());
        List<String> expectedEpisodeIds = Arrays.asList("1", "5");
        for(String episodeId : episodesToReceiveNotification) {
            assertTrue(expectedEpisodeIds.contains(episodeId));
        }
    }

    @Test
    public void getEpisodeIdsWithClientIdAndScheduleTimeAfternoonTest() {
        Long clientId = 1L;
        String timeZone = "Asia/Kolkata";
        when(diseaseTemplateService.getAllFieldsByNameIn(any())).thenReturn(Arrays.asList(new Field(1L, "treatmentPlanId", "Episode", null)));
        when(episodeStageDataRepository.getEpisodeFieldIdValueMapForFieldIdsInAndClientId(any(), any())).thenReturn(getEpisodeFieldIdValueDtos());
        when(treatmentPlanService.getTreatmentPlanAggregatedScheduleStringMapForTreatmentPlanIds(any(), any())).thenReturn(getTreatmentPlanToScheduleStringMap());
        List<String> episodesToReceiveNotification = episodeService.getEpisodeIdsWithClientIdAndScheduleTimeIndex(clientId, ScheduleTimeEnum.AFTERNOON.getScheduleStringIndex(), timeZone);
        Assert.assertEquals(3, episodesToReceiveNotification.size());
        List<String> expectedEpisodeIds = Arrays.asList("2", "5", "6");
        for(String episodeId : episodesToReceiveNotification) {
            assertTrue(expectedEpisodeIds.contains(episodeId));
        }
    }

    @Test
    public void getEpisodeIdsWithClientIdAndScheduleTimeEveningTest() {
        Long clientId = 1L;
        String timeZone = "Asia/Kolkata";
        when(diseaseTemplateService.getAllFieldsByNameIn(any())).thenReturn(Arrays.asList(new Field(1L, "treatmentPlanId", "Episode", null)));
        when(episodeStageDataRepository.getEpisodeFieldIdValueMapForFieldIdsInAndClientId(any(), any())).thenReturn(getEpisodeFieldIdValueDtos());
        when(treatmentPlanService.getTreatmentPlanAggregatedScheduleStringMapForTreatmentPlanIds(any(), any())).thenReturn(getTreatmentPlanToScheduleStringMap());
        List<String> episodesToReceiveNotification = episodeService.getEpisodeIdsWithClientIdAndScheduleTimeIndex(clientId, ScheduleTimeEnum.EVENING.getScheduleStringIndex(), timeZone);
        Assert.assertEquals(3, episodesToReceiveNotification.size());
        List<String> expectedEpisodeIds = Arrays.asList("3", "6", "7");
        for(String episodeId : episodesToReceiveNotification) {
            assertTrue(expectedEpisodeIds.contains(episodeId));
        }
    }

    @Test
    public void getEpisodeIdsWithClientIdAndScheduleTimeNightTest() {
        Long clientId = 1L;
        String timeZone = "Asia/Kolkata";
        when(diseaseTemplateService.getAllFieldsByNameIn(any())).thenReturn(Arrays.asList(new Field(1L, "treatmentPlanId", "Episode", null)));
        when(episodeStageDataRepository.getEpisodeFieldIdValueMapForFieldIdsInAndClientId(any(), any())).thenReturn(getEpisodeFieldIdValueDtos());
        when(treatmentPlanService.getTreatmentPlanAggregatedScheduleStringMapForTreatmentPlanIds(any(), any())).thenReturn(getTreatmentPlanToScheduleStringMap());
        List<String> episodesToReceiveNotification = episodeService.getEpisodeIdsWithClientIdAndScheduleTimeIndex(clientId, ScheduleTimeEnum.NIGHT.getScheduleStringIndex(), timeZone);
        Assert.assertEquals(2, episodesToReceiveNotification.size());
        List<String> expectedEpisodeIds = Arrays.asList("4", "7");
        for(String episodeId : episodesToReceiveNotification) {
            assertTrue(expectedEpisodeIds.contains(episodeId));
        }
    }

    private List<EpisodeFieldIdValueDtoInterface> getEpisodeFieldIdValueDtos() {
        List<EpisodeFieldIdValueDtoInterface> response = new ArrayList<>();
        response.add(new EpisodeFieldIdValue(1L, 11L, "21"));
        response.add(new EpisodeFieldIdValue(2L, 12L, "22"));
        response.add(new EpisodeFieldIdValue(3L, 13L, "23"));
        response.add(new EpisodeFieldIdValue(4L, 14L, "24"));
        response.add(new EpisodeFieldIdValue(5L, 15L, "25"));
        response.add(new EpisodeFieldIdValue(6L, 16L, "26"));
        response.add(new EpisodeFieldIdValue(7L, 17L, "27"));
        return response;
    }

    private Map<Long, String> getTreatmentPlanToScheduleStringMap() {
        Map<Long, String> response = new HashMap<>();
        response.put(21L, "1***");
        response.put(22L, "*1**");
        response.put(23L, "**1*");
        response.put(24L, "***1");
        response.put(25L, "11**");
        response.put(26L, "*11*");
        response.put(27L, "**11");
        return response;
    }

    @Test
    public void test_getMorningDoseReminderPNDtos() {
        Map<String, List<String>> userDeviceIdsMap = new HashMap<>();
        List<String> deviceIdList = new ArrayList<>();
        deviceIdList.add("1");
        userDeviceIdsMap.put("userNames", deviceIdList);
        DeviceIdsMapResponse deviceIdsMapResponse = new DeviceIdsMapResponse(userDeviceIdsMap);
        ResponseEntity<Response<DeviceIdsMapResponse>> deviceIdResponse = new ResponseEntity<>(new Response<>(true, deviceIdsMapResponse), HttpStatus.OK);
        doReturn(new ArrayList<>()).when(episodeService).getEpisodeIdsWithClientIdAndScheduleTimeIndex(any(), eq(ScheduleTimeEnum.MORNING.getScheduleStringIndex()), any());
        when(ssoService.fetchDeviceIdUserMap(any())).thenReturn(deviceIdResponse);
        episodeService.getMorningDoseReminderPNDtosForClientIdAndSchedule(1L, null);
        verify(episodeService, Mockito.times(1)).getEpisodeIdsWithClientIdAndScheduleTimeIndex(any(), eq(ScheduleTimeEnum.MORNING.getScheduleStringIndex()), any());
        verify(episodeService, Mockito.times(1)).getDeviceIdList(any());
    }

    @Test
    public void test_getAfternoonDoseReminderPNDtos() {
        Map<String, List<String>> userDeviceIdsMap = new HashMap<>();
        List<String> deviceIdList = new ArrayList<>();
        deviceIdList.add("1");
        userDeviceIdsMap.put("userNames", deviceIdList);
        DeviceIdsMapResponse deviceIdsMapResponse = new DeviceIdsMapResponse(userDeviceIdsMap);
        ResponseEntity<Response<DeviceIdsMapResponse>> deviceIdResponse = new ResponseEntity<>(new Response<>(true, deviceIdsMapResponse), HttpStatus.OK);
        doReturn(new ArrayList<>()).when(episodeService).getEpisodeIdsWithClientIdAndScheduleTimeIndex(any(), eq(ScheduleTimeEnum.AFTERNOON.getScheduleStringIndex()), any());
        when(ssoService.fetchDeviceIdUserMap(any())).thenReturn(deviceIdResponse);
        episodeService.getAfternoonDoseReminderPNDtosForClientIdAndSchedule(1L, null);
        verify(episodeService, Mockito.times(1)).getEpisodeIdsWithClientIdAndScheduleTimeIndex(any(), eq(ScheduleTimeEnum.AFTERNOON.getScheduleStringIndex()), any());
        verify(episodeService, Mockito.times(1)).getDeviceIdList(any());
    }

    @Test
    public void test_getEveningDoseReminderPNDtos() {
        Map<String, List<String>> userDeviceIdsMap = new HashMap<>();
        List<String> deviceIdList = new ArrayList<>();
        deviceIdList.add("1");
        userDeviceIdsMap.put("userNames", deviceIdList);
        DeviceIdsMapResponse deviceIdsMapResponse = new DeviceIdsMapResponse(userDeviceIdsMap);
        ResponseEntity<Response<DeviceIdsMapResponse>> deviceIdResponse = new ResponseEntity<>(new Response<>(true, deviceIdsMapResponse), HttpStatus.OK);
        doReturn(new ArrayList<>()).when(episodeService).getEpisodeIdsWithClientIdAndScheduleTimeIndex(any(), eq(ScheduleTimeEnum.EVENING.getScheduleStringIndex()), any());
        when(ssoService.fetchDeviceIdUserMap(any())).thenReturn(deviceIdResponse);
        episodeService.getEveningDoseReminderPNDtosForClientIdAndSchedule(1L, null);
        verify(episodeService, Mockito.times(1)).getEpisodeIdsWithClientIdAndScheduleTimeIndex(any(), eq(ScheduleTimeEnum.EVENING.getScheduleStringIndex()), any());
        verify(episodeService, Mockito.times(1)).getDeviceIdList(any());
    }

    @Test
    public void test_getNightDoseReminderPNDtos() {
        Map<String, List<String>> userDeviceIdsMap = new HashMap<>();
        List<String> deviceIdList = new ArrayList<>();
        deviceIdList.add("1");
        userDeviceIdsMap.put("userNames", deviceIdList);
        DeviceIdsMapResponse deviceIdsMapResponse = new DeviceIdsMapResponse(userDeviceIdsMap);
        ResponseEntity<Response<DeviceIdsMapResponse>> deviceIdResponse = new ResponseEntity<>(new Response<>(true, deviceIdsMapResponse), HttpStatus.OK);
        doReturn(new ArrayList<>()).when(episodeService).getEpisodeIdsWithClientIdAndScheduleTimeIndex(any(), eq(ScheduleTimeEnum.NIGHT.getScheduleStringIndex()), any());
        when(ssoService.fetchDeviceIdUserMap(any())).thenReturn(deviceIdResponse);
        episodeService.getNightDoseReminderPNDtosForClientIdAndSchedule(1L, null);
        verify(episodeService, Mockito.times(1)).getEpisodeIdsWithClientIdAndScheduleTimeIndex(any(), eq(ScheduleTimeEnum.NIGHT.getScheduleStringIndex()), any());
        verify(episodeService, Mockito.times(1)).getDeviceIdList(any());
    }

    @Test
    public void test_getEpisodesForPersonListFromElastic() {
        Long clientId = 1L;
        List<Long> personIdList = new ArrayList<>();
        personIdList.add(1L);
        List<EpisodeIndex> episodeIndexList = new ArrayList<>();
        EpisodeIndex episodeIndex = new EpisodeIndex();
        episodeIndex.setClientId(1L);
        episodeIndex.setId("1");
        episodeIndex.setTypeOfEpisode("Test");
        episodeIndex.setPersonId(1L);
        episodeIndexList.add(episodeIndex);
        when(esEpisodeService.findAllByPersonIdList(any(), any())).thenReturn(episodeIndexList);
        List<EpisodeDto> episodeDtoList = episodeService.getEpisodesForPersonListFromElastic(personIdList, clientId);
        Assert.assertEquals(episodeIndexList.size(), episodeDtoList.size());
        Assert.assertEquals(episodeIndexList.get(0).getTypeOfEpisode(), episodeDtoList.get(0).getTypeOfEpisode());
        Assert.assertEquals(episodeIndexList.get(0).getPersonId(), episodeDtoList.get(0).getPersonId());
        Mockito.verify(esEpisodeService, Mockito.times(1)).findAllByPersonIdList(any(), any());
    }

    @Test
    public void test_getEpisodeWithDeleted() {
        Long episodeId = 1L;
        Long clientId = 1L;
        Episode episode = new Episode();
        episode.setPersonId(10L);
        when(episodeRepository.findByIdAndClientId(eq(episodeId), eq(clientId))).thenReturn(episode);
        Episode episodeReturn = episodeService.getEpisodeWithDeleted(episodeId, clientId);
        Assert.assertEquals(episode.getPersonId(), episodeReturn.getPersonId());
        verify(episodeRepository, Mockito.times(1)).findByIdAndClientId(any(), any());
    }

    @Test
    public void testGetAddUserRelationPnDtos() {
        Map<String, List<String>> userDeviceIdsMap = new HashMap<>();
        List<String> deviceIdList = new ArrayList<>();
        deviceIdList.add("1");
        userDeviceIdsMap.put("userNames", deviceIdList);
        DeviceIdsMapResponse deviceIdsMapResponse = new DeviceIdsMapResponse(userDeviceIdsMap);
        ResponseEntity<Response<DeviceIdsMapResponse>> deviceIdResponse = new ResponseEntity<>(new Response<>(true, deviceIdsMapResponse), HttpStatus.OK);
        when(ssoService.fetchDeviceIdUserMap(any())).thenReturn(deviceIdResponse);
        Map<String, String> mobileListMap1 = new HashMap<>();
        mobileListMap1.put("number", "124432432");
        List<Map<String, String>> mobileList1 = new ArrayList<>();
        mobileList1.add(mobileListMap1);
        EpisodeDto episodeDto = new EpisodeDto();
        episodeDto.setId(1L);
        Map<String, Object> stageData = new HashMap<>();
        stageData.put(FieldConstants.PERSON_FIELD_FIRST_NAME, "test");
        stageData.put(FieldConstants.PERSON_MOBILE_LIST, mobileList1);
        episodeDto.setStageData(stageData);
        Map<String, String> mobileListMap2 = new HashMap<>();
        mobileListMap2.put("number", "914511");
        List<Map<String, String>> mobileList2 = new ArrayList<>();
        mobileList2.add(mobileListMap2);
        EpisodeDto episodeDto2 = new EpisodeDto();
        episodeDto2.setId(2L);
        Map<String, Object> stageData2 = new HashMap<>();
        stageData2.put(FieldConstants.PERSON_FIELD_FIRST_NAME, "primary");
        stageData2.put(FieldConstants.PERSON_MOBILE_LIST, mobileList2);
        episodeDto2.setStageData(stageData2);
        doReturn(Collections.singletonList(episodeDto2)).when(episodeService).getEpisodesForPersonList(any(), eq(171L), eq(true));

        Map<String, Object> userRelation = new HashMap<>();
        userRelation.put(UserServiceField.PRIMARY_USER_ID.getFieldName(), 123);
        PNDtoWithNotificationData data = episodeService.getAddUserRelationPnDtos(171L, episodeDto, userRelation);
        Assert.assertEquals("1", data.getPushNotificationTemplateDtoList().get(0).getId());
        Assert.assertEquals("914511", data.getPushNotificationTemplateDtoList().get(0).getContentParameters().get(NotificationConstants.PARAM_PHONE));
        Assert.assertEquals("primary", data.getPushNotificationTemplateDtoList().get(0).getContentParameters().get(NotificationConstants.PARAM_NAME));

    }

    
    @Test(expected = ValidationException.class)
    public void testUpdateOnTreatmentEpisodeEndDateInvalidEndDate() {
        EpisodeIndex episodeIndex = new EpisodeIndex();
        episodeIndex.setStageName("DIAGNOSED_ON_TREATMENT_PUBLIC");
        episodeIndex.setStartDate("2022-05-29 18:30:00");
        episodeIndex.setEndDate("2022-05-30 18:30:00");
        episodeIndex.setCurrentStageId(1L);
        episodeIndex.setId("1");

        episodeService.updateOnTreatmentEpisodeEndDate(episodeIndex.convertToDto(), 1L, null, null, Utils.toLocalDateTimeWithNull("29-05-2022 18:30:00"));
    }

    @Test
    public void testUpdateOnTreatmentEpisodeEndDate() {
        EpisodeIndex episodeIndex = new EpisodeIndex();
        episodeIndex.setStageName("DIAGNOSED_ON_TREATMENT_PUBLIC");
        episodeIndex.setStartDate("2022-05-29 18:30:00");
        episodeIndex.setEndDate("2022-05-30 18:30:00");
        episodeIndex.setId("123");
        episodeIndex.setCurrentStageId(1L);
        Client client = new Client(29L, "test", "password", null);

        when(clientService.getClientByTokenOrDefault()).thenReturn(client);
        when(episodeLogService.save(any())).thenReturn(null);
        doReturn(null).when(episodeService).processUpdateEpisode(eq(29L), any());

        episodeService.updateOnTreatmentEpisodeEndDate(episodeIndex.convertToDto(), 1L, null, null, Utils.toLocalDateTimeWithNull("31-05-2022 18:30:00"));

        Mockito.verify(episodeLogService, Mockito.times(1)).save(any());
    }

    @Test
    public void TestGetAaroRefillPNTest() {
        Client client = new Client();
        Map<Long, List<DiseaseStageFieldMapDto>> longListMap = new HashMap<>();
        List<EpisodeStageData> episodeStageDataList = new ArrayList<>();
        episodeStageDataList.add(new EpisodeStageData(1L, 1L, 1L, "1"));
        List list = new ArrayList();
        list.add(new DiseaseStageFieldMapDto(1L, 1L, 1L, 1L, 1L, "treatmentPlanId", "Module1", true, false, "0"));
        longListMap.put(1L, list);
        List<Episode> episodes = getEpisodesList_countTwo();
        EpisodeStage episodeStage = new EpisodeStage(1L, 1L, 1L, Utils.getCurrentDateNew().minusDays(4L), Utils.getCurrentDateNew().minusDays(3));
        List<Episode> episodeList = new ArrayList<>();
        Episode episode = new Episode(1L, 29L, 1L, false, null, "1", Utils.getCurrentDateNew(), Utils.getCurrentDateNew(),
                null, Utils.getCurrentDateNew(), null, 1L, null, 1L);
        EpisodeDto episodeDto = new EpisodeDto(episode, new ArrayList<>(), new Stage(),
                new DiseaseTemplate(),
                null,
                null,
                null,
                new ArrayList<TabPermissionDto>(),
                new HashMap<>());
        episodeDto.getStageData().put(FieldConstants.ESD_TREATMENT_PLAN_ID, 3);
        episodeList.add(episode);
        Map<String, List<String>> userDeviceIdsMap = new HashMap<>();
        List<String> deviceIdList = new ArrayList<>();
        deviceIdList.add("1");
        userDeviceIdsMap.put("1", deviceIdList);
        DeviceIdsMapResponse deviceIdsMapResponse = new DeviceIdsMapResponse(userDeviceIdsMap);
        ResponseEntity<Response<DeviceIdsMapResponse>> deviceIdResponse = new ResponseEntity<>(new Response<>(true, deviceIdsMapResponse), HttpStatus.OK);
        when(clientService.getClientByTokenOrDefault()).thenReturn(client);
        when(episodeRepository.findAllByClientIdAndDeletedFalse(eq(29L))).thenReturn(episodeList);
        List<TreatmentPlanProductMapDto> mappedProducts = new ArrayList();
        TreatmentPlanProductMapDto product = new TreatmentPlanProductMapDto(LocalDateTime.now(), LocalDateTime.now(), null, 1L, "11 111", LocalDateTime.now().plusDays(1), null, null, null, false);
        mappedProducts.add(product);
        TreatmentPlanDto treatmentPlanDto = new TreatmentPlanDto(1L, "treatmentPlan1", mappedProducts, new ArrayList<>(), null);
        when(treatmentPlanService.getPlanDetails(any(), any())).thenReturn(treatmentPlanDto);
        List<ProductResponseWithScheduleDto> productResponseWithScheduleDtos = new ArrayList<>();
        productResponseWithScheduleDtos.add(new ProductResponseWithScheduleDto(new ProductResponse(1L, "code", "1", "11 111", "", "", ""), "11 111", LocalDateTime.now().plusDays(1)));
        productResponseWithScheduleDtos.add(new ProductResponseWithScheduleDto(new ProductResponse(2L, "code", "2", "11 111", "", "", ""), "11 111", LocalDateTime.now().plusDays(7)));
        productResponseWithScheduleDtos.add(new ProductResponseWithScheduleDto(new ProductResponse(3L, "code", "3", "11 111", "", "", ""), "11 111", LocalDateTime.now().plusDays(8)));
        productResponseWithScheduleDtos.add(new ProductResponseWithScheduleDto(new ProductResponse(4L, "code", "4", "11 111", "", "", ""), "11 111", LocalDateTime.now()));
        productResponseWithScheduleDtos.add(new ProductResponseWithScheduleDto(new ProductResponse(5L, "code", "5", "11 111", "", "", ""), "11 111", LocalDateTime.now().minusDays(1)));
        when(dispensationService.getProductWithSchedulesFromTreatmentPlan(any())).thenReturn(productResponseWithScheduleDtos);
        when(ssoService.fetchDeviceIdUserMap(any())).thenReturn(deviceIdResponse); //used
        when(stageRepository.findAllById(any())).thenReturn(getStageList_testCase2()).thenReturn(getStageList_testCase2());
        when(diseaseTemplateService.getStageToTabPermissionsListMapForStageIds(any(), any(), any())).thenReturn(getStageToTabPermissions_testCase2());

        List<ProductResponse> productResponse = new ArrayList<>();
        productResponse.add(new ProductResponse(1L, "e", "1", " ", "", "", ""));
        productResponse.add(new ProductResponse(2L, "e", "2", " ", "", "", ""));
        productResponse.add(new ProductResponse(3L, "e", "3", " ", "", "", ""));
        productResponse.add(new ProductResponse(4L, "e", "4", " ", "", "", ""));
        productResponse.add(new ProductResponse(5L, "e", "5", " ", "", "", ""));

        ResponseEntity<Response<List<ProductResponse>>> response = new ResponseEntity<>(new Response<>(productResponse), HttpStatus.OK);
        when(dispensationService.searchProducts(any())).thenReturn(response);

        List<TreatmentPlanProductMap> allTreatmentPlans = new ArrayList<>();
        allTreatmentPlans.add(new TreatmentPlanProductMap(2L, 2L, "111 1111", LocalDateTime.now(), LocalDateTime.now(), LocalDateTime.now().plusDays(1), null, "Capsule", false));
        allTreatmentPlans.add(new TreatmentPlanProductMap(3L, 3L, "111 1111", LocalDateTime.now(), LocalDateTime.now(), LocalDateTime.now().minusDays(1), null, "Capsule", false));
        allTreatmentPlans.add(new TreatmentPlanProductMap(3L, 4L, "111 1111", LocalDateTime.now(), LocalDateTime.now(), LocalDateTime.now().minusDays(1), null, "Capsule", false));
        allTreatmentPlans.add(new TreatmentPlanProductMap(5L, 5L, "111 1111", LocalDateTime.now(), LocalDateTime.now(), LocalDateTime.now().plusDays(7), null, "Capsule", false));

        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(10, new ArrayList<>(), new HashMap<>());
        episodeSearchResult.getEpisodeIndexList().add(EpisodeIndex.convert(episodeDto, 29L));
        when(esEpisodeService.search(any(), anyMap(), eq(false))).thenReturn(episodeSearchResult);
        when(diseaseTemplateService.getDiseaseIdTemplateMap(Collections.singletonList(any()))).thenReturn(getDiseaseIdTemplateMap_testCase1());
        when(episodeStageRepository.getEpisodeStagesByEpisodeIdInOrderByStartDateAsc(any())).thenReturn(Collections.singletonList(episodeStage));
        when(diseaseTemplateService.getDefinedStageIdKeyMappingsListMap(any(), any())).thenReturn(longListMap);
        when(episodeStageDataRepository.findAllByEpisodeStageIdIn(any())).thenReturn(episodeStageDataList);
        when(episodeAssociationRepository.getAllByEpisodeIdIn(any())).thenReturn(getEpisodeAssociationsForTwoEpisodes());
        when(episodeHierarchyLinkageRepository.getAllByEpisodeIdIn(any())).thenReturn(getEpisodeHierarchyLinkagesForTwoEpisodes());
        when(episodeTagService.getEpisodeTags(eq(episodes.get(0).getId()))).thenReturn(new EpisodeTagResponse(episodes.get(0).getId()
                , new ArrayList<>(), Arrays.asList("aaaa", "cccc")));
        when(episodeTagService.getEpisodeTags(eq(episodes.get(1).getId()))).thenReturn(new EpisodeTagResponse(episodes.get(1).getId()
                , new ArrayList<>(), Arrays.asList("bbbb", "dddd")));
        when(supportedRelationRepository.findAll()).thenReturn(getAllSupportedRelation());

        when(treatmentPlanProductMapRepository.findAllActiveInRefillDateRange(any(), any())).thenReturn(allTreatmentPlans);
        PNDtoWithNotificationData pushNotificationTemplateDtoList = episodeService.getAaroRefillReminderPNDTOsForClientId(29L, Constants.IST_TIMEZONE);
        if (LocalDateTime.ofInstant(Utils.getCurrentDate().toInstant(), ZoneId.of(Constants.IST_TIMEZONE)).getDayOfWeek() != DayOfWeek.FRIDAY) {
            assertEquals(0, pushNotificationTemplateDtoList.getPushNotificationTemplateDtoList().stream().count());
        } else {
            assertNotNull(pushNotificationTemplateDtoList.getPushNotificationTemplateDtoList().get(0).getDeviceId());
            assertEquals("3, 4", pushNotificationTemplateDtoList.getPushNotificationTemplateDtoList()
                    .get(0).getContentParameters().get(NotificationConstants.PARAM_COUNT_OR_MEDICINE));
        }
    }

    @Test
    public void TestGetAaroRefillPNTodayTest() {
        Client client = new Client();
        Map<Long, List<DiseaseStageFieldMapDto>> longListMap = new HashMap<>();
        List<EpisodeStageData> episodeStageDataList = new ArrayList<>();
        episodeStageDataList.add(new EpisodeStageData(1L, 1L, 1L, "1"));
        List list = new ArrayList();
        list.add(new DiseaseStageFieldMapDto(1L, 1L, 1L, 1L, 1L, "treatmentPlanId", "Module1", true, false, "0"));
        longListMap.put(1L, list);
        List<Episode> episodes = getEpisodesList_countTwo();
        EpisodeStage episodeStage = new EpisodeStage(1L, 1L, 1L, Utils.getCurrentDateNew().minusDays(4L), Utils.getCurrentDateNew().minusDays(3));
        List<Episode> episodeList = new ArrayList<>();
        Episode episode = new Episode(1L, 29L, 1L, false, null, "1", Utils.getCurrentDateNew(), Utils.getCurrentDateNew(),
                null, Utils.getCurrentDateNew(), null, 1L, null, 1L);
        EpisodeDto episodeDto = new EpisodeDto(episode, new ArrayList<>(), new Stage(),
                new DiseaseTemplate(),
                null,
                null,
                null,
                new ArrayList<TabPermissionDto>(),
                new HashMap<>());
        episodeDto.getStageData().put(FieldConstants.ESD_TREATMENT_PLAN_ID, 3);
        episodeList.add(episode);
        Map<String, List<String>> userDeviceIdsMap = new HashMap<>();
        List<String> deviceIdList = new ArrayList<>();
        deviceIdList.add("1");
        userDeviceIdsMap.put("1", deviceIdList);
        DeviceIdsMapResponse deviceIdsMapResponse = new DeviceIdsMapResponse(userDeviceIdsMap);
        ResponseEntity<Response<DeviceIdsMapResponse>> deviceIdResponse = new ResponseEntity<>(new Response<>(true, deviceIdsMapResponse), HttpStatus.OK);
        when(clientService.getClientByTokenOrDefault()).thenReturn(client);
        when(episodeRepository.findAllByClientIdAndDeletedFalse(eq(29L))).thenReturn(episodeList);
        List<TreatmentPlanProductMapDto> mappedProducts = new ArrayList();
        TreatmentPlanProductMapDto product = new TreatmentPlanProductMapDto(LocalDateTime.now(), LocalDateTime.now(), null, 1L, "11 111", LocalDateTime.now().plusDays(1), null, null, null, false);
        mappedProducts.add(product);
        TreatmentPlanDto treatmentPlanDto = new TreatmentPlanDto(1L, "treatmentPlan1", mappedProducts, new ArrayList<>(), null);
        when(treatmentPlanService.getPlanDetails(any(), any())).thenReturn(treatmentPlanDto);
        List<ProductResponseWithScheduleDto> productResponseWithScheduleDtos = new ArrayList<>();
        productResponseWithScheduleDtos.add(new ProductResponseWithScheduleDto(new ProductResponse(4L, "code", "4", "11 111", "", "", ""), "11 111", LocalDateTime.now()));
        when(dispensationService.getProductWithSchedulesFromTreatmentPlan(any())).thenReturn(productResponseWithScheduleDtos);
        when(ssoService.fetchDeviceIdUserMap(any())).thenReturn(deviceIdResponse); //used
        when(stageRepository.findAllById(any())).thenReturn(getStageList_testCase2()).thenReturn(getStageList_testCase2());
        when(diseaseTemplateService.getStageToTabPermissionsListMapForStageIds(any(), any(), any())).thenReturn(getStageToTabPermissions_testCase2());

        List<ProductResponse> productResponse = new ArrayList<>();
        productResponse.add(new ProductResponse(1L, "e", "1", " ", "", "", ""));
        productResponse.add(new ProductResponse(2L, "e", "2", " ", "", "", ""));
        productResponse.add(new ProductResponse(3L, "e", "3333333333333333333333333333333333333", " ", "", "", ""));
        productResponse.add(new ProductResponse(4L, "e", "4", " ", "", "", ""));
        productResponse.add(new ProductResponse(5L, "e", "5", " ", "", "", ""));

        ResponseEntity<Response<List<ProductResponse>>> response = new ResponseEntity<>(new Response<>(productResponse), HttpStatus.OK);
        when(dispensationService.searchProducts(any())).thenReturn(response);

        List<TreatmentPlanProductMap> allTreatmentPlans = new ArrayList<>();
        allTreatmentPlans.add(new TreatmentPlanProductMap(2L, 2L, "111 1111", LocalDateTime.now(), LocalDateTime.now(), LocalDateTime.now().plusDays(1), null, "Capsule", false));
        allTreatmentPlans.add(new TreatmentPlanProductMap(3L, 3L, "111 1111", LocalDateTime.now(), LocalDateTime.now(), LocalDateTime.now().minusDays(1), null, "Capsule", false));
        allTreatmentPlans.add(new TreatmentPlanProductMap(3L, 4L, "111 1111", LocalDateTime.now(), LocalDateTime.now(), LocalDateTime.now(), null, "Capsule", false));
        allTreatmentPlans.add(new TreatmentPlanProductMap(5L, 5L, "111 1111", LocalDateTime.now(), LocalDateTime.now(), LocalDateTime.now().plusDays(7), null, "Capsule", false));

        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(10, new ArrayList<>(), new HashMap<>());
        episodeSearchResult.getEpisodeIndexList().add(EpisodeIndex.convert(episodeDto, 29L));
        when(esEpisodeService.search(any(), anyMap(), eq(false))).thenReturn(episodeSearchResult);
        when(diseaseTemplateService.getDiseaseIdTemplateMap(Collections.singletonList(any()))).thenReturn(getDiseaseIdTemplateMap_testCase1());
        when(episodeStageRepository.getEpisodeStagesByEpisodeIdInOrderByStartDateAsc(any())).thenReturn(Collections.singletonList(episodeStage));
        when(diseaseTemplateService.getDefinedStageIdKeyMappingsListMap(any(), any())).thenReturn(longListMap);
        when(episodeStageDataRepository.findAllByEpisodeStageIdIn(any())).thenReturn(episodeStageDataList);
        when(episodeAssociationRepository.getAllByEpisodeIdIn(any())).thenReturn(getEpisodeAssociationsForTwoEpisodes());
        when(episodeHierarchyLinkageRepository.getAllByEpisodeIdIn(any())).thenReturn(getEpisodeHierarchyLinkagesForTwoEpisodes());
        when(episodeTagService.getEpisodeTags(eq(episodes.get(0).getId()))).thenReturn(new EpisodeTagResponse(episodes.get(0).getId()
                , new ArrayList<>(), Arrays.asList("aaaa", "cccc")));
        when(episodeTagService.getEpisodeTags(eq(episodes.get(1).getId()))).thenReturn(new EpisodeTagResponse(episodes.get(1).getId()
                , new ArrayList<>(), Arrays.asList("bbbb", "dddd")));
        when(supportedRelationRepository.findAll()).thenReturn(getAllSupportedRelation());

        when(treatmentPlanProductMapRepository.findAllActiveInRefillDateRange(any(), any())).thenReturn(allTreatmentPlans);
        PNDtoWithNotificationData pushNotificationTemplateDtoList = episodeService.getAaroRefillTodayReminderPNDTOs(29L, Constants.IST_TIMEZONE);
        assertNotNull(pushNotificationTemplateDtoList.getPushNotificationTemplateDtoList().get(0).getDeviceId());
        assertEquals("2", pushNotificationTemplateDtoList.getPushNotificationTemplateDtoList()
                .get(0).getContentParameters().get(NotificationConstants.PARAM_COUNT_OR_MEDICINE));
    }
    
    @Test
    public void getAppointmentConfirmationPNDtoTest() {
        Trigger trigger = new Trigger();
        trigger.setTimeZone("Asia/Kolkata");
        when(triggerRepository.getTriggerByEventName(any())).thenReturn(trigger);
        Map<String, Object> stageData = new HashMap<>();
        stageData.put(FieldConstants.PERSON_FIELD_FIRST_NAME, "test");
        Map<String, Object> extraData = new HashMap<>();
        extraData.put(AppointmentConstants.PATIENT_NAME, "patient");
        extraData.put(AppointmentConstants.DOCTOR_NAME, "doctor");
        extraData.put(AppointmentConstants.APPOINTMENT_DATE, LocalDateTime.now());
        EpisodeDto episodeDto = new EpisodeDto(1L, 1L, 1L, true, "DSTB", 1L, null, Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, Utils.getCurrentDateNew(), null, null, 1L,"PRESUMPTIVE_OPEN", "Presumptive Open", "IndiaTbPublic", null, null, null, null, null, stageData);
        Map<String, List<String>> userDeviceIdsMap = new HashMap<>();
        List<String> deviceIdList = new ArrayList<>();
        deviceIdList.add("1");
        userDeviceIdsMap.put("userNames", deviceIdList);
        DeviceIdsMapResponse deviceIdsMapResponse = new DeviceIdsMapResponse(userDeviceIdsMap);
        ResponseEntity<Response<DeviceIdsMapResponse>> deviceIdResponse = new ResponseEntity<>(new Response<>(true, deviceIdsMapResponse), HttpStatus.OK);
        when(ssoService.fetchDeviceIdUserMap(any())).thenReturn(deviceIdResponse);
        PNDtoWithNotificationData response = episodeService.getAppointmentConfirmationPNDto(171L, episodeDto, extraData);
        assertEquals("1", response.getPushNotificationTemplateDtoList().get(0).getDeviceId());
    }

    @Test
    public void getEpisodeDocumentDetailsTestOnlyEpisodeFilter() {
        Long episodeId = 1L;
        Client client = new Client();
        String type = "type";
        String dateOfRecord = "2022-10-09 18:30:00";
        AddEpisodeDocumentRequest addEpisodeDocumentRequest = new AddEpisodeDocumentRequest(1L, 1L, Collections.singletonList(type), "test", "1111111111", Collections.singletonList(dateOfRecord),"notes", "Asia/Kolkata", "2022-10-19 00:00:00", 7945L, "Staff");
        EpisodeDocument episodeDocument = new EpisodeDocument(addEpisodeDocumentRequest);
        doReturn(getEpisodeDtoEntity_testCase1()).when(episodeService).getEpisodeDetails(any(), any(), eq(false), eq(false), eq(false), eq(false));
        when(clientService.getClientByTokenOrDefault()).thenReturn(client);
        when(episodeDocumentRepository.findAllByEpisodeIdCustom(any())).thenReturn(Collections.singletonList(episodeDocument));
        EpisodeDocumentSearchRequest searchRequest = new EpisodeDocumentSearchRequest(Collections.singletonList(episodeId), null, null, null);
        List<EpisodeDocumentResponseDto> response = episodeService.getEpisodeDocumentDetails(searchRequest);
        assertEquals(1L, Long.parseLong(String.valueOf(response.get(0).getEpisodeId())));
    }

    @Test
    public void getEpisodeDocumentDetailsTestOnlyAddedOnAndApprovedFilter() {
        Long episodeId = 1L;
        Client client = new Client();
        String type = "type";
        String dateOfRecord = "2022-10-09 18:30:00";
        AddEpisodeDocumentRequest addEpisodeDocumentRequest = new AddEpisodeDocumentRequest(1L, 1L, Collections.singletonList(type), "test", "1111111111", Collections.singletonList(dateOfRecord),"notes", "Asia/Kolkata", "2022-10-19 00:00:00", 7945L, "Staff");
        EpisodeDocument episodeDocument = new EpisodeDocument(addEpisodeDocumentRequest);
        doReturn(getEpisodeDtoEntity_testCase1()).when(episodeService).getEpisodeDetails(any(), any(), eq(false), eq(false), eq(false), eq(false));
        when(clientService.getClientByTokenOrDefault()).thenReturn(client);
        when(episodeDocumentRepository.findAllByEpisodeIdAndAddedOnBetweenAndApprovedCustom(any(), any(), any(), any())).thenReturn(Collections.singletonList(episodeDocument));
        EpisodeDocumentSearchRequest searchRequest = new EpisodeDocumentSearchRequest(Collections.singletonList(episodeId), "2022-10-10 18:30:00", "2022-11-10 18:30:00", true);
        List<EpisodeDocumentResponseDto> response = episodeService.getEpisodeDocumentDetails(searchRequest);
        assertEquals(1L, Long.parseLong(String.valueOf(response.get(0).getEpisodeId())));
    }

    @Test
    public void getEpisodeDocumentDetailsTestOnlyAddedOnAndFilter() {
        Long episodeId = 1L;
        Client client = new Client();
        String type = "type";
        String dateOfRecord = "2022-10-09 18:30:00";
        AddEpisodeDocumentRequest addEpisodeDocumentRequest = new AddEpisodeDocumentRequest(1L, 1L, Collections.singletonList(type), "test", "1111111111", Collections.singletonList(dateOfRecord),"notes", "Asia/Kolkata", "2022-10-19 00:00:00", 7945L, "Staff");
        EpisodeDocument episodeDocument = new EpisodeDocument(addEpisodeDocumentRequest);
        doReturn(getEpisodeDtoEntity_testCase1()).when(episodeService).getEpisodeDetails(any(), any(), eq(false), eq(false), eq(false), eq(false));
        when(clientService.getClientByTokenOrDefault()).thenReturn(client);
        when(episodeDocumentRepository.findAllByEpisodeIdAndAddedOnBetweenCustom(any(), any(), any())).thenReturn(Collections.singletonList(episodeDocument));
        EpisodeDocumentSearchRequest searchRequest = new EpisodeDocumentSearchRequest(Collections.singletonList(episodeId), "2022-10-10 18:30:00", "2022-11-10 18:30:00", null);
        List<EpisodeDocumentResponseDto> response = episodeService.getEpisodeDocumentDetails(searchRequest);
        assertEquals(1L, Long.parseLong(String.valueOf(response.get(0).getEpisodeId())));
    }

    @Test(expected = NotFoundException.class)
    public void test_getEpisodeDocumentDetailsByEpisodeDocumentDetailsId_throwsNotFoundException() {
        Long episodeDocumentDetailsId = 1L;
        Client client = new Client();
        String type = "type";
        String dateOfRecord = "2022-10-09 18:30:00";
        AddEpisodeDocumentRequest addEpisodeDocumentRequest = new AddEpisodeDocumentRequest(1L, 1L, Collections.singletonList(type), "test", "1111111111", Collections.singletonList(dateOfRecord),"notes", "Asia/Kolkata", "2022-10-19 00:00:00", 7945L, "Staff");
        doReturn(getEpisodeDtoEntity_testCase1()).when(episodeService).getEpisodeDetails(any(), any(), eq(false), eq(false), eq(false), eq(false));
        when(clientService.getClientByTokenOrDefault()).thenReturn(client);
        when(episodeDocumentDetailsRepository.findById(eq(1L))).thenReturn(Optional.empty());
        episodeService.getEpisodeDocumentDetailsByEpisodeDocumentDetailsId(episodeDocumentDetailsId);
    }

    @Test
    public void test_getEpisodeDocumentDetailsByEpisodeDocumentDetailsId_success() {
        Long episodeDocumentDetailsId = 1L;
        Client client = new Client();
        String type = "type";
        String dateOfRecord = "2022-10-09 18:30:00";
        AddEpisodeDocumentRequest addEpisodeDocumentRequest = new AddEpisodeDocumentRequest(1L, 1L, Collections.singletonList(type), "test", "1111111111", Collections.singletonList(dateOfRecord),"notes", "Asia/Kolkata", "2022-10-19 00:00:00", 7945L, "Staff");
        EpisodeDocument episodeDocument = new EpisodeDocument(addEpisodeDocumentRequest);
        EpisodeDocumentDetails episodeDocumentDetails = new EpisodeDocumentDetails(1L, 1L, "Test", "testUrl.com", LocalDateTime.now(), null, null, null, LocalDateTime.now(), null);
        episodeDocument.setEpisodeDocumentDetailsList(Collections.singletonList(episodeDocumentDetails));
        doReturn(getEpisodeDtoEntity_testCase1()).when(episodeService).getEpisodeDetails(any(), any(), eq(false), eq(false), eq(false), eq(false));
        when(clientService.getClientByTokenOrDefault()).thenReturn(client);
        when(episodeDocumentDetailsRepository.findById(eq(1L))).thenReturn(Optional.of(episodeDocumentDetails));
        when(episodeDocumentRepository.findById(any())).thenReturn(Optional.of(episodeDocument));
        List<EpisodeDocumentResponseDto> response = episodeService.getEpisodeDocumentDetailsByEpisodeDocumentDetailsId(episodeDocumentDetailsId);
        assertEquals(1L, Long.parseLong(String.valueOf(response.get(0).getEpisodeId())));
    }

    @Test
    public void getEpisodeReports() {
        EpisodeSearchRequest episodeSearchRequest = new EpisodeSearchRequest();
        episodeSearchRequest.setSortKey("id");
        episodeSearchRequest.setSortDirection(Sort.Direction.ASC);
        episodeSearchRequest.setSize(20);
        EpisodeIndex episodeIndex = new EpisodeIndex();
        episodeIndex.setId("1");
        EpisodeSearchResult episodeSearchResult = new EpisodeSearchResult(1L, Collections.singletonList(episodeIndex), null, "", "");
        Client testClient = new Client(1L, "test", "test", new Date(), 1L, "test");
        when(clientService.getClientByTokenOrDefault()).thenReturn(testClient);

        when(reportFiltersToEpisodeSearchFields.convert(any())).thenReturn(episodeSearchRequest);
        doReturn(episodeSearchResult).when(episodeService).elasticSearch(any());

        EpisodeSearchResult episodeSearchResultResponse = episodeService.episodeReports(new EpisodeReportsRequest(new EpisodeSearchRequest(), new EpisodeReportsFilter()));

        Assert.assertNotNull(episodeSearchResultResponse);
        Assert.assertEquals(1L, episodeSearchResultResponse.getTotalHits());
    }

    @Test
    public void testUpdateEpisodeDoc() {
        EpisodeDocumentDetails episodeDocumentDetails = new EpisodeDocumentDetails(1L, 1L, "Test", "testUrl.com", LocalDateTime.now(), null, null, null, null, null);
        when(episodeDocumentDetailsRepository.findById(eq(1L))).thenReturn(Optional.of(episodeDocumentDetails));

        episodeService.updateEpisodeDocumentDetails(1L, true, 7945L);

        Mockito.verify(episodeDocumentDetailsRepository, Mockito.times(1)).save(any());
    }

    @Test(expected = NotFoundException.class)
    public void testUpdateEpisodeDocNotFound() {
        when(episodeDocumentDetailsRepository.findById(eq(1L))).thenReturn(Optional.empty());

        episodeService.updateEpisodeDocumentDetails(1L, true, 7945L);
    }

    @Test(expected = ValidationException.class)
    public void testUpdateEpisodeDocAlreadyUpdated() {
        EpisodeDocumentDetails episodeDocumentDetails = new EpisodeDocumentDetails(1L, 1L, "Test", "testUrl.com", LocalDateTime.now(), false, null, null, null, null);
        when(episodeDocumentDetailsRepository.findById(eq(1L))).thenReturn(Optional.of(episodeDocumentDetails));

        episodeService.updateEpisodeDocumentDetails(1L, true, 7945L);
    }

}

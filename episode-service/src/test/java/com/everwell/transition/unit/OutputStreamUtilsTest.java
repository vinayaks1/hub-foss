package com.everwell.transition.unit;

import com.everwell.transition.BaseTest;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.utils.OutputStreamUtils;
import org.junit.Test;
import org.mockito.InjectMocks;

import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OutputStreamUtilsTest extends BaseTest {

    @InjectMocks
    OutputStreamUtils outStreamUtils;

    @Test
    public void testWriteToOutputStream() throws IOException {
        String testMessage = "test response";
        Response<String> customResponse = new Response<>(testMessage);
        HttpStatus status = HttpStatus.OK;
        MockHttpServletResponse httpServletResponse = new MockHttpServletResponse();
        outStreamUtils.writeCustomResponseToOutputStream(httpServletResponse, customResponse, status);
        assertEquals(httpServletResponse.getStatus(),status.value());
        assertEquals(httpServletResponse.getContentType(),"application/json");
        assertTrue(httpServletResponse.getContentAsString().contains(testMessage));
    }

}

package com.everwell.transition.unit.controller;

import com.everwell.transition.BaseTest;
import com.everwell.transition.controller.AndroidConfigController;
import com.everwell.transition.exceptions.CustomExceptionHandler;
import com.everwell.transition.model.response.android.GenericUiResponse;
import com.everwell.transition.service.AndroidConfigService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
public class AndroidConfigControllerTest extends BaseTest {

    @InjectMocks
    private AndroidConfigController androidConfigController;

    @Mock
    AndroidConfigService androidConfigService;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(androidConfigController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void getKnowledgeFitFeedTest() throws Exception {
        String region = "IN";
        // Create a list of GenericUiResponse objects to return from the service
        List<GenericUiResponse> responseList = new ArrayList<>();
        responseList.add(new GenericUiResponse("Title 1", "subtitle 1", 1, "", "", "", "", null, "", "", false, "", false));
        responseList.add(new GenericUiResponse("Title 2", "subtitle 2", 2, "", "", "", "", null, "", "", false, "", false));

        // Mock the service method to return the responseList
        when(androidConfigService.getFitFeed(anyString(), eq(region))).thenReturn(responseList);

        // Perform the GET request and expect a 200 OK response
        mockMvc.perform(get("/v1/feed/knowledge/12345")
                        .param("language", "en")
                        .param("region", "IN")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success", is(true)))
                .andExpect(jsonPath("$.data", hasSize(2)))
                .andExpect(jsonPath("$.data[0].title", is("Title 1")))
                .andExpect(jsonPath("$.data[1].title", is("Title 2")));

        // Verify that the service method was called with the correct language parameter
        verify(androidConfigService).getFitFeed("en", region);
    }

    @Test
    public void getFitFeedTest() throws Exception {
        String region = "IN";
        // Create a list of GenericUiResponse objects to return from the service
        List<GenericUiResponse> responseList = new ArrayList<>();
        responseList.add(new GenericUiResponse("Title 1", "subtitle 1", 1, "", "", "", "", null, "", "", false, "", false));
        responseList.add(new GenericUiResponse("Title 2", "subtitle 2", 2, "", "", "", "", null, "", "", false, "", false));

        // Mock the service method to return the responseList
        when(androidConfigService.getHomeFeed(anyString(), eq(region))).thenReturn(responseList);

        // Perform the GET request and expect a 200 OK response
        mockMvc.perform(get("/v1/feed/home/12345")
                        .param("language", "en")
                        .param("region", "IN")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success", is(true)))
                .andExpect(jsonPath("$.data", hasSize(2)))
                .andExpect(jsonPath("$.data[0].title", is("Title 1")))
                .andExpect(jsonPath("$.data[1].title", is("Title 2")));

        // Verify that the service method was called with the correct language parameter
        verify(androidConfigService).getHomeFeed("en", region);
    }
}

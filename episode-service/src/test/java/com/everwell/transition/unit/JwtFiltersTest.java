package com.everwell.transition.unit;

import com.everwell.transition.BaseTest;
import com.everwell.transition.constants.Constants;
import com.everwell.transition.filters.JwtFilters;
import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.SSOService;
import com.everwell.transition.utils.CacheUtils;
import com.everwell.transition.utils.JwtUtils;
import com.everwell.transition.utils.OutputStreamUtils;
import com.everwell.transition.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class JwtFiltersTest extends BaseTest {

    @InjectMocks
    JwtFilters jwtFilter;

    @Mock
    HttpServletRequest httpServletRequest;

    @Mock
    HttpServletResponse httpServletResponse;

    @Mock
    FilterChain filterChain;

    @Mock
    OutputStreamUtils outStreamUtils;

    @Spy
    JwtUtils jwtUtils;

    @Mock
    ClientService clientService;

    @InjectMocks
    CacheUtils cacheUtils;

    @Mock
    RedisTemplate<String, Object> redisTemplate;

    @Mock
    private ValueOperations<String, Object> valueOperations;

    Constants constants;

    private static final Long dummy_client_id = 1L;
    private static final String dummy_user = "dummy_user";
    private static final String password = "dummy_password";
    private String jwtToken;

    private static final String SIGNATURE_NO_MATCH = "JWT signature does not match locally computed signature. JWT validity cannot be asserted and should not be trusted.";
    private static final String UNSUPPORTED_KEY_TYPE = "Key bytes can only be specified for HMAC signatures. Please specify a PublicKey or PrivateKey instance.";

    @Before
    public void setUp() {
        jwtUtils.setSecretKey("abcd");
        jwtToken = jwtUtils.generateToken(String.valueOf(dummy_client_id));
        constants = new Constants();
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
        doReturn(null).when(valueOperations).get(anyString());
    }

    @Test
    public void testDoFilterInternalSuccess() throws ServletException, IOException {

        Client dummy_client = new Client(1L, dummy_user, password, Utils.getCurrentDate());
        String authorizationHeader = "Bearer "+jwtToken;

        when(httpServletRequest.getHeader(anyString())).thenReturn(authorizationHeader);
        when(clientService.getClient(any())).thenReturn(dummy_client);
        doNothing().when(filterChain).doFilter(any(), any());

        jwtFilter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);

        Mockito.verify(filterChain, Mockito.times(1)).doFilter(any(), any());
        Mockito.verify(SecurityContextHolder.getContext(), Mockito.times(1)).setAuthentication(any());
    }

    @Test
    public void testDoFilterInternalExpiredToken() throws ServletException, IOException, ParseException {

        Client dummy_client = new Client(1L, dummy_user, password, Utils.getCurrentDate());
        String authorizationHeader = "Bearer "+jwtToken;
        Date oldExpiration = Utils.convertStringToDate("2019-09-11 13:00:00");
        doReturn(oldExpiration).when(jwtUtils).getExpirationDateFromToken(anyString());
        when(httpServletRequest.getHeader(anyString())).thenReturn(authorizationHeader);
        when(clientService.getClient(any())).thenReturn(dummy_client);
        doNothing().when(filterChain).doFilter(any(), any());

        jwtFilter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);
        Mockito.verify(filterChain, Mockito.times(1)).doFilter(any(), any());
        Mockito.verify(SecurityContextHolder.getContext(), Mockito.times(0)).setAuthentication(any());
    }

    @Test
    public void testDoFilterInternalBlacklistToken() throws ServletException, IOException, ParseException {

        Client dummy_client = new Client(1L, dummy_user, password, Utils.getCurrentDate());
        String authorizationHeader = "Bearer "+jwtToken;
        doReturn(1).when(valueOperations).get(eq(Constants.BLACKLIST_TOKEN + jwtToken));
        Date oldExpiration = Utils.convertStringToDate("2019-09-11 13:00:00");
        doReturn(oldExpiration).when(jwtUtils).getExpirationDateFromToken(anyString());
        when(httpServletRequest.getHeader(anyString())).thenReturn(authorizationHeader);
        when(clientService.getClient(any())).thenReturn(dummy_client);
        doNothing().when(filterChain).doFilter(any(), any());

        jwtFilter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);
        Mockito.verify(filterChain, Mockito.times(1)).doFilter(any(), any());
        Mockito.verify(SecurityContextHolder.getContext(), Mockito.times(0)).setAuthentication(any());
    }

    @Test
    public void testDoFilterInternal_Wrong_ClientId() throws ServletException, IOException, ParseException {

        Client dummy_client = new Client(2L, dummy_user, password, Utils.getCurrentDate());
        String authorizationHeader = "Bearer "+jwtToken;

        when(httpServletRequest.getHeader(anyString())).thenReturn(authorizationHeader);
        when(clientService.getClient(any())).thenReturn(dummy_client);
        doNothing().when(filterChain).doFilter(any(), any());

        jwtFilter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);

        Mockito.verify(filterChain, Mockito.times(1)).doFilter(any(), any());
        Mockito.verify(SecurityContextHolder.getContext(), Mockito.times(0)).setAuthentication(any());
    }



    @Test
    public void testDoFilterInternalIncorrectJWT() throws ServletException, IOException {

        Client dummy_client = new Client(1L, dummy_user, password, Utils.getCurrentDate());
        String wrongJWT = jwtToken.substring(0,jwtToken.length() - 2) + "xy";
        String authorizationHeader = "Bearer "+wrongJWT;

        when(httpServletRequest.getHeader(anyString())).thenReturn(authorizationHeader);
        when(clientService.getClient(any())).thenReturn(dummy_client);
        doNothing().when(filterChain).doFilter(eq(httpServletRequest), eq(httpServletResponse));
        doNothing().when(outStreamUtils).writeCustomResponseToOutputStream(any(), any(), any());

        jwtFilter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);

        Response<String> expectedResponse = new Response<>(SIGNATURE_NO_MATCH);

        Mockito.verify(filterChain, Mockito.times(0)).doFilter(eq(httpServletRequest), eq(httpServletResponse));
        Mockito.verify(outStreamUtils, Mockito.times(1)).writeCustomResponseToOutputStream(eq(httpServletResponse), eq(expectedResponse), any());
    }

    @Test
    public void testDoFilterInternalMalformedJWT() throws ServletException, IOException {

        Client dummy_client = new Client(1L, dummy_user, password, Utils.getCurrentDate());
        String malformedJWT = "qwertyuo";
        String authorizationHeader = "Bearer "+malformedJWT;

        when(httpServletRequest.getHeader(anyString())).thenReturn(authorizationHeader);
        when(clientService.getClient(any())).thenReturn(dummy_client);
        doNothing().when(filterChain).doFilter(any(), any());
        doNothing().when(outStreamUtils).writeCustomResponseToOutputStream(any(), any(), any());

        jwtFilter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);

        Response<String> expectedResponse = new Response<>(constants.MALFORMED_JWT);

        Mockito.verify(filterChain, Mockito.times(0)).doFilter(any(), any());
        Mockito.verify(outStreamUtils, Mockito.times(1)).writeCustomResponseToOutputStream(any(), eq(expectedResponse), any());
    }

    @Test
    public void testDoFilterInternalUnsupportedJWT() throws ServletException, IOException {

        Client dummy_client = new Client(1L, dummy_user, password, Utils.getCurrentDate());
        String unsupportedES256Token = "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWUsImlhdCI6MTUxNjIzOTAyMn0.tyh-VfuzIxCyGYDlkBA7DfyjrqmSHu6pQ2hoZuFqUSLPNY2N0mpHb3nk5K17HWP_3cYHBw7AhHale5wky6-sVA";
        String authorizationHeader = "Bearer "+unsupportedES256Token;

        when(httpServletRequest.getHeader(anyString())).thenReturn(authorizationHeader);
        when(clientService.getClient(any())).thenReturn(dummy_client);
        doNothing().when(filterChain).doFilter(any(), any());
        doNothing().when(outStreamUtils).writeCustomResponseToOutputStream(any(), any(), any());

        jwtFilter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);

        Response<String> expectedResponse = new Response<>(UNSUPPORTED_KEY_TYPE);

        Mockito.verify(filterChain, Mockito.times(0)).doFilter(any(), any());
        Mockito.verify(outStreamUtils, Mockito.times(1)).writeCustomResponseToOutputStream(any(), eq(expectedResponse), any());
    }

    @Test
    public void testDoFilterInternalNullAuthorizationHeader() throws ServletException, IOException {

        String authorizationHeader = null;

        when(httpServletRequest.getHeader(anyString())).thenReturn(authorizationHeader);
        doNothing().when(filterChain).doFilter(any(), any());

        jwtFilter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);

        Mockito.verify(filterChain, Mockito.times(1)).doFilter(any(), any());
    }

    @Test
    public void testDoFilterInternalAuthorizationHeaderNotBearer() throws ServletException, IOException {

        String authorizationHeader = "abcd";

        when(httpServletRequest.getHeader(anyString())).thenReturn(authorizationHeader);
        doNothing().when(filterChain).doFilter(any(), any());

        jwtFilter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);

        Mockito.verify(filterChain, Mockito.times(1)).doFilter(any(), any());
    }

    @Test
    public void testDoFilterInternalAuthorizationHeaderUndefined() throws ServletException, IOException {

        String authorizationHeader = "Bearer undefined";

        when(httpServletRequest.getHeader(anyString())).thenReturn(authorizationHeader);
        doNothing().when(filterChain).doFilter(any(), any());

        jwtFilter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);

        Mockito.verify(filterChain, Mockito.times(1)).doFilter(any(), any());
    }

    @Test
    public void testShouldNotFilterTrue() {

        when(httpServletRequest.getRequestURI()).thenReturn("/v1/client");
        assertTrue(jwtFilter.shouldNotFilter(httpServletRequest));
    }

}

package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.constants.INSConstants;
import com.everwell.transition.model.response.AdherenceCodeConfigResponse;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.response.ins.TypeResponse;
import com.everwell.transition.postconstruct.INSTypeMap;
import com.everwell.transition.service.AggregationService;
import com.everwell.transition.service.INSService;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.*;

import static org.mockito.Mockito.when;


public class INSTypeMapTest extends BaseTest {

    @Spy
    @InjectMocks
    INSTypeMap insTypeMap;

    @Mock
    private INSService insService;

    @Test
    public void createInsTypeMapMapTest() {
        Map<String, Object> typeMap = new HashMap<>();
        typeMap.put(INSConstants.FIELD_TYPE, "SMS");
        typeMap.put(INSConstants.FIELD_TYPE_ID, "1");
        TypeResponse response = new TypeResponse(Collections.singletonList(typeMap));
        when(insService.getNotificationTypes()).thenReturn(response);
        insTypeMap.createNotificationTypeMap();
        Assert.assertEquals(1L, (long) insTypeMap.notificationTypeMap.get(INSConstants.TYPE_SMS));
    }
}

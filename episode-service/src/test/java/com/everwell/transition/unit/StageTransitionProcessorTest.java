package com.everwell.transition.unit;

import com.everwell.transition.BaseTest;
import com.everwell.transition.binders.PatientProcessorBinder;
import com.everwell.transition.exceptions.NotFoundException;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.dto.EventStreamingDto;
import com.everwell.transition.processor.StageTransitionProcessor;
import com.everwell.transition.service.EpisodeService;
import com.everwell.transition.service.StageTransitionService;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.messaging.MessageChannel;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class StageTransitionProcessorTest extends BaseTest {

    @InjectMocks
    StageTransitionProcessor stageTransitionProcessor;

    @Mock
    PatientProcessorBinder patientProcessorBinder;

    @Mock
    StageTransitionService stageTransitionService;

    @Mock
    MessageChannel stageOutput;

    @Mock
    EpisodeService episodeService;

    @Before
    public void init() {
        when(patientProcessorBinder.stageOutput()).thenReturn(stageOutput);
    }

    @Test
    public void stageTransitionHandler_nullInput() {
        stageTransitionProcessor.stageTransitionHandler(null,null);
        Mockito.verify(episodeService, Mockito.times(0))
            .processEpisodeRequestInputForStageTransition(any(), any());
    }

    @Test
    public void stageTransitionHandler_incorrectInputMap() {
        stageTransitionProcessor.stageTransitionHandler(null, getInputMap_incorrect());
        Mockito.verify(episodeService, Mockito.times(0))
                .processEpisodeRequestInputForStageTransition(any(), any());
    }

    @Test
    public void stageTransitionHandler_failsSilentlyForNotFoundEpisode() throws IOException {
        stageTransitionProcessor.stageTransitionHandler(null, getInputMap_correct());
        Mockito.verify(episodeService, Mockito.times(1))
                .processEpisodeRequestInputForStageTransition(any(), any());
    }

    HashMap<String, Object> getInputMap_incorrect() {
        HashMap<String, Object> map = new HashMap<>();
        return map;
    }

    Map<String, Object> getInputMap_correct() throws IOException {
        Map<String, Object> map = new HashMap<>();
        String jsonData = "{\"eventName\":\"patient_enrollment\",\"field\":{\"Id\":10712725,\"UserId\":123223,\"Deleted\":false,\"AddedTimestamp\":\"2021-02-03T20:36:02.9471744\",\"FirstName\":\"Testing\",\"LastName\":\"Test\",\"Gender\":\"Female\",\"Language\":null,\"Age\":12,\"Occupation\":\"Unknown\",\"Address\":\"test\",\"PrimaryPhone\":\"1413241234\",\"PreARTNumber\":null,\"ARTNumber\":null,\"EnrollmentDate\":null,\"EndDate\":null,\"HierarchyMapping_Initiation\":null,\"HierarchyMapping_Residence\":324,\"TypeOfPatient\":\"IndiaTbPublic\",\"CurrentSchedule\":null,\"CurrentTags\":\"New_Enrollment\",\"TreatmentOutcome\":null,\"AttentionRequired\":null,\"AdherenceStatus\":null,\"LastDosage\":null,\"WeightBand\":null,\"NewOrPreviouslyTreated\":null,\"SiteOfDisease\":null,\"EPSite\":null,\"TypeOfCase\":null,\"HIVTestStatus\":\"Unknown\",\"NikshayId\":null,\"TBNumber\":null,\"TBCategory\":null,\"MermId\":null,\"DrugRegimen\":null,\"TBTreatmentStartDate\":null,\"AddedBy\":7945,\"DeploymentCode\":\"IND\",\"AdherenceString\":null,\"RegistrationDate\":\"2021-02-03T20:36:02.9315483\",\"FathersName\":null,\"Pincode\":\"123412\",\"Taluka\":null,\"Town\":null,\"Ward\":null,\"Landmark\":null,\"Stage\":\"PRESUMPTIVE_OPEN\",\"DiagnosisDate\":null,\"DiagnosisBasis\":null,\"MonitoringMethod\":null,\"TreatmentType\":null,\"DrugSusceptibility\":null,\"RefillMonitoring\":null,\"Area\":\"Unknown\",\"MaritalStatus\":\"Unknown\",\"SocioeconomicStatus\":\"Unknown\",\"KeyPopulation\":\"NotApplicable\",\"ContactPersonName\":null,\"ContactPersonAddress\":null,\"ContactPersonPhone\":null,\"MonthsOfTreatment\":null,\"MonthsSinceEpisode\":null,\"TreatmentSource\":null,\"Height\":null,\"CPInitiationDate\":null,\"RegimenType\":null,\"DrugResistance\":null,\"HierarchyMapping_Enrollment\":285624,\"HierarchyMapping_Diagnosed\":null,\"HierarchyMapping_Current\":285624,\"HierarchyMapping_ART\":null,\"HierarchyMapping_DRTB\":null,\"RemarksOutcome\":null,\"MedicalOfficerOutcome\":null,\"NotificationTestId\":null,\"BeneficiaryId\":45036,\"MigrationStatus\":null,\"Latitude\":null,\"Longitude\":null,\"HierarchyMapping_PVTHUB\":null,\"ParentEpisodeId\":null,\"IsDuplicate\":false,\"DuplicateStatus\":null,\"DuplicateOf\":null,\"IsIAMEnabled\":false,\"HierarchyMapping_Outcome\":null,\"LastActivityDate\":\"2021-02-03T20:36:02.9471744\",\"TransferId\":null,\"ForegoBenefits\":false}}";
        return Utils.jsonToObject(jsonData, new TypeReference<Map<String, Object>>() {});
    }

}

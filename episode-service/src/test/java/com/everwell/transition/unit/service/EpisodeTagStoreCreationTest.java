package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.model.db.EpisodeTagStore;
import com.everwell.transition.model.response.TagsConfigResponse;
import com.everwell.transition.postconstruct.EpisodeTagStoreCreation;
import com.everwell.transition.repositories.EpisodeTagStoreRepository;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;

public class EpisodeTagStoreCreationTest extends BaseTest {

    @InjectMocks
    EpisodeTagStoreCreation episodeTagStoreCreation;

    @Mock
    EpisodeTagStoreRepository episodeTagStoreRepository;

    @Test
    public void test() {
        when(episodeTagStoreRepository.findAll()).thenReturn(getStore());
        episodeTagStoreCreation.initTagStore();
        List<EpisodeTagStore> config = new ArrayList<>(episodeTagStoreCreation.getEpisodeTagStoreMap().values());
        Assertions.assertEquals(config.get(0).getTagName(), getStore().get(0).getTagName());
        Assertions.assertEquals(config.get(0).getTagDescription(), getStore().get(0).getTagDescription());
        Assertions.assertEquals(config.get(0).getTagDisplayName(), getStore().get(0).getTagDisplayName());
    }

    List<EpisodeTagStore> getStore() {
        List<EpisodeTagStore> config = Arrays.asList(
                new EpisodeTagStore(1L, "Invalid_Phone_Number", "displayName", "icon", "desc", "Phone_issues")
        );
        return config;
    }

}

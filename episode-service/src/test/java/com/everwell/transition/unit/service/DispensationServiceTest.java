package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.elasticsearch.indices.EpisodeIndex;
import com.everwell.transition.enums.dispensation.DispensationCommentType;
import com.everwell.transition.enums.dispensation.DispensationStatus;
import com.everwell.transition.enums.dispensation.DispensationEndpoint;
import com.everwell.transition.enums.dispensation.DispensationTransactionIdType;
import com.everwell.transition.enums.dispensation.DispensationTransactionType;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.dto.dispensation.DispensationData;
import com.everwell.transition.model.dto.dispensation.DispensationProduct;
import com.everwell.transition.model.dto.dispensation.ProductInventoryLogDto;
import com.everwell.transition.model.dto.dispensation.ProductStockData;
import com.everwell.transition.model.dto.treatmentPlan.TreatmentPlanDto;
import com.everwell.transition.model.dto.treatmentPlan.TreatmentPlanProductMapDto;
import com.everwell.transition.model.request.dispensation.*;
import com.everwell.transition.model.response.ProductResponseWithScheduleDto;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.response.dispensation.*;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.EpisodeService;
import com.everwell.transition.service.RuleEngine;
import com.everwell.transition.service.impl.DispensationServiceImpl;
import com.everwell.transition.utils.Utils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.LocalDateTime;
import java.util.*;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.*;

public class DispensationServiceTest extends BaseTest {
    
    @Spy
    @InjectMocks
    DispensationServiceImpl dispensationService;

    @Mock
    ClientService clientService;

    @Mock
    EpisodeService episodeService;


    @Mock
    private RuleEngine ruleEngine;

    @Before
    public void init() {
        ReflectionTestUtils.setField(dispensationService, "dataGatewayURL", "http://test:8080");
        ReflectionTestUtils.setField(dispensationService, "username", "test");
        ReflectionTestUtils.setField(dispensationService, "password", "test");
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(29L,"test", "test", null ));
        dispensationService.init();
    }

    @Test
    public void searchProductsTest() {
        ProductSearchRequest productSearchRequest = new ProductSearchRequest();
        ProductResponse productResponse = new ProductResponse();
        productResponse.setProductId(1L);
        ResponseEntity<Response<List<ProductResponse>>> response
                = new ResponseEntity<>(
                new Response<>(Collections.singletonList(productResponse)),
                HttpStatus.OK
        );

        doReturn(response).when(dispensationService).postExchange(eq(DispensationEndpoint.SEARCH_PRODUCTS.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any());

        ResponseEntity<Response<List<ProductResponse>>> responseEntity = dispensationService.searchProducts(productSearchRequest);
        Assert.assertEquals(1L, (long)responseEntity.getBody().getData().get(0).getProductId());
        verify(dispensationService, Mockito.times(0)).getExchange(eq(DispensationEndpoint.GET_ALL_PRODUCTS.getEndpointUrl()), any(), any(), any());
        verify(dispensationService, Mockito.times(1)).postExchange(eq(DispensationEndpoint.SEARCH_PRODUCTS.getEndpointUrl()), any(), any());
    }

    @Test
    public void searchProductsTest_withEpisodeId() {
        ProductSearchRequest productSearchRequest = new ProductSearchRequest();
        productSearchRequest.setEpisodeId(1L);
        ProductResponse productResponse = new ProductResponse();
        productResponse.setProductId(1L);
        ResponseEntity<Response<List<ProductResponse>>> response
                = new ResponseEntity<>(
                new Response<>(Collections.singletonList(productResponse)),
                HttpStatus.OK
        );

        doReturn(response).when(dispensationService).getExchange(eq(DispensationEndpoint.GET_ALL_PRODUCTS.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any());

        ResponseEntity<Response<List<ProductResponse>>> responseEntity = dispensationService.searchProducts(productSearchRequest);
        Assert.assertEquals(1L, (long)responseEntity.getBody().getData().get(0).getProductId());
        verify(dispensationService, Mockito.times(1)).getExchange(eq(DispensationEndpoint.GET_ALL_PRODUCTS.getEndpointUrl()), any(), any(), any());
        verify(dispensationService, Mockito.times(0)).postExchange(eq(DispensationEndpoint.SEARCH_PRODUCTS.getEndpointUrl()), any(), any());
    }

    @Test
    public void productStockSearchTest() {
        ProductStockSearchRequest productSearchRequest = new ProductStockSearchRequest();
        ProductStockData productResponse = new ProductStockData(1L, "ABC", null, null);
        ResponseEntity<Response<List<ProductStockData>>> response
                = new ResponseEntity<>(
                new Response<>(Collections.singletonList(productResponse)),
                HttpStatus.OK
        );

        doReturn(response).when(dispensationService).postExchange(eq(DispensationEndpoint.PRODUCT_STOCK_SEARCH.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any());

        ResponseEntity<Response<List<ProductStockData>>> responseEntity = dispensationService.productStockSearch(productSearchRequest);
        Assert.assertEquals(1L, (long)responseEntity.getBody().getData().get(0).getProductId());
        Assert.assertEquals("ABC", responseEntity.getBody().getData().get(0).getBatchNumber());

    }

    @Test
    public void testGetAllDispensationDataForEntity() {
        EntityDispensationResponse entityDispensationResponse = new EntityDispensationResponse(123L, null, null, 10L);
        ResponseEntity<Response<EntityDispensationResponse>> response
                = new ResponseEntity<>(
                new Response<>(entityDispensationResponse),
                HttpStatus.OK
        );

        doReturn(response).when(dispensationService).getExchange(eq(DispensationEndpoint.DISPENSATION_ENTITY.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any());

        ResponseEntity<Response<EntityDispensationResponse>> responseEntity = dispensationService.getAllDispensationDataForEntity(123L, true);
        Assert.assertEquals(123L, (long)responseEntity.getBody().getData().getEntityId());
        Assert.assertEquals(10L, (long)responseEntity.getBody().getData().getTotalDispensedDrugs());
    }

    @Test
    public void testGetDispensation() {
        DispensationData dispensationData = new DispensationData();
        dispensationData.setDispensationId(123L);
        ResponseEntity<Response<DispensationData>> response
                = new ResponseEntity<>(
                new Response<>(dispensationData),
                HttpStatus.OK
        );

        doReturn(response).when(dispensationService).getExchange(eq(DispensationEndpoint.GET_DISPENSATION.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any());

        ResponseEntity<Response<DispensationData>> responseEntity = dispensationService.getDispensation(123L);
        Assert.assertEquals(123L, (long)responseEntity.getBody().getData().getDispensationId());
    }

    @Test
    public void testCreateDispensation() {
        DispensationRequest request = new DispensationRequest();
        DispensationResponse dispensationResponse = new DispensationResponse(123L);
        ResponseEntity<Response<DispensationResponse>> response
                = new ResponseEntity<>(
                new Response<>(dispensationResponse),
                HttpStatus.OK
        );

        doReturn(response).when(dispensationService).postExchange(eq(DispensationEndpoint.CREATE_DISPENSATION.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any());

        ResponseEntity<Response<DispensationResponse>> responseEntity = dispensationService.createDispensation(request);
        Assert.assertEquals(123L, (long)responseEntity.getBody().getData().getDispensationId());
    }

    @Test
    public void testCreateDispensationInventory() {
        DispensationRequest request = new DispensationRequest();
        DispensationResponse dispensationResponse = new DispensationResponse(123L);
        ResponseEntity<Response<DispensationResponse>> response
                = new ResponseEntity<>(
                new Response<>(dispensationResponse),
                HttpStatus.OK
        );

        doReturn(response).when(dispensationService).postExchange(eq(DispensationEndpoint.CREATE_DISPENSATION_INVENTORY.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any());

        ResponseEntity<Response<DispensationResponse>> responseEntity = dispensationService.createDispensationInventory(request);
        Assert.assertEquals(123L, (long)responseEntity.getBody().getData().getDispensationId());
    }

    @Test
    public void deleteDispensation() {
        DispensationResponse dispensationResponse = new DispensationResponse(123L);
        ResponseEntity<Response<DispensationResponse>> response
                = new ResponseEntity<>(
                new Response<>(dispensationResponse),
                HttpStatus.OK
        );

        doReturn(response).when(dispensationService).deleteExchange(eq(DispensationEndpoint.DELETE_DISPENSATION.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any(), any(), any());

        ResponseEntity<Response<DispensationResponse>> responseEntity = dispensationService.deleteDispensation(123L);
        Assert.assertEquals(123L, (long)responseEntity.getBody().getData().getDispensationId());
    }

    @Test
    public void testGetDispensationsForEntity() {
        ProductInventoryLogDto productInventoryLogDto = new ProductInventoryLogDto();
        productInventoryLogDto.setCommentType(DispensationCommentType.REASON_FOR_ADD);
        productInventoryLogDto.setTransactionIdType(DispensationTransactionIdType.EXTERNAL);
        productInventoryLogDto.setTransactionType(DispensationTransactionType.CREDIT);
        DispensationProduct product = new DispensationProduct(1L, "1");
        product.setLogs(Collections.singletonList(productInventoryLogDto));
        List<DispensationProduct> productList = Collections.singletonList(product);
        DispensationData dispensationData = new DispensationData(1L, 12345L, "04-03-2022 18:30:00", "06-03-2022 18:30:00", productList);
        List<DispensationData> dispensationDataList = Collections.singletonList(dispensationData);
        List<EntityDispensationResponse> entityDispensationResponseList = Collections.singletonList(new EntityDispensationResponse(12345L, dispensationDataList));
        SearchDispensationRequest searchDispensationRequest = new SearchDispensationRequest(Collections.singletonList(12345L), null);

        doReturn(new ResponseEntity<>(new Response<>(entityDispensationResponseList),HttpStatus.OK)).when(dispensationService).postExchange(eq(DispensationEndpoint.DISPENSATION_ENTITY_SEARCH.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any());

        ResponseEntity<Response<List<EntityDispensationResponse>>> responseObject = dispensationService.searchDispensationsForEntity(searchDispensationRequest);
        Assert.assertEquals((Long) 1L, responseObject.getBody().getData().get(0).getDispensations().get(0).getDispensationId());
    }

    @Test
    public void testReturnDispensation() {
        ReturnDispensationRequest returnDispensationRequest = new ReturnDispensationRequest();
        ReturnDispensationResponse returnDispensationResponse = new ReturnDispensationResponse(Collections.singletonList(123L));

        ResponseEntity<Response<ReturnDispensationResponse>> response
                = new ResponseEntity<>(
                new Response<>(returnDispensationResponse),
                HttpStatus.OK
        );

        doReturn(response).when(dispensationService).postExchange(eq(DispensationEndpoint.RETURN_DISPENSATION.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any());

        ResponseEntity<Response<ReturnDispensationResponse>> responseEntity = dispensationService.returnDispensation(returnDispensationRequest);
        Assert.assertEquals(123L, (long)responseEntity.getBody().getData().getReturnDispensationIds().get(0));
    }

    @Test
    public void testReturnDispensationInventory() {
        ReturnDispensationRequest returnDispensationRequest = new ReturnDispensationRequest();
        ReturnDispensationResponse returnDispensationResponse = new ReturnDispensationResponse(Collections.singletonList(123L));

        ResponseEntity<Response<ReturnDispensationResponse>> response
                = new ResponseEntity<>(
                new Response<>(returnDispensationResponse),
                HttpStatus.OK
        );

        doReturn(response).when(dispensationService).postExchange(eq(DispensationEndpoint.RETURN_DISPENSATION_INVENTORY.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any());

        ResponseEntity<Response<ReturnDispensationResponse>> responseEntity = dispensationService.returnDispensationInventory(returnDispensationRequest);
        Assert.assertEquals(123L, (long)responseEntity.getBody().getData().getReturnDispensationIds().get(0));
    }

    @Test
    public void testAddProduct() {
        ProductRequest productRequest = new ProductRequest();
        ProductResponse productResponse = new ProductResponse();
        productResponse.setProductId(1L);
        ResponseEntity<Response<ProductResponse>> response
                = new ResponseEntity<>(
                new Response<>(productResponse),
                HttpStatus.OK
        );

        doReturn(response).when(dispensationService).postExchange(eq(DispensationEndpoint.ADD_PRODUCT.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any());

        ResponseEntity<Response<ProductResponse>> responseEntity = dispensationService.addProduct(productRequest);
        Assert.assertEquals(1L, (long)responseEntity.getBody().getData().getProductId());
    }

    @Test
    public void testAddProductBulk() {
        ProductBulkRequest productRequest = new ProductBulkRequest();
        ProductResponse productResponse = new ProductResponse();
        productResponse.setProductId(1L);
        ResponseEntity<Response<List<ProductResponse>>> response
                = new ResponseEntity<>(
                new Response<>(Collections.singletonList(productResponse)),
                HttpStatus.OK
        );

        doReturn(response).when(dispensationService).postExchange(eq(DispensationEndpoint.ADD_PRODUCT_BULK.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any());

        ResponseEntity<Response<List<ProductResponse>>> responseEntity = dispensationService.addProductBulk(productRequest);
        Assert.assertEquals(1L, (long)responseEntity.getBody().getData().get(0).getProductId());
    }

    @Test
    public void testGetProduct() {
        ProductResponse productResponse = new ProductResponse();
        productResponse.setProductId(1L);
        ResponseEntity<Response<ProductResponse>> response
                = new ResponseEntity<>(
                new Response<>(productResponse),
                HttpStatus.OK
        );

        doReturn(response).when(dispensationService).getExchange(eq(DispensationEndpoint.GET_PRODUCT.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any());

        ResponseEntity<Response<ProductResponse>> responseEntity = dispensationService.getProduct(1L);
        Assert.assertEquals(1L, (long)responseEntity.getBody().getData().getProductId());
    }

    @Test
    public void testStockCredit() {
        StockRequest stockRequest = new StockRequest();
        ResponseEntity<Response<Void>> response
                = new ResponseEntity<>(
                new Response<>(true, null),
                HttpStatus.OK
        );

        doReturn(response).when(dispensationService).postExchange(eq(DispensationEndpoint.STOCK_CREDIT.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any());

        ResponseEntity<Response<Void>> responseEntity = dispensationService.stockCredit(stockRequest);
        Assert.assertTrue(responseEntity.getBody().isSuccess());
    }

    @Test
    public void testStockDebit() {
        StockRequest stockRequest = new StockRequest();
        ResponseEntity<Response<Void>> response
                = new ResponseEntity<>(
                new Response<>(true, null),
                HttpStatus.OK
        );

        doReturn(response).when(dispensationService).putExchange(eq(DispensationEndpoint.STOCK_DEBIT.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any());

        ResponseEntity<Response<Void>> responseEntity = dispensationService.stockDebit(stockRequest);
        Assert.assertTrue(responseEntity.getBody().isSuccess());
    }

    @Test
    public void createDispensationEpisodeTest() {
        DispensationRequest request = new DispensationRequest();
        request.setExtendEndDate(true);
        request.setDosingStartDate("2021-10-10 18:30:00");
        request.setDrugDispensedForDays(5L);
        request.setAddedBy(495L);
        request.setInventoryEnabled(false);
        EpisodeIndex episode = new EpisodeIndex();
        episode.setStartDate("2021-10-09 18:30:00");
        episode.setId("1");
        episode.setEndDate("2021-10-12 18:30:00");
        episode.setStageName("DIAGNOSED_ON_TREATMENT_PRIVATE");
        episode.setCurrentStageId(1L);
        DispensationResponse dispensationResponse = new DispensationResponse(123L);
        ResponseEntity<Response<DispensationResponse>> response
                = new ResponseEntity<>(
                new Response<>(dispensationResponse),
                HttpStatus.OK
        );

        doReturn(response).when(dispensationService).createDispensation(request);
        doNothing().when(episodeService).updateOnTreatmentEpisodeEndDate(any(), eq(495L), anyString(), anyString(), any());

        ResponseEntity<Response<DispensationResponse>> responseEntity = dispensationService.createDispensationEpisode(request, episode.convertToDto());
        Assert.assertEquals((long) responseEntity.getBody().getData().getDispensationId(), 123L);
    }

    @Test
    public void createDispensationEpisodeInventoryTest() {
        DispensationRequest request = new DispensationRequest();
        request.setExtendEndDate(true);
        request.setDosingStartDate("2021-10-10 18:30:00");
        request.setDrugDispensedForDays(5L);
        request.setAddedBy(495L);
        request.setInventoryEnabled(true);
        EpisodeIndex episode = new EpisodeIndex();
        episode.setStartDate("2021-10-09 18:30:00");
        episode.setEndDate("2021-10-12 18:30:00");
        episode.setCurrentStageId(1L);
        episode.setId("1");
        episode.setStageName("DIAGNOSED_ON_TREATMENT_PRIVATE");
        DispensationResponse dispensationResponse = new DispensationResponse(123L);
        ResponseEntity<Response<DispensationResponse>> response
                = new ResponseEntity<>(
                new Response<>(dispensationResponse),
                HttpStatus.OK
        );

        doReturn(response).when(dispensationService).createDispensationInventory(request);
        doNothing().when(episodeService).updateOnTreatmentEpisodeEndDate(any(), eq(495L), anyString(), anyString(), any());

        ResponseEntity<Response<DispensationResponse>> responseEntity = dispensationService.createDispensationEpisode(request, episode.convertToDto());
        Assert.assertEquals((long) responseEntity.getBody().getData().getDispensationId(), 123L);
    }

    @Test
    public void testGetEpisodeDispensationDataSuccess() {
        EntityDispensationResponse entityDispensationResponse = new EntityDispensationResponse(1L, null, null, 10L);
        ResponseEntity<Response<EntityDispensationResponse>> allDispensationResponse
                = new ResponseEntity<>(
                new Response<>(entityDispensationResponse),
                HttpStatus.OK
        );

        doReturn(allDispensationResponse).when(dispensationService).getExchange(anyString(), any(), any(), any());

        ResponseEntity<Response<EntityDispensationResponse>> responseEntity = dispensationService.getEpisodeDispensationData(123L);
        Assert.assertEquals(1L, (long) responseEntity.getBody().getData().getEntityId());
    }

    public void getWeightBandForEpisode() {
        EpisodeDto episodeDto = new EpisodeDto();
        DispensationWeightBandResponse dispensationWeightBandResponse = new DispensationWeightBandResponse("DRTB: Adult: 5-6 Kg");
        episodeDto.setDiseaseId(1L);
        Client client = new Client(29L, "test", "test", null);

        when(clientService.getClientByTokenOrDefault()).thenReturn(client);
        when(episodeService.getEpisodeDetails(eq(123L), eq(29L))).thenReturn(episodeDto);
        when(ruleEngine.run(any(), eq(episodeDto.getDiseaseId()), eq(null), eq(true), any(), eq(client.getId()))).thenReturn(dispensationWeightBandResponse);

        DispensationWeightBandResponse response = dispensationService.getWeightBandForEpisode(123L);
        Assert.assertEquals(dispensationWeightBandResponse.getWeightBand(), response.getWeightBand());
    }

    @Test
    public void getWeightBandForEpisodeNull() {
        EpisodeDto episodeDto = new EpisodeDto();
        episodeDto.setDiseaseId(4L);
        Client client = new Client(29L, "test", "test", null);

        when(clientService.getClientByTokenOrDefault()).thenReturn(client);
        when(episodeService.getEpisodeDetails(eq(123L), eq(29L))).thenReturn(episodeDto);
        when(ruleEngine.run(any(), eq(episodeDto.getDiseaseId()), eq(null), eq(true), any(), eq(client.getId()))).thenReturn(null);

        DispensationWeightBandResponse response = dispensationService.getWeightBandForEpisode(123L);
        Assert.assertNull(response);
    }

    @Test
    public void getEpisodeIdWithRefillDueDate() {
        RefillDatesRequest refillDatesRequest = new RefillDatesRequest(Collections.singletonList(12345L), "10-10-2022 18:30:00", "12-10-2022 18:30:00");
        DispensationData dispensationData = new DispensationData(1L, 12345L, "04-03-2022 18:30:00", "06-03-2022 18:30:00", null);
        List<DispensationData> dispensationDataList = Collections.singletonList(dispensationData);
        List<EntityDispensationResponse> entityDispensationResponseList = Arrays.asList(new EntityDispensationResponse(12345L, dispensationDataList, "06-03-2022 18:30:00", 1L), new EntityDispensationResponse(12356L, null, null, 1L));

        ResponseEntity<Response<List<EntityDispensationResponse>>> response
                = new ResponseEntity<>(
                new Response<>(entityDispensationResponseList),
                HttpStatus.OK
        );
        doReturn(response).when(dispensationService).searchDispensationsForEntity(any());

        Map<Long, String> entityRefillMap = dispensationService.getEpisodeIdsWithRefillDueDate(refillDatesRequest);
        Assert.assertEquals("06-03-2022 18:30:00", entityRefillMap.get(12345L));
    }
    @Test
    public void testStockUpdate() {
        Map<String,Object> request = new HashMap<>();
        ResponseEntity<Response<Void>> response
                = new ResponseEntity<>(
                new Response<>(true, null),
                HttpStatus.OK
        );

        doReturn(response).when(dispensationService).putExchange(eq(DispensationEndpoint.STOCK_UPDATE.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any());

        ResponseEntity<Response<Void>> responseEntity = dispensationService.stockUpdate(request);
        Assert.assertTrue(responseEntity.getBody().isSuccess());
    }

    @Test
    public void testNullTreatmentPlanProductWithScheduleDetails() {
        TreatmentPlanDto nullTreatmentPlan = null;
        List<ProductResponseWithScheduleDto> productResponseWithScheduleDtoList = dispensationService.getProductWithSchedulesFromTreatmentPlan(nullTreatmentPlan);
        Assert.assertNotNull(productResponseWithScheduleDtoList);
        Assert.assertEquals(productResponseWithScheduleDtoList.size(), 0);
    }

    @Test
    public void testEmptyProductTreatmentPlanProductWithScheduleDetails() {
        TreatmentPlanDto emptyTreatmentPlan = new TreatmentPlanDto();
        List<ProductResponseWithScheduleDto> productResponseWithScheduleDtoList = dispensationService.getProductWithSchedulesFromTreatmentPlan(emptyTreatmentPlan);
        Assert.assertNotNull(productResponseWithScheduleDtoList);
        Assert.assertEquals(productResponseWithScheduleDtoList.size(), 0);
    }

    @Test
    public void testFilledProductTreatmentPlanProductWithScheduleDetails() {
        TreatmentPlanDto filledTreatmentPlan = new TreatmentPlanDto();
        filledTreatmentPlan.setMappedProducts(getFilledMappedProducts());

        ResponseEntity<Response<List<ProductResponse>>> response
                = new ResponseEntity<>(
                new Response<>(getFilledProductResponses()),
                HttpStatus.OK
        );

        doReturn(response).when(dispensationService).postExchange(eq(DispensationEndpoint.SEARCH_PRODUCTS.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any());

        List<ProductResponseWithScheduleDto> productResponseWithScheduleDtoList = dispensationService.getProductWithSchedulesFromTreatmentPlan(filledTreatmentPlan);
        Assert.assertNotNull(productResponseWithScheduleDtoList);
        Assert.assertEquals(productResponseWithScheduleDtoList.size(), 2);
        Assert.assertEquals(productResponseWithScheduleDtoList.get(0).getProductId().longValue(), 1L);
        Assert.assertEquals(productResponseWithScheduleDtoList.get(1).getProductId().longValue(), 2L);
        Assert.assertEquals(productResponseWithScheduleDtoList.get(0).getSchedule(), "11** 1111111");
        Assert.assertEquals(productResponseWithScheduleDtoList.get(1).getSchedule(), "1**1 1111111");
    }

    private List<ProductResponse> getFilledProductResponses() {
        ProductResponse product1Response = new ProductResponse();
        product1Response.setProductId(1L);

        ProductResponse product2Response = new ProductResponse();
        product2Response.setProductId(2L);
        return Arrays.asList(product1Response, product2Response);
    }

    private List<TreatmentPlanProductMapDto> getFilledMappedProducts() {
        TreatmentPlanProductMapDto treatmentPlanProduct1MapDto = new TreatmentPlanProductMapDto(Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, 1L, "11** 1111111", LocalDateTime.now(), null, null, null, false);
        TreatmentPlanProductMapDto treatmentPlanProduct2MapDto = new TreatmentPlanProductMapDto(Utils.getCurrentDateNew(), Utils.getCurrentDateNew(), null, 2L, "1**1 1111111", LocalDateTime.now(), null, null, null, false);
        return Arrays.asList(treatmentPlanProduct1MapDto, treatmentPlanProduct2MapDto);
    }
}

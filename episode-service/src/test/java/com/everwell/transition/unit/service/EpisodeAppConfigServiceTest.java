package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.model.db.EpisodeAppConfig;
import com.everwell.transition.model.response.EpisodeAppConfigResponse;
import com.everwell.transition.repositories.EpisodeAppConfigRepository;
import com.everwell.transition.service.impl.EpisodeAppConfigServiceImpl;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.everwell.transition.constants.Constants.DEFAULT;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class EpisodeAppConfigServiceTest extends BaseTest {

    @InjectMocks
    EpisodeAppConfigServiceImpl episodeAppConfigService;

    @Mock
    EpisodeAppConfigRepository episodeAppConfigRepository;

    @Test
    public void getAppConfigTest() {
        String technology = "99DOTS";
        String deployment = "IND";
        Long episodeId = 1L;
        when(episodeAppConfigRepository.getAllByTechnologyAndDeployment(anyString(),anyString())).thenReturn(getAppConfig());
        EpisodeAppConfigResponse episodeConfig = episodeAppConfigService.getEpisodeAppConfig(episodeId,technology,deployment);
        Assertions.assertEquals(episodeConfig.getConfig().getDeployment(),getAppConfig().getDeployment());
    }

    EpisodeAppConfig getAppConfig() {
        return new EpisodeAppConfig(3L, "99DOTS", true, false, true, 28L, "IND");
    }

    @Test
    public void getAppConfigTest_emptyList() {
        String technology = "99DOTS";
        String deployment = "IND";
        Long episodeId = 1L;
        when(episodeAppConfigRepository.getAllByTechnologyAndDeployment(anyString(),anyString())).thenReturn(null);
        when(episodeAppConfigRepository.getAllByTechnologyAndDeployment(DEFAULT, DEFAULT)).thenReturn(getAppConfig());
        EpisodeAppConfigResponse episodeConfig = episodeAppConfigService.getEpisodeAppConfig(episodeId,technology,deployment);
        Assertions.assertEquals(episodeConfig.getConfig().getDeployment(),getAppConfig().getDeployment());
    }

}

package com.everwell.transition.unit;

import com.everwell.transition.BaseTest;
import com.everwell.transition.model.dto.EventStreamingDto;
import com.everwell.transition.service.RuleEngine;
import com.everwell.transition.service.impl.StageTransitionServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class StageTransitionServiceTest extends BaseTest {

    @InjectMocks
    StageTransitionServiceImpl stageTransitionService;

    @Mock
    RuleEngine ruleEngine;

    @Test
    public void stageTransitionHandler_nullResult() {
        Long clientId = 1L;
        when(ruleEngine.run(any(), any(), any(), any(), any())).thenReturn(null);
        EventStreamingDto<Map<String, Object>> output = stageTransitionService.stageTransitionHandler(new HashMap<>(), clientId);
        Mockito.verify(ruleEngine, Mockito.times(1)).run(any(), any(), any(), any(), any(), any());
        Assert.assertNull(output.getField());
    }

}

package com.everwell.transition.unit.controller;

import com.everwell.transition.BaseTest;
import com.everwell.transition.controller.DispensationController;
import com.everwell.transition.elasticsearch.indices.EpisodeIndex;
import com.everwell.transition.exceptions.CustomExceptionHandler;
import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.dto.dispensation.*;
import com.everwell.transition.model.request.dispensation.*;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.response.dispensation.DispensationResponse;
import com.everwell.transition.model.response.dispensation.EntityDispensationResponse;
import com.everwell.transition.model.response.dispensation.ProductResponse;
import com.everwell.transition.model.response.dispensation.ReturnDispensationResponse;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.model.response.dispensation.*;
import com.everwell.transition.service.DispensationService;
import com.everwell.transition.service.EpisodeService;
import com.everwell.transition.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class DispensationControllerTest extends BaseTest {

    private MockMvc mockMvc;

    @Spy
    @InjectMocks
    private DispensationController dispensationController;

    @Mock
    DispensationService dispensationService;

    @Mock
    EpisodeService episodeService;

    @Mock
    ClientService clientService;


    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(dispensationController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void searchProductsTest() throws Exception {
        String uri = "/v1/product/search";
        ProductSearchRequest productSearchRequest = new ProductSearchRequest();
        ProductResponse productResponse = new ProductResponse();
        productResponse.setProductId(1L);
        ResponseEntity<Response<List<ProductResponse>>> response
                = new ResponseEntity<>(
                new Response<>(Collections.singletonList(productResponse)),
                HttpStatus.OK
        );

        when(dispensationService.searchProducts(any())).thenReturn(response);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(productSearchRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].productId").value(1L));
    }

    @Test
    public void searchProductStockTest() throws Exception {
        String uri = "/v1/product/stocks/search";
        ProductStockSearchRequest productSearchRequest = new ProductStockSearchRequest();
        ProductStockData productResponse = new ProductStockData(1L, "ABC", null, null);
        ResponseEntity<Response<List<ProductStockData>>> response
                = new ResponseEntity<>(
                new Response<>(Collections.singletonList(productResponse)),
                HttpStatus.OK
        );

        when(dispensationService.productStockSearch(any())).thenReturn(response);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(productSearchRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].productId").value(1L))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].batchNumber").value("ABC"));
    }

    @Test
    public void getDispensationDetailsForEntityTest() throws Exception {
        String uri = "/v1/dispensation/entity/123?includeProductLog=true";
        EntityDispensationResponse entityDispensationResponse = new EntityDispensationResponse(123L, null, null, 10L);
        ResponseEntity<Response<EntityDispensationResponse>> response
                = new ResponseEntity<>(
                new Response<>(entityDispensationResponse),
                HttpStatus.OK
        );

        when(dispensationService.getAllDispensationDataForEntity(eq(123L), eq(true))).thenReturn(response);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.entityId").value(123L))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.totalDispensedDrugs").value(10L));
    }

    @Test
    public void getDispensationDetailsTest() throws Exception {
        String uri = "/v1/dispensation/123";
        DispensationData dispensationData = new DispensationData();
        dispensationData.setDispensationId(123L);
        ResponseEntity<Response<DispensationData>> response
                = new ResponseEntity<>(
                new Response<>(dispensationData),
                HttpStatus.OK
        );

        when(dispensationService.getDispensation(eq(123L))).thenReturn(response);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.dispensationId").value(123L));
    }

    @Test
    public void createDispensationTest() throws Exception {
        String uri = "/v1/dispensation";
        DispensationRequest request = new DispensationRequest();
        DispensationResponse dispensationResponse = new DispensationResponse(123L);
        ResponseEntity<Response<DispensationResponse>> response
                = new ResponseEntity<>(
                new Response<>(dispensationResponse),
                HttpStatus.OK
        );

        when(dispensationService.createDispensation(any())).thenReturn(response);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(request))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.dispensationId").value(123L));
    }

    @Test
    public void createDispensationInventoryTest() throws Exception {
        String uri = "/v2/dispensation";
        DispensationRequest request = new DispensationRequest();
        DispensationResponse dispensationResponse = new DispensationResponse(123L);
        ResponseEntity<Response<DispensationResponse>> response
                = new ResponseEntity<>(
                new Response<>(dispensationResponse),
                HttpStatus.OK
        );

        when(dispensationService.createDispensationInventory(any())).thenReturn(response);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(request))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.dispensationId").value(123L));
    }

    @Test
    public void deleteDispensationTest() throws Exception {
        String uri = "/v1/dispensation/123";
        DispensationResponse dispensationResponse = new DispensationResponse(123L);
        ResponseEntity<Response<DispensationResponse>> response
                = new ResponseEntity<>(
                new Response<>(dispensationResponse),
                HttpStatus.OK
        );

        when(dispensationService.deleteDispensation(any())).thenReturn(response);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .delete(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.dispensationId").value(123L));
    }

    @Test
    public void getDispensationForEntityTest() throws Exception {
        String uri = "/v1/dispensation/entity/search";
        SearchDispensationRequest searchDispensationRequest = new SearchDispensationRequest();
        List<DispensationProduct> productList = Collections.singletonList(new DispensationProduct(1L, "1"));
        DispensationData dispensationData = new DispensationData(1L, 12345L, "04-03-2022 18:30:00", "06-03-2022 18:30:00", productList);
        List<DispensationData> dispensationDataList = Collections.singletonList(dispensationData);
        List<EntityDispensationResponse> entityDispensationResponseList = Collections.singletonList(new EntityDispensationResponse(12345L, dispensationDataList));

        ResponseEntity<Response<List<EntityDispensationResponse>>> response
                = new ResponseEntity<>(
                new Response<>(entityDispensationResponseList),
                HttpStatus.OK
        );

        when(dispensationService.searchDispensationsForEntity(any())).thenReturn(response);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(searchDispensationRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].entityId").value(12345L));
    }

    @Test
    public void returnDispensationTest() throws Exception {
        String uri = "/v1/dispensation/return";
        ReturnDispensationRequest returnDispensationRequest = new ReturnDispensationRequest();
        ReturnDispensationResponse returnDispensationResponse = new ReturnDispensationResponse(Collections.singletonList(123L));

        ResponseEntity<Response<ReturnDispensationResponse>> response
                = new ResponseEntity<>(
                new Response<>(returnDispensationResponse),
                HttpStatus.OK
        );

        when(dispensationService.returnDispensation(any())).thenReturn(response);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(returnDispensationRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.returnDispensationIds[0]").value(123L));
    }

    @Test
    public void returnDispensationInventoryTest() throws Exception {
        String uri = "/v2/dispensation/return";
        ReturnDispensationRequest returnDispensationRequest = new ReturnDispensationRequest();
        ReturnDispensationResponse returnDispensationResponse = new ReturnDispensationResponse(Collections.singletonList(123L));

        ResponseEntity<Response<ReturnDispensationResponse>> response
                = new ResponseEntity<>(
                new Response<>(returnDispensationResponse),
                HttpStatus.OK
        );

        when(dispensationService.returnDispensationInventory(any())).thenReturn(response);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(returnDispensationRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.returnDispensationIds[0]").value(123L));
    }

    @Test
    public void addProductTest() throws Exception {
        String uri = "/v1/product";
        ProductRequest productRequest = new ProductRequest();
        ProductResponse productResponse = new ProductResponse();
        productResponse.setProductId(1L);
        ResponseEntity<Response<ProductResponse>> response
                = new ResponseEntity<>(
                new Response<>(productResponse),
                HttpStatus.OK
        );

        when(dispensationService.addProduct(any())).thenReturn(response);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(productRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.productId").value(1L));
    }

    @Test
    public void addProductBulkTest() throws Exception {
        String uri = "/v1/products";
        ProductBulkRequest productRequest = new ProductBulkRequest();
        ProductResponse productResponse = new ProductResponse();
        productResponse.setProductId(1L);
        ResponseEntity<Response<List<ProductResponse>>> response
                = new ResponseEntity<>(
                new Response<>(Collections.singletonList(productResponse)),
                HttpStatus.OK
        );

        when(dispensationService.addProductBulk(any())).thenReturn(response);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(productRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].productId").value(1L));
    }

    @Test
    public void getProductTest() throws Exception {
        String uri = "/v1/product/123";
        ProductResponse productResponse = new ProductResponse();
        productResponse.setProductId(1L);
        ResponseEntity<Response<ProductResponse>> response
                = new ResponseEntity<>(
                new Response<>(productResponse),
                HttpStatus.OK
        );


        when(dispensationService.getProduct(eq(123L))).thenReturn(response);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.productId").value(1L));
    }

    @Test
    public void stockCreditTest() throws Exception {
        String uri = "/v1/stock";
        StockRequest stockRequest = new StockRequest();
        ResponseEntity<Response<Void>> response
                = new ResponseEntity<>(
                new Response<>(),
                HttpStatus.OK
        );

        when(dispensationService.stockCredit(any())).thenReturn(response);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(stockRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk());
    }

    @Test
    public void stockDebitTest() throws Exception {
        String uri = "/v1/stock";
        StockRequest stockRequest = new StockRequest();
        ResponseEntity<Response<Void>> response
                = new ResponseEntity<>(
                new Response<>(),
                HttpStatus.OK
        );

        when(dispensationService.stockDebit(any())).thenReturn(response);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(uri)
                                .content(Utils.asJsonString(stockRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk());
    }

    @Test
    public void createDispensationForEpisodeTestSuccess() throws Exception {
        String uri = "/v1/dispensation/episode";
        DispensationRequest request = new DispensationRequest();
        request.setEntityId(123L);
        request.setDateOfPrescription("2022-05-20 18:30:30");
        request.setIssuedDate("2022-05-21 18:30:00");
        request.setDosingStartDate("2022-05-22 18:30:00");
        EpisodeIndex episode = new EpisodeIndex();
        episode.setId("123");
        episode.setStartDate("2022-05-18 18:30:00");
        episode.setEndDate("2022-05-28 18:30:00");
        episode.setStageName("DIAGNOSED_ON_TREATMENT_PUBLIC");
        episode.setCurrentStageId(1L);
        EpisodeDto episodeDto = episode.convertToDto();
        Client client = new Client(29L, "test", "test", null);


        DispensationResponse dispensationResponse = new DispensationResponse(123L);
        ResponseEntity<Response<DispensationResponse>> response
                = new ResponseEntity<>(
                new Response<>(dispensationResponse),
                HttpStatus.OK
        );

        when(clientService.getClientByTokenOrDefault()).thenReturn(client);
        when(episodeService.getEpisodeDetails(eq(123L), eq(29L), eq(false), eq(false), eq(false), eq(false))).thenReturn(episodeDto);
        when(dispensationService.createDispensationEpisode(any(), any())).thenReturn(response);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(request))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.dispensationId").value(123L));
    }

    @Test
    public void createDispensationForEpisodeTestInvalidIssuedDate() throws Exception {
        String uri = "/v1/dispensation/episode";
        DispensationRequest request = new DispensationRequest();
        request.setEntityId(123L);
        request.setDateOfPrescription("2022-05-20 18:30:00");
        request.setIssuedDate("2022-05-29 18:30:00");
        request.setDosingStartDate("2022-05-22 18:30:00");
        EpisodeIndex episode = new EpisodeIndex();
        episode.setId("123");
        episode.setStartDate("2022-05-18 18:30:00");
        episode.setEndDate("2022-05-28 18:30:00");
        episode.setStageName("DIAGNOSED_OUTCOME_ASSIGNED");
        Client client = new Client(29L, "test", "test", null);

        when(clientService.getClientByTokenOrDefault()).thenReturn(client);
        when(episodeService.getEpisodeDetails(eq(123L), eq(29L), eq(false), eq(false), eq(false), eq(false))).thenReturn(episode.convertToDto());

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(request))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest());
    }

    @Test
    public void createDispensationForEpisodeTestInvalidPrescriptionDate() throws Exception {
        String uri = "/v1/dispensation/episode";
        DispensationRequest request = new DispensationRequest();
        request.setEntityId(123L);
        request.setDateOfPrescription("2022-05-22 18:30:00");
        request.setIssuedDate("2022-05-21 18:30:00");
        request.setDosingStartDate("2022-05-22 18:30:00");
        EpisodeIndex episode = new EpisodeIndex();
        episode.setId("123");
        episode.setStartDate("2022-05-18 18:30:00");
        episode.setEndDate("2022-05-28 18:30:00");
        episode.setStageName("DIAGNOSED_ON_TREATMENT_PRIVATE");
        Client client = new Client(29L, "test", "test", null);

        when(clientService.getClientByTokenOrDefault()).thenReturn(client);
        when(episodeService.getEpisodeDetails(eq(123L), eq(29L), eq(false), eq(false), eq(false), eq(false))).thenReturn(episode.convertToDto());

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(request))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest());
    }

    @Test
    public void createDispensationForEpisodeTestInvalidDosingStartDate() throws Exception {
        String uri = "/v1/dispensation/episode";
        DispensationRequest request = new DispensationRequest();
        request.setEntityId(123L);
        request.setDateOfPrescription("2022-05-22 18:30:00");
        request.setIssuedDate("2022-05-23 18:30:00");
        request.setDosingStartDate("2022-05-29 18:30:00");
        EpisodeIndex episode = new EpisodeIndex();
        episode.setId("123");
        episode.setStartDate("2022-05-18 18:30:00");
        episode.setEndDate("2022-05-28 18:30:00");
        episode.setStageName("DIAGNOSED_ON_TREATMENT_PRIVATE");
        Client client = new Client(29L, "test", "test", null);

        when(clientService.getClientByTokenOrDefault()).thenReturn(client);
        when(episodeService.getEpisodeDetails(eq(123L), eq(29L), eq(false), eq(false), eq(false), eq(false))).thenReturn(episode.convertToDto());

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(Utils.asJsonString(request))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getEpisodeDispensationData() throws Exception {
        String uri = "/v1/dispensation/123/entity";
        EntityDispensationResponse entityDispensationResponse = new EntityDispensationResponse(123L, null, null, 10L);
        ResponseEntity<Response<EntityDispensationResponse>> response
                = new ResponseEntity<>(
                new Response<>(entityDispensationResponse),
                HttpStatus.OK
        );

        when(dispensationService.getEpisodeDispensationData(eq(123L))).thenReturn(response);

          mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk());
    }
    
    @Test
    public void getWeightBandByEpisodeTest() throws Exception {
        String uri = "/v1/dispensation/weight-band?episodeId=123";
        DispensationWeightBandResponse dispensationWeightBandResponse = new DispensationWeightBandResponse("DRTB: 10-15 kg");

        when(dispensationService.getWeightBandForEpisode(eq(123L))).thenReturn(dispensationWeightBandResponse);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.weightBand").value("DRTB: 10-15 kg"));
    }

    @Test
    public void stockUpdateTest() throws Exception {
        String uri = "/v1/stocks";
        StockRequest stockRequest = new StockRequest();
        ResponseEntity<Response<Void>> response
                = new ResponseEntity<>(
                new Response<>(),
                HttpStatus.OK
        );

        when(dispensationService.stockUpdate(any())).thenReturn(response);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(uri)
                                .content(Utils.asJsonString(stockRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk());
    }

}

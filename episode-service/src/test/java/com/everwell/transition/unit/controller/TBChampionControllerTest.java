package com.everwell.transition.unit.controller;

import com.everwell.transition.BaseTest;
import com.everwell.transition.controller.EpisodeTagController;
import com.everwell.transition.controller.TBChampionActivityController;
import com.everwell.transition.exceptions.CustomExceptionHandler;
import com.everwell.transition.model.db.EpisodeTag;
import com.everwell.transition.model.db.TBChampionActivity;
import com.everwell.transition.model.dto.ActivityDto;
import com.everwell.transition.model.request.AddActivityRequest;
import com.everwell.transition.model.response.EpisodeTagResponse;
import com.everwell.transition.model.response.TBChampionActivityResponse;
import com.everwell.transition.repositories.TBChampionActivityRepository;
import com.everwell.transition.service.EpisodeTagService;
import com.everwell.transition.service.TBChampionActivityService;
import com.everwell.transition.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TBChampionControllerTest extends BaseTest {

    private MockMvc mockMvc;

    @InjectMocks
    private TBChampionActivityController tbChampionActivityController;

    @Mock
    TBChampionActivityService tbChampionActivityService;

    @Mock
    TBChampionActivityRepository tbChampionActivityRepository;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(tbChampionActivityController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void getActivitiesTest() throws Exception {
        long episodeId = 12345L;
        String uri = "/v1/tb-champ-activity/" + episodeId;
        when(tbChampionActivityService.getActivities(episodeId)).thenReturn(getActivities().get(0));
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.episodeId").value(episodeId))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.activities[0].patientVisits").value(3))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.activities[1].patientVisits").value(2));
    }

    @Test
    public void getActivitiesTest_ZeroEpisodeId() throws Exception {
        String uri = "/v1/tb-champ-activity/0";
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("episodeId must not be null!"));
    }

    @Test
    public void saveActivityTest() throws Exception {
        long episodeId = 12345L;
        String uri = "/v1/tb-champ-activity";
        AddActivityRequest addActivityRequest = new AddActivityRequest();
        addActivityRequest.setEpisodeId(episodeId);
        doNothing().when(tbChampionActivityService).saveActivity(any());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(addActivityRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value("Activity Saved Successfully!"));
    }

    @Test
    public void saveActivityTestNullEpisodeId() throws Exception {
        String uri = "/v1/tb-champ-activity";
        AddActivityRequest addActivityRequest = new AddActivityRequest();

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(addActivityRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest());
    }

    List<TBChampionActivityResponse> getActivities() {
        long episodeId = 12345;
        List<TBChampionActivity> activities = Arrays.asList(
                new TBChampionActivity("22/03/2022", episodeId, 3L, 4L, "", ""),
                new TBChampionActivity("23/03/2022", episodeId, 2L, 5L,"", "")
        );
        List<TBChampionActivityResponse> list = new ArrayList<>();
        list.add(new TBChampionActivityResponse(12345L, activities));
        return list;
    }
}

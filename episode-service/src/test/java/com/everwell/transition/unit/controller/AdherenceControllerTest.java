package com.everwell.transition.unit.controller;

import com.everwell.transition.BaseTest;
import com.everwell.transition.constants.Constants;
import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.controller.AdherenceController;
import com.everwell.transition.enums.RangeFilterType;
import com.everwell.transition.exceptions.CustomExceptionHandler;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.db.EpisodeLog;
import com.everwell.transition.model.db.EpisodeTag;
import com.everwell.transition.model.dto.AdherenceData;
import com.everwell.transition.model.dto.AdherenceDataForCalendar;
import com.everwell.transition.model.dto.AdherenceMonthWiseData;
import com.everwell.transition.model.dto.RangeFilters;
import com.everwell.transition.model.db.TreatmentPlanProductMap;
import com.everwell.transition.model.dto.*;
import com.everwell.transition.model.request.adherence.*;
import com.everwell.transition.model.response.*;
import com.everwell.transition.model.response.adherence.AllAvgAdherenceResponse;
import com.everwell.transition.model.response.adherence.PositiveCountResponse;
import com.everwell.transition.model.response.adherence.AdherenceGlobalConfigResponse;
import com.everwell.transition.model.response.adherence.EditDoseBulkResponse;
import com.everwell.transition.model.response.adherence.GenericConfigResponse;
import com.everwell.transition.service.*;
import com.everwell.transition.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AdherenceControllerTest extends BaseTest {

    private MockMvc mockMvc;

    @Spy
    @InjectMocks
    private AdherenceController adherenceController;


    @Mock
    IAMService iamService;

    @Mock
    AdherenceService adherenceService;

    @Mock
    EpisodeTagService episodeTagService;

    @Mock
    EpisodeLogService episodeLogService;

    @Mock
    ClientService clientService;

    @Mock
    EpisodeService episodeService;

    @Mock
    TreatmentPlanService treatmentPlanService;

    private static long episodeId = 123L;
    private static String editDosesUri = "/v1/adherence/edit-doses";
    private final String GET_IMEI_URI = "/v1/imei";
    private final String GET_IMEI_BULK_URI = "/v1/imei/bulk";
    private final String GET_IMEI_ENTITY_URI = "/v1/imei/entity";
    private final String UPDATE_VIDEO_STATUS_URI = "/v1/vot/video/status";
    private final String GET_ADHERENCE_POSITIVE_EVENTS_URI = "/v1/adherence/positive-events";
    private final String GET_AVERAGE_ADHERENCE_URI = "/v1/adherence/average";
    private final String REALLOCATE_IMEI_URI = "/v1/imei";
    private final String EDIT_PHONE_URI = "/v1/phone";
    private ThreadPoolTaskExecutor spyTaskExecutor = new ThreadPoolTaskExecutor();

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(adherenceController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(29L, "test", "test", null));
        spyTaskExecutor.setCorePoolSize(1);
        spyTaskExecutor.setWaitForTasksToCompleteOnShutdown(true);
        spyTaskExecutor.initialize();
        ReflectionTestUtils.setField(adherenceController, "taskExecutor", spyTaskExecutor);
    }

    @Test
    public void adherenceConfigTest() throws Exception {
        String uri = "/v1/adherence/config";
        List<AdherenceCodeConfigResponse> mockResponse = Arrays.asList(
                new AdherenceCodeConfigResponse('2', "MISSED", null, "Missed Dose")
        );
        when(adherenceService.getAdherenceConfig()).thenReturn(mockResponse);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].code").value("2"));
    }

    @Test
    public void getAdherenceTest() throws Exception {
        String uri = "/v1/adherence/123456";
        String episodeId = "123456";
        List<AdherenceData> adherenceDataList = Arrays.asList(
                new AdherenceData("555444", "1")
        );
        AdherenceResponse adherenceResponse = new AdherenceResponse(episodeId, adherenceDataList, "666666", Utils.toLocalDateTimeWithNull("01-01-2022 01:00:00"), 0, 0, 0, null,null,null, 0,0, new ArrayList<>(), "MERM",  Utils.toLocalDateTimeWithNull("01-01-2022 01:00:00"));
        ResponseEntity<Response<AdherenceResponse>> response
                = new ResponseEntity<>(
                        new Response<>(adherenceResponse),
                        HttpStatus.OK
                );

        Map<String, Object> stageData = new HashMap<>();
        EpisodeDto episodeDto = new EpisodeDto();
        episodeDto.setStageData(stageData);

        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(29L, "test", "test", null));
        when(episodeService.getEpisodeDetails(any(), eq(29L))).thenReturn(episodeDto);

        when(iamService.getAdherence(eq(episodeId), any())).thenReturn(response);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.entityId").value("123456"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.adherenceString").value("666666"));
    }

    @Test
    public void testGetAdherence_withTreatmentPlanId() throws Exception {
        String episodeId = "1";
        Boolean logsRequired = false;
        EpisodeDto episodeDto = new EpisodeDto();
        episodeDto.setStageData(Collections.singletonMap(FieldConstants.ESD_TREATMENT_PLAN_ID, "1"));
        when(episodeService.getEpisodeDetails(eq(1L), any())).thenReturn(episodeDto);

        List<TreatmentPlanProductMap> treatmentPlanProductMapList = new ArrayList<>();
        TreatmentPlanProductMap treatmentPlanProductMap = new TreatmentPlanProductMap();
        treatmentPlanProductMap.setId(1L);
        treatmentPlanProductMap.setSos(false);
        treatmentPlanProductMapList.add(treatmentPlanProductMap);
        when(treatmentPlanService.getTreatmentPlanProductMapList(eq(1L))).thenReturn(treatmentPlanProductMapList);

        AdherenceResponse adherenceResponse = new AdherenceResponse();
        AdherenceData adherenceData = new AdherenceData();
        adherenceData.setAdhTechLogsData(Arrays.asList(
                new AdhTechLogsData()
        ));
        adherenceResponse.setAdherenceData(
                Arrays.asList(new AdherenceData())
        );
        adherenceResponse.setStartDate(Utils.getCurrentDateNew());
        List<AdherenceResponse> adherenceResponseList = new ArrayList<>();
        adherenceResponseList.add(adherenceResponse);

        AllAdherenceResponse allAdherenceResponse = new AllAdherenceResponse(adherenceResponseList, "666");
        when(adherenceService.getAdherenceBulk(any(SearchAdherenceRequest.class), eq(true))).thenReturn(allAdherenceResponse);

        List<AdhTechLogsData> adhTechLogsDataList = new ArrayList<>();
        AdhTechLogsData adhTechLogsData = new AdhTechLogsData();
        adhTechLogsDataList.add(adhTechLogsData);
        Response<AdherenceResponse> response = new Response<>(true, adherenceResponse);
        ResponseEntity<Response<AdherenceResponse>> responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
        when(iamService.getAdherence(eq("med_1"), eq(logsRequired))).thenReturn(responseEntity);

        mockMvc.perform(MockMvcRequestBuilders.get("/v1/adherence/{episodeId}", episodeId)
                        .param("logsRequired", logsRequired.toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void editDosesBulk_validRequest_shouldReturnOk() throws Exception {
        // Create mock request and response objects
        EditDoseBulkRequest request = new EditDoseBulkRequest();
        List<EditDosesRequest> editDosesRequestList = new ArrayList<>();
        EditDosesRequest editDosesRequest = new EditDosesRequest();
        editDosesRequest.setTimezone("Asia/Calcutta");
        editDosesRequest.setIamEntityId("111");
        editDosesRequest.setFetchAdherence(true);
        editDosesRequestList.add(editDosesRequest);
        EpisodeLog episodeLog = new EpisodeLog(
                1L, 1L, "category", "action", "comments"
        );
        request.setEditDosesRequestList(editDosesRequestList);

        AdherenceResponse adherenceResponse = new AdherenceResponse();
        AdherenceData adherenceData = new AdherenceData();
        adherenceData.setAdhTechLogsData(Arrays.asList(
                new AdhTechLogsData()
        ));
        adherenceResponse.setAdherenceData(
                Arrays.asList(new AdherenceData())
        );
        List<AdherenceResponse> adherenceResponseList = new ArrayList<>();
        adherenceResponseList.add(adherenceResponse);

        Map<String, Object> stageData = new HashMap<>();
        EpisodeDto episodeDto = new EpisodeDto();
        episodeDto.setStageData(stageData);

        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(29L, "test", "test", null));
        when(episodeService.getEpisodeDetails(any(), eq(29L), eq(false), eq(false), eq(false), eq(false))).thenReturn(episodeDto);

        // Mock service methods
        Mockito.when(adherenceService.editDosesBulk(request)).thenReturn(new HashMap<>());
        Mockito.when(episodeLogService.addAdherenceLogs(Mockito.any(EditDosesRequest.class), any(), any())).thenReturn(episodeLog);
        Mockito.when(iamService.getAdherenceBulk(Mockito.any(SearchAdherenceRequest.class), any())).thenReturn(new ResponseEntity<>(new Response<>(new AllAdherenceResponse(adherenceResponseList, "666")), HttpStatus.OK));

        // Send request
        mockMvc.perform(
                    MockMvcRequestBuilders.put("/v1/adherence/edit-doses/bulk")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .content(Utils.asJsonString(request))
                )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.mergedAdherence").value("666"));
    }

    @Test
    public void editDosesBulk_validRequest_hasTreatmentPlan() throws Exception {
        // Create mock request and response objects
        EditDoseBulkRequest request = new EditDoseBulkRequest();
        List<EditDosesRequest> editDosesRequestList = new ArrayList<>();
        EditDosesRequest editDosesRequest = new EditDosesRequest();
        editDosesRequest.setTimezone("Asia/Calcutta");
        editDosesRequest.setIamEntityId("111");
        editDosesRequest.setFetchAdherence(true);
        editDosesRequestList.add(editDosesRequest);
        EpisodeLog episodeLog = new EpisodeLog(
                1L, 1L, "category", "action", "comments"
        );
        request.setEditDosesRequestList(editDosesRequestList);

        AdherenceResponse adherenceResponse = new AdherenceResponse();
        AdherenceData adherenceData = new AdherenceData();
        adherenceData.setAdhTechLogsData(Arrays.asList(
                new AdhTechLogsData()
        ));
        adherenceResponse.setAdherenceData(
                Arrays.asList(new AdherenceData())
        );
        List<AdherenceResponse> adherenceResponseList = new ArrayList<>();
        adherenceResponseList.add(adherenceResponse);

        List<TreatmentPlanProductMap> treatmentPlanProductMapList = new ArrayList<>();
        TreatmentPlanProductMap treatmentPlanProductMap = new TreatmentPlanProductMap();
        treatmentPlanProductMap.setId(1L);
        treatmentPlanProductMapList.add(treatmentPlanProductMap);

        Map<String, Object> stageData = new HashMap<>();
        EpisodeDto episodeDto = new EpisodeDto();
        stageData.put(FieldConstants.ESD_TREATMENT_PLAN_ID, "123");
        episodeDto.setStageData(stageData);

        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(29L, "test", "test", null));
        when(episodeService.getEpisodeDetails(any(), eq(29L), eq(false), eq(false), eq(false), eq(false))).thenReturn(episodeDto);
        when(treatmentPlanService.getTreatmentPlanProductMapList(anyLong())).thenReturn(treatmentPlanProductMapList);
        // Mock service methods
        Mockito.when(adherenceService.editDosesBulk(request)).thenReturn(new HashMap<>());
        Mockito.when(episodeLogService.addAdherenceLogs(Mockito.any(EditDosesRequest.class), any(), any())).thenReturn(episodeLog);
        Mockito.when(iamService.getAdherenceBulk(Mockito.any(SearchAdherenceRequest.class), any())).thenReturn(new ResponseEntity<>(new Response<>(new AllAdherenceResponse(adherenceResponseList, "666")), HttpStatus.OK));

        // Send request
        mockMvc.perform(
                        MockMvcRequestBuilders.put("/v1/adherence/edit-doses/bulk")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                                .content(Utils.asJsonString(request))
                )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.mergedAdherence").value("666"));
    }

    @Test
    public void editDosesBulk_validRequest_throwException() throws Exception {
        // Create mock request and response objects
        EditDoseBulkRequest request = new EditDoseBulkRequest();
        List<EditDosesRequest> editDosesRequestList = new ArrayList<>();
        EditDosesRequest editDosesRequest = new EditDosesRequest();
        editDosesRequest.setTimezone("Asia/Calcutta");
        editDosesRequest.setIamEntityId("111");
        editDosesRequest.setFetchAdherence(true);
        editDosesRequestList.add(editDosesRequest);
        EpisodeLog episodeLog = new EpisodeLog(
                1L, 1L, "category", "action", "comments"
        );
        request.setEditDosesRequestList(editDosesRequestList);

        AdherenceResponse adherenceResponse = new AdherenceResponse();
        AdherenceData adherenceData = new AdherenceData();
        adherenceData.setAdhTechLogsData(Arrays.asList(
                new AdhTechLogsData()
        ));
        adherenceResponse.setAdherenceData(
                Arrays.asList(new AdherenceData())
        );
        List<AdherenceResponse> adherenceResponseList = new ArrayList<>();
        adherenceResponseList.add(adherenceResponse);

        List<TreatmentPlanProductMap> treatmentPlanProductMapList = new ArrayList<>();
        TreatmentPlanProductMap treatmentPlanProductMap = new TreatmentPlanProductMap();
        treatmentPlanProductMap.setId(1L);
        treatmentPlanProductMapList.add(treatmentPlanProductMap);

        Map<String, Object> stageData = new HashMap<>();
        EpisodeDto episodeDto = new EpisodeDto();
        stageData.put(FieldConstants.ESD_TREATMENT_PLAN_ID, "123");
        episodeDto.setStageData(stageData);

        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(29L, "test", "test", null));
        when(episodeService.getEpisodeDetails(any(), eq(29L), eq(false), eq(false), eq(false), eq(false))).thenReturn(episodeDto);
        when(treatmentPlanService.getTreatmentPlanProductMapList(anyLong())).thenReturn(treatmentPlanProductMapList);
        // Mock service methods
        Mockito.when(adherenceService.editDosesBulk(request)).thenReturn(new HashMap<>());
        Mockito.when(episodeLogService.addAdherenceLogs(Mockito.any(EditDosesRequest.class), any(), any())).thenReturn(episodeLog);
        when(iamService.getAdherenceBulk(any(), any())).thenThrow(new ValidationException("something went wrong"));

        // Send request
        mockMvc.perform(
                        MockMvcRequestBuilders.put("/v1/adherence/edit-doses/bulk")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                                .content(Utils.asJsonString(request))
                )
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Unable to fetch Updated Adherence"));
    }

    @Test
    public void getAdherenceSearchTest() throws Exception {
        String uri = "/v1/adherence/search";
        List<String> entityIdList = new ArrayList<>();
        String entityId = "123456";
        entityIdList.add(entityId);
        String from = "2019-08-31 18:30:00";
        String to = "2019-10-31 18:30:00";
        RangeFilters rangeFilter = new RangeFilters(RangeFilterType.LAST_DOSAGE, from, to);
        List<RangeFilters> filters = new ArrayList<>();
        filters.add(rangeFilter);
        SearchAdherenceRequest adherenceRequest = new SearchAdherenceRequest(entityIdList,filters);
        List<AdherenceData> adherenceDataList = new ArrayList<>();
        AdherenceData adherenceData = new AdherenceData();
        adherenceData.setStartDate(Utils.toLocalDateTimeWithNull("18-01-2019 18:30:00"));
        adherenceDataList.add(adherenceData);
        AdherenceResponse adherenceResponse = new AdherenceResponse(entityId, adherenceDataList, "666666", Utils.toLocalDateTimeWithNull("01-01-2022 01:00:00"), 0, 0, 0, null,null,null, 0,0, new ArrayList<>(), "MERM",  Utils.toLocalDateTimeWithNull("01-01-2022 01:00:00"));
        List<AdherenceResponse> allEntityResponseList = new ArrayList<>();
        allEntityResponseList.add(adherenceResponse);
        AllAdherenceResponse allAdherenceResponse = new AllAdherenceResponse(allEntityResponseList);
        ResponseEntity<Response<AllAdherenceResponse>> response
                = new ResponseEntity<>(
                new Response<>(allAdherenceResponse),
                HttpStatus.OK
        );
        when(iamService.getAdherenceBulk(any(), any())).thenReturn(response);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(adherenceRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.adherenceResponseList[0].adherenceString").value("666666"));
    }

    @Test
    public void getAdherenceForEpisodeMonthWiseTest() throws Exception {
        String uri = "/v1/adherence/monthly/12345?timezone=Europe/Tallinn";
        String episodeId = "12345";
        List<AdherenceData> adherenceDataList = Arrays.asList(
                new AdherenceData("555444", "1")
        );
        AdherenceResponse adherenceResponse = new AdherenceResponse(episodeId, adherenceDataList, "666666", Utils.toLocalDateTimeWithNull("01-01-2022 01:00:00"), 0, 0, 0, null,null,null, 0,0, new ArrayList<>(), "MERM",  Utils.toLocalDateTimeWithNull("01-01-2022 01:00:00"));
        ResponseEntity<Response<AdherenceResponse>> response
                = new ResponseEntity<>(
                new Response<>(adherenceResponse),
                HttpStatus.OK
        );

        List<AdherenceMonthWiseData> adherenceMonthWiseData = Arrays.asList(
                new AdherenceMonthWiseData("JANUARY", 1, 31, 2022, null)
        );

        EpisodeTagResponse tagResponse = EpisodeTagsControllerTest.getEpisodeTags().get(0);
        ZoneId zoneId = ZoneId.of("Europe/Tallinn");
        when(iamService.getAdherence(eq(Long.valueOf(episodeId)), eq(false))).thenReturn(response);
        when(episodeTagService.getEpisodeTags(Long.valueOf(episodeId))).thenReturn(tagResponse);
        when(adherenceService.formatAdherenceStringMonthWise(eq(adherenceResponse), eq(tagResponse), eq(zoneId)))
                .thenReturn(adherenceMonthWiseData);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.adherenceMonthWiseData[0].monthName").value("JANUARY"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.adherenceMonthWiseData[0].monthNumber").value(1));
    }

    @Test
    public void getAdherenceForEpisodeMonthWiseTestResponseBodySuccessFalse() throws Exception {
        String uri = "/v1/adherence/monthly/12345?timezone=Europe/Tallinn";
        String episodeId = "12345";
        ResponseEntity<Response<AdherenceResponse>> response
                = new ResponseEntity<>(
                    new Response<>(false, null, "Entity mapping does not exists!"),
                    HttpStatus.BAD_REQUEST
                );
        when(iamService.getAdherence(eq(Long.valueOf(episodeId)), eq(false))).thenReturn(response);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Entity mapping does not exists!"));
    }

    @Test
    public void getAdherenceForEpisodeMonthWiseTestResponseBodyNull() throws Exception {
        String uri = "/v1/adherence/monthly/12345?timezone=Europe/Tallinn";
        String episodeId = "12345";
        ResponseEntity<Response<AdherenceResponse>> response
                = new ResponseEntity<>(
                new Response<>(null),
                HttpStatus.OK
        );
        when(iamService.getAdherence(eq(Long.valueOf(episodeId)), eq(false))).thenReturn(response);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isNotFound());
    }


    @Test
    public void getAdherenceForEpisodeMonthWiseWrongTimeZoneTest() throws Exception {
        String uri = "/v1/adherence/monthly/12345?timezone=IND";
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getAdherenceForEpisodeMonthWiseBulkTestNullList() throws Exception {
        String uri = "/v1/adherence/monthly/bulk";
        MonthlyAdherenceBulkRequest monthlyAdherenceBulkRequest =
                new MonthlyAdherenceBulkRequest(
                        null,
                        "Europe/Tallinn"
                );
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(monthlyAdherenceBulkRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Invalid episode Id List!"));
    }

    @Test
    public void getAdherenceForEpisodeMonthWiseBulkTestEmptyList() throws Exception {
        String uri = "/v1/adherence/monthly/bulk";
        MonthlyAdherenceBulkRequest monthlyAdherenceBulkRequest =
                new MonthlyAdherenceBulkRequest(
                        new ArrayList<>(),
                        "Europe/Tallinn"
                );
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(monthlyAdherenceBulkRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Invalid episode Id List!"));
    }

    @Test
    public void getAdherenceForEpisodeMonthWiseBulkTestListSizeExceeds() throws Exception {
        String uri = "/v1/adherence/monthly/bulk";
        List<Long> ids = new ArrayList<>();
        for (int i = 0; i < 3001; i++)
            ids.add((long) i);
        MonthlyAdherenceBulkRequest monthlyAdherenceBulkRequest =
                new MonthlyAdherenceBulkRequest(
                        ids,
                        "Europe/Tallinn"
                );
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(monthlyAdherenceBulkRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Episode Ids should be less than 3000!"));
    }

    @Test
    public void getAdherenceForEpisodeMonthWiseBulkTest() throws Exception {
        String uri = "/v1/adherence/monthly/bulk";
        MonthlyAdherenceBulkRequest monthlyAdherenceBulkRequest =
            new MonthlyAdherenceBulkRequest(
                    Arrays.asList(1L),
                    "Asia/Kolkata"
            );
        List<AdherenceData> adherenceDataList = Arrays.asList(
            new AdherenceData("555444", "1")
        );
        AdherenceResponse adherenceResponse = new AdherenceResponse("1", adherenceDataList, "555444", Utils.toLocalDateTimeWithNull("01-01-2022 01:00:00"), 0, 0, 0, null,null,null, 0,0, new ArrayList<>(), "MERM",  Utils.toLocalDateTimeWithNull("01-01-2022 01:00:00"));
        AllAdherenceResponse allAdherenceResponse = new AllAdherenceResponse(
                Arrays.asList(adherenceResponse)
        );
        ResponseEntity<Response<AllAdherenceResponse>> bulkAdherenceResponse
                = new ResponseEntity<>(
                new Response<>(allAdherenceResponse),
                HttpStatus.OK
        );

        List<AdherenceMonthWiseData> adherenceMonthWiseData = Arrays.asList(
                new AdherenceMonthWiseData("JAN", 1, 31, 2022,
                        Arrays.asList(
                                new AdherenceDataForCalendar('6', "TUESDAY", new ArrayList<>())
                        ))
        );

        when(iamService.getAdherenceBulk(any())).thenReturn(bulkAdherenceResponse);
        when(episodeTagService.search(any())).thenReturn(Arrays.asList(
                new EpisodeTagResponse(1L, Arrays.asList(
                        new EpisodeTag(1L, "No_Coverage", Utils.toLocalDateTimeWithNull("02-01-2022 18:30:00"), false)
                ), Arrays.asList("No"))
        ));
        when(adherenceService.formatAdherenceStringMonthWise(any(), any(), any())).thenReturn(adherenceMonthWiseData);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(monthlyAdherenceBulkRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].adherenceMonthWiseData[0].monthName").value("JAN"));
    }

    @Test
    public void getAdherenceForEpisodeMonthWiseBulkTestNullResponse() throws Exception {
        String uri = "/v1/adherence/monthly/bulk";
        MonthlyAdherenceBulkRequest monthlyAdherenceBulkRequest =
                new MonthlyAdherenceBulkRequest(
                        Arrays.asList(1L),
                        "Asia/Kolkata"
                );
        List<AdherenceMonthWiseData> adherenceMonthWiseData = Arrays.asList(
                new AdherenceMonthWiseData("JAN", 1, 31, 2022,
                        Arrays.asList(
                                new AdherenceDataForCalendar('6', "TUESDAY", new ArrayList<>())
                        ))
        );

        when(iamService.getAdherenceBulk(any())).thenReturn(null);
        when(episodeTagService.search(any())).thenReturn(Arrays.asList(
                new EpisodeTagResponse(1L, Arrays.asList(
                        new EpisodeTag(1L, "No_Coverage", Utils.toLocalDateTimeWithNull("02-01-2022 18:30:00"), false)
                ), Arrays.asList("No"))
        ));
        when(adherenceService.formatAdherenceStringMonthWise(any(), any(), any())).thenReturn(adherenceMonthWiseData);

        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .post(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(Utils.asJsonString(monthlyAdherenceBulkRequest))
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isNotFound());
    }

    @Test
    public void getAdherenceForEpisodeMonthWiseBulkTestNullResponseBody() throws Exception {
        String uri = "/v1/adherence/monthly/bulk";
        MonthlyAdherenceBulkRequest monthlyAdherenceBulkRequest =
                new MonthlyAdherenceBulkRequest(
                        Arrays.asList(1L),
                        "Asia/Kolkata"
                );
        List<AdherenceMonthWiseData> adherenceMonthWiseData = Arrays.asList(
                new AdherenceMonthWiseData("JAN", 1, 31, 2022,
                        Arrays.asList(
                                new AdherenceDataForCalendar('6', "TUESDAY", new ArrayList<>())
                        ))
        );

        ResponseEntity<Response<AllAdherenceResponse>> bulkAdherenceResponse
                = new ResponseEntity<>(
                null,
                HttpStatus.OK
        );

        when(iamService.getAdherenceBulk(any())).thenReturn(bulkAdherenceResponse);
        when(episodeTagService.search(any())).thenReturn(Arrays.asList(
                new EpisodeTagResponse(1L, Arrays.asList(
                        new EpisodeTag(1L, "No_Coverage", Utils.toLocalDateTimeWithNull("02-01-2022 18:30:00"), false)
                ), Arrays.asList("No"))
        ));
        when(adherenceService.formatAdherenceStringMonthWise(any(), any(), any())).thenReturn(adherenceMonthWiseData);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(monthlyAdherenceBulkRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isNotFound());
    }

    @Test
    public void getAdherenceForEpisodeMonthWiseBulkTestBodyFalse() throws Exception {
        String uri = "/v1/adherence/monthly/bulk";
        MonthlyAdherenceBulkRequest monthlyAdherenceBulkRequest =
                new MonthlyAdherenceBulkRequest(
                        Arrays.asList(1L),
                        "Asia/Kolkata"
                );

        ResponseEntity<Response<AllAdherenceResponse>> bulkAdherenceResponse
                = new ResponseEntity<>(
                new Response<>(false, null, "Entity Mapping does not exists!"),
                HttpStatus.OK
        );

        when(iamService.getAdherenceBulk(any())).thenReturn(bulkAdherenceResponse);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(monthlyAdherenceBulkRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Entity Mapping does not exists!"));
    }

    @Test
    public void getAdherenceForEpisodeMonthWiseBulkTestBodyEmptyList() throws Exception {
        String uri = "/v1/adherence/monthly/bulk";
        MonthlyAdherenceBulkRequest monthlyAdherenceBulkRequest =
                new MonthlyAdherenceBulkRequest(
                        Arrays.asList(1L),
                        "Asia/Kolkata"
                );
        ResponseEntity<Response<AllAdherenceResponse>> bulkAdherenceResponse
                = new ResponseEntity<>(
                    new Response<>(new AllAdherenceResponse(Arrays.asList())),
                    HttpStatus.OK
                );

        when(iamService.getAdherenceBulk(any())).thenReturn(bulkAdherenceResponse);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(monthlyAdherenceBulkRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Entity mappings do not exist"));
    }

    @Test
    public void getAdherenceForEpisodeMonthWiseBulkTestInterruptedException() throws Exception {
        String uri = "/v1/adherence/monthly/bulk";
        MonthlyAdherenceBulkRequest monthlyAdherenceBulkRequest =
                new MonthlyAdherenceBulkRequest(
                        Arrays.asList(1L),
                        "Asia/Kolkata"
                );
        List<AdherenceMonthWiseData> adherenceMonthWiseData = Arrays.asList(
                new AdherenceMonthWiseData("JAN", 1, 31, 2022,
                        Arrays.asList(
                                new AdherenceDataForCalendar('6', "TUESDAY", new ArrayList<>())
                        ))
        );

        ResponseEntity<Response<AllAdherenceResponse>> bulkAdherenceResponse
                = new ResponseEntity<>(
                new Response<>(null),
                HttpStatus.OK
        );

        when(iamService.getAdherenceBulk(any())).thenReturn(bulkAdherenceResponse);
        when(episodeTagService.search(any())).thenReturn(Arrays.asList(
                new EpisodeTagResponse(1L, Arrays.asList(
                        new EpisodeTag(1L, "No_Coverage", Utils.toLocalDateTimeWithNull("02-01-2022 18:30:00"), false)
                ), Arrays.asList("No"))
        ));
        when(adherenceService.formatAdherenceStringMonthWise(any(), any(), any())).thenReturn(adherenceMonthWiseData);
        Thread.currentThread().interrupt();
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(monthlyAdherenceBulkRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isNotFound());
    }

    @Test
    public void getAdherenceForEpisodeMonthWiseInvalidTimezone() throws Exception {
        String uri = "/v1/adherence/monthly/bulk";
        MonthlyAdherenceBulkRequest monthlyAdherenceBulkRequest =
                new MonthlyAdherenceBulkRequest(
                        Arrays.asList(1L),
                        "IND"
                );
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(monthlyAdherenceBulkRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("IND - Timezone does not exists!"));
    }

    @Test
    public void editDosesNoEpisodeId() throws Exception {
        EditDosesRequest editDosesRequest = new EditDosesRequest();
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(editDosesUri)
                                .content(Utils.asJsonString(editDosesRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest());
    }

    @Test
    public void editDosesNoUserId() throws Exception {
        EditDosesRequest editDosesRequest = new EditDosesRequest();
        editDosesRequest.setEpisodeId(1L);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(editDosesUri)
                                .content(Utils.asJsonString(editDosesRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest());
    }


    @Test
    public void editDosesNoCode() throws Exception {
        EditDosesRequest editDosesRequest = new EditDosesRequest();
        editDosesRequest.setEpisodeId(1L);
        editDosesRequest.setUserId(1L);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(editDosesUri)
                                .content(Utils.asJsonString(editDosesRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest());
    }

    @Test
    public void editDosesNoMonitoringMethod() throws Exception {
        EditDosesRequest editDosesRequest = new EditDosesRequest();
        editDosesRequest.setEpisodeId(1L);
        editDosesRequest.setUserId(1L);
        editDosesRequest.setCode("PATIENT_MANUAL");

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(editDosesUri)
                                .content(Utils.asJsonString(editDosesRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest());
    }

    @Test
    public void editDosesNoDates() throws Exception {
        EditDosesRequest editDosesRequest = new EditDosesRequest();
        editDosesRequest.setEpisodeId(1L);
        editDosesRequest.setUserId(1L);
        editDosesRequest.setCode("PATIENT_MANUAL");
        editDosesRequest.setMonitoringMethod("99DOTS");

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(editDosesUri)
                                .content(Utils.asJsonString(editDosesRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest());
    }

    @Test
    public void editDosesDatesGreaterThanLimit() throws Exception {
        EditDosesRequest editDosesRequest = new EditDosesRequest();
        editDosesRequest.setEpisodeId(1L);
        editDosesRequest.setUserId(1L);
        editDosesRequest.setCode("PATIENT_MANUAL");
        editDosesRequest.setMonitoringMethod("99DOTS");
        List<String> utcDates = Collections.nCopies(61, "10-10-2021 18:30:00");
        editDosesRequest.setUtcDates(utcDates);


        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(editDosesUri)
                                .content(Utils.asJsonString(editDosesRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Number of days cannot be more than 60"));
    }

    @Test
    public void editDosesWrongTimeZone() throws Exception {
        EditDosesRequest editDosesRequest = new EditDosesRequest();
        editDosesRequest.setEpisodeId(1L);
        editDosesRequest.setUserId(1L);
        editDosesRequest.setCode("PATIENT_MANUAL");
        editDosesRequest.setMonitoringMethod("99DOTS");
        List<String> utcDates = new ArrayList<>();
        utcDates.add("10-10-2021 18:30:00");
        editDosesRequest.setUtcDates(utcDates);
        editDosesRequest.setTimezone("TEST");
        editDosesRequest.setIamEntityId("1");


        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(editDosesUri)
                                .content(Utils.asJsonString(editDosesRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("TEST - Timezone does not exists!"));
    }

    @Test
    public void editDosesNoTimeZone() throws Exception {
        EditDosesRequest editDosesRequest = new EditDosesRequest();
        editDosesRequest.setEpisodeId(1L);
        editDosesRequest.setUserId(1L);
        editDosesRequest.setCode("PATIENT_MANUAL");
        editDosesRequest.setMonitoringMethod("99DOTS");
        List<String> utcDates = new ArrayList<>();
        utcDates.add("10-10-2021 18:30:00");
        editDosesRequest.setUtcDates(utcDates);


        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(editDosesUri)
                                .content(Utils.asJsonString(editDosesRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Timezone cannot be empty!"));
    }

    @Test
    public void editDosesSuccessNoFetchAdherence() throws Exception {
        EditDosesRequest editDosesRequest = new EditDosesRequest();
        editDosesRequest.setEpisodeId(1L);
        editDosesRequest.setUserId(1L);
        editDosesRequest.setCode("PATIENT_MANUAL");
        editDosesRequest.setMonitoringMethod("99DOTS");
        List<String> utcDates = new ArrayList<>();
        utcDates.add("10-10-2021 18:30:00");
        editDosesRequest.setUtcDates(utcDates);
        editDosesRequest.setTimezone("IST");
        editDosesRequest.setFetchAdherence(false);
        editDosesRequest.setIamEntityId("1");

        when(adherenceService.editDoses(eq(editDosesRequest))).thenReturn(Collections.EMPTY_LIST);
        when(episodeLogService.addAdherenceLogs(any(), any(), any())).thenReturn(new EpisodeLog());


        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(editDosesUri)
                                .content(Utils.asJsonString(editDosesRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk());
    }


    @Test
    public void editDosesSuccessFetchAdherence() throws Exception {
        EditDosesRequest editDosesRequest = new EditDosesRequest();
        editDosesRequest.setEpisodeId(12345L);
        editDosesRequest.setUserId(1L);
        editDosesRequest.setCode("PATIENT_MANUAL");
        editDosesRequest.setMonitoringMethod("99DOTS");
        List<String> utcDates = new ArrayList<>();
        utcDates.add("10-10-2021 18:30:00");
        editDosesRequest.setUtcDates(utcDates);
        editDosesRequest.setTimezone("IST");
        editDosesRequest.setFetchAdherence(true);

        EpisodeLog episodeLog = new EpisodeLog();
        String episodeId = "12345";
        List<AdherenceData> adherenceDataList = Arrays.asList(
                new AdherenceData("555444", "1")
        );
        AdherenceResponse adherenceResponse = new AdherenceResponse(episodeId, adherenceDataList, "666666", Utils.toLocalDateTimeWithNull("01-01-2022 01:00:00"), 0, 0, 0, null,null,null, 0,0, new ArrayList<>(), "MERM",  Utils.toLocalDateTimeWithNull("01-01-2022 01:00:00"));
        AllAdherenceResponse allAdherenceResponse = new AllAdherenceResponse(
            Arrays.asList(adherenceResponse), "666666"
        );
        ResponseEntity<Response<AllAdherenceResponse>> response
                = new ResponseEntity<>(
                new Response<>(allAdherenceResponse),
                HttpStatus.OK
        );

        Map<String, Object> stageData = new HashMap<>();
        EpisodeDto episodeDto = new EpisodeDto();
        episodeDto.setStageData(stageData);

        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(29L, "test", "test", null));
        when(episodeService.getEpisodeDetails(any(), eq(29L), eq(false), eq(false), eq(false), eq(false))).thenReturn(episodeDto);

        when(adherenceService.editDoses(eq(editDosesRequest))).thenReturn(Collections.EMPTY_LIST);
        when(episodeLogService.addAdherenceLogs(any(), any(), any())).thenReturn(episodeLog);
        when(iamService.getAdherenceBulk(any(), any())).thenReturn(response);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(editDosesUri)
                                .content(Utils.asJsonString(editDosesRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.adherenceString").value("666666"));
    }

    @Test
    public void editDosesFailureFetchAdherence() throws Exception {
        EditDosesRequest editDosesRequest = new EditDosesRequest();
        editDosesRequest.setEpisodeId(12345L);
        editDosesRequest.setUserId(1L);
        editDosesRequest.setCode("PATIENT_MANUAL");
        editDosesRequest.setMonitoringMethod("99DOTS");
        List<String> utcDates = new ArrayList<>();
        utcDates.add("10-10-2021 18:30:00");
        editDosesRequest.setUtcDates(utcDates);
        editDosesRequest.setTimezone("IST");
        editDosesRequest.setFetchAdherence(true);

        EpisodeLog episodeLog = new EpisodeLog();

        Map<String, Object> stageData = new HashMap<>();
        EpisodeDto episodeDto = new EpisodeDto();
        episodeDto.setStageData(stageData);

        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(29L, "test", "test", null));
        when(episodeService.getEpisodeDetails(any(), eq(29L), eq(false), eq(false), eq(false), eq(false))).thenReturn(episodeDto);

        when(adherenceService.editDoses(eq(editDosesRequest))).thenReturn(Collections.EMPTY_LIST);
        when(episodeLogService.addAdherenceLogs(any(), any(), any())).thenReturn(episodeLog);
        when(iamService.getAdherenceBulk(any(), any())).thenThrow(new ValidationException("something went wrong"));

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(editDosesUri)
                                .content(Utils.asJsonString(editDosesRequest))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getAdherenceGlobalConfigTest() throws Exception {
        String uri = "/v1/adherence-global-config";
        List<AdherenceCodeConfigResponse> adherenceCodeConfigResponseList =  Arrays.asList(
                new AdherenceCodeConfigResponse('6', "NO_INFO", null, "No Info")
        );
        List<GenericConfigResponse> adherenceTechConfigResponseList =  Arrays.asList(
                new GenericConfigResponse("MERM", 3)
        );
        List<GenericConfigResponse> scheduleTypeConfigResponseList =  Arrays.asList(
                new GenericConfigResponse("DAILY", 1)
        );
        AdherenceGlobalConfigResponse adherenceGlobalConfigResponse = new AdherenceGlobalConfigResponse(adherenceCodeConfigResponseList,
                adherenceTechConfigResponseList, scheduleTypeConfigResponseList);
        ResponseEntity< Response<AdherenceGlobalConfigResponse>> responseEntity = new ResponseEntity<>(new Response<>(adherenceGlobalConfigResponse), HttpStatus.OK);

        when(iamService.getAdherenceGlobalConfig(eq(new HashMap<>()))).thenReturn(responseEntity);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(new HashMap<>()))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.adherenceTechConfigResponseList[0].name").value("MERM"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.adherenceCodeConfigResponseList[0].codeName").value("NO_INFO"));
    }

    @Test
    public void  getImeiTest() throws Exception {
        String uri = GET_IMEI_URI+"?entityId=1&active=true";
        ResponseEntity<Response<List<Map<String, Object>>>> response = new ResponseEntity<>(
                new Response<>(new ArrayList<>()),
                HttpStatus.OK
        );
        when(iamService.getImei(eq("1"), eq(true))).thenReturn(response);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value(new ArrayList<>()));
    }

    @Test
    public void getImeiBulkTest() throws Exception {
        ResponseEntity<Response<List<Map<String, Object>>>> response = new ResponseEntity<>(
                new Response<>(new ArrayList<>()),
                HttpStatus.OK
        );
        when(iamService.getImeiBulk(eq(new HashMap<>()))).thenReturn(response);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(GET_IMEI_BULK_URI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(new HashMap<>()))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value(new ArrayList<>()));
    }

    @Test
    public void getActiveEntityFromImeiTest() throws Exception {
        String uri = GET_IMEI_ENTITY_URI+"?imei=1";
        ResponseEntity<Response<List<Map<String, Object>>>> response = new ResponseEntity<>(
                new Response<>(new ArrayList<>()),
                HttpStatus.OK
        );
        when(iamService.getActiveEntityFromImei(eq("1"))).thenReturn(response);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value(new ArrayList<>()));
    }

    @Test
    public void updateVideoStatusTest() throws Exception {
        ResponseEntity<Response<Map<String, Object>>> response = new ResponseEntity<>(
                new Response<>(new HashMap<>()),
                HttpStatus.OK
        );
        when(iamService.updateVideoStatus(eq(new HashMap<>()))).thenReturn(response);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(UPDATE_VIDEO_STATUS_URI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(new HashMap<>()))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value(new ArrayList<>()));
    }

    @Test
    public void getPositiveEventsCountTest() throws Exception {
        PositiveCountResponse positiveCountResponse = new PositiveCountResponse(1L, 10L, 100L);
        ResponseEntity<Response<PositiveCountResponse>> responseEntity = new ResponseEntity<>(new Response<>(positiveCountResponse), HttpStatus.OK);
        PositiveAdherenceRequest positiveAdherenceRequest = new PositiveAdherenceRequest();
        when(iamService.getPositiveEventCount(any())).thenReturn(responseEntity);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(GET_ADHERENCE_POSITIVE_EVENTS_URI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(positiveAdherenceRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.manualCount").value(1L))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.digitalCount").value(10L))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.missedCount").value(100L));
    }

    @Test
    public void getAverageAdherenceTest() throws Exception{
        AllAvgAdherenceResponse allAvgAdherenceResponse = new AllAvgAdherenceResponse(1L, 10L, new HashMap<>(), new ArrayList<>());
        ResponseEntity<Response<AllAvgAdherenceResponse>> responseEntity = new ResponseEntity<>(new Response<>(allAvgAdherenceResponse), HttpStatus.OK);
        AverageAdherenceRequest averageAdherenceRequest = new AverageAdherenceRequest();
        when(iamService.getAverageAdherence(any())).thenReturn(responseEntity);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(GET_AVERAGE_ADHERENCE_URI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(averageAdherenceRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.digitalAdherence").value(1L))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.totalAdherence").value(10L));
    }

    @Test 
    public void reallocateImeiTest() throws Exception {
        ResponseEntity<Response<Long>> response = new ResponseEntity<>(
                new Response<>(1L),
                HttpStatus.OK
        );
        when(iamService.reallocateIMEI(eq(new HashMap<>()))).thenReturn(response);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(REALLOCATE_IMEI_URI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(new HashMap<>()))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value(1L));
    }


    @Test
    public void editPhoneTest() throws Exception {
        ResponseEntity<Response<String>> response = new ResponseEntity<>(
                new Response<>("data", true),
                HttpStatus.OK
        );
        when(iamService.editPhone(eq(new HashMap<>()))).thenReturn(response);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(EDIT_PHONE_URI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(new HashMap<>()))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value("data"));
    }

    @Test
    public void testEditDosesWhenTreatmentPlanIdIsPresent() throws Exception {
        EditDosesRequest editDosesRequest = new EditDosesRequest();
        editDosesRequest.setEpisodeId(1L);
        editDosesRequest.setUserId(1L);
        editDosesRequest.setCode("PATIENT_MANUAL");
        editDosesRequest.setMonitoringMethod("99DOTS");
        editDosesRequest.setTimezone("Asia/Calcutta");

        List<String> utcDates = new ArrayList<>();
        utcDates.add("03-01-2023 10:00:00");
        utcDates.add("04-01-2023 11:00:00");
        editDosesRequest.setUtcDates(utcDates);
        editDosesRequest.setAddLogs(true);

        EpisodeDto episodeDto = new EpisodeDto();
        Map<String, Object> stageData = new HashMap<>();
        stageData.put(FieldConstants.ESD_TREATMENT_PLAN_ID, 2L);
        episodeDto.setStageData(stageData);

        when(episodeService.getEpisodeDetails(eq(editDosesRequest.getEpisodeId()), anyLong(), eq(false), eq(false), eq(false), eq(false))).thenReturn(episodeDto);

        List<TreatmentPlanProductMap> treatmentPlanProductMaps = new ArrayList<>();
        TreatmentPlanProductMap tppm = new TreatmentPlanProductMap();
        tppm.setId(3L);
        tppm.setStartDate(Utils.convertStringToDateNew("2023-01-01 00:00:00"));
        treatmentPlanProductMaps.add(tppm);

        when(treatmentPlanService.getMedicinesForSchedule(anyList(), anyLong())).thenReturn(treatmentPlanProductMaps);
        EditDoseBulkResponse editDoseBulkResponse = new EditDoseBulkResponse();
        editDoseBulkResponse.setMergedAdherence("DDDD");

        List<EditDosesResponse> editDosesResponseList = new ArrayList<>();
        EditDosesResponse editDosesResponse = new EditDosesResponse();
        editDosesResponse.setActionTaken("Mocked Action Taken");
        editDosesResponse.setComments("Mocked Comments");
        editDosesResponse.setAddedOn(LocalDateTime.of(2022, 1, 1, 10, 30, 0));
        editDosesResponse.setAdherenceString("DDDD");
        editDosesResponseList.add(editDosesResponse);

        editDoseBulkResponse.setEditDosesResponseList(editDosesResponseList);

        ResponseEntity<Response<EditDoseBulkResponse>> response = ResponseEntity.ok(new Response<>(editDoseBulkResponse));

        doReturn(response).when(adherenceController).editDosesBulk(any());

        this.mockMvc.perform(MockMvcRequestBuilders.put("/v1/adherence/edit-doses")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Utils.asJsonString(editDosesRequest))
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.adherenceString").value("DDDD"));
    }

    @Test
    public void testUpdateEntity() throws Exception {
        // Mock the response from the iamService
        Map<String, Object> data = new HashMap<>();
        data.put("id", 12345L);
        Response<Map<String, Object>> response = new Response<>(data);
        ResponseEntity<Response<Map<String, Object>>> responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
        when(iamService.updateEntity(any(EntityRequest.class))).thenReturn(responseEntity);

        // Send the request to the controller
        mockMvc.perform(MockMvcRequestBuilders.put("/v1/entity")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Utils.asJsonString(new EntityRequest())))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteEntity() throws Exception {
        String entityId = "123";
        String endDate = "2023-04-09";
        boolean updateEndDate = true;
        boolean deleteIfRequired = false;
        Map<String, Object> response = new HashMap<>();
        response.put("message", "Entity deleted successfully");
        Response<Map<String, Object>> iamResponse = new Response<>(response);
        when(iamService.deleteEntity(entityId, endDate, updateEndDate, deleteIfRequired)).thenReturn(new ResponseEntity<>(iamResponse, HttpStatus.OK));

        mockMvc.perform(
                MockMvcRequestBuilders.delete("/v1/entity")
                        .param("entityId", entityId)
                        .param("endDate", endDate)
                        .param("updateEndDate", String.valueOf(updateEndDate))
                        .param("deleteIfRequired", String.valueOf(deleteIfRequired))
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk());

        verify(iamService, times(1)).deleteEntity(entityId, endDate, updateEndDate, deleteIfRequired);
    }
}

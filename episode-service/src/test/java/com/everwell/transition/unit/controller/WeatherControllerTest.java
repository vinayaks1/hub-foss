package com.everwell.transition.unit.controller;

import com.everwell.transition.BaseTest;
import com.everwell.transition.controller.WeatherController;
import com.everwell.transition.exceptions.CustomExceptionHandler;
import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.db.VendorConfig;
import com.everwell.transition.model.response.WeatherResponse;
import com.everwell.transition.repositories.VendorConfigRepository;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.WeatherService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Date;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class WeatherControllerTest extends BaseTest {

    private MockMvc mockMvc;

    @Spy
    @InjectMocks
    private WeatherController weatherController;

    @Mock
    private WeatherService weatherService;

    @Mock
    private VendorConfigRepository configRepository;

    @Mock
    private ClientService clientService;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(weatherController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void testWeather() throws Exception {
        String uri = "/v1/weather?lat=17.66&lon=13.986";
        when(weatherService.getCompleteWeather(anyDouble(), anyDouble(), anyLong())).thenReturn(new WeatherResponse());
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(1L, "test", "ahgfsdsj", new Date()));
        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }
}

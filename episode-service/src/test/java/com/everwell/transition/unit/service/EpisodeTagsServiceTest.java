package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.model.db.EpisodeTag;
import com.everwell.transition.model.db.EpisodeTagStore;
import com.everwell.transition.model.request.episodetags.EpisodeTagDataRequest;
import com.everwell.transition.model.request.episodetags.EpisodeTagBulkDeletionRequest;
import com.everwell.transition.model.request.episodetags.EpisodeTagDeletionRequest;
import com.everwell.transition.model.request.episodetags.SearchTagsRequest;
import com.everwell.transition.model.response.EpisodeTagResponse;
import com.everwell.transition.model.response.TagsConfigResponse;
import com.everwell.transition.postconstruct.EpisodeTagStoreCreation;
import com.everwell.transition.repositories.EpisodeTagRepository;
import com.everwell.transition.service.impl.EpisodeTagServiceImpl;
import com.everwell.transition.utils.Utils;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

public class EpisodeTagsServiceTest extends BaseTest {

    @InjectMocks
    EpisodeTagServiceImpl episodeTagService;

    @Mock
    EpisodeTagRepository episodeTagRepository;

    @Mock
    EpisodeTagStoreCreation episodeTagStoreCreation;

    @Test
    public void getEpisodeTagsTest() {
        when(episodeTagRepository.getAllByEpisodeId(anyLong())).thenReturn(getEpisodeTags());
        EpisodeTagResponse episodeTags = episodeTagService.getEpisodeTags(1234L);
        for (int i = 0; i < episodeTags.getTags().size(); i++) {
            Assertions.assertEquals(episodeTags.getTags().get(i).getTagName(), getEpisodeTags().get(i).getTagName());
        }

        Assertions.assertEquals("PHONE_SWITCHED_OFF", episodeTags.getCurrentTags().get(0));
    }

    @Test
    public void saveTest() {
        when(episodeTagRepository.saveAll(any())).thenReturn(getEpisodeTags());
        List<EpisodeTagDataRequest> episodeTagRequest = Arrays.asList(
            new EpisodeTagDataRequest("PHONE_UNREACHABLE", LocalDateTime.now(), 12345L, true)
        );
        List<EpisodeTagResponse> episodeTagResponses = episodeTagService.save(episodeTagRequest);
        for (int i = 0; i < episodeTagResponses.get(0).getTags().size(); i++) {
            Assertions.assertEquals(episodeTagResponses.get(0).getTags().get(i).getTagName(), getEpisodeTags().get(i).getTagName());
        }
        Assertions.assertEquals("PHONE_SWITCHED_OFF", episodeTagResponses.get(0).getCurrentTags().get(0));
    }

    @Test
    public void search_TagsFilterNull_Test() {
        SearchTagsRequest searchTagsRequest = new SearchTagsRequest(
            Arrays.asList(1L, 2L, 3L), null
        );
        episodeTagService.search(searchTagsRequest);
        Mockito.verify(episodeTagRepository, Mockito.times(1)).getAllByEpisodeIdIn(any());
    }

    @Test
    public void deleteTagsBulk() {
        List<String> tagList = Arrays.asList(
            "A", "B", "C"
        );
        List<String> tagDate = Arrays.asList(
            "12-12-2021 18:30:00", "12-12-2021 18:30:00", "12-12-2021 18:30:00"
        );
        EpisodeTagBulkDeletionRequest episodeTagBulkDeletionRequest = new EpisodeTagBulkDeletionRequest(Collections.singletonList(1L), tagList, tagDate, false);
        episodeTagService.deleteBulk(episodeTagBulkDeletionRequest);
        List<LocalDateTime> localDates = tagDate.stream().map(Utils::toLocalDateTimeWithNull).collect(Collectors.toList());
        Mockito.verify(episodeTagRepository, Mockito.times(1))
                .deleteByEpisodeIdIsInAndTagNameIsInAndTagDateIsIn(eq(Collections.singletonList(1L)), eq(tagList), eq(localDates));
    }

    @Test
    public void deleteTagsBulkStickyEmptyTagList() {
        EpisodeTagBulkDeletionRequest episodeTagBulkDeletionRequest = new EpisodeTagBulkDeletionRequest(Collections.singletonList(1L), null, null, true);
        episodeTagService.deleteBulk(episodeTagBulkDeletionRequest);
        Mockito.verify(episodeTagRepository, Mockito.times(1))
                .deleteByEpisodeIdIsInAndStickyIsTrue(eq(Collections.singletonList(1L)));
    }

    @Test
    public void deleteTagsBulkStickyTagList() {
        List<String> tagList = Arrays.asList(
                "A", "B", "C"
        );
        EpisodeTagBulkDeletionRequest episodeTagBulkDeletionRequest = new EpisodeTagBulkDeletionRequest(Collections.singletonList(1L), tagList, null, true);
        episodeTagService.deleteBulk(episodeTagBulkDeletionRequest);
        Mockito.verify(episodeTagRepository, Mockito.times(1))
                .deleteByEpisodeIdIsInAndTagNameIsInAndStickyIsTrue(eq(Collections.singletonList(1L)), eq(tagList));
    }

    @Test
    public void searchTest() {
        SearchTagsRequest searchTagsRequest = new SearchTagsRequest(
            Arrays.asList(1L, 2L, 3L), Arrays.asList("TAG", "TAG2")
        );
        episodeTagService.search(searchTagsRequest);
        Mockito.verify(episodeTagRepository, Mockito.times(1)).getTagsWithFilters(eq(Arrays.asList(1L, 2L, 3L)), eq(Arrays.asList("TAG", "TAG2")));
    }

    @Test
    public void groupTagsTest() {
        List<EpisodeTag> tagList = new ArrayList<>(getEpisodeTags());
        //add more to check function correctness
        tagList.add(new EpisodeTag(1L, 44444L, "PHONE_UNREACHABLE", LocalDateTime.now(), LocalDateTime.now(), false));
        tagList.add(new EpisodeTag(1L, 55555L, "PHONE_UNREACHABLE", LocalDateTime.now(), LocalDateTime.now(), false));
        List<EpisodeTagResponse> response = episodeTagService.groupTags(tagList);
        Assertions.assertEquals(response.size(), 3);
    }

    @Test
    public void getEpisodeConfigTest() {
        Map<String, EpisodeTagStore> episodeTagStoreMap = new HashMap<>();
        episodeTagStoreMap.put("A", getStore().get(0));
        when(episodeTagStoreCreation.getEpisodeTagStoreMap()).thenReturn(episodeTagStoreMap);
        List<TagsConfigResponse> config = episodeTagService.getEpisodeConfig();
        Assertions.assertEquals(config.get(0).getTagName(), getStore().get(0).getTagName());
        Assertions.assertEquals(config.get(0).getTagDescription(), getStore().get(0).getTagDescription());
        Assertions.assertEquals(config.get(0).getTagDisplayName(), getStore().get(0).getTagDisplayName());
    }

    @Test
    public void deleteTagsForSwitchCase() {
        String endDate = "10-10-2021 18:30:00";
        episodeTagService.deleteAllTagsAfterEndDate(7L, endDate);
        Mockito.verify(episodeTagRepository, Mockito.times(1))
            .deleteAllByEpisodeIdEqualsAndTagDateAfter(eq(7L), eq(Utils.toLocalDateTimeWithNull(endDate)));
    }

    @Test
    public void deleteTags() {
        List<String> tagList = Arrays.asList(
                "A", "B", "C"
        );
        List<String> tagDate = Arrays.asList(
                "12-12-2021 18:30:00", "12-12-2021 18:30:00", "12-12-2021 18:30:00"
        );
        EpisodeTagDeletionRequest episodeTagDeletionRequest = new EpisodeTagDeletionRequest(1L, tagList, tagDate);
        episodeTagService.delete(episodeTagDeletionRequest);
        List<LocalDateTime> localDates = tagDate.stream().map(Utils::toLocalDateTimeWithNull).collect(Collectors.toList());
        Mockito.verify(episodeTagRepository, Mockito.times(1))
                .deleteTags(eq(1L), eq(tagList), eq(localDates));
    }


    List<EpisodeTagStore> getStore() {
        List<EpisodeTagStore> config = Arrays.asList(
            new EpisodeTagStore(1L, "Invalid_Phone_Number", "displayName", "icon", "desc", "Phone_issues")
        );
        return config;
    }

    List<EpisodeTag> getEpisodeTags() {
        long episodeId = 12345;
        return Arrays.asList(
            new EpisodeTag(1L, episodeId, "PHONE_UNREACHABLE", LocalDateTime.now(), LocalDateTime.now(), false),
            new EpisodeTag(1L, episodeId, "PHONE_SWITCHED_OFF", LocalDateTime.now(), LocalDateTime.now(), true)
        );
    }

}

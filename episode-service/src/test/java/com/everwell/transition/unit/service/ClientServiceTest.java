package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.exceptions.NotFoundException;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.request.RegisterClientRequest;
import com.everwell.transition.model.response.ClientResponse;
import com.everwell.transition.repositories.ClientRepository;
import com.everwell.transition.service.impl.ClientServiceImpl;
import com.everwell.transition.utils.JwtUtils;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ClientServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    private ClientServiceImpl clientService;

    @Mock
    private ClientRepository clientRepository;

    @Spy
    JwtUtils jwtUtils;

    @Test(expected = ValidationException.class)
    public void testGetClientWithoutClientId() {
        clientService.getClient(null);
    }

    @Test(expected = NotFoundException.class)
    public void testGetClientNotFoundClient() {
        Long id = 1L;

        when(clientRepository.findById(id)).thenReturn(Optional.empty());

        clientService.getClient(id);
    }

    @Test
    public void testGetClientSuccess() {
        Long id = 1L;
        String text = "Test";
        Client client = new Client(text, text);

        when(clientRepository.findById(id)).thenReturn(Optional.of((client)));

        clientService.getClient(id);
    }

    @Test
    public void testGetClientWithToken() {
        Long id = 1L;
        String text = "abcdefhijklmnopqrstuvwxyzabcdefhijklmnopqrstuvwxyzabcdefhijklmnopqrstuvwxyzabcdefhijklmnopqrstuvwxyzabcdefhijklmnopqrstuvwxyz";
        Client client = new Client(text, text);

        jwtUtils.setSecretKey("sercret");
        when(clientRepository.findById(id)).thenReturn(Optional.of((client)));

        ClientResponse clientResponse = clientService.getClientWithToken(id);
        assertEquals(client.getName(), clientResponse.getName());
        verify(clientRepository, Mockito.times(1)).findById(any());
    }

    @Test(expected = ValidationException.class)
    public void testRegisterClientExistingClient() {
        String text = "Test";
        Client client = new Client(text, text);
        RegisterClientRequest clientRequest = new RegisterClientRequest(text, text);

        when(clientRepository.findByName(any())).thenReturn(client);

        clientService.registerClient(clientRequest);
    }

    @Test
    public void testRegisterClientSuccess() {
        String text = "Test";
        RegisterClientRequest clientRequest = new RegisterClientRequest(text, text);

        when(clientRepository.findByName(any())).thenReturn(null);
        when(clientRepository.save(any())).thenReturn(null);

        ClientResponse clientResponse = clientService.registerClient(clientRequest);
        assertEquals(clientRequest.getName(), clientResponse.getName());
        verify(clientRepository, Mockito.times(1)).findByName(any());
        verify(clientRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void testGetClientByToken() {
        String text = "Test";
        Client client = new Client(text, text);
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(authentication.getPrincipal()).thenReturn(client);
        SecurityContextHolder.setContext(securityContext);

        Client clientResponse = clientService.getClientByTokenOrDefault();
        Assert.assertEquals(clientResponse.getName(), text);
    }

}
package com.everwell.transition.unit.controller;

import com.everwell.transition.BaseTest;
import com.everwell.transition.controller.EpisodeAppConfigController;
import com.everwell.transition.exceptions.CustomExceptionHandler;
import com.everwell.transition.model.db.EpisodeAppConfig;
import com.everwell.transition.model.response.EpisodeAppConfigResponse;
import com.everwell.transition.service.EpisodeAppConfigService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class EpisodeAppConfigControllerTest extends BaseTest {

    private MockMvc mockMvc;

    @InjectMocks
    private EpisodeAppConfigController episodeAppConfigController;

    @Mock
    EpisodeAppConfigService episodeAppConfigService;

    @Before
    public void setup() throws Exception{
        mockMvc = MockMvcBuilders.standaloneSetup(episodeAppConfigController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void getConfigTest() throws Exception{
        String technology = "99DOTS";
        String deployment = "IND";
        Long episodeId = 1L;
        String uri = "/v1/patient-app/config?episodeId="+episodeId+"&technology="+ technology +"&deployment=" + deployment ;
        when(episodeAppConfigService.getEpisodeAppConfig(episodeId,technology,deployment)).thenReturn(getAppConfig());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.config.technology").value(technology))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.config.deployment").value(deployment))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.config.allowDoseMarking").value(true))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.config.validationRequired").value(false));
    }

    @Test
    public void getConfigTest_emptyTechnology() throws Exception{
        String technology = "";
        String deployment = "IND";
        Long episodeId = 1L;
        String uri = "/v1/patient-app/config?episodeId="+episodeId+"&technology="+ technology +"&deployment=" + deployment ;
        when(episodeAppConfigService.getEpisodeAppConfig(episodeId, "DEFAULT", "DEFAULT")).thenReturn(getDefaultConfig());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.config.technology").value("DEFAULT"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.config.deployment").value("DEFAULT"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.config.allowDoseMarking").value(true))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.config.validationRequired").value(false));
    }

    @Test
    public void getConfigTest_emptyDeployment() throws Exception{
        String technology = "99DOOTS";
        String deployment = "";
        Long episodeId = 1L;
        String uri = "/v1/patient-app/config?episodeId="+episodeId+"&technology="+ technology +"&deployment=" + deployment ;
        when(episodeAppConfigService.getEpisodeAppConfig(episodeId,"DEFAULT", "DEFAULT")).thenReturn(getDefaultConfig());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.config.technology").value("DEFAULT"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.config.deployment").value("DEFAULT"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.config.allowDoseMarking").value(true))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.config.validationRequired").value(false));
    }

    EpisodeAppConfigResponse getDefaultConfig() {
        return  new EpisodeAppConfigResponse(0L,new EpisodeAppConfig(3L,"DEFAULT",true,false,true,28L,"DEFAULT"));
    }

    EpisodeAppConfigResponse getAppConfig() {
        return  new EpisodeAppConfigResponse(0L,new EpisodeAppConfig(3L,"99DOTS",true,false,true,28L,"IND"));
    }
}

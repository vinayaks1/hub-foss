package com.everwell.transition.unit.service;

import com.everwell.transition.BaseTest;
import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.enums.diagnostics.DiagnosticsEndpoints;
import com.everwell.transition.enums.diagnostics.TestTypeEnum;
import com.everwell.transition.exceptions.NotFoundException;
import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.db.Episode;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.dto.diagnostics.TypeRequest;
import com.everwell.transition.model.request.diagnostics.ResultRequestV2;
import com.everwell.transition.model.request.diagnostics.TestRequestV2;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.response.diagnostics.AddTestResponseV2;
import com.everwell.transition.constants.diagnostics.ResponseConstants;
import com.everwell.transition.constants.DiagnosticsConstants;
import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.elasticsearch.indices.EpisodeIndex;
import com.everwell.transition.enums.diagnostics.DiagnosticsEndpoint;
import com.everwell.transition.exceptions.NotFoundException;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.enums.diagnostics.DiagnosticsEndpoints;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.enums.user.UserServiceField;
import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.request.diagnostics.GetSampleIdListRequest;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.response.diagnostics.DiagnosticsFilterDataResponse;
import com.everwell.transition.model.response.diagnostics.EpisodeTestsResponse;
import com.everwell.transition.model.response.diagnostics.ResultSampleResponse;
import com.everwell.transition.model.response.diagnostics.TestResponse;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.EpisodeService;
import com.everwell.transition.service.impl.DiagnosticsServiceImpl;
import com.everwell.transition.service.impl.EpisodeServiceImpl;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import java.util.*;

import static org.mockito.ArgumentMatchers.*;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

public class DiagnosticsServiceTest extends BaseTest {
    @Spy
    @InjectMocks
    DiagnosticsServiceImpl diagnosticsHelperService;

    @Mock
    EpisodeServiceImpl episodeService;

    @Mock
    ClientService clientService;


    @Before
    public void init() {
        ReflectionTestUtils.setField(diagnosticsHelperService, "dataGatewayURL", "http://test:8080");
        ReflectionTestUtils.setField(diagnosticsHelperService, "username", "test");
        ReflectionTestUtils.setField(diagnosticsHelperService, "password", "test");
        when(clientService.getClientByTokenOrDefault()).thenReturn(new Client(29L, "test", "test", null));
        diagnosticsHelperService.init();
    }

    @Test
    public void testTestConnection() {
        String successfulResponse = "Successful Response.";
        ResponseEntity<Response<String>> response
                = new ResponseEntity<>(
                new Response<>(successfulResponse, true),
                HttpStatus.OK
        );

        doReturn(response).when(diagnosticsHelperService)
                .getExchange(ArgumentMatchers.anyString(), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any());

        ResponseEntity<Response<String>> responseEntity = diagnosticsHelperService.testConnection();
        Assert.assertEquals(successfulResponse, responseEntity.getBody().getData());
    }

    @Test
    public void testGetTestsByEpisodeId() {
        Map<String, Object> stageData = new HashMap<>();
        stageData.put(FieldConstants.PERSON_FIELD_NAME, "test");
        stageData.put(UserServiceField.AGE.getFieldName(), 12);
        stageData.put(UserServiceField.PRIMARY_PHONE.getFieldName(), "123456");
        stageData.put(UserServiceField.GENDER.getFieldName(), "Male");
        EpisodeIndex episodeIndex = new EpisodeIndex();
        episodeIndex.setStageData(stageData);
        episodeIndex.setId("123");
        episodeIndex.setStartDate("2022-10-08 18:30:00");

        Map<String, Object> testDataV2 = new HashMap<>();
        testDataV2.put("testRequestId", 49604L);
        List<Map<String, Object>> testDataV2List = Collections.singletonList(testDataV2);
        ResponseEntity<Response<List<Map<String , Object>>>> diagnosticsResponse = new ResponseEntity<>(new Response<>(testDataV2List), HttpStatus.OK);

        when(episodeService.getEpisodeDetails(eq(123L), eq(29L), eq(false), eq(false), eq(false), eq(false))).thenReturn(episodeIndex.convertToDto());
        doReturn(diagnosticsResponse).when(diagnosticsHelperService).getExchange(eq(DiagnosticsEndpoint.GET_TESTS_BY_ENTITY.getEndpointUrl()), any(), any(), any());

        EpisodeTestsResponse episodeTestsResponse = diagnosticsHelperService.getTestsByEpisode(123L);
        Assert.assertEquals(1, episodeTestsResponse.getTests().size());
        Assert.assertEquals(12L, (long) episodeTestsResponse.getAge());
    }

    @Test
    public void testGetTestsByEpisodeIdNullAge() {
        Map<String, Object> stageData = new HashMap<>();
        stageData.put(FieldConstants.PERSON_FIELD_NAME, "test");
        stageData.put(UserServiceField.GENDER.getFieldName(), "Male");
        EpisodeIndex episodeIndex = new EpisodeIndex();
        episodeIndex.setStageData(stageData);
        episodeIndex.setId("123");
        episodeIndex.setStartDate("2022-10-08 18:30:00");

        Map<String, Object> testDataV2 = new HashMap<>();
        testDataV2.put("testRequestId", 49604L);
        List<Map<String, Object>> testDataV2List = Collections.singletonList(testDataV2);
        ResponseEntity<Response<List<Map<String , Object>>>> diagnosticsResponse = new ResponseEntity<>(new Response<>(testDataV2List), HttpStatus.OK);

        when(episodeService.getEpisodeDetails(eq(123L), eq(29L), eq(false), eq(false), eq(false), eq(false))).thenReturn(episodeIndex.convertToDto());
        doReturn(diagnosticsResponse).when(diagnosticsHelperService).getExchange(eq(DiagnosticsEndpoint.GET_TESTS_BY_ENTITY.getEndpointUrl()), any(), any(), any());

        EpisodeTestsResponse episodeTestsResponse = diagnosticsHelperService.getTestsByEpisode(123L);
        Assert.assertEquals(1, episodeTestsResponse.getTests().size());
        Assert.assertNull(episodeTestsResponse.getAge());
        Assert.assertNull(episodeTestsResponse.getPrimaryPhone());
    }

    @Test(expected = NotFoundException.class)
    public void testGetTestsByEpisodeIdNoEpisode() {
        when(episodeService.getEpisodeDetails(eq(123L), eq(29L), eq(false), eq(false), eq(false), eq(false))).thenThrow(NotFoundException.class);

        diagnosticsHelperService.getTestsByEpisode(123L);
    }

    @Test
    public void testGetSampleIdList() {
        EpisodeIndex episodeIndex = new EpisodeIndex();
        episodeIndex.setStageName("DIAGNOSED_NOT_ON_TREATMENT");
        episodeIndex.setId("123");
        episodeIndex.setStartDate("2022-10-08 18:30:00");
        GetSampleIdListRequest sampleIdListRequest = new GetSampleIdListRequest();
        sampleIdListRequest.setEpisodeId(123L);
        sampleIdListRequest.setStagesToExclude(Collections.singletonList("DIAGNOSED_OUTCOME_ASSIGNED"));
        ResponseEntity<Response<List<Long>>> diagnosticsResponse = new ResponseEntity<>(new Response<>(Collections.singletonList(123L)), HttpStatus.OK);

        when(episodeService.getEpisodeDetails(eq(123L), eq(29L), eq(false), eq(false), eq(false), eq(false))).thenReturn(episodeIndex.convertToDto());
        doReturn(diagnosticsResponse).when(diagnosticsHelperService).getExchange(eq(DiagnosticsEndpoint.GET_SAMPLE_ID_LIST_BY_ENTITY.getEndpointUrl()), any(), any(), any());

        List<Long> response = diagnosticsHelperService.getSampleIdListByEpisodeId(sampleIdListRequest);
        Assert.assertEquals(1, response.size());
        Assert.assertEquals(123L, (long) response.get(0));
    }

    @Test
    public void testFilterKPI() {
        String input = "1";
        Map<String, Object> kpiFilterRequest = new HashMap<>();
        kpiFilterRequest.put("testRequestIds", input);
        Map<String, Object> kpiResponse = new HashMap<>();
        ResponseEntity<Response<Map<String, Object>>> response
                = new ResponseEntity<>(new Response<>(true, kpiResponse), HttpStatus.OK);

        doReturn(response).when(diagnosticsHelperService)
                .postExchange(eq(DiagnosticsEndpoints.KPI.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any());

        ResponseEntity<Response<Map<String, Object>>> responseEntity = diagnosticsHelperService.filterKPI(kpiFilterRequest);
        Assert.assertEquals(response.getStatusCode(), responseEntity.getStatusCode());
    }

    @Test
    public void testQRCodeValidation() {
        String testQRCode = "A123";
        Map<String, Object> responseMap = new HashMap<>();
        ResponseEntity<Response<Map<String, Object>>> response
                = new ResponseEntity<>(
                new Response<>(true, responseMap),
                HttpStatus.OK
        );

        doReturn(response).when(diagnosticsHelperService)
                .getExchange(eq(DiagnosticsEndpoints.QR_VALIDATION.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any());

        ResponseEntity<Response<Map<String, Object>>> responseEntity = diagnosticsHelperService.qrCodeValidation(testQRCode);
        Assert.assertEquals(response.getStatusCode(), responseEntity.getStatusCode());
    }

    @Test
    public void addTest() {
        Long clientId = 1L;
        TestRequestV2 testRequest = new TestRequestV2();
        testRequest.setEntityId("1");
        testRequest.setClientId(1L);
        TypeRequest typeRequest = new TypeRequest() {
            @Override
            public List<String> getFinalInterpretationList() {
                return null;
            }

            @Override
            public ResultRequestV2 getResult() {
                return new ResultRequestV2();
            }
        };
        typeRequest.hasSampleSupport();
        typeRequest.getResult().setTestResultId(1L);
        typeRequest.getResult().setDateTested("");
        typeRequest.getResult().setRemarks("None");
        typeRequest.getResult().setDateReported("");
        typeRequest.getResult().setFinalInterpretation("Positive");
        typeRequest.getResult().setLabSerialNumber("1");
        testRequest.setTypeRequestMap(new HashMap<TestTypeEnum, TypeRequest>() {
        });
        testRequest.setSubReason("");
        testRequest.setTestRequestId(1L);
        testRequest.setSubReason("");
        testRequest.setType("CBNAAT");
        testRequest.setPreviousATT("None");
        testRequest.setStagesToExclude(new ArrayList<>());
        Map<String, Object> stageData = new HashMap<>();
        stageData.put(FieldConstants.EPISODE_FIELD_STAGE_KEY, "PRESUMPTIVE_CLOSED");
        stageData.put(FieldConstants.IS_EPISODE_ENABLED, true);
        EpisodeIndex episodeIndex = new EpisodeIndex();
        episodeIndex.setId("123");
        episodeIndex.setStartDate("2021-10-10 18:30:00");
        episodeIndex.setStageData(stageData);
        Map<String, Object> test = new HashMap<>();
        test.put(DiagnosticsConstants.FIELD_EPISODE_ID, 1L);
        Episode episode = new Episode();
        when(episodeService.processEpisodeRequestInputForStageTransition(any(), any())).thenReturn(episode);
        ResponseEntity<Response<Map<String, Object>>> diagnosticsResponse = new ResponseEntity<>(new Response<>(test), HttpStatus.OK);
        testRequest.setTypeRequestMap(new HashMap<TestTypeEnum, TypeRequest>() {
        });
        EpisodeDto testEpisodeDto = new EpisodeDto();
        testEpisodeDto.setStageKey("PRESUMPTIVE_CLOSED");
        when(episodeService.getEpisodeDetails(eq(1L), eq(1L), eq(false), eq(false), eq(false), eq(false))).thenReturn(testEpisodeDto);
        ResultSampleResponse resultSampleResponse = new ResultSampleResponse();
        resultSampleResponse.setTestResultId(2L);
        resultSampleResponse.setSampleIdList(new ArrayList<>());
        AddTestResponseV2 testResponse = new AddTestResponseV2();
        testResponse.setStatus(DiagnosticsConstants.RESULT_AVAILABLE);
        testResponse.setTestRequestId(1L);
        testResponse.setReason("");
        Map<String,ResultSampleResponse> mp = new HashMap<>();
        mp.put("Test",resultSampleResponse);
        testResponse.setTypeMap(mp);
        ResponseEntity<Response<AddTestResponseV2>> response
                = new ResponseEntity<>(new Response<>(true, testResponse), HttpStatus.OK);

        doReturn(diagnosticsResponse).when(diagnosticsHelperService).getExchange(eq(DiagnosticsEndpoint.GET_TEST_BY_ID.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any());
        doReturn(response).when(diagnosticsHelperService)
                .postExchange(eq(DiagnosticsEndpoints.ADD_TEST.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any());

        ResponseEntity<Response<AddTestResponseV2>> responseEntity = diagnosticsHelperService.addTest(testRequest, clientId);
        Assert.assertEquals(response.getStatusCode(), responseEntity.getStatusCode());
    }

    @Test(expected = ValidationException.class)
    public void addTestStageFailure() {
        Long clientId = 1L;
        TestRequestV2 testRequest = new TestRequestV2();
        testRequest.setEntityId("1");
        testRequest.setClientId(1L);
        testRequest.setStagesToExclude(Collections.singletonList("PRESUMPTIVE_CLOSED"));
        testRequest.setTypeRequestMap(new HashMap<TestTypeEnum, TypeRequest>() {
        });
        EpisodeDto testEpisodeDto = new EpisodeDto();
        testEpisodeDto.setStageKey("PRESUMPTIVE_CLOSED");
        when(episodeService.getEpisodeDetails(eq(1L), eq(1L), eq(false), eq(false), eq(false), eq(false))).thenReturn(testEpisodeDto);
        diagnosticsHelperService.addTest(testRequest, clientId);
    }
    
    @Test
    public void testGenerateQrPdf() {
        Map<String, Object> request = new HashMap<>();
        Map<String, Object> response = new HashMap<>();
        ResponseEntity<Response<Map<String, Object>>> diagnosticsResponse
                = new ResponseEntity<>(new Response<>(true, response), HttpStatus.OK);

        doReturn(diagnosticsResponse).when(diagnosticsHelperService)
                .postExchange(eq(DiagnosticsEndpoints.GENERATE_QR_PDF.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any());

        ResponseEntity<Response<Map<String, Object>>> responseEntity = diagnosticsHelperService.generateQrPdf(request);
        Assert.assertEquals(diagnosticsResponse.getStatusCode(), responseEntity.getStatusCode());
    }
    
    @Test
    public void getSampleById() {
        String by = "sampleId";
        Long sampleId = 1L;
        String qrCode = null;
        Boolean journeyDetails = false;
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put(ResponseConstants.ENTITY_ID, 1L);
        EpisodeDto episodeDto = new EpisodeDto();
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("name", "Test Name");
        episodeDto.setStageData(stageData);
        ResponseEntity<Response<Map<String, Object>>> response
                = new ResponseEntity<>(
                new Response<>(true, responseMap),
                HttpStatus.OK
        );

        doReturn(response).when(diagnosticsHelperService)
                .getExchange(eq(DiagnosticsEndpoints.SAMPLE_BY_ID_OR_QRCODE.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any());

        doReturn(episodeDto).when(episodeService)
                .getEpisodeDetails(ArgumentMatchers.any(), ArgumentMatchers.any());
        
        ResponseEntity<Response<Map<String, Object>>> responseEntity = diagnosticsHelperService.getSampleByIdOrQRCode(by, sampleId, qrCode, journeyDetails);
        Assert.assertEquals(response.getStatusCode(), responseEntity.getStatusCode());
    }
    @Test
    public void getSampleByQRCode() {
        String by = "qrCode";
        Long sampleId = null;
        String qrCode = "testQR";
        Boolean journeyDetails = false;
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put(ResponseConstants.ENTITY_ID, 1L);
        EpisodeDto episodeDto = new EpisodeDto();
        Map<String, Object> stageData = new HashMap<>();
        stageData.put("name", "Test Name");
        episodeDto.setStageData(stageData);
        ResponseEntity<Response<Map<String, Object>>> response
                = new ResponseEntity<>(
                new Response<>(true, responseMap),
                HttpStatus.OK
        );

        doReturn(response).when(diagnosticsHelperService)
                .getExchange(eq(DiagnosticsEndpoints.SAMPLE_BY_ID_OR_QRCODE.getEndpointUrl()), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any());

        doReturn(episodeDto).when(episodeService)
                .getEpisodeDetails(ArgumentMatchers.any(), ArgumentMatchers.any());

        ResponseEntity<Response<Map<String, Object>>> responseEntity = diagnosticsHelperService.getSampleByIdOrQRCode(by, sampleId, qrCode, journeyDetails);
        Assert.assertEquals(response.getStatusCode(), responseEntity.getStatusCode());
    }

    @Test
    public void testGetTestById() {
        Map<String, Object> stageData = new HashMap<>();
        stageData.put(FieldConstants.EPISODE_FIELD_STAGE_NAME, "PRESUMPTIVE_CLOSED");
        EpisodeIndex episodeIndex = new EpisodeIndex();
        episodeIndex.setId("123");
        episodeIndex.setStartDate("2021-10-10 18:30:00");
        episodeIndex.setStageData(stageData);
        Map<String, Object> test = new HashMap<>();
        test.put(DiagnosticsConstants.FIELD_EPISODE_ID, 123L);
        ResponseEntity<Response<Map<String, Object>>> diagnosticsResponse = new ResponseEntity<>(new Response<>(test), HttpStatus.OK);
        Long testRequestId = 12345L;

        when(episodeService.getEpisodeDetails(eq(123L), eq(29L), eq(false), eq(false), eq(false), eq(false))).thenReturn(episodeIndex.convertToDto());
        doReturn(diagnosticsResponse).when(diagnosticsHelperService).getExchange(eq(DiagnosticsEndpoint.GET_TEST_BY_ID.getEndpointUrl()), any(), any(), any());

        TestResponse testResponse = diagnosticsHelperService.getTestById(testRequestId);
        Assert.assertEquals(123L, testResponse.getTest().get(DiagnosticsConstants.FIELD_EPISODE_ID));
    }

    @Test
    public void editTestSuccessResultsNotAvailable() {
        Map<String, Object> requestInput = new HashMap<>();
        requestInput.put(DiagnosticsConstants.FIELD_EPISODE_ID, 123);
        requestInput.put(DiagnosticsConstants.FIELD_NON_EDITABLE_STAGES, Collections.singletonList("PRESUMPTIVE_CLOSED"));

        Map<String, Object> stageData = new HashMap<>();
        stageData.put(FieldConstants.EPISODE_FIELD_STAGE_NAME, "DIAGNOSED_ON_TREATMENT");
        EpisodeIndex episodeIndex = new EpisodeIndex();
        episodeIndex.setId("123");
        episodeIndex.setStartDate("2021-10-10 18:30:00");
        episodeIndex.setStageData(stageData);
        Map<String, Object> response = new HashMap<>();
        response.put(DiagnosticsConstants.FIELD_TEST_REQUEST_ID, 123L);
        ResponseEntity<Response<Map<String, Object>>> diagnosticsResponse = new ResponseEntity<>(new Response<>(response), HttpStatus.OK);

        when(episodeService.getEpisodeDetails(eq(123L), eq(29L), eq(false), eq(false), eq(false), eq(false))).thenReturn(episodeIndex.convertToDto());
        doReturn(diagnosticsResponse).when(diagnosticsHelperService).putExchange(eq(DiagnosticsEndpoint.EDIT_TEST.getEndpointUrl()), any(), any());
        Episode episode = new Episode();
        when(episodeService.processEpisodeRequestInputForStageTransition(any(), any())).thenReturn(episode);

        Map<String, Object> actualResponse = diagnosticsHelperService.editTest(requestInput);
        Assert.assertEquals(actualResponse.get(DiagnosticsConstants.FIELD_TEST_REQUEST_ID), 123L);
    }

    @Test
    public void editTestSuccessResultsAvailable() {
        Map<String, Object> requestInput = new HashMap<>();
        requestInput.put(DiagnosticsConstants.FIELD_EPISODE_ID, 123);
        requestInput.put(DiagnosticsConstants.FIELD_NON_EDITABLE_STAGES, Collections.singletonList("PRESUMPTIVE_CLOSED"));

        Map<String, Object> stageData = new HashMap<>();
        stageData.put(FieldConstants.EPISODE_FIELD_STAGE_NAME, "DIAGNOSED_NOT_ON_TREATMENT");
        stageData.put(FieldConstants.IS_EPISODE_ENABLED, true);
        EpisodeIndex episodeIndex = new EpisodeIndex();
        episodeIndex.setId("123");
        episodeIndex.setStartDate("2021-10-10 18:30:00");
        episodeIndex.setStageData(stageData);
        Map<String, Object> response = new HashMap<>();
        response.put(DiagnosticsConstants.FIELD_TEST_REQUEST_ID, 123L);
        response.put(DiagnosticsConstants.RESULT_AVAILABLE, true);
        ResponseEntity<Response<Map<String, Object>>> diagnosticsResponse = new ResponseEntity<>(new Response<>(response), HttpStatus.OK);
        Client client = new Client(29L, "test", "test", null);

        when(clientService.getClientByTokenOrDefault()).thenReturn(client);
        when(episodeService.getEpisodeDetails(eq(123L), eq(29L), eq(false), eq(false), eq(false), eq(false))).thenReturn(episodeIndex.convertToDto());
        doReturn(diagnosticsResponse).when(diagnosticsHelperService).putExchange(eq(DiagnosticsEndpoint.EDIT_TEST.getEndpointUrl()), any(), any());
        Episode episode = new Episode();
        when(episodeService.processEpisodeRequestInputForStageTransition(any(), any())).thenReturn(episode);

        Map<String, Object> actualResponse = diagnosticsHelperService.editTest(requestInput);
        Assert.assertEquals(actualResponse.get(DiagnosticsConstants.FIELD_TEST_REQUEST_ID), 123L);
    }

    @Test
    public void editTestSuccessNoStageValidation() {
        Map<String, Object> requestInput = new HashMap<>();
        requestInput.put(DiagnosticsConstants.FIELD_EPISODE_ID, 123);

        Map<String, Object> stageData = new HashMap<>();
        stageData.put(FieldConstants.EPISODE_FIELD_STAGE_NAME, "DIAGNOSED_ON_TREATMENT");
        EpisodeIndex episodeIndex = new EpisodeIndex();
        episodeIndex.setId("123");
        episodeIndex.setStartDate("2021-10-10 18:30:00");
        episodeIndex.setStageData(stageData);
        Map<String, Object> response = new HashMap<>();
        response.put(DiagnosticsConstants.FIELD_TEST_REQUEST_ID, 123L);
        ResponseEntity<Response<Map<String, Object>>> diagnosticsResponse = new ResponseEntity<>(new Response<>(response), HttpStatus.OK);

        when(episodeService.getEpisodeDetails(eq(123L), eq(29L), eq(false), eq(false), eq(false), eq(false))).thenReturn(episodeIndex.convertToDto());
        doReturn(diagnosticsResponse).when(diagnosticsHelperService).putExchange(eq(DiagnosticsEndpoint.EDIT_TEST.getEndpointUrl()), any(), any());

        Map<String, Object> actualResponse = diagnosticsHelperService.editTest(requestInput);
        Assert.assertEquals(actualResponse.get(DiagnosticsConstants.FIELD_TEST_REQUEST_ID), 123L);
    }

    @Test(expected = ValidationException.class)
    public void editTestNoEpisodeId() {
        Map<String, Object> requestInput = new HashMap<>();
        diagnosticsHelperService.editTest(requestInput);
    }

    @Test(expected = ValidationException.class)
    public void editTestInvalidStage() {
        Map<String, Object> requestInput = new HashMap<>();
        requestInput.put(DiagnosticsConstants.FIELD_EPISODE_ID, 123);
        requestInput.put(DiagnosticsConstants.FIELD_NON_EDITABLE_STAGES, Collections.singletonList("PRESUMPTIVE_CLOSED"));

        Map<String, Object> stageData = new HashMap<>();
        stageData.put(FieldConstants.EPISODE_FIELD_STAGE_KEY, "PRESUMPTIVE_CLOSED");
        EpisodeIndex episodeIndex = new EpisodeIndex();
        episodeIndex.setId("123");
        episodeIndex.setStartDate("2021-10-10 18:30:00");
        episodeIndex.setStageData(stageData);


        when(episodeService.getEpisodeDetails(eq(123L), eq(29L), eq(false), eq(false), eq(false), eq(false))).thenReturn(episodeIndex.convertToDto());

        diagnosticsHelperService.editTest(requestInput);
    }

    @Test
    public void deleteTest() {
        ResponseEntity<Response<String>> diagnosticsResponse = new ResponseEntity<>(new Response<>(true, "Deleted successfully"), HttpStatus.OK);

        doReturn(diagnosticsResponse).when(diagnosticsHelperService).deleteExchange(eq(DiagnosticsEndpoint.DELETE_TEST.getEndpointUrl()), any(), any(), any(), any());

        String actualResponse = diagnosticsHelperService.deleteTest(123L);
        Assert.assertEquals(actualResponse, "Deleted successfully");
    }
    
    @Test
    public void episodeWithTbTest() {
        DiagnosticsFilterDataResponse diagnosticsFilterDataResponse = new DiagnosticsFilterDataResponse(Arrays.asList(new DiagnosticsFilterDataResponse.FilterResponse(1L, "{\"hcpVisits\": \"33\", \"diagnosisOfTB\""), new DiagnosticsFilterDataResponse.FilterResponse(2L, "{\"hcpVisits\": \"33\", \"testedForTb\"")));
        ResponseEntity<Response<DiagnosticsFilterDataResponse>> diagnosticsResponse = new ResponseEntity<>(new Response<>(diagnosticsFilterDataResponse), HttpStatus.OK);

        doReturn(diagnosticsResponse).when(diagnosticsHelperService).postExchange(eq(DiagnosticsEndpoints.FILTER.getEndpointUrl()), any(), any());
        List<Long> episodes = diagnosticsHelperService.episodesWithTbTest(Arrays.asList(1L, 2L));
        Assert.assertEquals(2L , (long) episodes.get(0));
    }

    @Test(expected = ValidationException.class)
    public void episodeWithTbTestFailure() {
        DiagnosticsFilterDataResponse diagnosticsFilterDataResponse = new DiagnosticsFilterDataResponse(Arrays.asList(new DiagnosticsFilterDataResponse.FilterResponse(1L, "{\"hcpVisits\": \"33\", \"diagnosisOfTB\""), new DiagnosticsFilterDataResponse.FilterResponse(2L, "{\"hcpVisits\": \"33\", \"testedForTb\"")));
        ResponseEntity<Response<DiagnosticsFilterDataResponse>> diagnosticsResponse = new ResponseEntity<>(new Response<>(diagnosticsFilterDataResponse), HttpStatus.OK);

        doReturn(null).when(diagnosticsHelperService).postExchange(eq(DiagnosticsEndpoints.FILTER.getEndpointUrl()), any(), any());
        diagnosticsHelperService.episodesWithTbTest(Arrays.asList(1L, 2L));
    }

    Map<String, Object> getInputMap_correct() throws IOException {
        Map<String, Object> map = new HashMap<>();
        String jsonData = "{\"eventName\":\"patient_enrollment\",\"field\":{\"Id\":10712725,\"UserId\":123223,\"Deleted\":false,\"AddedTimestamp\":\"2021-02-03T20:36:02.9471744\",\"FirstName\":\"Testing\",\"LastName\":\"Test\",\"Gender\":\"Female\",\"Language\":null,\"Age\":12,\"Occupation\":\"Unknown\",\"Address\":\"test\",\"PrimaryPhone\":\"1413241234\",\"PreARTNumber\":null,\"ARTNumber\":null,\"EnrollmentDate\":null,\"EndDate\":null,\"HierarchyMapping_Initiation\":null,\"HierarchyMapping_Residence\":324,\"TypeOfPatient\":\"IndiaTbPublic\",\"CurrentSchedule\":null,\"CurrentTags\":\"New_Enrollment\",\"TreatmentOutcome\":null,\"AttentionRequired\":null,\"AdherenceStatus\":null,\"LastDosage\":null,\"WeightBand\":null,\"NewOrPreviouslyTreated\":null,\"SiteOfDisease\":null,\"EPSite\":null,\"TypeOfCase\":null,\"HIVTestStatus\":\"Unknown\",\"NikshayId\":null,\"TBNumber\":null,\"TBCategory\":null,\"MermId\":null,\"DrugRegimen\":null,\"TBTreatmentStartDate\":null,\"AddedBy\":7945,\"DeploymentCode\":\"IND\",\"AdherenceString\":null,\"RegistrationDate\":\"2021-02-03T20:36:02.9315483\",\"FathersName\":null,\"Pincode\":\"123412\",\"Taluka\":null,\"Town\":null,\"Ward\":null,\"Landmark\":null,\"Stage\":\"PRESUMPTIVE_OPEN\",\"DiagnosisDate\":null,\"DiagnosisBasis\":null,\"MonitoringMethod\":null,\"TreatmentType\":null,\"DrugSusceptibility\":null,\"RefillMonitoring\":null,\"Area\":\"Unknown\",\"MaritalStatus\":\"Unknown\",\"SocioeconomicStatus\":\"Unknown\",\"KeyPopulation\":\"NotApplicable\",\"ContactPersonName\":null,\"ContactPersonAddress\":null,\"ContactPersonPhone\":null,\"MonthsOfTreatment\":null,\"MonthsSinceEpisode\":null,\"TreatmentSource\":null,\"Height\":null,\"CPInitiationDate\":null,\"RegimenType\":null,\"DrugResistance\":null,\"HierarchyMapping_Enrollment\":285624,\"HierarchyMapping_Diagnosed\":null,\"HierarchyMapping_Current\":285624,\"HierarchyMapping_ART\":null,\"HierarchyMapping_DRTB\":null,\"RemarksOutcome\":null,\"MedicalOfficerOutcome\":null,\"NotificationTestId\":null,\"BeneficiaryId\":45036,\"MigrationStatus\":null,\"Latitude\":null,\"Longitude\":null,\"HierarchyMapping_PVTHUB\":null,\"ParentEpisodeId\":null,\"IsDuplicate\":false,\"DuplicateStatus\":null,\"DuplicateOf\":null,\"IsIAMEnabled\":false,\"HierarchyMapping_Outcome\":null,\"LastActivityDate\":\"2021-02-03T20:36:02.9471744\",\"TransferId\":null,\"ForegoBenefits\":false}}";
        return Utils.jsonToObject(jsonData, new TypeReference<Map<String, Object>>() {});
    }
}

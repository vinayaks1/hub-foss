
DO
$do$
    DEClARE
        _client_id_list int[] := array[171];
        _client_id int;
        _disease_template_name_list varchar[] := array['Generic Disease', 'TB', 'Hypertension (High Blood pressure)', 'HIV', 'Diabetes' ];
        _disease_template_id int;
        _presumptive_open_stage_id int;
        _presumptive_closed_stage_id int;
        _on_treatment_stage_id int;
        _basic_details_tab_id varchar;
        _adherence_tab_id varchar;
        _disease_stage_id int;
    BEGIN
        select id into _presumptive_open_stage_id from stages where stage_name = 'PRESUMPTIVE_OPEN';
        select id into _presumptive_closed_stage_id from stages where stage_name = 'PRESUMPTIVE_CLOSED';
 	   	select id into _on_treatment_stage_id from stages where stage_name = 'ON_TREATMENT';
        select CAST(id as varchar) into _basic_details_tab_id  from supported_tab where type = 'basic_details' or type = 'Basic Details';
        select CAST(id as varchar) into _adherence_tab_id from supported_tab where type = 'Adherence';
        foreach _client_id  in array _client_id_list loop
              FOR i IN 1 .. array_upper(_disease_template_name_list, 1) loop
               SELECT id INTO _disease_template_id FROM disease_template WHERE disease_name = _disease_template_name_list[i] AND client_id = _client_id;
                    SELECT id INTO _disease_stage_id FROM disease_stage_mapping WHERE stage_id = _presumptive_open_stage_id AND disease_template_id = _disease_template_id;
                    IF NOT EXISTS(SELECT id from tab_permission where disease_stage_mapping_id = _disease_stage_id and tab_id = _basic_details_tab_id) THEN
                        INSERT INTO tab_permission (id, disease_stage_mapping_id, tab_id, add, edit, view, delete)
                        VALUES ((select max(id)+1 from tab_permission), _disease_stage_id, _basic_details_tab_id, false, true, true, false);
                    END IF;
                    SELECT id INTO _disease_stage_id FROM disease_stage_mapping WHERE stage_id = _presumptive_closed_stage_id AND disease_template_id = _disease_template_id;
                    IF NOT EXISTS(SELECT id from tab_permission where disease_stage_mapping_id = _disease_stage_id and tab_id = _basic_details_tab_id) THEN
                        INSERT INTO tab_permission (id, disease_stage_mapping_id, tab_id, add, edit, view, delete)
                        VALUES ((select max(id)+1 from tab_permission), _disease_stage_id, _basic_details_tab_id, false, true, true, false);
                    END IF;
                    SELECT id INTO _disease_stage_id FROM disease_stage_mapping WHERE stage_id = _on_treatment_stage_id AND disease_template_id = _disease_template_id;
                    IF NOT EXISTS(SELECT id from tab_permission where disease_stage_mapping_id = _disease_stage_id and tab_id = _basic_details_tab_id) THEN
                        INSERT INTO tab_permission (id, disease_stage_mapping_id, tab_id, add, edit, view, delete)
                        VALUES ((select max(id)+1 from tab_permission), _disease_stage_id, _basic_details_tab_id, false, true, true, false);
                    END IF;
                    IF NOT EXISTS(SELECT id from tab_permission where disease_stage_mapping_id = _disease_stage_id and tab_id = _adherence_tab_id) THEN
                        INSERT INTO tab_permission (id, disease_stage_mapping_id, tab_id, add, edit, view, delete)
                        VALUES ((select max(id)+1 from tab_permission), _disease_stage_id, _adherence_tab_id, true, true, true, false);
                    END IF;
              END LOOP ;
        end loop ;
    END

$do$;
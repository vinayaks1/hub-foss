UPDATE trigger
SET function_name = 'processDoseReminderPNDtos',
event_name = 'processDoseReminderPNDtos'
where function_name = (select function_name from trigger where function_name = 'getPatientsWhoHaveNotMarkedAdherenceForToday');
--ToDo Update the trigger and template id once INS MR is merged and deployed


INSERT INTO public.trigger(client_id, cron_time, default_template_id, event_name, function_name, notification_type, template_ids, trigger_id, vendor_id, time_zone)
	VALUES (171, '30 03 * * *', 19, 'getMorningDoseReminderPNDtosForClientIdAndSchedule', 'getMorningDoseReminderPNDtosForClientIdAndSchedule', 'Push Notification', '19', 16, 20, 'Asia/Kolkata');

INSERT INTO public.trigger(client_id, cron_time, default_template_id, event_name, function_name, notification_type, template_ids, trigger_id, vendor_id, time_zone)
	VALUES (171, '30 07 * * *', 20, 'getAfternoonDoseReminderPNDtosForClientIdAndSchedule', 'getAfternoonDoseReminderPNDtosForClientIdAndSchedule', 'Push Notification', '20', 17, 20, 'Asia/Kolkata');

INSERT INTO public.trigger(client_id, cron_time, default_template_id, event_name, function_name, notification_type, template_ids, trigger_id, vendor_id, time_zone)
	VALUES (171, '30 11 * * *', 21, 'getEveningDoseReminderPNDtosForClientIdAndSchedule', 'getEveningDoseReminderPNDtosForClientIdAndSchedule', 'Push Notification', '21', 18, 20, 'Asia/Kolkata');

INSERT INTO public.trigger(client_id, cron_time, default_template_id, event_name, function_name, notification_type, template_ids, trigger_id, vendor_id, time_zone)
	VALUES (171, '30 15 * * *', 22, 'getNightDoseReminderPNDtosForClientIdAndSchedule', 'getNightDoseReminderPNDtosForClientIdAndSchedule', 'Push Notification', '22', 19, 20, 'Asia/Kolkata');

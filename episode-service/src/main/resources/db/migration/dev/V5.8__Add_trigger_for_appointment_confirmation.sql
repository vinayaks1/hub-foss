INSERT INTO public.trigger(client_id, default_template_id, event_name, function_name, notification_type, template_ids, trigger_id, vendor_id)
	VALUES ((select id from client where name = 'HHCPlus'), 26, 'appointmentConfirmationPN', 'getAppointmentConfirmationPNDto', 'Push Notification', '26', 23, 21);

SELECT setval('trigger_id_seq', (SELECT max(id) FROM trigger));

INSERT INTO public.trigger(client_id, default_template_id, event_name, function_name, notification_type, template_ids, trigger_id, vendor_id)
	VALUES ((select id from client where name = 'HHCPlus'), 203, 'appointmentConfirmationSMS', 'sendAppointmentConfirmationSms', 'SMS', '203', 61, 3);

SELECT setval('trigger_id_seq', (SELECT max(id) FROM trigger));
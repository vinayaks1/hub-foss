create table if not exists episode_tag_store
(
    id               bigserial
    constraint episode_tag_store_pkey
    primary key,
    tag_description  varchar(255),
    tag_display_name varchar(255),
    tag_icon         varchar(255),
    tag_name         varchar(255),
    tag_group varchar(255)
);

INSERT INTO public.episode_tag_store (id, tag_description, tag_display_name, tag_icon, tag_name, tag_group) VALUES (1, null, 'Phone inaccessible', null, 'Phone_inaccessible', 'Phone_issues');
INSERT INTO public.episode_tag_store (id, tag_description, tag_display_name, tag_icon, tag_name, tag_group) VALUES (2, null, 'No mobile coverage', 'signal', 'No_mobile_coverage', 'Phone_issues');
INSERT INTO public.episode_tag_store (id, tag_description, tag_display_name, tag_icon, tag_name, tag_group) VALUES (3, null, 'Phone not answered', null, 'Phone_not_answered', 'Phone_issues');
INSERT INTO public.episode_tag_store (id, tag_description, tag_display_name, tag_icon, tag_name, tag_group) VALUES (4, null, 'Wrong number', null, 'Wrong_number', 'Phone_issues');
INSERT INTO public.episode_tag_store (id, tag_description, tag_display_name, tag_icon, tag_name, tag_group) VALUES (5, null, 'Tollfree lines not working', 'signal', 'Tollfree_lines_not_working', 'Phone_issues');
INSERT INTO public.episode_tag_store (id, tag_description, tag_display_name, tag_icon, tag_name, tag_group) VALUES (6, null, 'Invalid phone number', null, 'Invalid_Phone_Number', 'Phone_issues');
INSERT INTO public.episode_tag_store (id, tag_description, tag_display_name, tag_icon, tag_name, tag_group) VALUES (7, null, 'Changed pharmacy', null, 'Changed_pharmacy', 'Supply_chain_issues');
INSERT INTO public.episode_tag_store (id, tag_description, tag_display_name, tag_icon, tag_name, tag_group) VALUES (8, null, 'No medications', null, 'No_medications', 'Supply_chain_issues');
INSERT INTO public.episode_tag_store (id, tag_description, tag_display_name, tag_icon, tag_name, tag_group) VALUES (9, null, 'No envelopes', null, 'No_envelopes', 'Supply_chain_issues');
INSERT INTO public.episode_tag_store (id, tag_description, tag_display_name, tag_icon, tag_name, tag_group) VALUES (10, null, 'Treatment stopped on medical advice', null, 'Treatment_stopped_on_medical_advice', 'Health_issues');
INSERT INTO public.episode_tag_store (id, tag_description, tag_display_name, tag_icon, tag_name, tag_group) VALUES (11, null, 'Hospitalized', null, 'Hospitalized', 'Health_issues');
INSERT INTO public.episode_tag_store (id, tag_description, tag_display_name, tag_icon, tag_name, tag_group) VALUES (12, null, 'New prescription', null, 'New_prescription', 'Health_issues');
INSERT INTO public.episode_tag_store (id, tag_description, tag_display_name, tag_icon, tag_name, tag_group) VALUES (13, null, 'Adverse reaction', null, 'Adverse_reaction', 'Health_issues');
INSERT INTO public.episode_tag_store (id, tag_description, tag_display_name, tag_icon, tag_name, tag_group) VALUES (14, null, 'Registration through Screening Tool', null, 'Registration_through_Screening_Tool', 'Externally_Enrolled');
INSERT INTO public.episode_tag_store (id, tag_description, tag_display_name, tag_icon, tag_name, tag_group) VALUES (15, null, 'No visit recorded', null, 'No_visit_recorded', 'Registration_through_Screening_Tool');
INSERT INTO public.episode_tag_store (id, tag_description, tag_display_name, tag_icon, tag_name, tag_group) VALUES (16, null, 'Follow up done by STS', null, 'Follow_up_done_by_STS', 'Registration_through_Screening_Tool');
INSERT INTO public.episode_tag_store (id, tag_description, tag_display_name, tag_icon, tag_name, tag_group) VALUES (17, null, 'Follow up done by DTO', null, 'Follow_up_done_by_DTO', 'Registration_through_Screening_Tool');
INSERT INTO public.episode_tag_store (id, tag_description, tag_display_name, tag_icon, tag_name, tag_group) VALUES (18, null, 'Follow up done by call centre', null, 'Follow_up_done_by_call_centre', 'Registration_through_Screening_Tool');
INSERT INTO public.episode_tag_store (id, tag_description, tag_display_name, tag_icon, tag_name, tag_group) VALUES (19, null, 'End date passed', null, 'End_Date_Passed', 'Automatic_Tags');
INSERT INTO public.episode_tag_store (id, tag_description, tag_display_name, tag_icon, tag_name, tag_group) VALUES (20, null, 'New Enrollment', null, 'New_Enrollment', 'Automatic_Tags');
create table if not exists trigger
(
    id                  bigserial not null,
    client_id           bigint,
    hierarchy_id        bigint,
    trigger_id          bigint,
    default_template_id bigint,
    template_ids        text,
    event_name          text,
    function_name       text,
    cron_time           text,
    notification_type   text,
    vendor_id           bigint,
    primary key (id)
);
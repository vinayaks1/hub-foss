DO
$do$
    declare
         disease_stage_ids int[] := ARRAY(SELECT id FROM disease_stage_mapping
                                                 where stage_id in (SELECT id FROM stages where stage_name in ('PRESUMPTIVE_OPEN_PUBLIC', 'PRESUMPTIVE_OPEN_PRIVATE', 'PRESUMPTIVE_CLOSED', 'DIAGNOSED_NOT_ON_TREATMENT', 'DIAGNOSED_ON_TREATMENT_PRIVATE', 'DIAGNOSED_ON_TREATMENT_PUBLIC', 'DIAGNOSED_OUTCOME_ASSIGNED', 'DIAGNOSED_NOT_ON_TREATMENT_CLOSED' ))
                                                 and disease_template_id in (select id from disease_template dt where disease_name in ('Latent Tuberculosis Infection', 'Drug Resistant Tuberculosis', 'Drug Sensitive Tuberculosis')));

                var int;
    BEGIN
        foreach var in array disease_stage_ids loop
        IF NOT EXISTS (SELECT * from disease_stage_key_mapping where disease_stage_id = var and field_id = (select id from field where key = 'householdVisits'and module = 'Episode' limit 1) ) THEN
          insert into disease_stage_key_mapping (id, default_value, deleted, disease_stage_id, required, field_id) VALUES ((select max(id) + 1 from disease_stage_key_mapping), null, false, var, false, (select id from field where key = 'householdVisits'and module = 'Episode' limit 1));
        end if;

        IF NOT EXISTS (SELECT * from disease_stage_key_mapping where disease_stage_id = var and field_id = (select id from field where key = 'callCenterCalls'and module = 'Episode' limit 1) ) THEN
          insert into disease_stage_key_mapping (id, default_value, deleted, disease_stage_id, required, field_id) VALUES ((select max(id) + 1 from disease_stage_key_mapping), null, false, var, false, (select id from field where key = 'callCenterCalls'and module = 'Episode' limit 1));
        end if;

        IF NOT EXISTS (SELECT * from disease_stage_key_mapping where disease_stage_id = var and field_id = (select id from field where key = 'engagementExists'and module = 'Episode' limit 1) ) THEN
                  insert into disease_stage_key_mapping (id, default_value, deleted, disease_stage_id, required, field_id) VALUES ((select max(id) + 1 from disease_stage_key_mapping), null, false, var, false, (select id from field where key = 'engagementExists'and module = 'Episode' limit 1));
                end if;

    end loop ;
end;
$do$;

create table if not exists episode_log_store
(
    id bigserial not null
    constraint episode_log_store_pkey
    primary key,
    category text,
    category_group text
);

INSERT into public.episode_log_store (id, category, category_group) VALUES (1, 'Patient_Added', 'Add/delete etc.');
INSERT into public.episode_log_store (id, category, category_group) VALUES (2, 'Patient_Deleted', 'Add/delete etc.');
INSERT into public.episode_log_store (id, category, category_group) VALUES (3, 'Case_Closed', 'Add/delete etc.');
INSERT into public.episode_log_store (id, category, category_group) VALUES (4, 'Case_Reopened', 'Add/delete etc.');
INSERT into public.episode_log_store (id, category, category_group) VALUES (5, 'Patient_Enrolled', 'Add/delete etc.');
INSERT into public.episode_log_store (id, category, category_group) VALUES (6, 'Patient_Archived', 'Add/delete etc.');

INSERT into public.episode_log_store (id, category, category_group) VALUES (7, 'Manual_Doses_Added', 'Dosage level changes');
INSERT into public.episode_log_store (id, category, category_group) VALUES (8, 'Manual_Doses_Removed', 'Dosage level changes');
INSERT into public.episode_log_store (id, category, category_group) VALUES (9, 'Missed_Doses_Added', 'Dosage level changes');
INSERT into public.episode_log_store (id, category, category_group) VALUES (10, 'Missed_Doses_Removed', 'Dosage level changes');
INSERT into public.episode_log_store (id, category, category_group) VALUES (11, 'Tags_Added_For_Past_Doses', 'Dosage level changes');
INSERT into public.episode_log_store (id, category, category_group) VALUES (12, 'Tags_Removed_For_Past_Doses', 'Dosage level changes');
INSERT into public.episode_log_store (id, category, category_group) VALUES (13, 'Patient_Missed_Doses_Added', 'Dosage level changes');
INSERT into public.episode_log_store (id, category, category_group) VALUES (14, 'Patient_Missed_Doses_Removed', 'Dosage level changes');
INSERT into public.episode_log_store (id, category, category_group) VALUES (15, 'Patient_Manual_Doses_Added', 'Dosage level changes');
INSERT into public.episode_log_store (id, category, category_group) VALUES (16, 'Patient_Manual_Doses_Removed', 'Dosage level changes');

INSERT into public.episode_log_store (id, category, category_group) VALUES (17, 'Attention_Required_Updated', 'General patient management');
INSERT into public.episode_log_store (id, category, category_group) VALUES (18, 'TreatmentCenter_Changed', 'General patient management');
INSERT into public.episode_log_store (id, category, category_group) VALUES (19, 'ARTCenter_Changed', 'General patient management');
INSERT into public.episode_log_store (id, category, category_group) VALUES (20, 'DRTBCenter_Changed', 'General patient management');
INSERT into public.episode_log_store (id, category, category_group) VALUES (21, 'PrivateCenter_Changed', 'General patient management');
INSERT into public.episode_log_store (id, category, category_group) VALUES (22, 'TreatmentStage_Changed', 'General patient management');

INSERT into public.episode_log_store (id, category, category_group) VALUES (23, 'Regular_Note_and_Current_tags', 'Actual notes');

INSERT into public.episode_log_store (id, category, category_group) VALUES (24, 'Current_Tags_Removed_By_Administrator', 'Automated changes');
INSERT into public.episode_log_store (id, category, category_group) VALUES (25, 'Attention_Required_Updated_By_Administrator', 'Automated changes');

INSERT into public.episode_log_store (id, category, category_group) VALUES (26, 'VOT_Pin', 'Adherence changes');
INSERT into public.episode_log_store (id, category, category_group) VALUES (27, 'Monitoring_Method_Changed', 'Adherence changes');

INSERT into public.episode_log_store (id, category, category_group) VALUES (28, 'MERM_Allocated', 'MERM Related Actions');
INSERT into public.episode_log_store (id, category, category_group) VALUES (29, 'MERM_Deallocated', 'MERM Related Actions');

INSERT into public.episode_log_store (id, category, category_group) VALUES (30, 'Duplicate_Status_Updated', 'Deduplication');

INSERT into public.episode_log_store (id, category, category_group) VALUES (31, 'Beneficiary_Duplicate_Accepted', 'DBT');
INSERT into public.episode_log_store (id, category, category_group) VALUES (32, 'Beneficiary_Duplicate_Rejected', 'DBT');
INSERT into public.episode_log_store (id, category, category_group) VALUES (33, 'Beneficiary_Details_Change_Request', 'DBT');
INSERT into public.episode_log_store (id, category, category_group) VALUES (34, 'Beneficiary_Details_Changed', 'DBT');
INSERT into public.episode_log_store (id, category, category_group) VALUES (35, 'Beneficiary_Details_Change_Cancelled', 'DBT');
INSERT into public.episode_log_store (id, category, category_group) VALUES (36, 'Beneficiary_BankAccount_BlackListed', 'DBT');

INSERT into public.episode_log_store (id, category, category_group) VALUES (37, 'EndDate_Updated', 'Treatment Details(End Date)');

INSERT into public.episode_log_store (id, category, category_group) VALUES (38, 'TribalSupport_Benefit_Removed', 'Benefit');

INSERT into public.episode_log_store (id, category, category_group) VALUES (39, 'Patient_Engagement_Changed', 'Engagement');
INSERT into public.episode_log_store (id, category, category_group) VALUES (40, 'Patient_Engagement_Added', 'Engagement');

INSERT into public.episode_log_store (id, category, category_group) VALUES (41, 'Tag_Removed', 'Symptom Checker');

INSERT into public.episode_log_store (id, category, category_group) VALUES (42, 'Details_Updated', 'General patient management');
INSERT into public.episode_log_store (id, category, category_group) VALUES (43, 'Providers_Updated', 'General patient management');
INSERT into public.episode_log_store (id, category, category_group) VALUES (44, 'Medical_Details_Updated', 'General patient management');

INSERT into public.episode_log_store (id, category, category_group) VALUES (45, 'Duplicate_Beneficiary_Migration', 'DBT');

INSERT into public.episode_log_store (id, category, category_group) VALUES (46, 'Support_Action_Call', 'Support Actions');
INSERT into public.episode_log_store (id, category, category_group) VALUES (47, 'Support_Action_Home_Visit', 'Support Actions');
INSERT into public.episode_log_store (id, category, category_group) VALUES (48, 'Support_Action_Facility_Visit', 'Support Actions');

INSERT into public.episode_log_store (id, category, category_group) VALUES (49, 'Vot_User_Created', 'VOT Related Actions');

INSERT into public.episode_log_store (id, category, category_group) VALUES (50, 'Patient_Engagement_Updated', 'Engagement');
INSERT into public.episode_log_store (id, category, category_group) VALUES (51, 'Schedule_Day_Changed', 'Scheduling Actions');
INSERT into public.episode_log_store (id, category, category_group) VALUES (52, 'Comorbidity_Updated', 'Comorbidity Changes');




DO
$do$
BEGIN
If NOT EXISTS (select * from field where key = 'treatmentPlanId') THEN
INSERT INTO field ( key, module, entity) VALUES ( 'treatmentPlanId', 'Episode', null);
end if;

INSERT INTO disease_stage_key_mapping (disease_stage_id, required, deleted, default_value, field_id)
VALUES
       (    (
           Select Id from disease_stage_mapping
           where disease_template_id = (Select Id from disease_template where disease_name = 'TB')),
        false,
        false,
        null,
        (Select Id from field
           where key = 'treatmentPlanId'));

INSERT INTO disease_stage_key_mapping (disease_stage_id, required, deleted, default_value, field_id)
VALUES
       (    (
           Select Id from disease_stage_mapping
           where disease_template_id = (Select Id from disease_template where disease_name = 'Hypertension')),
        false,
        false,
        null,
        (Select Id from field
           where key = 'treatmentPlanId'));

INSERT INTO disease_stage_key_mapping (disease_stage_id, required, deleted, default_value, field_id)
VALUES
       (    (
           Select Id from disease_stage_mapping
           where disease_template_id = (Select Id from disease_template where disease_name = 'HIV')),
        false,
        false,
        null,
        (Select Id from field
           where key = 'treatmentPlanId'));

INSERT INTO disease_stage_key_mapping (disease_stage_id, required, deleted, default_value, field_id)
VALUES
       (    (
           Select Id from disease_stage_mapping
           where disease_template_id = (Select Id from disease_template where disease_name = 'Diabetes')),
        false,
        false,
        null,
        (Select Id from field
           where key = 'treatmentPlanId'));
end;
$do$
DO
$do$
BEGIN

    IF NOT EXISTS (SELECT * from field where key = 'verifications' and module = 'Person') THEN
      insert into field (id, key, module) values((select max(id)+1 from field), 'verifications', 'Person');
    end if;

    IF NOT EXISTS (SELECT * from field where key = 'externalIds' and module = 'Person') THEN
      insert into field (id, key, module) values((select max(id)+1 from field), 'externalIds', 'Person');
    end if;

end;
$do$
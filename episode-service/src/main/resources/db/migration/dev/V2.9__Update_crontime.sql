UPDATE trigger
SET cron_time = '30 16 * * *'
WHERE function_name = (select function_name from trigger where function_name = 'getPatientsWhoHaveNotMarkedAdherenceForToday');
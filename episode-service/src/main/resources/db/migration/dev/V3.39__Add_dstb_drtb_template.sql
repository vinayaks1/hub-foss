DO $$

DECLARE
	v_d2c_client_id INTEGER;
	v_new_disease_id INTEGER;
	_disease_template_name_list varchar[] := array['DSTB', 'DRTB'];
	v_new_disease_ids INTEGER [];
	old_disease_ids INTEGER[];
	v_new_stage_ids INTEGER[];
	v_new_DS_mapping_ids INTEGER[];
	v_new_field_ids INTEGER[];
	v_required_existing_field_ids_both_stages INTEGER[];
	v_existing_field_ids_both_stages INTEGER[];
	v_required_existing_field_ids_stage_1 INTEGER[];
	v_existing_field_ids_stage_1 INTEGER[];
	v_required_existing_field_ids_stage_2 INTEGER[];
	v_existing_field_ids_stage_2 INTEGER[];
	DS_id INTEGER;
	new_field_id INTEGER;
	existing_field_id INTEGER;
	on_treatement_stage INTEGER;
	off_treatement_stage INTEGER;
	v_new_DS_mapping_id_1 INTEGER;
	v_new_DS_mapping_id_2 INTEGER;
	tb_status INTEGER;
	viralLoad INTEGER;
	deletionReason INTEGER;
	deletionNote INTEGER;
	supported_tab_list varchar [] := array['Adherence', 'Basic Details', 'Health Facilities', 'Staff/Treatment Supporters', 'Tags and Notes', 'Comorbidity', 'Adherence Logs', 'Support action', 'Engagement', 'Outcomes', 'Delete Patient']

BEGIN
    FOR i in 1.. array_upper(supported_tab_list, 1)
        LOOP
            IF NOT EXISTS (SELECT id from supported_tab where type = supported_tab_list[i]) THEN
                INSERT INTO supported_tab(id, type)
                VALUES ((select max(id)+1 from supported_tab), supported_tab_list[i]);
        END LOOP;
	FOR i in 1 .. array_upper(_disease_template_name_list, 1)
		LOOP
			SELECT id INTO v_d2c_client_id FROM client WHERE  id=63;
			IF v_d2c_client_id > 0 THEN
				SELECT id INTO v_new_disease_id FROM disease_template where disease_name=_disease_template_name_list[i] and client_id=63;
				IF v_new_disease_id  is NULL THEN
			-- New diseases
					WITH new_diseases AS (
						INSERT INTO disease_template (id, disease_name, client_id)
							VALUES
								((select max(id)+1 from disease_template), _disease_template_name_list[i], v_d2c_client_id)
							RETURNING id)
						SELECT id INTO v_new_disease_id FROM new_diseases;
				END IF;
				SELECT id into on_treatement_stage FROM stages where stage_name = 'ON_TREATMENT' and stage_display_name ='Treatment';
				IF on_treatement_stage  is NULL THEN
					WITH new_stage_1 AS (
						INSERT INTO stages (id, stage_name, stage_display_name) VALUES
						((select max(id)+1 from stages), 'ON_TREATMENT', 'Treatment')
						RETURNING id)
					SELECT id INTO on_treatement_stage from new_stage_1;
				END IF;
				SELECT id into off_treatement_stage FROM stages where stage_name = 'OFF_TREATMENT' and stage_display_name ='Off Treatment';
				IF off_treatement_stage  is NULL THEN
					WITH new_stage_2 AS (
						INSERT INTO stages (id, stage_name, stage_display_name) VALUES
						((select max(id)+1 from stages), 'OFF_TREATMENT', 'Off Treatment')
						RETURNING id)
					SELECT id INTO off_treatement_stage from new_stage_2;
				END IF;

			-- Add disease_stage_mapping with newly created diseases and stage
				SELECT id into v_new_DS_mapping_id_1 from disease_stage_mapping where disease_template_id = v_new_disease_id and stage_id = on_treatement_stage;
				IF v_new_DS_mapping_id_1  is NULL THEN
					WITH new_DS_mapping_1 AS (
						INSERT INTO disease_stage_mapping (id, disease_template_id, stage_id)
						VALUES
							((select max(id)+1 from disease_stage_mapping), v_new_disease_id, on_treatement_stage)
						RETURNING id)
					SELECT id INTO v_new_DS_mapping_id_1 FROM new_DS_mapping_1;
				END IF;

				SELECT id into v_new_DS_mapping_id_2 from disease_stage_mapping where disease_template_id = v_new_disease_id and stage_id = off_treatement_stage;
				IF v_new_DS_mapping_id_2  is NULL  THEN
					WITH new_DS_mapping_2 AS (
						INSERT INTO disease_stage_mapping (id, disease_template_id, stage_id)
						VALUES
							((select max(id)+1 from disease_stage_mapping), v_new_disease_id, off_treatement_stage)
						RETURNING id)
					SELECT id INTO v_new_DS_mapping_id_2 FROM new_DS_mapping_2;
				END IF;

			-- New field TB Status

				SELECT id INTO tb_status FROM field where key = 'tbStatus';
				raise notice 'tb_status: %', tb_status;

				SELECT id INTO viralLoad FROM field where key = 'viralLoad';
				raise notice 'viralLoad: %', viralLoad;

				SELECT id INTO deletionReason FROM field where key = 'deletionReason';
				raise notice 'deletionReason: %', deletionReason;

				SELECT id INTO deletionNote FROM field where key = 'deletionNote';
				raise notice 'deletionNote: %', deletionNote;

			-- Add disease_stage_key_mapping for existing and newly created fields and disease_stage_mappings
				-- Add viralLoad
				IF NOT EXISTS(SELECT id from disease_stage_key_mapping where disease_stage_id=v_new_DS_mapping_id_1 and field_id=viralLoad) THEN
					INSERT INTO disease_stage_key_mapping (id,disease_stage_id, required, deleted, default_value, field_id)
					VALUES ((select max(id)+1 from disease_stage_key_mapping), v_new_DS_mapping_id_1, false, false, null, viralLoad);
				END IF;


				SELECT array_agg(id) INTO v_existing_field_ids_both_stages FROM field where key in ('remarksOutcome', 'tbStatus', 'deletionNote');

				SELECT array_agg(id) INTO v_required_existing_field_ids_both_stages FROM field where key in ('firstName', 'lastName', 'address','dateOfBirth','gender','treatmentStartTimeStamp','NumberOfDaysOfMedication','mobileList','treatmentOutcome', 'deletionReason');


				FOREACH existing_field_id IN ARRAY v_existing_field_ids_both_stages
					LOOP
						IF NOT EXISTS(SELECT id from disease_stage_key_mapping where disease_stage_id=v_new_DS_mapping_id_1 and field_id=existing_field_id) THEN
							INSERT INTO disease_stage_key_mapping (id, disease_stage_id, required, deleted, default_value, field_id)
							VALUES ((select max(id)+1 from disease_stage_key_mapping),v_new_DS_mapping_id_1, false, false, null, existing_field_id);
						END IF;
						IF NOT EXISTS(SELECT id from disease_stage_key_mapping where disease_stage_id=v_new_DS_mapping_id_2 and field_id=existing_field_id) THEN
							INSERT INTO disease_stage_key_mapping (id, disease_stage_id, required, deleted, default_value, field_id)
							VALUES ((select max(id)+1 from disease_stage_key_mapping), v_new_DS_mapping_id_2, false, false, null, existing_field_id);
						END IF;
					END LOOP;


			FOREACH existing_field_id IN ARRAY v_required_existing_field_ids_both_stages
				LOOP
					IF NOT EXISTS(SELECT id from disease_stage_key_mapping where disease_stage_id=v_new_DS_mapping_id_1 and field_id=existing_field_id) THEN
						INSERT INTO disease_stage_key_mapping (id, disease_stage_id, required, deleted, default_value, field_id)
						VALUES ((select max(id)+1 from disease_stage_key_mapping), v_new_DS_mapping_id_1, true, false, null, existing_field_id);
					END IF;
					IF NOT EXISTS(SELECT id from disease_stage_key_mapping where disease_stage_id=v_new_DS_mapping_id_2 and field_id=existing_field_id) THEN
						INSERT INTO disease_stage_key_mapping (id, disease_stage_id, required, deleted, default_value, field_id)
						VALUES ((select max(id)+1 from disease_stage_key_mapping), v_new_DS_mapping_id_2, true, false, null, existing_field_id);
					END IF;
				END LOOP;

				-- module tabs config
				-- Adherence
				IF NOT EXISTS (SELECT id from tab_permission where disease_stage_mapping_id = v_new_DS_mapping_id_1 and CAST(tab_id as INTEGER) = (select id from supported_tab where type='Adherence')) THEN
					INSERT INTO tab_permission (id, disease_stage_mapping_id ,tab_id, add, edit, view, delete)
					VALUES((select max(id) + 1 from tab_permission), v_new_DS_mapping_id_1, (select id from supported_tab where type='Adherence'), true, true,true, true);
				END IF;
				IF NOT EXISTS (SELECT id from tab_permission where disease_stage_mapping_id = v_new_DS_mapping_id_2 and CAST(tab_id as INTEGER) = (select id from supported_tab where type='Adherence')) THEN
					INSERT INTO tab_permission (id, disease_stage_mapping_id ,tab_id, add, edit, view, delete)
					VALUES((select max(id) + 1 from tab_permission), v_new_DS_mapping_id_2, (select id from supported_tab where type='Adherence'), true, true,true, true);
				END IF;
				-- Basic Details
				IF NOT EXISTS (SELECT id from tab_permission where disease_stage_mapping_id = v_new_DS_mapping_id_1 and CAST(tab_id as INTEGER) = (select id from supported_tab where type='Basic Details')) THEN
					INSERT INTO tab_permission (id, disease_stage_mapping_id ,tab_id, add, edit, view, delete)
					VALUES((select max(id) + 1 from tab_permission), v_new_DS_mapping_id_1, (select id from supported_tab where type='Basic Details'), true, true,true, true);
				END IF;
				IF NOT EXISTS (SELECT id from tab_permission where disease_stage_mapping_id = v_new_DS_mapping_id_2 and CAST(tab_id as INTEGER) = (select id from supported_tab where type='Basic Details')) THEN
					INSERT INTO tab_permission (id, disease_stage_mapping_id ,tab_id, add, edit, view, delete)
					VALUES((select max(id) + 1 from tab_permission), v_new_DS_mapping_id_2, (select id from supported_tab where type='Basic Details'), true, true,true, true);
				END IF;
				-- Treatment center
				IF NOT EXISTS (SELECT id from tab_permission where disease_stage_mapping_id = v_new_DS_mapping_id_1 and CAST(tab_id as INTEGER) = (select id from supported_tab where type='Health Facilities')) THEN
					INSERT INTO tab_permission (id, disease_stage_mapping_id ,tab_id, add, edit, view, delete)
					VALUES((select max(id) + 1 from tab_permission), v_new_DS_mapping_id_1, (select id from supported_tab where type='Health Facilities'), true, true,true, true);
				END IF;
				IF NOT EXISTS (SELECT id from tab_permission where disease_stage_mapping_id = v_new_DS_mapping_id_2 and CAST(tab_id as INTEGER) = (select id from supported_tab where type='Health Facilities')) THEN
					INSERT INTO tab_permission (id, disease_stage_mapping_id ,tab_id, add, edit, view, delete)
					VALUES((select max(id) + 1 from tab_permission), v_new_DS_mapping_id_2, (select id from supported_tab where type='Health Facilities'), true, true,true, true);
				END IF;
				-- staff
				IF NOT EXISTS (SELECT id from tab_permission where disease_stage_mapping_id = v_new_DS_mapping_id_1 and CAST(tab_id as INTEGER) = (select id from supported_tab where type='Staff/Treatment Supporters')) THEN
					INSERT INTO tab_permission (id, disease_stage_mapping_id ,tab_id, add, edit, view, delete)
					VALUES((select max(id) + 1 from tab_permission), v_new_DS_mapping_id_1, (select id from supported_tab where type='Staff/Treatment Supporters'), true, true,true, true);
				END IF;
				IF NOT EXISTS (SELECT id from tab_permission where disease_stage_mapping_id = v_new_DS_mapping_id_2 and CAST(tab_id as INTEGER) = (select id from supported_tab where type='Staff/Treatment Supporters')) THEN
					INSERT INTO tab_permission (id, disease_stage_mapping_id ,tab_id, add, edit, view, delete)
					VALUES((select max(id) + 1 from tab_permission), v_new_DS_mapping_id_2, (select id from supported_tab where type='Staff/Treatment Supporters'), true, true,true, true);
				END IF;
				-- tags and notes
				IF NOT EXISTS (SELECT id from tab_permission where disease_stage_mapping_id = v_new_DS_mapping_id_1 and CAST(tab_id as INTEGER) = (select id from supported_tab where type='Tags and Notes')) THEN
					INSERT INTO tab_permission (id, disease_stage_mapping_id ,tab_id, add, edit, view, delete)
					VALUES((select max(id) + 1 from tab_permission), v_new_DS_mapping_id_1, (select id from supported_tab where type='Tags and Notes'), true, true,true, true);
				END IF;
				IF NOT EXISTS (SELECT id from tab_permission where disease_stage_mapping_id = v_new_DS_mapping_id_2 and CAST(tab_id as INTEGER) = (select id from supported_tab where type='Tags and Notes')) THEN
					INSERT INTO tab_permission (id, disease_stage_mapping_id ,tab_id, add, edit, view, delete)
					VALUES((select max(id) + 1 from tab_permission), v_new_DS_mapping_id_2, (select id from supported_tab where type='Tags and Notes'), true, true,true, true);
				END IF;
				-- Comorbidity
				IF NOT EXISTS (SELECT id from tab_permission where disease_stage_mapping_id = v_new_DS_mapping_id_1 and CAST(tab_id as INTEGER) = (select id from supported_tab where type='Comorbidity')) THEN
					INSERT INTO tab_permission (id, disease_stage_mapping_id ,tab_id, add, edit, view, delete)
					VALUES((select max(id) + 1 from tab_permission), v_new_DS_mapping_id_1, (select id from supported_tab where type='Comorbidity'), true, true,true, true);
				END IF;
				IF NOT EXISTS (SELECT id from tab_permission where disease_stage_mapping_id = v_new_DS_mapping_id_2 and CAST(tab_id as INTEGER) = (select id from supported_tab where type='Comorbidity')) THEN
					INSERT INTO tab_permission (id, disease_stage_mapping_id ,tab_id, add, edit, view, delete)
					VALUES((select max(id) + 1 from tab_permission), v_new_DS_mapping_id_2, (select id from supported_tab where type='Comorbidity'), true, true,true, true);
				END IF;
				-- Adherence Logs
				IF NOT EXISTS (select id from supported_tab where type='Adherence Logs') THEN
					INSERT INTO supported_tab(id,type)
					VALUES((select max(id) +1 from supported_tab), 'Adherence Logs');
				END IF;
				IF NOT EXISTS (SELECT id from tab_permission where disease_stage_mapping_id = v_new_DS_mapping_id_1 and CAST(tab_id as INTEGER) = (select id from supported_tab where type='Adherence Logs')) THEN
					INSERT INTO tab_permission (id, disease_stage_mapping_id ,tab_id, add, edit, view, delete)
					VALUES((select max(id) + 1 from tab_permission), v_new_DS_mapping_id_1, (select id from supported_tab where type='Adherence Logs'), true, true,true, true);
				END IF;
				IF NOT EXISTS (SELECT id from tab_permission where disease_stage_mapping_id = v_new_DS_mapping_id_2 and CAST(tab_id as INTEGER) = (select id from supported_tab where type='Adherence Logs')) THEN
					INSERT INTO tab_permission (id, disease_stage_mapping_id ,tab_id, add, edit, view, delete)
					VALUES((select max(id) + 1 from tab_permission), v_new_DS_mapping_id_2, (select id from supported_tab where type='Adherence Logs'), true, true,true, true);
				END IF;
				-- Support action
				IF NOT EXISTS (select id from supported_tab where type='Support action') THEN
					INSERT INTO supported_tab(id, type)
					VALUES((select max(id) +1 from supported_tab),'Support action');
				END IF;
				IF NOT EXISTS (SELECT id from tab_permission where disease_stage_mapping_id = v_new_DS_mapping_id_1 and CAST(tab_id as INTEGER) = (select id from supported_tab where type='Support action')) THEN
					INSERT INTO tab_permission (id, disease_stage_mapping_id ,tab_id, add, edit, view, delete)
					VALUES((select max(id) + 1 from tab_permission), v_new_DS_mapping_id_1, (select id from supported_tab where type='Support action'), true, true,true, true);
				END IF;
				IF NOT EXISTS (SELECT id from tab_permission where disease_stage_mapping_id = v_new_DS_mapping_id_2 and CAST(tab_id as INTEGER) = (select id from supported_tab where type='Support action')) THEN
					INSERT INTO tab_permission (id, disease_stage_mapping_id ,tab_id, add, edit, view, delete)
					VALUES((select max(id) + 1 from tab_permission), v_new_DS_mapping_id_2, (select id from supported_tab where type='Support action'), true, true,true, true);
				END IF;
				-- Engagement
				IF NOT EXISTS (SELECT id from tab_permission where disease_stage_mapping_id = v_new_DS_mapping_id_1 and CAST(tab_id as INTEGER) = (select id from supported_tab where type='Engagement')) THEN
					INSERT INTO tab_permission (id, disease_stage_mapping_id ,tab_id, add, edit, view, delete)
					VALUES((select max(id) + 1 from tab_permission), v_new_DS_mapping_id_1, (select id from supported_tab where type='Engagement'), true, true,true, true);
				END IF;
				-- Close Case and Reopen case
				IF NOT EXISTS (SELECT id from tab_permission where disease_stage_mapping_id = v_new_DS_mapping_id_1 and CAST(tab_id as INTEGER) = (select id from supported_tab where type='Outcomes')) THEN
					INSERT INTO tab_permission (id, disease_stage_mapping_id ,tab_id, add, edit, view, delete)
					VALUES((select max(id) + 1 from tab_permission), v_new_DS_mapping_id_1, (select id from supported_tab where type='Outcomes'), true, true,true, true);
				END IF;
				IF NOT EXISTS (SELECT id from tab_permission where disease_stage_mapping_id = v_new_DS_mapping_id_2 and CAST(tab_id as INTEGER) = (select id from supported_tab where type='Outcomes')) THEN
					INSERT INTO tab_permission (id, disease_stage_mapping_id ,tab_id, add, edit, view, delete)
					VALUES((select max(id) + 1 from tab_permission), v_new_DS_mapping_id_2, (select id from supported_tab where type='Outcomes'), true, true,true, true);
				END IF;
				-- Delete Patient
				IF NOT EXISTS (select id from supported_tab where type='Delete Patient') THEN
					INSERT INTO supported_tab(id,type)
					VALUES((select max(id) +1 from supported_tab),'Delete Patient');
				END IF;
				IF NOT EXISTS (SELECT id from tab_permission where disease_stage_mapping_id = v_new_DS_mapping_id_1 and CAST(tab_id as INTEGER) = (select id from supported_tab where type='Delete Patient')) THEN
					INSERT INTO tab_permission (id, disease_stage_mapping_id ,tab_id, add, edit, view, delete)
					VALUES((select max(id) + 1 from tab_permission), v_new_DS_mapping_id_1, (select id from supported_tab where type='Delete Patient'), true, true,true, true);
				END IF;
				IF NOT EXISTS (SELECT id from tab_permission where disease_stage_mapping_id = v_new_DS_mapping_id_2 and CAST(tab_id as INTEGER) = (select id from supported_tab where type='Delete Patient')) THEN
					INSERT INTO tab_permission (id, disease_stage_mapping_id ,tab_id, add, edit, view, delete)
					VALUES((select max(id) + 1 from tab_permission), v_new_DS_mapping_id_2, (select id from supported_tab where type='Delete Patient'), true, true,true, true);
				END IF;
			END IF;
	END LOOP;
END $$;


INSERT into regimen (name, dosing_days, disease_id) values ('Regimen for H mono/poly', 179, (Select Id from disease_template where disease_name = 'Drug Resistant Tuberculosis'));
INSERT into regimen (name, dosing_days, disease_id) values ('Longer M/XDR-TB regimen', 539, (Select Id from disease_template where disease_name = 'Drug Resistant Tuberculosis'));
INSERT into regimen (name, dosing_days, disease_id) values ('Shorter MDR/RR-TB regimen', 269, (Select Id from disease_template where disease_name = 'Drug Resistant Tuberculosis'));
INSERT into regimen (name, dosing_days, disease_id) values ('BPaL regimen', 182, (Select Id from disease_template where disease_name = 'Drug Resistant Tuberculosis'));

INSERT into regimen (name, dosing_days, disease_id) values ('6H', 180, (Select Id from disease_template where disease_name = 'Latent Tuberculosis Infection'));
INSERT into regimen (name, dosing_days, disease_id) values ('3RH', 90, (Select Id from disease_template where disease_name = 'Latent Tuberculosis Infection'));
INSERT into regimen (name, dosing_days, disease_id) values ('3HP', 84, (Select Id from disease_template where disease_name = 'Latent Tuberculosis Infection'));
INSERT into regimen (name, dosing_days, disease_id) values ('6Lfx', 180, (Select Id from disease_template where disease_name = 'Latent Tuberculosis Infection'));
INSERT into regimen (name, dosing_days, disease_id) values ('4R', 182, (Select Id from disease_template where disease_name = 'Latent Tuberculosis Infection'));
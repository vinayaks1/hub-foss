INSERT INTO public.trigger(client_id, cron_time, default_template_id, event_name, function_name, notification_type, template_ids, trigger_id, vendor_id)
	VALUES (171, '0 7 * * *', 10, 'getPatientsWhoHaveNotMarkedAdherenceForToday', 'getPatientsWhoHaveNotMarkedAdherenceForToday', 'Push Notification', '10', 9, 20);

SELECT setval('trigger_id_seq', (SELECT max(id) FROM trigger));
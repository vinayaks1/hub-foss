---Updating the monitoring methods mappings when stage = diagnosed on treatment for both public and private (switch case)

DO
$do$
    declare
        disease_stage_ids int[] := ARRAY(SELECT id FROM disease_stage_mapping where stage_id in (SELECT id FROM stages where stage_name in ('DIAGNOSED_ON_TREATMENT_PRIVATE', 'DIAGNOSED_ON_TREATMENT_PUBLIC' ))
                                         and disease_template_id in (select id from disease_template dt where disease_name in ('Drug Resistant Tuberculosis', 'Drug Sensitive Tuberculosis', 'Latent Tuberculosis Infection')));

        var int;
    BEGIN
        foreach var in array disease_stage_ids loop
        IF NOT EXISTS (SELECT * from disease_stage_key_mapping where disease_stage_id = var and field_id = (select id from field where key = 'monitoringMethod' and module = 'IAM' limit 1) ) THEN
          insert into disease_stage_key_mapping (id, default_value, deleted, disease_stage_id, required, field_id) VALUES ((select max(id) + 1 from disease_stage_key_mapping), null, false, var, true, (select id from field where key = 'monitoringMethod' and module = 'IAM' limit 1));
        end if;

    end loop ;
end;
$do$;


-- Fixing for typeOfCaseFinding
-- Adding the mapping for all stages [except close] for disease = TPT

DO
$do$
    declare
        disease_stage_ids int[] := ARRAY(SELECT id FROM disease_stage_mapping
                                         where stage_id in (SELECT id FROM stages where stage_name in ('PRESUMPTIVE_OPEN_PUBLIC', 'PRESUMPTIVE_OPEN_PRIVATE', 'DIAGNOSED_NOT_ON_TREATMENT', 'DIAGNOSED_ON_TREATMENT_PRIVATE', 'DIAGNOSED_ON_TREATMENT_PUBLIC' ))
                                         and disease_template_id in (select id from disease_template dt where disease_name in ('Latent Tuberculosis Infection')));

        var int;
    BEGIN
        foreach var in array disease_stage_ids loop
        IF NOT EXISTS (SELECT * from disease_stage_key_mapping where disease_stage_id = var and field_id = (select id from field where key = 'typeOfCaseFinding' and module = 'Episode' limit 1) ) THEN
          insert into disease_stage_key_mapping (id, default_value, deleted, disease_stage_id, required, field_id) VALUES ((select max(id) + 1 from disease_stage_key_mapping), null, false, var, true, (select id from field where key = 'typeOfCaseFinding' and module = 'Episode' limit 1));
        end if;

    end loop ;
end;
$do$;




--- Fixing for weight
--- Adding the mapping for on treatment for tpt

DO
$do$
    declare
        disease_stage_ids int[] := ARRAY(SELECT id FROM disease_stage_mapping
                                         where stage_id in (SELECT id FROM stages where stage_name in ('DIAGNOSED_ON_TREATMENT_PRIVATE', 'DIAGNOSED_ON_TREATMENT_PUBLIC' ))
                                         and disease_template_id in (select id from disease_template dt where disease_name in ('Latent Tuberculosis Infection')));

        var int;
    BEGIN
        foreach var in array disease_stage_ids loop
        IF NOT EXISTS (SELECT * from disease_stage_key_mapping where disease_stage_id = var and field_id = (select id from field where key = 'weight' and module = 'Person' limit 1) ) THEN
          insert into disease_stage_key_mapping (id, default_value, deleted, disease_stage_id, required, field_id) VALUES ((select max(id) + 1 from disease_stage_key_mapping), null, false, var, true, (select id from field where key = 'weight' and module = 'Person' limit 1));
        end if;

    end loop ;
end;
$do$;
DO
$do$
    declare
disease_stage_ids int[] := ARRAY(SELECT id FROM disease_stage_mapping
                                         where stage_id in (SELECT id FROM stages where stage_name in ('PRESUMPTIVE_OPEN_PRIVATE', 'DIAGNOSED_NOT_ON_TREATMENT', 'DIAGNOSED_ON_TREATMENT_PRIVATE', 'DIAGNOSED_ON_TREATMENT_PUBLIC'  ))
                                         and disease_template_id in (select id from disease_template dt where disease_name in ('Drug Resistant Tuberculosis', 'Drug Sensitive Tuberculosis', 'Latent Tuberculosis Infection')));

        var int;
BEGIN
delete from disease_stage_key_mapping where field_id = (Select id from field where key = 'epSite' and module = 'Episode');
foreach var in array disease_stage_ids loop
          insert into disease_stage_key_mapping (id, default_value, deleted, disease_stage_id, required, field_id) VALUES ((select max(id) + 1 from disease_stage_key_mapping), null, false, var, true, (select id from field where key = 'epSite' and module = 'Episode' limit 1));
end loop ;
end;
$do$;

--- update DrugResistance mapping

DO
$do$
    declare
disease_stage_ids int[] := ARRAY(SELECT id FROM disease_stage_mapping
                                         where stage_id in (SELECT id FROM stages where stage_name in ('PRESUMPTIVE_OPEN_PRIVATE', 'DIAGNOSED_NOT_ON_TREATMENT', 'DIAGNOSED_ON_TREATMENT_PRIVATE', 'DIAGNOSED_ON_TREATMENT_PUBLIC'  ))
                                         and disease_template_id in (select id from disease_template dt where disease_name in ('Drug Resistant Tuberculosis', 'Drug Sensitive Tuberculosis', 'Latent Tuberculosis Infection')));

        var int;
BEGIN
delete from disease_stage_key_mapping where field_id = (Select id from field where key = 'drugResistance' and module = 'Episode');
foreach var in array disease_stage_ids loop
          insert into disease_stage_key_mapping (id, default_value, deleted, disease_stage_id, required, field_id) VALUES ((select max(id) + 1 from disease_stage_key_mapping), null, false, var, true, (select id from field where key = 'drugResistance' and module = 'Episode' limit 1));
end loop ;
end;
$do$;
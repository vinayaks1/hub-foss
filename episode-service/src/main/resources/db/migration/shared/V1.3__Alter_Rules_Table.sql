ALTER TABLE rules
    ADD COLUMN IF NOT EXISTS to_disease_stage_mapping_id bigint,
    ADD COLUMN IF NOT EXISTS from_disease_stage_mapping_id bigint;
ALTER table adverse_reaction
    ADD COLUMN if not exists otherPredominantCondition text;
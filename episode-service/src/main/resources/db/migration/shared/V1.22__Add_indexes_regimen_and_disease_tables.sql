create index if not exists disease_template_disease_name_index
    on disease_template (disease_name);

create index if not exists regimen_disease_id_index
    on regimen (disease_id);
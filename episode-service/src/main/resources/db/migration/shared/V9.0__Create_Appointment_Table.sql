create table if not exists appointment
(
    id bigserial not null,
    episode_id bigint,
    deleted_at        timestamp,
    deleted_by_id      integer,
    date             timestamp,
    time             text,
    visit_number     integer,
    follow_up_date   timestamp,
    status           text,
    primary key (id),
    constraint fk_episode_id
        foreign key(episode_id)
	    references episode(id)
);

create table if not exists appointment_answer
(
    id bigserial not null
        constraint appointment_answer_pkey
        primary key,
    appointment_id integer not null,
        constraint fk_appointment_id
        foreign key(appointment_id)
        references appointment(id),
    field_id         integer not null,
    answer           text
);
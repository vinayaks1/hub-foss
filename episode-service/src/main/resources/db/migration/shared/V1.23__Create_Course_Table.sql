create table if not exists course
(
    id         bigserial
        constraint course_pkey
            primary key,
    title   varchar(255)
);
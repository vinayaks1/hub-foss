create table if not exists episode_document
(
    id                  bigserial not null,
    added_on            timestamp,
    date_of_record      timestamp,
    disease_id          bigint,
    doctor_name         varchar(255),
    doctor_phone        varchar(255),
    episode_id          bigint,
    image_url           varchar(255),
    notes               varchar(255),
    type                varchar(255),
    primary key (id)
);
create table if not exists course_url_mapping
(
    id bigserial not null
    constraint course_url_mapping_pkey
    primary key,
    course_id bigint,
    language varchar(255),
    title varchar(255),
    url varchar(255)
);
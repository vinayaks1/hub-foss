create table if not exists client
(
    id            bigserial
        constraint client_pkey
            primary key,
    event_flow_id bigint,
    created_date  timestamp,
    name          varchar(255),
    password      varchar(255)
);
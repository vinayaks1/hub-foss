create index if not exists episode_client_id_deleted_index
on episode (client_id, deleted);
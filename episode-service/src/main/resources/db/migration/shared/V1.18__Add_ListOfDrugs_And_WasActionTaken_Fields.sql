ALTER TABLE adverse_reaction
ADD COLUMN list_of_drugs text;

ALTER TABLE causality_management
ADD COLUMN was_action_taken text;
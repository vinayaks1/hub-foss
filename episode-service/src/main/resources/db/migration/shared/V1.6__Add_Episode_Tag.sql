create table if not exists episode_tag
(
    id         bigserial
        constraint episode_tag_pkey
            primary key,
    added_on   timestamp,
    episode_id bigint,
    tag_name   varchar(255),
    tag_date   timestamp,
    sticky  boolean
);

create index if not exists episode_tag_tag_name_index
    on episode_tag (tag_name);

create index if not exists episode_tag_episode_id_index
    on episode_tag (episode_id);

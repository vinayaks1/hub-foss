ALTER TABLE disease_stage_key_mapping
DROP COLUMN IF EXISTS key;

ALTER TABLE disease_stage_key_mapping
DROP COLUMN IF EXISTS module;

ALTER TABLE disease_stage_key_mapping
ADD COLUMN field_id integer;

ALTER TABLE episode
ADD COLUMN disease_id integer;

ALTER TABLE episode
ALTER COLUMN type_of_episode TYPE varchar(255);

ALTER TABLE IF EXISTS episode_associations
RENAME TO episode_association;

ALTER TABLE episode_association
DROP COLUMN IF EXISTS id_name;

ALTER TABLE episode_association
ADD COLUMN association_id integer;

ALTER TABLE episode_association
RENAME id_value TO value;

ALTER TABLE IF EXISTS episode_hierarchy_linkages
RENAME TO episode_hierarchy_linkage;

ALTER TABLE episode_hierarchy_linkage
RENAME relation TO relation_id;

ALTER TABLE IF EXISTS episode_hierarchy_linkages
RENAME TO episode_hierarchy_linkage;

ALTER TABLE IF EXISTS episode_stages
RENAME TO episode_stage;

ALTER TABLE episode_stage_data
DROP COLUMN IF EXISTS key;

ALTER TABLE episode_stage_data
ADD COLUMN field_id integer;

create table if not exists field
(
    id          integer not null
    constraint field_pkey
    primary key,
    key        varchar(255),
    module varchar(255)
);

ALTER TABLE IF EXISTS supported_relations
RENAME TO supported_relation;

ALTER TABLE supported_relation
RENAME type TO name;

ALTER TABLE supported_relation
ADD COLUMN type integer;

ALTER TABLE IF EXISTS supported_tabs
RENAME TO supported_tab;

ALTER TABLE IF EXISTS tab_permissions
RENAME TO tab_permission;
ALTER table adverse_reaction
    ADD COLUMN if not exists created_on timestamp;

ALTER table adverse_reaction
    ADD COLUMN if not exists updated_on timestamp;

ALTER table causality_management
    ADD COLUMN if not exists created_on timestamp;

ALTER table causality_management
    ADD COLUMN if not exists updated_on timestamp;

ALTER table aers_outcome
    ADD COLUMN if not exists created_on timestamp;

ALTER table aers_outcome
    ADD COLUMN if not exists updated_on timestamp;


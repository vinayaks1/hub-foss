create table if not exists episode_app_config
(
    id         bigserial
        constraint episode_app_config_pkey
            primary key,
    technology   varchar(255),
    allow_dose_marking boolean,
    validation_required   boolean,
    backdated_dose_allowed   boolean,
    backdated_limit  bigint,
    deployment  varchar(255),
    UNIQUE (deployment, technology)
);



INSERT INTO public.episode_app_config ( technology, deployment, allow_dose_marking, validation_required, backdated_dose_allowed, backdated_limit) VALUES ( 'DEFAULT', 'DEFAULT', false, false, false, 0);
INSERT INTO public.episode_app_config ( technology, deployment, allow_dose_marking, validation_required, backdated_dose_allowed, backdated_limit) VALUES ( 'None', 'IND', true, false, true, 28);
INSERT INTO public.episode_app_config ( technology, deployment, allow_dose_marking, validation_required, backdated_dose_allowed, backdated_limit) VALUES ( '99DOTS', 'IND', true, false, true, 28);
INSERT INTO public.episode_app_config ( technology, deployment, allow_dose_marking, validation_required, backdated_dose_allowed, backdated_limit) VALUES ( 'MERM', 'IND', true, false, true, 28);

ALTER TABLE public.rules ADD COLUMN IF NOT EXISTS client_id integer;

UPDATE public.rules SET  client_id = ( select id from client where name = 'Nikshay' ) where client_id is null and id not in (
	select Id from public.rules rd where "condition" like '%clientId%'
);
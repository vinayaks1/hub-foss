create table if not exists regimen
(
    id  bigserial not null
    constraint regimen_pkey
    primary key,
    name        varchar(255),
    dosing_days INTEGER,
    disease_id INTEGER
)
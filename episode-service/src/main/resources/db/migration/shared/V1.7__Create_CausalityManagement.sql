create table if not exists causality_management
(
    id                     bigserial not null
        constraint causality_management_pkey
            primary key,
    action_taken           text,
    anti_tb_medicines      text,
    cause_medicine_linkage text,
    dechallenge            text,
    expectedness           text,
    new_medicines          text,
    other_medicines        text,
    rechallenge            text,
    suspected_drugs        text,
    treatment_details      text
);
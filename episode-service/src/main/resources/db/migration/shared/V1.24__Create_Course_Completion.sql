create table if not exists course_completion
(
    id         bigserial
        constraint course_completion_pkey
            primary key,
    course_id  bigint,
    created_on   timestamp,
    episode_id bigint,
    is_complete  boolean
);

create index if not exists course_completion_episode_id_index
    on course_completion (episode_id);
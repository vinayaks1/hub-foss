INSERT INTO public.rules (id, action, condition, description, priority, rule_namespace)
VALUES (9, 'output.put("ToStage", "DIAGNOSED_NOT_ON_TREATMENT");',
        'return null != input.get("Stage") && input.get("Stage").equals("DIAGNOSED_NOT_ON_TREATMENT_CLOSED") ' ||
        '&& (null == input.get("TreatmentOutcome") || input.get("TreatmentOutcome").equals(""));',
        'TB Stage Transitions from DIAGNOSED_NOT_ON_TREATMENT_CLOSED to DIAGNOSED_NOT_ON_TREATMENT', 1,
        'STAGE_TRANSITION')
ON CONFLICT DO NOTHING;

INSERT INTO public.rules (id, action, condition, description, priority, rule_namespace)
VALUES (10, 'output.put("ToStage", "DIAGNOSED_ON_TREATMENT");',
        'return null != input.get("Stage") && input.get("Stage").equals("DIAGNOSED_OUTCOME_ASSIGNED") ' ||
        '&& (null == input.get("TreatmentOutcome") || input.get("TreatmentOutcome").equals(""));',
        'TB Stage Transitions from DIAGNOSED_OUTCOME_ASSIGNED to DIAGNOSED_ON_TREATMENT', 1,
        'STAGE_TRANSITION')
ON CONFLICT DO NOTHING;
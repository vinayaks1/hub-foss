-- Update/Insert rules
UPDATE public.rules
SET
    action = 'output.put("ToStage", "PRESUMPTIVE_OPEN_PUBLIC");',
    condition = 'var typeOfEpisode = input.get("typeOfEpisode");' ||
                'var outcome = input.get("TreatmentOutcome");' ||
                'return (null == outcome || outcome.equals("")) && (typeOfEpisode.equals("IndiaTbPublic"));',
    description = 'TB Stage Transitions to PRESUMPTIVE_OPEN_PUBLIC',
    priority = 2,
    rule_namespace = 'STAGE_TRANSITION',
    to_disease_stage_mapping_id = 1,
    from_disease_stage_mapping_id = null
WHERE id = 1;


INSERT INTO public.rules
    (id, action, condition, description, priority, rule_namespace, to_disease_stage_mapping_id, from_disease_stage_mapping_id)
    VALUES
           (14,
            'output.put("ToStage", "PRESUMPTIVE_OPEN_PRIVATE");',
            'var typeOfEpisode = input.get("typeOfEpisode");' ||
            'var outcome = input.get("TreatmentOutcome");' ||
            'return (null == outcome || outcome.equals("")) && (typeOfEpisode.equals("IndiaTbPrivate"));',
            'TB Stage Transitions to PRESUMPTIVE_OPEN_PRIVATE',
            2,
            'STAGE_TRANSITION',
            2,
            null);

UPDATE public.rules
SET
    action = 'output.put("ToStage", "PRESUMPTIVE_CLOSED");',
    condition = 'var outcome = input.get("TreatmentOutcome");'  ||
                'return null != outcome && !outcome.equals("");',
    description = 'TB Stage Transitions from PRESUMPTIVE_OPEN_PUBLIC to PRESUMPTIVE_CLOSED',
    priority = 1,
    rule_namespace = 'STAGE_TRANSITION',
    to_disease_stage_mapping_id = 3,
    from_disease_stage_mapping_id = 1
WHERE id = 2;

INSERT INTO public.rules
    (id, action, condition, description, priority, rule_namespace, to_disease_stage_mapping_id, from_disease_stage_mapping_id)
    VALUES
           (15,
            'output.put("ToStage", "PRESUMPTIVE_CLOSED");',
            'var outcome = input.get("TreatmentOutcome");' ||
            'return null != outcome && !outcome.equals("");',
            'TB Stage Transitions from PRESUMPTIVE_OPEN_PRIVATE to PRESUMPTIVE_CLOSED',
            1,
            'STAGE_TRANSITION',
            3,
            2);


UPDATE public.rules
SET
    action = 'output.put("ToStage", "PRESUMPTIVE_OPEN_PUBLIC");',
    condition = 'var outcome = input.get("TreatmentOutcome");' ||
                'var typeOfEpisode = input.get("typeOfEpisode");' ||
                'return (null == outcome || outcome.equals("")) && ' ||
                'typeOfEpisode.equals("IndiaTbPublic");',
    description = 'TB Stage Transitions from PRESUMPTIVE_CLOSED to PRESUMPTIVE_OPEN_PUBLIC',
    priority = 1,
    rule_namespace = 'STAGE_TRANSITION',
    to_disease_stage_mapping_id = 1,
    from_disease_stage_mapping_id = 3
WHERE id = 3;

INSERT INTO public.rules
    (id, action, condition, description, priority, rule_namespace, to_disease_stage_mapping_id, from_disease_stage_mapping_id)
    VALUES
           (16,
            'output.put("ToStage", "PRESUMPTIVE_OPEN_PRIVATE");',
            'var outcome = input.get("TreatmentOutcome");' ||
            'var typeOfEpisode = input.get("typeOfEpisode");' ||
            'return (null == outcome || outcome.equals("")) && ' ||
            'typeOfEpisode.equals("IndiaTbPrivate");',
            'TB Stage Transitions from PRESUMPTIVE_CLOSED to PRESUMPTIVE_OPEN_PRIVATE',
            1,
            'STAGE_TRANSITION',
            2,
            3);

UPDATE public.rules
SET
    action = 'output.put("ToStage", "DIAGNOSED_NOT_ON_TREATMENT_CLOSED");',
    condition = 'var outcome = input.get("TreatmentOutcome");' ||
                'return outcome != null && !outcome.equals("");',
    description = 'TB Stage Transitions from DIAGNOSED_NOT_ON_TREATMENT to DIAGNOSED_NOT_ON_TREATMENT_CLOSED',
    priority = 1,
    rule_namespace = 'STAGE_TRANSITION',
    to_disease_stage_mapping_id = 8,
    from_disease_stage_mapping_id = 4
WHERE id = 4;

UPDATE public.rules
SET
    action = 'output.put("ToStage", "DIAGNOSED_OUTCOME_ASSIGNED");',
    condition = 'var outcome = input.get("TreatmentOutcome");' ||
                'var typeOfEpisode = input.get("typeOfEpisode");' ||
                'return outcome != null && !outcome.equals("") && typeOfEpisode.equals("IndiaTbPublic");',
    description = 'TB Stage Transitions from DIAGNOSED_ON_TREATMENT_PUBLIC to DIAGNOSED_OUTCOME_ASSIGNED',
    priority = 1,
    rule_namespace = 'STAGE_TRANSITION',
    to_disease_stage_mapping_id = 7,
    from_disease_stage_mapping_id = 6
WHERE id = 5;

INSERT INTO public.rules
    (id, action, condition, description, priority, rule_namespace, to_disease_stage_mapping_id, from_disease_stage_mapping_id)
    VALUES
           (17,
            'output.put("ToStage", "DIAGNOSED_OUTCOME_ASSIGNED");',
            'var outcome = input.get("TreatmentOutcome");' ||
            'var typeOfEpisode = input.get("typeOfEpisode");' ||
            'return outcome != null && !outcome.equals("") && typeOfEpisode.equals("IndiaTbPrivate");',
            'TB Stage Transitions from DIAGNOSED_ON_TREATMENT_PRIVATE to DIAGNOSED_OUTCOME_ASSIGNED',
            1,
            'STAGE_TRANSITION',
            7,
            5);

-- Rule - No TB Transition for DIAGNOSED_OUTCOME_ASSIGNED When Outcome Already Assigned
DELETE from public.rules where id = 6;

-- Rule - No TB Transition for PRESUMPTIVE_CLOSED When Outcome Already Assigned
DELETE from public.rules WHERE id = 7;

-- Rule - No TB Transition for DIAGNOSED_NOT_ON_TREATMENT_CLOSED When Outcome Already Assigned
DELETE from public.rules WHERE id = 8;

UPDATE public.rules
SET id = 9,
    action = 'output.put("ToStage", "DIAGNOSED_NOT_ON_TREATMENT");',
    condition = 'var outcome = input.get("TreatmentOutcome");' ||
                'return null == outcome || outcome.equals("");',
    description = 'TB Stage Transitions from DIAGNOSED_NOT_ON_TREATMENT_CLOSED to DIAGNOSED_NOT_ON_TREATMENT',
    priority = 1,
    rule_namespace = 'STAGE_TRANSITION',
    to_disease_stage_mapping_id = 4,
    from_disease_stage_mapping_id = 8
WHERE id = 9;

UPDATE public.rules
SET id = 10,
    action = 'output.put("ToStage", "DIAGNOSED_ON_TREATMENT_PUBLIC");',
    condition = 'var outcome = input.get("TreatmentOutcome");' ||
                'var typeOfEpisode = input.get("typeOfEpisode");' ||
                'return (null == outcome || outcome.equals("")) && typeOfEpisode.equals("IndiaTbPublic");',
    description = 'TB Stage Transitions from DIAGNOSED_OUTCOME_ASSIGNED to DIAGNOSED_ON_TREATMENT_PUBLIC',
    priority = 1,
    rule_namespace = 'STAGE_TRANSITION',
    to_disease_stage_mapping_id = 6,
    from_disease_stage_mapping_id = 7
WHERE id = 10;

INSERT INTO public.rules (id, action, condition, description, priority, rule_namespace, to_disease_stage_mapping_id, from_disease_stage_mapping_id)
VALUES (18,
        'output.put("ToStage", "DIAGNOSED_ON_TREATMENT_PRIVATE");',
        'var outcome = input.get("TreatmentOutcome");' ||
        'var typeOfEpisode = input.get("typeOfEpisode");' ||
        'return (null == outcome || outcome.equals("")) && typeOfEpisode.equals("IndiaTbPrivate");',
        'TB Stage Transitions from DIAGNOSED_OUTCOME_ASSIGNED to DIAGNOSED_ON_TREATMENT_PRIVATE',
        1,
        'STAGE_TRANSITION',
        5,
        7);

UPDATE public.rules
SET id = 11,
    action = 'output.put("ToStage", "DIAGNOSED_NOT_ON_TREATMENT");',
    condition = 'var testReason = input.get("Reason"); ' ||
                'var status = input.get("Status"); ' ||
                'var isPositive = false; ' ||
                'var typeMap = input.get("TypeMap"); ' ||
                'if(null != typeMap)' ||
                '{' ||
                    'foreach(testType: typeMap.entrySet())' ||
                    '{' ||
                        'var testTypeData = testType.value;' ||
                        'var testResult = testTypeData.Result;' ||
                        'if(testResult.Positive)' ||
                        '{' ||
                            'isPositive = true;' ||
                        '}' ||
                    '}' ||
                '} ' ||
                'return testReason == "Diagnosis" && status == "Results Available" && isPositive;',
    description = 'TB Stage Transitions from PRESUMPTIVE_OPEN_PUBLIC to DIAGNOSED_NOT_ON_TREATMENT',
    priority = 1,
    rule_namespace = 'STAGE_TRANSITION',
    to_disease_stage_mapping_id = 4,
    from_disease_stage_mapping_id = 1
WHERE id = 11;

INSERT INTO public.rules (id, action, condition, description, priority, rule_namespace, to_disease_stage_mapping_id, from_disease_stage_mapping_id)
VALUES (19,
        'output.put("ToStage", "DIAGNOSED_NOT_ON_TREATMENT");',
        'var testReason = input.get("Reason"); ' ||
        'var status = input.get("Status"); ' ||
        'var isPositive = false; ' ||
        'var typeMap = input.get("TypeMap"); ' ||
        'if(null != typeMap)' ||
        '{' ||
            'foreach(testType: typeMap.entrySet())' ||
            '{' ||
                'var testTypeData = testType.value;' ||
                'var testResult = testTypeData.Result;' ||
                'if(testResult.Positive)' ||
                '{' ||
                    'isPositive = true;' ||
                '}' ||
            '}' ||
        '} ' ||
        'return testReason == "Diagnosis" && status == "Results Available" && isPositive;',
        'TB Stage Transitions from PRESUMPTIVE_OPEN_PRIVATE to DIAGNOSED_NOT_ON_TREATMENT',
        1,
        'STAGE_TRANSITION',
        4,
        2);

UPDATE public.rules
SET id = 12,
    action = 'output.put("ToStage", "DIAGNOSED_ON_TREATMENT_PUBLIC");',
    condition = 'var treatmentStartTimeStamp = input.get("TreatmentStartTimeStamp");' ||
                'var outcome = input.get("TreatmentOutcome");' ||
                'var typeOfEpisode = input.get("typeOfEpisode");' ||
                'return null != treatmentStartTimeStamp && (null == outcome || outcome.equals(""))' ||
                '&& typeOfEpisode.equals("IndiaTbPublic");',
    description = 'TB Stage Transitions from DIAGNOSED_NOT_ON_TREATMENT to DIAGNOSED_ON_TREATMENT_PUBLIC',
    priority = 1,
    rule_namespace = 'STAGE_TRANSITION',
    to_disease_stage_mapping_id = 6,
    from_disease_stage_mapping_id = 4
WHERE id = 12;

INSERT INTO public.rules (id, action, condition, description, priority, rule_namespace, to_disease_stage_mapping_id, from_disease_stage_mapping_id)
VALUES (20,
        'output.put("ToStage", "DIAGNOSED_ON_TREATMENT_PRIVATE");',
        'var treatmentStartTimeStamp = input.get("TreatmentStartTimeStamp");' ||
        'var outcome = input.get("TreatmentOutcome");' ||
        'var typeOfEpisode = input.get("typeOfEpisode");' ||
        'return null != treatmentStartTimeStamp && typeOfEpisode.equals("IndiaTbPrivate") ' ||
        '&& (null == outcome ||outcome.equals(""));',
        'TB Stage Transitions from DIAGNOSED_NOT_ON_TREATMENT to DIAGNOSED_ON_TREATMENT_PRIVATE',
        1,
        'STAGE_TRANSITION',
        5,
        4);

UPDATE public.rules
SET id = 13,
    action = 'output.put("ToStage", "DIAGNOSED_ON_TREATMENT_PRIVATE");',
    condition = 'var treatmentStartDate = input.get("TreatmentStartTimeStamp");' ||
                'var outcome = input.get("TreatmentOutcome");' ||
                'var typeOfEpisode = input.get("typeOfEpisode");' ||
                'return null != treatmentStartDate && typeOfEpisode.equals("IndiaTbPrivate") ' ||
                '&& (null == outcome ||outcome.equals(""));',
    description = 'TB Stage Transitions from PRESUMPTIVE_OPEN_PRIVATE to DIAGNOSED_ON_TREATMENT_PRIVATE',
    priority = 0,
    rule_namespace = 'STAGE_TRANSITION',
    to_disease_stage_mapping_id = 5,
    from_disease_stage_mapping_id = 2
WHERE id = 13;

ALTER TABLE rules
RENAME from_disease_stage_mapping_id TO from_id;

ALTER TABLE rules
RENAME to_disease_stage_mapping_id TO to_id;

ALTER TABLE public.field
ADD COLUMN If Not exists entity varchar(255);

INSERT INTO public.rules (id, action, condition, description, priority, rule_namespace, to_id, from_id) VALUES
((select max(id) + 1 from rules), 'output.put("ToStage", "DIAGNOSED_ON_TREATMENT_PUBLIC");',
 'var treatmentStartDate = input.get("treatmentStartTimeStamp");' ||
 'var outcome = input.get("treatmentOutcome");' ||
 'var typeOfEpisode = input.get("typeOfEpisode");' ||
 'var validKeyPopulationForTreatment = {"Contact of Known TB Patients", "Patient on immunosuppressants", "Silicosis", "Anti-TNF treatment", "Dialysis", "Transplantation"};' ||
 'var keyPopulation = input.get("keyPopulation");' ||
 'boolean valid = false;' ||
 'if(null != keyPopulation && !keyPopulation.equals("")) {' ||
 '  foreach(keyPopulationIterator : validKeyPopulationForTreatment) {
        if(keyPopulation.contains(keyPopulationIterator)) {
            valid = true;
        }
    }' ||
 '}' ||
 'return null != treatmentStartDate && typeOfEpisode.equals("IndiaTbPublic") && (null == outcome ||outcome.equals("")) && valid;',
 'TB Stage Transitions from PRESUMPTIVE_OPEN_PUBLIC to DIAGNOSED_ON_TREATMENT_PUBLIC', 0, 'STAGE_TRANSITION', (Select Id from stages where stage_name = 'DIAGNOSED_ON_TREATMENT_PUBLIC'), (Select Id from stages where stage_name = 'PRESUMPTIVE_OPEN_PUBLIC'));



 Update rules set condition = 'var testreason = input.get("reason"); var status = input.get("status"); var isPositive = false; var typeMap = input.get("typeMap"); if(null != typeMap){foreach(testType: typeMap.entrySet()){var testTypeData = testType.value;var testResult = testTypeData.result;if(testResult.positive){isPositive = true;}}} return testreason == "Diagnosis" && status == "Results Available" && isPositive;' where Id in (11,19);
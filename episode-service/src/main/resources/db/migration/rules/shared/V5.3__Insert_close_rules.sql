INSERT INTO public.rules (id, action, condition, description, priority, rule_namespace)
VALUES (4, 'output.put("ToStage", "DIAGNOSED_NOT_ON_TREATMENT_CLOSED");',
        'var stage = input.get("Stage");' ||
        'var outcome = input.get("TreatmentOutcome");' ||
        'return stage != null && stage.equals("DIAGNOSED_NOT_ON_TREATMENT") && outcome != null && !outcome.equals("");',
        'TB Stage Transitions from DIAGNOSED_NOT_ON_TREATMENT to DIAGNOSED_NOT_ON_TREATMENT_CLOSED', 1,
        'STAGE_TRANSITION')
ON CONFLICT DO NOTHING;

INSERT INTO public.rules (id, action, condition, description, priority, rule_namespace)
VALUES (5, 'output.put("ToStage", "DIAGNOSED_OUTCOME_ASSIGNED");',
        'var stage = input.get("Stage");' ||
        'var outcome = input.get("TreatmentOutcome");' ||
        'return stage != null && stage.equals("DIAGNOSED_ON_TREATMENT") && outcome != null && !outcome.equals("");',
        'TB Stage Transitions from DIAGNOSED_ON_TREATMENT to DIAGNOSED_OUTCOME_ASSIGNED', 1,
        'STAGE_TRANSITION')
ON CONFLICT DO NOTHING;

INSERT INTO public.rules (id, action, condition, description, priority, rule_namespace)
VALUES (6, 'output.put("ToStage", "DIAGNOSED_OUTCOME_ASSIGNED");',
        'var stage = input.get("Stage");' ||
        'var outcome = input.get("TreatmentOutcome");' ||
        'return stage != null && stage.equals("DIAGNOSED_OUTCOME_ASSIGNED") && outcome != null && !outcome.equals("");',
        'No TB Transition for DIAGNOSED_OUTCOME_ASSIGNED When Outcome Already Assigned', 1,
        'STAGE_TRANSITION')
ON CONFLICT DO NOTHING;

INSERT INTO public.rules (id, action, condition, description, priority, rule_namespace)
VALUES (7, 'output.put("ToStage", "PRESUMPTIVE_CLOSED");',
        'var stage = input.get("Stage");' ||
        'var outcome = input.get("TreatmentOutcome");' ||
        'return stage != null && stage.equals("PRESUMPTIVE_CLOSED") && outcome != null && !outcome.equals("");',
        'No TB Transition for PRESUMPTIVE_CLOSED When Outcome Already Assigned', 1,
        'STAGE_TRANSITION')
ON CONFLICT DO NOTHING;

INSERT INTO public.rules (id, action, condition, description, priority, rule_namespace)
VALUES (8, 'output.put("ToStage", "DIAGNOSED_NOT_ON_TREATMENT_CLOSED");',
        'var stage = input.get("Stage");' ||
        'var outcome = input.get("TreatmentOutcome");' ||
        'return stage != null && stage.equals("DIAGNOSED_NOT_ON_TREATMENT_CLOSED") && outcome != null && !outcome.equals("");',
        'No TB Transition for DIAGNOSED_NOT_ON_TREATMENT_CLOSED When Outcome Already Assigned', 1,
        'STAGE_TRANSITION')
ON CONFLICT DO NOTHING;

Alter table rules Alter COLUMN condition TYPE text;

INSERT INTO public.rules (id, action, condition, description, priority, rule_namespace)
VALUES (11, 'output.put("ToStage", "DIAGNOSED_NOT_ON_TREATMENT");',
        'var testReason = input.get("Reason"); var status = input.get("Status"); var isPositive = false; ' ||
        'var stage = input.get("Stage"); var typeMap = input.get("TypeMap"); if(null != typeMap)' ||
        '{foreach(testType: typeMap.entrySet()){var testTypeData = testType.value;var testResult = testTypeData.Result;if(testResult.Positive == true){isPositive = true;}}} ' ||
        'return testReason == "Diagnosis" && status == "Results Available" && isPositive == true && stage == "PRESUMPTIVE_OPEN";',
        'TB Stage Transitions from PRESUMPTIVE_OPEN to DIAGNOSED_NOT_ON_TREATMENT', 1,
        'STAGE_TRANSITION')
ON CONFLICT DO NOTHING;

UPDATE public.rules SET PRIORITY = 2 WHERE ID = 1;
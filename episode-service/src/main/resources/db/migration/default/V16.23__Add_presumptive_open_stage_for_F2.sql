DO $$

DECLARE
	v_d2c_client_id INTEGER;
	v_new_disease_id INTEGER;
	v_new_disease_ids INTEGER [];
	old_disease_ids INTEGER[];
	v_new_stage_ids INTEGER[];
	v_new_DS_mapping_ids INTEGER[];
	v_new_field_ids INTEGER[];
	v_required_field_both_stages INTEGER[];
	v_field_ids_both_stages INTEGER[];
	DS_id INTEGER;
	new_field_id INTEGER;
	existing_field_id INTEGER;
	v_new_DS_mapping_id_on_treatment INTEGER;
	v_new_DS_mapping_id_off_treatment INTEGER;
	_disease_template_name_list varchar[] := array['F2 Disease Template'];
	presumptive_open_stage INTEGER;
  BEGIN
        SELECT id into presumptive_open_stage FROM stages where stage_name = 'PRESUMPTIVE_OPEN' and stage_display_name ='Presumptive (Open)';
            IF presumptive_open_stage  is NULL THEN
                WITH new_stage_1 AS (
                    INSERT INTO stages (id, stage_name, stage_display_name) VALUES
                    ((select COALESCE(MAX(id), 0) + 1 from stages),'PRESUMPTIVE_OPEN', 'Presumptive (Open)')
                    RETURNING id)
                SELECT id INTO presumptive_open_stage from new_stage_1;
            END IF;
        SELECT id into v_new_DS_mapping_id_on_treatment from disease_stage_mapping where disease_template_id = (select id from disease_template where disease_name='F2 Disease Template') and stage_id = (select id from stages where stage_name='PRESUMPTIVE_OPEN' and stage_display_name='Presumptive (Open)');
        IF v_new_DS_mapping_id_on_treatment  is NULL THEN
            WITH new_DS_mapping_1 AS (
                INSERT INTO disease_stage_mapping (id, disease_template_id, stage_id)
                VALUES
                    ((select COALESCE(MAX(id), 0) + 1 from disease_stage_mapping), (select id from disease_template where disease_name='F2 Disease Template'), (select id from stages where stage_name='PRESUMPTIVE_OPEN' and stage_display_name='Presumptive (Open)'))
                RETURNING id)
            SELECT id INTO v_new_DS_mapping_id_on_treatment FROM new_DS_mapping_1;
        END IF;

        SELECT array_agg(id) INTO v_field_ids_both_stages FROM field where key in ('primaryPhoneNumber','lastName', 'isDeleted', 'emailList', 'gender', 'mobileList', 'dateOfBirth', 'treatmentOutcome', 'address', 'city', 'pincode', 'contactPersonName', 'contactPersonAddress', 'contactPersonPhone', 'maritalStatus', 'langugage', 'iamStartDate');

        SELECT array_agg(id) INTO v_required_field_both_stages FROM field where key in ('firstName');

        FOREACH existing_field_id IN ARRAY v_field_ids_both_stages
            LOOP
                IF NOT EXISTS(SELECT id from disease_stage_key_mapping where disease_stage_id=v_new_DS_mapping_id_on_treatment and field_id=existing_field_id) THEN
                    INSERT INTO disease_stage_key_mapping (id, disease_stage_id, required, deleted, default_value, field_id)
                    VALUES ((select COALESCE(MAX(id), 0) + 1 from disease_stage_key_mapping), v_new_DS_mapping_id_on_treatment, false, false, null, existing_field_id);
                END IF;
                IF NOT EXISTS(SELECT id from disease_stage_key_mapping where disease_stage_id=v_new_DS_mapping_id_off_treatment and field_id=existing_field_id) THEN
                    INSERT INTO disease_stage_key_mapping (id, disease_stage_id, required, deleted, default_value, field_id)
                    VALUES ((select COALESCE(MAX(id), 0) + 1 from disease_stage_key_mapping), v_new_DS_mapping_id_off_treatment, false, false, null, existing_field_id);
                END IF;
            END LOOP;


        FOREACH existing_field_id IN ARRAY v_required_field_both_stages
            LOOP
                IF NOT EXISTS(SELECT id from disease_stage_key_mapping where disease_stage_id=v_new_DS_mapping_id_on_treatment and field_id=existing_field_id) THEN
                    INSERT INTO disease_stage_key_mapping (id, disease_stage_id, required, deleted, default_value, field_id)
                    VALUES ((select COALESCE(MAX(id), 0) + 1 from disease_stage_key_mapping), v_new_DS_mapping_id_on_treatment, true, false, null, existing_field_id);
                END IF;
                IF NOT EXISTS(SELECT id from disease_stage_key_mapping where disease_stage_id=v_new_DS_mapping_id_off_treatment and field_id=existing_field_id) THEN
                    INSERT INTO disease_stage_key_mapping (id, disease_stage_id, required, deleted, default_value, field_id)
                    VALUES ((select COALESCE(MAX(id), 0) + 1 from disease_stage_key_mapping), v_new_DS_mapping_id_off_treatment, true, false, null, existing_field_id);
                END IF;
            END LOOP;
END $$;


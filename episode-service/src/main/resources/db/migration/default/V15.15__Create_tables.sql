create table if not exists disease_stage_mapping
    (
        id                  integer not null
        constraint disease_stage_mapping_pkey
        primary key,
        disease_template_id integer,
        stage_id            integer,
        updated_on timestamp,
        created_on timestamp
    );

create table if not exists disease_stage_key_mapping
(
    id               integer not null
    constraint disease_stage_key_mapping_pkey
    primary key,
    disease_stage_id integer,
    required         boolean,
    deleted          boolean,
    default_value    varchar(255),
    field_id bigint,
    updated_on timestamp,
    created_on timestamp
);

create table if not exists field
(
    id bigserial
    constraint field_pkey
    primary key,
    key  varchar(255),
    module varchar(255),
    entity varchar(255)
);

CREATE TABLE if not exists rules (
  id bigint NOT NULL constraint rules_pkey primary key,
  action character varying(255) NULL,
  condition character varying(255) NULL,
  description character varying(255) NULL,
  priority integer NULL,
  rule_namespace character varying(255) NULL,
  to_id bigint NULL,
  from_id bigint NULL,
  client_id int NULL
);

create table if not exists stages
(
    id integer not null
    constraint stages_pkey
    primary key,
    stage_name         varchar(255),
    metadata           text,
    meta               text,
    stage_display_name varchar(255)
);


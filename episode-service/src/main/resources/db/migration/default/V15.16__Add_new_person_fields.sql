DO $$
DECLARE
_person_fields varchar[] := array['firstName','primaryPhoneNumber', 'lastName', 'emailList', 'gender', 'mobileList', 'dateOfBirth', 'address', 'isDeleted', 'city', 'pincode', 'contactPersonName', 'contactPersonAddress', 'contactPersonPhone', 'maritalStatus', 'language'];

BEGIN
    FOR i in 1 .. array_upper(_person_fields, 1)
        LOOP
        IF NOT EXISTS (SELECT * from field where key = _person_fields[i] and module = 'Person') THEN
                    insert into field (id, key, module) values((select COALESCE(MAX(id), 0) + 1 from field), _person_fields[i], 'Person');
                end if;
        END LOOP;

IF NOT EXISTS (SELECT * from field where key = 'treatmentOutcome' and module = 'Episode') THEN
  insert into field (id, key, module) values((select max(id)+1 from field), 'treatmentOutcome', 'Episode');
end if;

IF NOT EXISTS (SELECT * from field where key = 'iamStartDate' and module = 'IAM') THEN
  insert into field (id, key, module) values((select max(id)+1 from field), 'iamStartDate', 'IAM');
end if;

    END $$;




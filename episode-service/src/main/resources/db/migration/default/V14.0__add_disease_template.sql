CREATE TABLE IF NOT EXISTS disease_template (
	id serial NOT NULL,
	disease_name varchar(255) NULL,
	client_id int8 NULL DEFAULT 29,
	is_default bool NULL,
	CONSTRAINT disease_template_pkey PRIMARY KEY (id)
);

INSERT INTO disease_template (disease_name,client_id,is_default) VALUES ('Drug Resistant Tuberculosis',1,NULL);
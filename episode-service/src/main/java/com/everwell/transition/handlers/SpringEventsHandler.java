package com.everwell.transition.handlers;

import com.everwell.transition.model.request.episode.UpdateRiskRequest;
import com.everwell.transition.model.request.episodelogs.EpisodeLogDataRequest;
import com.everwell.transition.model.request.episodelogs.EpisodeLogDeletionRequest;
import com.everwell.transition.service.EpisodeLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.everwell.transition.constants.Constants.*;

@Component
public class SpringEventsHandler {

    @Autowired
    EpisodeLogService episodeLogService;

    @EventListener
    public void updateRiskLogs(UpdateRiskRequest updateRiskRequest) {
        List<EpisodeLogDataRequest> episodeLogDataRequests = new ArrayList<>();
        for(Long id : updateRiskRequest.getHighRiskIdList()) {
            episodeLogDataRequests.add(new EpisodeLogDataRequest(id,LFU_RISK_LOG_CATEGORY,updateRiskRequest.getAddedBy(),LFU_RISK_LOG_ACTION,LFU_RISK_LOG_COMMENT ));
        }
        episodeLogService.save(episodeLogDataRequests);
        for (Long id : updateRiskRequest.getLowRiskIdList()) {
            EpisodeLogDeletionRequest episodeLogDeletionRequest = new EpisodeLogDeletionRequest(id,Collections.singletonList(LFU_RISK_LOG_CATEGORY),null);
            episodeLogService.delete(episodeLogDeletionRequest);
        }
    }
}

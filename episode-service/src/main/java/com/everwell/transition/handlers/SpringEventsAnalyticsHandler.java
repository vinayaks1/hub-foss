package com.everwell.transition.handlers;

import com.everwell.transition.binders.EpisodeEventsBinder;
import com.everwell.transition.enums.eventStreamingEnums.EventAction;
import com.everwell.transition.enums.eventStreamingEnums.EventCategory;
import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.dto.EventStreamingAnalyticsDto;
import com.everwell.transition.model.request.episodetags.EpisodeTagBulkDeletionRequest;
import com.everwell.transition.model.request.episodetags.EpisodeTagRequest;
import com.everwell.transition.model.request.episodetags.SearchTagsRequest;
import com.everwell.transition.postconstruct.EpisodeTagStoreCreation;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import java.util.*;

import static com.everwell.transition.constants.Constants.*;

@Component
@EnableBinding(EpisodeEventsBinder.class)
public class SpringEventsAnalyticsHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(SpringEventsAnalyticsHandler.class);

    @Autowired
    EpisodeEventsBinder episodeEventsBinder;


    @Autowired
    private EpisodeTagStoreCreation episodeTagStoreCreation;

    @Autowired
    private ClientService clientService;

    private Long eventFlowId;


    public  List<EventStreamingAnalyticsDto> getSaveTagsAnalyticsDto(EpisodeTagRequest episodeTagRequest) {
        List<EventStreamingAnalyticsDto> eventStreamingAnalyticsDto = new ArrayList<>();
                episodeTagRequest.getEpisodeTagData().forEach(t ->{
                    eventStreamingAnalyticsDto.add(new EventStreamingAnalyticsDto(episodeTagStoreCreation.getEpisodeTagStoreMap().get(t.getTagName()).getTagGroup(),t.getTagName(),ACTION_ADD));
                });

                return eventStreamingAnalyticsDto;
    }

    public List<EventStreamingAnalyticsDto> getDeleteTagsAnalyticsDto(EpisodeTagBulkDeletionRequest episodeTagBulkDeletionRequest) {
        List<EventStreamingAnalyticsDto> eventStreamingAnalyticsDto = new ArrayList<>();
        episodeTagBulkDeletionRequest.getTagList().forEach(t ->{
            eventStreamingAnalyticsDto.add(new EventStreamingAnalyticsDto(episodeTagStoreCreation.getEpisodeTagStoreMap().get(t).getTagGroup(),t,ACTION_REMOVE));
        });
        return eventStreamingAnalyticsDto;
    }

    public List<EventStreamingAnalyticsDto> getSearchTagsAnalyticsDto(SearchTagsRequest searchTagsRequest) {
        List<EventStreamingAnalyticsDto> eventStreamingAnalyticsDto = new ArrayList<>();
        searchTagsRequest.getEpisodeIdList().forEach(t ->{
            eventStreamingAnalyticsDto.add(new EventStreamingAnalyticsDto(EventCategory.EPISODE_TAGS.getName(),t.toString(), EventAction.TAGS_SEARCH.getName()));
        });
        return eventStreamingAnalyticsDto;
    }


    @EventListener
    public void trackSaveTagsEvent(EpisodeTagRequest episodeTagRequest) {
        List<EventStreamingAnalyticsDto> eventStreamingAnalyticsDto = getSaveTagsAnalyticsDto(episodeTagRequest);
        LOGGER.info("[saveTagsEvent] called - " + eventStreamingAnalyticsDto.toString());
        pushToQueue(eventStreamingAnalyticsDto);
    }

    @EventListener
    public void trackDeleteTagsEvent(EpisodeTagBulkDeletionRequest episodeTagBulkDeletionRequest) {
        List<EventStreamingAnalyticsDto> eventStreamingAnalyticsDto = getDeleteTagsAnalyticsDto(episodeTagBulkDeletionRequest);
        LOGGER.info("[deleteTagsEvent] called - " + eventStreamingAnalyticsDto.toString());
        pushToQueue(eventStreamingAnalyticsDto);
    }

    @EventListener
    public void trackSearchTagsEvent(SearchTagsRequest searchTagsRequest) {
        List<EventStreamingAnalyticsDto> eventStreamingAnalyticsDto = getSearchTagsAnalyticsDto(searchTagsRequest);
        LOGGER.info("[searchTagsEvent] called - " + eventStreamingAnalyticsDto.toString());
        pushToQueue(eventStreamingAnalyticsDto);
    }

    @EventListener
    public void trackTagsEvent(EventStreamingAnalyticsDto eventStreamingAnalyticDto) {
        List<EventStreamingAnalyticsDto> eventStreamingAnalyticsDto = Collections.singletonList(eventStreamingAnalyticDto);
        LOGGER.info("[TagsEvent] called - " + eventStreamingAnalyticsDto.toString());
        pushToQueue(eventStreamingAnalyticsDto);
    }

    public void pushToQueue(List<EventStreamingAnalyticsDto> eventStreamingAnalyticsDto) {
        Client client = clientService.getClientByTokenOrDefault();
        eventFlowId = client.getEventFlowId();
        eventStreamingAnalyticsDto.forEach( event-> {
                    episodeEventsBinder.eventOutput().send(MessageBuilder.withPayload(Utils.asJsonString(event)).setHeader("CLIENT_ID",eventFlowId).build());
                }
        );
    }
}

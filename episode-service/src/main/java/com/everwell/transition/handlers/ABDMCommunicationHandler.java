package com.everwell.transition.handlers;

import com.everwell.transition.binders.ABDMCommunicationBinder;
import com.everwell.transition.constants.Constants;
import com.everwell.transition.enums.episode.EpisodeAssociationEnum;
import com.everwell.transition.model.dto.*;
import com.everwell.transition.model.dto.dispensation.DispensationData;
import com.everwell.transition.model.request.ABDMCareContextLinkRequest;
import com.everwell.transition.model.request.ABDMCareContextNotifyRequest;
import com.everwell.transition.model.request.ABDMNotifyViaSMSRequest;
import com.everwell.transition.service.EpisodeService;
import com.everwell.transition.service.impl.EpisodeServiceImpl;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import io.sentry.Sentry;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static com.everwell.transition.constants.Constants.*;
import static com.everwell.transition.constants.FieldConstants.*;

@Component
@EnableBinding(ABDMCommunicationBinder.class)
public class ABDMCommunicationHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(ABDMCommunicationHandler.class);

    @Autowired
    ABDMCommunicationBinder abdmCommunicationBinder;

    @Autowired
    EpisodeService episodeService;

    @Autowired
    EpisodeServiceImpl episodeServiceImpl;

    @Setter
    @Value("${default.client.id:}")
    private long clientId;


    @StreamListener(ABDMCommunicationBinder.DIAGNOSTICS_ADD_UPDATE_EVENT)
    public void diagnosticsEventHandler(Message<ABDMNotifyInputDto> input)
    {
        try{
            boolean success = Objects.requireNonNull(input.getHeaders().get(X_CLIENT_ID)).toString().equals(Long.toString(clientId));
            String sb = "input client_id = " +
                    input.getHeaders().get(X_CLIENT_ID) +
                    " nikshay client_Id = " + clientId +
                    " are equal = " + success;
            LOGGER.info(sb);

            if(!success) {
                return;
            }
            pushToQueue(input.getPayload(), Constants.HI_TYPE_DIAGNOSTIC_REPORT);
        }
        catch(Exception ex)
        {
            LOGGER.error(ex.toString());
            Sentry.capture(ex);
        }
    }

    @StreamListener(ABDMCommunicationBinder.DISPENSATION_ADD_UPDATE_EVENT)
    public void dispensationEventHandler(Message<EventStreamingDto<DispensationData>> message)
    {
        try {
            boolean success = Objects.requireNonNull(message.getHeaders().get(X_CLIENT_ID)).toString().equals(Long.toString(clientId));
            String sb = "input client_id = " +
                    message.getHeaders().get(X_CLIENT_ID) +
                    " nikshay client_Id = " + clientId +
                    " are equal = " + success;
            LOGGER.info(sb);

            if (!success) return;
            EventStreamingDto<DispensationData> input = message.getPayload();
            LOGGER.info("Input Data Dispensation " + Utils.asJsonString(input));
            DispensationData dispensationData = Utils.jsonToObject(Utils.asJsonString(input.getField()), new TypeReference<DispensationData>() {});
            LOGGER.info("Entity Id " + dispensationData.getEntityId());
            ABDMNotifyInputDto msg = new ABDMNotifyInputDto(clientId, dispensationData.getEntityId());
            pushToQueue(msg, Constants.HI_TYPE_PRESCRIPTION);
        }
        catch(Exception ex)
        {
            LOGGER.error(ex.toString());
            Sentry.capture(ex);
        }
    }

    private void pushToQueue(ABDMNotifyInputDto input, String hiType)
    {
        try
        {
            LOGGER.info("In push to queue");
            EpisodeDto episodeDto = episodeService.getEpisodeDetails(input.getEpisodeId(), input.getClientId());
            boolean generateAddReqForABDM = true;
            if(episodeDto.getAssociations().containsKey(EpisodeAssociationEnum.CareContextCreated.toString())
                    && episodeDto.getAssociations().get(EpisodeAssociationEnum.CareContextCreated.toString()).equals(Boolean.TRUE.toString())) {
                generateAddReqForABDM = false;
            }

            Map<String, Object> stageData = episodeDto.getStageData();
            String json = Utils.asJsonString(stageData.get(PERSON_EXTERNAL_IDS));
            List<ExternalIdResponse> externalIds = Utils.jsonToObject(json, new TypeReference<List<ExternalIdResponse>>() {});

            List<ExternalIdResponse> abhaAddressResponses = externalIds
                    .stream()
                    .filter(x -> x.getType().equals(HEALTH_ADDRESS))
                    .collect(Collectors.toList());

            LOGGER.info(abhaAddressResponses.toString());
            if(abhaAddressResponses.isEmpty() || null == abhaAddressResponses.get(0).getValue()) {
                String phoneNo = (String)stageData.get(PERSON_PRIMARY_PHONE);
                if(phoneNo != null && phoneNo.length() == 10 ) {
                    ABDMNotifyViaSMSRequest abdmNotifyViaSMSRequest = new ABDMNotifyViaSMSRequest(phoneNo);
                    abdmCommunicationBinder.abdmNotifyViaSMSOutput().send(MessageBuilder.withPayload(Utils.asJsonString(abdmNotifyViaSMSRequest)).build());
                }
                else {
                    LOGGER.info("Phone No. is not correct");
                }
                return;
            }
            String abhaAddress = abhaAddressResponses.get(0).getValue();
            String personName = stageData.get(PERSON_FIELD_FIRST_NAME) + (stageData.get(PERSON_FIELD_LAST_NAME) == null ? "" : (" " + stageData.get(PERSON_FIELD_LAST_NAME)));

            if (!generateAddReqForABDM)
            {
                ABDMCareContextNotifyRequest req =  new ABDMCareContextNotifyRequest(
                        episodeDto.getId(),
                        episodeDto.getPersonId(),
                        hiType,
                        personName,
                        abhaAddress);
                Message notifyMsg = MessageBuilder.withPayload(Utils.asJsonString(req)).build();
                abdmCommunicationBinder.abdmCareContextNotifyOutput().send(notifyMsg);
            }
            else {
                String dobStrAsLong = stageData.get(PERSON_DATE_OF_BIRTH).toString();
                LocalDate dob = Utils.convertMilliSecToDateNew(Long.parseLong(dobStrAsLong));
                String yob = Integer.toString(dob.getYear());
                LOGGER.info("yob  = " + yob);
                LOGGER.info(Utils.getFormattedDateNew(episodeDto.getCreatedDate(),"YYYY-MM-dd"));

                ABDMCareContextLinkRequest req =  new ABDMCareContextLinkRequest(
                        episodeDto.getId(),
                        episodeDto.getPersonId(),
                        hiType,
                        personName,
                        Utils.getFormattedDateNew(episodeDto.getCreatedDate(),"YYYY-MM-dd"),
                        Constants.IDENTIFIER_MOBILE,
                        (String)stageData.get(PERSON_PRIMARY_PHONE),
                        ((String)stageData.get(PERSON_GENDER)).charAt(0),
                        abhaAddress,
                        yob
                );
                Message linkCareContextMsg = MessageBuilder.withPayload(Utils.asJsonString(req)).build();
                abdmCommunicationBinder.abdmLinkCareContextOutput().send(linkCareContextMsg);
            }
        }
        catch (Exception ex)
        {
            LOGGER.error(ex.toString());
            Sentry.capture(ex);
        }
    }
}

package com.everwell.transition.consumers;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.constants.QueueConstants;
import com.everwell.transition.elasticsearch.ElasticConstants;
import com.everwell.transition.elasticsearch.indices.EpisodeIndex;
import com.everwell.transition.enums.ValidationStatusEnum;
import com.everwell.transition.model.dto.EpisodeSearchResult;
import com.everwell.transition.model.dto.notification.UserSmsDetails;
import com.everwell.transition.model.request.DoseReminderRequest;
import com.everwell.transition.model.request.adherence.SearchAdherenceRequest;
import com.everwell.transition.model.request.episodesearch.EpisodeSearchRequest;
import com.everwell.transition.model.response.AllAdherenceResponse;
import com.everwell.transition.service.EpisodeService;
import com.everwell.transition.service.IAMService;
import com.everwell.transition.service.NotificationService;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import io.sentry.Sentry;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class EpisodeDoseReminderConsumer implements Consumer {

    @Autowired
    IAMService iamService;

    @Autowired
    EpisodeService episodeService;

    @Autowired
    NotificationService notificationService;

    @RabbitListener(queues = QueueConstants.EPISODE_DOSE_REMINDER, concurrency = "1")
    public void consume(Message message) throws IOException {
        try {
            String body = new String(message.getBody());
            DoseReminderRequest doseReminderRequest = Utils.jsonToObject(body, new TypeReference<DoseReminderRequest>() {
            });
            EpisodeSearchRequest episodeSearchRequest = doseReminderRequest.getEpisodeSearchRequest();
            boolean fetchEpisodeFromElastic = true;
            List<EpisodeIndex> episodeIndexList = new ArrayList<>();
            do {
                EpisodeSearchResult episodeSearchResult = episodeService.elasticSearch(episodeSearchRequest);
                long episodeIndexSize = !CollectionUtils.isEmpty(episodeSearchResult.getEpisodeIndexList()) ? episodeSearchResult.getEpisodeIndexList().size() : 0;
                if (episodeIndexSize > 0) {
                    episodeSearchResult.getEpisodeIndexList().forEach(episode -> {
                        if (null != episode && null != episode.getStageData() && Objects.toString(episode.getStageData().get(FieldConstants.EPISODE_FIELD_PHONE_NUMBER)).matches(doseReminderRequest.getPhoneNumberValidationRegex())) {
                            episodeIndexList.add(episode);
                        }
                    });
                }
                if (episodeIndexSize < ElasticConstants.ELASTIC_BATCH_SIZE)
                    fetchEpisodeFromElastic = false;
                episodeSearchRequest.setScrollId(episodeSearchResult.getScrollId());
            } while (fetchEpisodeFromElastic);
            processDoseReminderJob(episodeIndexList, doseReminderRequest);
        } catch (Exception e) {
            Sentry.capture(e);
        }
    }

    private void processDoseReminderJob(List<EpisodeIndex> episodeIndexList, DoseReminderRequest doseReminderRequest) {
        List<String> episodesWhoTookDose = filterEpisodeWithDose(episodeIndexList, doseReminderRequest.isExcludingTodayDose(), doseReminderRequest.getDays());
        List<UserSmsDetails> userSmsDetails = new ArrayList<>();
        episodeIndexList.forEach(episode -> {
            if (!episodesWhoTookDose.contains(episode.getId())) {
                String phoneNumber = episode.getStageData().get(FieldConstants.EPISODE_FIELD_PHONE_NUMBER).toString();
                userSmsDetails.add(new UserSmsDetails(Long.valueOf(episode.getId()), null, doseReminderRequest.getPhonePrefix() + phoneNumber));
            }
        });
        if (!CollectionUtils.isEmpty(userSmsDetails)) {
            notificationService.sendSmsToIns(userSmsDetails, doseReminderRequest.getNotificationTriggerDto(), doseReminderRequest.getEventName(), doseReminderRequest.getClientId());
        }
    }

    public List<String> filterEpisodeWithDose(List<EpisodeIndex> episodeIndexList, boolean excludingTodayDose, long days) {
        List<String> episodesWhoTookDose;
        if (excludingTodayDose) {
            List<String> episodeIdList = episodeIndexList.stream().map(EpisodeIndex::getId).collect(Collectors.toList());
            episodesWhoTookDose = filterEpisodesForXDays(days, episodeIdList);
        } else {
            episodesWhoTookDose = filterEpisodesOnLastDosage(episodeIndexList);
        }
        return episodesWhoTookDose;
    }

    public List<String> filterEpisodesForXDays(long days, List<String> episodeIds) {
        List<String> episodesWhoTookDoseForLastXDays = new ArrayList<>();
        SearchAdherenceRequest searchAdherenceRequest = new SearchAdherenceRequest(episodeIds, null);
        AllAdherenceResponse adherenceResponse = Utils.processResponseEntity(iamService.getAdherenceBulk(searchAdherenceRequest), ValidationStatusEnum.ERROR_MESSAGE_FOR_GET_ADHERENCE_BULK.getMessage());
        if (!CollectionUtils.isEmpty(adherenceResponse.getAdherenceResponseList())) {
            adherenceResponse.getAdherenceResponseList().forEach(adherence -> {
                if (adherence.getConsecutiveMissedDosesFromYesterday() < days) {
                    episodesWhoTookDoseForLastXDays.add(adherence.getEntityId());
                }
            });
        }
        return episodesWhoTookDoseForLastXDays;
    }

    public List<String> filterEpisodesOnLastDosage(List<EpisodeIndex> episodeIndexList) {
        List<String> filteredIds = new ArrayList<>();
        LocalDateTime to = LocalDate.now(ZoneOffset.UTC).atStartOfDay();
        LocalDateTime from = to.minusDays(1);
        if (!CollectionUtils.isEmpty(episodeIndexList)) {
            episodeIndexList.forEach(episodeIndex -> {
                String lastDosageDateString = Objects.toString(episodeIndex.getStageData().get(FieldConstants.EPISODE_FIELD_LAST_DOSAGE), null);
                LocalDateTime lastDosageDate = Utils.toLocalDateTimeWithNull(lastDosageDateString, Constants.DATE_FORMAT);
                if (null != lastDosageDate && lastDosageDate.isBefore(to) && lastDosageDate.compareTo(from) >= 0) {
                    filteredIds.add(episodeIndex.getId());
                }
            });
        }
        return filteredIds;
    }
}

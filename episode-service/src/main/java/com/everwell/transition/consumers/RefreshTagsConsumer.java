package com.everwell.transition.consumers;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.constants.EpisodeTagConstants;
import com.everwell.transition.constants.QueueConstants;
import com.everwell.transition.elasticsearch.ElasticConstants;
import com.everwell.transition.elasticsearch.indices.EpisodeIndex;
import com.everwell.transition.handlers.INSHandler;
import com.everwell.transition.model.dto.ElasticDocTagsWrapper;
import com.everwell.transition.model.dto.EpisodeSearchResult;
import com.everwell.transition.model.dto.GenericEvents;
import com.everwell.transition.model.dto.notification.EmailRequest;
import com.everwell.transition.model.dto.notification.EmailTemplateDto;
import com.everwell.transition.model.request.RefreshTagsRequest;
import com.everwell.transition.model.request.episodesearch.EpisodeSearchRequest;
import com.everwell.transition.model.request.episodetags.EpisodeTagBulkDeletionRequest;
import com.everwell.transition.model.request.episodetags.EpisodeTagDataRequest;
import com.everwell.transition.model.request.episodetags.EpisodeTagRequest;
import com.everwell.transition.model.response.EpisodeTagResponse;
import com.everwell.transition.service.EpisodeService;
import com.everwell.transition.service.EpisodeTagService;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import io.sentry.Sentry;
import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

@Component
public class RefreshTagsConsumer implements Consumer {

    @Autowired
    private EpisodeTagService episodeTagService;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;
    @Autowired
    private EpisodeService episodeService;

    @Autowired
    private INSHandler insHandler;

    @RabbitListener(queues = QueueConstants.REFRESH_TAGS_QUEUE, concurrency = "1")
    public void consume(Message message) {
        RefreshTagsRequest refreshTagsRequest = new RefreshTagsRequest();
        try {
            String body = new String(message.getBody());
            refreshTagsRequest = Utils.jsonToObject(body, new TypeReference<RefreshTagsRequest>() {
            });
            EpisodeSearchRequest episodeSearchRequest = refreshTagsRequest.getEpisodeSearchRequest();
            episodeSearchRequest.setScroll(true);
            episodeSearchRequest.setSize(ElasticConstants.ELASTIC_BATCH_SIZE);
            List<String> failedIds = new ArrayList<>();
            long successfullIdsTotal = 0;
            boolean fetchEpisodeFromElastic = true;
            do {
                List<Long> successfullIds = new ArrayList<>();
                EpisodeSearchResult episodeSearchResult = episodeService.elasticSearch(episodeSearchRequest);
                EpisodeTagRequest episodeTagRequest = new EpisodeTagRequest(new ArrayList<>());
                long episodeIndexSize = !CollectionUtils.isEmpty(episodeSearchResult.getEpisodeIndexList()) ? episodeSearchResult.getEpisodeIndexList().size() : 0;
                if (episodeIndexSize > 0) {
                    episodeSearchResult.getEpisodeIndexList().forEach(episodeIndex -> {
                        List<EpisodeTagDataRequest> episodeTag = refreshTagsForEpisode(episodeIndex, failedIds, successfullIds);
                        episodeTagRequest.getEpisodeTagData().addAll(episodeTag);
                    });
                }
                List<String> automaticTags = EpisodeTagConstants.AUTOMATIC_TAGS;
                deleteEpisodeTags(new EpisodeTagBulkDeletionRequest(successfullIds, automaticTags, null, true));
                saveEpisodeTags(episodeTagRequest);
                successfullIdsTotal += successfullIds.size();
                if (episodeIndexSize < ElasticConstants.ELASTIC_BATCH_SIZE)
                    fetchEpisodeFromElastic = false;
                episodeSearchRequest.setScrollId(episodeSearchResult.getScrollId());
            } while (fetchEpisodeFromElastic);
            if (CollectionUtils.isEmpty(failedIds)) {
                sendEmail(EpisodeTagConstants.REFRESH_TAGS_JOB_SUCCESS, String.format(EpisodeTagConstants.REFRESH_TAGS_JOB_SUCCESS_BODY, successfullIdsTotal), refreshTagsRequest);
            } else {
                String failedIdsList = StringUtils.join(failedIds, ",");
                sendEmail(String.format(EpisodeTagConstants.REFRESH_TAGS_JOB_ERROR, refreshTagsRequest.getDeploymentCode()), String.format(EpisodeTagConstants.REFRESH_TAGS_JOB_ERROR_BODY, failedIds.size(), successfullIdsTotal, failedIdsList), refreshTagsRequest);
            }
        } catch (Exception e) {
            Sentry.capture(e);
            sendEmail(EpisodeTagConstants.REFRESH_TAGS_JOB_FAILURE, EpisodeTagConstants.REFRESH_TAGS_JOB_FAILURE_BODY + e, refreshTagsRequest);
        }
    }

    private List<EpisodeTagDataRequest> refreshTagsForEpisode(EpisodeIndex episode, List<String> failedIds, List<Long> successfulIds) {
        List<EpisodeTagDataRequest> episodeTagDataRequestList = new ArrayList<>();
        try {
            Long episodeId = Long.valueOf(episode.getId());
            LocalDateTime currentDate = LocalDateTime.now(ZoneOffset.UTC);
            LocalDateTime endDate = Utils.convertStringToDateNew(episode.getEndDate());
            LocalDateTime createdDate = Utils.convertStringToDateNew(episode.getCreatedDate());
            long daysSinceAddition = ChronoUnit.DAYS.between(createdDate, currentDate);
            List<String> tagsToAdd = new ArrayList<>();
            // Keep the New_Enrollment tag for at most 7 days
            if (daysSinceAddition <= EpisodeTagConstants.NEW_ENROLLMENT_TAG_MAX_DAYS && CollectionUtils.isEmpty(episode.getCurrentTags())) {
                tagsToAdd.add(EpisodeTagConstants.NEW_ENROLLMENT);
            }
            if (endDate.isBefore(currentDate) && (CollectionUtils.isEmpty(episode.getCurrentTags()) || !episode.getCurrentTags().contains(EpisodeTagConstants.END_DATE_PASSED))) {
                tagsToAdd.add(EpisodeTagConstants.END_DATE_PASSED);
            }
            tagsToAdd.forEach(tag -> {
                LocalDateTime tagDate = LocalDateTime.now(ZoneOffset.UTC).minusDays(1);
                EpisodeTagDataRequest episodeTagSticky = new EpisodeTagDataRequest(tag, episodeId, true);
                EpisodeTagDataRequest episodeTag = new EpisodeTagDataRequest(tag, tagDate, episodeId, false);
                episodeTagDataRequestList.add(episodeTagSticky);
                episodeTagDataRequestList.add(episodeTag);
            });
            successfulIds.add(episodeId);
        } catch (Exception e) {
            Sentry.capture(e);
            failedIds.add(episode.getId());
        }
        return episodeTagDataRequestList;
    }

    private void deleteEpisodeTags(EpisodeTagBulkDeletionRequest episodeTagBulkDeletionRequest) {
        episodeTagBulkDeletionRequest.validate();
        episodeTagService.deleteBulk(episodeTagBulkDeletionRequest);
        applicationEventPublisher.publishEvent(episodeTagBulkDeletionRequest);
    }

    private void saveEpisodeTags(EpisodeTagRequest episodeTagRequest) {
        episodeTagRequest.validate();
        List<EpisodeTagResponse> episodeTagResponse = episodeTagService.save(episodeTagRequest.getEpisodeTagData());
        applicationEventPublisher.publishEvent(episodeTagRequest);
        applicationEventPublisher.publishEvent(new ElasticDocTagsWrapper(episodeTagResponse));
    }

    private void sendEmail(String subject, String body, RefreshTagsRequest refreshTagsRequest) {
        EmailTemplateDto emailTemplateDto = new EmailTemplateDto();
        HashMap<String, String> bodyParameters = new HashMap<String, String>() {{
            put(Constants.BODY, body);
        }};
        HashMap<String, String> subjectParameters = new HashMap<String, String>() {{
            put(Constants.SUBJECT, subject);
        }};
        emailTemplateDto.setRecipient(Constants.EVERWELL_DEVELOPERS_MAIL);
        emailTemplateDto.setBodyParameters(bodyParameters);
        emailTemplateDto.setSubjectParameters(subjectParameters);
        EmailRequest emailRequest = new EmailRequest(refreshTagsRequest.getClientId(), refreshTagsRequest.getTriggerId(), refreshTagsRequest.getTemplateId(), refreshTagsRequest.getVendorId(), Constants.EVERWELL_DEVELOPERS_MAIL, Collections.singletonList(emailTemplateDto));
        GenericEvents<EmailRequest> emailDetails = new GenericEvents<>(emailRequest, Constants.EMPTY_STRING);
        insHandler.sendEmailToINS(emailDetails, refreshTagsRequest.getClientId());
    }
}

package com.everwell.transition.utils;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.RedisConnectionCommands;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;


@Component
public class CacheUtils {
    private static RedisTemplate<String, Object> redisTemplate;

    @Autowired
    public CacheUtils(RedisTemplate<String, Object> redisTemplate) {
        CacheUtils.redisTemplate = redisTemplate;
    }

    public static <T> T getFromCache(String key) {
        ValueOperations valueOperations = redisTemplate.opsForValue();
        Object value = valueOperations != null ? valueOperations.get(key) : null;
        return (null == value) ? null: (T) value;
    }

    public static void putIntoCache(String key, Object value, Long ttl) {
        redisTemplate.opsForValue().set(key, value, ttl, TimeUnit.SECONDS);
    }

    public static <V> void putIntoCacheBulk(Map<String, V> map) {
        redisTemplate.opsForValue().multiSet(map);
    }

    public static <T> T getFromCache(List<String> key) {
        Object obj = redisTemplate.opsForValue().multiGet(key);
        return (null == obj) ? null : ((T) obj);
    }
}

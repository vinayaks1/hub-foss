package com.everwell.transition.utils;

import com.everwell.transition.model.response.Response;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class OutputStreamUtils {

    public void writeCustomResponseToOutputStream(HttpServletResponse response, Response CustomResponse, HttpStatus httpStatus) throws IOException {
        response.setContentType("application/json");
        response.setStatus(httpStatus.value());
        response.getOutputStream()
                .println(Utils.asJsonString(CustomResponse));
    }
}

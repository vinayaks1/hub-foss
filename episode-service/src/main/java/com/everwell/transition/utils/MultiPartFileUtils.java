package com.everwell.transition.utils;

import com.everwell.transition.exceptions.InternalServerErrorException;
import com.everwell.transition.exceptions.ValidationException;
import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MultiPartFileUtils {

    private final static List<String> DEFAULT_SUPPORTED_FILE_EXTENSTION = Arrays.asList(
            "pdf", "jpeg", "jpg", "png"
    );

    private final static List<String> DEFAULT_IMAGE_SUPPORTED_FILE_EXTENSTION = Arrays.asList(
            "jpeg", "jpg", "png"
    );

    private final static List<String> COMMON_HTML_TAGS = Arrays.asList(
            "<html>", "<head>", "<title>", "<body>", "<script>", "<iframe>", "<b>", "<u>"
    );

    // speical characters that are allowed ".", "-", "_"
    private final static String DEFAULT_NON_SUPPORTED_FILE_CHARACTERS = "[${}&+\\[\\],:;=\\\\?@#|/'<>^*()%!]";

    private final static int MAX_ALLOWED_FILE_NAME_SIZE = 100;

    public static void validateMultipartFiles (List<MultipartFile> multipartFiles) {
        for (MultipartFile file : multipartFiles) {
            if (file.isEmpty())
                throw new ValidationException("Invalid file name, cannot be empty");
            String extension = FilenameUtils.getExtension(file.getOriginalFilename());
            validateFileExtension(extension);
            validateFileName(file.getOriginalFilename());
            validateFileContent(multipartFiles);
        }
    }

    private static void validateFileContent(List<MultipartFile> multipartFiles) {
        for (MultipartFile multipartFile : multipartFiles) {
            BufferedReader br;
            try {
                String line;
                InputStream is = multipartFile.getInputStream();
                String extension = FilenameUtils.getExtension(multipartFile.getOriginalFilename());
                if (DEFAULT_IMAGE_SUPPORTED_FILE_EXTENSTION.contains(extension)) {
                    isImageValid(is);
                    return;
                }
                br = new BufferedReader(new InputStreamReader(is));
                while ((line = br.readLine()) != null) {
                    // keeping it simple/brute force since PDF file has similar content to html syntax like:
                    // </Page/Some 0 0 1 /Page> which was matching regex and Jsoup html validations
                    for (String commonHtmlTag : COMMON_HTML_TAGS) {
                        if (line.contains(commonHtmlTag)) {
                            throw new ValidationException("Invalid file content!");
                        }
                    }
                }
            } catch (IOException e) {
                throw new ValidationException("Unable to process file, file might be currupted!");
            }
        }
    }

    public static void validateFileExtension (String extension) {
        if (!DEFAULT_SUPPORTED_FILE_EXTENSTION.contains(extension.toLowerCase(Locale.ROOT))) {
            throw new ValidationException("Unsupported File Format: " + extension);
        }
    }

    public static void validateFileName (String fileName) {
        if (fileName.length() > MAX_ALLOWED_FILE_NAME_SIZE)
            throw new ValidationException("File name too long!");
        Pattern regex = Pattern.compile(DEFAULT_NON_SUPPORTED_FILE_CHARACTERS);
        Matcher matcher = regex.matcher(fileName);
        String[] nameArr = fileName.split("\\.");
        if (nameArr.length > 2) {
            throw new ValidationException("Invalid file name, multiple periods are not allowed!");
        }
        if (matcher.find()) {
            throw new ValidationException("Invalid file name, special characters are not allowed!");
        }
    }

    public static void isImageValid(InputStream is) {
        try {
            BufferedImage image = ImageIO.read(is);
            if (image == null)
                throw new ValidationException("Invalid file content for image!");
        } catch (IOException e) {
            throw new ValidationException("Invalid file content for image!");
        }
    }

}

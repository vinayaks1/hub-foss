package com.everwell.transition.binders;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface INSBinder {
    String INS_SMS_EXCHANGE = "ins-sms-input";
    String INS_NOTIFICATION_EXCHANGE = "ins-notification-input";

    String INS_EMAIL_EXCHANGE = "ins-email-input";

    @Output(INS_NOTIFICATION_EXCHANGE)
    MessageChannel insNotificationOutput();

    @Output(INS_SMS_EXCHANGE)
    MessageChannel insSmsOutput();

    @Output(INS_EMAIL_EXCHANGE)
    MessageChannel insEmailOutput();

}

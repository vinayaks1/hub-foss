package com.everwell.transition.repositories;

import com.everwell.transition.model.db.EpisodeDocumentDetails;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EpisodeDocumentDetailsRepository extends JpaRepository<EpisodeDocumentDetails, Long> {

}

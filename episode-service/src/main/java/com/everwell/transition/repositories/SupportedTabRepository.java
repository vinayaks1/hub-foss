package com.everwell.transition.repositories;

import com.everwell.transition.model.db.SupportedTab;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SupportedTabRepository extends JpaRepository<SupportedTab, Long> {
}

package com.everwell.transition.repositories;

import com.everwell.transition.model.db.Episode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface EpisodeRepository extends JpaRepository<Episode, Long> {

    Episode findByIdAndClientIdAndDeletedIs (Long episodeId, Long clientId, Boolean deleted);

    Episode findByIdAndClientId(Long episodeId, Long clientId);

    List<Episode> findAllByPersonIdInAndClientIdAndDeletedFalse(List<Long> personId, Long clientId);

    @Query(value = "SELECT e FROM Episode e WHERE e.id in :idList and e.clientId = :clientId and e.deleted = false")
    List<Episode> getAllEpisodes(List<Long> idList, Long clientId);

    @Modifying
    @Transactional
    @Query(value = "UPDATE Episode e SET  e.riskStatus = :status WHERE e.id in :episodeIdList")
    void updateRiskStatusForEpisodeIdWithStatus(List<Long> episodeIdList,String status);
    List<Episode> findAllByClientIdAndDeletedFalse(Long clientId);

}

package com.everwell.transition.repositories;

import com.everwell.transition.model.db.Regimen;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RegimenRepository extends JpaRepository<Regimen, Long> {

    List<Regimen> findAllByDiseaseId(Long diseaseId);
}

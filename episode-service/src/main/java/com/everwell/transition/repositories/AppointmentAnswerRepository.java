package com.everwell.transition.repositories;

import com.everwell.transition.model.db.AppointmentAnswer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AppointmentAnswerRepository extends JpaRepository<AppointmentAnswer, Integer> {

    List<AppointmentAnswer> getAllByAppointmentId(Integer appointmentId);

}

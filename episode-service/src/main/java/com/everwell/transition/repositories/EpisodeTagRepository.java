package com.everwell.transition.repositories;

import com.everwell.transition.model.db.EpisodeTag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

public interface EpisodeTagRepository extends JpaRepository<EpisodeTag, Long> {

    List<EpisodeTag> getAllByEpisodeId(Long episodeId);

    @Modifying
    @Transactional
    @Query(value = "DELETE from episode_tag where id in (SELECT id FROM episode_tag e WHERE e.episode_id = :episodeId and e.tag_name in :tagList and e.tag_date in :tagDates)", nativeQuery = true)
    void deleteTags(Long episodeId, List<String> tagList, List<LocalDateTime> tagDates);


    @Query(value = "SELECT e FROM EpisodeTag e WHERE e.episodeId in :episodeIds and e.tagName in :tagNames")
    List<EpisodeTag> getTagsWithFilters(List<Long> episodeIds, List<String> tagNames);

    List<EpisodeTag> getAllByEpisodeIdIn(List<Long> episodeIds);


    @Modifying
    @Transactional
    void deleteByEpisodeIdIsInAndTagNameIsInAndTagDateIsIn(List<Long> episodeIds, List<String> tagList, List<LocalDateTime> tagDates);

    @Modifying
    @Transactional
    void deleteAllByEpisodeIdEqualsAndTagDateAfter(Long episodeId, LocalDateTime endDate);

    @Transactional
    @Modifying
    void deleteByEpisodeIdIsInAndTagNameIsInAndStickyIsTrue(List<Long> episodeIds, List<String> tagNames);

    @Transactional
    @Modifying
    void deleteByEpisodeIdIsInAndStickyIsTrue(List<Long> episodeIds);

}

package com.everwell.transition.repositories;

import com.everwell.transition.model.db.DiseaseStageMapping;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DiseaseStageMappingRepository extends JpaRepository<DiseaseStageMapping, Long> {

    List<DiseaseStageMapping> findAllByDiseaseTemplateIdInAndStageIdIn(Iterable<Long> diseaseIds, List<Long> stageIds);
}

package com.everwell.transition.repositories;

import com.everwell.transition.model.db.Appointment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AppointmentRepository extends JpaRepository<Appointment, Integer> {
    Appointment findByIdAndDeletedAtIsNull(Integer id);
    List<Appointment> findAllByIdInAndDeletedAtIsNullAndStatusIn(List<Integer> ids, List<String> status);
    List<Appointment> findAllByIdInAndDeletedAtIsNull(List<Integer> ids);
    List<Appointment> findAllByEpisodeIdAndDeletedAtIsNull(Long episodeId);
}

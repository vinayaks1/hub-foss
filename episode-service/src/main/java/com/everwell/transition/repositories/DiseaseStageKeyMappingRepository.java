package com.everwell.transition.repositories;

import com.everwell.transition.model.db.DiseaseStageKeyMapping;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DiseaseStageKeyMappingRepository extends JpaRepository<DiseaseStageKeyMapping, Long> {

    List<DiseaseStageKeyMapping> findAllByDiseaseStageIdIn(List<Long> diseaseStageIds);
}

package com.everwell.transition.repositories;

import com.everwell.transition.model.db.EpisodeTag;
import com.everwell.transition.model.db.EpisodeTagStore;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

public interface EpisodeTagStoreRepository extends JpaRepository<EpisodeTagStore, Long> {
}

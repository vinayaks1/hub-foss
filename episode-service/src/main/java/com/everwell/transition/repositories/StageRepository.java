package com.everwell.transition.repositories;

import com.everwell.transition.model.db.Stage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface StageRepository extends JpaRepository<Stage, Long> {

    Stage findByStageName(String stageName);
}

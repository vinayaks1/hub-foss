package com.everwell.transition.repositories;

import com.everwell.transition.model.db.EpisodeAssociation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EpisodeAssociationRepository extends JpaRepository<EpisodeAssociation, Long> {

    List<EpisodeAssociation> getAllByEpisodeIdIn(List<Long> episodeIds);
}

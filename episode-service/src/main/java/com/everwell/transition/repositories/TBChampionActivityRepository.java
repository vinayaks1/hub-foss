package com.everwell.transition.repositories;

import com.everwell.transition.model.db.TBChampionActivity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TBChampionActivityRepository extends JpaRepository<TBChampionActivity, Long> {
    List<TBChampionActivity> getAllByEpisodeIdOrderByCreatedOnDesc(Long episodeId);
}

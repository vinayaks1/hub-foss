package com.everwell.transition.repositories;

import com.everwell.transition.model.db.TreatmentPlan;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TreatmentPlanRepository extends JpaRepository<TreatmentPlan, Long> {

    TreatmentPlan findByIdAndClientId(Long id, Long clientId);
     List<TreatmentPlan> findByClientId(Long clientId);
}

package com.everwell.transition.repositories;

import com.everwell.transition.model.db.CourseCompletion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CourseCompletionRepository extends JpaRepository<CourseCompletion,Long> {
    List<CourseCompletion> getAllByEpisodeId(Long episodeId);
}

package com.everwell.transition.repositories;

import com.everwell.transition.model.db.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client, Long> {
    Client findByName(String name);
}


package com.everwell.transition.repositories;

import com.everwell.transition.model.db.DiseaseTemplate;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface DiseaseTemplateRepository extends JpaRepository<DiseaseTemplate, Long> {

    DiseaseTemplate findByDiseaseNameAndClientId(String name, Long clientId);

    List<DiseaseTemplate> findByClientId(Long clientId);

    Optional<DiseaseTemplate> findByIdAndClientId(Long id, Long clientId);

    DiseaseTemplate findByClientIdAndIsDefaultTrue(Long clientId);
}

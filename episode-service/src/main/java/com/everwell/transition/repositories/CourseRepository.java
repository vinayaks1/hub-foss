package com.everwell.transition.repositories;

import com.everwell.transition.model.db.Course;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseRepository extends JpaRepository<Course,Long> {
}

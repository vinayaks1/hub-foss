package com.everwell.transition.repositories;

import com.everwell.transition.model.db.TabPermission;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TabPermissionRepository extends JpaRepository<TabPermission, Long> {

    List<TabPermission> findAllByDiseaseStageMappingIdIn(Iterable<Long> diseaseStageMappingIds);
}

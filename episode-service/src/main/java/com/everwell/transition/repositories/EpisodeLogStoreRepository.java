package com.everwell.transition.repositories;

import com.everwell.transition.model.db.EpisodeLogStore;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EpisodeLogStoreRepository extends JpaRepository<EpisodeLogStore, Long> {
}

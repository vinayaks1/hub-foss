package com.everwell.transition.repositories;

import com.everwell.transition.model.db.AersOutcome;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OutcomeRepository extends JpaRepository<AersOutcome,Long> {
}

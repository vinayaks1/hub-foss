package com.everwell.transition.repositories;

import com.everwell.transition.model.db.Field;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FieldRepository extends JpaRepository<Field, Long> {

    List<Field> findAllByKeyIn(List<String> fieldNames);
}

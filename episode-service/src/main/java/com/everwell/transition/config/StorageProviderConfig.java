package com.everwell.transition.config;

import org.jobrunr.jobs.mappers.JobMapper;
import org.jobrunr.storage.StorageProvider;
import org.jobrunr.storage.sql.postgres.PostgresStorageProvider;
import org.postgresql.ds.PGSimpleDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class StorageProviderConfig {

    @Value("${org.jobrunr.database.datasource.url}")
    private String jobrunrURL;

    @Value("${org.jobrunr.database.datasource.username}")
    private String jobrunrDbUser;

    @Value("${org.jobrunr.database.datasource.password}")
    private String jobrunrDbPass;

    @Value("${org.jobrunr.database.datasource.db}")
    private String jobrunrDbName;
    @Bean
    public StorageProvider storageProvider(JobMapper jobMapper)
    {
        PGSimpleDataSource pgSimpleDataSource = new PGSimpleDataSource();
        pgSimpleDataSource.setURL(jobrunrURL);
        pgSimpleDataSource.setUser(jobrunrDbUser);
        pgSimpleDataSource.setPassword(jobrunrDbPass);
        pgSimpleDataSource.setDatabaseName(jobrunrDbName);

        PostgresStorageProvider storageProvider = new PostgresStorageProvider(pgSimpleDataSource);
        storageProvider.setJobMapper(jobMapper);
        return storageProvider;
    }
}

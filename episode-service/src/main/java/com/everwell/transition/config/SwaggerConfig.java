package com.everwell.transition.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@EnableSwagger2
@Profile({"beta","dev","oncall"})
@Configuration
public class SwaggerConfig {

    @Bean
    public Docket api() {

        Contact contact = new Contact(
                "Everwell Health Solutions",
                "https://www.everwell.org/",
                "developers@everwell.org");

        List<VendorExtension> vext = new ArrayList<>();
        ApiInfo apiInfo = new ApiInfo(
                "Episode Service",
                "Episode Management Service is responsible for taking care of single or multiple episodes of a patient. Also maintains other episode related data.",
                "1.1.1",
                "",
                contact,
                "",
                "",
                vext);


        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.everwell.transition"))
                .paths(PathSelectors.any())
                .build()
                .securitySchemes(Arrays.asList(apiKey()))
                .genericModelSubstitutes(ResponseEntity.class)
                .useDefaultResponseMessages(false);
    }


    private ApiKey apiKey() {
        return new ApiKey("jwtToken", "Authorization", "header");
    }


}

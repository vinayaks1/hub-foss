package com.everwell.transition.service;

import com.everwell.transition.model.request.SSO.*;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.response.SSO.DeviceIdsMapResponse;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

public interface SSOService {

    ResponseEntity<Response<Map<String, Object>>> login(Map<String, Object> loginRequest, Long PlatformId);

    Boolean registration (RegistrationRequest registrationRequest);

    ResponseEntity<Response<Void>> logout (Map<String, Object> logoutRequest);

    ResponseEntity<Response<Void>> refreshDeviceIdAndLastActivityDate(Map<String, Object> updateDeviceIdRequest);

    ResponseEntity<Response<DeviceIdsMapResponse>> fetchDeviceIdUserMap(List<String> userNames);

}

package com.everwell.transition.service;

import com.everwell.transition.model.response.android.GenericUiResponse;

import java.util.List;

public interface AndroidConfigService {
    List<GenericUiResponse> getFitFeed(String language, String region);

    List<GenericUiResponse> getHomeFeed(String language, String region);
}

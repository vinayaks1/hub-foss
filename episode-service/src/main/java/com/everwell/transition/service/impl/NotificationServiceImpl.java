package com.everwell.transition.service.impl;

import com.everwell.transition.constants.AppointmentConstants;
import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.constants.NotificationConstants;
import com.everwell.transition.enums.notification.NotificationEndpoint;
import com.everwell.transition.enums.notification.NotificationField;
import com.everwell.transition.handlers.INSHandler;
import com.everwell.transition.model.db.Trigger;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.dto.GenericEvents;
import com.everwell.transition.model.dto.notification.*;
import com.everwell.transition.model.request.EpisodeNotificationSearchRequest;
import com.everwell.transition.model.request.EpisodeUnreadNotificationSearchRequest;
import com.everwell.transition.model.response.EpisodeUnreadNotificationResponse;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.repositories.TriggerRepository;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.EpisodeService;
import com.everwell.transition.service.NotificationService;
import com.everwell.transition.utils.CacheUtils;
import com.everwell.transition.utils.Utils;
import io.sentry.Sentry;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.time.*;
import java.util.*;

@Service
public class NotificationServiceImpl extends DataGatewayHelperServiceImpl implements NotificationService {

    private static Logger LOGGER = LoggerFactory.getLogger(NotificationServiceImpl.class);

    @Autowired
    private EpisodeService episodeService;

    @Autowired
    private INSHandler insHandler;

    @Autowired
    private TriggerRepository triggerStoreRepository;

    @Autowired
    ClientService clientService;

    @Async
    @EventListener
    public void notificationAndSmsEvent(NotificationEventDto notificationEventDto) {
        sendPushNotificationForEpisodeWithExtraData(notificationEventDto.getEpisodeDto(), notificationEventDto.getExtraData(), notificationEventDto.getEventName());
        sendAppointmentConfirmationSms(notificationEventDto.getEpisodeDto(), notificationEventDto.getDoctorName(), notificationEventDto.getAppointmentDate());
    }

    @Override
    public void sendSms(NotificationTriggerEvent notificationTriggerEvent, String idHeader, String eventName) {
        try {
            String functionName = notificationTriggerEvent.getNotificationTriggerDto().getFunctionName();
            String invokeMethod = triggerStoreRepository.getTriggerByFunctionName(functionName).getFunctionName();
            List<UserSmsDetails> userSmsDetails = (List<UserSmsDetails>) Utils.invokeMethod(episodeService, notificationTriggerEvent, idHeader, invokeMethod);
            NotificationTriggerDto notificationTriggerDto = notificationTriggerEvent.getNotificationTriggerDto();
            SmsDetails smsDetails = new SmsDetails(userSmsDetails, notificationTriggerDto.getVendorId(), notificationTriggerDto.getTriggerId(), notificationTriggerDto.getTemplateId(), notificationTriggerDto.getTemplateIds(), notificationTriggerDto.getMandatory(), notificationTriggerDto.getDefaultConsent(), notificationTriggerDto.getIsDefaultTime(), notificationTriggerDto.getTimeOfSms());
            GenericEvents<SmsDetails> event = new GenericEvents<>(smsDetails, eventName);
            insHandler.sendSmsToINS(Collections.singletonList(event), Long.parseLong(idHeader));
        } catch (Exception e) {
            LOGGER.info("[sendSms] threw exception " + e);
            Sentry.capture(e);
        }
    }

    @Override
    public void sendPushNotificationGeneric(NotificationTriggerDto notificationTriggerDto, Long clientId, String eventName) {
        try {
            LOGGER.info("[sendPushNotificationGeneric] Starting execution for params - " + notificationTriggerDto + " ClientId - " + clientId + " EventName - " + eventName);
            setAuthentication(clientId);
            String functionName = notificationTriggerDto.getFunctionName();
            String invokeMethod = triggerStoreRepository.getTriggerByFunctionName(functionName).getFunctionName();
            PNDtoWithNotificationData pushNotificationDetails = (PNDtoWithNotificationData) episodeService.getClass().getDeclaredMethod(invokeMethod, Long.class, String.class).invoke(episodeService, clientId, notificationTriggerDto.getTimeZone());
            sendToIns(pushNotificationDetails, notificationTriggerDto, eventName, clientId);
            LOGGER.info("[sendPushNotificationGeneric] completed. PNDtoWithNotificationData - " + pushNotificationDetails + " EventName -" + eventName + " ClientId - " + clientId);
        } catch (Exception e) {
            LOGGER.info("[sendPushNotificationGeneric] exception " + e.toString());
            Sentry.capture(e);
        } finally {
            SecurityContextHolder.clearContext();
        }
    }

    @Override
    public void sendPushNotificationForEpisodeWithExtraData(EpisodeDto episodeDto, Map<String, Object> extraData, String eventName) {
        try {
            Trigger trigger = triggerStoreRepository.getTriggerByEventName(eventName);
            String functionName = trigger.getFunctionName();
            Long clientId = trigger.getClientId();
            setAuthentication(clientId);
            NotificationTriggerDto notificationTriggerDto = new NotificationTriggerDto(trigger.getHierarchyId(), trigger.getVendorId(), trigger.getDefaultTemplateId(), trigger.getTriggerId(), trigger.getFunctionName(), trigger.getTimeZone(), trigger.getNotificationType(), true, true, true, null, Collections.singletonList(trigger.getDefaultTemplateId()));
            PNDtoWithNotificationData pushNotificationDetails = (PNDtoWithNotificationData) episodeService.getClass().getDeclaredMethod(functionName, Long.class, EpisodeDto.class, Map.class).invoke(episodeService, clientId, episodeDto, extraData);
            sendToIns(pushNotificationDetails, notificationTriggerDto, eventName, clientId);
        } catch (Exception e) {
            LOGGER.info("[sendPushNotificationForEpisodeWithExtraData] error " + e.toString());
            Sentry.capture(e);
        }
        finally {
            SecurityContextHolder.clearContext();
        }
    }

    @Override
    public void sendAppointmentConfirmationSms(EpisodeDto episodeDto, String doctorName, LocalDateTime appointmentDate) {
        String functionName = NotificationConstants.SEND_APPOINTMENT_CONFIRMATION_SMS;
        Trigger trigger =  triggerStoreRepository.getTriggerByFunctionName(functionName);
        Object mobileList = episodeDto.getStageData().get(FieldConstants.PERSON_MOBILE_LIST);
        List<Map<String, String>> mobileListMap = (List<Map<String, String>>) mobileList;
        NotificationTriggerDto notificationTriggerDto = new NotificationTriggerDto(trigger.getHierarchyId(), trigger.getVendorId(), trigger.getDefaultTemplateId(), trigger.getTriggerId(), trigger.getFunctionName(), trigger.getTimeZone(), trigger.getNotificationType(), true, true,true, null, Collections.singletonList(trigger.getDefaultTemplateId()));
        String patientName = episodeDto.getStageData().get(FieldConstants.PERSON_FIELD_FIRST_NAME).toString() + " " + episodeDto.getStageData().getOrDefault(FieldConstants.PERSON_FIELD_LAST_NAME, "").toString();
        String appointmentDateInRequiredTimeZone = Utils.convertLocalDateTimeToGivenTimeZone(appointmentDate, trigger.getTimeZone());
        List<UserSmsDetails> userSmsDetails = new ArrayList<>();
        Map<String, String> userParameters = new HashMap<>();
        userParameters.put(AppointmentConstants.PATIENT_NAME, patientName);
        userParameters.put(AppointmentConstants.DOCTOR_NAME, doctorName);
        userParameters.put(AppointmentConstants.APPOINTMENT_DATE, appointmentDateInRequiredTimeZone);
        userParameters.put(NotificationConstants.HERE, NotificationConstants.AARO_DOWNLOAD_LINK);
        userSmsDetails.add(new UserSmsDetails(0L, userParameters, mobileListMap.get(0).get(FieldConstants.NUMBER)));
        sendSmsToIns(userSmsDetails, notificationTriggerDto, trigger.getEventName(), trigger.getClientId());
    }

    @Override
    public Boolean validateOtp(String phoneNumber, String otp){
        String receivedOtp = CacheUtils.getFromCache(NotificationConstants.REGISTRATION_OTP_PREFIX + phoneNumber);
        boolean otpValidated = null != receivedOtp && receivedOtp.equals(otp);
        return otpValidated;
    }

    @Override
    public void sendRegistrationOtp(Long clientId, String phoneNumber, String purposeText, String source) {
        String functionName = NotificationConstants.REGISTRATION_EVENT;
        Trigger trigger =  triggerStoreRepository.getTriggerByFunctionName(functionName);
        NotificationTriggerDto notificationTriggerDto = new NotificationTriggerDto(trigger.getHierarchyId(), trigger.getVendorId(), trigger.getDefaultTemplateId(), trigger.getTriggerId(), trigger.getFunctionName(), trigger.getTimeZone(), trigger.getNotificationType(), true, true,true, null, Collections.singletonList(trigger.getDefaultTemplateId()));
        Random random = new Random();
        int value = random.nextInt(9999 - 100) + 100;
        String otp = StringUtils.leftPad(String.valueOf(value), 4, '0');

        CacheUtils.putIntoCache(NotificationConstants.REGISTRATION_OTP_PREFIX + phoneNumber, otp, NotificationConstants.OTP_VALID_FOR_SECONDS);

        List<UserSmsDetails> userSmsDetails = new ArrayList<>();
        Map<String, String> userParameters = new HashMap<>();
        userParameters.put(NotificationConstants.TEMPLATE_OTP, otp);
        userParameters.put(NotificationConstants.TEMPLATE_USER, purposeText);
        userParameters.put(NotificationConstants.NAME_OF_APP, source);
        userSmsDetails.add(new UserSmsDetails(0L, userParameters, phoneNumber));
        sendSmsToIns(userSmsDetails, notificationTriggerDto, trigger.getEventName(), clientId);
    }

    @Override
    public ResponseEntity<Response<String>> updateNotification(Long notificationId, Boolean read, String status) {
        String endPoint = NotificationEndpoint.UPDATE_NOTIFICATION.getEndpointUrl();
        Map<String, Object> updateNotificationRequest = new HashMap<>();
        updateNotificationRequest.put(NotificationField.NOTIFICATION_ID.getFieldName(), notificationId);
        updateNotificationRequest.put(NotificationField.READ.getFieldName(), read);
        updateNotificationRequest.put(NotificationField.STATUS.getFieldName(), status);
        ResponseEntity<Response<String>> response = putExchange(endPoint, updateNotificationRequest, new ParameterizedTypeReference<Response<String>>() {});
        try {
            Utils.processResponseEntity(response, "Error in updating notification");
        } catch (Exception e) {
            LOGGER.info("[updateNotification] failed with error " + e.toString());
            Sentry.capture(e);
        }
        return response;
    }

    @Override
    public ResponseEntity<Response<List<Map<String, Object>>>> getEpisodeNotifications(List<Long> episodeIdList) {
        String endPoint = NotificationEndpoint.GET_NOTIFICATION.getEndpointUrl();
        EpisodeNotificationSearchRequest episodeNotificationSearchRequest = new EpisodeNotificationSearchRequest(episodeIdList);
        return postExchange(endPoint, episodeNotificationSearchRequest, new ParameterizedTypeReference<Response<List<Map<String, Object>>>>() {});
    }


    public void sendToIns(PNDtoWithNotificationData pushNotificationTemplateDtos, NotificationTriggerDto notificationTriggerDto, String eventName, Long clientId) {
        PushNotificationDetails pushNotificationDetails = new PushNotificationDetails(pushNotificationTemplateDtos.getPushNotificationTemplateDtoList(), notificationTriggerDto.getVendorId(), notificationTriggerDto.getTriggerId(), notificationTriggerDto.getTemplateId(), pushNotificationTemplateDtos.getSendNotificationData(), pushNotificationTemplateDtos.getNotificationExtraData(), pushNotificationTemplateDtos.getIsMandatory());
        GenericEvents<PushNotificationDetails> event = new GenericEvents<>(pushNotificationDetails, eventName);
        insHandler.sendNotificationToINS(Collections.singletonList(event), clientId);
    }

    @Override
    public void sendSmsToIns(List<UserSmsDetails> userSmsDetails, NotificationTriggerDto notificationTriggerDto, String eventName, Long clientId) {
        SmsDetails smsDetails = new SmsDetails(userSmsDetails, notificationTriggerDto.getVendorId(), notificationTriggerDto.getTriggerId(), notificationTriggerDto.getTemplateId(), notificationTriggerDto.getTemplateIds(), notificationTriggerDto.getMandatory(), notificationTriggerDto.getDefaultConsent(), notificationTriggerDto.getIsDefaultTime(), notificationTriggerDto.getTimeOfSms());
        GenericEvents<SmsDetails> event = new GenericEvents<>(smsDetails, eventName);
        insHandler.sendSmsToINS(Collections.singletonList(event), clientId);
    }

    @Override
    public ResponseEntity<Response<String>> deleteNotification(Map<String,Object> deleteUserRelationRequest) {
        String endPoint = NotificationEndpoint.DELETE_NOTIFICATION.getEndpointUrl();
        Map<String, Object> deleteNotificationRequest = new HashMap<>();
        deleteNotificationRequest.put(NotificationField.NOTIFICATION_EXTRA_KEYS.getFieldName(), deleteUserRelationRequest);
        ResponseEntity<Response<String>> response = deleteExchange(endPoint, new ParameterizedTypeReference<Response<String>>() {}, null, null, deleteNotificationRequest);
        try {
            Utils.processResponseEntity(response, "Error in delete notification");
        } catch (Exception e) {
            LOGGER.info("[deleteNotification] failed with error " + e.toString());
            Sentry.capture(e);
        }
        return response;
    }
    
    private void setAuthentication(Long clientId) {
        SecurityContext ctx = SecurityContextHolder.createEmptyContext();
        ctx.setAuthentication(new UsernamePasswordAuthenticationToken(clientService.getClient(clientId), null, Collections.emptyList()));
        SecurityContextHolder.setContext(ctx);
    }

    @Override
    public ResponseEntity<Response<String>> updateNotificationBulk(List<Long> episodeIdList) {
        String endPoint = NotificationEndpoint.UPDATE_NOTIFICATION_BULK.getEndpointUrl();
        Map<String, Object> updateNotificationBulkRequest = new HashMap<>();
        updateNotificationBulkRequest.put(NotificationField.EPISODE_ID_LIST.getFieldName(), episodeIdList);
        updateNotificationBulkRequest.put(NotificationField.READ.getFieldName(), true);
        updateNotificationBulkRequest.put(NotificationField.STATUS.getFieldName(), null);
        ResponseEntity<Response<String>> response = putExchange(endPoint, updateNotificationBulkRequest, new ParameterizedTypeReference<Response<String>>() {});
        try {
            Utils.processResponseEntity(response, "Error in update Notification Bulk notification");
        } catch (Exception e) {
            LOGGER.info("[updateNotificationBulk] failed with error " + e);
            Sentry.capture(e);
        }
        return response;
    }
    @Override
    public ResponseEntity<Response<EpisodeUnreadNotificationResponse>> getUnreadEpisodeNotifications(List<Long> episodeIdList, Boolean count) {
        String endPoint = NotificationEndpoint.GET_UNREAD_NOTIFICATION.getEndpointUrl();
        EpisodeUnreadNotificationSearchRequest episodeNotificationSearchRequest = new EpisodeUnreadNotificationSearchRequest(episodeIdList, count);
        return postExchange(endPoint, episodeNotificationSearchRequest, new ParameterizedTypeReference<Response<EpisodeUnreadNotificationResponse>>() {});
    }
}


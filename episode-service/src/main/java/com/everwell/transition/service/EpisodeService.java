package com.everwell.transition.service;

import com.everwell.transition.elasticsearch.indices.EpisodeIndex;
import com.everwell.transition.model.db.Episode;
import com.everwell.transition.model.dto.BeneficiaryDetailsDto;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.dto.notification.*;
import com.everwell.transition.model.dto.EpisodeUnifiedSearchRequest;
import com.everwell.transition.model.dto.EpisodeReportsRequest;
import com.everwell.transition.model.dto.EpisodeSearchResult;
import com.everwell.transition.model.dto.EpisodeUnifiedSearchRequest;
import com.everwell.transition.model.dto.EpisodeDocumentResponseDto;
import com.everwell.transition.model.dto.episode.EpisodeDetailsDto;
import com.everwell.transition.model.dto.notification.PNDtoWithNotificationData;
import com.everwell.transition.model.request.AddEpisodeDocumentRequest;
import com.everwell.transition.model.request.EpisodeDocumentSearchRequest;
import com.everwell.transition.model.request.episode.DeleteRequest;
import com.everwell.transition.model.request.episode.EpisodeBulkRequest;
import com.everwell.transition.model.request.episode.SearchEpisodesRequest;
import com.everwell.transition.model.request.episode.UpdateRiskRequest;
import com.everwell.transition.model.request.episodesearch.EpisodeSearchRequest;
import com.everwell.transition.model.request.tasklistfilters.EpisodeTasklistRequest;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.List;

public interface EpisodeService {

    EpisodeSearchResult elasticSearch(EpisodeSearchRequest searchReq);

    Episode getEpisode(Long id, Long clientId);

    Episode getEpisodeWithDeleted(Long episodeId, Long clientId);

    EpisodeDto getEpisodeDetails(Long id, Long clientId);

    Episode processCreateEpisode(Long clientId, Map<String, Object> requestInput);

    Episode processUpdateEpisode(Long clientId, Map<String, Object> requestInput);

    void processEpisodeDataWithStageTransitionHandler(Episode episode, EpisodeDetailsDto episodeDetailsDtoBeforeUpdate,
                                                      Map<String, Object> requestInput, Map<String, Map<String, Object>> processedRequestInput, Long clientId);

    Episode processUpdateEpisode(Long clientId, Map<String, Object> requestInput, boolean isWriteToElastic);

    Episode processEpisodeRequestInputForStageTransition(Long clientId, Map<String, Object> requestInput);

    Episode processEpisodeRequestInputForStageTransition(Long clientId, Map<String, Object> requestInput,boolean isWriteToElastic);

    EpisodeDto getEpisodeDetails(Long id, Long clientId, boolean stageLogsRequired, boolean tabsPermissionRequired, boolean fetchExternalData, boolean includeDeleted);

    Episode deleteEpisode(Long clientId, DeleteRequest request);

    EpisodeIndex saveToElastic(Long id, Long clientId);

    EpisodeIndex getEpisodeDocument(Long id, Long clientId);

    void updateElasticDocument(Long id, Long clientId, boolean isHierarchyDataUpdated);

    EpisodeSearchResult elasticSearch(EpisodeSearchRequest searchReq, boolean fetchFullDoc);

    Map<String, String> getFieldToTableTypeMap();

    Map<String, String> getFieldToTableTypeMap(String module);

    Map<String, Object> clientObjToElasticDoc(Map<String, Object> episodeObj);

    Map<String, Object> clientObjToElasticDoc(Map<String, Object> episodeObj, boolean onlyStageDate, String module);

    void convertAndSavetoElastic(Map<String, Object> episodeObj, Long clientId);

    void updateClientEpisodeToElastic(Map<String, Object> episodeObj, Long clientId, String module);

    List<EpisodeDto> getEpisodesForPersonListFromElastic(List<Long> personIdList, Long clientId);
    
    List<EpisodeDto> getEpisodesForPersonList(List<Long> personId, Long clientId, boolean includeDetails);

    List<Episode> getEpisodeByPhoneNumber(String phoneNumber);

    List<EpisodeDto> getDuplicateEpisodesForUser(Long clientId, Map<String, Object> fetchDuplicatesRequest);

    void processStageTransitionResult(Episode episode, Map<String, Object> stageTransitionResult);
    
    List<Long> search(SearchEpisodesRequest searchEpisodesRequest);

    void updateRiskStatus(UpdateRiskRequest updateRiskRequest);

    void updateValuesToElasticBulk(List<Long> episodeIds, Map<String, Object> valuesToUpdate);

    List<EpisodeDto> getEpisodesBulk(EpisodeBulkRequest episodeBulkRequest);

    PNDtoWithNotificationData getDoseReminderPNDtosForClientId(Long idHeader, String timeZone);

    Map<String, List<String>> getDeviceIdList(List<String> episodeIdList);

    List<String> getEpisodeIdListWhoHaveNotMarkedAdherenceForToday(List<String> episodeIdList, String currentDayStartTimeInUTC, String currentDayEndTimeInUTC);

    PNDtoWithNotificationData getRefillReminderPNDtosForClientId(Long idHeader, String timeZone);

    EpisodeSearchResult taskListSearch(EpisodeTasklistRequest episodeSearchRequest);

    EpisodeSearchResult episodeUnifiedSearch(EpisodeUnifiedSearchRequest episodeUnifiedSearchRequest);

    void uploadEpisodeDocument(List<MultipartFile> file, AddEpisodeDocumentRequest addEpisodeDocumentRequest) throws IOException;

    List<EpisodeDocumentResponseDto> getPersonDocumentDetails(Long personId, boolean fetchAll);

    PNDtoWithNotificationData getMorningDoseReminderPNDtosForClientIdAndSchedule(Long clientId, String timeZone);

    PNDtoWithNotificationData getAfternoonDoseReminderPNDtosForClientIdAndSchedule(Long clientId, String timeZone);

    PNDtoWithNotificationData getEveningDoseReminderPNDtosForClientIdAndSchedule(Long clientId, String timeZone);

    PNDtoWithNotificationData getNightDoseReminderPNDtosForClientIdAndSchedule(Long clientId, String timeZone);

    List<String> getEpisodeIdsWithClientIdAndScheduleTimeIndex(Long clientId, Integer index, String timeZone);

    PNDtoWithNotificationData getAddUserRelationPnDtos(Long clientId, EpisodeDto episodeDto, Map<String, Object> userRelationRequest);
    
    void updateOnTreatmentEpisodeEndDate(EpisodeDto episodeDto, Long addedBy, String reason, String comment, LocalDateTime endDate);

    List<UserSmsDetails> getEpisodesForTreatmentInitiation(NotificationTriggerEvent notificationTriggerEvent, String clientId);

    List<UserSmsDetails> getEpisodesForAdherenceReminder(NotificationTriggerEvent notificationTriggerEvent, String clientId);

    List<UserSmsDetails> getEpisodesForPostTreatmentReminder(NotificationTriggerEvent notificationTriggerEvent, String clientId);

    List<UserSmsDetails> getEpisodesForFollowUp(NotificationTriggerEvent notificationTriggerEvent, String clientId);

    List<UserSmsDetails> getEpisodesForEmptyContactTracing(NotificationTriggerEvent notificationTriggerEvent, String clientId);

    EmptyBankDetailsDto getEpisodesForEmptyBankDetails(EpisodeSearchRequest episodeSearchRequest);

    List<BeneficiaryDetailsDto> getEpisodesForBenefitCreated(EpisodeSearchRequest episodeSearchRequest);

    List<UserSmsDetails> getEpisodesForInvalidBankDetails(EpisodeSearchRequest episodeSearchRequest);
    
    EpisodeSearchResult episodeReports (EpisodeReportsRequest episodeReportsRequest);
    
    PNDtoWithNotificationData getAaroRefillReminderPNDTOsForClientId(Long clientId, String timeZone);

    PNDtoWithNotificationData getAaroRefillTodayReminderPNDTOs(Long clientId, String timeZone);
    
    PNDtoWithNotificationData getAppointmentConfirmationPNDto(Long clientId, EpisodeDto episodeDto, Map<String, Object> extraData);

    List<EpisodeDocumentResponseDto> getEpisodeDocumentDetails(EpisodeDocumentSearchRequest episodeDocumentSearchRequest);

    List<EpisodeDocumentResponseDto> getEpisodeDocumentDetailsByEpisodeDocumentDetailsId(Long episodeDocumentDetailsId);

    void updateEpisodeDocumentDetails(Long episodeDocumentDetailsId, Boolean approved, Long validatedBy);


}
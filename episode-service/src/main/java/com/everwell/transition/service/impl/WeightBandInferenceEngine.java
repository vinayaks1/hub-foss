package com.everwell.transition.service.impl;

import com.everwell.transition.model.Rule;
import com.everwell.transition.model.RuleNamespace;
import com.everwell.transition.model.response.dispensation.DispensationWeightBandResponse;
import com.everwell.transition.repositories.RulesRepository;
import com.everwell.transition.service.InferenceEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class WeightBandInferenceEngine extends InferenceEngine<Long, DispensationWeightBandResponse> {

    @Autowired
    private RulesRepository rulesRepository;

    @Override
    public Long resolveMissingData(Long input, List<Rule> rules) {
        return null;
    }

    @Override
    protected List<String> getRequiredFields(Rule rules) {
        return null;
    }

    @Override
    public String getRuleNamespace() {
        return RuleNamespace.WEIGHT_BAND.name();
    }

    @Override
    protected DispensationWeightBandResponse initializeOutputResult() {
        return new DispensationWeightBandResponse();
    }

    @Override
    public List<Rule> getRules(Long fromId, boolean shouldEvaluateAll, Long clientId) {
        return
                rulesRepository
                        .findAllByRuleNamespaceAndClientId(getRuleNamespace(), clientId)
                        .stream()
                        .map(r -> new Rule(
                                RuleNamespace.valueOf(r.getRuleNamespace()),
                                r.getId(), r.getCondition(), r.getAction(), r.getPriority(), r.getDescription())
                        ).collect(Collectors.toList());
    }


}

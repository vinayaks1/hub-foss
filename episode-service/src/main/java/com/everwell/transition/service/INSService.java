package com.everwell.transition.service;

import com.everwell.transition.model.dto.EngagementDto;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.request.EngagementRequest;
import com.everwell.transition.model.request.ins.NotificationLogRequest;
import com.everwell.transition.model.response.ins.TypeResponse;

import java.util.List;
import java.util.Map;

public interface INSService {

    List<Map<String, Object>> getEngagement(EpisodeDto episodeDto);

    EngagementDto saveEngagement(EngagementRequest engagementRequest, EpisodeDto episodeDto);

    TypeResponse getNotificationTypes();

    String addNotificationLog(NotificationLogRequest notificationLogRequest, Long clientId);

}

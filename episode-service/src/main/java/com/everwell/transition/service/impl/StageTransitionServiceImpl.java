package com.everwell.transition.service.impl;

import com.everwell.transition.model.dto.EventStreamingDto;
import com.everwell.transition.service.RuleEngine;
import com.everwell.transition.service.StageTransitionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class StageTransitionServiceImpl implements StageTransitionService {

    @Autowired
    RuleEngine ruleEngine;

    @Autowired
    private StageTransitionKeyValueInferenceEngine stageTransitionKeyValueInferenceEngine;

    public EventStreamingDto<Map<String, Object>> stageTransitionHandler(Map<String, Object> patientInput, Long clientId) {
        Long currentStageId = patientInput.get("currentStageId") == null ? null : Long.parseLong(String.valueOf(patientInput.get("currentStageId")));
        Map<String, Object> result = ruleEngine.run(stageTransitionKeyValueInferenceEngine, patientInput, currentStageId, false, patientInput, clientId);
        return new EventStreamingDto<>("#", result);
    }
}
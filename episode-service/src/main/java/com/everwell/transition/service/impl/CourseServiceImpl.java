package com.everwell.transition.service.impl;

import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.db.CourseCompletion;
import com.everwell.transition.model.db.CourseUrlMapping;
import com.everwell.transition.model.dto.CourseDto;
import com.everwell.transition.model.request.course.MarkCourseRequest;
import com.everwell.transition.model.response.CourseResponse;
import com.everwell.transition.model.response.MarkCourseResponse;
import com.everwell.transition.postconstruct.CourseUrlMappingList;
import com.everwell.transition.repositories.CourseCompletionRepository;
import com.everwell.transition.repositories.CourseRepository;
import com.everwell.transition.repositories.CourseUrlMappingRepository;
import com.everwell.transition.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CourseServiceImpl implements CourseService {


    @Autowired
    CourseCompletionRepository courseCompletionRepository;

    @Autowired
    CourseRepository courseRepository;

    @Autowired
    private CourseUrlMappingList courseUrlMappingList;

    @Override
    public CourseResponse getAllCourses(Long episodeId) {

        List<Long> completedCourses = courseCompletionRepository.getAllByEpisodeId(episodeId)
                                        .stream()
                                        .map(CourseCompletion::getCourseId).collect(Collectors.toList());

        List<CourseUrlMapping> courseInfo = courseUrlMappingList.getCourseUrlMappingList();
        List<CourseDto> courseDtos = courseInfo
                                        .stream()
                                        .map(e -> new CourseDto(e.getCourseId(),e.getLanguage(),e.getTitle(),e.getUrl(),completedCourses.contains(e.getCourseId())))
                                        .collect(Collectors.toList());

        return new CourseResponse(episodeId,courseDtos);
    }

    @Override
    public MarkCourseResponse markCourseAsComplete(MarkCourseRequest markCourseRequest) {
        if(markCourseRequest.getCourseId() == null)
            throw new ValidationException("courseId is invalid!");
        CourseCompletion courseCompletion = new CourseCompletion(markCourseRequest.getEpisodeId(),markCourseRequest.getCourseId(),true);
        courseCompletionRepository.save(courseCompletion);
        return new MarkCourseResponse(markCourseRequest.getEpisodeId(),markCourseRequest.getCourseId(),courseCompletion.getCreatedDate());
    }
}

package com.everwell.transition.service.impl;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.constants.LocaliseKeyConstants;
import com.everwell.transition.exceptions.InternalServerErrorException;
import com.everwell.transition.model.response.android.TranslationCacheObject;
import com.everwell.transition.service.LocaliseApiService;
import com.everwell.transition.service.LocaliseService;
import com.everwell.transition.utils.CacheUtils;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import io.sentry.Sentry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class LocaliseServiceImpl implements LocaliseService {

    private static Logger LOGGER = LoggerFactory.getLogger(LocaliseServiceImpl.class);

    @Autowired
    LocaliseApiService localiseApiService;

    @Override
    public Map<String, String> getTranslations(String language, String region) {
        Map<String, String> translationMapResponse = new HashMap<>();
        // we only cache keys which are mentioned in this map (this is across project)
        Set<String> requiredTranslationKeys = LocaliseKeyConstants.defaultTranslationMap.keySet();
        String cacheKey = Constants.TRANSLATION_CACHE_KEY_PREFIX + language + ":";
        List<String> translatedValues = CacheUtils.getFromCache(requiredTranslationKeys.stream().map(m -> cacheKey + m).collect(Collectors.toList()));
        if (Utils.isEmptyOrAnyNull(translatedValues) || translatedValues.size() != requiredTranslationKeys.size()) {
            Map<String, Object> translationMap = localiseApiService.getPatientAppTranslationsFromLocalise(language, region);
            requiredTranslationKeys.forEach(key -> {
                if (translationMap.containsKey(key)) {
                    String val = String.valueOf(translationMap.get(key));
                    if (!StringUtils.hasLength(val)) {
                        val = LocaliseKeyConstants.defaultTranslationMap.get(key);
                    }
                    // TO:DO bulk insert with TTL when keys size increase
                    CacheUtils.putIntoCache(cacheKey + key, Utils.asJsonString(new TranslationCacheObject(key, val)), 86400L);
                    translationMapResponse.put(key, val);
                }
            });
        } else {
            translatedValues.forEach(val -> {
                try {
                    TranslationCacheObject translationCacheObject = Utils.jsonToObject(val, new TypeReference<TranslationCacheObject>() {});
                    translationMapResponse.put(translationCacheObject.getKey(), translationCacheObject.getValue());
                } catch (IOException e) {
                    LOGGER.error(e.getMessage());
                    throw new InternalServerErrorException("[getTranslations] Unable to parse value received from cache");
                }
            });
        }
        return translationMapResponse;
    }

}

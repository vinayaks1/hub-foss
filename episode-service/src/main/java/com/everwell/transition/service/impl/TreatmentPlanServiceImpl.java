package com.everwell.transition.service.impl;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.constants.IAMConstants;
import com.everwell.transition.enums.AdherenceTypeMappingEnum;
import com.everwell.transition.enums.treamentPlan.TreatmentPlanValidation;
import com.everwell.transition.exceptions.NotFoundException;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.db.RegimenTreatmentPlanMap;
import com.everwell.transition.model.db.TreatmentPlan;
import com.everwell.transition.model.db.TreatmentPlanProductMap;
import com.everwell.transition.model.dto.IAMScheduleDto;
import com.everwell.transition.model.dto.treatmentPlan.*;
import com.everwell.transition.model.request.adherence.SearchAdherenceRequest;
import com.everwell.transition.model.request.treatmentPlan.*;
import com.everwell.transition.model.response.AdherenceResponse;
import com.everwell.transition.model.response.AllAdherenceResponse;
import com.everwell.transition.repositories.RegimenRepository;
import com.everwell.transition.repositories.RegimenTreatmentPlanMapRepository;
import com.everwell.transition.repositories.TreatmentPlanProductMapRepository;
import com.everwell.transition.repositories.TreatmentPlanRepository;
import com.everwell.transition.service.AdherenceService;
import com.everwell.transition.service.TreatmentPlanService;
import com.everwell.transition.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.time.*;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.everwell.transition.constants.Constants.EPISODE_SCHEDULE_TIME_TRUE_BIT;
import static com.everwell.transition.constants.IAMConstants.DEFAULT_DOSE_LIST;

@Service
public class TreatmentPlanServiceImpl implements TreatmentPlanService {

    @Autowired
    private TreatmentPlanRepository treatmentPlanRepository;

    @Autowired
    private RegimenTreatmentPlanMapRepository regimenTreatmentPlanMapRepository;

    @Autowired
    private TreatmentPlanProductMapRepository treatmentPlanProductMapRepository;

    @Autowired
    private RegimenRepository regimenRepository;

    @Autowired
    private AdherenceService adherenceService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public TreatmentPlan createPlan(AddTreatmentPlanRequest request, Long clientId) {
        TreatmentPlan treatmentPlan = new TreatmentPlan(clientId, request.getName());
        treatmentPlanRepository.save(treatmentPlan);
        return addToTreatmentPlan(request, clientId, treatmentPlan);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public TreatmentPlan addToTreatmentPlan(AddTreatmentPlanRequest request, Long clientId, TreatmentPlan treatmentPlan) {
        if (null != request.getRegimenId()) {
            regimenRepository.findById(request.getRegimenId())
                    .orElseThrow(() -> new NotFoundException("invalid regimen id"));
            TreatmentPlanConfig config = treatmentPlan.getConfig();
            config.setHasRegimenMapping(true);
            treatmentPlan.setConfig(config);
            RegimenTreatmentPlanMap regimenTreatmentPlanMap = new RegimenTreatmentPlanMap(request.getRegimenId(), treatmentPlan.getId());
            regimenTreatmentPlanMapRepository.save(regimenTreatmentPlanMap);
        }
        if (!CollectionUtils.isEmpty(request.getProductMap())) {
            List<TreatmentPlanProductMap> treatmentPlanProductMap = new ArrayList<>();
            LocalDateTime startDate = StringUtils.hasText(request.getStartDate()) ? Utils.convertStringToDateNew(request.getStartDate()) : LocalDateTime.now();
            Map<String, List<DoseDetails>> productIdToDoseDetails = new HashMap<>();
            // for backwards compatibility, when dose details is not being sent
            for (TreatmentPlanProductMapRequest mapRequest : request.getProductMap()) {
                if (CollectionUtils.isEmpty(mapRequest.getDoseDetails()) && (mapRequest.getSos() == null || !mapRequest.getSos())) {
                    String scheduleString = mapRequest.getSchedule();
                    List<DoseDetails> doseDetails = new ArrayList<>();
                    for (int i = 0; i < 4; i++) {
                        if (scheduleString.charAt(i) == EPISODE_SCHEDULE_TIME_TRUE_BIT) {
                            doseDetails.add(new DoseDetails(DEFAULT_DOSE_LIST.get(i), "{}", "1"));
                        }
                    }
                    mapRequest.setDoseDetails(doseDetails);
                }
            }
            for (TreatmentPlanProductMapRequest mapRequest : request.getProductMap()) {
                if (null != mapRequest.getProductId()) {
                    LocalDateTime refillDate = mapRequest.getFrequencyInDays() != null ? LocalDateTime.now().plusDays(mapRequest.getFrequencyInDays()) : null;
                    TreatmentPlanProductMap map = new TreatmentPlanProductMap(treatmentPlan.getId(), mapRequest.getProductId(), mapRequest.getSchedule(), startDate, null, refillDate, mapRequest.getOtherDrugName(), mapRequest.getOtherDosageForm(), mapRequest.getSos());
                    treatmentPlanProductMap.add(map);
                    String mapKey = String.valueOf(map.getProductId());
                    if (StringUtils.hasLength(mapRequest.getOtherDrugName()))
                        mapKey = mapRequest.getOtherDrugName();
                    productIdToDoseDetails.put(mapKey, mapRequest.getDoseDetails());
                }
            }
            treatmentPlanProductMapRepository.saveAll(treatmentPlanProductMap);
            // register on iam with the productMapId table's id
            treatmentPlanProductMap.forEach(map -> {
                IAMScheduleDto scheduleInfo = new IAMScheduleDto(IAMConstants.MULTI_FREQUENCY, IAMConstants.DAILY_SCHEDULE_STRING, IAMConstants.FIRST_DOSE_OFFSET, IAMConstants.DEFAULT_POSITIVE_SENSITIVITY, IAMConstants.DEFAULT_NEGATIVE_SENSITIVITY, adherenceService.getScheduleTypeConfig().get(IAMConstants.MULTI_FREQUENCY.toLowerCase()).longValue());
                String mapKey = String.valueOf(map.getProductId());
                if (StringUtils.hasLength(map.getOtherDrugName()))
                    mapKey = map.getOtherDrugName();
                Object doseDetails = productIdToDoseDetails.get(mapKey);
                if (map.getSos() != null && map.getSos()) {
                    scheduleInfo = new IAMScheduleDto(IAMConstants.DEFAULT_SCHEDULE_TYPE, IAMConstants.DAILY_SCHEDULE_STRING, IAMConstants.FIRST_DOSE_OFFSET, IAMConstants.DEFAULT_POSITIVE_SENSITIVITY, IAMConstants.DEFAULT_NEGATIVE_SENSITIVITY, adherenceService.getScheduleTypeConfig().get(IAMConstants.DAILY.toLowerCase()).longValue());
                    doseDetails = null;
                }
                adherenceService.registerEntity(
                        Constants.IAM_ACCESS_MAPPING_ENTITY_PREFIX_MED + "_" + map.getId(),
                        AdherenceTypeMappingEnum.NONE.getName(),
                        Constants.IAM_ACCESS_MAPPING_ENTITY_PREFIX_MED + "_" + map.getId(),
                        Utils.getFormattedDateNew(map.getStartDate()),
                        scheduleInfo,
                        doseDetails
                );
            });
            if (!treatmentPlanProductMap.isEmpty()) {
                TreatmentPlanConfig config = treatmentPlan.getConfig();
                config.setHasProductMapping(true);
                treatmentPlan.setConfig(config);
            }
        }
        treatmentPlanRepository.save(treatmentPlan);
        return treatmentPlan;
    }

    @Override
    public TreatmentPlanDto getPlanDetails(Long id, Long clientId){
        return getPlanDetails(id, clientId, null);
    }

    @Override
    public TreatmentPlanDto getPlanDetails(Long id, Long clientId, Boolean onlySos) {
        TreatmentPlan treatmentPlan = getTreatmentPlan(id, clientId);
        List<RegimenTreatmentPlanDto> regimenTreatmentPlanMapList = new ArrayList<>();
        if(treatmentPlan.getConfig().getHasRegimenMapping()) {
            regimenTreatmentPlanMapList = regimenTreatmentPlanMapRepository.findAllByTreatmentPlanId(treatmentPlan.getId())
                    .stream()
                    .map(m -> new RegimenTreatmentPlanDto(m.getCreatedDate(), m.getRegimenId()))
                    .collect(Collectors.toList());
        }
        Map<String, TreatmentPlanProductMapDto> treatmentPlanProductMapDtoMap = new HashMap<>();
        List<TreatmentPlanProductMapDto> filteredProductMapList = new ArrayList<>();
        AllAdherenceResponse allAdherenceResponse = null;
        if(treatmentPlan.getConfig().getHasProductMapping()) {
            treatmentPlanProductMapRepository.findAllByTreatmentPlanId(id)
                    .stream()
                    .filter(m -> onlySos == null || m.getSos() == onlySos)
                    .forEach(m -> {
                        TreatmentPlanProductMapDto treatmentPlanProductMapDto = new TreatmentPlanProductMapDto(m.getCreatedDate(), m.getStartDate(), m.getEndDate(), m.getProductId(), m.getSchedule(), m.getRefillDueDate(), m.getOtherDrugName(), m.getId(), m.getOtherDosageForm(), m.getSos());
                        treatmentPlanProductMapDtoMap.put(Constants.IAM_ACCESS_MAPPING_ENTITY_PREFIX_MED + "_" + m.getId(), treatmentPlanProductMapDto);
                    });
            if (!CollectionUtils.isEmpty(treatmentPlanProductMapDtoMap)) {
                allAdherenceResponse = adherenceService.getAdherenceBulk(new SearchAdherenceRequest(new ArrayList<>(treatmentPlanProductMapDtoMap.keySet()), null), true);
                for (AdherenceResponse adherenceResponse : allAdherenceResponse.getAdherenceResponseList()) {
                    TreatmentPlanProductMapDto productMap = treatmentPlanProductMapDtoMap.getOrDefault(adherenceResponse.getEntityId(), null);
                    if (productMap != null) {
                        productMap.setAdherenceString(adherenceResponse.getAdherenceString());
                        productMap.setDoseDetailsList(adherenceResponse.getDoseTimeList());
                    }
                }
                filteredProductMapList = treatmentPlanProductMapDtoMap.values().stream().filter(f -> !f.getStartDate().equals(f.getEndDate())).collect(Collectors.toList());
            }
        }
        return new TreatmentPlanDto(treatmentPlan.getId(), treatmentPlan.getName(), filteredProductMapList, regimenTreatmentPlanMapList, allAdherenceResponse != null ? allAdherenceResponse.getMergedAdherence() : "");
    }

    private TreatmentPlan getTreatmentPlan(Long id, Long clientId) {
        TreatmentPlan treatmentPlan = treatmentPlanRepository.findByIdAndClientId(id, clientId);
        if (null == treatmentPlan) {
            throw new NotFoundException("no treatment plan found with id : " + id);
        }
        return treatmentPlan;
    }

    @Override
    public Map<Long, String> getTreatmentPlanAggregatedScheduleStringMapForTreatmentPlanIds(List<Long> treatmentPlanIds, LocalDate date) {
        List<TreatmentPlanProductMap> treatmentPlanProductMaps = treatmentPlanProductMapRepository.findAllByTreatmentPlanIdIn(treatmentPlanIds)
                .stream()
                .filter(treatmentPlanProductMap -> {
                    if (null == date) return true;
                    return  ((null == treatmentPlanProductMap.getStartDate() || !treatmentPlanProductMap.getStartDate().toLocalDate().isAfter(date)) &&
                            (null == treatmentPlanProductMap.getEndDate() || date.isBefore(treatmentPlanProductMap.getEndDate().toLocalDate())));
                })
                .collect(Collectors.toList());
        Map<Long, String> treatmentPlanIdToAggregatedStringMap = new HashMap<>();
        for(TreatmentPlanProductMap treatmentPlanProductMap : treatmentPlanProductMaps) {
            treatmentPlanIdToAggregatedStringMap.putIfAbsent(treatmentPlanProductMap.getTreatmentPlanId(), Constants.DEFAULT_AGGREGATED_SCHEDULE);
            String currentSchedule = treatmentPlanIdToAggregatedStringMap.get(treatmentPlanProductMap.getTreatmentPlanId());
            treatmentPlanIdToAggregatedStringMap.put(treatmentPlanProductMap.getTreatmentPlanId(), getAggregatedSchedule(currentSchedule, treatmentPlanProductMap.getSchedule().split(" ")[0]));
        }
        return treatmentPlanIdToAggregatedStringMap;
    }

    public List<TreatmentPlanProductMap> getActiveTreatmentPlanList(Long treatmentPlanId) {
        return  treatmentPlanProductMapRepository.findAllByTreatmentPlanId(treatmentPlanId)
                .stream()
                .filter(i -> null == i.getEndDate())
                .collect(Collectors.toList());
    }

    public TreatmentPlanDoseTimeDto getDoseTimeCountMapForTreatmentPlanProductMaps(List<TreatmentPlanProductMap> treatmentPlanProductMaps) {
        Map<String, Integer> doseTimeToDoseCountMap = new HashMap<>();
        Set<String> doseTimeSet = new HashSet<>();
        for (TreatmentPlanProductMap treatmentPlanProductMap : treatmentPlanProductMaps) {
            String scheduleString = treatmentPlanProductMap.getSchedule().split(" ")[0];
            for (int i = 0 ; i < 4; i++) {
                String currentDoseTime = DEFAULT_DOSE_LIST.get(i);
                int currentCount = doseTimeToDoseCountMap.getOrDefault(currentDoseTime,  0);
                if (scheduleString.charAt(i) == EPISODE_SCHEDULE_TIME_TRUE_BIT) {
                    currentCount += 1;
                    doseTimeSet.add(currentDoseTime);
                }
                doseTimeToDoseCountMap.put(currentDoseTime, currentCount);
            }
        }
        return new TreatmentPlanDoseTimeDto(doseTimeSet, doseTimeToDoseCountMap);
    }

    @Override
    public List<TreatmentPlanProductMapDto> processAddProductMappingsForTreatmentPlan(Long treatmentPlanId, Long episodeId, Long clientId, EditTreatmentPlanRequest editTreatmentPlanRequest) {
        List<TreatmentPlanProductMapDto> treatmentPlanProductMapDtoList = new ArrayList<>();
        LocalDateTime startDate = StringUtils.hasText(editTreatmentPlanRequest.getStartDate()) ? Utils.convertStringToDateNew(editTreatmentPlanRequest.getStartDate()) : LocalDateTime.now().plusDays(1);
        TreatmentPlan treatmentPlan = getTreatmentPlan(treatmentPlanId, clientId);
        if (Boolean.TRUE.equals(treatmentPlan.getConfig().getHasProductMapping())) {
            List<TreatmentPlanProductMap> existingTreatmentPlanProductMapList = getActiveTreatmentPlanList(treatmentPlanId);
            AddTreatmentPlanRequest treatmentPlanRequest = new AddTreatmentPlanRequest();
            List<TreatmentPlanProductMapRequest> treatmentPlanProductMapRequest = new ArrayList<>();
            List<TreatmentPlanProductMap> treatmentPlanProductMapList = new ArrayList<>();
            editTreatmentPlanRequest.getProductMap().forEach(mapRequest -> {
                Optional<TreatmentPlanProductMap> existingTreatmentPlanProductMap = existingTreatmentPlanProductMapList.stream()
                        .filter(existingTreatmentPlan ->
                                Objects.equals(existingTreatmentPlan.getId(), mapRequest.getProductId())
                                        && Objects.equals(existingTreatmentPlan.getOtherDrugName() != null ? existingTreatmentPlan.getOtherDrugName().toLowerCase() : "",
                                        mapRequest.getOtherDrugName() != null ? mapRequest.getOtherDrugName().toLowerCase() : "")
                        ).findFirst();
                LocalDateTime refillDueDate = mapRequest.getFrequencyInDays() != null
                        ? LocalDateTime.now().plusDays(mapRequest.getFrequencyInDays())
                        : null;
                if (!existingTreatmentPlanProductMap.isPresent()) {
                    treatmentPlanProductMapRequest.add(new TreatmentPlanProductMapRequest(
                            mapRequest.getProductId(),
                            mapRequest.getProductName(),
                            mapRequest.getSchedule(),
                            mapRequest.getFrequencyInDays(),
                            mapRequest.getOtherDrugName(),
                            mapRequest.getTreatmentPlanProductMapId(),
                            mapRequest.getDoseDetails(),
                            mapRequest.getOtherDosageForm(),
                            mapRequest.getSos()
                    ));
                    treatmentPlanProductMapDtoList.add(new TreatmentPlanProductMapDto(Utils.getCurrentDateNew(), startDate, null,
                            mapRequest.getProductId(), mapRequest.getProductName(), mapRequest.getSchedule(), refillDueDate, null, null, mapRequest.getOtherDrugName(), null, mapRequest.getOtherDosageForm(), mapRequest.getSos()));
                } else {
                    String dayStartTime = editTreatmentPlanRequest.getDayStartTime() == null ? Utils.getFormattedDateNew(startDate, "yyyy-MM-dd HH:mm:ss") : editTreatmentPlanRequest.getDayStartTime();
                    TreatmentPlanProductMap treatmentPlanProductMap = existingTreatmentPlanProductMap.get();
                    if (!CollectionUtils.isEmpty(mapRequest.getDoseDetails())) {
                        adherenceService.updateIamSchedule(mapRequest.getDoseDetails(), dayStartTime, Constants.IAM_ACCESS_MAPPING_ENTITY_PREFIX_MED + "_" + treatmentPlanProductMap.getId(), clientId);
                    }
                    treatmentPlanProductMap.setRefillDueDate(refillDueDate);
                    treatmentPlanProductMap.setOtherDosageForm(mapRequest.getOtherDosageForm());
                    treatmentPlanProductMapList.add(treatmentPlanProductMap);
                    treatmentPlanProductMapDtoList.add(new TreatmentPlanProductMapDto(Utils.getCurrentDateNew(), startDate, null,
                            mapRequest.getProductId(), mapRequest.getProductName(), mapRequest.getSchedule(), refillDueDate, null, null, mapRequest.getOtherDrugName(), treatmentPlanProductMap.getId(), mapRequest.getOtherDosageForm(), mapRequest.getSos()));
                }
            });
            if (!CollectionUtils.isEmpty(treatmentPlanProductMapRequest)) {
                treatmentPlanRequest.setProductMap(treatmentPlanProductMapRequest);
                // during editing start date sent = T + 1
                String dayStartTime = editTreatmentPlanRequest.getDayStartTime() == null ? Utils.getFormattedDateNew(startDate, "yyyy-MM-dd HH:mm:ss") : editTreatmentPlanRequest.getDayStartTime();
                treatmentPlanRequest.setStartDate(dayStartTime);
                addToTreatmentPlan(treatmentPlanRequest, clientId, treatmentPlan);
            }
            if (!CollectionUtils.isEmpty(treatmentPlanProductMapList)) {
                treatmentPlanProductMapRepository.saveAll(treatmentPlanProductMapList);
            }
        }
        return treatmentPlanProductMapDtoList;
    }

    @Override
    public List<TreatmentPlanProductMapDto> processDeleteProductMappingsForTreatmentPlan(Long treatmentPlanId, Long episodeId, Long clientId, DeleteProductsMappingRequest deleteProductsMappingRequest) {
        List<TreatmentPlanProductMapDto> treatmentPlanProductMapDtoList = new ArrayList<>();
        LocalDateTime endDate = StringUtils.hasText(deleteProductsMappingRequest.getEndDate()) ? Utils.convertStringToDateNew(deleteProductsMappingRequest.getEndDate()).plusDays(-1) : LocalDateTime.now();
        TreatmentPlan treatmentPlan = getTreatmentPlan(treatmentPlanId, clientId);
        if (Boolean.TRUE.equals(treatmentPlan.getConfig().getHasProductMapping())) {
            List<TreatmentPlanProductMap> existingTreatmentPlanProductMapList = getActiveTreatmentPlanList(treatmentPlanId);
            Map<Long, TreatmentPlanProductMapRequest> toDeleteProductIdToProductNameMap = deleteProductsMappingRequest.getProductMap()
                    .stream()
                    .collect(Collectors.toMap(TreatmentPlanProductMapRequest::getTreatmentPlanProductMapId, Function.identity(), (u, v) -> u));
            List<TreatmentPlanProductMap> sameDayStartEndDateIds = new ArrayList<>();
            List<TreatmentPlanProductMap> toDeleteTreatmentPlanProductMapList = new ArrayList<>();
            existingTreatmentPlanProductMapList.forEach(treatmentPlanProductMap -> {
                if (toDeleteProductIdToProductNameMap.containsKey(treatmentPlanProductMap.getId())) {
                    if (treatmentPlanProductMap.getStartDate().equals(endDate)) {
                        sameDayStartEndDateIds.add(treatmentPlanProductMap);
                    }
                    treatmentPlanProductMap.setEndDate(endDate);
                    treatmentPlanProductMap.setRefillDueDate(null);
                    toDeleteTreatmentPlanProductMapList.add(treatmentPlanProductMap);
                    treatmentPlanProductMapDtoList.add(new TreatmentPlanProductMapDto(treatmentPlanProductMap.getCreatedDate(), treatmentPlanProductMap.getStartDate(), treatmentPlanProductMap.getEndDate(),
                            treatmentPlanProductMap.getProductId(), toDeleteProductIdToProductNameMap.get(treatmentPlanProductMap.getId()).getProductName(), treatmentPlanProductMap.getSchedule(), null, null, null, treatmentPlanProductMap.getOtherDrugName(), treatmentPlanProductMap.getId(), treatmentPlanProductMap.getOtherDosageForm(), treatmentPlanProductMap.getSos()));
                }
            });
            if (toDeleteTreatmentPlanProductMapList.isEmpty()) {
                throw new ValidationException(TreatmentPlanValidation.PRODUCT_LIST_INVALID_OR_DOES_NOT_EXIST.getMessage());
            }
            toDeleteTreatmentPlanProductMapList.forEach(treatmentPlanProductMap -> {
                String dayStartTime = deleteProductsMappingRequest.getDayStartTime() == null ? Utils.getFormattedDateNew(endDate, "yyyy-MM-dd HH:mm:ss") : deleteProductsMappingRequest.getDayStartTime();
                adherenceService.deleteEntity(Constants.IAM_ACCESS_MAPPING_ENTITY_PREFIX_MED + "_" + treatmentPlanProductMap.getId(), dayStartTime, false, false);
                treatmentPlanProductMap.setRefillDueDate(null);
                treatmentPlanProductMapRepository.save(treatmentPlanProductMap);
            });
            treatmentPlanProductMapRepository.saveAll(toDeleteTreatmentPlanProductMapList);
//            treatmentPlanProductMapRepository.deleteAll(sameDayStartEndDateIds); // hard delete products added then deleted on same day
        }
        return treatmentPlanProductMapDtoList;
    }

    @Override
    public List<TreatmentPlanProductMap> getMedicinesForSchedule(List<String> scheduleTime, Long treatmentPlanId) {
        List<Integer> scheduleTimeIndexList = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            if (scheduleTime.contains(DEFAULT_DOSE_LIST.get(i)))
                scheduleTimeIndexList.add(i);
        }
        List<TreatmentPlanProductMap> treatmentPlanProductMap = treatmentPlanProductMapRepository.findAllByTreatmentPlanId(treatmentPlanId);
        List<TreatmentPlanProductMap> medsForSchedule = new ArrayList<>();
        for (TreatmentPlanProductMap productMap : treatmentPlanProductMap) {
            String scheduleString = productMap.getSchedule().split(" ")[0];
            for (Integer idx : scheduleTimeIndexList) {
                if (scheduleString.charAt(idx) == EPISODE_SCHEDULE_TIME_TRUE_BIT)
                    medsForSchedule.add(productMap);
            }
        }
        return medsForSchedule;
    }

    private String getAggregatedSchedule(String currentEpisodeSchedule, String newProductSchedule) {
        StringBuilder builder = new StringBuilder(currentEpisodeSchedule);
        for (int i =0; i < 4; i++) {
            if (Constants.EPISODE_SCHEDULE_TIME_FALSE_BIT == builder.charAt(i) && Constants.PRODUCT_SCHEDULE_TIME_NONE_BIT != newProductSchedule.charAt(i)) {
                builder.setCharAt(i, Constants.EPISODE_SCHEDULE_TIME_TRUE_BIT);
            }
        }
        return builder.toString();
    }

    @Override
    public List<String> getDoseTimesForTreatmentPlan(Long treatmentPlanId) {
        Map<Long, String> treatmentPlanIdScheduleMap = getTreatmentPlanAggregatedScheduleStringMapForTreatmentPlanIds(Collections.singletonList(treatmentPlanId), null);
        String scheduleString = treatmentPlanIdScheduleMap.getOrDefault(treatmentPlanId, null);
        List<String> doseTimeList = new ArrayList<>();
        if (null != scheduleString) {
            for (int i = 0; i < 4; i++) {
                if (scheduleString.charAt(i) == EPISODE_SCHEDULE_TIME_TRUE_BIT) {
                    doseTimeList.add(DEFAULT_DOSE_LIST.get(i));
                }
            }
        }
        return doseTimeList;
    }

    @Override
    public void validateMedicine(Long treatmentPlanId, List<Long> medicineIdList) {
        List<TreatmentPlanProductMap> treatmentPlanProductMapList = treatmentPlanProductMapRepository.findAllById(medicineIdList);
        treatmentPlanProductMapList.forEach(map -> {
            if (!Objects.equals(map.getTreatmentPlanId(), treatmentPlanId))
                throw new ValidationException("Permission denied to delete medicine!");
        });
    }

    @Override
    public List<TreatmentPlanProductMap> getTreatmentPlanProductMapList(Long treatmentPlanId) {
        return treatmentPlanProductMapRepository
                .findAllByTreatmentPlanId(treatmentPlanId);
    }
}
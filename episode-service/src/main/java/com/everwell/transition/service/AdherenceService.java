package com.everwell.transition.service;

import com.everwell.transition.model.db.EpisodeTag;
import com.everwell.transition.model.dto.AdherenceMonthWiseData;
import com.everwell.transition.model.dto.IAMScheduleDto;
import com.everwell.transition.model.request.adherence.EditDoseBulkRequest;
import com.everwell.transition.model.dto.episode.EpisodeTransitionDetails;
import com.everwell.transition.model.request.adherence.EditDosesRequest;
import com.everwell.transition.model.request.adherence.SearchAdherenceRequest;
import com.everwell.transition.model.request.treatmentPlan.DoseDetails;
import com.everwell.transition.model.response.*;
import com.everwell.transition.model.response.dispensation.EntityDispensationResponse;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Map;

public interface AdherenceService {

    List<AdherenceMonthWiseData> formatAdherenceStringMonthWise(AdherenceResponse data, EpisodeTagResponse tagResponse, ZoneId timezoneId);

    Map<LocalDateTime, List<EpisodeTag>> getTagsByDate(EpisodeTagResponse tagResponse, ZoneId timezoneId);

    List<AdherenceCodeConfigResponse> getAdherenceConfig();

    Map<String, List<LocalDateTime>> editDosesBulk(EditDoseBulkRequest editDoseBulkRequest);

    Map<String, Integer> getMonitoringMethodConfig();

    Map<String, Integer> getScheduleTypeConfig();

    List<LocalDateTime> editDoses(EditDosesRequest editDosesRequest);

    List<LocalDateTime> validateDispensation(List<String> dates, Long episodeId, EntityDispensationResponse entityDispensationResponse);

    void updateIamSchedule(List<DoseDetails> doseTimeList, String startDate, String entityId, Long clientId);

    Boolean registerEntity(String entityId, String monitoringMethod, String uniqueIdentifier, String startDate, IAMScheduleDto scheduleInfo, Object doseDetails);

    AllAdherenceResponse getAdherenceBulk(SearchAdherenceRequest adherenceRequest, Boolean merge);

    ResponseEntity<Response<Map<String, Object>>> deleteEntity(String entityId, String endDate, Boolean updateEndDate, Boolean deleteIfRequired);

    void registerEntity(Long episodeId, Map<String, Object> requestInput, Map<String, Object> processedRequestInput, Map<String, Object> stageTransitionResult);

    void updateEntity(EpisodeTransitionDetails episodeTransitionDetails, Map<String, Object> requestInput, Map<String, Object> processedRequestInput, Map<String, Object> stageTransitionResult);

    Map<String, Object> getAdherenceDetails(Long episodeId);
}

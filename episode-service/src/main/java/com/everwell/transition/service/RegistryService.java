package com.everwell.transition.service;

import com.everwell.transition.model.request.appointment.StaffMapRequest;
import com.everwell.transition.model.response.HierarchyResponseData;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.response.appointment.AppointmentStaffMap;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

public interface RegistryService {

    ResponseEntity<Response<HierarchyResponseData>> getHierarchyById(Long hId, boolean includeAssociations);

    List<Long> getAllLevelIdsForHierarchy(Long id, boolean includeAssociations);

    AppointmentStaffMap addAppointmentStaffMap(StaffMapRequest appointmentStaffMap);

}

package com.everwell.transition.service.impl;

import com.everwell.transition.constants.EpisodeLogConstants;
import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.constants.INSConstants;
import com.everwell.transition.enums.episode.EpisodeField;
import com.everwell.transition.enums.ins.INSEndpoint;
import com.everwell.transition.model.db.Trigger;
import com.everwell.transition.model.dto.EngagementDto;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.request.EngagementRequest;
import com.everwell.transition.model.request.episodelogs.EpisodeLogDataRequest;
import com.everwell.transition.model.request.ins.INSEngagementRequest;
import com.everwell.transition.model.request.ins.NotificationLogRequest;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.response.ins.TypeResponse;
import com.everwell.transition.postconstruct.INSTypeMap;
import com.everwell.transition.repositories.TriggerRepository;
import com.everwell.transition.service.INSService;
import com.everwell.transition.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class INSServiceImpl extends DataGatewayHelperServiceImpl implements INSService {

    @Autowired
    private INSTypeMap insTypeMap;

    @Autowired
    TriggerRepository triggerRepository;


    @Override
    public List<Map<String, Object>> getEngagement(EpisodeDto episodeDto) {
        if (null == episodeDto.getStageData().getOrDefault(EpisodeField.ENGAGEMENT_EXISTS.getFieldName(), null)) {
            return null;
        } else {
            ResponseEntity<Response<List<Map<String, Object>>>> insResponse = getExchange(INSEndpoint.GET_ENGAGEMENT_BULK.getEndpointUrl(), new ParameterizedTypeReference<Response<List<Map<String, Object>>>>() {}, episodeDto.getId(), null);
            return Utils.processResponseEntity(insResponse, "Error from INS to Fetch Engagement for entity Id " + episodeDto.getId());
        }
    }

    @Override
    public EngagementDto saveEngagement(EngagementRequest engagementRequest, EpisodeDto episodeDto) {
        List<INSEngagementRequest> insEngagementRequest = Collections.singletonList(new INSEngagementRequest(String.valueOf(engagementRequest.getEpisodeId()), engagementRequest.getReceiveSms(), engagementRequest.getTimeOfDay(), engagementRequest.getLanguageId(), insTypeMap.notificationTypeMap.get(INSConstants.TYPE_SMS)));
        Map<String, Object> insRequest = new HashMap<>();
        insRequest.put(INSConstants.FIELD_ENGAGEMENT_LIST, insEngagementRequest);
        List<EpisodeLogDataRequest> episodeLogDataRequest;
        if (null == episodeDto.getStageData().getOrDefault(EpisodeField.ENGAGEMENT_EXISTS.getFieldName(), null)) {
            addEngagement(insRequest);
            episodeLogDataRequest = Collections.singletonList(new EpisodeLogDataRequest(engagementRequest.getEpisodeId(), EpisodeLogConstants.ENGAGEMENT_UPDATE_CATEGORY, engagementRequest.getUserId(), EpisodeLogConstants.ADDED_PATIENT_ENGAGEMENT, EpisodeLogConstants.ADDED_PATIENT_CONSENT + engagementRequest.getReceiveSms()));
        } else {
            updateEngagement(insRequest);
            episodeLogDataRequest = Collections.singletonList(new EpisodeLogDataRequest(engagementRequest.getEpisodeId(), EpisodeLogConstants.ENGAGEMENT_UPDATE_CATEGORY, engagementRequest.getUserId(), EpisodeLogConstants.CHANGED_PATIENT_ENGAGEMENT, EpisodeLogConstants.CHANGED_PATIENT_CONSENT + engagementRequest.getReceiveSms()));
        }
        Map<String, Object> updateInput = new HashMap<>();
        if (null != engagementRequest.getHouseholdVisits()) {
            updateInput.put(EpisodeField.HOUSEHOLD_VISITS.getFieldName(), engagementRequest.getHouseholdVisits());
        }
        if (null != engagementRequest.getCallCenterCalls()) {
            updateInput.put(EpisodeField.CALL_CENTER_CALLS.getFieldName(), engagementRequest.getCallCenterCalls());
        }
        updateInput.put(FieldConstants.EPISODE_FIELD_EPISODE_ID, engagementRequest.getEpisodeId());
        updateInput.put(EpisodeField.ENGAGEMENT_EXISTS.getFieldName(), true);
        return new EngagementDto(updateInput, episodeLogDataRequest);
    }

    public void addEngagement(Map<String, Object> insRequest) {
       ResponseEntity<Response<Void>> insResponse = postExchange(INSEndpoint.SAVE_ENGAGEMENT_BULK.getEndpointUrl(), insRequest, new ParameterizedTypeReference<Response<Void>>() {});
       Utils.processResponseEntity(insResponse, "Unable to add engagement for request " + insRequest);
    }

    public void updateEngagement(Map<String, Object> insRequest) {
        ResponseEntity<Response<Void>> insResponse = putExchange(INSEndpoint.SAVE_ENGAGEMENT_BULK.getEndpointUrl(), insRequest, new ParameterizedTypeReference<Response<Void>>() {});
        Utils.processResponseEntity(insResponse, "Unable to update engagement for request " + insRequest);
    }

    @Override
    public TypeResponse getNotificationTypes() {
        ResponseEntity<Response<TypeResponse>> insResponse = getExchange(INSEndpoint.GET_NOTIFICATION_TYPES.getEndpointUrl(), new ParameterizedTypeReference<Response<TypeResponse>>() {}, null, null);
        return Utils.processResponseEntity(insResponse, "Unable to fetch notification types");
    }

    @Override
    public String addNotificationLog(NotificationLogRequest notificationLogRequest, Long clientId) {
        Trigger trigger = triggerRepository.getTriggerByEventNameAndClientId(notificationLogRequest.getEventName(), clientId);
        if (trigger != null) {
            notificationLogRequest.getNotifications().forEach(notification -> {
                notification.put(INSConstants.VENDOR_ID, trigger.getVendorId());
                notification.put(INSConstants.TRIGGER_ID, trigger.getTriggerId());
                notification.put(INSConstants.TEMPLATE_ID, trigger.getDefaultTemplateId());
            });
        }
        ResponseEntity<Response<String>> insResponse = postExchange(INSEndpoint.ADD_NOTIFICATION_LOGS.getEndpointUrl(), notificationLogRequest, new ParameterizedTypeReference<Response<String>>() {});
        return Utils.processResponseEntity(insResponse, "Unable to add PN logs");
    }
}

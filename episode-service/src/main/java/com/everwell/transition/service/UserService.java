package com.everwell.transition.service;

import com.everwell.transition.model.dto.DiseaseStageFieldMapDto;
import com.everwell.transition.model.dto.FieldDataDto;
import com.everwell.transition.model.response.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;
import java.util.Map;


public interface UserService {

    ResponseEntity<Response<List<Map<String, Object>>>> getByPrimaryPhone(String phoneNumber);

    List<Long> fetchDuplicates(Map<String, Object> fetchDuplicatesRequest);

    Map<String, Object> getUserDetails(Long personId);

    Long createUser(Map<String, Object> processedRequestMap);

    void updateUser(Map<String, Object> processedRequestInput, Long personId);

    void deleteUser(Map<String, Object> deleteUserMap, Long personId);
    
    <T> T processResponseEntity(ResponseEntity<Response<T>> responseEntity, String errorResponse);

    void addUserRelation(Map<String, Object> userRelationRequest);

    ResponseEntity<Response<List<Map<String, Object>>>> getUserRelations(Long userId, String status);

    void deleteUserRelation(Map<String, Object> userRelationRequest);

    void updateUserRelation(Map<String, Object> userRelationRequest);

    ResponseEntity<Response<List<Map<String, Object>>>> getUserRelationsPrimaryPhone(String phoneNumber, Long primaryUserId);

    ResponseEntity<Response<List<Map<String, Object>>>> getAllVitals();

    ResponseEntity<Response<Map<String, Object>>> getVitalsById(Long vitalId);

    ResponseEntity<Response<List<Map<String, Object>>>> saveVitals(Map<String, Object> vitalBulkRequest);

    ResponseEntity<Response<Map<String, Object>>> updateVitalsById(Long vitalId, Map<String, Object> vitalBulkRequest);

    ResponseEntity<Response<String>> deleteVitalsById(Long vitalId);

    ResponseEntity<Response<List<Map<String, Object>>>> getVitalsGoalByUserId(Long userId);

    ResponseEntity<Response<String>> saveUserVitalsGoal(Long userId, Map<String, Object> vitalGoalRequest);

    ResponseEntity<Response<String>> updateUserVitalsGoal(Long userId, Map<String, Object> vitalGoalRequest);

    ResponseEntity<Response<String>> deleteUserVitalsGoal(Long userId);

    ResponseEntity<Response<Map<String, Object>>> getUserVitalsDetails(Map<String, Object> vitalDetailsRequest);

    ResponseEntity<Response<String>> saveUserVitalsDetails(Long userId, Map<String, Object> addVitalDetailsRequest);

}
package com.everwell.transition.service.impl;

import com.everwell.transition.constants.EpisodeLogConstants;
import com.everwell.transition.elasticsearch.indices.EpisodeIndex;
import com.everwell.transition.enums.dispensation.DispensationEndpoint;
import com.everwell.transition.enums.dispensation.DispensationStatus;
import com.everwell.transition.enums.dispensation.DispensationRangeFilterType;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.dto.dispensation.DispensationData;
import com.everwell.transition.model.dto.dispensation.DispensationRangeValueFilters;
import com.everwell.transition.model.dto.dispensation.ProductStockData;
import com.everwell.transition.model.dto.treatmentPlan.TreatmentPlanDto;
import com.everwell.transition.model.dto.treatmentPlan.TreatmentPlanProductMapDto;
import com.everwell.transition.model.request.dispensation.*;
import com.everwell.transition.model.response.ProductResponseWithScheduleDto;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.response.dispensation.*;
import com.everwell.transition.service.DispensationService;
import com.everwell.transition.service.EpisodeService;
import com.everwell.transition.service.RuleEngine;
import com.everwell.transition.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;
import java.util.stream.Collectors;


@Service
public class DispensationServiceImpl extends DataGatewayHelperServiceImpl implements DispensationService {

    @Autowired
    private EpisodeService episodeService;

    @Autowired
    private RuleEngine ruleEngine;

    @Autowired
    private WeightBandInferenceEngine weightBandInferenceEngine;

    @Override
    public ResponseEntity<Response<List<ProductResponse>>> searchProducts(ProductSearchRequest productSearchRequest)  {
        ResponseEntity<Response<List<ProductResponse>>> response;
        String endPoint;
        if(null != productSearchRequest.getEpisodeId()) {
            endPoint = DispensationEndpoint.GET_ALL_PRODUCTS.getEndpointUrl();
            response = getExchange(endPoint, new ParameterizedTypeReference<Response<List<ProductResponse>>>(){}, null, Collections.EMPTY_MAP);
        } else {
            endPoint = DispensationEndpoint.SEARCH_PRODUCTS.getEndpointUrl();
            response = postExchange(endPoint, productSearchRequest, new ParameterizedTypeReference<Response<List<ProductResponse>>>() {});
        }
        return response;
    }

    @Override
    public ResponseEntity<Response<List<ProductStockData>>> productStockSearch(ProductStockSearchRequest productStockSearchRequest) {
        String endPoint = DispensationEndpoint.PRODUCT_STOCK_SEARCH.getEndpointUrl();
        return postExchange(endPoint, productStockSearchRequest, new ParameterizedTypeReference<Response<List<ProductStockData>>>() {});
    }

    @Override
    public ResponseEntity<Response<EntityDispensationResponse>> getAllDispensationDataForEntity(Long entityId, boolean includeProductLog) {
        String endPoint = DispensationEndpoint.DISPENSATION_ENTITY.getEndpointUrl();
        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("includeProductLog", includeProductLog);
        return getExchange(endPoint, new ParameterizedTypeReference<Response<EntityDispensationResponse>>() {}, entityId, queryParams);
    }

    @Override
    public ResponseEntity<Response<DispensationData>> getDispensation(Long id) {
        String endPoint = DispensationEndpoint.GET_DISPENSATION.getEndpointUrl();
        return getExchange(endPoint, new ParameterizedTypeReference<Response<DispensationData>>() {}, id, null);
    }

    @Override
    public ResponseEntity<Response<DispensationResponse>> createDispensation(DispensationRequest dispensationRequest) {
        String endPoint = DispensationEndpoint.CREATE_DISPENSATION.getEndpointUrl();
        return postExchange(endPoint, dispensationRequest, new ParameterizedTypeReference<Response<DispensationResponse>>() {});
    }

    @Override
    public ResponseEntity<Response<DispensationResponse>> createDispensationInventory(DispensationRequest dispensationRequest) {
        String endPoint = DispensationEndpoint.CREATE_DISPENSATION_INVENTORY.getEndpointUrl();
        return postExchange(endPoint, dispensationRequest, new ParameterizedTypeReference<Response<DispensationResponse>>() {});
    }

    @Override
    public ResponseEntity<Response<DispensationResponse>> deleteDispensation(Long id) {
        String endPoint = DispensationEndpoint.DELETE_DISPENSATION.getEndpointUrl();
        return deleteExchange(endPoint, new ParameterizedTypeReference<Response<DispensationResponse>>() {}, id, null, null);
    }

    @Override
    public ResponseEntity<Response<List<EntityDispensationResponse>>> searchDispensationsForEntity(SearchDispensationRequest searchDispensationRequest) {
        String endPoint = DispensationEndpoint.DISPENSATION_ENTITY_SEARCH.getEndpointUrl();
        List<CompletableFuture<ResponseEntity<Response<List<EntityDispensationResponse>>>>> allFutures = new ArrayList<>();
        List<List<Long>> entityIdListBathes = Utils.getBatches(searchDispensationRequest.getEntityIds(), 1000);
        entityIdListBathes.forEach(f -> {
            SearchDispensationRequest batchDispensationRequest = new SearchDispensationRequest(f, searchDispensationRequest.getFilters());
            allFutures.add(
                    CompletableFuture.supplyAsync(() -> postExchange(endPoint, batchDispensationRequest, new ParameterizedTypeReference<Response<List<EntityDispensationResponse>>>() {}))
            );
        });
        CompletableFuture.allOf(allFutures.toArray(new CompletableFuture[0])).join();
        List<EntityDispensationResponse> dispensationRequest = new ArrayList<>();
        for (int i = 0; i < entityIdListBathes.size(); i++) {
            try {
                ResponseEntity<Response<List<EntityDispensationResponse>>> partialResponse = allFutures.get(i).get();
                if (partialResponse.getBody() != null && partialResponse.getBody().isSuccess()) {
                    dispensationRequest.addAll(partialResponse.getBody().getData());
                }
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        return new ResponseEntity<>(new Response<>(true, dispensationRequest), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Response<ReturnDispensationResponse>> returnDispensation(ReturnDispensationRequest returnDispensationRequest) {
        String endPoint = DispensationEndpoint.RETURN_DISPENSATION.getEndpointUrl();
        return postExchange(endPoint, returnDispensationRequest, new ParameterizedTypeReference<Response<ReturnDispensationResponse>>() {});
    }

    @Override
    public ResponseEntity<Response<ReturnDispensationResponse>> returnDispensationInventory(ReturnDispensationRequest returnDispensationRequest) {
        String endPoint = DispensationEndpoint.RETURN_DISPENSATION_INVENTORY.getEndpointUrl();
        return postExchange(endPoint, returnDispensationRequest, new ParameterizedTypeReference<Response<ReturnDispensationResponse>>() {});
    }

    @Override
    public ResponseEntity<Response<ProductResponse>> addProduct(ProductRequest productRequest) {
        String endPoint = DispensationEndpoint.ADD_PRODUCT.getEndpointUrl();
        return postExchange(endPoint, productRequest, new ParameterizedTypeReference<Response<ProductResponse>>() {});
    }

    @Override
    public ResponseEntity<Response<List<ProductResponse>>> addProductBulk(ProductBulkRequest productBulkRequest) {
        String endPoint = DispensationEndpoint.ADD_PRODUCT_BULK.getEndpointUrl();
        return postExchange(endPoint, productBulkRequest,new ParameterizedTypeReference<Response<List<ProductResponse>>>() {});
    }

    @Override
    public ResponseEntity<Response<ProductResponse>> getProduct(Long id) {
        String endPoint = DispensationEndpoint.GET_PRODUCT.getEndpointUrl();
        return getExchange(endPoint, new ParameterizedTypeReference<Response<ProductResponse>>() {}, id, null);
    }

    @Override
    public ResponseEntity<Response<Void>> stockCredit(StockRequest stockRequest) {
        String endPoint = DispensationEndpoint.STOCK_CREDIT.getEndpointUrl();
        return postExchange(endPoint, stockRequest, new ParameterizedTypeReference<Response<Void>>() {});
    }

    @Override
    public ResponseEntity<Response<Void>> stockDebit(StockRequest stockRequest) {
        String endPoint = DispensationEndpoint.STOCK_DEBIT.getEndpointUrl();
        return putExchange(endPoint, stockRequest, new ParameterizedTypeReference<Response<Void>>() {});
    }

    @Override
    public DispensationWeightBandResponse getWeightBandForEpisode(Long episodeId) {
        Long clientId = clientService.getClientByTokenOrDefault().getId();
        EpisodeDto episodeDto = episodeService.getEpisodeDetails(episodeId, clientId);
        return ruleEngine.run(weightBandInferenceEngine, episodeDto.getDiseaseId(), null, true, episodeDto, clientId);
    }

    @Override
    public Map<Long, String> getEpisodeIdsWithRefillDueDate(RefillDatesRequest refillDatesRequest) {
        List<DispensationRangeValueFilters> rangeValueFilters = new ArrayList<>();
        rangeValueFilters.add(new DispensationRangeValueFilters(DispensationRangeFilterType.NEXT_REFILL_DATE, refillDatesRequest.getFromDate(), refillDatesRequest.getToDate(), 0L));
        rangeValueFilters.add(new DispensationRangeValueFilters(DispensationRangeFilterType.NUMBER_OF_DISPENSATIONS, null, null, 0L));
        SearchDispensationRequest searchDispensationRequest = new SearchDispensationRequest(refillDatesRequest.getEpisodeIds(), rangeValueFilters);
        ResponseEntity<Response<List<EntityDispensationResponse>>> response = searchDispensationsForEntity(searchDispensationRequest);
        List<EntityDispensationResponse> entityDispensationResponseList = Utils.processResponseEntity(response, "Something went wrong while fetching the dispensation details for the entities. Please try after some time");
        Map<Long, String> entityIdRefillDateMap = new HashMap<>();
        entityDispensationResponseList.forEach(e -> {
            if (!StringUtils.isEmpty(e.getRefillDate())) {
                entityIdRefillDateMap.put(e.getEntityId(), e.getRefillDate());
            } else if (CollectionUtils.isEmpty(e.getDispensations())) {
                entityIdRefillDateMap.put(e.getEntityId(), "");
            }
        });
        return entityIdRefillDateMap;
    }
    @Override
    public ResponseEntity<Response<Void>> stockUpdate(Map<String,Object> stockRequest) {
        String endPoint = DispensationEndpoint.STOCK_UPDATE.getEndpointUrl();
        return putExchange(endPoint, stockRequest, new ParameterizedTypeReference<Response<Void>>() {});
    }

    public ResponseEntity<Response<DispensationResponse>> createDispensationEpisode(DispensationRequest dispensationRequest, EpisodeDto episodeDto) {
        ResponseEntity<Response<DispensationResponse>> response;
        if (dispensationRequest.getInventoryEnabled()) {
            response = createDispensationInventory(dispensationRequest);
        } else {
            response = createDispensation(dispensationRequest);
        }
        //Update Patient End Date if Required
        Long dispensationId =  Utils.processResponseEntity(response, "Unable to add dispensation").getDispensationId();
        if (dispensationRequest.getExtendEndDate()) {
            LocalDateTime dosingStartDate = Utils.convertStringToDateNew(dispensationRequest.getDosingStartDate());
            LocalDateTime nextRefill = dosingStartDate.plusDays(dispensationRequest.getDrugDispensedForDays());
            LocalDateTime endDate = nextRefill.minusDays(1);
            if (endDate.isAfter(episodeDto.getEndDate())) {
                String reason = "Prescriptions dispensed for days greater than existing End Date";
                String comments = String.format(EpisodeLogConstants.EPISODE_DISPENSATION_ADD_COMMENT, dispensationId.toString(), dispensationRequest.getAddedBy().toString());
                episodeService.updateOnTreatmentEpisodeEndDate(episodeDto, dispensationRequest.getAddedBy(), reason, comments, endDate);
            }
        }
        return response;
    }

    @Override
    public ResponseEntity<Response<EntityDispensationResponse>> getEpisodeDispensationData(Long dispensationId) {
        String endPoint = String.format(DispensationEndpoint.SEARCH_DISPENSATION_ENTITY.getEndpointUrl(), dispensationId);
        return getExchange(endPoint, new ParameterizedTypeReference<Response<EntityDispensationResponse>>() {
        }, null, null);
    }

    @Override
    public List<ProductResponseWithScheduleDto> getProductWithSchedulesFromTreatmentPlan(TreatmentPlanDto treatmentDetails) {
        List<ProductResponseWithScheduleDto> productResponseWithScheduleDtoList = new ArrayList<>();
        if (null != treatmentDetails && !CollectionUtils.isEmpty(treatmentDetails.getMappedProducts())) {
            ProductSearchRequest searchRequest = new ProductSearchRequest(
                    treatmentDetails.getMappedProducts()
                            .stream()
                            .map(TreatmentPlanProductMapDto::getId)
                            .collect(Collectors.toList()), null, null, null);
            List<ProductResponse> productResponse = searchProducts(searchRequest).getBody().getData();
            List<TreatmentPlanProductMapDto> treatmentPlanProductMapDtoMap = treatmentDetails.getMappedProducts();

            treatmentPlanProductMapDtoMap.forEach(treatmentPlanProductMapDto -> {
                Optional<ProductResponse> product = productResponse.stream()
                        .filter(p -> p.getProductId().toString().equals(treatmentPlanProductMapDto.getId().toString())).findFirst();
                if(product.isPresent()) {
                    if(StringUtils.hasLength(treatmentPlanProductMapDto.getOtherDrugName())) {
                        treatmentPlanProductMapDto.setName(treatmentPlanProductMapDto.getOtherDrugName());
                        product.get().setProductName(treatmentPlanProductMapDto.getOtherDrugName());
                    }

                    ProductResponseWithScheduleDto withScheduleDto = new ProductResponseWithScheduleDto(product.get(),
                            treatmentPlanProductMapDto.getSchedule(),
                            treatmentPlanProductMapDto.getRefillDueDate(),
                            treatmentPlanProductMapDto);

                    productResponseWithScheduleDtoList.add(withScheduleDto);
                }
            });
        }
        return productResponseWithScheduleDtoList;
    }
}

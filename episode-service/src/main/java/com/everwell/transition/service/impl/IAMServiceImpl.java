package com.everwell.transition.service.impl;

import com.everwell.transition.constants.IAMConstants;
import com.everwell.transition.enums.iam.IAMEndpoint;
import com.everwell.transition.enums.iam.IAMField;
import com.everwell.transition.exceptions.InternalServerErrorException;
import com.everwell.transition.model.dto.IAMScheduleDto;
import com.everwell.transition.model.request.adherence.*;
import com.everwell.transition.model.request.adherence.EntityRequest;
import com.everwell.transition.model.request.adherence.RecordAdherenceRequest;
import com.everwell.transition.model.request.adherence.SearchAdherenceRequest;
import com.everwell.transition.model.response.AdherenceCodeConfigResponse;
import com.everwell.transition.model.response.AdherenceResponse;
import com.everwell.transition.model.response.AllAdherenceResponse;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.response.adherence.AllAvgAdherenceResponse;
import com.everwell.transition.model.response.adherence.PositiveCountResponse;
import com.everwell.transition.model.response.adherence.AdherenceGlobalConfigResponse;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.IAMService;
import com.everwell.transition.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;


@Service
public class IAMServiceImpl extends DataGatewayHelperServiceImpl implements IAMService {

    @Autowired
    private ClientService clientService;

    @Autowired
    @Qualifier("episodeTaskExecutor")
    TaskExecutor taskExecutor;

    @Override
    public ResponseEntity<Response<List<AdherenceCodeConfigResponse>>> getAdherenceConfig() {
        String endPoint = IAMEndpoint.ADHERENCE_CONFIG.getEndpointUrl();
        return getExchange(endPoint, new ParameterizedTypeReference<Response<List<AdherenceCodeConfigResponse>>>() {}, null, null);
    }

    @Override
    public ResponseEntity<Response<AdherenceGlobalConfigResponse>> getAdherenceGlobalConfig(Map<String, Object> configRequest) {
        String endPoint = IAMEndpoint.ADHERENCE_GLOBAL_CONFIG.getEndpointUrl();
        return postExchange(endPoint, configRequest, new ParameterizedTypeReference<Response<AdherenceGlobalConfigResponse>>() {});
    }

    @Override
    public ResponseEntity<Response<AdherenceResponse>> getAdherence(Long episodeId, Boolean logsRequired) {
        return getAdherence(String.valueOf(episodeId), logsRequired);
    }

    @Override
    public ResponseEntity<Response<AdherenceResponse>> getAdherence(String iamEntityId, Boolean logsRequired) {
        String endPoint = IAMEndpoint.GET_ADHERENCE.getEndpointUrl();
        Map<String,Object> queryParams = new HashMap<>();
        queryParams.put("entityId", iamEntityId);
        queryParams.put("logsRequired", logsRequired);
        return getExchange(endPoint, new ParameterizedTypeReference<Response<AdherenceResponse>>() {}, null, queryParams);
    }

    @Override
    public ResponseEntity<Response<AllAdherenceResponse>> getAdherenceBulk(SearchAdherenceRequest adherenceRequest) {
        return getAdherenceBulk(adherenceRequest, false);
    }

    @Override
    public ResponseEntity<Response<AllAdherenceResponse>> getAdherenceBulk(SearchAdherenceRequest adherenceRequest, Boolean merge) {
        String endPoint = IAMEndpoint.GET_ADHERENCE_BULK.getEndpointUrl() + (Boolean.TRUE.equals(merge) ? "?merge=true" : "");
        List<CompletableFuture<ResponseEntity<Response<AllAdherenceResponse>>>> allFutures = new ArrayList<>();
        //2500 is derived from extensive testing, please test any new number before changing
        List<List<String>> entityIdListBathes = Utils.getBatches(adherenceRequest.getEntityIdList(), 2500);
        entityIdListBathes.forEach(f -> {
            SearchAdherenceRequest batchAdherenceRequest = new SearchAdherenceRequest(f, adherenceRequest.getFilters());
            allFutures.add(
                CompletableFuture.supplyAsync(() -> postExchange(endPoint, batchAdherenceRequest, new ParameterizedTypeReference<Response<AllAdherenceResponse>>() {}), taskExecutor)
            );
        });
        CompletableFuture.allOf(allFutures.toArray(new CompletableFuture[0])).join();
        AllAdherenceResponse adherenceResponse = new AllAdherenceResponse(new ArrayList<>());
        for (int i = 0; i < entityIdListBathes.size(); i++) {
            try {
                ResponseEntity<Response<AllAdherenceResponse>> partialResponse = allFutures.get(i).get();
                if (partialResponse.getBody() != null && partialResponse.getBody().isSuccess()) {
                    adherenceResponse.getAdherenceResponseList().addAll(partialResponse.getBody().getData().getAdherenceResponseList());
                    adherenceResponse.setMergedAdherence(partialResponse.getBody().getData().getMergedAdherence());
                }
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        return new ResponseEntity<>(new Response<>(true, adherenceResponse), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Response<Void>> recordAdherenceForMultipleDates(RecordAdherenceRequest recordAdherenceRequest) {
        String endPoint = IAMEndpoint.RECORD_ADHERENCE.getEndpointUrl();
        ResponseEntity<Response<Void>> response = postExchange(endPoint, recordAdherenceRequest, new ParameterizedTypeReference<Response<Void>>() {});
        return response;
    }

    @Override
    public ResponseEntity<Response<Void>> recordAdherenceBulk(RecordAdherenceBulkRequest recordAdherenceBulkRequest) {
        String endPoint = IAMEndpoint.RECORD_ADHERENCE_BULK.getEndpointUrl();
        ResponseEntity<Response<Void>> response = postExchange(endPoint, recordAdherenceBulkRequest, new ParameterizedTypeReference<Response<Void>>() {});
        return response;
    }

    @Override
    public Boolean registerEntity(String entityId, String monitoringMethod, String uniqueIdentifier, String startDate, IAMScheduleDto scheduleInfo, Object doseDetails){
        String endPoint = "/iam/v1/entity";
        Integer adherenceMapping = 5;
        EntityRequest entityRequest = new EntityRequest(entityId, adherenceMapping, entityId, startDate, scheduleInfo.getScheduleTypeId(), scheduleInfo.scheduleString, scheduleInfo.positiveSensitivity, scheduleInfo.negativeSensitivity, scheduleInfo.firstDoseOffset, clientService.getClientByTokenOrDefault().getId(), doseDetails);
        ResponseEntity<Response<Void>> response = postExchange(endPoint, entityRequest, new ParameterizedTypeReference<Response<Void>>() {});
        return response.getBody().isSuccess();
    }

    @Override
    public ResponseEntity<Response<Map<String, Object>>> registerEntity(EntityRequest entityRequest) {
        String endPoint = IAMEndpoint.REGISTER_ENTITY.getEndpointUrl();
        return postExchange(endPoint, entityRequest, new ParameterizedTypeReference<Response<Map<String, Object>>>() {});
    }

    @Override
    public ResponseEntity<Response<Map<String, Object>>> updateEntity(EntityRequest entityRequest) {
        String endPoint = IAMEndpoint.UPDATE_ENTITY.getEndpointUrl();
        return putExchange(endPoint, entityRequest, new ParameterizedTypeReference<Response<Map<String, Object>>>() {});
    }

    @Override
    public ResponseEntity<Response<Map<String, Object>>> deleteEntity(String entityId) {
        return deleteEntity(entityId, Utils.getFormattedDateNew(Utils.getCurrentDateNew()), true, true);
    }

    @Override
    public ResponseEntity<Response<Map<String, Object>>> deleteEntity(String entityId, String endDate, Boolean updateEndDate, Boolean deleteIfRequired) {
        String endpoint = IAMEndpoint.DELETE_ENTITY.getEndpointUrl();
        Map<String, Object> queryParams = new HashMap<String, Object>(){
            {
                put("entityId", entityId);
                put("endDate", endDate);
                put("updateEndDate", updateEndDate);
                put("deleteIfRequired", deleteIfRequired);
            }
        };
        ResponseEntity<Response<Map<String, Object>>> response = deleteExchange(endpoint, new ParameterizedTypeReference<Response<String>>() {}, null, queryParams, null);

        if (!response.getStatusCode().equals(HttpStatus.OK)) {
            throw new InternalServerErrorException("Error while deleting entity");
        }

        return response;
    }

    @Override
    public ResponseEntity<Response<PositiveCountResponse>> getPositiveEventCount(PositiveAdherenceRequest positiveAdherenceRequest) {
        String endPoint = IAMEndpoint.POSITIVE_EVENT.getEndpointUrl();
        List<CompletableFuture<ResponseEntity<Response<PositiveCountResponse>>>> completableFutureList = new ArrayList<>();
        List<List<String>> entityIdListBatches = Utils.getBatches(positiveAdherenceRequest.getEntityIdList(), IAMConstants.BATCH_SIZE_GET_POSITIVE_COUNT);
        entityIdListBatches.forEach(f -> {
            PositiveAdherenceRequest batchPositiveAdherenceRequest = new PositiveAdherenceRequest(f);
            completableFutureList.add(
                    CompletableFuture.supplyAsync(() -> postExchange(endPoint, batchPositiveAdherenceRequest, new ParameterizedTypeReference<Response<PositiveCountResponse>>() {}))
            );
        });
        CompletableFuture.allOf(completableFutureList.toArray(new CompletableFuture[0])).join();
        PositiveCountResponse positiveCountResponse = new PositiveCountResponse(0L, 0L, 0L);
        for (int i = 0; i < entityIdListBatches.size(); i++) {
            try {
                ResponseEntity<Response<PositiveCountResponse>> partialResponse = completableFutureList.get(i).get();
                if (partialResponse.getBody() != null && partialResponse.getBody().isSuccess())
                {
                    PositiveCountResponse batchPositiveCountResponse = partialResponse.getBody().getData();
                    positiveCountResponse.setDigitalCount(positiveCountResponse.getDigitalCount() + batchPositiveCountResponse.getDigitalCount());
                    positiveCountResponse.setManualCount(positiveCountResponse.getManualCount() + batchPositiveCountResponse.getManualCount());
                    positiveCountResponse.setMissedCount(positiveCountResponse.getMissedCount() + batchPositiveCountResponse.getMissedCount());
                }
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        return new ResponseEntity<>(new Response<>(positiveCountResponse), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Response<AllAvgAdherenceResponse>> getAverageAdherence(AverageAdherenceRequest averageAdherenceRequest) {
        String endPoint = IAMEndpoint.AVERAGE_ADHERENCE.getEndpointUrl();
        List<CompletableFuture<ResponseEntity<Response<AllAvgAdherenceResponse>>>> completableFutureList = new ArrayList<>();
        List<List<String>> entityIdListBatches = Utils.getBatches(averageAdherenceRequest.getEntityIdList(), IAMConstants.BATCH_SIZE_GET_AVERAGE_ADHERENCE);
        entityIdListBatches.forEach(f -> {
            AverageAdherenceRequest batchAverageAdherenceRequest= new AverageAdherenceRequest(f, averageAdherenceRequest.isPatientDetailsRequired());
            completableFutureList.add(
                    CompletableFuture.supplyAsync(() -> postExchange(endPoint, batchAverageAdherenceRequest, new ParameterizedTypeReference<Response<AllAvgAdherenceResponse>>() {}))
            );
        });
        CompletableFuture.allOf(completableFutureList.toArray(new CompletableFuture[0])).join();
        AllAvgAdherenceResponse allAvgAdherenceResponse = new AllAvgAdherenceResponse(0L, 0L);
        for (int i = 0; i < entityIdListBatches.size(); i++) {
            try {
                ResponseEntity<Response<AllAvgAdherenceResponse>> partialResponse = completableFutureList.get(i).get();
                if (partialResponse.getBody() != null && partialResponse.getBody().isSuccess())
                {
                    AllAvgAdherenceResponse batchAllAverageAdherenceResponse = partialResponse.getBody().getData();
                    allAvgAdherenceResponse.setTotalAdherence(allAvgAdherenceResponse.getTotalAdherence() + batchAllAverageAdherenceResponse.getTotalAdherence());
                    allAvgAdherenceResponse.setDigitalAdherence(allAvgAdherenceResponse.getDigitalAdherence() + batchAllAverageAdherenceResponse.getDigitalAdherence());
                }
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        return new ResponseEntity<>(new Response<>(allAvgAdherenceResponse), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Response<List<Map<String, Object>>>> getImei(String entityId, boolean active) {
        String endPoint = IAMEndpoint.IMEI.getEndpointUrl();
        Map<String,Object> queryParams = new HashMap<>();
        queryParams.put(IAMField.ENTITY_ID.getFieldName(), String.valueOf(entityId));
        queryParams.put(IAMField.ACTIVE.getFieldName(), String.valueOf(active));
        return getExchange(endPoint,  new ParameterizedTypeReference<Response<List<Map<String, Object>>>>() {}, null, queryParams);
    }

    @Override
    public ResponseEntity<Response<List<Map<String, Object>>>> getImeiBulk(Map<String, Object> imeiRequest) {
        String endPoint = IAMEndpoint.IMEI_BULK.getEndpointUrl();
        return postExchange(endPoint, imeiRequest, new ParameterizedTypeReference<Response<List<Map<String, Object>>>>() {});
    }

    @Override
    public ResponseEntity<Response<List<Map<String, Object>>>> getActiveEntityFromImei(String imei) {
        String endPoint = IAMEndpoint.IMEI_ENTITY.getEndpointUrl();
        Map<String,Object> queryParams = new HashMap<>();
        queryParams.put(IAMField.IMEI.getFieldName(), String.valueOf(imei));
        return getExchange(endPoint, new ParameterizedTypeReference<Response<List<Map<String, Object>>>>() {}, null, queryParams);
    }

    @Override
    public ResponseEntity<Response<Map<String, Object>>> updateVideoStatus(Map<String, Object> reviewAdherenceRequest) {
        String endPoint = IAMEndpoint.VOT_VIDEO_STATUS.getEndpointUrl();
        return putExchange(endPoint, reviewAdherenceRequest, new ParameterizedTypeReference<Response<Map<String, Object>>>() {});
    }

    @Override
    public ResponseEntity<Response<Long>> reallocateIMEI(Map<String, Object> imeiEditRequest) {
        String endPoint = IAMEndpoint.IMEI.getEndpointUrl();
        return putExchange(endPoint, imeiEditRequest, new ParameterizedTypeReference<Response<Long>>() {});
    }

    @Override
    public ResponseEntity<Response<String>> editPhone(Map<String, Object> phoneEditDto) {
        String endPoint = IAMEndpoint.EDIT_PHONE.getEndpointUrl();
        return putExchange(endPoint, phoneEditDto, new ParameterizedTypeReference<Response<String>>() {});
    }

    public ResponseEntity<Response<String>> closeCase(Long episodeId, String endDate, Boolean deleteIfRequired, Boolean updateEndDate)
    {
        String endPoint = IAMEndpoint.CLOSE_ENTITY.getEndpointUrl();
        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put(IAMField.ENTITY_ID.getFieldName(), episodeId);
        queryParams.put(IAMField.END_DATE.getFieldName(), endDate);
        queryParams.put(IAMField.DELETE_IF_REQUIRED.getFieldName(), deleteIfRequired);
        queryParams.put(IAMField.UPDATE_END_DATE.getFieldName(), updateEndDate);
        return deleteExchange(endPoint, new ParameterizedTypeReference<Response<String>>() {}, null, queryParams, null);
    }

    @Override
    public ResponseEntity<Response<Map<String, Object>>> reopenEntity(EntityRequest entityRequest) {
        String endPoint = IAMEndpoint.ENTITY_REOPEN.getEndpointUrl();
        return putExchange(endPoint, entityRequest, new ParameterizedTypeReference<Response<Map<String, Object>>>() {});
    }
}

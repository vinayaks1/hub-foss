package com.everwell.transition.service;

import com.everwell.transition.model.db.EpisodeLog;
import com.everwell.transition.model.request.adherence.EditDosesRequest;
import com.everwell.transition.model.request.episode.DeleteRequest;
import com.everwell.transition.model.request.episodelogs.EpisodeLogDataRequest;
import com.everwell.transition.model.request.episodelogs.EpisodeLogDeletionRequest;
import com.everwell.transition.model.request.episodelogs.SearchLogsRequest;
import com.everwell.transition.model.request.episodelogs.*;
import com.everwell.transition.model.response.EpisodeLogResponse;
import com.everwell.transition.model.response.LogsConfigResponse;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

public interface EpisodeLogService {

    EpisodeLogResponse getEpisodeLogs(Long episodeId);
    List<EpisodeLogResponse> save(List<EpisodeLogDataRequest> episodeLogRequest);
    List<EpisodeLogResponse> search(SearchLogsRequest searchLogsRequest);
    void delete(EpisodeLogDeletionRequest episodeLogDeletionRequest);
    List<LogsConfigResponse> getEpisodeLogsConfig();
    List<EpisodeLogResponse> groupLogs(List<EpisodeLog> logList);
    void syncLogs(Long episodeId, String endDate);
    EpisodeLog addAdherenceLogs(EditDosesRequest editDosesRequest, List<LocalDateTime> dates, ZoneId zoneId);
    EpisodeLog addEpisodeDeletionLog(DeleteRequest episodeDeleteRequest);

    /**
     * Update existing logs
     * @param episodeLogRequest - request payload
     */
    void updateSupportActions(UpdateSupportActionsRequest episodeLogRequest);
}

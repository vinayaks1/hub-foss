package com.everwell.transition.service.impl;

import com.everwell.transition.service.LocaliseApiService;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
public class LocaliseApiServiceImpl implements LocaliseApiService {

    public RestTemplate restTemplate = new RestTemplate();

    @Override
    public Map<String, Object> getPatientAppTranslationsFromLocalise(String language, String region) {
        Map<String, Object> translations = new HashMap<>();
        int retryCount = 3;
        String endPoint = "https://localise.biz/api/export/locale/"+language+"-"+region+".json?fallback=en";
        boolean retry = false;
        do {
            MultiValueMap<String, String> headers = new HttpHeaders();
            headers.add("Authorization", "authtoken");
            HttpEntity<Map<String, Object>> requestEntity = new HttpEntity<>(null, headers);
            try {
                ResponseEntity<Map<String, Object>> response = restTemplate.exchange(
                        endPoint,
                        HttpMethod.GET,
                        requestEntity,
                        new ParameterizedTypeReference<Map<String, Object>>() {}
                );
                if (response.getStatusCode() == HttpStatus.OK) {
                    translations = response.getBody();
                }
                retry = false;
            } catch (HttpClientErrorException ex) {
                retry = true;
                retryCount--;
            }
        } while (retry && retryCount > 0);
        return translations;
    }

}

package com.everwell.transition.service.impl;

import com.everwell.transition.constants.LocaliseKeyConstants;
import com.everwell.transition.model.dto.wix.WixPostFilter;
import com.everwell.transition.model.request.wix.WixPostResponse;
import com.everwell.transition.model.response.android.GenericUiResponse;
import com.everwell.transition.service.AndroidConfigService;
import com.everwell.transition.service.LocaliseService;
import com.everwell.transition.service.WixApiIntegrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class AndroidConfigServiceImpl implements AndroidConfigService {

    @Autowired
    WixApiIntegrationService wixApiIntegrationService;

    @Autowired
    LocaliseService localiseService;

    @Override
    public List<GenericUiResponse> getFitFeed(String language, String region) {
        Map<String, String> translationMap = localiseService.getTranslations(language, region);
        int order = 1;
        Map<String, String> categoriesFilter = new HashMap<>();
        categoriesFilter.put("language", language);
        List<GenericUiResponse> uiResponse = new ArrayList<>();
        List<WixPostResponse> featuredPosts = wixApiIntegrationService.listPosts(new WixPostFilter(true, null, language));
        // add featured
        if (!CollectionUtils.isEmpty(featuredPosts)) {
            List<GenericUiResponse.AndroidItem> listItems = featuredPosts.stream().map(m -> new GenericUiResponse.AndroidItem(m.getImageUrl(), m.getTitle(), "", m.getPostUrl())).collect(Collectors.toList());
            uiResponse.add(new GenericUiResponse(translationMap.get(LocaliseKeyConstants.trending), "", order++, "", "3x2", "recyclerView", "h", listItems, "s", "left", true, "m", true));
        }
        // fetch categories
        Map<String, WixPostResponse> categories = wixApiIntegrationService.listCategoriesFilter(categoriesFilter);
        // add category wise
        for (Map.Entry<String, WixPostResponse> entry : categories.entrySet()) {
            String key = entry.getKey();
            WixPostResponse val = entry.getValue();
            WixPostFilter wixPostFilter = new WixPostFilter(false, Arrays.asList(key), language);
            List<WixPostResponse> posts = wixApiIntegrationService.listPosts(wixPostFilter);
            if (!CollectionUtils.isEmpty(posts)) {
                List<GenericUiResponse.AndroidItem> listItems = posts.stream().map(m -> new GenericUiResponse.AndroidItem(m.getImageUrl(), m.getTitle(), "", m.getPostUrl())).collect(Collectors.toList());
                uiResponse.add(new GenericUiResponse(val.getCategory(), "", order++, "", "3x2", "recyclerView", "h", listItems, "s", "left", true, "m", true));
            }
        }
        // get categories
        List<GenericUiResponse.AndroidItem> categoryItems = categories.values().stream().map(m -> new GenericUiResponse.AndroidItem(m.getImageUrl() != null ? m.getImageUrl().replace("/media/media", "/media") : null, m.getCategory(), "", m.getPostUrl())).collect(Collectors.toList());
        uiResponse.add(new GenericUiResponse(translationMap.get(LocaliseKeyConstants.explore_categories), "", order++, "", "2x3", "recyclerView", "h", categoryItems, "s", "left", true, "m", true));
        return uiResponse;
    }

    @Override
    public List<GenericUiResponse> getHomeFeed(String language, String region) {
        Map<String, String> translationMap = localiseService.getTranslations(language, region);
        List<GenericUiResponse> uiResponse = new ArrayList<>();
        int order = 1;
        uiResponse.add(new GenericUiResponse(order++, "weather"));
        uiResponse.add(new GenericUiResponse(order++, "lung-menu"));
        Map<String, String> categoriesFilter = new HashMap<>();
        categoriesFilter.put("language", language);
        Map<String, WixPostResponse> categories = wixApiIntegrationService.listCategoriesFilter(categoriesFilter);
        List<GenericUiResponse.AndroidItem> categoryItems = categories.values().stream().map(m -> new GenericUiResponse.AndroidItem(m.getImageUrl() != null ? m.getImageUrl().replace("/media/media", "/media") : null, m.getCategory(), "", m.getPostUrl())).collect(Collectors.toList());
        uiResponse.add(new GenericUiResponse(translationMap.get(LocaliseKeyConstants.bottom_bar_knowledge), "", order++, "", "2.5x2.5", "recyclerView", "h", categoryItems, "l", "center", true, "xl", true));
        uiResponse.add(new GenericUiResponse(translationMap.get(LocaliseKeyConstants.discover_homepage_label), "", order++, "", "3x1.5", "recyclerView", "h", getDiscoverItems(language), "s", "left", false, "xl", true));
        uiResponse.add(new GenericUiResponse(order++, "tools"));
        uiResponse.add(new GenericUiResponse(order++, "lung-footer"));
        return uiResponse;
    }

    public List<GenericUiResponse.AndroidItem> getDiscoverItems (String language) {
        List<GenericUiResponse.AndroidItem> items = new ArrayList<>();
        switch (language) {
            case "bn":
                items = Arrays.asList(
                        new GenericUiResponse.AndroidItem("https://drive.google.com/uc?id=19q0JDM5-StSkInmgFRiB3XOs3i0sXhYc", "", "", "https://aaro.health/marketplace"),
                        new GenericUiResponse.AndroidItem("https://drive.google.com/uc?id=19_8M3aKGj_D7fYP9W_AvoAADsIqwuPMa", "", "", "https://youtu.be/batzSytA1Y0")
                );
                break;
            case "gu":
                items = Arrays.asList(
                        new GenericUiResponse.AndroidItem("https://drive.google.com/uc?id=1fFqyFv7PkfTR59DqbdKUyn3hyPvAg6-V", "", "", "https://aaro.health/marketplace"),
                        new GenericUiResponse.AndroidItem("https://drive.google.com/uc?id=1NuHTunYHTBFrmVGspYLr9k3wFq9hrC_F", "", "", "https://youtu.be/batzSytA1Y0")
                );
                break;
            case "hi":
                items = Arrays.asList(
                        new GenericUiResponse.AndroidItem("https://drive.google.com/uc?id=1Hnb8cIyHqOSI70ptlzIv-kJ-DEGJHHOS", "", "", "https://aaro.health/marketplace"),
                        new GenericUiResponse.AndroidItem("https://drive.google.com/uc?id=1xm5byeAA6dpsklC2qhMAwUuGygHTwbzQ", "", "", "https://youtu.be/batzSytA1Y0")
                );
                break;
            case "kn":
                items = Arrays.asList(
                        new GenericUiResponse.AndroidItem("https://drive.google.com/uc?id=1PZ6qlwQqvghqBDTUkCqgDF71Tlw6x9mL", "", "", "https://aaro.health/marketplace"),
                        new GenericUiResponse.AndroidItem("https://drive.google.com/uc?id=1XUV2tGvO9Ukw_ok-QNvzoNoGEwj2vIuP", "", "", "https://youtu.be/batzSytA1Y0")
                );
                break;
            default:
                items = Arrays.asList(
                        new GenericUiResponse.AndroidItem("https://drive.google.com/uc?id=1j-G__5maGLiIe12mcfnw6vV_O4dn5UCk", "", "", "https://aaro.health/marketplace"),
                        new GenericUiResponse.AndroidItem("https://drive.google.com/uc?id=1ZS4oeD69b6OMx9eNY368Df2-6qTdbxgo", "", "", "https://youtu.be/batzSytA1Y0")
                );
                break;
        }
        return items;
    }

}

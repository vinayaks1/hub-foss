package com.everwell.transition.service;

import com.everwell.transition.model.dto.EventStreamingDto;

import java.util.Map;

public interface StageTransitionService {
    EventStreamingDto<Map<String, Object>> stageTransitionHandler(Map<String, Object> patientInput, Long clientId);
}

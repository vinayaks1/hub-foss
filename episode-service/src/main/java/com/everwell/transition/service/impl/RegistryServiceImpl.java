package com.everwell.transition.service.impl;

import com.everwell.transition.constants.AppointmentConstants;
import com.everwell.transition.enums.registry.RegistryEndpoints;
import com.everwell.transition.model.request.appointment.StaffMapRequest;
import com.everwell.transition.model.response.HierarchyData;
import com.everwell.transition.model.response.HierarchyResponseData;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.response.appointment.AppointmentStaffMap;
import com.everwell.transition.service.RegistryService;
import com.everwell.transition.utils.Utils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;


@Service
public class RegistryServiceImpl extends DataGatewayHelperServiceImpl implements RegistryService {

    private Map<Long, List<Long>> hierarchyIdToallLevelIdsMap = new HashMap<>();

    @Override
    public ResponseEntity<Response<HierarchyResponseData>> getHierarchyById(Long hId, boolean includeAssociations) {
        String endPoint = RegistryEndpoints.GET_HIERARCHY.getEndpointUrl();
        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("includeAssociations", includeAssociations);
        return getExchange(endPoint, new ParameterizedTypeReference<Response<HierarchyResponseData>>() {}, hId, queryParams);
    }

    @Override
    public List<Long> getAllLevelIdsForHierarchy(Long id, boolean includeAssociations) {
        // can expect a lot of repeated calls, thus caching in memory for now
        if (hierarchyIdToallLevelIdsMap.containsKey(id)) return hierarchyIdToallLevelIdsMap.get(id);
        List<Long> allIds = new ArrayList<>();
        Response<HierarchyResponseData> h = getHierarchyById(id, includeAssociations).getBody();
        if (h != null && h.isSuccess()) {
            HierarchyData data = h.getData().getHierarchy();
            allIds.add(data.getId());
            allIds.add(data.getLevel1Id());
            allIds.add(data.getLevel2Id());
            allIds.add(data.getLevel3Id());
            allIds.add(data.getLevel4Id());
            allIds.add(data.getLevel5Id());
            allIds.add(data.getLevel6Id());
        }
        List<Long> allIdsList = allIds.stream().filter(f -> f != null && f != 0).collect(Collectors.toList());
        hierarchyIdToallLevelIdsMap.put(id, allIdsList);
        return allIdsList;
    }

    @Override
    public AppointmentStaffMap addAppointmentStaffMap(StaffMapRequest appointmentStaffMap) {
        String endpoint = RegistryEndpoints.ADD_STAFF_MAP.getEndpointUrl();
        ResponseEntity<Response<AppointmentStaffMap>> responseEntity =
                postExchange(endpoint, appointmentStaffMap, new ParameterizedTypeReference<Response<AppointmentStaffMap>>() {
        });
        return Utils.processResponseEntity(responseEntity, AppointmentConstants.COULD_NOT_SAVE_APPOINTMENT_TRY_AGAIN);
    }

}

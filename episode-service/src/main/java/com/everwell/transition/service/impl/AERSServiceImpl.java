package com.everwell.transition.service.impl;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.enums.AERSFormEnum;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.db.AdverseEvent;
import com.everwell.transition.model.db.AdverseReaction;
import com.everwell.transition.model.db.CausalityManagement;
import com.everwell.transition.model.db.AersOutcome;
import com.everwell.transition.model.dto.CausalityMedicineDto;
import com.everwell.transition.model.dto.FormConfigDto;
import com.everwell.transition.model.dto.SuspectedDrugsDto;
import com.everwell.transition.model.request.aers.AdverseReactionRequest;
import com.everwell.transition.model.request.aers.CausalityManagementRequest;
import com.everwell.transition.model.request.aers.OutcomeRequest;
import com.everwell.transition.model.response.AdverseReactionResponse;
import com.everwell.transition.model.response.CausalityManagementResponse;
import com.everwell.transition.model.response.FormConfigResponse;
import com.everwell.transition.model.response.OutcomeResponse;
import com.everwell.transition.repositories.AdverseEventRepository;
import com.everwell.transition.repositories.AdverseReactionRepository;
import com.everwell.transition.repositories.CausalityManagementRepository;
import com.everwell.transition.repositories.OutcomeRepository;
import com.everwell.transition.service.AERSService;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AERSServiceImpl implements AERSService {

    @Autowired
    private AdverseEventRepository adverseEventRepository;

    @Autowired
    private CausalityManagementRepository causalityManagementRepository;

    @Autowired
    private OutcomeRepository outcomeRepository;

    @Autowired
    private AdverseReactionRepository adverseReactionRepository;

    private static Logger LOGGER = LoggerFactory.getLogger(AERSServiceImpl.class);

    private AdverseEvent getAdverseEvent(Long adverseEventId) {
        Optional<AdverseEvent> optionalAdverseEvent = adverseEventRepository.findById(adverseEventId);
        if(!optionalAdverseEvent.isPresent())
            throw new ValidationException("Adverse Event not found");
        AdverseEvent adverseEvent = optionalAdverseEvent.get();
        return adverseEvent;
    }

    private void compareAndSetOnsetAndOutcomeDates(AdverseEvent adverseEvent, AersOutcome outcome)
    {
        LocalDateTime onsetDate = adverseEvent.getOnsetDate();
        if(outcome.getDateOfResolution() == null) {
            LocalDateTime dateOfDeath = outcome.getDateOfDeath();
            if(Utils.isDateBefore(dateOfDeath, onsetDate))
                throw new ValidationException("Date of Death cannot be before Onset Date");
            adverseEvent.setOutcomeDate(dateOfDeath);
        }
        else {
            LocalDateTime dateOfResolution = outcome.getDateOfResolution();
            if(Utils.isDateBefore(dateOfResolution, onsetDate))
                throw new ValidationException("Date of resolution cannot be before Onset Date");
            adverseEvent.setOutcomeDate(dateOfResolution);
        }
    }

    @Override
    public void addAdverseReaction(AdverseReactionRequest adverseReactionRequest) {
        if(null != adverseReactionRequest.getAdverseEventId())
            updateAdverseReaction(adverseReactionRequest);
        else {
            validateDates(adverseReactionRequest.getDateOfDeath(), adverseReactionRequest.getHospitalAdmissionDate(), adverseReactionRequest.getHospitalDischargeDate());
            validateDeathDateVsOnset(adverseReactionRequest.getDateOfDeath(), adverseReactionRequest.getOnsetDate());
            AdverseReaction adverseReaction = new AdverseReaction(adverseReactionRequest);
            adverseReaction.setCreatedOn(LocalDateTime.now());
            Long adverseReactionId = adverseReactionRepository.save(adverseReaction).getId();
            AdverseEvent adverseEvent = new AdverseEvent(adverseReactionRequest.getEpisodeId(), adverseReactionId);
            adverseEvent.setOnsetDate(Utils.toLocalDateTimeWithNull(adverseReactionRequest.getOnsetDate()));
            adverseEventRepository.save(adverseEvent);
        }
    }

    @Override
    public void updateAdverseReaction(AdverseReactionRequest adverseReactionRequest) {
        validateDates(adverseReactionRequest.getDateOfDeath(), adverseReactionRequest.getHospitalAdmissionDate(), adverseReactionRequest.getHospitalDischargeDate());
        validateDeathDateVsOnset(adverseReactionRequest.getDateOfDeath(), adverseReactionRequest.getOnsetDate());
        if(adverseReactionRequest.getAdverseEventId() == null)
            throw new ValidationException("Adverse Event Id cannot be null");
        AdverseEvent adverseEvent = getAdverseEvent(adverseReactionRequest.getAdverseEventId());
        Long adverseReactionId = adverseEvent.getAdverseReactionId();
        Optional<AdverseReaction> optionalAdverseReaction = adverseReactionRepository.findById(adverseReactionId);
        if(!optionalAdverseReaction.isPresent())
            throw new ValidationException("Adverse Reaction not found");
        AdverseReaction adverseReaction = optionalAdverseReaction.get();
        AdverseReaction newAdverseReaction = new AdverseReaction(adverseReactionRequest);
        adverseReaction.update(newAdverseReaction);
        adverseReaction.setUpdatedOn(LocalDateTime.now());
        adverseReactionRepository.save(adverseReaction);
        adverseEvent.setOnsetDate(Utils.toLocalDateTimeWithNull(adverseReactionRequest.getOnsetDate()));
        adverseEventRepository.save(adverseEvent);
    }

    @Override
    public void addCausalityManagement(CausalityManagementRequest causalityManagementRequest) {
        AdverseEvent adverseEvent = getAdverseEvent(causalityManagementRequest.getAdverseEventId());
        if(null != adverseEvent.getCausalityManagementId())
            updateCausalityManagement(causalityManagementRequest);
        else {
            validateDates(causalityManagementRequest.getAntiTbMedicines());
            validateDates(causalityManagementRequest.getOtherMedicines());
            validateDates(causalityManagementRequest.getNewMedicines());
            SuspectedDrugsDto suspectedDrugsDto = new SuspectedDrugsDto(causalityManagementRequest);
            CausalityManagement causalityManagement = new CausalityManagement(causalityManagementRequest, suspectedDrugsDto);
            causalityManagement.setCreatedOn(LocalDateTime.now());
            Long causalityManagementId = causalityManagementRepository.save(causalityManagement).getId();
            adverseEvent.setCausalityManagementId(causalityManagementId);
            adverseEventRepository.save(adverseEvent);
        }
    }

    @Override
    public void updateCausalityManagement(CausalityManagementRequest causalityManagementRequest) {
        validateDates(causalityManagementRequest.getAntiTbMedicines());
        validateDates(causalityManagementRequest.getOtherMedicines());
        validateDates(causalityManagementRequest.getNewMedicines());
        AdverseEvent adverseEvent = getAdverseEvent(causalityManagementRequest.getAdverseEventId());
        Long causalityManagementId = adverseEvent.getCausalityManagementId();
        Optional<CausalityManagement> optionalCausalityManagement = causalityManagementRepository.findById(causalityManagementId);
        if(!optionalCausalityManagement.isPresent())
            throw new ValidationException("Causality and Management not found");
        CausalityManagement causalityManagement = optionalCausalityManagement.get();
        SuspectedDrugsDto suspectedDrugsDto = new SuspectedDrugsDto(causalityManagementRequest);
        CausalityManagement newCausalityManagement = new CausalityManagement(causalityManagementRequest, suspectedDrugsDto);
        causalityManagement.update(newCausalityManagement);
        causalityManagement.setUpdatedOn(LocalDateTime.now());
        causalityManagementRepository.save(causalityManagement);
    }

    @Override
    public void addOutcome(OutcomeRequest outcomeRequest) {
        AdverseEvent adverseEvent = getAdverseEvent(outcomeRequest.getAdverseEventId());
        if(null != adverseEvent.getOutcomeId())
            updateOutcome(outcomeRequest);
        else {
            validateDates(outcomeRequest.getDateOfDeath(), outcomeRequest.getHospitalAdmissionDate(), outcomeRequest.getHospitalDischargeDate());
            AersOutcome outcome = new AersOutcome(outcomeRequest);
            compareAndSetOnsetAndOutcomeDates(adverseEvent, outcome);
            outcome.setCreatedOn(LocalDateTime.now());
            Long outcomeId = outcomeRepository.save(outcome).getId();
            adverseEvent.setOutcomeId(outcomeId);
            adverseEventRepository.save(adverseEvent);
        }
    }

    @Override
    public void updateOutcome(OutcomeRequest outcomeRequest) {
        validateDates(outcomeRequest.getDateOfDeath(), outcomeRequest.getHospitalAdmissionDate(), outcomeRequest.getHospitalDischargeDate());
        AdverseEvent adverseEvent = getAdverseEvent(outcomeRequest.getAdverseEventId());
        Long outcomeId = adverseEvent.getOutcomeId();
        Optional<AersOutcome> optionalOutcome = outcomeRepository.findById(outcomeId);
        if(!optionalOutcome.isPresent())
            throw new ValidationException("Outcome not found");
        AersOutcome outcome = optionalOutcome.get();
        AersOutcome newOutcome = new AersOutcome(outcomeRequest);
        outcome.update(newOutcome);
        outcome.setUpdatedOn(LocalDateTime.now());
        String outcomeField = outcomeRequest.getOutcome();
        if (outcomeField.equals(Constants.FATAL)) {
            outcome.setDateOfResolution(null);
        }
        else {
            outcome.setDateOfDeath(null);
            if (!outcomeField.equals(Constants.RECOVERED) && !outcomeField.equals(Constants.RECOVERED_RESOLVED_WITH_SEQUEL)) {
                outcome.setDateOfResolution(null);
            }
        }
        compareAndSetOnsetAndOutcomeDates(adverseEvent,outcome);
        outcomeRepository.save(outcome);
        adverseEventRepository.save(adverseEvent);
    }

    @Override
    public AdverseReactionResponse getAdverseReaction(Long adverseEventId) throws IOException {
        AdverseEvent adverseEvent = getAdverseEvent(adverseEventId);
        Long adverseReactionId = adverseEvent.getAdverseReactionId();
        Optional<AdverseReaction> optionalAdverseReaction = adverseReactionRepository.findById(adverseReactionId);
        if(!optionalAdverseReaction.isPresent())
            throw new ValidationException("Adverse Reaction not found");
        AdverseReaction adverseReaction = optionalAdverseReaction.get();
        return new AdverseReactionResponse(adverseReaction, adverseEvent.getEpisodeId(), adverseEventId);
    }

    @Override
    public CausalityManagementResponse getCausalityManagement(Long adverseEventId) throws IOException {
        AdverseEvent adverseEvent = getAdverseEvent(adverseEventId);
        Long causalityManagementId = adverseEvent.getCausalityManagementId();
        if (null == causalityManagementId)
            return null;
        Optional<CausalityManagement> optionalCausalityManagement = causalityManagementRepository.findById(causalityManagementId);
        if(!optionalCausalityManagement.isPresent())
            throw new ValidationException("Causality Management not found");
        CausalityManagement causalityManagement = optionalCausalityManagement.get();
        SuspectedDrugsDto suspectedDrugs = causalityManagement.getSuspectedDrugs() == null ? null : Utils.jsonToObject(causalityManagement.getSuspectedDrugs(), new TypeReference<SuspectedDrugsDto>(){});
        return new CausalityManagementResponse(causalityManagement, adverseEvent.getEpisodeId(), adverseEventId,suspectedDrugs);
    }

    @Override
    public OutcomeResponse getOutcome(Long adverseEventId) {
        AdverseEvent adverseEvent = getAdverseEvent(adverseEventId);
        Long outcomeId = adverseEvent.getOutcomeId();
        if (null == outcomeId)
            return null;
        Optional<AersOutcome> optionalOutcome = outcomeRepository.findById(outcomeId);
        if(!optionalOutcome.isPresent())
            throw new ValidationException("Outcome not found");
        AersOutcome outcome = optionalOutcome.get();
        return new OutcomeResponse(outcome, adverseEvent.getEpisodeId(), adverseEventId);
    }

    @Override
    public List<FormConfigResponse> getStatus(Long episodeId) {
        List<AdverseEvent> adverseEvents = adverseEventRepository.getAllByEpisodeId(episodeId);
        List<FormConfigResponse> aersStatusList = new ArrayList<>();
        for(AdverseEvent event : adverseEvents) {
            String onsetDate = event.getOnsetDate() == null ? null : event.getOnsetDate().toString();
            String outcomeDate = event.getOutcomeDate() == null ? null : event.getOutcomeDate().toString();
            List<FormConfigDto> formConfigDtos = new ArrayList<>();
            formConfigDtos.add(new FormConfigDto(AERSFormEnum.AdverseReactionForm.getName(), AERSFormEnum.AdverseReactionForm.getOrder(), event.getAdverseReactionId()));
            formConfigDtos.add(new FormConfigDto(AERSFormEnum.CausalityManagementForm.getName(),AERSFormEnum.CausalityManagementForm.getOrder(), event.getCausalityManagementId()));
            formConfigDtos.add(new FormConfigDto(AERSFormEnum.OutcomeForm.getName(), AERSFormEnum.OutcomeForm.getOrder(), event.getOutcomeId()));
            aersStatusList.add(new FormConfigResponse(event.getId(),formConfigDtos,onsetDate,outcomeDate));
        }
        return aersStatusList;
    }

    public void validateDates(String dateDeath, String dateHospitalAdmission, String dateHospitalDischarge) {
        LocalDateTime dateOfDeath = Utils.toLocalDateTimeWithNull(dateDeath);
        LocalDateTime hospitalAdmissionDate = Utils.toLocalDateTimeWithNull(dateHospitalAdmission);
        LocalDateTime hospitalDischargeDate = Utils.toLocalDateTimeWithNull(dateHospitalDischarge);
        if (Utils.isDateBefore(dateOfDeath,hospitalAdmissionDate))
            throw new ValidationException("Date of death cannot be before Hospital Admission Date");
        if (Utils.isDateBefore(dateOfDeath, hospitalDischargeDate))
            throw new ValidationException("Date of death cannot be before Hospital Discharge Date");
        if (Utils.isDateBefore(hospitalDischargeDate, hospitalAdmissionDate))
            throw new ValidationException("Hospital Discharge Date cannot be before Hospital Admission Date");
    }

    public void validateDeathDateVsOnset(String dateDeath, String dateOnset) {
        LocalDateTime dateOfDeath = Utils.toLocalDateTimeWithNull(dateDeath);
        LocalDateTime dateOfOnset = Utils.toLocalDateTimeWithNull(dateOnset);
        if (Utils.isDateBefore(dateOfDeath,dateOfOnset))
            throw new ValidationException("Date of death cannot be before Date of Onset");
    }

    public void validateDates(List<CausalityMedicineDto> medicines) {
        if (!CollectionUtils.isEmpty(medicines))
        {
            medicines.forEach(m -> {
                LocalDateTime startDate = Utils.toLocalDateTimeWithNull(m.getStartDate());
                LocalDateTime stopDate = Utils.toLocalDateTimeWithNull(m.getStopDate());
                if (Utils.isDateBefore(stopDate, startDate))
                    throw new ValidationException("Medicines Stop Date needs to be after Start Date");
            });
        }
    }
}

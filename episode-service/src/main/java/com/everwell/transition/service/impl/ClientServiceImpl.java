package com.everwell.transition.service.impl;

import com.everwell.transition.exceptions.NotFoundException;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.request.RegisterClientRequest;
import com.everwell.transition.model.response.ClientResponse;
import com.everwell.transition.repositories.ClientRepository;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.utils.JwtUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class ClientServiceImpl implements ClientService {

    private static Logger LOGGER = LoggerFactory.getLogger(ClientServiceImpl.class);

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    JwtUtils jwtUtils;

    @Value("${default.client.id}")
    Long defaultClientId;

    @Override
    public ClientResponse registerClient(RegisterClientRequest clientRequest) {
        Client existingClient = clientRepository.findByName(clientRequest.getName());
        if (null != existingClient) {
            LOGGER.error("[registerClient] User already exists");
            throw new ValidationException("User already exists");
        }
        String encodedPassword = new BCryptPasswordEncoder().encode(clientRequest.getPassword());
        Client newClient = new Client(clientRequest.getName(), encodedPassword);
        clientRepository.save(newClient);
        return new ClientResponse(newClient);
    }

    @Override
    public ClientResponse getClientWithToken(Long id) {
        Client client = getClient(id);
        String jwtToken = jwtUtils.generateToken(String.valueOf(id));
        return new ClientResponse(client, jwtToken, jwtUtils.getExpirationDateFromToken(jwtToken).getTime());
    }

    @Override
    public Client getClient(Long id) {
        if (null == id) {
            LOGGER.error("[getClient] id cannot be null");
            throw new ValidationException("id cannot be null");
        }
        Client client = clientRepository.findById(id).orElse(null);
        if (null == client) {
            LOGGER.error("[getClient] No client found with id:" + id);
            throw new NotFoundException("No client found with id:" + id);
        }
        return client;
    }

    @Override
    public Client getClientByTokenOrDefault() {
        try {
            return (Client) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        }
        catch (Exception e) {
            return clientRepository.findById(defaultClientId).orElse(null);
        }
    }
}

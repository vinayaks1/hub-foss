package com.everwell.transition.service;


import com.everwell.transition.model.response.*;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;

import java.util.List;
import java.util.Map;

public interface AggregationService {

    public Map<String, Object> resolveFields(List<String> inputFields, Long entityId);

    public void authenticate();

    public Map<String, Object> getByEntityIdAndType(String module, Long entityId, String entityType);

    public <T> ResponseEntity<Response<T>> genericExchange(String url, HttpMethod method, @Nullable HttpEntity<?> requestEntity, ParameterizedTypeReference typeReference);

    public <T> ResponseEntity<Response<T>> genericExchange(String url, HttpMethod method, @Nullable HttpEntity<?> requestEntity, ParameterizedTypeReference typeReference, Map<String,String> CustomHeaders);
    
    public <T> ResponseEntity<Response<T>> getExchange(String endpoint, ParameterizedTypeReference typeReference, Object id, Map<String,Object> queryParams);

    public <T> ResponseEntity<Response<T>> deleteExchange(String endpoint, ParameterizedTypeReference typeReference, Long id, Map<String,Object> queryParams, Object body);
    
    public <T> ResponseEntity<Response<T>> postExchange(String endpoint, Object body, ParameterizedTypeReference typeReference);

    public <T> ResponseEntity<Response<T>> postExchange(String endpoint, Object body, ParameterizedTypeReference typeReference, Map<String,String> CustomHeaders);

    public <T> ResponseEntity<Response<T>> putExchange(String endpoint, Object body, ParameterizedTypeReference typeReference);

}

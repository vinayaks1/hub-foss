package com.everwell.transition.service.impl;

import com.everwell.transition.model.db.EpisodeAppConfig;
import com.everwell.transition.model.response.EpisodeAppConfigResponse;
import com.everwell.transition.repositories.EpisodeAppConfigRepository;
import com.everwell.transition.service.EpisodeAppConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import static com.everwell.transition.constants.Constants.DEFAULT;

@Service
public class EpisodeAppConfigServiceImpl implements EpisodeAppConfigService {

    @Autowired
    EpisodeAppConfigRepository episodeAppConfigRepository;

    @Override
    public EpisodeAppConfigResponse getEpisodeAppConfig(Long episodeId,String technology, String deployment) {

       EpisodeAppConfig episodeAppConfig = episodeAppConfigRepository.getAllByTechnologyAndDeployment(technology, deployment);
       if(episodeAppConfig == null) {
           episodeAppConfig = episodeAppConfigRepository.getAllByTechnologyAndDeployment(DEFAULT, DEFAULT);
       }
        return new EpisodeAppConfigResponse(episodeId,episodeAppConfig);
    }
}

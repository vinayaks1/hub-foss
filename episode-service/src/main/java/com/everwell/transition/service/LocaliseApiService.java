package com.everwell.transition.service;

import java.util.Map;

public interface LocaliseApiService {
    Map<String, Object> getPatientAppTranslationsFromLocalise(String language, String region);
}

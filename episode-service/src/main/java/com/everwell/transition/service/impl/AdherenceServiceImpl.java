package com.everwell.transition.service.impl;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.constants.IAMConstants;
import com.everwell.transition.constants.RuleConstants;
import com.everwell.transition.enums.AdherenceCodeEnum;
import com.everwell.transition.enums.AdherenceTypeMappingEnum;
import com.everwell.transition.enums.adherence.AdherenceMonitoringMethod;
import com.everwell.transition.enums.episodeLog.EpisodeLogAction;
import com.everwell.transition.enums.episodeLog.EpisodeLogCategory;
import com.everwell.transition.enums.episodeLog.EpisodeLogComments;
import com.everwell.transition.enums.iam.IAMField;
import com.everwell.transition.enums.iam.IAMValidation;
import com.everwell.transition.enums.user.UserServiceField;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.db.EpisodeTag;
import com.everwell.transition.model.dto.AdherenceDataForCalendar;
import com.everwell.transition.model.dto.AdherenceMonthWiseData;
import com.everwell.transition.model.dto.IAMScheduleDto;
import com.everwell.transition.model.dto.dispensation.DispensationData;
import com.everwell.transition.model.request.adherence.*;
import com.everwell.transition.model.request.treatmentPlan.DoseDetails;
import com.everwell.transition.model.dto.episode.EpisodeTransitionDetails;
import com.everwell.transition.model.request.adherence.EditDosesRequest;
import com.everwell.transition.model.request.adherence.EntityRequest;
import com.everwell.transition.model.request.adherence.PhoneRequest;
import com.everwell.transition.model.request.adherence.RecordAdherenceRequest;
import com.everwell.transition.model.request.episodelogs.EpisodeLogDataRequest;
import com.everwell.transition.model.response.*;
import com.everwell.transition.model.response.dispensation.EntityDispensationResponse;
import com.everwell.transition.postconstruct.AdherenceGlobalConfigMap;
import com.everwell.transition.postconstruct.EpisodeTagStoreCreation;
import com.everwell.transition.service.*;
import com.everwell.transition.utils.Utils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static com.everwell.transition.constants.FieldConstants.FIELD_CLOSE_CASE_NOTE_TEXT;
import static com.everwell.transition.constants.FieldConstants.FIELD_COUNTRY_PREFIX;
import static com.everwell.transition.constants.IAMConstants.MONITORING_METHODS_NNDOTS;
import static com.everwell.transition.constants.RuleConstants.*;

@Service
public class AdherenceServiceImpl implements AdherenceService {

    @Autowired
    AdherenceGlobalConfigMap adherenceGlobalConfigMap;

    @Autowired
    IAMService iamService;

    @Autowired
    DispensationService dispensationService;

    @Autowired
    EpisodeTagStoreCreation episodeTagStoreCreation;

    @Autowired
    EpisodeLogService episodeLogService;

    @Autowired
    EpisodeTagService episodeTagService;

    public List<AdherenceMonthWiseData> formatAdherenceStringMonthWiseAndAttachTags(AdherenceResponse data, Map<LocalDateTime, List<EpisodeTag>> dateToTagListMap, ZoneId zoneId) {
        LocalDateTime startDate = ZonedDateTime.of(data.getStartDate(), ZoneOffset.UTC).toOffsetDateTime().atZoneSameInstant(zoneId).toLocalDateTime();
        LocalDateTime endDateLocal = data.getEndDate() == null ? LocalDateTime.now() : data.getEndDate();
        LocalDateTime endDate = ZonedDateTime.of(endDateLocal, ZoneOffset.UTC).toOffsetDateTime().atZoneSameInstant(zoneId).toLocalDateTime();
        String paddedAdherenceString = StringUtils.repeat(adherenceGlobalConfigMap.getCodeToConfigMap().get("BLANK").getCode(), startDate.getDayOfMonth() - 1) + data.getAdherenceString();
        YearMonth startYearMonth = YearMonth.of(startDate.getYear(), startDate.getMonth());
        LocalDateTime startOfMonth = startYearMonth.atDay(1).atStartOfDay();
        StringBuilder adherenceString = new StringBuilder(paddedAdherenceString);
        List<AdherenceMonthWiseData> adherenceMonthWiseData = new ArrayList<>();
        while (startOfMonth.isBefore(endDate) || startOfMonth.equals(endDate)) {
            YearMonth currYearMonth = YearMonth.of(startOfMonth.getYear(), startOfMonth.getMonth());
            int daysInMonth = currYearMonth.lengthOfMonth();
            int daysLeft = Math.min(adherenceString.length(), daysInMonth);
            List<AdherenceDataForCalendar> adherenceDataForCalendar = new ArrayList<>();
            String adherenceStringForMonth = adherenceString.substring(0, daysLeft);
            LocalDateTime currDate = startOfMonth;
            for (char c : adherenceStringForMonth.toCharArray()) {
                adherenceDataForCalendar.add(new AdherenceDataForCalendar(c, currDate.getDayOfWeek().name(), dateToTagListMap.getOrDefault(currDate, new ArrayList<>()).stream().map(m -> episodeTagStoreCreation.getEpisodeTagStoreMap().get(m.getTagName()).getTagDisplayName()).collect(Collectors.toList())));
                currDate = currDate.plusDays(1);
            }
            adherenceMonthWiseData.add(new AdherenceMonthWiseData(currYearMonth.getMonth().name(), currYearMonth.getMonthValue(), daysInMonth, currYearMonth.getYear(), adherenceDataForCalendar));
            adherenceString = new StringBuilder(adherenceString.substring(daysLeft));
            startOfMonth = startOfMonth.plusMonths(1);
        }
        return adherenceMonthWiseData;
    }

    @Override
    public List<AdherenceMonthWiseData> formatAdherenceStringMonthWise(AdherenceResponse data, EpisodeTagResponse tagResponse, ZoneId timezoneId) {
        return formatAdherenceStringMonthWiseAndAttachTags(data, getTagsByDate(tagResponse, timezoneId), timezoneId);
    }

    @Override
    public Map<LocalDateTime, List<EpisodeTag>> getTagsByDate(EpisodeTagResponse tagResponse, ZoneId timezoneId) {
        Map<LocalDateTime, List<EpisodeTag>> dateToTagListMap = new HashMap<>();
        if (tagResponse.getTags() != null) {
            tagResponse.getTags().forEach(tag -> {
                LocalDateTime tagDate = ZonedDateTime.of(tag.getTagDate(), ZoneOffset.UTC).toOffsetDateTime().atZoneSameInstant(timezoneId).toLocalDateTime();
                dateToTagListMap.putIfAbsent(tagDate, new ArrayList<>());
                dateToTagListMap.get(tagDate).add(tag);
            });
        }
        return dateToTagListMap;
    }

    @Override
    public List<AdherenceCodeConfigResponse> getAdherenceConfig() {
        Map<String, AdherenceCodeConfigResponse> adherenceCodeConfigResponseMap = adherenceGlobalConfigMap.getCodeToConfigMap();
        if (CollectionUtils.isEmpty(adherenceCodeConfigResponseMap)) {
            adherenceGlobalConfigMap.createConfigMaps();
            adherenceCodeConfigResponseMap = adherenceGlobalConfigMap.getCodeToConfigMap();
        }
        return new ArrayList<>(adherenceCodeConfigResponseMap.values());
    }

    @Override
    public Map<String, List<LocalDateTime>> editDosesBulk(EditDoseBulkRequest editDosesRequestList) {
        Map<String, List<LocalDateTime>> entityToAlteredDates = new HashMap<>();
        List<RecordAdherenceRequest> recordAdherenceRequests = new ArrayList<>();
        for (EditDosesRequest editDosesRequest : editDosesRequestList.getEditDosesRequestList()) {
            List<LocalDateTime> eventDatesAltered;
            // TODO: Try to make `getAllDispensationDataForEntity` as bulk as well
            if (editDosesRequest.getAdherenceDispensationValidation()) {
                ResponseEntity<Response<EntityDispensationResponse>> response = dispensationService.getAllDispensationDataForEntity(editDosesRequest.getEpisodeId(), false);
                eventDatesAltered = validateDispensation(editDosesRequest.getUtcDates(), editDosesRequest.getEpisodeId(), Objects.requireNonNull(response.getBody()).getData());
                if (CollectionUtils.isEmpty(eventDatesAltered))
                    throw new ValidationException("Dispensation Does not exist for selected Dates");
            } else {
                eventDatesAltered = editDosesRequest.getUtcDates().stream().map(Utils::toLocalDateTimeWithNull).collect(Collectors.toList());
            }
            char adherenceCode = AdherenceCodeEnum.getAdherenceCode(editDosesRequest.getCode());
            if (!editDosesRequest.getAddDoses())
                adherenceCode = AdherenceCodeEnum.NO_INFO.getAdherenceCode();
            Long adherenceType = AdherenceTypeMappingEnum.getMatch(editDosesRequest.getMonitoringMethod());
            Map<String, Character> datesValueMap = new HashMap<>();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            char finalAdherenceCode = adherenceCode;
            eventDatesAltered.forEach(d -> {
                String date = d.format(formatter);
                if (!datesValueMap.containsKey(date))
                    datesValueMap.put(date, finalAdherenceCode);
            });
            entityToAlteredDates.putIfAbsent(editDosesRequest.getIamEntityId(), new ArrayList<>());
            entityToAlteredDates.get(editDosesRequest.getIamEntityId()).addAll(eventDatesAltered);
            RecordAdherenceRequest recordAdherenceRequest = new RecordAdherenceRequest(editDosesRequest.getIamEntityId(), adherenceType, datesValueMap);
            recordAdherenceRequests.add(recordAdherenceRequest);
        }
        RecordAdherenceBulkRequest request = new RecordAdherenceBulkRequest(recordAdherenceRequests);
        ResponseEntity<Response<Void>> iamResponse = iamService.recordAdherenceBulk(request);
        Utils.processResponseEntity(iamResponse, "Unable to update Adherence");
        return entityToAlteredDates;
    }

    public Map<String, Integer> getMonitoringMethodConfig() {
        Map<String, Integer> monitoringMethodToIdMap = adherenceGlobalConfigMap.getMonitoringMethodToIdMap();
        if (CollectionUtils.isEmpty(monitoringMethodToIdMap)) {
            adherenceGlobalConfigMap.createConfigMaps();
            monitoringMethodToIdMap = adherenceGlobalConfigMap.getMonitoringMethodToIdMap();
        }
        return monitoringMethodToIdMap;
    }

    @Override
    public Map<String, Integer> getScheduleTypeConfig() {
        Map<String, Integer> scheduleTypeToIdMap = adherenceGlobalConfigMap.getScheduleTypeToIdMap();
        if (CollectionUtils.isEmpty(scheduleTypeToIdMap)) {
            adherenceGlobalConfigMap.createConfigMaps();
            scheduleTypeToIdMap = adherenceGlobalConfigMap.getScheduleTypeToIdMap();
        }
        return scheduleTypeToIdMap;
    }

    @Override
    public List<LocalDateTime> editDoses(EditDosesRequest editDosesRequest) {
        List<LocalDateTime> eventDatesAltered;
        if (editDosesRequest.getAdherenceDispensationValidation()) {
            ResponseEntity<Response<EntityDispensationResponse>> response =  dispensationService.getAllDispensationDataForEntity(editDosesRequest.getEpisodeId(), false);
            eventDatesAltered = validateDispensation(editDosesRequest.getUtcDates(), editDosesRequest.getEpisodeId(), Objects.requireNonNull(response.getBody()).getData());
            if (CollectionUtils.isEmpty(eventDatesAltered))
                throw new ValidationException("Dispensation Does not exist for selected Dates");
        }
        else {
            eventDatesAltered = editDosesRequest.getUtcDates().stream().map(Utils::toLocalDateTimeWithNull).collect(Collectors.toList());
        }
        char adherenceCode = AdherenceCodeEnum.getAdherenceCode(editDosesRequest.getCode());
        if (!editDosesRequest.getAddDoses())
            adherenceCode = AdherenceCodeEnum.NO_INFO.getAdherenceCode();
        Long adherenceType = AdherenceTypeMappingEnum.getMatch(editDosesRequest.getMonitoringMethod());
        Map<String,Character> datesValueMap = new HashMap<>();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        char finalAdherenceCode = adherenceCode;
        eventDatesAltered.forEach(d -> {
            String date = d.format(formatter);
            if (!datesValueMap.containsKey(date))
                datesValueMap.put(date, finalAdherenceCode);
        });
        RecordAdherenceRequest recordAdherenceRequest = new RecordAdherenceRequest(editDosesRequest.getIamEntityId(), adherenceType, datesValueMap);
        ResponseEntity<Response<Void>> iamResponse = iamService.recordAdherenceForMultipleDates(recordAdherenceRequest);
        Utils.processResponseEntity(iamResponse, "Unable to update Adherence");
        return eventDatesAltered;
    }

    @Override
    public List<LocalDateTime> validateDispensation(List<String> dates, Long episodeId, EntityDispensationResponse entityDispensationResponse) {
        List<LocalDateTime> eventDatesAltered = new ArrayList<>();
        List<DispensationData> dispensationData =  entityDispensationResponse.getDispensations();
        dispensationData.forEach(d -> d.setProductList(d.getProductList().stream().filter(p -> !p.getNumberOfUnitsIssued().equals("0")).collect(Collectors.toList())));
        dispensationData = dispensationData.stream().filter(d -> d.getProductList().size() > 0).collect(Collectors.toList());
        dispensationData.forEach( d -> {
           LocalDateTime startDate = Utils.toLocalDateTimeWithNull(d.getDosingStartDate());
           LocalDateTime endDate = Utils.toLocalDateTimeWithNull(d.getRefillDate());
           dates.forEach(date -> {
               LocalDateTime eventDate = Utils.toLocalDateTimeWithNull(date);
               if (!eventDate.isBefore(startDate) && eventDate.isBefore(endDate))
                   eventDatesAltered.add(eventDate);
           });
        });
        return eventDatesAltered.stream().distinct().collect(Collectors.toList());
    }

    @Override
    public void updateIamSchedule(List<DoseDetails> doseTimeList, String startDate, String entityId, Long clientId) {
        IAMScheduleDto scheduleInfo = new IAMScheduleDto(IAMConstants.MULTI_FREQUENCY, null, IAMConstants.FIRST_DOSE_OFFSET, IAMConstants.DEFAULT_POSITIVE_SENSITIVITY, IAMConstants.DEFAULT_NEGATIVE_SENSITIVITY, IAMConstants.SCHEDULE_TYPE_ID);
        Integer adherenceMapping = AdherenceTypeMappingEnum.NONE.getId().intValue();
        EntityRequest entityRequest = new EntityRequest(entityId, adherenceMapping, entityId, startDate, scheduleInfo.getScheduleTypeId(), scheduleInfo.scheduleString, scheduleInfo.positiveSensitivity, scheduleInfo.negativeSensitivity, scheduleInfo.firstDoseOffset, clientId, doseTimeList);
        ResponseEntity<Response<Map<String, Object>>> responseEntity = iamService.updateEntity(entityRequest);
        Utils.processResponseEntity(responseEntity, "Error updating entity at IAM");
    }

    @Override
    public Boolean registerEntity(String entityId, String monitoringMethod, String uniqueIdentifier, String startDate, IAMScheduleDto scheduleInfo, Object doseDetails) {
        return iamService.registerEntity(entityId, monitoringMethod, uniqueIdentifier, startDate, scheduleInfo, doseDetails);
    }

    @Override
    public AllAdherenceResponse getAdherenceBulk(SearchAdherenceRequest adherenceRequest, Boolean merge) {
        return Utils.processResponseEntity(iamService.getAdherenceBulk(adherenceRequest, merge), "Unable to fetch Adherence");
    }

    @Override
    public ResponseEntity<Response<Map<String, Object>>> deleteEntity(String entityId, String endDate, Boolean updateEndDate, Boolean deleteIfRequired) {
        return iamService.deleteEntity(entityId, endDate, updateEndDate, deleteIfRequired);
    }

    private List<PhoneRequest.PhoneNumbers> getPhoneNumbersForAdherence(String countryPrefix, Map<String, Object> episodeStageData, AtomicReference<String> primaryPhoneNumber) {
        List<PhoneRequest.PhoneNumbers> phoneNumberList = new ArrayList<>();
        Object mobileData = episodeStageData.get(UserServiceField.MOBILE_LIST.getFieldName());
        if (null != mobileData) {
            List<Map<String, Object>> mobileList = (List<Map<String, Object>>) mobileData;
            mobileList.forEach(mobile -> {
                String number = Utils.convertObjectToStringWithNull(mobile.get(UserServiceField.NUMBER.getFieldName()));
                if (!StringUtils.isBlank(number)) {
                    String fullNumber = countryPrefix + number.trim();
                    String stoppedAtData = Utils.convertObjectToStringWithNull(mobile.get(UserServiceField.STOPPED_AT.getFieldName()));
                    String stoppedAtFormattedData = null != stoppedAtData ? Utils.convertEpochToDate(Long.parseLong(stoppedAtData)) : null;
                    phoneNumberList.add(new PhoneRequest.PhoneNumbers(fullNumber, stoppedAtFormattedData));
                    if (Boolean.TRUE.equals(mobile.get(UserServiceField.PRIMARY.getFieldName()))) {
                        primaryPhoneNumber.set(fullNumber);
                    }
                }
            });
        }
        return phoneNumberList;
    }

    private Integer getAdherenceTypeMapping(String monitoringMethod) {
        return AdherenceTypeMappingEnum.getMatch(monitoringMethod).intValue();
    }

    private Integer getScheduleTypeMapping(String scheduleType) {
        return getScheduleTypeConfig().get(StringUtils.lowerCase(scheduleType));
    }

    private EntityRequest constructEntityRequest(Long episodeId, Map<String, Object> requestInput, Map<String, Object> processedRequestInput, Map<String, Object> episodeStageData) {
        String monitoringMethod = Utils.convertObjectToStringWithNull(processedRequestInput.getOrDefault(IAMField.MONITORING_METHOD.getFieldName(), IAMConstants.NONE));
        String scheduleType = Utils.convertObjectToStringWithNull(processedRequestInput.getOrDefault(IAMField.SCHEDULE_TYPE.getFieldName(), IAMConstants.DEFAULT_SCHEDULE_TYPE));
        Integer adherenceTypeMapping = getAdherenceTypeMapping(monitoringMethod);
        Integer scheduleTypeMapping = getScheduleTypeMapping(scheduleType);
        EntityRequest entityRequest = new EntityRequest(episodeId, processedRequestInput, adherenceTypeMapping, scheduleTypeMapping);
        if (MONITORING_METHODS_NNDOTS.contains(monitoringMethod)) {
            String countryPrefix = Utils.convertObjectToStringWithEmpty(requestInput.get(FIELD_COUNTRY_PREFIX));
            AtomicReference<String> primaryPhoneNumber = new AtomicReference<>();
            List<PhoneRequest.PhoneNumbers> phoneNumberList = getPhoneNumbersForAdherence(countryPrefix, episodeStageData, primaryPhoneNumber);
            entityRequest.setPhoneNumbers(phoneNumberList);
            entityRequest.setUniqueIdentifier(primaryPhoneNumber.get());
        } else if (AdherenceMonitoringMethod.MERM.getName().equals(monitoringMethod)){
            String imei = Utils.convertObjectToStringWithNull(requestInput.get(IAMField.IMEI.getFieldName()));
            entityRequest.setUniqueIdentifier(imei);
            entityRequest.setImei(imei);
        }
        entityRequest.validate();
        return entityRequest;
    }

    @Override
    public void registerEntity(Long episodeId, Map<String, Object> requestInput, Map<String, Object> processedRequestInput, Map<String, Object> stageTransitionResult)
    {
        String actionOnIAM = String.valueOf(stageTransitionResult.get(RuleConstants.ACTION_ON_IAM));
        if (IAM_REGISTER_OR_UPDATE.equals(actionOnIAM))
        {
            registerCase(episodeId, requestInput, processedRequestInput, requestInput);
        }
    }

    private void registerCase(Long episodeId, Map<String, Object> requestInput, Map<String, Object> processedRequestInput, Map<String, Object> episodeStageData)
    {
        EntityRequest entityRequest = constructEntityRequest(episodeId, requestInput, processedRequestInput, episodeStageData);
        ResponseEntity<Response<Map<String, Object>>> response = iamService.registerEntity(entityRequest);
        Utils.processResponseEntity(response, IAMValidation.ERROR_AT_REGISTER_ENTITY.getMessage());
    }

    @Override
    public void updateEntity(EpisodeTransitionDetails episodeTransitionDetails, Map<String, Object> requestInput, Map<String, Object> processedRequestInput, Map<String, Object> stageTransitionResult)
    {
        String actionOnIAM = CollectionUtils.isEmpty(stageTransitionResult) ? IAM_REGISTER_OR_UPDATE : Utils.convertObjectToStringWithEmpty(stageTransitionResult.get(ACTION_ON_IAM));
        switch (actionOnIAM) {
            case IAM_REGISTER_OR_UPDATE:
                updateEntity(episodeTransitionDetails, requestInput, processedRequestInput);
                break;
            case IAM_CLOSED:
                closeEntity(episodeTransitionDetails, requestInput, processedRequestInput);
                break;
            case IAM_REOPEN:
                reopenEntity(episodeTransitionDetails, requestInput, processedRequestInput);
                break;
            default:
                break;
        }
    }

    private void saveMonitoringMethodChangedLogs(Long episodeId, String oldMonitoringMethod, String newMonitoringMethod, Map<String, Object> requestInput) {
        Long userId = NumberUtils.toLong(Utils.convertObjectToStringWithNull(requestInput.get(FieldConstants.FIELD_LOGS_ADDED_BY)), Constants.DEFAULT_ADDED_BY_ID);
        List<EpisodeLogDataRequest> episodeLogRequest = new ArrayList<>();
        episodeLogRequest.add(new EpisodeLogDataRequest(episodeId, EpisodeLogCategory.MONITORING_METHOD_CHANGED.getCategoryName(), userId,
                EpisodeLogAction.MONITORING_METHOD_CHANGED.getActionName(), String.format(EpisodeLogComments.MONITORING_METHOD_CHANGED.getText(), oldMonitoringMethod, newMonitoringMethod)));
        episodeLogService.save(episodeLogRequest);
    }

    private void saveMonitoringMethodDeletedSwitchesLogs(Long episodeId, String endDate, Map<String, Object> requestInput) {
        Long userId = NumberUtils.toLong(Utils.convertObjectToStringWithNull(requestInput.get(FieldConstants.FIELD_LOGS_ADDED_BY)), Constants.DEFAULT_ADDED_BY_ID);
        String closeCaseNoteText = Utils.convertObjectToStringWithNull(requestInput.get(FIELD_CLOSE_CASE_NOTE_TEXT));
        List<EpisodeLogDataRequest> episodeLogRequest = new ArrayList<>();
        episodeLogRequest.add(new EpisodeLogDataRequest(episodeId, EpisodeLogCategory.CLOSE_CASE.getCategoryName(), userId,
                String.format(EpisodeLogAction.DELETED_TECHNOLOGY_SWITCHES.getActionName(), endDate),  closeCaseNoteText));
        episodeLogService.save(episodeLogRequest);
    }

    private void closeCase(Long episodeId, Map<String, Object> requestInput, Map<String, Object> processedRequestInput) {
        String endDate = Utils.convertObjectToStringWithNull(processedRequestInput.get(IAMField.IAM_END_DATE.getFieldName()));
        boolean deleteIfRequired = Boolean.parseBoolean(String.valueOf(requestInput.get(IAMField.DELETE_IF_REQUIRED.getFieldName())));
        boolean updateEndDate = Boolean.parseBoolean(String.valueOf(requestInput.get(IAMField.UPDATE_END_DATE.getFieldName())));
        ResponseEntity<Response<String>> responseEntity = iamService.closeCase(episodeId, endDate, deleteIfRequired, updateEndDate);
        Utils.processResponseEntity(responseEntity, IAMValidation.CLOSE_CASE_VALIDATION.getMessage());
    }

    private void updateEntity(EpisodeTransitionDetails episodeTransitionDetails, Map<String, Object> requestInput, Map<String, Object> processedRequestInput)
    {
        String oldMonitoringMethod = Utils.convertObjectToStringWithNull(episodeTransitionDetails.getEpisodeDtoBeforeUpdate().getStageData().get(IAMField.MONITORING_METHOD.getFieldName()));
        String newMonitoringMethod = Utils.convertObjectToStringWithNull(processedRequestInput.get(IAMField.MONITORING_METHOD.getFieldName()));
        Long episodeId = episodeTransitionDetails.getEpisodeDtoBeforeUpdate().getId();
        if (!StringUtils.isEmpty(newMonitoringMethod) && !newMonitoringMethod.equalsIgnoreCase(oldMonitoringMethod)) {
            if (!StringUtils.isEmpty(oldMonitoringMethod)) {
                closeCase(episodeId, requestInput, processedRequestInput);
                registerCase(episodeId, requestInput, processedRequestInput, episodeTransitionDetails.getEpisodeDtoAfterUpdate().getStageData());
                saveMonitoringMethodChangedLogs(episodeId, oldMonitoringMethod, newMonitoringMethod, requestInput);
            } else {
                registerCase(episodeId, requestInput, processedRequestInput, episodeTransitionDetails.getEpisodeDtoAfterUpdate().getStageData());
            }
        }
    }

    private void reopenEntity(EpisodeTransitionDetails episodeTransitionDetails, Map<String, Object> requestInput, Map<String, Object> processedRequestInput)
    {
        String oldMonitoringMethod = Utils.convertObjectToStringWithNull(episodeTransitionDetails.getEpisodeDtoBeforeUpdate().getStageData().get(IAMField.MONITORING_METHOD.getFieldName()));
        Long episodeId = episodeTransitionDetails.getEpisodeDtoBeforeUpdate().getId();
        if (!AdherenceMonitoringMethod.MERM.getName().equals(oldMonitoringMethod)) {
            processedRequestInput.put(IAMField.MONITORING_METHOD.getFieldName(), oldMonitoringMethod);
            EntityRequest entityRequest = constructEntityRequest(episodeId, requestInput, processedRequestInput, episodeTransitionDetails.getEpisodeDtoAfterUpdate().getStageData());
            ResponseEntity<Response<Map<String, Object>>> response = iamService.reopenEntity(entityRequest);
            Utils.processResponseEntity(response, IAMValidation.UNABLE_TO_OPEN_REOPEN_CASE.getMessage());
        }
    }

    private void closeEntity(EpisodeTransitionDetails episodeTransitionDetails, Map<String, Object> requestInput, Map<String, Object> processedRequestInput)
    {
        Long episodeId = episodeTransitionDetails.getEpisodeDtoBeforeUpdate().getId();
        closeCase(episodeId, requestInput, processedRequestInput);
        boolean deleteIfRequired = Boolean.parseBoolean(String.valueOf(requestInput.get(IAMField.DELETE_IF_REQUIRED.getFieldName())));
        if (deleteIfRequired)
        {
            String endDate = Utils.convertObjectToStringWithNull(processedRequestInput.get(IAMField.IAM_END_DATE.getFieldName()));
            endDate = Utils.convertDateFromCurrentToExpectedFormatWithNull(endDate, Constants.DATE_FORMAT, Constants.DATE_TIME_FORMAT);
            episodeLogService.syncLogs(episodeId, endDate);
            episodeTagService.deleteAllTagsAfterEndDate(episodeId, endDate);
            saveMonitoringMethodDeletedSwitchesLogs(episodeId, endDate, requestInput);
        }
    }

    @Override
    public Map<String, Object> getAdherenceDetails(Long episodeId)
    {
        Map<String, Object> processedAdherenceResponseMap = new HashMap<>();
        if (null != episodeId)
        {
            ResponseEntity<Response<AdherenceResponse>> responseEntity = iamService.getAdherence(episodeId, false);
            if (Utils.isResponseValid(responseEntity)) {
                AdherenceResponse adherenceResponse = responseEntity.getBody().getData();
                processedAdherenceResponseMap = Utils.objectToMap(adherenceResponse);
            }
        }
        return processedAdherenceResponseMap;
    }

}

package com.everwell.transition.controller;

import com.everwell.transition.enums.eventStreamingEnums.EventAction;
import com.everwell.transition.enums.eventStreamingEnums.EventCategory;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.dto.ElasticDocTagsWrapper;
import com.everwell.transition.model.dto.EventStreamingAnalyticsDto;
import com.everwell.transition.model.request.episodetags.EpisodeTagBulkDeletionRequest;
import com.everwell.transition.model.request.episodetags.EpisodeTagDeletionRequest;
import com.everwell.transition.model.request.episodetags.EpisodeTagRequest;
import com.everwell.transition.model.request.episodetags.SearchTagsRequest;
import com.everwell.transition.model.response.EpisodeTagResponse;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.response.TagsConfigResponse;
import com.everwell.transition.service.EpisodeTagService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EpisodeTagController {

    private static Logger LOGGER = LoggerFactory.getLogger(EpisodeTagController.class);

    @Autowired
    private EpisodeTagService episodeTagService;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @GetMapping("/v1/tags")
    public ResponseEntity<Response<EpisodeTagResponse>> getTags(@RequestParam(value = "episodeId") Long episodeId) {
        LOGGER.info("[getTags] received request for episodeId: " + episodeId);
        if (episodeId == null || episodeId == 0)
            throw new ValidationException("episodeId must not be null!");
        EpisodeTagResponse episodeTagResponse = episodeTagService.getEpisodeTags(episodeId);
        applicationEventPublisher.publishEvent(new EventStreamingAnalyticsDto(EventCategory.EPISODE_TAGS.getName(),episodeId.toString(), EventAction.TAGS_GET.getName()));
        return new ResponseEntity<>(new Response<>(episodeTagResponse), HttpStatus.OK);
    }

    @PostMapping("/v1/tags/bulk")
    public ResponseEntity<Response<List<EpisodeTagResponse>>> getTagsWithFilter(@RequestBody SearchTagsRequest searchTagsRequest) {
        LOGGER.info("[getTagsWithFilter] received request: " + searchTagsRequest);
        searchTagsRequest.validate();
        List<EpisodeTagResponse> episodeTagResponse = episodeTagService.search(searchTagsRequest);
        applicationEventPublisher.publishEvent(searchTagsRequest);
        return new ResponseEntity<>(new Response<>(episodeTagResponse), HttpStatus.OK);
    }

    @PostMapping("/v1/tags")
    public ResponseEntity<Response<List<EpisodeTagResponse>>> saveTags(@RequestBody EpisodeTagRequest episodeTagRequest) {
        LOGGER.info("[saveTags] received request: " + episodeTagRequest);
        episodeTagRequest.validate();
        List<EpisodeTagResponse> episodeTagResponse = episodeTagService.save(episodeTagRequest.getEpisodeTagData());
        applicationEventPublisher.publishEvent(episodeTagRequest);
        applicationEventPublisher.publishEvent(new ElasticDocTagsWrapper(episodeTagResponse));
        return new ResponseEntity<>(new Response<>(episodeTagResponse), HttpStatus.OK);
    }

    @Deprecated
    @PostMapping("/v1/tags/delete")
    public ResponseEntity<Response<String>> deleteTags(@RequestBody EpisodeTagDeletionRequest episodeTagDeletionRequest) {
        LOGGER.info("[deleteTags] received request: " + episodeTagDeletionRequest);
        episodeTagDeletionRequest.validate();
        episodeTagService.delete(episodeTagDeletionRequest);
        return new ResponseEntity<>(new Response<>(true, "Deleted Successfully!"), HttpStatus.OK);
    }


    @PostMapping("/v1/tags/delete/bulk")
    public ResponseEntity<Response<String>> deleteTagsBulk(@RequestBody EpisodeTagBulkDeletionRequest episodeTagBulkDeletionRequest) {
        LOGGER.info("[deleteTagsBulk] received request: " + episodeTagBulkDeletionRequest);
        episodeTagBulkDeletionRequest.validate();
        episodeTagService.deleteBulk(episodeTagBulkDeletionRequest);
        applicationEventPublisher.publishEvent(episodeTagBulkDeletionRequest);
        return new ResponseEntity<>(new Response<>(true, "Deleted Successfully!"), HttpStatus.OK);
    }

    @GetMapping("/v1/tags/config")
    public ResponseEntity<Response<List<TagsConfigResponse>>> getTagsConfig() {
        LOGGER.info("[getTagsConfig] received request");
        List<TagsConfigResponse> tagsConfigResponse = episodeTagService.getEpisodeConfig();
        applicationEventPublisher.publishEvent(new EventStreamingAnalyticsDto(EventCategory.EPISODE_TAGS.getName(), EventAction.TAGS_GET_CONFIG.getName(),EventAction.TAGS_GET.getName()));
        return new ResponseEntity<>(new Response<>(tagsConfigResponse), HttpStatus.OK);
    }

}

package com.everwell.transition.controller;

import com.everwell.transition.elasticsearch.indices.EpisodeIndex;
import com.everwell.transition.model.db.Episode;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.dto.dispensation.DispensationData;
import com.everwell.transition.model.dto.dispensation.ProductStockData;
import com.everwell.transition.model.request.dispensation.*;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.model.response.dispensation.*;
import com.everwell.transition.service.DispensationService;
import com.everwell.transition.service.EpisodeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class DispensationController {

    private static Logger LOGGER = LoggerFactory.getLogger(DispensationController.class);

    @Autowired
    DispensationService dispensationService;

    @Autowired
    EpisodeService episodeService;

    @Autowired
    ClientService clientService;


    @RequestMapping(value = "/v1/product/search", method = RequestMethod.POST)
    public ResponseEntity<Response<List<ProductResponse>>> searchProducts(@RequestBody ProductSearchRequest productSearchRequest) {
        LOGGER.info("[searchProducts] product search request " + productSearchRequest);
        return dispensationService.searchProducts(productSearchRequest);
    }

    @RequestMapping(value = "/v1/product/stocks/search", method = RequestMethod.POST)
    public ResponseEntity<Response<List<ProductStockData>>> productStockSearch(@RequestBody ProductStockSearchRequest productStockSearchRequest) {
        LOGGER.info(" [productStockSearch] request received" + productStockSearchRequest);
        ResponseEntity<Response<List<ProductStockData>>> res = dispensationService.productStockSearch(productStockSearchRequest);

        if(res != null) {
            
            HttpHeaders headers = new HttpHeaders();
            headers.add("Connection", "keep-alive");

            return ResponseEntity
                    .status(res.getStatusCode())
                    .headers(headers)
                    .body(res.getBody());
        }

        return res;
    }

    @RequestMapping(value = "/v1/dispensation/entity/{entityId}", method = RequestMethod.GET)
    public ResponseEntity<Response<EntityDispensationResponse>> getDispensationDetailsForEntity(@PathVariable Long entityId, @RequestParam(required = false) boolean includeProductLog) {
        LOGGER.info("[getDispensationDetailsForEntity] request received: " + entityId);
        return dispensationService.getAllDispensationDataForEntity(entityId, includeProductLog);
    }

    @RequestMapping(value = "/v1/dispensation/{id}", method = RequestMethod.GET)
    public ResponseEntity<Response<DispensationData>> getDispensationDetails(@PathVariable Long id) {
        LOGGER.info("[getDispensationDetails] request received: " + id);
        return dispensationService.getDispensation(id);
    }

    @RequestMapping(value = "/v1/dispensation", method = RequestMethod.POST)
    public ResponseEntity<Response<DispensationResponse>> createDispensation(@RequestBody DispensationRequest dispensationRequest) {
        LOGGER.info("[createDispensation] request received: " + dispensationRequest);
        return dispensationService.createDispensation(dispensationRequest);
    }

    @RequestMapping(value = "/v2/dispensation", method = RequestMethod.POST)
    public ResponseEntity<Response<DispensationResponse>> createDispensationInventory(@RequestBody DispensationRequest dispensationRequest) {
        LOGGER.info("[createDispensationInventory] request received: " + dispensationRequest);
        return dispensationService.createDispensationInventory(dispensationRequest);
    }

    @RequestMapping(value = "/v1/dispensation/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Response<DispensationResponse>> deleteDispensation(@PathVariable Long id) {
        LOGGER.info("[deleteDispensation] request received: " + id);
        return dispensationService.deleteDispensation(id);
    }

    @RequestMapping(value = "/v1/dispensation/entity/search", method = RequestMethod.POST)
    public ResponseEntity<Response<List<EntityDispensationResponse>>> searchDispensation(@RequestBody SearchDispensationRequest searchDispensationRequest) {
        LOGGER.info("[searchDispensation] request received: " + searchDispensationRequest);
        return dispensationService.searchDispensationsForEntity(searchDispensationRequest);
    }

    @RequestMapping(value = "/v1/dispensation/return", method = RequestMethod.POST)
    public ResponseEntity<Response<ReturnDispensationResponse>> returnDispensation(@RequestBody ReturnDispensationRequest returnDispensationRequest) {
        LOGGER.info("[returnDispensation] request received: " + returnDispensationRequest);
        return dispensationService.returnDispensation(returnDispensationRequest);
    }

    @RequestMapping(value = "/v2/dispensation/return", method = RequestMethod.POST)
    public ResponseEntity<Response<ReturnDispensationResponse>> returnDispensationInventory(@RequestBody ReturnDispensationRequest returnDispensationRequest) {
        LOGGER.info("[returnDispensationInventory] request received: " + returnDispensationRequest);
        return dispensationService.returnDispensationInventory(returnDispensationRequest);
    }

    @RequestMapping(value = "/v1/product", method = RequestMethod.POST)
    public ResponseEntity<Response<ProductResponse>> addProduct(@RequestBody ProductRequest productRequest) {
        LOGGER.info(" [addProduct] request received: " + productRequest);
        return dispensationService.addProduct(productRequest);
    }

    @RequestMapping(value = "/v1/products", method = RequestMethod.POST)
    public ResponseEntity<Response<List<ProductResponse>>> addProductBulk(@RequestBody ProductBulkRequest productBulkRequest) {
        LOGGER.info(" [addProductBulk] request received: " + productBulkRequest);
        return dispensationService.addProductBulk(productBulkRequest);
    }

    @RequestMapping(value = {"/v1/product/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<Response<ProductResponse>> getProductDetails(@PathVariable Long id) {
        LOGGER.info(" [getProductDetails] request received: " + id);
        return dispensationService.getProduct(id);
    }

    @RequestMapping(value = "/v1/stock", method = RequestMethod.POST)
    public ResponseEntity<Response<Void>> stockCredit(@RequestBody StockRequest stockRequest) {
        LOGGER.info("[stockCredit] request received: " + stockRequest);

        ResponseEntity<Response<Void>> res = dispensationService.stockCredit(stockRequest);

        if(res != null) {
            HttpHeaders headers = new HttpHeaders();
            headers.add("Connection", "keep-alive");

            return ResponseEntity
                    .status(res.getStatusCode())
                    .headers(headers)
                    .body(res.getBody());
        }

        return res;
    }

    @RequestMapping(value = "/v1/stock", method = RequestMethod.PUT)
    public ResponseEntity<Response<Void>> stockDebit(@RequestBody StockRequest stockRequest) {
        LOGGER.info("[stockDebit request received: " + stockRequest);
        return dispensationService.stockDebit(stockRequest);
    }

    @GetMapping(value = "/v1/dispensation/weight-band")
    public ResponseEntity<Response<DispensationWeightBandResponse>> getWeightBandForEpisode(@RequestParam(name = "episodeId") Long episodeId) {
        LOGGER.info("[getWeightBandForEpisode] request received with episode id: " + episodeId);
        DispensationWeightBandResponse dispensationWeightBandResponse = dispensationService.getWeightBandForEpisode(episodeId);
        return new ResponseEntity<>(new Response<>(dispensationWeightBandResponse), HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/stocks", method = RequestMethod.PUT)
    public ResponseEntity<Response<Void>> stockUpdate(@RequestBody Map<String,Object> stockRequest) {
        LOGGER.info("[stockUpdate] request received: " + stockRequest);
        return dispensationService.stockUpdate(stockRequest);
    }

    @RequestMapping(value = "/v1/dispensation/episode", method = RequestMethod.POST)
    public ResponseEntity<Response<DispensationResponse>> createDispensationForEpisode(@RequestBody DispensationRequest dispensationRequest) {
        LOGGER.info("[createDispensationForEpisode] request received: " + dispensationRequest);
        EpisodeDto episodeDto = episodeService.getEpisodeDetails(dispensationRequest.getEntityId(), clientService.getClientByTokenOrDefault().getId(), false, false, false, false);
        dispensationRequest.validate(dispensationRequest, episodeDto);
        return dispensationService.createDispensationEpisode(dispensationRequest, episodeDto);
    }

    @RequestMapping(value = "/v1/dispensation/{dispensationId}/entity", method = RequestMethod.GET)
    public ResponseEntity<Response<EntityDispensationResponse>> getEpisodeDispensationData(@PathVariable Long dispensationId) {
        LOGGER.info("[getEpisodeDispensationData] request received: " + dispensationId);
        return dispensationService.getEpisodeDispensationData(dispensationId);
    }

}

package com.everwell.transition.controller;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.exceptions.NotFoundException;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.db.EpisodeLog;
import com.everwell.transition.model.db.TreatmentPlanProductMap;
import com.everwell.transition.model.dto.AdhTechLogsData;
import com.everwell.transition.model.dto.AdherenceMonthWiseData;
import com.everwell.transition.model.request.adherence.*;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.request.episodetags.SearchTagsRequest;
import com.everwell.transition.model.response.*;
import com.everwell.transition.model.response.adherence.AllAvgAdherenceResponse;
import com.everwell.transition.model.response.adherence.PositiveCountResponse;
import com.everwell.transition.model.response.adherence.AdherenceGlobalConfigResponse;
import com.everwell.transition.model.response.adherence.EditDoseBulkResponse;
import com.everwell.transition.service.*;
import com.everwell.transition.utils.Utils;
import io.sentry.Sentry;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@RestController
public class AdherenceController {

    private static Logger LOGGER = LoggerFactory.getLogger(AdherenceController.class);

    @Autowired
    IAMService iamService;

    @Autowired
    AdherenceService adherenceService;

    @Autowired
    EpisodeTagService episodeTagService;

    @Autowired
    EpisodeLogService episodeLogService;

    @Autowired
    EpisodeService episodeService;

    @Autowired
    TreatmentPlanService treatmentPlanService;

    @Autowired
    ClientService clientService;

    @Autowired
    @Qualifier("episodeTaskExecutor")
    TaskExecutor taskExecutor;

    @GetMapping(value = "/v1/adherence/config")
    public ResponseEntity<Response<List<AdherenceCodeConfigResponse>>> getAdherenceConfig() {
        LOGGER.info("[getAdherenceConfig] request accepted");
        Response<List<AdherenceCodeConfigResponse>> response = new Response<>(adherenceService.getAdherenceConfig());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/entity", method = RequestMethod.PUT)
    public ResponseEntity<Response<Map<String, Object>>> updateEntity(@RequestBody EntityRequest entityRequest) {
        LOGGER.info("[updateEntity] request accepted");
        return iamService.updateEntity(entityRequest);
    }
    @RequestMapping(value = "/v1/entity", method = RequestMethod.DELETE)
    public ResponseEntity<Response<Map<String, Object>>> deleteEntity(@RequestParam(value = "entityId") String entityId, @RequestParam(value = "endDate") String endDate,
                                                         @RequestParam(value = "updateEndDate", defaultValue = "false") boolean updateEndDate,
                                                         @RequestParam(value = "deleteIfRequired", defaultValue = "false") boolean deleteIfRequired) {
        LOGGER.debug("[deleteEntity] request received : " + entityId + ", " + endDate + ", " + deleteIfRequired + ", " + updateEndDate);
        return iamService.deleteEntity(String.valueOf(entityId), endDate, updateEndDate, deleteIfRequired);
    }

    @PostMapping(value = "/v1/adherence-global-config")
    public ResponseEntity<Response<AdherenceGlobalConfigResponse>> getAdherenceGlobalConfig(Map<String, Object> configRequest) {
        LOGGER.debug("[getAdherenceGlobalConfig] request accepted : " + configRequest);
        return iamService.getAdherenceGlobalConfig(configRequest);
    }

    @GetMapping(value = "/v1/adherence/{episodeId}")
    public ResponseEntity<Response<AdherenceResponse>> getAdherence(@PathVariable String episodeId,
                                                                    @RequestParam(defaultValue = "false") Boolean logsRequired,
                                                                    @RequestParam(required = false) String iamEntity) {
        LOGGER.info("[getAdherence] request accepted");
        EpisodeDto episodeDto = episodeService.getEpisodeDetails(Long.valueOf(episodeId), clientService.getClientByTokenOrDefault().getId());
        AdherenceResponse adherenceResponse;
        if (episodeDto.getStageData().containsKey(FieldConstants.ESD_TREATMENT_PLAN_ID)) {
            Long treatmentPlanId = Long.valueOf(String.valueOf(episodeDto.getStageData().get(FieldConstants.ESD_TREATMENT_PLAN_ID)));
            adherenceResponse = getAdherenceByEpisodeIdForBackSupport(treatmentPlanId, iamEntity, logsRequired);
        } else {
            String entityId = iamEntity != null ? iamEntity : episodeId;
            adherenceResponse = iamService.getAdherence(entityId, logsRequired).getBody().getData();
        }
        return ResponseEntity.ok(new Response<>(true, adherenceResponse));
    }

    AdherenceResponse getAdherenceByEpisodeIdForBackSupport(Long treatmentPlanId, String iamEntity, Boolean logsRequired) {
        AdherenceResponse adherenceResponse;
        if (StringUtils.isEmpty(iamEntity)) {
            List<String> iamEntityIds =
                    treatmentPlanService
                            .getTreatmentPlanProductMapList(treatmentPlanId)
                            .stream()
                            .filter(f -> !f.getSos())
                            .map(m -> Constants.IAM_ACCESS_MAPPING_ENTITY_PREFIX_MED + "_" + m.getId())
                            .collect(Collectors.toList());
            AllAdherenceResponse allAdherenceResponse = adherenceService.getAdherenceBulk(new SearchAdherenceRequest(iamEntityIds, null), true);
            LocalDateTime minDate = allAdherenceResponse.getAdherenceResponseList().stream().map(AdherenceResponse::getStartDate).min(LocalDateTime::compareTo).get();
            adherenceResponse = allAdherenceResponse.getAdherenceResponseList().get(0);
            adherenceResponse.setStartDate(minDate);
            adherenceResponse.getAdherenceData().get(0).setAdherenceString(allAdherenceResponse.getMergedAdherence());
            adherenceResponse.setAdherenceString(allAdherenceResponse.getMergedAdherence()); // imp
            List<AdhTechLogsData> adhTechLogsData = new ArrayList<>();
            for (String iamEntityId : iamEntityIds) {
                ResponseEntity<Response<AdherenceResponse>> response = iamService.getAdherence(iamEntityId, logsRequired);
                if (response.getBody() != null && response.getBody().getData().getAdherenceData().get(0).getAdhTechLogsData() != null)
                    adhTechLogsData.addAll(response.getBody().getData().getAdherenceData().get(0).getAdhTechLogsData());
            }
            adherenceResponse.getAdherenceData().get(0).setAdhTechLogsData(logsRequired ? adhTechLogsData : null);
        } else {
            adherenceResponse = iamService.getAdherence(iamEntity, logsRequired).getBody().getData();
        }
        return adherenceResponse;
    }

    @PostMapping(value = "/v1/adherence/search")
    public ResponseEntity<Response<AllAdherenceResponse>> getAdherenceBulk(@RequestBody SearchAdherenceRequest adherenceRequest, @RequestParam(value = "merge", required = false) Boolean merge) {
        LOGGER.info("[getAdherenceBulk] bulk adherence request " + adherenceRequest);
        return iamService.getAdherenceBulk(adherenceRequest, merge);
    }

    @GetMapping("/v1/adherence/monthly/{episodeId}")
    public ResponseEntity<Response<AdherenceDetailsWithMonths>> getAdherenceForEpisodeMonthWise(@PathVariable Long episodeId, @RequestParam(value = "timezone") String timezone) {
        LOGGER.info("[getAdherenceForEpisodeMonthWise] request accepted");
        //validate timezone sent
        ZoneId zoneId = null;
        try {
            zoneId = ZoneId.of(timezone);
        } catch (DateTimeException e) {
            throw new ValidationException(timezone + " - Timezone does not exists!");
        }
        ResponseEntity<Response<AdherenceResponse>> response = iamService.getAdherence(episodeId, false);
        if (response.getBody() == null || (response.getBody() != null && !response.getBody().isSuccess())) {
            throw new NotFoundException(response.getBody().getMessage());
        }
        EpisodeTagResponse tagResponse = episodeTagService.getEpisodeTags(episodeId);
        List<AdherenceMonthWiseData> adherenceMonthWiseData = adherenceService.formatAdherenceStringMonthWise(response.getBody().getData(), tagResponse, zoneId);
        LocalDateTime lastDosageLocal = null;
        if (response.getBody().getData().getLastDosage() != null)
            lastDosageLocal = ZonedDateTime.of(response.getBody().getData().getLastDosage(), ZoneOffset.UTC).toOffsetDateTime().atZoneSameInstant(zoneId).toLocalDateTime();
        AdherenceDetailsWithMonths adherenceDetailsWithMonths = new AdherenceDetailsWithMonths(
                Long.valueOf(response.getBody().getData().getEntityId()),
                lastDosageLocal,
                response.getBody().getData().getTechnologyDoses(),
                response.getBody().getData().getManualDoses(),
                response.getBody().getData().getTotalDoses(),
                adherenceMonthWiseData
        );
        return new ResponseEntity<>(new Response<>(adherenceDetailsWithMonths), HttpStatus.OK);
    }

    @PostMapping("/v1/adherence/monthly/bulk")
    public ResponseEntity<Response<List<AdherenceDetailsWithMonths>>> getAdherenceForEpisodeMonthWiseBulk(@RequestBody MonthlyAdherenceBulkRequest monthlyAdherenceBulkRequest) {
        LOGGER.info("[getAdherenceForEpisodeMonthWiseBulk] request accepted");
        monthlyAdherenceBulkRequest.validate();
        SearchAdherenceRequest adherenceRequest = new SearchAdherenceRequest(
                monthlyAdherenceBulkRequest.getEpisodeIdList().stream().map(String::valueOf).collect(Collectors.toList()),
                null
        );
        SearchTagsRequest searchTagsRequest = new SearchTagsRequest(
                monthlyAdherenceBulkRequest.getEpisodeIdList(),
                null
        );
        //add more requests here
        CompletableFuture<ResponseEntity<Response<AllAdherenceResponse>>> adherenceFuture = CompletableFuture.supplyAsync(() -> iamService.getAdherenceBulk(adherenceRequest), taskExecutor);
        CompletableFuture<List<EpisodeTagResponse>> tagsFuture = CompletableFuture.supplyAsync(() -> episodeTagService.search(searchTagsRequest));

        AllAdherenceResponse allAdherenceResponse;
        List<EpisodeTagResponse> tagResponseList;

        try {
            ResponseEntity<Response<AllAdherenceResponse>> allAdherenceResponseEntity = adherenceFuture.get();
            if (allAdherenceResponseEntity == null || allAdherenceResponseEntity.getBody() == null)
                throw new NotFoundException("Unable to fetch bulk adherence");
            // if success is false
            if (allAdherenceResponseEntity.getBody() != null && !allAdherenceResponseEntity.getBody().isSuccess()) {
                throw new NotFoundException(allAdherenceResponseEntity.getBody().getMessage());
            }
            // if success, but list is empty
            if (allAdherenceResponseEntity.getBody().getData().getAdherenceResponseList().isEmpty()) {
                throw new NotFoundException("Entity mappings do not exist");
            }
            allAdherenceResponse = allAdherenceResponseEntity.getBody().getData();
            tagResponseList = tagsFuture.get();
        } catch (InterruptedException | ExecutionException e) {
            throw new NotFoundException("[getAdherenceForEpisodeMonthWiseBulk] Unable to process request currently!");
        }
        Map<Long, AdherenceResponse> episodeIdToAdherence = allAdherenceResponse.getAdherenceResponseList().stream().collect(Collectors.toMap(k -> Long.valueOf(k.getEntityId()), v -> v));
        Map<Long, EpisodeTagResponse> episodeIdToTagResponse = tagResponseList.stream().collect(Collectors.toMap(EpisodeTagResponse::getEpisodeId, v -> v));

        List<AdherenceDetailsWithMonths> allMonthlyData = new ArrayList<>();
        ZoneId zoneId = ZoneId.of(monthlyAdherenceBulkRequest.getTimezone());
        monthlyAdherenceBulkRequest.getEpisodeIdList().forEach(episodeId -> {
            AdherenceResponse adherenceResponse = episodeIdToAdherence.getOrDefault(episodeId, new AdherenceResponse());
            if (adherenceResponse.getEntityId() != null) {
                List<AdherenceMonthWiseData> adherenceMonthWiseData = adherenceService.formatAdherenceStringMonthWise(
                        adherenceResponse,
                        episodeIdToTagResponse.getOrDefault(episodeId, new EpisodeTagResponse()),
                        zoneId
                );
                LocalDateTime lastDosageLocal = null;
                if (adherenceResponse.getLastDosage() != null)
                    lastDosageLocal = ZonedDateTime.of(adherenceResponse.getLastDosage(), ZoneOffset.UTC).toOffsetDateTime().atZoneSameInstant(zoneId).toLocalDateTime();
                allMonthlyData.add(new AdherenceDetailsWithMonths(
                        Long.valueOf(adherenceResponse.getEntityId()),
                        lastDosageLocal,
                        adherenceResponse.getTechnologyDoses(),
                        adherenceResponse.getManualDoses(),
                        adherenceResponse.getTotalDoses(),
                        adherenceMonthWiseData
                ));
            }
        });

        return new ResponseEntity<>(new Response<>(allMonthlyData), HttpStatus.OK);
    }

    @PutMapping("/v1/adherence/edit-doses")
    public ResponseEntity<Response<EditDosesResponse>> editDoses(@RequestBody EditDosesRequest editDosesRequest) {
        LOGGER.info("[editDoses] request accepted");
        editDosesRequest.validate(editDosesRequest);
        Long clientId = clientService.getClientByTokenOrDefault().getId();
        Set<String> iamEntityIdList = new HashSet<>();
        // since episode ids are Long always and linked IAM ids can be strings
        // so if iamEntityId is not sent, we fallback to episodeId
        if (editDosesRequest.getIamEntityId() == null) {
            // for backwards compatibility
            EpisodeDto episodeDto = episodeService.getEpisodeDetails(editDosesRequest.getEpisodeId(), clientId, false, false, false, false);
            if (episodeDto.getStageData().containsKey(FieldConstants.ESD_TREATMENT_PLAN_ID)) {
                List<String> timeList = editDosesRequest.getUtcDates().stream().map(m -> m.split(" ")[1]).collect(Collectors.toList());
                List<TreatmentPlanProductMap> treatmentPlanProductMaps = treatmentPlanService.getMedicinesForSchedule(timeList, Long.valueOf(String.valueOf(episodeDto.getStageData().get(FieldConstants.ESD_TREATMENT_PLAN_ID))));
                Set<TreatmentPlanProductMap> eligibleMedsForDate = new HashSet<>();
                for (String utcDateStr : editDosesRequest.getUtcDates()) {
                    LocalDateTime utcDate = Utils.convertStringToDateNew(utcDateStr, "dd-MM-yyyy HH:mm:ss");
                    eligibleMedsForDate.addAll(treatmentPlanProductMaps.stream().filter(
                            f -> (!utcDate.isBefore(f.getStartDate()))
                                && (f.getEndDate() == null || !utcDate.isAfter(f.getEndDate()))
                    ).collect(Collectors.toSet()));
                }
                List<EditDosesRequest> editDosesRequestList = new ArrayList<>();
                for (TreatmentPlanProductMap treatmentPlanProductMap : eligibleMedsForDate) {
                    EditDosesRequest request = SerializationUtils.clone(editDosesRequest);
                    String entityId = Constants.IAM_ACCESS_MAPPING_ENTITY_PREFIX_MED + "_" + treatmentPlanProductMap.getId();
                    request.setIamEntityId(entityId);
                    request.setFetchAdherence(true);
                    iamEntityIdList.add(entityId);
                    editDosesRequestList.add(request);
                }
                EditDoseBulkResponse response = editDosesBulk(new EditDoseBulkRequest(editDosesRequestList)).getBody().getData();
                response.getEditDosesResponseList().get(0).setAdherenceString(response.getMergedAdherence());
                return new ResponseEntity<>(new Response<>(response.getEditDosesResponseList().get(0)), HttpStatus.OK);
            }
            editDosesRequest.setIamEntityId(String.valueOf(editDosesRequest.getEpisodeId()));
        }
        iamEntityIdList.add(editDosesRequest.getIamEntityId());
        //validate timezone sent
        ZoneId zoneId;
        try {
            zoneId = ZoneId.of(editDosesRequest.getTimezone(), ZoneId.SHORT_IDS);
        } catch (DateTimeException e) {
            throw new ValidationException(editDosesRequest.getTimezone() + " - Timezone does not exists!");
        }

        List<LocalDateTime> dates = adherenceService.editDoses(editDosesRequest);
        EpisodeLog episodeLog = new EpisodeLog();
        if (editDosesRequest.getAddLogs()) {
            episodeLog = episodeLogService.addAdherenceLogs(editDosesRequest, dates, zoneId);
        }

        // Return because no response data is needed
        if (!editDosesRequest.getFetchAdherence())
            return new ResponseEntity<>(new Response<>(new EditDosesResponse(episodeLog.getActionTaken(), episodeLog.getComments(), episodeLog.getAddedOn(), null)), HttpStatus.OK);

        String adherenceString;
        try {
            ResponseEntity<Response<AllAdherenceResponse>> adherenceResponse = iamService.getAdherenceBulk(new SearchAdherenceRequest(new ArrayList<>(iamEntityIdList), null), true);
            adherenceString = adherenceResponse.getBody().getData().getMergedAdherence();
        } catch (Exception e) {
            Sentry.capture(e);
            LOGGER.error(e.toString());
            throw new ValidationException("Unable to fetch Updated Adherence");
        }
        EditDosesResponse editDosesResponse = new EditDosesResponse(episodeLog.getActionTaken(), episodeLog.getComments(), episodeLog.getAddedOn(), adherenceString);
        return new ResponseEntity<>(new Response<>(editDosesResponse), HttpStatus.OK);
    }

    @GetMapping(value = "/v1/imei")
    public ResponseEntity<Response<List<Map<String, Object>>>> getImei(@RequestParam String entityId, @RequestParam(defaultValue = "false")  boolean active) {
        LOGGER.debug("[getImei] request received for: " + entityId);
        return iamService.getImei(entityId, active);
    }

    @PostMapping(value = "/v1/imei/bulk")
    public ResponseEntity<Response<List<Map<String, Object>>>> getImeiBulk(@RequestBody Map<String, Object> imeiRequest) {
        LOGGER.debug("[getImeiBulk] request received for: " + imeiRequest);
        return iamService.getImeiBulk(imeiRequest);
    }

    @GetMapping(value = "/v1/imei/entity")
    public ResponseEntity<Response<List<Map<String, Object>>>> getActiveEntityFromImei(@RequestParam String imei) {
        LOGGER.debug("[getActiveEntityFromImei] request received: " + imei);
        return iamService.getActiveEntityFromImei(imei);
    }

    @PutMapping(value = "/v1/vot/video/status")
    public ResponseEntity<Response<Map<String, Object>>> updateVideoStatus(@RequestBody Map<String, Object> reviewAdherenceRequest) {
        LOGGER.debug("[updateVideoStatus] request received: " + reviewAdherenceRequest);
        return iamService.updateVideoStatus(reviewAdherenceRequest);
    }

    @PostMapping(value = "/v1/adherence/positive-events")
    public ResponseEntity<Response<PositiveCountResponse>> getPositiveEventsCount(@RequestBody PositiveAdherenceRequest positiveAdherenceRequest) {
        LOGGER.debug("[getPositiveEventsCount] request received: " + positiveAdherenceRequest);
        return iamService.getPositiveEventCount(positiveAdherenceRequest);
    }

    @PostMapping(value = "/v1/adherence/average")
    public ResponseEntity<Response<AllAvgAdherenceResponse>> getAverageAdherence(@RequestBody AverageAdherenceRequest averageAdherenceRequest) {
        LOGGER.debug("[getAverageAdherence] request received: " + averageAdherenceRequest);
        return iamService.getAverageAdherence(averageAdherenceRequest);
    }

    @PutMapping(value = "v1/imei")
    public ResponseEntity<Response<Long>> reallocateImei(@RequestBody Map<String, Object> imeiEditRequest) {
        LOGGER.debug("[reallocateImei] request received: " + imeiEditRequest);
        return iamService.reallocateIMEI(imeiEditRequest);
    }

    @PutMapping(value = "v1/phone")
    public ResponseEntity<Response<String>> editPhone(@RequestBody Map<String, Object> phoneEditDto) {
        LOGGER.debug("[editPhone] request received: "+ phoneEditDto);
        return iamService.editPhone(phoneEditDto);
    }

    @PutMapping("/v1/adherence/edit-doses/bulk")
    public ResponseEntity<Response<EditDoseBulkResponse>> editDosesBulk(@RequestBody EditDoseBulkRequest editDosesBulkRequest) {
        LOGGER.info("[editDosesBulk] request accepted");
        editDosesBulkRequest.validate();
        boolean fetchAdherenceAny = false;
        Long clientId = clientService.getClientByTokenOrDefault().getId();
        List<EditDosesResponse> editDosesResponseList = new ArrayList<>();
        for (EditDosesRequest editDosesRequest : editDosesBulkRequest.getEditDosesRequestList()) {
            // since episode ids are Long always and linked IAM ids can be strings
            // so if iamEntityId is not sent, we fallback to episodeId
            if (editDosesRequest.getIamEntityId() == null)
                editDosesRequest.setIamEntityId(String.valueOf(editDosesRequest.getEpisodeId()));
            //validate timezone sent
            try {
                ZoneId.of(editDosesRequest.getTimezone(), ZoneId.SHORT_IDS);
            } catch (DateTimeException e) {
                throw new ValidationException(editDosesRequest.getTimezone() + " - Timezone does not exists!");
            }
            fetchAdherenceAny |= editDosesRequest.getFetchAdherence();
        }
        Map<String, List<LocalDateTime>> entityToLogs = adherenceService.editDosesBulk(editDosesBulkRequest);
        Map<String, EpisodeLog> episodeLogMap = new HashMap<>();
        // add logs always
        for (EditDosesRequest editDosesRequest : editDosesBulkRequest.getEditDosesRequestList()) {
            ZoneId zoneId = ZoneId.of(editDosesRequest.getTimezone(), ZoneId.SHORT_IDS);
            EpisodeLog episodeLog = episodeLogService.addAdherenceLogs(editDosesRequest, entityToLogs.get(editDosesRequest.getIamEntityId()), zoneId);
            episodeLogMap.putIfAbsent(editDosesRequest.getIamEntityId(), episodeLog);
        }
        Map<String, String> adherenceStringMap = new HashMap<>();
        List<String> entityIds = new ArrayList<>(entityToLogs.keySet());
        String mergedAdherence = null;
        if (fetchAdherenceAny) {
            try {
                // since we return the adherence string as well, we cannot return adh string of only doses
                // which are sent in request, need to send merged adh string for all the meds which belong
                // to that episode. To keep it simple doing it only for 1 episode id from the request
                EpisodeDto episodeDto = episodeService.getEpisodeDetails(editDosesBulkRequest.getEditDosesRequestList().get(0).getEpisodeId(), clientId, false, false, false, false);
                if (episodeDto.getStageData().containsKey(FieldConstants.ESD_TREATMENT_PLAN_ID)) {
                    Long treatmentPlanId = Long.valueOf(String.valueOf(episodeDto.getStageData().get(FieldConstants.ESD_TREATMENT_PLAN_ID)));
                    entityIds = new ArrayList<>(treatmentPlanService
                            .getTreatmentPlanProductMapList(treatmentPlanId)
                            .stream()
                            .map(m -> Constants.IAM_ACCESS_MAPPING_ENTITY_PREFIX_MED + "_" + m.getId())
                            .collect(Collectors.toList()));
                }
                ResponseEntity<Response<AllAdherenceResponse>> allAdherenceResponse = iamService.getAdherenceBulk(new SearchAdherenceRequest(new ArrayList<>(entityIds), null), true);
                for (AdherenceResponse adherenceResponse : allAdherenceResponse.getBody().getData().getAdherenceResponseList()) {
                    adherenceStringMap.put(adherenceResponse.getEntityId(), adherenceStringMap.get(adherenceResponse.getEntityId()));
                }
                mergedAdherence = allAdherenceResponse.getBody().getData().getMergedAdherence();
            } catch (Exception e) {
                Sentry.capture(e);
                LOGGER.error(e.toString());
                throw new ValidationException("Unable to fetch Updated Adherence");
            }
        }
        episodeLogMap.forEach((id, val) -> {
            String adherenceString = adherenceStringMap.getOrDefault(id, "");
            editDosesResponseList.add(new EditDosesResponse(
                val.getActionTaken(), val.getActionTaken(), val.getAddedOn(), adherenceString
            ));
        });
        return new ResponseEntity<>(new Response<>(new EditDoseBulkResponse(editDosesResponseList, mergedAdherence)), HttpStatus.OK);
    }
}

package com.everwell.transition.controller;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.model.db.Episode;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.dto.BeneficiaryDetailsDto;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.dto.EpisodeSearchResult;
import com.everwell.transition.model.dto.notification.EmptyBankDetailsDto;
import com.everwell.transition.model.dto.notification.UserSmsDetails;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.dto.EpisodeReportsRequest;
import com.everwell.transition.model.dto.EpisodeSearchResult;
import com.everwell.transition.model.dto.EpisodeUnifiedSearchRequest;
import com.everwell.transition.model.dto.EpisodeDocumentResponseDto;
import com.everwell.transition.model.request.AddEpisodeDocumentRequest;
import com.everwell.transition.model.request.EpisodeDocumentSearchRequest;
import com.everwell.transition.model.request.episode.DeleteRequest;
import com.everwell.transition.model.request.episodesearch.EpisodeSearchRequest;
import com.everwell.transition.model.request.episodesearch.EpisodeHeaderSearchRequest;
import com.everwell.transition.model.request.episode.EpisodeBulkRequest;
import com.everwell.transition.model.request.episode.SearchEpisodesRequest;
import com.everwell.transition.model.request.episode.UpdateRiskRequest;
import com.everwell.transition.model.request.tasklistfilters.EpisodeTasklistRequest;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.EpisodeService;
import com.everwell.transition.utils.MultiPartFileUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collections;
import java.io.IOException;
import java.util.*;
import java.util.Map;
import java.util.List;

@RestController
public class EpisodeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EpisodeController.class);

    @Autowired
    private EpisodeService episodeService;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    ClientService clientService;

    @RequestMapping(value="/v1/episode", method = RequestMethod.POST)
    public ResponseEntity<Response<Episode>> createEpisode(@RequestHeader(name = Constants.X_CLIENT_ID) Long clientId, @RequestBody Map<String, Object> requestInput) {
        LOGGER.info("[createEpisode] create episode request accepted : " + requestInput.toString());
        requestInput.put(FieldConstants.CLIENT_ID,clientId);
        Episode episode = episodeService.processCreateEpisode(clientId, requestInput);
        return new ResponseEntity<>(new Response<>(episode), HttpStatus.OK);
    }

    @RequestMapping(value="/v1/episode", method = RequestMethod.PUT)
    public ResponseEntity<Response<Episode>> updateEpisodeDetails(@RequestHeader(name = Constants.X_CLIENT_ID) Long clientId, @RequestBody Map<String, Object> requestInput) {
        LOGGER.info("[updateEpisodeDetails] update episode request received :" + requestInput.toString());
        requestInput.put(FieldConstants.CLIENT_ID,clientId);
        Episode episode = episodeService.processUpdateEpisode(clientId, requestInput);
        return new ResponseEntity<>(new Response<>(episode), HttpStatus.OK);
    }

    @RequestMapping(value="/v1/episode/stage", method = RequestMethod.PUT)
    public ResponseEntity<Response<Episode>> updateEpisodeStageAndDetails(@RequestHeader(name = Constants.X_CLIENT_ID) Long clientId, @RequestBody Map<String, Object> requestInput) {
        LOGGER.info("[updateEpisodeDetails] update episode stage request received :" + requestInput.toString());
        Episode episode = episodeService.processEpisodeRequestInputForStageTransition(clientId, requestInput);
        return new ResponseEntity<>(new Response<>(episode), HttpStatus.OK);
    }

    @RequestMapping(value="/v1/episode/{id}", method = RequestMethod.GET)
    public ResponseEntity<Response<EpisodeDto>> getEpisodeDetails(
            @RequestHeader(name = Constants.X_CLIENT_ID) Long clientId,
            @PathVariable Long id,
            @RequestParam(value = "stageLogsRequired", defaultValue = "false") boolean stageLogsRequired,
            @RequestParam(value = "tabsPermissionRequired", defaultValue = "false") boolean tabsPermissionRequired) {
        LOGGER.info("[getEpisodeDetails] request accepted with values id: " + id + " clientId: " + clientId);
        if(null == id) {
            throw new ValidationException("episodeId cannot be null");
        }
        EpisodeDto data = episodeService.getEpisodeDetails(id, clientId);
        return new ResponseEntity<>(new Response<>(data), HttpStatus.OK);
    }

    @RequestMapping(value="/v1/episodes/personId/{id}", method = RequestMethod.GET)
    public ResponseEntity<Response<List<EpisodeDto>>> getEpisodesForPerson(
            @RequestHeader(name = Constants.X_CLIENT_ID) Long clientId,
            @PathVariable Long id,
            @RequestParam(name = "includeDetails", required = false, defaultValue = "false") Boolean includeDetails) {
        LOGGER.info("[getEpisodesForPerson] request accepted with values id: " + id + " clientId: " + clientId);
        if(null == id) {
            throw new ValidationException("personId cannot be null");
        }
        List<EpisodeDto> data = episodeService.getEpisodesForPersonList(Collections.singletonList(id), clientId, includeDetails);
        return new ResponseEntity<>(new Response<>(data), HttpStatus.OK);
    }

    @PostMapping(value="/v2/episode/search")
    public ResponseEntity<Response<EpisodeSearchResult>> episodeSearch(@RequestBody EpisodeSearchRequest episodeSearchRequest) {
        episodeSearchRequest.validate();
        if (null != episodeSearchRequest.getFilters()) {
            episodeSearchRequest.mergeFilters();
        }
        episodeSearchRequest.getSearch().getMust().put(Constants.CLIENT_ID, clientService.getClientByTokenOrDefault().getId());
        return new ResponseEntity<>(new Response<>(episodeService.elasticSearch(episodeSearchRequest)), HttpStatus.OK);
    }

    @PostMapping(value="/v2/episode/search/bulk")
    public ResponseEntity<Response<List<EpisodeSearchResult>>> episodeSearchBulk(@RequestBody List<EpisodeSearchRequest> episodeSearchRequest) {
        episodeSearchRequest.forEach(EpisodeSearchRequest::validate);
        List<EpisodeSearchResult> searchResults = new ArrayList<>();
        for (EpisodeSearchRequest searchRequest : episodeSearchRequest) {
            if (searchRequest.getFilters() != null) {
                searchRequest.mergeFilters();
            }
            searchRequest.getSearch().getMust().put(Constants.CLIENT_ID, clientService.getClientByTokenOrDefault().getId());
            searchResults.add(episodeService.elasticSearch(searchRequest));
        }
        return new ResponseEntity<>(new Response<>(searchResults), HttpStatus.OK);
    }

    @PostMapping(value="/v1/episode/header")
    public ResponseEntity<Response<EpisodeSearchResult>> episodeHeaderSearch(@RequestBody EpisodeHeaderSearchRequest headerSearchRequest) {
        headerSearchRequest.validate();
        EpisodeSearchRequest episodeSearchRequest = headerSearchRequest.convert();
        episodeSearchRequest.getSearch().getMust().put(Constants.CLIENT_ID, clientService.getClientByTokenOrDefault().getId());
        EpisodeSearchResult episodeSearchResult =  episodeService.elasticSearch(episodeSearchRequest);
        return new ResponseEntity<>(new Response<>(episodeSearchResult), HttpStatus.OK);
    }
    
    @RequestMapping(value="v1/episode/phone/{phoneNumber}", method = RequestMethod.GET)
    public ResponseEntity<Response<List<Episode>>> getEpisodeByPhoneNumber(@PathVariable String phoneNumber) {
        LOGGER.info("[getEpisodeByPhoneNumber] request accepted with values phoneNumber: " + phoneNumber);
        List<Episode> data = episodeService.getEpisodeByPhoneNumber(phoneNumber);
        return new ResponseEntity<>(new Response<>(data), HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/episode/duplicates", method = RequestMethod.POST)
    public ResponseEntity<Response<List<EpisodeDto>>> getDuplicateEpisodes(@RequestHeader(name = Constants.X_CLIENT_ID) Long clientId, @RequestBody Map<String, Object> fetchDuplicatesRequest)
    {
        LOGGER.info("[getDuplicateEpisodes] request received " + fetchDuplicatesRequest);
        Response<List<EpisodeDto>> episodeDtoResponse = new Response<>( episodeService.getDuplicateEpisodesForUser(clientId, fetchDuplicatesRequest), true);
        return new ResponseEntity<>(episodeDtoResponse, HttpStatus.OK);
    }

    @PostMapping("/v1/episode/search")
    public ResponseEntity<Response<List<Long>>> getEpisodesWithFilter(@RequestHeader(name = Constants.X_CLIENT_ID) Long clientId, @RequestBody SearchEpisodesRequest searchEpisodesRequest) {
        LOGGER.info("[getEpisodesWithFilter] received request: " + searchEpisodesRequest);
        List<Long> episodeResponse = episodeService.search(searchEpisodesRequest);
        return new ResponseEntity<>(new Response<>(episodeResponse), HttpStatus.OK);
    }

    @PostMapping("/v1/episode/risk")
    public ResponseEntity<Response<String>> updateEpisodeRisk(@RequestHeader(name = Constants.X_CLIENT_ID) Long clientId, @RequestBody UpdateRiskRequest updateRiskRequest) {
        LOGGER.info("[updateRiskStatusForEpisode] received request: " + updateRiskRequest);
        updateRiskRequest.validate();
        episodeService.updateRiskStatus(updateRiskRequest);
        applicationEventPublisher.publishEvent(updateRiskRequest);
        return new ResponseEntity<>(new Response<>(true,"Updated Successfully!"), HttpStatus.OK);
    }

    @PostMapping("/v1/episode/bulk")
    public ResponseEntity<Response<List<EpisodeDto>>> getEpisodesBulk(@RequestHeader(name = Constants.X_CLIENT_ID) Long clientId, @RequestBody EpisodeBulkRequest episodeBulkRequest) {
        LOGGER.info("[getEpisodesBulk] received request: " + episodeBulkRequest);
        episodeBulkRequest.validate();
        List<EpisodeDto> episodeResponse = episodeService.getEpisodesBulk(episodeBulkRequest);
        return new ResponseEntity<>(new Response<>(episodeResponse), HttpStatus.OK);
    }

    @PostMapping(value="/v1/episode/tasklist")
    public ResponseEntity<Response<EpisodeSearchResult>> episodeTaskListSearch(@RequestBody EpisodeTasklistRequest episodeTasklistRequest) {
        LOGGER.info("episodeTaskListSearch request " + episodeTasklistRequest.toString());
        episodeTasklistRequest.validate();
        EpisodeSearchResult episodeSearchResult = episodeService.taskListSearch(episodeTasklistRequest);
        LOGGER.info("episodeTaskListSearch " + episodeSearchResult.toString());
        return new ResponseEntity<>(new Response<>(episodeSearchResult), HttpStatus.OK);
    }

    @PostMapping(value="/v1/episode/upp")
    public ResponseEntity<Response<EpisodeSearchResult>> episodeUnifiedPageFilters(@RequestBody EpisodeUnifiedSearchRequest episodeUnifiedSearchRequest) {
        episodeUnifiedSearchRequest.validate();
        return new ResponseEntity<>(new Response<>(episodeService.episodeUnifiedSearch(episodeUnifiedSearchRequest)), HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/episode/document", method = RequestMethod.POST)
    public ResponseEntity<Response<String>> uploadEpisodeDocument(@RequestParam(value = "file") List<MultipartFile> file, @RequestPart(value = "addEpisodeDocument") AddEpisodeDocumentRequest addEpisodeDocument) throws IOException {
        LOGGER.info("[uploadEpisodeDocument] request received: " + addEpisodeDocument);
        MultiPartFileUtils.validateMultipartFiles(file);
        addEpisodeDocument.validate();
        episodeService.uploadEpisodeDocument(file, addEpisodeDocument);
        return new ResponseEntity<>(new Response<>(true, "Document uploaded successfully"), HttpStatus.OK);
    }

    @GetMapping(value = "/v1/episode/document")
    public ResponseEntity<Response<List<EpisodeDocumentResponseDto>>> personDocumentDetails(@RequestParam(required = false) Long personId,
                                                                                            @RequestParam(defaultValue = "false") boolean fetchAll,
                                                                                            @RequestParam(required = false) Long episodeDocumentDetailsId) {
        LOGGER.info("[personDocumentDetails] request received (personId, episodeDocumentDetailsId): " + personId + "," + episodeDocumentDetailsId);
        List<EpisodeDocumentResponseDto> episodeDocumentResponseList;
        if(null != personId) {
            episodeDocumentResponseList = episodeService.getPersonDocumentDetails(personId, fetchAll);
        } else if (null != episodeDocumentDetailsId) {
            episodeDocumentResponseList = episodeService.getEpisodeDocumentDetailsByEpisodeDocumentDetailsId(episodeDocumentDetailsId);
        } else {
            throw new ValidationException("invalid input parameters");
        }
        return new ResponseEntity<>(new Response<>(episodeDocumentResponseList), HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/episode/empty-bank", method = RequestMethod.POST)
    public ResponseEntity<Response<EmptyBankDetailsDto>> getEpisodesForEmptyBankDetails(EpisodeSearchRequest episodeSearchRequest) {
        LOGGER.info("[getEpisodesForEmptyBankDetails] request accepted  " + episodeSearchRequest);
        episodeSearchRequest.validate();
        EmptyBankDetailsDto userDetails = episodeService.getEpisodesForEmptyBankDetails(episodeSearchRequest);
        return new ResponseEntity<>(new Response<>(userDetails), HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/episode/benefits", method = RequestMethod.POST)
    public ResponseEntity<Response<List<BeneficiaryDetailsDto>>> getEpisodesForBenefitsCreated(@RequestBody EpisodeSearchRequest episodeSearchRequest) {
        LOGGER.info("[getEpisodesForBenefitsCreated] request accepted with beneficiaries " + episodeSearchRequest);
        episodeSearchRequest.validate();
        List<BeneficiaryDetailsDto> beneficiaryDetails = episodeService.getEpisodesForBenefitCreated(episodeSearchRequest);
        return new ResponseEntity<>(new Response<>(beneficiaryDetails), HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/episode/invalid-bank", method = RequestMethod.POST)
    public ResponseEntity<Response<List<UserSmsDetails>>> getEpisodesForInvalidBankDetails(@RequestBody EpisodeSearchRequest episodeSearchRequest) {
        LOGGER.info("[getEpisodesForInvalidBankDetails] request accepted with beneficiaries " + episodeSearchRequest);
        episodeSearchRequest.validate();
        List<UserSmsDetails> userSmsDetails = episodeService.getEpisodesForInvalidBankDetails(episodeSearchRequest);
        return new ResponseEntity<>(new Response<>(userSmsDetails), HttpStatus.OK);
    }
    
    @ApiOperation(value = "Delete an episode")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Episode deleted successfully"),
            @ApiResponse(code = 404, message = "no episode found for Id"),
            @ApiResponse(code = 500, message = "[delete] Unable to process request")
    })
    @RequestMapping(value = "/v1/episode/delete", method = RequestMethod.POST)
    public ResponseEntity<Response<String>> deleteEpisode(@RequestBody DeleteRequest request) {
        LOGGER.info("[deleteEpisode] request received: " + request);
        episodeService.deleteEpisode(clientService.getClientByTokenOrDefault().getId(), request);
        return new ResponseEntity<>(new Response<>(true, "Episode deleted successfully"), HttpStatus.OK);
    }

    @ApiOperation(value = "Get episode reports data for a set of filters")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Reports filters cannot be empty"),
            @ApiResponse(code = 400, message = "Invalid reports date type ")
    })
    @PostMapping(value="/v1/episode/reports")
    public ResponseEntity<Response<EpisodeSearchResult>> episodeReportsFilters(@RequestBody EpisodeReportsRequest episodeReportsRequest) {
        episodeReportsRequest.validate();
        return new ResponseEntity<>(new Response<>(episodeService.episodeReports(episodeReportsRequest)), HttpStatus.OK);
    }

    @PostMapping(value = "/v1/episode/search/document")
    public ResponseEntity<Response<List<EpisodeDocumentResponseDto>>> episodeDocumentDetailsForIds(@RequestBody EpisodeDocumentSearchRequest request) {
        request.validate();
        LOGGER.info("[episodeDocumentDetailsForIds] request received: " + request);
        List<EpisodeDocumentResponseDto> episodeDocumentResponseList = episodeService.getEpisodeDocumentDetails(request);
        return new ResponseEntity<>(new Response<>(episodeDocumentResponseList), HttpStatus.OK);
    }

    @PutMapping(value = "/v1/episode/document/{episodeDocumentDetailsId}")
    public ResponseEntity<Response<String>> updateEpisodeDocument(@PathVariable Long episodeDocumentDetailsId, @RequestParam Boolean approved, @RequestParam Long validatedBy) {
        LOGGER.info("[updateEpisodeDocument] request received for id = " + episodeDocumentDetailsId + "status " + approved + "validated by " + validatedBy);
        if (null == approved || null == validatedBy) {
            throw new ValidationException("One or more missing parameters");
        }
        episodeService.updateEpisodeDocumentDetails(episodeDocumentDetailsId, approved, validatedBy);
        return new ResponseEntity<>(new Response<>(true, "Document updated successfully"), HttpStatus.OK);

    }
}
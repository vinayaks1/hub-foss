package com.everwell.transition.controller;

import com.everwell.transition.model.request.aers.AdverseReactionRequest;
import com.everwell.transition.model.request.aers.CausalityManagementRequest;
import com.everwell.transition.model.request.aers.OutcomeRequest;
import com.everwell.transition.model.response.*;
import com.everwell.transition.service.AERSService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
public class AERSController {

    private static Logger LOGGER = LoggerFactory.getLogger(AERSController.class);

    @Autowired
    private AERSService aersService;

    @PostMapping(value = "/v1/aers/adverse-reaction")
    public ResponseEntity<Response<String>> addAdverseReaction(@RequestBody AdverseReactionRequest adverseReactionRequest) {
        LOGGER.info("[addAdverseReaction] request accepted");
        adverseReactionRequest.validate(adverseReactionRequest);
        aersService.addAdverseReaction(adverseReactionRequest);
        Response<String> response = new Response<>("Successfully saved Adverse Reaction", true);
        return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
    }

    @PutMapping(value = "/v1/aers/adverse-reaction")
    public ResponseEntity<Response<String>> updateAdverseReaction(@RequestBody AdverseReactionRequest adverseReactionRequest) {
        LOGGER.info("[updateAdverseReaction] request accepted");
        adverseReactionRequest.validate(adverseReactionRequest);
        aersService.updateAdverseReaction(adverseReactionRequest);
        Response<String> response = new Response<>("Successfully saved Adverse Reaction", true);
        return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
    }

    @PostMapping(value = "/v1/aers/causality-management")
    public ResponseEntity<Response<String>> addCausalityManagement(@RequestBody CausalityManagementRequest causalityManagementRequest) {
        LOGGER.info("[addCausalityManagement] request accepted");
        causalityManagementRequest.validate(causalityManagementRequest);
        aersService.addCausalityManagement(causalityManagementRequest);
        Response<String> response = new Response<>("Successfully saved Causality and Management Details",true);
        return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
    }

    @PutMapping(value = "/v1/aers/causality-management")
    public ResponseEntity<Response<String>> updateCausalityManagement(@RequestBody CausalityManagementRequest causalityManagementRequest) {
        LOGGER.info("[updateCausalityManagement] request accepted");
        causalityManagementRequest.validate(causalityManagementRequest);
        aersService.updateCausalityManagement(causalityManagementRequest);
        Response<String> response = new Response<>("Successfully saved Causality and Management Details",true);
        return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
    }

    @PostMapping(value = "/v1/aers/outcome")
    public ResponseEntity<Response<String>> addOutcome(@RequestBody OutcomeRequest outcomeRequest) {
        LOGGER.info("[addOutcome] request accepted");
        outcomeRequest.validate(outcomeRequest);
        aersService.addOutcome(outcomeRequest);
        Response<String> response = new Response<>("Successfully saved Outcome Details",true);
        return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
    }

    @PutMapping(value = "/v1/aers/outcome")
    public ResponseEntity<Response<String>> updateOutcome(@RequestBody OutcomeRequest outcomeRequest) {
        LOGGER.info("[updateOutcome] request accepted");
        outcomeRequest.validate(outcomeRequest);
        aersService.updateOutcome(outcomeRequest);
        Response<String> response = new Response<>("Successfully saved Outcome Details",true);
        return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
    }

    @GetMapping(value = "/v1/aers/adverse-reaction")
    public ResponseEntity<Response<AdverseReactionResponse>> getAdverseReactionDetails(@RequestParam(value = "adverseEventId") Long adverseEventId) throws IOException {
        LOGGER.info("[getAdverseReactionDetails] request accepted");
        AdverseReactionResponse adverseReactionResponse = aersService.getAdverseReaction(adverseEventId);
        Response<AdverseReactionResponse> response = new Response<>(adverseReactionResponse);
        return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
    }

    @GetMapping(value = "/v1/aers/causality-management")
    public  ResponseEntity<Response<CausalityManagementResponse>> getCausalityManagementDetails(@RequestParam(value = "adverseEventId") Long adverseEventId) throws IOException {
        LOGGER.info("[getCausalityManagementDetails] request accepted");
        CausalityManagementResponse causalityManagementResponse = aersService.getCausalityManagement(adverseEventId);
        Response<CausalityManagementResponse> response = new Response<>(causalityManagementResponse);
        return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
    }

    @GetMapping(value = "/v1/aers/outcome")
    public ResponseEntity<Response<OutcomeResponse>> getOutcomeDetails(@RequestParam(value = "adverseEventId") Long adverseEventId) {
        LOGGER.info("[getOutcomeDetails] request accepted");
        OutcomeResponse outcomeResponse  = aersService.getOutcome(adverseEventId);
        Response<OutcomeResponse> response = new Response<>(outcomeResponse);
        return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
    }

    @GetMapping(value = "/v1/aers/status")
    public ResponseEntity<Response<AERSStatusResponse>> getAdverseEvents(@RequestParam(value = "episodeId") Long episodeId) {
        LOGGER.info("[getAdverseEvents] request accepted");
        List<FormConfigResponse> aersStatusResponse = aersService.getStatus(episodeId);
        AERSStatusResponse aersStatusList = new AERSStatusResponse(aersStatusResponse);
        Response<AERSStatusResponse> response = new Response<>(aersStatusList);
        return new ResponseEntity<>(response,HttpStatus.ACCEPTED);
    }

}

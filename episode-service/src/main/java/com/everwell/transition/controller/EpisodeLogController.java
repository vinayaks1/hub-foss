package com.everwell.transition.controller;

import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.request.episodelogs.*;
import com.everwell.transition.model.response.EpisodeLogResponse;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.response.LogsConfigResponse;
import com.everwell.transition.service.EpisodeLogService;
import com.everwell.transition.service.EpisodeTagService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EpisodeLogController {

    private static Logger LOGGER = LoggerFactory.getLogger(EpisodeLogController.class);

    @Autowired
    private EpisodeLogService episodeLogService;

    @Autowired
    private EpisodeTagService episodeTagService;

    @GetMapping("/v1/logs")
    public ResponseEntity<Response<EpisodeLogResponse>> getLogs(@RequestParam(value = "episodeId") Long episodeId) {
        LOGGER.info("[getLogs] received request for episodeId: " + episodeId);
        if (episodeId == 0)
            throw new ValidationException("episodeId must not be null!");
        EpisodeLogResponse episodeLogResponse = episodeLogService.getEpisodeLogs(episodeId);
        return new ResponseEntity<>(new Response<>(episodeLogResponse), HttpStatus.OK);
    }

    @PostMapping("/v1/logs/bulk")
    public ResponseEntity<Response<List<EpisodeLogResponse>>> getLogsWithFilter(@RequestBody SearchLogsRequest searchLogsRequest) {
        LOGGER.info("[getLogsWithFilter] received request: " + searchLogsRequest);
        searchLogsRequest.validate();
        List<EpisodeLogResponse> episodeLogResponse = episodeLogService.search(searchLogsRequest);
        return new ResponseEntity<>(new Response<>(episodeLogResponse), HttpStatus.OK);
    }

    @PostMapping("/v1/logs")
    public ResponseEntity<Response<List<EpisodeLogResponse>>> saveLogs(@RequestBody EpisodeLogRequest episodeLogRequest) {
        LOGGER.info("[saveLogs] received request: " + episodeLogRequest);
        episodeLogRequest.validate();
        List<EpisodeLogResponse> episodeLogResponse = episodeLogService.save(episodeLogRequest.getEpisodeLogData());
        return new ResponseEntity<>(new Response<>(episodeLogResponse), HttpStatus.OK);
    }

    @PostMapping("/v1/logs/delete")
    public ResponseEntity<Response<String>> deleteLogs(@RequestBody EpisodeLogDeletionRequest episodeLogDeletionRequest) {
        LOGGER.info("[deleteLogs] received request: " + episodeLogDeletionRequest);
        episodeLogDeletionRequest.validate();
        episodeLogService.delete(episodeLogDeletionRequest);
        return new ResponseEntity<>(new Response<>(true, "Deleted Successfully!"), HttpStatus.OK);
    }

    @GetMapping("/v1/logs/config")
    public ResponseEntity<Response<List<LogsConfigResponse>>> getLogsConfig() {
        LOGGER.info("[getLogsConfig] received request");
        List<LogsConfigResponse> LogsConfigResponse = episodeLogService.getEpisodeLogsConfig();
        return new ResponseEntity<>(new Response<>(LogsConfigResponse), HttpStatus.OK);
    }

    @PostMapping("/v1/logs/sync")
    public ResponseEntity<Response<String>> syncLogs(@RequestBody SyncLogsRequest syncLogsRequest) {
        LOGGER.info("[syncLogs] received request" + syncLogsRequest);
        syncLogsRequest.validate();
        Long episodeId = syncLogsRequest.getEpisodeId();
        String endDate = syncLogsRequest.getEndDate();
        episodeLogService.syncLogs(episodeId, endDate);
        episodeTagService.deleteAllTagsAfterEndDate(episodeId, endDate);
        return new ResponseEntity<>(new Response<>(true, "Logs Synced Successfully!"), HttpStatus.OK);
    }

    @ApiOperation(value = "Update existing logs")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Updated successfully"),
        @ApiResponse(code = 400, message = "Log data cannot be empty!, Id cannot be empty!, Category cannot be empty!, Episode Id cannot be empty!, Added By cannot be null")
    })
    @PutMapping("/v1/support-actions")
    public ResponseEntity<Response<String>> updateSupportActions(@RequestBody UpdateSupportActionsRequest updateSupportActionsRequest) {
        LOGGER.info("[updateSupportActions] received request: " + updateSupportActionsRequest);
        episodeLogService.updateSupportActions(updateSupportActionsRequest);
        return new ResponseEntity<>(new Response<>(true, "Updated successfully"), HttpStatus.OK);
    }

}

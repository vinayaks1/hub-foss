package com.everwell.transition.controller;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.enums.episodeLog.EpisodeLogAction;
import com.everwell.transition.enums.episodeLog.EpisodeLogCategory;
import com.everwell.transition.enums.episodeLog.EpisodeLogComments;
import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.enums.treamentPlan.TypeOfMed;
import com.everwell.transition.exceptions.NotFoundException;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.db.Episode;
import com.everwell.transition.model.db.Field;
import com.everwell.transition.model.db.TreatmentPlan;
import com.everwell.transition.model.db.TreatmentPlanProductMap;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.dto.treatmentPlan.TreatmentPlanDto;
import com.everwell.transition.model.dto.treatmentPlan.TreatmentPlanProductMapDto;
import com.everwell.transition.model.request.episodelogs.EpisodeLogDataRequest;
import com.everwell.transition.model.request.treatmentPlan.*;
import com.everwell.transition.model.response.ProductResponseWithScheduleDto;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.EpisodeLogService;
import com.everwell.transition.service.DispensationService;
import com.everwell.transition.service.EpisodeService;
import com.everwell.transition.service.TreatmentPlanService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;


import javax.validation.Valid;

@RestController
public class TreatmentPlanController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TreatmentPlanController.class);

    @Autowired
    private TreatmentPlanService treatmentPlanService;

    @Autowired
    private ClientService clientService;

    @Autowired
    private EpisodeService episodeService;

    @Autowired
    private EpisodeLogService episodeLogService;

    @Autowired
    private DispensationService dispensationService;

    @RequestMapping(value = "/v1/treatment-plan", method = RequestMethod.POST)
    public ResponseEntity<Response<TreatmentPlan>> createTreatmentPlan(
            @RequestBody AddTreatmentPlanRequest request) {
        Long clientId = clientService.getClientByTokenOrDefault().getId();
        LOGGER.info("[createTreatmentPlan] request accepted for clientId - " + clientId + " and request : " + request);
        request.validate();
        TreatmentPlan treatmentPlan = treatmentPlanService.createPlan(request, clientId);
        if (request.getEpisodeId() != null) {
            Map<String, Object> requestData = new HashMap<>();
            requestData.put(FieldConstants.EPISODE_FIELD_EPISODE_ID, request.getEpisodeId());
            requestData.put(FieldConstants.ESD_TREATMENT_PLAN_ID, treatmentPlan.getId());
            episodeService.processUpdateEpisode(clientId, requestData);
        }
        return new ResponseEntity<>(new Response<>(treatmentPlan), HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/treatment-plan/{id}", method = RequestMethod.GET)
    public ResponseEntity<Response<TreatmentPlanDto>> getTreatmentPlan(
            @RequestHeader(name = Constants.X_CLIENT_ID) Long clientId,
            @PathVariable Long id) {
        LOGGER.info("[getTreatmentPlan] request accepted for clientId - " + clientId + " and id : " + id);
        TreatmentPlanDto treatmentDetails = treatmentPlanService.getPlanDetails(id, clientId);
        return new ResponseEntity<>(new Response<>(treatmentDetails), HttpStatus.OK);
    }

    @PutMapping(value = "/v1/episode/{episodeId}/treatment-plan/{id}/products")
    public ResponseEntity<Response<List<TreatmentPlanProductMapDto>>> updateProductMappingsForTreatmentPlan(@PathVariable Long episodeId,  @PathVariable Long id, @RequestBody EditTreatmentPlanRequest editTreatmentPlanRequest) {
        Long clientId = clientService.getClientByTokenOrDefault().getId();
        LOGGER.info("[updateProductMappingsForTreatmentPlan] request accepted for clientId - " + clientId + " and request : " + editTreatmentPlanRequest);
        editTreatmentPlanRequest.validate();
        episodeService.getEpisode(episodeId, clientId);
        List<TreatmentPlanProductMapDto> treatmentPlanProductMapDtoList = treatmentPlanService.processAddProductMappingsForTreatmentPlan(id, episodeId, clientId, editTreatmentPlanRequest);
        if (!treatmentPlanProductMapDtoList.isEmpty()) {
            String medicineNameConcatenated = treatmentPlanProductMapDtoList.stream().map(TreatmentPlanProductMapDto::getName).collect(Collectors.joining(", "));
            List<EpisodeLogDataRequest> episodeLogRequest = new ArrayList<>();
            episodeLogRequest.add(new EpisodeLogDataRequest(episodeId, EpisodeLogCategory.MEDICINE_MAPPINGS_UPDATED.getCategoryName(), Constants.DEFAULT_ADDED_BY_ID,
                    EpisodeLogAction.NEW_MEDICINE_MAPPINGS_ADDED.getActionName(), String.format(EpisodeLogComments.NEW_MEDICINE_MAPPINGS_ADDED.getText(), medicineNameConcatenated)));
            episodeLogService.save(episodeLogRequest);
        }
        return new ResponseEntity<>(new Response<>(treatmentPlanProductMapDtoList), HttpStatus.OK);
    }

    @DeleteMapping(value = "/v1/episode/{episodeId}/treatment-plan/{id}/products")
    public ResponseEntity<Response<List<TreatmentPlanProductMapDto>>> deleteProductMappingsForTreatmentPlan(@PathVariable Long episodeId, @PathVariable Long id, @RequestBody DeleteProductsMappingRequest deleteProductsMappingRequest) {
        Long clientId = clientService.getClientByTokenOrDefault().getId();
        LOGGER.info("[deleteProductMappingsForTreatmentPlan] request accepted for clientId - " + clientId + " and request : " + deleteProductsMappingRequest);
        deleteProductsMappingRequest.validate();
        episodeService.getEpisode(episodeId, clientId);
        List<TreatmentPlanProductMapDto> treatmentPlanProductMapDtoList = treatmentPlanService.processDeleteProductMappingsForTreatmentPlan(id, episodeId, clientId, deleteProductsMappingRequest);
        if (!treatmentPlanProductMapDtoList.isEmpty()) {
            String medicineNameConcatenated = treatmentPlanProductMapDtoList.stream().map(TreatmentPlanProductMapDto::getName).collect(Collectors.joining(", "));
            List<EpisodeLogDataRequest> episodeLogRequest = new ArrayList<>();
            episodeLogRequest.add(new EpisodeLogDataRequest(episodeId, EpisodeLogCategory.MEDICINE_MAPPINGS_UPDATED.getCategoryName(), Constants.DEFAULT_ADDED_BY_ID,
                    EpisodeLogAction.EXISTING_MEDICINE_MAPPINGS_DELETED.getActionName(), String.format(EpisodeLogComments.EXISTING_MEDICINE_MAPPINGS_DELETED.getText(), medicineNameConcatenated)));
            episodeLogService.save(episodeLogRequest);
        }
        return new ResponseEntity<>(new Response<>(treatmentPlanProductMapDtoList), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/v1/treatment-plan/{treatmentPlanId}/product", method = RequestMethod.GET)
    public ResponseEntity<Response<List<ProductResponseWithScheduleDto>>> getTreatmentPlanProductDetails(
            @PathVariable @Valid Long treatmentPlanId) {
        Long clientId = clientService.getClientByTokenOrDefault().getId();
        TreatmentPlanDto treatmentDetails = null;
        treatmentDetails = treatmentPlanService.getPlanDetails(treatmentPlanId, clientId);
        List<ProductResponseWithScheduleDto> productResponseWithScheduleDtoList = dispensationService.getProductWithSchedulesFromTreatmentPlan(treatmentDetails);
        return new ResponseEntity<>(new Response<>(productResponseWithScheduleDtoList), HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/treatment-plan/product/episode/{episodeId}", method = RequestMethod.GET)
    public ResponseEntity<Response<List<ProductResponseWithScheduleDto>>> getTreatmentPlanProductDetailsForEpisode(
            @PathVariable @Valid Long episodeId,
            @RequestParam(value = "typeOfMed", defaultValue = "normal") TypeOfMed typeOfMed) {
        Long clientId = clientService.getClientByTokenOrDefault().getId();
        TreatmentPlanDto treatmentDetails = null;
        EpisodeDto episodeDto = episodeService.getEpisodeDetails(episodeId, clientId);
        if(null != episodeDto.getStageData() && null != episodeDto.getStageData().get(FieldConstants.ESD_TREATMENT_PLAN_ID)) {
                Long treatmentPlanId = Long.parseLong(episodeDto.getStageData().get(FieldConstants.ESD_TREATMENT_PLAN_ID).toString());
                treatmentDetails = treatmentPlanService.getPlanDetails(treatmentPlanId, clientId, typeOfMed.getState());
        } else {
            throw new NotFoundException("Treatment Plan is not found");
        }
        List<ProductResponseWithScheduleDto> productResponseWithScheduleDtoList = dispensationService.getProductWithSchedulesFromTreatmentPlan(treatmentDetails);
        return new ResponseEntity<>(new Response<>(productResponseWithScheduleDtoList), HttpStatus.OK);
    }
}

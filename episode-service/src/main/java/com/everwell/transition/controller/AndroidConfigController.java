package com.everwell.transition.controller;

import com.everwell.transition.model.dto.wix.WixPostFilter;
import com.everwell.transition.model.request.wix.WixPostResponse;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.response.android.GenericUiResponse;
import com.everwell.transition.service.AndroidConfigService;
import com.everwell.transition.service.WixApiIntegrationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.stream.Collectors;

@RestController
public class AndroidConfigController {

    private static Logger LOGGER = LoggerFactory.getLogger(AndroidConfigController.class);

    @Autowired
    AndroidConfigService androidConfigService;

    @GetMapping(value = "/v1/feed/knowledge/{episodeId}")
    public ResponseEntity<Response<List<GenericUiResponse>>> getFitFeed(@PathVariable Long episodeId, @RequestParam(value = "language", defaultValue = "en") String language,
                                                                        @RequestParam(value = "region", defaultValue = "IN") String region) {
        LOGGER.info("[getFitFeed] language: " + language);
        List<GenericUiResponse> uiResponse = androidConfigService.getFitFeed(language, region);
        return new ResponseEntity<>(new Response<>(true, uiResponse), HttpStatus.OK);
    }

    @GetMapping(value = "/v1/feed/home/{episodeId}")
    public ResponseEntity<Response<List<GenericUiResponse>>> getHomeFeed(@PathVariable Long episodeId, @RequestParam(value = "language", defaultValue = "en") String language,
                                                                        @RequestParam(value = "region", defaultValue = "IN") String region) {
        LOGGER.info("[getHomeFeed] language: " + language);
        List<GenericUiResponse> uiResponse = androidConfigService.getHomeFeed(language, region);
        return new ResponseEntity<>(new Response<>(true, uiResponse), HttpStatus.OK);
    }

}

package com.everwell.transition.controller;

import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.request.course.MarkCourseRequest;
import com.everwell.transition.model.response.CourseResponse;
import com.everwell.transition.model.response.MarkCourseResponse;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.service.CourseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CourseController {
    private static Logger LOGGER = LoggerFactory.getLogger(CourseController.class);

    @Autowired
    CourseService courseService;

    @GetMapping("/v1/course/{episodeId}")
    public ResponseEntity<Response<CourseResponse>> getCourses(@PathVariable(value = "episodeId") Long episodeId) {
        LOGGER.info("[getCourses] received request for episodeId: " + episodeId);
        if (episodeId == 0)
            throw new ValidationException("episodeId must not be null!");
        CourseResponse courseResponse = courseService.getAllCourses(episodeId);
        return new ResponseEntity<>(new Response<>(courseResponse), HttpStatus.OK);
    }

    @PostMapping("/v1/course/status")
    public ResponseEntity<Response<MarkCourseResponse>> markCourseStatus(@RequestBody MarkCourseRequest markCourseRequest) {
        LOGGER.info("[markCourse] received request for episodeId: " + markCourseRequest.getEpisodeId());
        markCourseRequest.validate();
        MarkCourseResponse markCourseResponse = courseService.markCourseAsComplete(markCourseRequest);
        return new ResponseEntity<>(new Response<>(markCourseResponse), HttpStatus.OK);
    }

}

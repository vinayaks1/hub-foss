package com.everwell.transition.controller;

import com.everwell.transition.model.db.DiseaseTemplate;
import com.everwell.transition.model.response.DiseaseTemplateResponse;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.repositories.DiseaseTemplateRepository;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.DiseaseTemplateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DiseaseController {

    @Autowired
    DiseaseTemplateService diseaseTemplateService;

    @Autowired
    ClientService clientService;

    private static Logger LOGGER = LoggerFactory.getLogger(DiseaseController.class);

    @GetMapping("/v1/disease")
    public ResponseEntity<Response<DiseaseTemplateResponse>> getDiseases() {
        LOGGER.debug("[getDiseases] received request for diseases");
        Long clientId = clientService.getClientByTokenOrDefault().getId();
        DiseaseTemplateResponse diseaseTemplateResponse = diseaseTemplateService.getDiseasesForClient(clientId);
        return new ResponseEntity<>(new Response<>(diseaseTemplateResponse), HttpStatus.OK);
    }

    @GetMapping("v1/disease/default")
    public ResponseEntity<Response<DiseaseTemplate>> defaultDiseaseForClientId() {
        LOGGER.debug("[defaultDiseaseForClientId] received request for defaultDiseaseForClientId");
        DiseaseTemplate diseaseTemplateResponse = diseaseTemplateService.getDefaultDiseaseForClientId(clientService.getClientByTokenOrDefault().getId());
        return new ResponseEntity<>(new Response<>(diseaseTemplateResponse), HttpStatus.OK);
    }

}

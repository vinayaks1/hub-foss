package com.everwell.transition.controller;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.request.diagnostics.TestRequestV2;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.response.diagnostics.AddTestResponseV2;
import com.everwell.transition.model.request.diagnostics.GetSampleIdListRequest;
import com.everwell.transition.model.request.dispensation.ProductSearchRequest;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.response.diagnostics.EpisodeTestsResponse;
import com.everwell.transition.model.response.diagnostics.TestResponse;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.DiagnosticsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class DiagnosticsController {

    private static Logger LOGGER = LoggerFactory.getLogger(DiagnosticsController.class);

    @Autowired
    DiagnosticsService diagnosticsService;

    @Autowired
    ClientService clientService;

    @RequestMapping(value = "/diagnostics/test-connection", method = RequestMethod.GET)
    public ResponseEntity<Response<String>> testConnection() {
        LOGGER.info("Testing Diagnostics connection");
        return diagnosticsService.testConnection();
    }

    @RequestMapping(value = "/v2/test", method = RequestMethod.POST)
    public ResponseEntity<Response<AddTestResponseV2>> addTest(@RequestBody TestRequestV2 genericTestRequest) {
        LOGGER.debug("[addTest] test request received " + genericTestRequest);
        if (StringUtils.isEmpty(genericTestRequest.getEntityId()))
            throw new ValidationException("EntityId cannot be  empty");

        ResponseEntity<Response<AddTestResponseV2>> res = diagnosticsService.addTest(genericTestRequest, clientService.getClientByTokenOrDefault().getId());
        if(res != null) {
            HttpHeaders headers = new HttpHeaders();
            headers.add(Constants.CONNECTION, Constants.KEEP_ALIVE);

            return ResponseEntity
                    .status(res.getStatusCode())
                    .headers(headers)
                    .body(res.getBody());
        }
        return res;
    }

    @RequestMapping(value = "/v1/diagnostics/kpi", method = RequestMethod.POST)
    public ResponseEntity<Response<Map<String, Object>>> filterKPI(@RequestBody Map<String, Object> kpiFilterRequest) {
        LOGGER.info("[filterKPI] request received " + kpiFilterRequest);
        return diagnosticsService.filterKPI(kpiFilterRequest);
    }

    @RequestMapping(value = "/v1/diagnostics/qr-code", method = RequestMethod.GET)
    public ResponseEntity<Response<Map<String, Object>>> qrCodeValidation(@RequestParam String qrCode) {
        LOGGER.info("[qrCodeValidation] request received" + qrCode);
        return diagnosticsService.qrCodeValidation(qrCode);
    }

    @RequestMapping(value = "/v1/diagnostics/qr-code/generate-pdf", method = RequestMethod.POST)
    public ResponseEntity<Response<Map<String, Object>>> generateQrPdf(@RequestBody Map<String, Object> request) {
        LOGGER.info("[generateQrPdf] request received " + request);
        return diagnosticsService.generateQrPdf(request);
    }
    
    @RequestMapping(value = "/v1/diagnostics/sample", method = RequestMethod.GET)
    public ResponseEntity<Response<Map<String, Object>>> getSampleByIdOrQRCode(@RequestParam(value = "by", required = false) String by,
                                                                               @RequestParam(value = "sampleId", required = false) Long sampleId,
                                                                               @RequestParam(value = "qrCode", required = false) String qrCode,
                                                                               @RequestParam(value = "journeyDetails", required = false) Boolean journeyDetails) {
        LOGGER.info("[getSampleByIdOrQRCode] request received");
        LOGGER.info(String.format("[getSampleByIdOrQRCode] search by=%s, value=%s%d, journeyDetails=%s", by, qrCode, sampleId, journeyDetails));
        return diagnosticsService.getSampleByIdOrQRCode(by, sampleId, qrCode, journeyDetails);
    }

    @GetMapping(value = "/v2/tests")
    public ResponseEntity<Response<EpisodeTestsResponse>> getTestsByEpisodeId(@RequestParam Long episodeId) {
        LOGGER.info("[getTestsByEpisodeId] request received for episodeId " + episodeId);
        EpisodeTestsResponse episodeTestsResponse = diagnosticsService.getTestsByEpisode(episodeId);
        return new ResponseEntity<>(new Response<>(episodeTestsResponse), HttpStatus.OK);
    }

    @PostMapping(value = "/v1/sampleIds")
    public ResponseEntity<Response<List<Long>>> getSampleIdListByEpisodeId(@RequestBody GetSampleIdListRequest sampleIdListRequest) {
        LOGGER.info("[getSampleIdListByEpisodeId] request received for " + sampleIdListRequest);
        List<Long> sampleIds = diagnosticsService.getSampleIdListByEpisodeId(sampleIdListRequest);
        return new ResponseEntity<>(new Response<>(sampleIds), HttpStatus.OK);
    }

    @GetMapping(value = "/v2/test")
    public ResponseEntity<Response<TestResponse>> getTestsById(@RequestParam Long testRequestId) {
        LOGGER.info("[getTestsById] request received for testRequestId " + testRequestId);
        if (null == testRequestId) {
            throw new ValidationException("Test request Id cannot be  null");
        }
        TestResponse testResponse = diagnosticsService.getTestById(testRequestId);
        return new ResponseEntity<>(new Response<>(testResponse), HttpStatus.OK);
    }

    @PutMapping(value = "/v2/test")
    public ResponseEntity<Response<Map<String, Object>>> editTest(@RequestBody Map<String, Object> requestInput) {
        LOGGER.info("[editTest] request received for requestInput " + requestInput);
        Map<String, Object> response = diagnosticsService.editTest(requestInput);
        return new ResponseEntity<>(new Response<>(response), HttpStatus.OK);
    }

    @DeleteMapping(value = "/v1/test")
    public ResponseEntity<Response<String>> deleteTest(@RequestParam Long testRequestId) {
        LOGGER.info("[deleteTest] request received for testRequestId " + testRequestId);
        if (null == testRequestId)
            throw new ValidationException("Test request Id cannot be  null");
        String response = diagnosticsService.deleteTest(testRequestId);
        return new ResponseEntity<>(new Response<>(true, response), HttpStatus.OK);
    }
}

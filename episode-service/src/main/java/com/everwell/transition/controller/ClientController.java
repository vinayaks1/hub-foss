package com.everwell.transition.controller;


import com.everwell.transition.constants.Constants;
import com.everwell.transition.model.request.RegisterClientRequest;
import com.everwell.transition.model.response.ClientResponse;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.service.ClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
public class ClientController {

    private static Logger LOGGER = LoggerFactory.getLogger(ClientController.class);

    @Autowired
    ClientService clientService;

    @RequestMapping(value = "/v1/client", method = RequestMethod.GET)
    public ResponseEntity<Response<ClientResponse>> getClient(@RequestHeader(name = Constants.X_CLIENT_ID) Long id) {
        LOGGER.info("[getClient] get client request accepted for client - " + id);
        return ResponseEntity.ok(new Response<>(clientService.getClientWithToken(id)));
    }

    @RequestMapping(value = "/v1/client", method = RequestMethod.POST)
    public ResponseEntity<Response<ClientResponse>> registerClient(@RequestBody RegisterClientRequest registerClientRequest) {
        LOGGER.info("[registerClient] register client request accepted");
        registerClientRequest.validate();
        ClientResponse clientResponse = clientService.registerClient(registerClientRequest);
        Response<ClientResponse> response = new Response<>(clientResponse);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

}

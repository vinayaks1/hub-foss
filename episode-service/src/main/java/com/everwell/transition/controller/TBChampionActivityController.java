package com.everwell.transition.controller;

import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.db.TBChampionActivity;
import com.everwell.transition.model.request.AddActivityRequest;
import com.everwell.transition.model.request.episodelogs.SyncLogsRequest;
import com.everwell.transition.model.response.EpisodeLogResponse;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.response.TBChampionActivityResponse;
import com.everwell.transition.service.TBChampionActivityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class TBChampionActivityController {

    private static Logger LOGGER = LoggerFactory.getLogger(TBChampionActivityController.class);

    @Autowired
    private TBChampionActivityService tbChampionActivityService;

    @GetMapping("/v1/tb-champ-activity/{episodeId}")
    public ResponseEntity<Response<TBChampionActivityResponse>> getActivities(@PathVariable(value = "episodeId") Long episodeId) {
        LOGGER.info("[getActivities] received request for episodeId: " + episodeId);
        if (episodeId == null || episodeId == 0)
            throw new ValidationException("episodeId must not be null!");
        TBChampionActivityResponse tbChampionActivityResponse = tbChampionActivityService.getActivities(episodeId);
        return new ResponseEntity<>(new Response<>(tbChampionActivityResponse), HttpStatus.OK);
    }

    @PostMapping("/v1/tb-champ-activity")
    public ResponseEntity<Response<String>> saveActivity(@RequestBody AddActivityRequest addActivityRequest) {
        LOGGER.info("[saveActivity] received requestfor episodeId: " + addActivityRequest.getEpisodeId());
        addActivityRequest.validate();
        tbChampionActivityService.saveActivity(addActivityRequest);
        return new ResponseEntity<>(new Response<>(true, "Activity Saved Successfully!"), HttpStatus.OK);
    }
}

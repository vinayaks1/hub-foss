package com.everwell.transition.controller;

import com.everwell.transition.model.db.Client;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.response.WeatherResponse;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.WeatherService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WeatherController {

    @Autowired
    private WeatherService weatherService;

    @Autowired
    private ClientService clientService;

    private static Logger LOGGER = LoggerFactory.getLogger(WeatherController.class);

    @GetMapping("/v1/weather")
    @ApiOperation(
            value = "Fetch Weather details",
            notes = "Weather details using lat&lon"
    )
    public ResponseEntity<Response<WeatherResponse>> getWeather(@RequestParam Double lat, @RequestParam Double lon){
        LOGGER.info(String.format("[getWeather] request received with lat %s & lon %s",lat,lon));
        WeatherResponse response = weatherService.getCompleteWeather(lat, lon, clientService.getClientByTokenOrDefault().getId());
        return new ResponseEntity<>(new Response<>(response), HttpStatus.OK);
    }
}

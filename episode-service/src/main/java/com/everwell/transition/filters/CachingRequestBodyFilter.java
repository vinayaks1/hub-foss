package com.everwell.transition.filters;

import com.everwell.transition.utils.MultipleReadHttpRequest;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class CachingRequestBodyFilter extends GenericFilterBean {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest currentRequest = (HttpServletRequest) servletRequest;
        MultipleReadHttpRequest wrappedRequest = getMultipleReadHttpRequest(currentRequest);
        filterChain.doFilter(wrappedRequest,servletResponse);
    }

    public MultipleReadHttpRequest getMultipleReadHttpRequest(HttpServletRequest httpServletRequest) throws IOException {
        return new MultipleReadHttpRequest(httpServletRequest);
    }
}

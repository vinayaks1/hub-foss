package com.everwell.transition.enums;

import com.everwell.transition.binders.EpisodeEventsBinder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
public enum Event
{
    UPDATE_USER(EpisodeEventsBinder.USER_SERVICE_EXCHANGE, "update-user"),
    DELETE_USER(EpisodeEventsBinder.USER_SERVICE_EXCHANGE,  "delete-user"),
    UPDATE_USER_MOBILE(EpisodeEventsBinder.USER_SERVICE_EXCHANGE, "update-user-mobile"),
    UPDATE_USER_EMAIL(EpisodeEventsBinder.USER_SERVICE_EXCHANGE,  "update-user-email");

    @Getter
    private final String exchangeName;

    @Getter
    private final String routingKey;
}

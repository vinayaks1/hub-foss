package com.everwell.transition.enums.episode;


import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum EpisodeField {
    STAGE_ID_LIST("stageIdList"),
    NUMBER_OF_EPISODE_RECORDS("numberOfEpisodeRecords"),
    HOUSEHOLD_VISITS("householdVisits"),
    CALL_CENTER_CALLS("callCenterCalls"),
    ENGAGEMENT_EXISTS("engagementExists"),
    START_INDEX("startIndex"),
    DUPLICATE_STATUS("duplicateStatus"),
    IS_DUPLICATE("isDuplicate"),
    DUPLICATE_OF("duplicateOf"),
    FETCH_DUPLICATE_REQUEST("fetchDuplicateRequest"),
    PROCESS_EPISODE_DUPLICATES("processEpisodeDuplicates");

    @Getter
    private final String fieldName;

}

package com.everwell.transition.enums.diagnostics;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum DiagnosticsEndpoint {
    GET_TESTS_BY_ENTITY("/diagnostics/v2/tests"),
    GET_SAMPLE_ID_LIST_BY_ENTITY("/diagnostics/v1/sampleIds"),
    EDIT_TEST("/diagnostics/v2/test"),
    DELETE_TEST("/diagnostics/v1/test"),
    GET_TEST_BY_ID("/diagnostics/v2/test");

    @Getter
    private final String endpointUrl;
}

package com.everwell.transition.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum AERSFormEnum {

    AdverseReactionForm("Adverse Event Reported",1),
    CausalityManagementForm("Add Causality and Management Details",2),
    OutcomeForm("Add an Outcome",3);

    @Getter
    private final String name;

    @Getter
    private final int order;

}

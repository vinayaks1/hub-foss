package com.everwell.transition.enums.registry;

import lombok.Getter;
import lombok.RequiredArgsConstructor;


    @RequiredArgsConstructor
    public enum RegistryEndpoints {

        GET_HIERARCHY("/registry/v1/hierarchy"),
        ADD_STAFF_MAP("/registry/v1/appointment/staff"),
        GET_QUESTIONS("/registry/v1/appointment/questions");

        @Getter
        private final String endpointUrl;
    }

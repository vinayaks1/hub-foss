package com.everwell.transition.enums.diagnostics;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum DiagnosticsField {

    ENTITY_ID("entityId");

    @Getter
    private final String fieldName;
}

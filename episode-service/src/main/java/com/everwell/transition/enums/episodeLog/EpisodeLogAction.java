package com.everwell.transition.enums.episodeLog;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum EpisodeLogAction {
    NEW_MEDICINE_MAPPINGS_ADDED("New Medicine mappings added"),
    EXISTING_MEDICINE_MAPPINGS_DELETED("Existing Medicine mappings deleted"),
    DUPLICATE_STATUS_UPDATED_By_SYSTEM("Duplicate status Updated by System"),
    MONITORING_METHOD_CHANGED("Monitoring Method Changed"),
    DELETED_TECHNOLOGY_SWITCHES("Deleted all technology switches after %s");

    @Getter
    private final String actionName;

}

package com.everwell.transition.enums;

public enum SupportedRelationType {
    HIERARCHY,
    ASSOCIATION;
}
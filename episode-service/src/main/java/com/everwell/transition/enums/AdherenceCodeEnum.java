package com.everwell.transition.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum AdherenceCodeEnum {

    QUIET("QUIET", '0'),
    ENDED("ENDED", '1'),
    MISSED("MISSED", '2'),
    UNVALIDATED("UNVALIDATED", '3'),
    RECEIVED("RECEIVED", '4'),
    RECEIVED_UNSURE("RECEIVED_UNSURE", '5'),
    NO_INFO("NO_INFO", '6'),
    BLANK("BLANK", '7'),
    ENROLLMENT("ENROLLMENT", '8'),
    MANUAL("MANUAL", '9'),
    RECEIVED_UNSCHEDULED("RECEIVED_UNSCHEDULED", 'C'),
    TFN_REPEAT_RECEIVED("TFN_REPEAT_RECEIVED", 'A'),
    TFN_REPEAT_RECEIVED_UNSURE("TFN_REPEAT_RECEIVED_UNSURE", 'B'),
    PATIENT_MANUAL("PATIENT_MANUAL", 'D'),
    PATIENT_MISSED("PATIENT_MISSED", 'E');

    @Getter
    private final String name;

    @Getter
    private final char adherenceCode;

    public static char getAdherenceCode(String name) {
        char adherenceCode = ' ';
        for(AdherenceCodeEnum code: values()) {
            if(code.name.toLowerCase().equalsIgnoreCase(name)) {
                adherenceCode = code.adherenceCode;
                break;
            }
        }
        return adherenceCode;
    }


}

package com.everwell.transition.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ValidationStatusEnum {
    ERROR_MESSAGE_FOR_GET_ADHERENCE_BULK("Something went wrong while fetching bulk adherence details");
    private final String message;
}

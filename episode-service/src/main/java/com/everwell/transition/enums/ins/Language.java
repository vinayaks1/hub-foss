package com.everwell.transition.enums.ins;

public enum Language {
    NON_UNICODE,
    UNICODE
}
package com.everwell.transition.enums.notification;

public enum NotificationActionStatus {
    ACCEPT,
    DENY
}

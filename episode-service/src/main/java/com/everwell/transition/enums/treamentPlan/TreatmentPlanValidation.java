package com.everwell.transition.enums.treamentPlan;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum TreatmentPlanValidation {
    PRODUCT_MAP_INVALID("product map cannot be empty or null"),
    PRODUCT_ID_INVALID("product id cannot be null or empty"),
    PRODUCT_NAME_INVALID("product name cannot be null or empty"),
    SCHEDULE_STRING_INVALID("invalid schedule string ! A schedule string can have only 1 or * chars and is represented by [4 chars] [space] [7 chars]"),
    PRODUCT_ID_LIST_INVALID("productIdList cannot be empty or null"),
    PRODUCT_LIST_INVALID_OR_DOES_NOT_EXIST("invalid product list : products mapping does not exist or products are not in active schedule");

    @Getter
    private  final String message;
}

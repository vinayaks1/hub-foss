package com.everwell.transition.enums.SSO;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum SSOEndpoint {

    LOGIN("/sso/v1/ssoserver/validateUserCredentials"),
    REGISTRATION("/sso/v1/user/create"),
    LOGOUT("/sso/v1/ssoserver/logout"),
    UPDATE_DEVICE_ID("/sso/v1/ssoserver/androidSessionUpdate"),
    GET_DEVICE_ID("/sso/v1/ssoserver/deviceIdsUserMap");

    @Getter
    private final String endpointUrl;
}

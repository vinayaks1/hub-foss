package com.everwell.transition.enums.episodeLog;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum EpisodeLogComments {
    NEW_MEDICINE_MAPPINGS_ADDED("%s has been added."),
    EXISTING_MEDICINE_MAPPINGS_DELETED("%s has been deleted."),
    DUPLICATE_STATUS_CHANGED("Duplicate status changed from %s to %s"),
    MONITORING_METHOD_CHANGED(" Old : %s New : %s");

    @Getter
    private final String text;
}
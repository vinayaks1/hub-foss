package com.everwell.transition.enums.iam;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum IAMField {
    ACTIVE("active"),
    IAM_START_DATE("iamStartDate"),
    SCHEDULE_TYPE("scheduleType"),
    SCHEDULE_STRING("scheduleString"),
    FIRST_DOSE_OFFSET("firstDoseOffset"),
    POSITIVE_SENSITIVITY("positiveSensitivity"),
    NEGATIVE_SENSITIVITY("negativeSensitivity"),
    MONITORING_METHOD("monitoringMethod"),
    IMEI("imei"),
    IAM_END_DATE("iamEndDate"),
    ENTITY_ID("entityId"),
    END_DATE("endDate"),
    DELETE_IF_REQUIRED("deleteIfRequired"),
    UPDATE_END_DATE("updateEndDate");

    @Getter
    private final String fieldName;
}

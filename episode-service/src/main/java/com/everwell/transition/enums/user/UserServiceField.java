package com.everwell.transition.enums.user;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum UserServiceField {

    Id("id"),
    PERSON_ID("userId"),
    MOBILE_LIST("mobileList"),
    EMAIL_LIST("emailList"),
    OFFSET("offset"),
    COUNT("count"),
    PHONE_NUMBER("phoneNumber"),
    PRIMARY_USER_ID("primaryUserId"),
    STATUS("status"),
    SECONDARY_USER_ID("secondaryUserId"),
    AGE("age"),
    PRIMARY_PHONE("primaryPhoneNumber"),
    GENDER("gender"),
    DATE_OF_BIRTH("dateOfBirth"),
    NUMBER("number"),
    STOPPED_AT("stoppedAt"),
    PRIMARY("primary");

    @Getter
    private final String fieldName;
}


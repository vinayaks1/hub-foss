package com.everwell.transition.enums.adherence;

import lombok.Getter;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
public enum AdherenceMonitoringMethod {
    MERM("MERM"),
    NNDLITE("99DOTSLite"),
    NNDOTS("99DOTS"),
    VOT("VOT"),
    NONE("NONE");

    @Getter
    private final String name;
}

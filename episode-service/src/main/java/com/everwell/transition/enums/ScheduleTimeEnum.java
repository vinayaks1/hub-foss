package com.everwell.transition.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ScheduleTimeEnum {

    MORNING(0),
    AFTERNOON(1),
    EVENING(2),
    NIGHT(3);

    @Getter
    private final Integer scheduleStringIndex;
}

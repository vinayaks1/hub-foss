package com.everwell.transition.enums.episode;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum EpisodeValidation {
    STAGE_DOES_NOT_EXIST("stage does not exist"),
    EPISODE_ID_NOT_FOUND("episode id not found"),
    EPISODE_NOT_FOUND("episode not found"),
    INVALID_FETCH_DUPLICATE_REQUEST("fetch duplicate request cannot be empty or null"),
    INVALID_EPISODE_REPORTS_FILTER("Reports filters cannot be empty!"),
    INVALID_EPISODE_REPORTS_DATE_TYPE("Invalid reports date type %s"),
    INVALID_EPISODE_ID("Invalid Episode id ! Episode id cannot be null or empty");

    @Getter
    private  final String message;
}

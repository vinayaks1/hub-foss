package com.everwell.transition.enums.dispensation;

public enum DispensationTransactionIdType {
    INTERNAL,
    EXTERNAL
}

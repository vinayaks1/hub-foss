package com.everwell.transition.enums.episodeLog;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum EpisodeLogCategory {
    MEDICINE_MAPPINGS_UPDATED("Medicine_Mappings_Updated"),
    DUPLICATE_STATUS_UPDATED("Duplicate_Status_Updated"),
    MONITORING_METHOD_CHANGED("Monitoring_Method_Changed"),
    CLOSE_CASE("Case_Closed");

    @Getter
    private final String categoryName;

}
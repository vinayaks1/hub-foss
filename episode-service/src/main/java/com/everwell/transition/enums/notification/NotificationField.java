package com.everwell.transition.enums.notification;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum NotificationField {


    NOTIFICATION_ID("notificationId"),
    STATUS("status"),

    READ("read"),
    EPISODE_ID_LIST("episodeIdList"),

    NOTIFICATION_EXTRA_KEYS("notificationExtraKeys");

    @Getter
    private final String fieldName;
}

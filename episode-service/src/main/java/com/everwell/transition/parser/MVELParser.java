package com.everwell.transition.parser;

import org.mvel2.MVEL;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class MVELParser {

    public <O> boolean parseMvelExpression(String expression, Map<String, O> inputObjects){
        return MVEL.evalToBoolean(expression, inputObjects, new HashMap<>());
    }

    public <O> void eval(String expression, Map<String, O> inputObjects, Map<String, Object> inputData){
        MVEL.eval(expression, inputObjects, inputData);
    }

}

package com.everwell.transition.model.dto;

import com.everwell.transition.model.request.episodelogs.EpisodeLogDataRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;
import java.util.Map;

@ToString
@NoArgsConstructor
@Getter
@AllArgsConstructor
public class EngagementDto {
    private Map<String, Object> updateRequest;
    List<EpisodeLogDataRequest> episodeLogDataRequest;

}

package com.everwell.transition.model.dto.dispensation;

import com.everwell.transition.enums.dispensation.DispensationCommentType;
import com.everwell.transition.enums.dispensation.DispensationTransactionIdType;
import com.everwell.transition.enums.dispensation.DispensationTransactionType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductInventoryLogDto {

    public Long id;

    public DispensationTransactionType transactionType;

    public Long transactionId;

    public Long transactionQuantity;

    public DispensationCommentType commentType;

    public String comment;

    public String dateOfAction;

    public Long pInventoryId;

    public Long productId;

    public Long updatedBy;

    public DispensationTransactionIdType transactionIdType;

    public String batchNumber;

    public String expiryDate;

}

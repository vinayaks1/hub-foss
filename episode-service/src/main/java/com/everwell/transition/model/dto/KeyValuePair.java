package com.everwell.transition.model.dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class KeyValuePair {
    @JsonAlias(value = "Key")
    String key;
    @JsonAlias(value = "Value")
    String value;
}


package com.everwell.transition.model.request.ins;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.exceptions.ValidationException;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
public class NotificationLogRequest {
    Long episodeId;
    String eventName;
    List<Map<String, Object>> notifications;

    public void validate() {
        if (episodeId == null || eventName == null) {
            throw new ValidationException("Episode Id and Event Name cannot be empty!");
        }
    }
}

package com.everwell.transition.model.dto.treatmentPlan;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TreatmentPlanConfig {

    private Boolean hasRegimenMapping = false;

    private Boolean hasProductMapping = false;
}

package com.everwell.transition.model.db;

import com.everwell.transition.constants.Constants;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "episode_tag")
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EpisodeTag {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "episode_id")
    Long episodeId;

    @Column(name = "tag_name")
    String tagName;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @Column(name = "added_on")
    LocalDateTime addedOn;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @Column(name = "tag_date")
    LocalDateTime tagDate;

    Boolean sticky;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @UpdateTimestamp
    @Column(name = "updated_on")
    private LocalDateTime updatedOn;

    public EpisodeTag(Long episodeId, String tagName, LocalDateTime tagDate, Boolean sticky) {
        this.episodeId = episodeId;
        this.tagName = tagName;
        this.addedOn = LocalDateTime.now();
        this.tagDate = tagDate;
        this.sticky = sticky;
    }

    public EpisodeTag(Long id, Long episodeId, String tagName, LocalDateTime addedOn, LocalDateTime tagDate, Boolean sticky) {
        this.id = id;
        this.episodeId = episodeId;
        this.tagName = tagName;
        this.addedOn = addedOn;
        this.tagDate = tagDate;
        this.sticky = sticky;
    }
}

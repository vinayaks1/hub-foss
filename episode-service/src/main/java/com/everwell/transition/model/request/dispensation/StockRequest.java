package com.everwell.transition.model.request.dispensation;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class StockRequest {

    public Long hierarchyMappingId;

    public Long productId;

    public String batchNumber;

    public String expiryDate;

    public Long quantity;

    public Long externalTransactionId;

    public Long lastUpdatedBy;

    public String reasonForTransaction;

}

package com.everwell.transition.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class Rule {
    RuleNamespace ruleNamespace;
    Long ruleId;
    String condition;
    String action;
    Integer priority;
    String description;
}


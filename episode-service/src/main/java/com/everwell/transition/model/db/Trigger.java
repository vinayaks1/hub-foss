package com.everwell.transition.model.db;

import com.everwell.transition.constants.Constants;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "trigger")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Trigger {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "hierarchy_id")
    private Long hierarchyId;

    @Column(name = "client_id")
    private Long clientId;

    @Column(name = "trigger_id")
    private Long triggerId;

    @Column(name = "default_template_id")
    private Long defaultTemplateId;

    @Column(name = "template_ids")
    private String templateIds;

    @Column(name = "event_name")
    private String eventName;

    @Column(name = "function_name")
    private String functionName;

    @Column(name = "cron_time")
    private String cronTime;

    @Column(name = "vendor_id")
    private Long vendorId;

    @Column(name = "notification_type")
    private String notificationType;
    
    @Column(name = "time_zone")
    private String timeZone;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @CreationTimestamp
    @Column(name = "created_on")
    private LocalDateTime createdOn;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @UpdateTimestamp
    @Column(name = "updated_on")
    private LocalDateTime updatedOn;

    public Trigger(Long id, Long hierarchyId, Long clientId, Long triggerId, Long defaultTemplateId, String templateIds, String eventName, String functionName, String cronTime, Long vendorId, String notificationType, String timeZone) {
        this.id = id;
        this.hierarchyId = hierarchyId;
        this.clientId = clientId;
        this.triggerId = triggerId;
        this.defaultTemplateId = defaultTemplateId;
        this.templateIds = templateIds;
        this.eventName = eventName;
        this.functionName = functionName;
        this.cronTime = cronTime;
        this.vendorId = vendorId;
        this.notificationType = notificationType;
        this.timeZone = timeZone;
    }
}

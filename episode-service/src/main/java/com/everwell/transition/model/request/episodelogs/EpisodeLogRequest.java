package com.everwell.transition.model.request.episodelogs;

import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.request.episodetags.EpisodeTagDataRequest;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EpisodeLogRequest {

    List<EpisodeLogDataRequest> episodeLogData;

    public void validate() {
        if (CollectionUtils.isEmpty(episodeLogData)) {
            throw new ValidationException("Log data cannot be null!");
        }
        episodeLogData.forEach(EpisodeLogDataRequest::validate);
    }
}

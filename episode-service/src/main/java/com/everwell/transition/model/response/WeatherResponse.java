package com.everwell.transition.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.lang.Nullable;

import java.util.List;

@Getter
@Setter
public class WeatherResponse {
    private Coord coord;
    private List<Weather> weather;
    private Main main;
    private List<Aqi> aqi;

    @AllArgsConstructor
    @Getter
    public static class Coord {
        private String lon;
        private String lat;
    }

    @AllArgsConstructor
    @Getter
    public static class Weather {
        private String main;
        private String description;
        private String icon;
        private String iconUrl;
    }

    @AllArgsConstructor
    @Getter
    public static class Main {
        private String temp;
        private String feelsLike;
        private String tempMin;
        private String tempMax;
        private String pressure;
        private String humidity;
    }

    @AllArgsConstructor
    @Getter
    public static class Aqi {
        public String aqi;
        public String co;
        public String no;
        public String no2;
        public String o3;
        public String so2;
        public String pm2_5;
        public String pm10;
        public String nh3;
    }
}

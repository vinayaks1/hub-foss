package com.everwell.transition.model.dto.notification;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
public class EmptyBankDetailsDto {

    Map<Long, UserSmsDetails> personIdSmsDetailsMap;
}

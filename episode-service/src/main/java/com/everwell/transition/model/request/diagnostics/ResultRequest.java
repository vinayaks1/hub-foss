package com.everwell.transition.model.request.diagnostics;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResultRequest {

    private Long testResultId;

    private String finalInterpretation;

    private String remarks;

    private String labSerialNumber;


    private String dateTested;

    private String dateReported;

}

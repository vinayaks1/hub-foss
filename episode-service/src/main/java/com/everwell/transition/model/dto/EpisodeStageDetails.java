package com.everwell.transition.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Data
public class EpisodeStageDetails {

    private Map<Long,List<EpisodeStageDto>> episodeIdToEpisodeStageDtoListMap;

    private Map<Long, Map<String, FieldDataDto>> episodeIdToFieldKeyEntityMap;


}

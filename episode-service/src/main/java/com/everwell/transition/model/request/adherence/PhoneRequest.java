package com.everwell.transition.model.request.adherence;

import lombok.*;

import java.util.List;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PhoneRequest {
    String entityId;
    List<PhoneNumbers> phones;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class PhoneNumbers {
        String phoneNumber;
        String stopDate;
    }
}

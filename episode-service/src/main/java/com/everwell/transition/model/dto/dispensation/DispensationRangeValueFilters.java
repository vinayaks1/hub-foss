package com.everwell.transition.model.dto.dispensation;

import com.everwell.transition.enums.dispensation.DispensationRangeFilterType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class DispensationRangeValueFilters {

    private DispensationRangeFilterType type;

    private String from;

    private String to;

    private Long value;
}

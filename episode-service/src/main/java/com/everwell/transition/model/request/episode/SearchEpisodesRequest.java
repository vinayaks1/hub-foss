package com.everwell.transition.model.request.episode;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SearchEpisodesRequest {
    List<Long> hiearchyIdList;
     String from;
     String to;
     Boolean isRiskStatusOptional;
     List<Long> episodeStageIdList;
     String hierarchyType;
     List<Long> diseaseTemplateIds;

}

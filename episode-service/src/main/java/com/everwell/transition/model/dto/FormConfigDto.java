package com.everwell.transition.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@NoArgsConstructor
@Getter
public class FormConfigDto {
    String formName;
    int order;
    Long id;

    public FormConfigDto(String formName, int order, Long id)
    {
        this.formName = formName;
        this.order = order;
        this.id = id;
    }
}

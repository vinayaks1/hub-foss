package com.everwell.transition.model.request.episodelogs;

import com.everwell.transition.enums.episode.EpisodeValidation;
import com.everwell.transition.exceptions.ValidationException;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UpdateSupportActionsRequest {
    Long episodeId;
    List<UpdateSupportActionsDataRequest> episodeLogData;

    public void validate() {
        if (null == episodeId) {
            throw new ValidationException(EpisodeValidation.INVALID_EPISODE_ID.getMessage());
        }
        episodeLogData.forEach(UpdateSupportActionsDataRequest::validate);
    }
}

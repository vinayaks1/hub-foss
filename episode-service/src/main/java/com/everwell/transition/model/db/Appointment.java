package com.everwell.transition.model.db;

import com.everwell.transition.constants.AppointmentConstants;
import com.everwell.transition.constants.Constants;
import com.everwell.transition.model.request.appointment.AppointmentRequest;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "appointment")
@Getter
@Setter
@Entity
@NoArgsConstructor
public class Appointment {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "appointment_id")
    private Integer appointmentId;

    @Column(name = "episode_id")
    private Long episodeId;

    @Column(name = "deleted_at")
    private LocalDateTime deletedAt;

    @Column(name = "deleted_by_id")
    private Long deletedById;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @Column(name = "date")
    private LocalDateTime date;

    @Column(name = "visit_number")
    private Integer visitNumber;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @Column(name = "follow_up_date")
    private LocalDateTime followUpDate;

    @Column(name = "status")
    private String status;

    @Column(name = "added_on")
    private LocalDateTime added_on;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @UpdateTimestamp
    @Column(name = "updated_on")
    private LocalDateTime updatedOn;


    public Appointment(AppointmentRequest appointment, String status) {
        this(null,
                null,
                appointment.getEpisodeId(),
                null,
                null,
                appointment.getDate(),
                1,
                null,
                status);
    }

    public Appointment(Integer appointmentId, Long episodeId, LocalDateTime followUpDate, Integer visitNumber, String status) {
        this(null,
                appointmentId,
                episodeId,
                null,
                null,
                followUpDate,
                visitNumber,
                null,
                status);
    }

    public Appointment(Integer id, Integer appointmentId, Long episodeId, LocalDateTime deletedAt, Long deletedById, LocalDateTime date, Integer visitNumber, LocalDateTime followUpDate, String status) {
        this.id = id;
        this.appointmentId = appointmentId;
        this.episodeId = episodeId;
        this.deletedAt = deletedAt;
        this.deletedById = deletedById;
        this.date = date;
        this.visitNumber = visitNumber;
        this.followUpDate = followUpDate;
        this.status = status;
        this.added_on = LocalDateTime.now();
    }

}

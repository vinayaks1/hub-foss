package com.everwell.transition.model.response.diagnostics;

import com.everwell.transition.elasticsearch.indices.EpisodeIndex;
import com.everwell.transition.model.dto.EpisodeDto;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Map;


@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EpisodeTestsResponse {
    private List<Map<String, Object>> tests;

    private String name;

    private Integer age;

    private String primaryPhone;

    private String gender;
}

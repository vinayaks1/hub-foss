package com.everwell.transition.model.request.episode;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DeleteRequest {
    private Long episodeId;
    private Long deletedBy;
    private String reason;
    private String additionalNotes;
}

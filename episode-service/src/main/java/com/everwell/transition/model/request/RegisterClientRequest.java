package com.everwell.transition.model.request;

import com.everwell.transition.exceptions.ValidationException;
import lombok.*;
import org.springframework.util.StringUtils;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class RegisterClientRequest {

    private String name;

    private String password;

    public void validate() {
        if (StringUtils.isEmpty(name))
            throw new ValidationException("name is required");
        if (StringUtils.isEmpty(password))
            throw new ValidationException("password is required");
        if (password.length() > 255)
            throw new ValidationException("password length should be less than 255 characters");
    }
}
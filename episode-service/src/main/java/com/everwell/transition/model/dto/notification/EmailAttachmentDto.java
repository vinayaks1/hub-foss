package com.everwell.transition.model.dto.notification;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class EmailAttachmentDto implements Serializable {
    String attachmentContent;
    String attachmentName;
    String attachmentType;
}

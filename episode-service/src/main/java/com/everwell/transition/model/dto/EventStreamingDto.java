package com.everwell.transition.model.dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Data
public class EventStreamingDto<T> {
    @JsonAlias("EventName")
    String eventName;
    @JsonAlias("Field")
    T field;
}
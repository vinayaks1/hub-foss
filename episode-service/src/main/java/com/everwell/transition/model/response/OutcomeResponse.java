package com.everwell.transition.model.response;

import com.everwell.transition.model.db.AersOutcome;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.time.LocalDate;

@AllArgsConstructor
@ToString
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OutcomeResponse {
    Long adverseEventId;
    Long episodeId;
    String outcome;
    String dateOfResolution;
    String dateOfDeath;
    String causeOfDeath;
    String autopsyPerformed;
    String hospitalAdmissionDate;
    String hospitalDischargeDate;

    public OutcomeResponse(AersOutcome outcome, Long episodeId, Long adverseEventId) {
        this.episodeId = episodeId;
        this.adverseEventId = adverseEventId;
        this.outcome = outcome.getOutcome();
        this.autopsyPerformed = outcome.getAutopsyPerformed();
        this.causeOfDeath = outcome.getCauseOfDeath();
        this.dateOfResolution = outcome.getDateOfResolution() == null ? null : outcome.getDateOfResolution().toString();
        this.dateOfDeath = outcome.getDateOfDeath() == null ? null : outcome.getDateOfDeath().toString();
        this.hospitalAdmissionDate = outcome.getHospitalAdmissionDate() == null ? null : outcome.getHospitalAdmissionDate().toString();
        this.hospitalDischargeDate = outcome.getHospitalDischargeDate() == null ? null : outcome.getHospitalDischargeDate().toString();
    }
}

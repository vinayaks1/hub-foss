package com.everwell.transition.model.db;

import com.everwell.transition.constants.Constants;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "episode_app_config")
@Getter
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class EpisodeAppConfig {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "technology")
    private String technology;

    @Column(name = "allow_dose_marking")
    private boolean allowDoseMarking;

    @Column(name = "validation_required")
    private boolean validationRequired;

    @Column(name = "backdated_dose_allowed")
    private boolean backdatedDoseAllowed;

    @Column(name = "backdated_limit")
    private Long backdatedLimit;

    @Column(name = "deployment")
    private String deployment;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @CreationTimestamp
    @Column(name = "created_on")
    private LocalDateTime createdOn;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @UpdateTimestamp
    @Column(name = "updated_on")
    private LocalDateTime updatedOn;

    public EpisodeAppConfig(Long id, String technology, boolean allowDoseMarking, boolean validationRequired, boolean backdatedDoseAllowed, Long backdatedLimit, String deployment) {
        this.id = id;
        this.technology = technology;
        this.allowDoseMarking = allowDoseMarking;
        this.validationRequired = validationRequired;
        this.backdatedDoseAllowed = backdatedDoseAllowed;
        this.backdatedLimit = backdatedLimit;
        this.deployment = deployment;
    }
}

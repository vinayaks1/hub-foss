package com.everwell.transition.model.dto.notification;

import com.everwell.transition.model.dto.EpisodeDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class NotificationEventDto {
    private EpisodeDto episodeDto;
    private String doctorName;
    private LocalDateTime appointmentDate;
    private Map<String, Object> extraData;
    private String eventName;
}

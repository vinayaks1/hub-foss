package com.everwell.transition.model.request.episodelogs;

import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.db.EpisodeLog;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UpdateSupportActionsDataRequest extends EpisodeLogDataRequest {
    Long id;

    public UpdateSupportActionsDataRequest(Long id, Long episodeId, String category, String subCategory, Long addedBy, String actionTaken, String comments, LocalDateTime dateOfAction)
    {
        super(episodeId, category, subCategory, addedBy, actionTaken, comments, dateOfAction);
        this.id = id;
    }

    public void validate()
    {
        super.validate();
        if (id == null)
            throw new ValidationException("Id cannot be empty!");
    }

    /**
     * Used to check if the request object has modified data with respect to the existing log entry
     * @param existingLog - existing log object
     * @return if the request object is equivalent to the existing log entry
     */
    public boolean equal(EpisodeLog existingLog)
    {
        return (
            this.id.equals(existingLog.getId())
            && this.episodeId.equals(existingLog.getEpisodeId())
            && (
                (this.addedBy == null && existingLog.getAddedBy() == null)
                    ||
                (
                    (this.addedBy != null && existingLog.getAddedBy() != null)
                    && this.addedBy.equals(existingLog.getAddedBy())
                )
            )
            && (
                (this.category == null && existingLog.getCategory() == null)
                    ||
                (
                    (this.category != null && existingLog.getCategory() != null)
                    && this.category.equals(existingLog.getCategory())
                )
            )
            && (
                (this.subCategory == null && existingLog.getSubCategory() == null)
                    ||
                (
                    (this.subCategory != null && existingLog.getSubCategory() != null)
                    && this.subCategory.equals(existingLog.getSubCategory())
                )
            )
            && (
                (this.comments == null && existingLog.getComments() == null)
                    ||
                (
                    (this.comments != null && existingLog.getComments() != null)
                    && this.comments.equals(existingLog.getComments())
                )
            )
            && (
                (this.actionTaken == null && existingLog.getActionTaken() == null)
                    ||
                (
                    (this.actionTaken != null && existingLog.getActionTaken() != null)
                    && this.actionTaken.equals(existingLog.getActionTaken())
                )
            )
            && (
                (this.dateOfAction == null && existingLog.getDateOfAction() == null)
                    ||
                (
                    (this.dateOfAction != null && existingLog.getDateOfAction() != null)
                    && this.dateOfAction.equals(existingLog.getDateOfAction())
                )
            )
        );
    }
}

package com.everwell.transition.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.List;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EngagementResponse {
    List<Map<String, Object>> engagements;
    Boolean callCenterCalls;
    Boolean householdVisits;
    Long currentHierarchyId;
}

package com.everwell.transition.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response<T> {
    boolean success;
    T data;
    String message;

    public Response(T data) {
        this.data = data;
        success = true;
    }

    public Response(T data, boolean success) {
        this.data = data;
        this.success = success;
    }

    public Response(boolean success, T data) {
        this.data = data;
        this.success = success;
    }

    public Response(String message) {
        success = false;
        this.message = message;
    }
}

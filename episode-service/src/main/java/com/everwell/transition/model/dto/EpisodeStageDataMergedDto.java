package com.everwell.transition.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class EpisodeStageDataMergedDto {

    private Map<Long, Map<String, String>> episodeStageIdToStageDataMappingsListMap;

    private Map<Long, Map<String, FieldDataDto>> episodeIdToFieldKeyEntityMap;
}



package com.everwell.transition.model.request.treatmentPlan;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DoseDetails {
    String time;
    String extraInfo;
    String pillNumber;
}

package com.everwell.transition.model.request.dispensation;

import lombok.*;

import java.util.List;

@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ProductSearchRequest {

    @Setter
    public List<Long> productIds;

    public List<String> drugCodes;

    public String pattern;

    @Setter
    private Long episodeId;
}

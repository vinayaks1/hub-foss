package com.everwell.transition.model.request.appointment;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class StaffMapRequest {
    private Long staffId;
    private Integer appointmentId;
}

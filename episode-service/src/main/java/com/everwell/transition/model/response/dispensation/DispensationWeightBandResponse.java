package com.everwell.transition.model.response.dispensation;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class DispensationWeightBandResponse {
    String weightBand;
}

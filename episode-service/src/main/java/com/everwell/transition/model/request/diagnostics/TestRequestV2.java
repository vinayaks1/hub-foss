package com.everwell.transition.model.request.diagnostics;

import com.everwell.transition.enums.diagnostics.TestTypeEnum;
import com.everwell.transition.model.dto.diagnostics.TypeRequest;
import lombok.Data;

import java.util.Map;


@Data
public class TestRequestV2 extends GenericTestRequest {
    private Map<TestTypeEnum, TypeRequest> typeRequestMap;
}

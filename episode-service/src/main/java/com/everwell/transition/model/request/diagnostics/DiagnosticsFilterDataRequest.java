package com.everwell.transition.model.request.diagnostics;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DiagnosticsFilterDataRequest {

    private Map<String, Object> filters;

    private List<Long> entityIdList;

    private List<Map<String, Object>> testList;
}

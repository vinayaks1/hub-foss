package com.everwell.transition.model.response;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.model.dto.treatmentPlan.TreatmentPlanProductMapDto;
import com.everwell.transition.model.request.treatmentPlan.DoseDetails;
import com.everwell.transition.model.response.dispensation.ProductResponse;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class ProductResponseWithScheduleDto {

    private Long productId;

    private String drugCode;

    private String productName;

    private String dosage;

    private String dosageForm;

    private String manufacturer;

    private String composition;

    private String schedule;

    private String otherDosageForm;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    private LocalDateTime createdDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    private LocalDateTime startDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    private LocalDateTime endDate;

    private Long productPlanMapId;

    private String otherDrugName;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    private LocalDateTime refillDueDate;

    private List<Map<String, Object>> doseDetailsList;

    private String mergedAdherence;

    private Boolean isSos;

    public ProductResponseWithScheduleDto(ProductResponse product, String scheduleString, LocalDateTime refillDueDate) {
        this.productId = product.productId;
        this.drugCode = product.drugCode;
        this.productName = product.productName;
        this.dosage = product.dosage;
        this.dosageForm = product.dosageForm;
        this.manufacturer = product.manufacturer;
        this.composition = product.composition;
        this.schedule = scheduleString;
        this.refillDueDate = refillDueDate;
    }

    public ProductResponseWithScheduleDto(ProductResponse product, String scheduleString, LocalDateTime refillDueDate, TreatmentPlanProductMapDto treatmentPlanProductMapDto) {
        this.productId = product.productId;
        this.drugCode = product.drugCode;
        this.productName = product.productName;
        this.dosage = product.dosage;
        this.dosageForm = product.dosageForm;
        this.manufacturer = product.manufacturer;
        this.composition = product.composition;
        this.schedule = scheduleString;
        this.refillDueDate = refillDueDate;
        this.startDate = treatmentPlanProductMapDto.getStartDate();
        this.endDate = treatmentPlanProductMapDto.getEndDate();
        this.createdDate = treatmentPlanProductMapDto.getCreatedDate();
        this.productPlanMapId = treatmentPlanProductMapDto.getTreatmentPlanProductMapId();
        this.otherDrugName = treatmentPlanProductMapDto.getOtherDrugName();
        this.doseDetailsList = treatmentPlanProductMapDto.getDoseDetailsList();
        this.mergedAdherence = treatmentPlanProductMapDto.getAdherenceString();
        this.otherDosageForm = treatmentPlanProductMapDto.getOtherDosageForm();
        this.isSos = treatmentPlanProductMapDto.getIsSos();
    }
}

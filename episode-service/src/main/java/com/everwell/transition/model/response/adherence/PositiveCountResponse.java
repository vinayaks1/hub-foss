package com.everwell.transition.model.response.adherence;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PositiveCountResponse {
    Long manualCount;
    Long digitalCount;
    Long missedCount;
}

package com.everwell.transition.model.request.episodetags;

import com.everwell.transition.exceptions.ValidationException;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EpisodeTagRequest {

    List<EpisodeTagDataRequest> episodeTagData;

    public void validate() {
        if (CollectionUtils.isEmpty(episodeTagData)) {
            throw new ValidationException("Tag data is null!");
        }
        episodeTagData.forEach(EpisodeTagDataRequest::validate);
    }

}

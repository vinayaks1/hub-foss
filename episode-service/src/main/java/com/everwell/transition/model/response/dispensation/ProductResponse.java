package com.everwell.transition.model.response.dispensation;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ProductResponse {

    public Long productId;

    public String drugCode;

    public String productName;

    public String dosage;

    public String dosageForm;

    public String manufacturer;

    public String composition;
}

package com.everwell.transition.model.response.SSO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeviceIdsMapResponse {

    private Map<String, List<String>> userDeviceIdsMap;
}

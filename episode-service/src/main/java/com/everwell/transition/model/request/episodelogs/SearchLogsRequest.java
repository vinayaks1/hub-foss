package com.everwell.transition.model.request.episodelogs;

import com.everwell.transition.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SearchLogsRequest {
    List<Long> episodeIdList;
    Long addedBy;
    List<String> categoriesToFilter;

    public void validate()
    {
        if(CollectionUtils.isEmpty(episodeIdList))
            throw new ValidationException("Episode Id List cannot be empty");
    }
}

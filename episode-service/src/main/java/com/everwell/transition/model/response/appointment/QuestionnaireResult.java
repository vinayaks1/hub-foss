package com.everwell.transition.model.response.appointment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class QuestionnaireResult {
    private Long id;
    private String type;
    private Long order;
    private String question;
    private String options;
    private String validationList;
    private String answer;
}

package com.everwell.transition.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class AllAdherenceResponse {

    private List<AdherenceResponse> adherenceResponseList;

    @Setter
    private String mergedAdherence;

    public AllAdherenceResponse(List<AdherenceResponse> responseList) {
        this.adherenceResponseList = responseList;
    }
}

package com.everwell.transition.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EpisodeEsInsertUpdateDto {
    Long episodeId;
    Long clientId;
    boolean isUpdate;
    boolean isHierarchyDataUpdated;
}

package com.everwell.transition.model.response.dispensation;

import com.everwell.transition.model.dto.dispensation.DispensationData;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class EntityDispensationResponse {

    public Long entityId;

    public List<DispensationData> dispensations;

    public String refillDate;

    private Long totalDispensedDrugs;

    public EntityDispensationResponse(Long entityId, List<DispensationData> dispensationDataList) {
        this.entityId = entityId;
        this.dispensations = dispensationDataList;
    }
}

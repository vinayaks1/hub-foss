package com.everwell.transition.model.db;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Table(name = "rules")
@Getter
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Rules {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "rule_namespace")
    private String ruleNamespace;

    @Column(name = "condition")
    private String condition;

    @Column(name = "action")
    private String action;

    @Column(name = "priority")
    private Integer priority;

    @Column(name = "description")
    private String description;

    @Column(name = "to_id")
    private Long toId;

    @Column(name = "from_id")
    private Long fromId;

    @Column(name = "client_id")
    private Long clientId;

}
package com.everwell.transition.model.request.tasklistfilters;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaskListFilterRequest {
    String filterName;
    String value;
    Long filterValueId;
}

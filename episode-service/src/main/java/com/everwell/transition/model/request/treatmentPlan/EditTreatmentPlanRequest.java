package com.everwell.transition.model.request.treatmentPlan;

import com.everwell.transition.enums.treamentPlan.TreatmentPlanValidation;
import com.everwell.transition.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EditTreatmentPlanRequest {
    String startDate;
    List<TreatmentPlanProductMapRequest> productMap;
    String dayStartTime;

    public void validate() {
        if (CollectionUtils.isEmpty(productMap)) {
            throw new ValidationException(TreatmentPlanValidation.PRODUCT_MAP_INVALID.getMessage());
        }
        productMap.forEach(p -> {
            p.validate();
            if (StringUtils.isEmpty(p.productName)) {
                throw new ValidationException(TreatmentPlanValidation.PRODUCT_NAME_INVALID.getMessage());
            }
        });
    }
}

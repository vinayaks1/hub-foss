package com.everwell.transition.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AggregationDto {
    private int size;
    private String key;
    private String fieldName;
}

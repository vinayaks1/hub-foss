package com.everwell.transition.model;

import com.everwell.transition.model.dto.dtoInterface.EpisodeFieldIdValueDtoInterface;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class EpisodeFieldIdValue implements EpisodeFieldIdValueDtoInterface {

    Long episodeId;

    Long fieldId;

    String fieldValue;
}

package com.everwell.transition.model.db;

import com.everwell.transition.constants.Constants;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "episode_hierarchy_linkage")
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class EpisodeHierarchyLinkage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "episode_id")
    private Long episodeId;

    @Column(name = "relation_id")
    private Long relationId;

    @Setter
    @Column(name = "hierarchy_id")
    private Long hierarchyId;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @CreationTimestamp
    @Column(name = "created_on")
    private LocalDateTime createdOn;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @UpdateTimestamp
    @Column(name = "updated_on")
    private LocalDateTime updatedOn;

    public EpisodeHierarchyLinkage(Long episodeId, Long relationId, Long hierarchyId) {
        this.episodeId = episodeId;
        this.relationId = relationId;
        this.hierarchyId = hierarchyId;
    }

    public EpisodeHierarchyLinkage(Long id, Long episodeId, Long relationId, Long hierarchyId) {
        this.id = id;
        this.episodeId = episodeId;
        this.relationId = relationId;
        this.hierarchyId = hierarchyId;
    }
}

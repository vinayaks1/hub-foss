package com.everwell.transition.model.dto.episode;


import com.everwell.transition.model.dto.EpisodeDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EpisodeTransitionDetails {
    private EpisodeDto episodeDtoBeforeUpdate;
    private EpisodeDto episodeDtoAfterUpdate;
}

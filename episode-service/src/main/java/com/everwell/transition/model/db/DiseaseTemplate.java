package com.everwell.transition.model.db;

import com.everwell.transition.constants.Constants;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "disease_template")
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class DiseaseTemplate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "disease_name")
    private String diseaseName;

    @Column(name = "client_id")
    private Long clientId;

    @Column(name = "is_default")
    private Boolean isDefault;

    @Column(name = "display_name")
    private String displayName;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @CreationTimestamp
    @Column(name = "created_on")
    private LocalDateTime createdOn;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @UpdateTimestamp
    @Column(name = "updated_on")
    private LocalDateTime updatedOn;

    public DiseaseTemplate(Long id, String diseaseName, Long clientId, Boolean isDefault, String displayName) {
        this.id = id;
        this.diseaseName = diseaseName;
        this.clientId = clientId;
        this.isDefault = isDefault;
        this.displayName = displayName;
    }
}

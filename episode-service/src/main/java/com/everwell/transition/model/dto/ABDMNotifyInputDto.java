package com.everwell.transition.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ABDMNotifyInputDto {
    private Long clientId;
    private Long episodeId;
}

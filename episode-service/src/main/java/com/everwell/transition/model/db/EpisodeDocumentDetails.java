package com.everwell.transition.model.db;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.model.request.AddEpisodeDocumentRequest;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "episode_document_details")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EpisodeDocumentDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "episode_document_id")
    private Long episodeDocumentId;
    @Column(name = "type")
    private String type;
    @Column(name = "file_url")
    private String fileUrl;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "date_of_record")
    private LocalDateTime dateOfRecord;
    @Column(name = "is_approved")
    private Boolean isApproved;
    @Column(name = "validated_by")
    private Long validatedBy;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "validated_at")
    private LocalDateTime validatedAt;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @CreationTimestamp
    @Column(name = "created_on")
    private LocalDateTime createdOn;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @UpdateTimestamp
    @Column(name = "updated_on")
    private LocalDateTime updatedOn;

    public EpisodeDocumentDetails(AddEpisodeDocumentRequest addEpisodeDocumentRequest, int index, Long episodeDocumentId, String fileUrl) {
        this.episodeDocumentId = episodeDocumentId;
        this.type = addEpisodeDocumentRequest.getType().get(index);
        this.dateOfRecord = Utils.toLocalDateTimeWithNull(addEpisodeDocumentRequest.getDateOfRecord().get(index), Constants.DATE_FORMAT);
        this.fileUrl = fileUrl;
    }
}

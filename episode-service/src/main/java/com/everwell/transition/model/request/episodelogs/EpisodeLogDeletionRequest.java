package com.everwell.transition.model.request.episodelogs;

import com.everwell.transition.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EpisodeLogDeletionRequest {
    Long episodeId;
    List<String> categories;
    String deleteAllAfter;

    public void validate()
    {
        if (episodeId == null)
            throw new ValidationException("Episode Id cannot be null!");

        if (CollectionUtils.isEmpty(categories))
            throw new ValidationException("Categories cannot be empty!");
    }

}

package com.everwell.transition.model.dto.adherence;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DosageCounts {
    long missed;
    long manual;
    long called;
    long calledAndManual;
    long total;
    double callsOnlyAdherencePercentage;
    double callsAndManualAdherencePercentage;
    boolean neverCalled;
    long noInfo;

    public DosageCounts(DosageCountsDetailed dosageCountsDetailed) {
        this.missed = dosageCountsDetailed.missed + dosageCountsDetailed.noInfo;
        this.manual = dosageCountsDetailed.manual;
        this.called = dosageCountsDetailed.enrollment + dosageCountsDetailed.received + dosageCountsDetailed.receivedUnsure + dosageCountsDetailed.ended + dosageCountsDetailed.repeatReceived + dosageCountsDetailed.repeatReceivedUnsure + dosageCountsDetailed.unValidated + dosageCountsDetailed.receivedUnscheduled;
        this.calledAndManual = this.called + dosageCountsDetailed.manual;
        this.total = this.calledAndManual + dosageCountsDetailed.missed + dosageCountsDetailed.noInfo;
        this.callsOnlyAdherencePercentage = ((this.total > 0) ? this.called * 100.0d / this.total : 0);
        this.callsAndManualAdherencePercentage = ((this.total > 0) ? this.calledAndManual * 100.0d / this.total : 0);
        this.neverCalled = (dosageCountsDetailed.received + dosageCountsDetailed.receivedUnsure) == 0;
        this.noInfo = dosageCountsDetailed.noInfo;
    }

}

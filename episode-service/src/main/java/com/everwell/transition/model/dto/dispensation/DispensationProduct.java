package com.everwell.transition.model.dto.dispensation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.List;

@ToString
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DispensationProduct {
    public Long id;

    public Long productId;

    public String productName;

    public Long productConfigId;

    public Long dispensationId;

    public String unitOfMeasurement;

    public String numberOfUnitsIssued;

    public String refillDate;

    public String dosingSchedule;

    public List<StockQuantityDto> stockData;

    public List<ProductInventoryLogDto> logs;

    public DispensationProduct(Long id, String numberOfUnitsIssued) {
        this.id = id;
        this.numberOfUnitsIssued = numberOfUnitsIssued;
    }
}

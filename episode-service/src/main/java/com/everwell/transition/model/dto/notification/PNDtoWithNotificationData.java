package com.everwell.transition.model.dto.notification;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PNDtoWithNotificationData {
    List<PushNotificationTemplateDto> pushNotificationTemplateDtoList;
    Boolean sendNotificationData;
    String notificationExtraData;
    Boolean isMandatory;
}

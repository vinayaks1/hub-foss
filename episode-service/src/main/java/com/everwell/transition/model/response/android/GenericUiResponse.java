package com.everwell.transition.model.response.android;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GenericUiResponse {
    String title;
    String subTitle;
    Integer order;
    String icon;
    //supported: 3x2, 2.5x2.5, 3x1.5, 2x3
    String sectionType;
    String widgetType;
    String orientation;
    List<AndroidItem> items;
    //supported: s, m, l, xl
    String fontSize;
    //supported: left, center
    String fontAlign;
    //supported: true/false
    Boolean showChildItemTitle;
    // supported: 0, s=10px, m, l, xl=50px
    String radius;

    Boolean showTitle;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class AndroidItem {
        String imageUrl;
        String title;
        String subText;
        String onClickUrl;
    }

    public GenericUiResponse(Integer order, String widgetType) {
        this.order = order;
        this.widgetType = widgetType;
    }
}

package com.everwell.transition.model.db;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Table(name = "adverse_event")
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class AdverseEvent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "episode_id")
    Long episodeId;

    @Column(name = "adverse_reaction_id")
    Long adverseReactionId;

    @Column(name = "causality_management_id")
    Long causalityManagementId;

    @Column(name = "outcome_id")
    Long outcomeId;

    @Column(name = "created_on")
    private LocalDateTime createdOn;

    @Column(name = "onset_date")
    private LocalDateTime onsetDate;

    @Column(name = "outcome_date")
    private LocalDateTime outcomeDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @UpdateTimestamp
    @Column(name = "updated_on")
    private LocalDateTime updatedOn;


    public AdverseEvent(Long episodeId, Long adverseReactionId) {
        this.episodeId = episodeId;
        this.adverseReactionId = adverseReactionId;
        this.createdOn = LocalDateTime.now();
    }

    public AdverseEvent(Long episodeId, Long adverseReactionId, String onsetDate) {
        this.episodeId = episodeId;
        this.adverseReactionId = adverseReactionId;
        this.createdOn = LocalDateTime.now();
        this.onsetDate = Utils.toLocalDateTimeWithNull(onsetDate);
    }

    public AdverseEvent(Long episodeId, Long adverseReactionId, Long causalityManagementId) {
        this.episodeId = episodeId;
        this.adverseReactionId = adverseReactionId;
        this.causalityManagementId = causalityManagementId;
    }

    public AdverseEvent(Long episodeId, Long adverseReactionId, Long causalityManagementId, Long outcomeId) {
        this.episodeId = episodeId;
        this.adverseReactionId = adverseReactionId;
        this.causalityManagementId = causalityManagementId;
        this.outcomeId = outcomeId;
    }

    public AdverseEvent(Long episodeId, Long adverseReactionId, Long causalityManagementId,Long outcomeId ,String onsetDate) {
        this.episodeId = episodeId;
        this.adverseReactionId = adverseReactionId;
        this.causalityManagementId = causalityManagementId;
        this.outcomeId = outcomeId;
        this.onsetDate = Utils.toLocalDateTimeWithNull(onsetDate);
    }

    public AdverseEvent(Long episodeId, Long adverseReactionId, Long causalityManagementId,Long outcomeId ,String onsetDate, String outcomeDate) {
        this.episodeId = episodeId;
        this.adverseReactionId = adverseReactionId;
        this.causalityManagementId = causalityManagementId;
        this.outcomeId = outcomeId;
        this.onsetDate = Utils.toLocalDateTimeWithNull(onsetDate);
        this.outcomeDate = Utils.toLocalDateTimeWithNull(outcomeDate);
    }

}

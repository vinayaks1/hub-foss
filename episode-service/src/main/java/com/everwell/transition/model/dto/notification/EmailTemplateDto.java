package com.everwell.transition.model.dto.notification;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmailTemplateDto implements Serializable {
    String body;
    String subject;
    Map<String,String> bodyParameters;
    Map<String,String> subjectParameters;
    String recipient;
    Long languageId;
}

package com.everwell.transition.model.db;


import com.everwell.transition.constants.Constants;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "appointment_answer")
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class AppointmentAnswer {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;

    @Column(name = "appointment_id")
    private Integer appointmentId;

    @Column(name = "field_id")
    private Long fieldId;

    @Column(name = "answer")
    private String answer;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @UpdateTimestamp
    @Column(name = "updated_on")
    private LocalDateTime updatedOn;

    public AppointmentAnswer(Integer id, Integer appointmentId, Long fieldId, String answer) {
        Id = id;
        this.appointmentId = appointmentId;
        this.fieldId = fieldId;
        this.answer = answer;
    }
}

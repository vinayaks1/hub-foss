package com.everwell.transition.model.request.dispensation;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ProductBulkRequest {
    public List<ProductRequest> productDetails;

}

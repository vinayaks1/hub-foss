package com.everwell.transition.model.db;

import com.everwell.transition.constants.Constants;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "episode_association")
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class EpisodeAssociation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "episode_id")
    private Long episodeId;

    @Column(name = "association_id")
    private Long associationId;

    @Setter
    @Column(name = "value")
    private String value;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @CreationTimestamp
    @Column(name = "created_on")
    private LocalDateTime createdOn;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @UpdateTimestamp
    @Column(name = "updated_on")
    private LocalDateTime updatedOn;

    public EpisodeAssociation(Long episodeId, Long associationId, String value) {
        this.episodeId = episodeId;
        this.associationId = associationId;
        this.value = value;
    }

    public EpisodeAssociation(Long id, Long episodeId, Long associationId, String value) {
        this.id = id;
        this.episodeId = episodeId;
        this.associationId = associationId;
        this.value = value;
    }
}

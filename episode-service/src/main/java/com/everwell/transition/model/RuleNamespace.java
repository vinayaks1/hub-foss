package com.everwell.transition.model;

public enum RuleNamespace {
    STAGE_TRANSITION,
    KEY_VALUE,
    WEIGHT_BAND
}

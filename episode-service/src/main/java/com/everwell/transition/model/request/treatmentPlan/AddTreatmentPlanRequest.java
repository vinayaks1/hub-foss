package com.everwell.transition.model.request.treatmentPlan;

import com.everwell.transition.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddTreatmentPlanRequest {

    String name;
    Long regimenId;
    String startDate;
    List<TreatmentPlanProductMapRequest> productMap;
    Long episodeId;

    public void validate() {
        if(StringUtils.isEmpty(name)) {
            throw new ValidationException("name cannot be null or empty");
        }
        productMap.forEach(TreatmentPlanProductMapRequest::validate);
    }

    public AddTreatmentPlanRequest(String name, Long regimenId, String startDate, List<TreatmentPlanProductMapRequest> productMap) {
        this.name = name;
        this.regimenId = regimenId;
        this.startDate = startDate;
        this.productMap = productMap;
    }
}

package com.everwell.transition.model.response.adherence;

import com.everwell.transition.model.response.EditDosesResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class EditDoseBulkResponse {

    List<EditDosesResponse> editDosesResponseList;

    String mergedAdherence;

}

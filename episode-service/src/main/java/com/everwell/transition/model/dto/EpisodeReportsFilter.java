package com.everwell.transition.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EpisodeReportsFilter {
    private String hierarchyToSearch;

    private List<Long> hierarchyIds;

    private List<Long> episodeIdList;

    private List<String> typeOfEpisodeList;

    private List<String> stageList;

    private String dateType;

    private String startDate;

    private String endDate;
}

package com.everwell.transition.model.request;

import com.everwell.transition.model.request.episodesearch.EpisodeSearchRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RefreshTagsRequest {
    EpisodeSearchRequest episodeSearchRequest;
    String deploymentCode;
    Long vendorId;
    Long clientId;
    Long triggerId;
    Long templateId;
}

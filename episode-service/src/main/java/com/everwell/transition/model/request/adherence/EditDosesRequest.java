package com.everwell.transition.model.request.adherence;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.exceptions.ValidationException;
import lombok.*;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class EditDosesRequest implements Serializable {

    Long episodeId;
    String monitoringMethod;
    Long userId;
    String code;
    List<String> utcDates;
    Boolean addDoses = false;
    String addedNote;
    Boolean adherenceDispensationValidation = false;
    String timezone;
    Boolean fetchAdherence = false;
    Boolean addLogs = true;
    String iamEntityId;

    public void validate (EditDosesRequest editDosesRequest) {
        if (editDosesRequest.getEpisodeId() == null)
            throw new ValidationException("Episode Id cannot be null!!");
        if (editDosesRequest.getUserId() == null && editDosesRequest.getAddLogs())
            throw new ValidationException("User Id cannot be null!");
        if (!StringUtils.hasText(editDosesRequest.getCode()))
            throw new ValidationException("Adherence Code cannot be null!");
        if (!StringUtils.hasText(editDosesRequest.getMonitoringMethod()))
            throw new ValidationException("Monitoring Method cannot be empty!");
        if (CollectionUtils.isEmpty(editDosesRequest.getUtcDates()))
            throw new ValidationException("Dates cannot be empty");
        if (editDosesRequest.getUtcDates().size() > Constants.ALTER_DOSES_LIMIT)
            throw new ValidationException("Number of days cannot be more than " + Constants.ALTER_DOSES_LIMIT);
        if (!StringUtils.hasText(editDosesRequest.getTimezone()))
            throw new ValidationException("Timezone cannot be empty!");
    }

}

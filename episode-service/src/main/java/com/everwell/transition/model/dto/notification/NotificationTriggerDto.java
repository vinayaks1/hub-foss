package com.everwell.transition.model.dto.notification;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationTriggerDto {
    private Long hierarchyId;
    private Long vendorId;
    private Long templateId;
    private Long triggerId;
    private String functionName;
    private String timeZone;
    private String notificationType;
    private Boolean mandatory;
    private Boolean defaultConsent;
    private Boolean isDefaultTime;
    private String timeOfSms;
    private List<Long> templateIds;

    public NotificationTriggerDto(Long vendorId, Long templateId, Long triggerId, String functionName, String timeZone) {
        this.vendorId = vendorId;
        this.templateId = templateId;
        this.triggerId = triggerId;
        this.functionName = functionName;
        this.timeZone = timeZone;
    }
}

package com.everwell.transition.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

@ToString
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AERSStatusResponse {
    List<FormConfigResponse> aersStatusList;

    public AERSStatusResponse(List<FormConfigResponse> aersStatusList) {
        this.aersStatusList = aersStatusList;
    }
}

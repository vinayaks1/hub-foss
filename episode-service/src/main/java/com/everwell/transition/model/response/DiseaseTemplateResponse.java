package com.everwell.transition.model.response;

import com.everwell.transition.model.db.DiseaseTemplate;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class DiseaseTemplateResponse {
    List<DiseaseTemplate> diseases;
}

package com.everwell.transition.model.dto.appointment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Document {
    private Long id;
    private String title;
    private String description;
    private String url;
    private String thumbnail;
    private LocalDateTime dateOfRecord;
}


package com.everwell.transition.model.db;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.*;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "treatment_plan_product_map")
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TreatmentPlanProductMap {

    @Id
    @Getter
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @Column(name = "created_date")
    @Getter
    @Setter
    private LocalDateTime createdDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @Column(name = "start_date")
    @Getter
    @Setter
    private LocalDateTime startDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @Column(name = "end_date")
    @Getter
    @Setter
    private LocalDateTime endDate;

    @Getter
    @Column(name = "treatment_plan_id")
    private Long treatmentPlanId;

    @Getter
    @Column(name = "product_id")
    private Long productId;

    @Column(name = "schedule")
    @Getter
    private String schedule;

    @Column(name = "refill_due_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @Getter
    @Setter
    private LocalDateTime refillDueDate;

    @Column(name = "other_drug_name")
    @Getter
    @Setter
    private String otherDrugName;

    @Column(name = "other_dosage_form")
    @Getter
    @Setter
    private String otherDosageForm;

    @Column(name = "sos", columnDefinition = "boolean default false")
    @Getter
    @Setter
    private Boolean sos;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @UpdateTimestamp
    @Column(name = "updated_on")
    private LocalDateTime updatedOn;

    public TreatmentPlanProductMap(Long treatmentPlanId, Long productId, String schedule, LocalDateTime startDate, LocalDateTime endDate, LocalDateTime refillDueDate, String otherDrugName, String otherDosageForm, Boolean sos) {
        this.treatmentPlanId = treatmentPlanId;
        this.productId = productId;
        this.schedule = schedule;
        this.createdDate = Utils.getCurrentDateNew();
        this.startDate = startDate;
        this.endDate = endDate;
        this.refillDueDate = refillDueDate;
        this.otherDrugName = otherDrugName;
        this.otherDosageForm = otherDosageForm;
        this.sos = sos;
    }

    public TreatmentPlanProductMap(Long id, LocalDateTime createdDate, LocalDateTime startDate, LocalDateTime endDate, Long treatmentPlanId, Long productId, String schedule, LocalDateTime refillDueDate, String otherDrugName, String otherDosageForm, Boolean sos) {
        this.id = id;
        this.createdDate = createdDate;
        this.startDate = startDate;
        this.endDate = endDate;
        this.treatmentPlanId = treatmentPlanId;
        this.productId = productId;
        this.schedule = schedule;
        this.refillDueDate = refillDueDate;
        this.otherDrugName = otherDrugName;
        this.otherDosageForm = otherDosageForm;
        this.sos = sos;
    }
}

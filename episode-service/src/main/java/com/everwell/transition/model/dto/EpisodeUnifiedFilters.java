package com.everwell.transition.model.dto;

import com.everwell.transition.constants.UppFieldsConstants;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EpisodeUnifiedFilters {
    private List<String> types;

    private List<Long> diseaseId;

    private String diseaseIdOptions;

    private List<Integer> hierarchyIds;

    private int singleId;

    private int excludeUnassigned;

    private String pHIPatientType;

    private List<String> monitoringMethod;

    private String stage;

    private String dateStart;

    private String dateEnd;

    private String dateType;

    public void transformMonitoringMethod() {
        if (this.monitoringMethod == null) return;
        List<String> transformedList = new ArrayList<>();
        this.getMonitoringMethod().forEach(mm -> {
            if (mm.equalsIgnoreCase(UppFieldsConstants.MONITORING_METHOD_99DOTS_DISPLAY)) {
                transformedList.add(UppFieldsConstants.MONITORING_METHOD_99DOTS);
            } else if (mm.equalsIgnoreCase(UppFieldsConstants.MONITORING_METHOD_99DOTS_LITE_DISPLAY)) {
                transformedList.add(UppFieldsConstants.MONITORING_METHOD_99DOTS_LITE);
            } else
                transformedList.add(mm);
        });
        this.monitoringMethod = new ArrayList<>(transformedList);
    }

}
package com.everwell.transition.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ExtraDataForDay {
    String name;
    String iconName;
    String iconColor;
}
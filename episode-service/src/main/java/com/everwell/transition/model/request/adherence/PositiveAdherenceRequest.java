package com.everwell.transition.model.request.adherence;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class PositiveAdherenceRequest {
    private List<String> entityIdList;
}

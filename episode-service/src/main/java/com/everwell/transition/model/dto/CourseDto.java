package com.everwell.transition.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@NoArgsConstructor
@Getter
public class CourseDto {

    Long courseId;

    String language;

    String title;

    String url;

    Boolean isComplete;

    public CourseDto(Long courseId, String language, String title, String url, boolean isComplete) {
        this.courseId = courseId;
        this.language = language;
        this.title = title;
        this.url = url;
        this.isComplete = isComplete;
    }
}

package com.everwell.transition.model.db;

import com.everwell.transition.constants.Constants;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "disease_stage_key_mapping")
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class DiseaseStageKeyMapping {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "disease_stage_id")
    private Long diseaseStageId;

    @Column(name = "field_id")
    private Long fieldId;

    @Column(name = "required")
    private boolean required;

    @Column(name = "deleted")
    private boolean deleted;

    @Column(name = "default_value")
    private String defaultValue;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @CreationTimestamp
    @Column(name = "created_on")
    private LocalDateTime createdOn;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @UpdateTimestamp
    @Column(name = "updated_on")
    private LocalDateTime updatedOn;

    public DiseaseStageKeyMapping(Long id, Long diseaseStageId, Long fieldId, boolean required, boolean deleted, String defaultValue) {
        this.id = id;
        this.diseaseStageId = diseaseStageId;
        this.fieldId = fieldId;
        this.required = required;
        this.deleted = deleted;
        this.defaultValue = defaultValue;
    }
}

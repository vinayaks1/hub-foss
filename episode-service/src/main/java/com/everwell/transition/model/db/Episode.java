package com.everwell.transition.model.db;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.*;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "episode")
@Getter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Episode {

    @Id
    @Setter
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "client_id")
    private Long clientId;

    @Setter
    @Column(name = "person_id")
    private Long personId;

    @Setter
    @Column(name = "deleted")
    private boolean deleted;

    @Setter
    @Column(name = "disease_id")
    private Long diseaseId;

    @Setter
    @Column(name = "disease_id_options")
    private String diseaseIdOptions;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @Column(name = "created_date")
    private LocalDateTime createdDate;

    @Setter
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @Column(name = "start_date")
    private LocalDateTime startDate;

    @Setter
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @Column(name = "end_date")
    private LocalDateTime endDate;

    @Setter
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @Column(name = "last_activity_date")
    private LocalDateTime lastActivityDate;

    @Setter
    @Column(name = "risk_status")
    private String riskStatus;

    @Setter
    @Column(name = "current_stage_id")
    private Long currentStageId;

    @Setter
    @Column(name = "type_of_episode")
    private String typeOfEpisode;

    @Column(name = "added_by")
    private Long addedBy;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @UpdateTimestamp
    @Column(name = "updated_on")
    private LocalDateTime updatedOn;

    public Episode(Long clientId, Long personId, String diseaseIdOptions, String typeOfEpisode, LocalDateTime startDate, LocalDateTime endDate, Long addedBy) {
        this(null, clientId, personId, diseaseIdOptions, typeOfEpisode, startDate, endDate, addedBy);
    }

    public Episode(Long id, Long clientId, Long personId, String diseaseIdOptions, String typeOfEpisode, LocalDateTime startDate, LocalDateTime endDate, Long addedBy) {
        this.id = id;
        this.clientId = clientId;
        this.personId = personId;
        this.diseaseIdOptions = diseaseIdOptions;
        this.deleted = false;
        this.createdDate = Utils.getCurrentDateNew();
        this.lastActivityDate = Utils.getCurrentDateNew();
        this.typeOfEpisode = typeOfEpisode;
        this.addedBy = addedBy;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Episode(Long id, Long clientId, Long personId, boolean deleted, Long diseaseId, String diseaseIdOptions, LocalDateTime createdDate, LocalDateTime startDate, LocalDateTime endDate, LocalDateTime lastActivityDate, String riskStatus, Long currentStageId, String typeOfEpisode, Long addedBy) {
        this.id = id;
        this.clientId = clientId;
        this.personId = personId;
        this.deleted = deleted;
        this.diseaseId = diseaseId;
        this.diseaseIdOptions = diseaseIdOptions;
        this.createdDate = createdDate;
        this.startDate = startDate;
        this.endDate = endDate;
        this.lastActivityDate = lastActivityDate;
        this.riskStatus = riskStatus;
        this.currentStageId = currentStageId;
        this.typeOfEpisode = typeOfEpisode;
        this.addedBy = addedBy;
    }
}

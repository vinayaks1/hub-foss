package com.everwell.transition.model.dto;

import com.everwell.transition.model.db.EpisodeDocument;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class EpisodeDocumentResponseDto {

    Long episodeId;
    List<EpisodeDocument> episodeDocumentList;
}

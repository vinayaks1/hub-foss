package com.everwell.transition.model.dto.appointment;

import com.everwell.transition.constants.AppointmentConstants;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.request.appointment.AppointmentRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppointmentData {
    private Long staffId;
    private String addedBy;
    private AppointmentRequest appointment;
    private List<QuestionAnswers> questionnaire;
    private List<Document> documents;

    public void isValid() {
        if (appointment.getEpisodeId() == null) {
            throw new ValidationException(AppointmentConstants.MISSING_EPISODE_ID);
        }
        if (staffId == null) {
            throw new ValidationException(AppointmentConstants.MISSING_STAFF_ID);
        }
    }
}

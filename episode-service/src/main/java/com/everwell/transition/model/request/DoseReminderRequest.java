package com.everwell.transition.model.request;

import com.everwell.transition.model.dto.notification.NotificationTriggerDto;
import com.everwell.transition.model.request.episodesearch.EpisodeSearchRequest;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class DoseReminderRequest {
    EpisodeSearchRequest episodeSearchRequest;
    String deploymentCode;
    String phonePrefix;
    Long clientId;
    String phoneNumberValidationRegex;
    long days;
    boolean excludingTodayDose;
    NotificationTriggerDto notificationTriggerDto;
    String eventName;
}
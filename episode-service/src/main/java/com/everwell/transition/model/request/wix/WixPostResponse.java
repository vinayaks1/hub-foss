package com.everwell.transition.model.request.wix;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WixPostResponse {
    String imageUrl;
    String title;
    String category;
    String postUrl;
    String language;
}

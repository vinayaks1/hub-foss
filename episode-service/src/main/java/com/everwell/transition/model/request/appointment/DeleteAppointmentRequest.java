package com.everwell.transition.model.request.appointment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DeleteAppointmentRequest {
    private Long deletedById;
    private Integer appointmentId;
}


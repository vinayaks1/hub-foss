package com.everwell.transition.model.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NotNull
@NotBlank
@NotEmpty
public class ABDMCareContextLinkRequest {

    private long episodeId;
    private long personId;

    private String hiTypes;
    private String personName;
    private String enrollmentDate;
    private String identifierType;
    private String identifierValue;
    private char gender;
    private String abhaAddress;
    private String yearOfBirth;
}

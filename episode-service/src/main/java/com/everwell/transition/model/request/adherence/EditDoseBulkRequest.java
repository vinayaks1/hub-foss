package com.everwell.transition.model.request.adherence;

import com.everwell.transition.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EditDoseBulkRequest {
    List<EditDosesRequest> editDosesRequestList;

    public void validate () {
        if (CollectionUtils.isEmpty(editDosesRequestList))
            throw new ValidationException("request list cannot be empty!");
    }
}

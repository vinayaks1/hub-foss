package com.everwell.transition.model.db;

import com.everwell.transition.constants.Constants;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "episode_log")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EpisodeLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "episode_id")
    Long episodeId;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @Column(name = "added_on")
    LocalDateTime addedOn;

    @Column(name = "added_by")
    Long addedBy;

    @Column(name = "category",columnDefinition = "text")
    String category;

    @Column(name = "sub_category",columnDefinition = "text")
    String subCategory;

    @Column(name = "action_taken",columnDefinition = "text")
    String actionTaken;

    @Column(name = "comments",columnDefinition = "text")
    String comments;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @Column(name = "date_of_action")
    LocalDateTime dateOfAction;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @UpdateTimestamp
    @Column(name = "updated_on")
    private LocalDateTime updatedOn;

    public EpisodeLog(Long episodeId, Long addedBy, String category, String actionTaken, String comments)
    {
        this.episodeId = episodeId;
        this.addedBy = addedBy;
        this.category = category;
        this.comments = comments;
        this.actionTaken = actionTaken;
        this.addedOn = LocalDateTime.now();
    }

    public EpisodeLog(Long episodeId, Long addedBy, String category, String subCategory, String actionTaken, String comments, LocalDateTime dateOfAction)
    {
        this.episodeId = episodeId;
        this.addedBy = addedBy;
        this.category = category;
        this.subCategory = subCategory;
        this.comments = comments;
        this.actionTaken = actionTaken;
        this.addedOn = LocalDateTime.now();
        this.dateOfAction = dateOfAction;
    }

    public EpisodeLog(Long id, Long episodeId, LocalDateTime addedOn, Long addedBy, String category, String subCategory, String actionTaken, String comments, LocalDateTime dateOfAction) {
        this.id = id;
        this.episodeId = episodeId;
        this.addedOn = addedOn;
        this.addedBy = addedBy;
        this.category = category;
        this.subCategory = subCategory;
        this.actionTaken = actionTaken;
        this.comments = comments;
        this.dateOfAction = dateOfAction;
    }
}

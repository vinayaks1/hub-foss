package com.everwell.transition.model.dto.adherence;


import com.everwell.transition.enums.AdherenceCodeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DosageCountsDetailed {
    long enrollment;
    long received;
    long manual;
    long receivedUnsure;
    long missed;
    long ended;
    long noInfo;
    long repeatReceived;
    long repeatReceivedUnsure;
    long receivedUnscheduled;
    long unValidated;

    public DosageCountsDetailed(String adherenceString) {
        if (StringUtils.hasText(adherenceString)) {
            for (char c : adherenceString.toCharArray()) {
                this.enrollment += AdherenceCodeEnum.ENROLLMENT.getAdherenceCode() == c ? 1 : 0;
                this.received += AdherenceCodeEnum.RECEIVED.getAdherenceCode() == c ? 1 : 0;
                this.manual += (AdherenceCodeEnum.MANUAL.getAdherenceCode() == c || AdherenceCodeEnum.PATIENT_MANUAL.getAdherenceCode() == c) ? 1 : 0;
                this.receivedUnsure += AdherenceCodeEnum.RECEIVED_UNSURE.getAdherenceCode() == c ? 1 : 0;
                this.missed += (AdherenceCodeEnum.MISSED.getAdherenceCode() == c || AdherenceCodeEnum.PATIENT_MISSED.getAdherenceCode() == c) ? 1 : 0;
                this.ended += AdherenceCodeEnum.ENDED.getAdherenceCode() == c ? 1 : 0;
                this.noInfo += AdherenceCodeEnum.NO_INFO.getAdherenceCode() == c ? 1 : 0;
                this.receivedUnscheduled += AdherenceCodeEnum.RECEIVED_UNSCHEDULED.getAdherenceCode() == c ? 1 : 0;
                this.repeatReceived += AdherenceCodeEnum.TFN_REPEAT_RECEIVED.getAdherenceCode() == c ? 1 : 0;
                this.repeatReceivedUnsure += AdherenceCodeEnum.TFN_REPEAT_RECEIVED_UNSURE.getAdherenceCode() == c ? 1 : 0;
                this.unValidated += AdherenceCodeEnum.UNVALIDATED.getAdherenceCode() == c ? 1 : 0;
            }
        }
    }
}

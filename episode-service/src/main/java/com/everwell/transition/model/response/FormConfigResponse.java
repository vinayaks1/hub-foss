package com.everwell.transition.model.response;

import com.everwell.transition.model.dto.FormConfigDto;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

@ToString
@Getter
public class FormConfigResponse {
    Long adverseEventId;
    String onsetDate;
    String outcomeDate;
    List<FormConfigDto> formConfigs;


    public  FormConfigResponse(Long adverseEventId, List<FormConfigDto> formConfigs, String onsetDate, String outcomeDate) {
        this.adverseEventId = adverseEventId;
        this.formConfigs = formConfigs;
        this.onsetDate = onsetDate;
        this.outcomeDate = outcomeDate;
    }
}

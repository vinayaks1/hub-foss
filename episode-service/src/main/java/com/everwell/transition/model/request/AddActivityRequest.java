package com.everwell.transition.model.request;

import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.dto.ActivityDto;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.util.StringUtils;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class AddActivityRequest {
    Long episodeId;
    ActivityDto activity;

    public void validate() {
        if (episodeId == null)
            throw new ValidationException("Episode Id cannot be empty!");
    }
}

package com.everwell.transition.model.response.android;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TranslationCacheObject {
    String key;
    String value;
}

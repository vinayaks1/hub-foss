package com.everwell.transition.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class FieldDataDto {

    private String key;

    private String value;

    private Boolean isDefault;

    private Long episodeStageId;

    private String module;
}

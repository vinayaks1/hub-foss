package com.everwell.transition.model.response;

import com.everwell.transition.model.db.Client;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ClientResponse {
    Long id;
    String name;
    String authToken;
    Long nextRefresh;

    public ClientResponse(Client client, String authToken, Long nextRefresh) {
        this.id = client.getId();
        this.name = client.getName();
        this.authToken = authToken;
        this.nextRefresh = nextRefresh;
    }

    public ClientResponse(Client client) {
        this.id = client.getId();
        this.name = client.getName();
    }

}
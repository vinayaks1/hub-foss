package com.everwell.transition.model.response.adherence;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class AllAvgAdherenceResponse {
    private double digitalAdherence;
    private double totalAdherence;
    private Map<Object, Object> averageAdherenceIntervals;
    private List<Map<String, Object>> entityAverageAdherenceList;

    public AllAvgAdherenceResponse(double digitalAdherence, double totalAdherence) {
        this.digitalAdherence = digitalAdherence;
        this.totalAdherence = totalAdherence;
    }
}

package com.everwell.transition.model.db;

import com.everwell.transition.constants.Constants;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "episode_stage_data")
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class EpisodeStageData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "episode_stage_id")
    private Long episodeStageId;

    @Column(name = "field_id")
    private Long fieldId;

    @Setter
    @Column(name = "value")
    private String value;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @CreationTimestamp
    @Column(name = "created_on")
    private LocalDateTime createdOn;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @UpdateTimestamp
    @Column(name = "updated_on")
    private LocalDateTime updatedOn;

    public EpisodeStageData(Long episodeStageId, Long fieldId, String value) {
        this.episodeStageId = episodeStageId;
        this.fieldId = fieldId;
        this.value = value;
    }

    public EpisodeStageData(Long id, Long episodeStageId, Long fieldId, String value) {
        this.id = id;
        this.episodeStageId = episodeStageId;
        this.fieldId = fieldId;
        this.value = value;
    }
}

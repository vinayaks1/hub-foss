package com.everwell.transition.constants;

public class QueueConstants {
    public static final String REFRESH_TAGS_QUEUE = "q.episode.refreshTagForAllEpisodes";
    public static final String EPISODE_DOSE_REMINDER = "q.episode.episodeDoseReminder";
}

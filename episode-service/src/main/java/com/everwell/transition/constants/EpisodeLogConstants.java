package com.everwell.transition.constants;

public class EpisodeLogConstants {

    public static final String ENGAGEMENT_UPDATE_CATEGORY = "Patient_Engagement_Changed";
    public static final String CHANGED_PATIENT_ENGAGEMENT = "Changed Patient Engagement";
    public static final String ADDED_PATIENT_ENGAGEMENT = "Added Patient Engagement";
    public static final String CHANGED_PATIENT_CONSENT = "Changed the consent of patient to ";
    public static final String ADDED_PATIENT_CONSENT = "Added Patient consent as ";

    public static final String EPISODE_END_DATE_UPDATE_COMMENT = "Patient's End date changed from %s to %s by UserName : %s, Designation : %s, Reason : %s, Comment : %s";
    public static final String EPISODE_DISPENSATION_ADD_COMMENT = "Patient End Date extended because dispensation with Id : %s was added by the user with Id : %s";
}

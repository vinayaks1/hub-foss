package com.everwell.transition.constants;

public class UppFieldsConstants {

    public static final String MONITORING_METHOD_99DOTS_DISPLAY = "Envelopes - 99DOTS";

    public static final String MONITORING_METHOD_99DOTS_LITE_DISPLAY = "Stickers/Labels - 99DOTS Lite";

    public static final String MONITORING_METHOD_99DOTS = "99DOTS";

    public static final String MONITORING_METHOD_99DOTS_LITE = "99DOTSLite";

    public static final String TYPE_OF_CASE_PMDT = "PMDT";

    public static final String FIELD_NOTIFICATION_DATE = "notificationDate";

    public static final String FIELD_REGISTRATION_DATE = "registrationDate";
}

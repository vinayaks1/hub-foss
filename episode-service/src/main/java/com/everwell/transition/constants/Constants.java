package com.everwell.transition.constants;


import com.everwell.transition.utils.Utils;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Constants {
    public static final String X_CLIENT_ID = "X-Client-Id";
    public static final String[] AUTH_WHITELIST = {
            "/v1/client",
            "/actuator/**",
            "/v2/api-docs",
            "/configuration/ui",
            "/swagger-resources/**",
            "/configuration/security",
            "/swagger-ui.html",
            "/webjars/**"
    };
    public static final String MALFORMED_JWT = "JWT Token Malformed";
    public static final String DEFAULT = "DEFAULT";
    public static final String PATIENT = "Patient";
    public static final String ADHERENCE_CHANGES_LOG = "Adherence changes";
    public static final String DOSAGE_CHANGES_LOG = "Dosage level changes";
    public static final String SUPPORT_ACTION_LOGS = "Support Actions";
    public static final Integer MAX_BULK_ADHERENCE_SIZE = 3000;
    public static final Integer ALTER_DOSES_LIMIT = 60;
    public static final String ACTION_REMOVE = "REMOVE";
    public static final String ACTION_ADD = "ADD";
    public static final String FATAL = "Fatal";
    public static final String RECOVERED = "Recovered / Resolved";
    public static final String RECOVERED_RESOLVED_WITH_SEQUEL = "Recovered / Resolved with sequel";

    public static final String HI_TYPE_PRESCRIPTION = "Prescription";
    public static final String HI_TYPE_DIAGNOSTIC_REPORT = "DiagnosticReport";
    public static final String ROUTING_KEY = "ROUTING_KEY";
    public static final String IDENTIFIER_MOBILE = "MOBILE";
    public static final String ABDM_HIP_ID = "NIKSHAY-HIP";
    public static final String ABDM_HIP_NAME = "NIKSHAY";
    public static final String TIMESTAMP_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS";
    public static final String X_PLATFORM_ID = "X-Platform-Id";
    public static final String CLIENT_ID = "clientId";
    public static final String MODULE = "module";
    public static final String MY_MODULE = "episode";
    public static final String EVENT_CLIENT_ID = "CLIENT_ID";
    public static final String PRIVATE_EPISODE_TYPE = "IndiaTbPrivate";
    public static final String PUBLIC_EPISODE_TYPE = "IndiaTbPublic";
    public static final String PUSH_NOTIFICATION = "Push Notification";
    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_TIME_FORMAT = "d-M-yyyy HH:mm:ss";
    public static final String HIGH = "HIGH";
    public static final String LOW = "LOW";
    public static final String LFU_RISK_LOG_CATEGORY = "LFU_Risk_Stratification";
    public static final String LFU_RISK_LOG_ACTION = "High LFU Risk at enrollment";
    public static final String LFU_RISK_LOG_COMMENT = "Patient identified as high risk for lost to follow up at treatment";
    public static final String DATE_FORMAT_WITHOUT_TIME = "yyyy-MM-dd";
    public static final String IST_TIMEZONE = "Asia/Kolkata";
    public static final LocalDateTime DISPENSATION_MODULE_ACTUAL_START_DATE = Utils.convertStringToDateNew("2021-06-17 00:00:00");
    public static final String PRIMARY = "primary";
    public static final String NUMBER = "number";
    public static final String CATEGORY_EndDate_Updated = "EndDate_Updated";
    public static final String USERNAME_NIKSHAY = "NIKSHAY";
    public static final String DESIGNATION_ADMIN = "Admin";
    public static final String ACTION_END_DATE_EXTENDED = "End date Extended";
    public static final String DEFAULT_AGGREGATED_SCHEDULE = "****";
    public static final char EPISODE_SCHEDULE_TIME_TRUE_BIT = '1';
    public static final char EPISODE_SCHEDULE_TIME_FALSE_BIT = '*';
    public static final char PRODUCT_SCHEDULE_TIME_NONE_BIT = '*';

    public static final String HEALTH_ADDRESS = "HEALTH_ADDRESS";
    public static final Long DEFAULT_ADDED_BY_ID = -1L;
    public static final String IAM_ACCESS_MAPPING_ENTITY_PREFIX_MED = "med";
    public static final String AUTHORIZATION = "Authorization";
    public static final String BLACKLIST_TOKEN  = "bltoken:";
    public static final Set<String> MODULE_NAMES =  Collections.unmodifiableSet(new HashSet<>(Arrays.asList(UserServiceConstants.PERSON_MODULE_NAME, IAMConstants.IAM_MODULE_NAME)));
    public static final String EMPTY_STRING = "";
    public static final String CONNECTION = "Connection";
    public static final String KEEP_ALIVE = "keep-alive";

    public static final String TRANSLATION_CACHE_KEY_PREFIX = "tln:";

    public static final String BODY = "body";
    public static final String SUBJECT = "subject";
    public static final String EVERWELL_DEVELOPERS_MAIL = "developers@everwell.org";
    public static final String DATA = "data";
    public static final String DIAGNOSIS = "Diagnosis";
    public static final String YES = "Yes";
    public static final String RETREATMENT_OTHERS = "Retreatment: Others";
    public static final String NEW = "New";
    public static final String UNKNOWN = "Unknown";

}

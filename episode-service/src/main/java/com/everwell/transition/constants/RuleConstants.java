package com.everwell.transition.constants;

public class RuleConstants {
    public static final String CHECK_DUPLICATES_AND_DELETE = "checkDuplicatesAndDelete";

    public static final String CHECK_DUPLICATES_AND_UPDATE = "checkDuplicatesAndUpdate";

    public static final String NOTIFIED_STAGE_ID_LIST = "notifiedStageIdList";

    public static final String DUPLICATION_SCHEME = "duplicationScheme";

    public static final String EPISODE_SSO_REGISTRATION = "ssoRegistration";

    public static final String ACTION_ON_IAM = "actionOnIAM";

    public static final String IAM_REGISTER_OR_UPDATE = "registerOrUpdate";

    public static final String IAM_CLOSED= "close";

    public static final String IAM_REOPEN = "reopen";
}

package com.everwell.transition.constants;

import com.everwell.transition.enums.adherence.AdherenceMonitoringMethod;

import java.util.*;

public class IAMConstants {
    public static final String NONE = "NONE";
    public static final String DAILY = "Daily";

    public static final String MULTI_FREQUENCY = "Multi Frequency";
    public static final Long CLIENT_ID = 171L;
    public static final Long SCHEDULE_TYPE_ID = 3L;
    public static final Long SCHEDULE_TYPE_ID_DAILY = 1L;
    public static final Long FIRST_DOSE_OFFSET = 0L;
    public static final Long DEFAULT_POSITIVE_SENSITIVITY = 0L;
    public static final Long DEFAULT_NEGATIVE_SENSITIVITY = 0L;

    public static final List<String> DEFAULT_DOSE_LIST = Arrays.asList("03:30:00", "07:30:00", "11:30:00", "15:30:00");

    //batch number is derived from extensive testing, please test any new number before changing
    public static final Integer BATCH_SIZE_ADHERENCE_BULK = 2500;

    public static final Integer BATCH_SIZE_GET_POSITIVE_COUNT = 10000;

    public static final Integer BATCH_SIZE_GET_AVERAGE_ADHERENCE = 1000;

    public static final String DAILY_SCHEDULE_STRING = "MTWHFSU";

    public static final String IAM_MODULE_NAME = "IAM";
    public static final String DEFAULT_SCHEDULE_TYPE = "DAILY";
    public static final Set<String> MONITORING_METHODS_NNDOTS =  Collections.unmodifiableSet(new HashSet<>(Arrays.asList(AdherenceMonitoringMethod.NNDOTS.getName(), AdherenceMonitoringMethod.NNDLITE.getName())));
}

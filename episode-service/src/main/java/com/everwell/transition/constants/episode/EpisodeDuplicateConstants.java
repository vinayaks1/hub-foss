package com.everwell.transition.constants.episode;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class EpisodeDuplicateConstants {
    public static final String SYSTEM_IDENTIFIED_UNIQUE = "SYSTEM_IDENTIFIED_UNIQUE";

    public static final String USER_CONFIRMED_UNIQUE = "USER_CONFIRMED_UNIQUE";

    public static final String SYSTEM_IDENTIFIED_DUPLICATE = "SYSTEM_IDENTIFIED_DUPLICATE";

    public static final String USER_CONFIRMED_DUPLICATE = "USER_CONFIRMED_DUPLICATE";

    public static final String NULL = null;

    public static final Set<String> UNIQUE_STATUS_WITH_NULL =  Collections.unmodifiableSet(new HashSet<>(Arrays.asList(SYSTEM_IDENTIFIED_UNIQUE, USER_CONFIRMED_UNIQUE, NULL)));

    public static final Set<String> ALLOWED_UNIQUE_STATUSES = Collections.unmodifiableSet(new HashSet<>(Arrays.asList(SYSTEM_IDENTIFIED_UNIQUE, USER_CONFIRMED_UNIQUE)));

    public static final Set<String> VALID_STATUSES = Collections.unmodifiableSet(new HashSet<>(Arrays.asList(SYSTEM_IDENTIFIED_UNIQUE,  USER_CONFIRMED_UNIQUE, SYSTEM_IDENTIFIED_DUPLICATE, USER_CONFIRMED_DUPLICATE)));
}

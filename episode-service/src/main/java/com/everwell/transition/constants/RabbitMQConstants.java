package com.everwell.transition.constants;

public class RabbitMQConstants {
    public static final String EPISODE_CLIENT_ADD_EPISODE = "q.episode.client.add_episode";
    public static final String EPISODE_UPDATE_TRACKER = "q.episode.update_tracker";
    public static final String REFRESH_TAG_FOR_ALL_EPISOES = "q.episode.refreshTagForAllEpisodes";
    public static final String EPISODE_DOSE_REMINDER = "q.episode.episodeDoseReminder";
}

package com.everwell.transition.constants;

public class DocumentConstants {
    public static final String DEFAULT_ENDPOINTS_PROTOCOL = "DefaultEndpointsProtocol=https;AccountName=";
    public static final String ACCOUNT_KEY = ";AccountKey=";
    public static final String ENDPOINT_SUFFIX = ";EndpointSuffix=core.windows.net";
    public static final Long UPLOAD_SIZE_50_MB = 50 * 1000 * 1000L;
}

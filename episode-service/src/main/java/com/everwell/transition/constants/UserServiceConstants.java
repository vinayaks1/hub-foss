package com.everwell.transition.constants;


public class UserServiceConstants {

    public static final String X_US_CLIENT_ID = "X-US-Client-Id";

    public static final String PERSON_MODULE_NAME = "Person";

    public static final String PERSON_RELATION_ACCEPT = "ACCEPT";
}

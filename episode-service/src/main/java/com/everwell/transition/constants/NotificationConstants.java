package com.everwell.transition.constants;

import java.util.Arrays;
import java.util.List;

public class NotificationConstants {
    public static final String TEMPLATE_OTP = "otp";
    public static final String TEMPLATE_USER = "user";
    public static final String REGISTRATION_EVENT = "sendRegistrationOtp";
    public static final String NAME_OF_APP = "nameofapp";
    public final static Long OTP_VALID_FOR_SECONDS = 300L;
    public static final String REGISTRATION_OTP_PREFIX = "registration_otp ";
    public static final String SEND_APPOINTMENT_CONFIRMATION_SMS = "sendAppointmentConfirmationSms";
    public static final String APPOINTMENT_CONFIRMATION_PN = "appointmentConfirmationPN";
    public static final String AARO_DOWNLOAD_LINK = "shorturl.at/clvGI";
    public static final String HERE = "here";
    public static final String EVENT_ADD_RELATION = "addUserRelationNotification";

    public static final String PARAM_NAME = "name";

    public static final String PARAM_PHONE = "phone";
    public static final String TEMPLATE_PREFIX = "prefix";
    public static final String NIKSHAY_PREFIX = "Nikshay: ";
    public static final String TEMPLATE_FISRTNAME = "firstName";
    public static final String TEMPLATE_ID = "id";
    public static final String TEMPLATE_NAME = "name";
    public static final String[] Key_Population_Not_applicable = {"Not Applicable"};
    public static final List<String> HIV_STATUS_FOR_TPT = Arrays.asList("Positive", "Reactive");
    public static final Long DSTB_IP = 54L;
    public static final Long DSTB_CP = 124L;
    public static List<Long> POST_TREATMENT_REMINDER_MONTH_LIST = Arrays.asList( 6 * 28L, 12 * 28L, 18 * 28L, 24 * 28L);
    public static final String TEMPLATE_DATE = "date";
    public static List<Long> BANK_DETAILS_REMINDER_DAY_LIST = Arrays.asList(15L, 30L);
    public static final String TESTED_FOR_TB_SUBREASON = "testedfortb";
    public static final String PARAM_COUNT_OR_MEDICINE = "countOrMedicineNames";
}


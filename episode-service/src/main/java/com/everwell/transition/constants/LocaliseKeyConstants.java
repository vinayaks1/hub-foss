package com.everwell.transition.constants;

import java.util.HashMap;
import java.util.Map;

public class LocaliseKeyConstants {
    // variable named as per their keys so that it's easy to identify which keys are being used

    public static Map<String, String> defaultTranslationMap = new HashMap<String, String>() {{
        put(trending, "Trending");
        put(explore_categories, "Explore Categories");
        put(bottom_bar_knowledge, "Fit Feed");
        put(discover_homepage_label, "Discover");
    }};

    public static final String trending = "trending";
    public static final String explore_categories = "explore_categories";
    public static final String bottom_bar_knowledge = "bottom_bar_knowledge";
    public static final String discover_homepage_label = "discover_homepage_label";

}

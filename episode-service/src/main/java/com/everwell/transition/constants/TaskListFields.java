package com.everwell.transition.constants;

public class TaskListFields {

    public static final String TASK_LIST_MONITORING_METHOD = "MonitoringMethod";

    public static final String TASK_LIST_TREATMENT_OUTCOME = "TreatmentOutcome";

    public static final String TASK_LIST_MAX_DAYS_SINCE_TREATMENT_START = "MaxDaysSinceTreatmentStart";

    public static final String TASK_LIST_MIN_DAYS_SINCE_TREATMENT_START = "MinDaysSinceTreatmentStart";

    public static final String TASK_LIST_REGISTRATION_DATE_RANGE = "RegistrationDateRange";

    public static final String TASK_LIST_TAG_LIST = "TagList";

    public static final String TASK_LIST_END_DATE_PASSED = "EndDatePassed";

    public static final String TASK_LIST_DOSE_NOT_REPORTED_TODAY = "DoseNotReportedToday";

    public static final String TASK_LIST_REFILL_DUE = "RefillDue";

    public static final String TASK_LIST_ALLOWED_RANGE_JSON = "AllowedRangeJsonString";

    public static final String TASK_LIST_FIELD_CONTACT_NUMBER = "contactNumber";

    public static final String TASK_LIST_FIELD_LAST_CALLED_ON = "lastCalledOn";

    public static final String TASK_LIST_FIELD_DIAGNOSIS_DATE = "dateOfDiagnosis";

    public static final String TASK_LIST_FIELD_ADHERENCE_TECH = "adherenceTechnology";

    public static final String TASK_LIST_FIELD_PATIENT_TYPE = "PatientType";

    public static final String TASK_LIST_POSITIVE_DOSING_DAYS = "PositiveDosingDays";
}

package com.everwell.transition.processor;

import com.everwell.transition.binders.PatientProcessorBinder;
import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.controller.EpisodeController;
import com.everwell.transition.exceptions.NotFoundException;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.db.Episode;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.model.dto.EventStreamingDto;
import com.everwell.transition.service.EpisodeService;
import com.everwell.transition.service.StageTransitionService;
import com.everwell.transition.utils.Utils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Headers;

import java.util.Map;
import java.util.logging.Logger;

@EnableBinding(PatientProcessorBinder.class)
public class StageTransitionProcessor {

    @Autowired
    PatientProcessorBinder patientProcessorBinder;

    @Autowired
    StageTransitionService stageTransitionService;

    @Autowired
    EpisodeService episodeService;

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(StageTransitionProcessor.class);

    @StreamListener(PatientProcessorBinder.STAGE_INPUT)
    public void stageTransitionHandler(@Header("X-Client-Id") Long clientId, Map<String, Object> input) {
        LOGGER.info("[stageTransitionHandler] event accepted for clientId : " + clientId + " and input : " + input);
        if (input != null && input.containsKey("field")) {
            try{
                Map<String, Object> fieldObject = (Map<String, Object>) input.get("field");
                Long entityId = NumberUtils.toLong(String.valueOf(fieldObject.get("entityId")));
                fieldObject.put(FieldConstants.EPISODE_FIELD_EPISODE_ID, entityId);
                episodeService.processEpisodeRequestInputForStageTransition(clientId, fieldObject);
            } catch (NotFoundException ex) {
                LOGGER.info(ex.getMessage());
            }
        }
    }

}

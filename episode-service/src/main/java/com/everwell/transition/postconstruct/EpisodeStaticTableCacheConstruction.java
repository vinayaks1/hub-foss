package com.everwell.transition.postconstruct;

import com.everwell.transition.model.db.*;
import com.everwell.transition.repositories.*;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;

@Component
public class EpisodeStaticTableCacheConstruction {
    @Autowired
    private DiseaseStageKeyMappingRepository diseaseStageKeyMappingRepository;

    @Autowired
    private DiseaseStageMappingRepository diseaseStageMappingRepository;

    @Autowired
    private TabPermissionRepository tabPermissionRepository;

    @Autowired
    private SupportedTabRepository supportedTabRepository;

    @Autowired
    private DiseaseTemplateRepository diseaseTemplateRepository;

    @Autowired
    private FieldRepository fieldRepository;

    @Getter
    Map<Long, Set<DiseaseStageMapping>> diseaseStageMappingMap = new HashMap<>();

    @Getter
    List<Field> fieldList = new ArrayList<>();

    @Getter
    List<DiseaseStageKeyMapping> diseaseStageKeyMappingList = new ArrayList<>();

    @Getter
    List<SupportedTab> supportedTabList = new ArrayList<>();

    @Getter
    List<TabPermission> tabPermissionList = new ArrayList<>();

    @Getter
    List<DiseaseTemplate> diseaseTemplateList = new ArrayList<>();

    @PostConstruct
    public void init () {
        setFields();
        setTabPermissions();
        setSupportedTabs();
        setDiseaseStageKeyMapping();
        setDiseaseStageMappingMap();
        setDiseaseTemplate();
    }

    private void setFields() {
        fieldList = new ArrayList<>(fieldRepository.findAll());
    }

    private void setTabPermissions() {
        tabPermissionList = new ArrayList<>(tabPermissionRepository.findAll());
    }

    private void setSupportedTabs() {
        supportedTabList = new ArrayList<>(supportedTabRepository.findAll());
    }

    private void setDiseaseStageKeyMapping() {
        diseaseStageKeyMappingList = new ArrayList<>(diseaseStageKeyMappingRepository.findAll());
    }

    private void setDiseaseStageMappingMap() {
        // static table, size ranges approx ~ 1-2K
        for (DiseaseStageMapping diseaseStageMapping : diseaseStageMappingRepository.findAll()) {
            diseaseStageMappingMap.putIfAbsent(diseaseStageMapping.getDiseaseTemplateId(), new HashSet<>());
            diseaseStageMappingMap.get(diseaseStageMapping.getDiseaseTemplateId()).add(diseaseStageMapping);
        }
    }

    private void setDiseaseTemplate() {
        diseaseTemplateList = new ArrayList<>(diseaseTemplateRepository.findAll());
    }

}

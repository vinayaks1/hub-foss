package com.everwell.transition.postconstruct;

import com.everwell.transition.model.db.CourseUrlMapping;
import com.everwell.transition.repositories.CourseUrlMappingRepository;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
public class CourseUrlMappingList {

    @Autowired
    private CourseUrlMappingRepository courseUrlMappingRepository;

    @Getter
    private List<CourseUrlMapping> courseUrlMappingList;

    @PostConstruct
    public void initCourseList() {
        courseUrlMappingList = courseUrlMappingRepository.findAll();
    }

}

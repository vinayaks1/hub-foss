package com.everwell.transition.postconstruct;

import com.everwell.transition.enums.iam.IAMValidation;
import com.everwell.transition.model.response.AdherenceCodeConfigResponse;
import com.everwell.transition.model.response.Response;
import com.everwell.transition.model.response.adherence.AdherenceGlobalConfigResponse;
import com.everwell.transition.model.response.adherence.GenericConfigResponse;
import com.everwell.transition.service.IAMService;
import com.everwell.transition.utils.Utils;
import io.sentry.Sentry;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.http.ResponseEntity;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Configuration
@DependsOn("DataGateway")
public class AdherenceGlobalConfigMap {
    @Autowired
    IAMService iamService;

    @Getter
    private Map<String, AdherenceCodeConfigResponse> codeToConfigMap = new HashMap<>();

    @Getter
    private Map<String, Integer> monitoringMethodToIdMap = new HashMap<>();

    @Getter
    private Map<String, Integer> scheduleTypeToIdMap = new HashMap<>();

    @PostConstruct
    public void createConfigMaps()
    {
        try {
            ResponseEntity<Response<AdherenceGlobalConfigResponse>> response = iamService.getAdherenceGlobalConfig(new HashMap<>());
            if (Utils.isResponseValid(response)) {
                AdherenceGlobalConfigResponse adherenceGlobalConfigResponse = response.getBody().getData();
                codeToConfigMap = adherenceGlobalConfigResponse.getAdherenceCodeConfigResponseList()
                        .stream()
                        .collect(Collectors.toMap(AdherenceCodeConfigResponse::getCodeName, i->i ));
                monitoringMethodToIdMap = adherenceGlobalConfigResponse.getAdherenceTechConfigResponseList()
                        .stream()
                        .collect(Collectors.toMap(i-> StringUtils.lowerCase(i.getName()), GenericConfigResponse::getId));
                scheduleTypeToIdMap = adherenceGlobalConfigResponse.getScheduleTypeConfigResponseList()
                        .stream()
                        .collect(Collectors.toMap(i-> StringUtils.lowerCase(i.getName()), GenericConfigResponse::getId));
            } else {
                Sentry.capture(IAMValidation.UNABLE_TO_CONNECT.getMessage());
            }
        } catch (Exception ex) {
            Sentry.capture(ex.getMessage());
        }
    }
}

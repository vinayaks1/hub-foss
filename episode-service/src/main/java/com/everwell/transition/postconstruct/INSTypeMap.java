package com.everwell.transition.postconstruct;

import com.everwell.transition.constants.INSConstants;
import com.everwell.transition.model.response.ins.TypeResponse;
import com.everwell.transition.service.INSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
public class INSTypeMap {

    @Autowired
    private INSService insService;

    public Map<String, Long> notificationTypeMap = new HashMap<>();

    @PostConstruct
    public void createNotificationTypeMap() {
        List<Map<String, Object>> insResponse = insService.getNotificationTypes().getTypeMappings();
        insResponse.forEach(i -> {
            String type = String.valueOf(i.get(INSConstants.FIELD_TYPE));
            Long typeId = Long.parseLong(String.valueOf(i.get(INSConstants.FIELD_TYPE_ID)));
            notificationTypeMap.put(type, typeId);
        });
    }
}

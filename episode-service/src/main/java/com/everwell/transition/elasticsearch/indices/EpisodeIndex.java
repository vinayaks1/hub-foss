package com.everwell.transition.elasticsearch.indices;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.elasticsearch.ElasticConstants;
import com.everwell.transition.model.db.Episode;
import com.everwell.transition.model.db.Stage;
import com.everwell.transition.model.dto.EpisodeDto;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.tomcat.util.bcel.Const;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(indexName = ElasticConstants.EPISODE_INDEX)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EpisodeIndex {

    @Id
    private String id;

    @Field(type = FieldType.Integer, name = "clientId")
    private Long clientId;

    @Field(type = FieldType.Integer, name = "personId")
    private Long personId;

    @Field(type = FieldType.Boolean, name = "deleted")
    private Boolean deleted;

    @Field(type = FieldType.Long, name = "diseaseId")
    private Long diseaseId;

    @Field(type = FieldType.Text, name = "diseaseIdOptions")
    private String diseaseIdOptions;

    @Field(type = FieldType.Date, name = "createdDate")
    private String createdDate;

    @Field(type = FieldType.Keyword, name = "riskStatus")
    private String riskStatus;

    @Field(type = FieldType.Integer, name = "currentStageId")
    private Long currentStageId;

    @Field(type = FieldType.Integer, name = "addedBy")
    private Long addedBy;

    @Field(type = FieldType.Keyword, name = "stageKey")
    private String stageKey;

    @Field(type = FieldType.Keyword, name = "stageName")
    private String stageName;

    @Field(type = FieldType.Keyword, name = "typeOfEpisode")
    private String typeOfEpisode;

    @Field(type = FieldType.Date, name = "endDate")
    private String endDate;

    @Field(type = FieldType.Date, name = "startDate")
    private String startDate;

    @Field(type = FieldType.Date, name = "lastActivityDate")
    private String lastActivityDate;

    @Field(type = FieldType.Object, name = "hierarchyMappings")
    Map<String, Object> hierarchyMappings;

    @Field(type = FieldType.Object, name = "associations")
    Map<String, Object> associations;

    @Field(type = FieldType.Object, name = "stageData")
    Map<String, Object> stageData;

    @Field(type = FieldType.Text, name = "currentTags")
    List<String> currentTags;

    public void clean () {
        this.hierarchyMappings = cleanUpMap(this.hierarchyMappings);
        this.stageData = cleanUpMap(this.stageData);
        this.associations = cleanUpMap(this.associations);
    }

    public Map<String, Object> cleanUpMap (Map<String, Object> map) {
        if (map == null) return null;
        Map<String, Object> h = new HashMap<>();
        map.forEach((k, v) -> {
            if (v != null) {
                h.put(k, v);
            }
        });
        return h;
    }

    public static EpisodeIndex convert (EpisodeDto episodeDto, Long clientId) {
        return new EpisodeIndex (
            String.valueOf(episodeDto.getId()),
            clientId, episodeDto.getPersonId(),
            false,
            episodeDto.getDiseaseId(),
            episodeDto.getDiseaseIdOptions(),
            Utils.getFormattedDateNewEmptyAsNull(episodeDto.getCreatedDate()),
            episodeDto.getRiskStatus(),
            episodeDto.getCurrentStageId(),
            episodeDto.getAddedBy(),
            episodeDto.getStageKey(),
            episodeDto.getStageName(),
            episodeDto.getTypeOfEpisode(),
            Utils.getFormattedDateNewEmptyAsNull(episodeDto.getEndDate()),
            Utils.getFormattedDateNewEmptyAsNull(episodeDto.getStartDate()),
            Utils.getFormattedDateNewEmptyAsNull(episodeDto.getLastActivityDate()),
            episodeDto.getHierarchyMappings(),
            episodeDto.getAssociations(),
            episodeDto.getStageData(),
            episodeDto.getCurrentTags()
        );
    }

    public static EpisodeIndex convert (Map<String, Object> valueMap, Long clientId) {
        return new EpisodeIndex (
            String.valueOf(valueMap.get(FieldConstants.EPISODE_FIELD_ID)),
            clientId,
            Long.valueOf(String.valueOf(valueMap.get(FieldConstants.EPISODE_FIELD_PERSON_ID))),
            false,
            valueMap.get(FieldConstants.EPISODE_FIELD_DISEASE_ID) != null ? Long.parseLong((String)valueMap.get(FieldConstants.EPISODE_FIELD_DISEASE_ID)) : null,
            (String) valueMap.getOrDefault(FieldConstants.EPISODE_FIELD_DISEASE_ID_OPTIONS, ""),
            formatStringToDateOrEmpty((String) valueMap.getOrDefault(FieldConstants.EPISODE_ADDED_TIMESTAMP, "")),
            Constants.LOW,
            null,
             Long.valueOf(String.valueOf(((Map<String, Object>) valueMap.get(ElasticConstants.STAGE_DATA_ELASTIC_NOMENCLATURE)).getOrDefault(FieldConstants.EPISODE_FIELD_ADDED_BY, "0"))),
            (String) valueMap.get(FieldConstants.EPISODE_FIELD_STAGE),
            null,
            (String) valueMap.getOrDefault(FieldConstants.CLIENT_TYPE_OF_PATIENT, ""),
            formatStringToDateOrEmpty((String) valueMap.getOrDefault(FieldConstants.EPISODE_FIELD_END_DATE, "")),
            formatStringToDateOrEmpty((String) valueMap.getOrDefault(FieldConstants.CLIENT_TB_TREATMENT_START_DATE_FIELD, "")),
            formatStringToDateOrEmpty((String) valueMap.getOrDefault(FieldConstants.CLIENT_LAST_ACTIVITY_DATE, "")),
            (Map<String, Object>) valueMap.getOrDefault(ElasticConstants.HIERARCHY_MAPPING_ELASTIC_NOMENCLATURE, new HashMap<>()),
            (Map<String, Object>) valueMap.getOrDefault(ElasticConstants.ASSOCIATIONS_ELASTIC_NOMENCLATURE, new HashMap<>()),
            (Map<String, Object>) valueMap.getOrDefault(ElasticConstants.STAGE_DATA_ELASTIC_NOMENCLATURE, new HashMap<>()),
            null
        );
    }

    public static String formatStringToDateOrEmpty(String badDate) {
        return Utils.getFormattedDateNewEmptyAsNull(Utils.toLocalDateTimeWithNull(badDate, Constants.DATE_FORMAT));
    }

    public EpisodeDto convertToDto() {
        return new EpisodeDto(
            new Episode(
                Long.valueOf(this.id),
                this.clientId,
                this.personId,
                false,
                this.diseaseId,
                this.diseaseIdOptions,
                Utils.toLocalDateTimeWithNull(this.createdDate, Constants.DATE_FORMAT),
                Utils.toLocalDateTimeWithNull(this.startDate, Constants.DATE_FORMAT),
                Utils.toLocalDateTimeWithNull(this.endDate, Constants.DATE_FORMAT),
                Utils.toLocalDateTimeWithNull(this.lastActivityDate, Constants.DATE_FORMAT),
                this.riskStatus,
                this.currentStageId,
                this.typeOfEpisode,
                this.addedBy
            ),
            currentTags,
            new Stage(this.currentStageId, this.stageKey, "", this.stageName),
            null,
            null,
            this.associations,
            this.hierarchyMappings,
            null,
            this.stageData
        );
    }

}

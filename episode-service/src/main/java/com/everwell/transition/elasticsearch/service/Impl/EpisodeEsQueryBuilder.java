package com.everwell.transition.elasticsearch.service.Impl;

import com.everwell.transition.constants.Constants;
import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.elasticsearch.ElasticConstants;
import com.everwell.transition.model.dto.AggregationDto;
import com.everwell.transition.model.request.episodesearch.EpisodeSearchRequest;
import com.everwell.transition.utils.Utils;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.aggregations.AbstractAggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogramInterval;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.*;

@Component
public class EpisodeEsQueryBuilder {

    private static final Logger LOGGER = LoggerFactory.getLogger(EpisodeEsQueryBuilder.class);

    private final List<String> matchQueryTerms = Arrays.asList(FieldConstants.EPISODE_FIELD_DISEASE_ID_OPTIONS);

    private final Set<String> prefixSearchQueryTerms = Collections.unmodifiableSet(new HashSet<>(Arrays.asList(FieldConstants.FIELD_NAME, FieldConstants.FIELD_STAGE_NAME)));

    public BoolQueryBuilder build(EpisodeSearchRequest searchReq, Map<String, String> fieldToTableMap) {
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
        if (searchReq.getSearch() != null) {
            EpisodeSearchRequest.SupportedSearchMethods search = searchReq.getSearch();
            if (search.getMustNot() != null) {
                filterQuery(queryBuilder, search.getMustNot(), fieldToTableMap, ElasticConstants.ELASTIC_SEARCH_KEYWORD_MUST_NOT, searchReq.getKeywordFields());
            }
            if (search.getMust() != null) {
                filterQuery(queryBuilder, search.getMust(), fieldToTableMap, ElasticConstants.ELASTIC_SEARCH_KEYWORD_MUST, searchReq.getKeywordFields());
            }
            if (search.getContains() != null) {
                filterQuery(queryBuilder, search.getContains(), fieldToTableMap, ElasticConstants.ELASTIC_SEARCH_KEYWORD_CONTAINS, searchReq.getKeywordFields());
            }
        }
        if (searchReq.getRange() != null)
            rangeQuery(queryBuilder, searchReq.getRange(), fieldToTableMap);
        return queryBuilder;
    }
    public List<AbstractAggregationBuilder<?>> buildAggregation(EpisodeSearchRequest.SupportedAggMethods aggMethods) {
        List<AbstractAggregationBuilder<?>> aggregationBuilderList = new ArrayList<>();
        List<AggregationDto> termsAggregations = aggMethods.getTerms();
        if (!CollectionUtils.isEmpty(termsAggregations)) {
            aggregationBuilderList.addAll(buildTermsAggregation(termsAggregations));
        }

        List<AggregationDto> dateAggregations = aggMethods.getDate();
        if (!CollectionUtils.isEmpty(dateAggregations)) {
            aggregationBuilderList.addAll(buildDateAggregation(dateAggregations));
        }
        return aggregationBuilderList;
    }

    public List<AbstractAggregationBuilder<?>> buildTermsAggregation(List<AggregationDto> aggregationDtoList) {
        List<AbstractAggregationBuilder<?>> aggregationBuilderList = new ArrayList<>();
        aggregationDtoList.forEach(aggregationDto -> aggregationBuilderList.add(AggregationBuilders
                .terms(aggregationDto.getKey())
                .field(aggregationDto.getFieldName())
                .size(aggregationDto.getSize())));
       return aggregationBuilderList;
    }

    public List<AbstractAggregationBuilder<?>> buildDateAggregation(List<AggregationDto> aggregationDtoList) {
        List<AbstractAggregationBuilder<?>> aggregationBuilderList = new ArrayList<>();
        aggregationDtoList.forEach(aggregationDto -> aggregationBuilderList.add(AggregationBuilders
            .dateHistogram(aggregationDto.getKey())
            .field(aggregationDto.getFieldName())
            .calendarInterval(DateHistogramInterval.DAY)
            .format(Constants.DATE_FORMAT_WITHOUT_TIME)
        ));
        return aggregationBuilderList;
    }

    public void filterQuery (BoolQueryBuilder boolQuery, Map<String, Object> searchReq, Map<String, String> fieldToTableMap, String type, List<String> keywordFields) {
        searchReq.forEach((key, val) -> {
            if (!Utils.isEmpty(val)) {
                String searchKey = convertFieldNames(fieldToTableMap, key, keywordFields);
                AbstractQueryBuilder query = getQuery(key, val, searchKey, type);
                if (type.equalsIgnoreCase(ElasticConstants.ELASTIC_SEARCH_KEYWORD_MUST) || type.equals(ElasticConstants.ELASTIC_SEARCH_KEYWORD_CONTAINS))
                    boolQuery.filter(query);
                else if (type.equalsIgnoreCase(ElasticConstants.ELASTIC_SEARCH_KEYWORD_MUST_NOT))
                    boolQuery.mustNot(query);
            }
        });
    }

    AbstractQueryBuilder getQuery (String key, Object value, String searchKey, String type) {
        if(type.equals(ElasticConstants.ELASTIC_SEARCH_KEYWORD_CONTAINS)) {
            return QueryBuilders.wildcardQuery(searchKey, ElasticConstants.ASTERISK + value + ElasticConstants.ASTERISK);
        }
        else if (prefixSearchQueryTerms.contains(key)) {
            return QueryBuilders.matchPhrasePrefixQuery(searchKey, value);
        } else if (matchQueryTerms.contains(key)) {
            return QueryBuilders.matchQuery(searchKey, value);
        } else if (value instanceof Collection) {
            return QueryBuilders.termsQuery(searchKey, (List<String>) value);
        } else if (value.equals("NULL")) {
            return QueryBuilders.existsQuery(searchKey);
        } else {
            return QueryBuilders.termQuery(searchKey, value);
        }
    }

    public String convertFieldNames(Map<String, String> fieldToTableMap, String key, List<String> keywordFields) {
        String searchKey = key;
        if (fieldToTableMap.containsKey(key) && !ElasticConstants.ELASTIC_NO_PREFIX_FIELDS.contains(key)) {
            searchKey = fieldToTableMap.get(key) + "." + searchKey;
        }
        if (keywordFields.contains(key)) {
            searchKey = searchKey + "." + ElasticConstants.ELASTIC_SEARCH_KEYWORD_KEYWORD;
        }
        return searchKey;
    }

    public void rangeQuery (BoolQueryBuilder boolQuery, Map<String, Object> rangeReq, Map<String, String> fieldToTableMap) {
        rangeReq.forEach((key, val) -> {
            String searchKey = fieldToTableMap.containsKey(key) ? fieldToTableMap.get(key) + "." + key : key;
            RangeQueryBuilder rangeQueryBuilder = QueryBuilders.rangeQuery(searchKey);
            Map<String, String> rangeVals = (Map<String, String>) val;
            if (rangeVals.containsKey(ElasticConstants.ELASTIC_SEARCH_RANGE_LOWER_THAN_EQUAL))
                rangeQueryBuilder.lte(rangeVals.get(ElasticConstants.ELASTIC_SEARCH_RANGE_LOWER_THAN_EQUAL));
            if (rangeVals.containsKey(ElasticConstants.ELASTIC_SEARCH_RANGE_GREATER_THAN_EQUAL))
                rangeQueryBuilder.gte(rangeVals.get(ElasticConstants.ELASTIC_SEARCH_RANGE_GREATER_THAN_EQUAL));
            if (rangeVals.containsKey(ElasticConstants.ELASTIC_SEARCH_RANGE_LOWER_THAN))
                rangeQueryBuilder.lt(rangeVals.get(ElasticConstants.ELASTIC_SEARCH_RANGE_LOWER_THAN));
            if (rangeVals.containsKey(ElasticConstants.ELASTIC_SEARCH_RANGE_GREATER_THAN))
                rangeQueryBuilder.gt(rangeVals.get(ElasticConstants.ELASTIC_SEARCH_RANGE_GREATER_THAN));
            boolQuery.filter(rangeQueryBuilder);
        });
    }

}

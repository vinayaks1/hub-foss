package com.everwell.transition.elasticsearch.service.Impl;

import com.everwell.transition.binders.EpisodeEsInsertUpdateBinder;
import com.everwell.transition.constants.Constants;
import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.constants.UserServiceConstants;
import com.everwell.transition.model.dto.EpisodeEsInsertUpdateDto;
import com.everwell.transition.model.dto.EventStreamingDto;
import com.everwell.transition.service.ClientService;
import com.everwell.transition.service.EpisodeService;
import com.everwell.transition.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import io.sentry.Sentry;
import io.sentry.event.EventBuilder;
import io.sentry.event.interfaces.ExceptionInterface;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@EnableBinding(EpisodeEsInsertUpdateBinder.class)
public class EpisodeEsConsumers {

    @Autowired
    EpisodeEsInsertUpdateBinder episodeEsInsertUpdateBinder;

    @Autowired
    EpisodeService episodeService;

    @Autowired
    ClientService clientService;

    @StreamListener(EpisodeEsInsertUpdateBinder.EPISODE_ELASTIC_EXCHANGE)
    public void addUpdateElasticDocumentHandler(EventStreamingDto<EpisodeEsInsertUpdateDto> eventStreamingDto) {
        EpisodeEsInsertUpdateDto episodeEsInsertUpdateDto = eventStreamingDto.getField();
        Long episodeId = episodeEsInsertUpdateDto.getEpisodeId();
        Long clientId = episodeEsInsertUpdateDto.getClientId();
        setAuthentication(clientId);
        if (episodeEsInsertUpdateDto.isUpdate()) {
            episodeService.updateElasticDocument(episodeId, clientId, episodeEsInsertUpdateDto.isHierarchyDataUpdated());
        } else {
            episodeService.saveToElastic(episodeId, clientId);
        }
    }

   @RabbitListener(queues = "q.episode.client.add_episode", concurrency = "1")
    public void addClientEpisodeToElastic(Message message) throws IOException {
        try {
            EventStreamingDto<Map<String, Object>> episodeData = Utils.jsonToObject(new String(message.getBody()), new TypeReference<EventStreamingDto<Map<String, Object>>>() {});
            Long clientId = Long.parseLong((String) message.getMessageProperties().getHeaders().get(Constants.CLIENT_ID));
            setAuthentication(clientId);
            episodeService.convertAndSavetoElastic(episodeData.getField(), clientId);
        } catch (Exception e) {
            EventBuilder builder = new EventBuilder().withSentryInterface(new ExceptionInterface(e)).withExtra("Queue", message.getMessageProperties().getConsumerQueue()).withExtra("Message Payload", Utils.asJsonString(new String(message.getBody())));
            Sentry.capture(builder);
            throw new AmqpRejectAndDontRequeueException("Exception at consumer", e);
        }
    }

    @RabbitListener(queues = "q.episode.update_tracker", concurrency = "1")
    public void updateElasticDetails(Message message) {
        try {
            EventStreamingDto<Map<String, Object>> episodeData = Utils.jsonToObject(new String(message.getBody()), new TypeReference<EventStreamingDto<Map<String, Object>>>() {});
            Long clientId = Long.parseLong(String.valueOf(message.getMessageProperties().getHeaders().get(Constants.CLIENT_ID)));
            String moduleName = (String) episodeData.getField().get(Constants.MODULE);
            Map<String, Object> fieldsMap = ((Map<String, Object>) episodeData.getField().get("fields"));
            setAuthentication(clientId);
            handleTransformations(moduleName, fieldsMap);
            episodeService.updateClientEpisodeToElastic(fieldsMap, clientId, moduleName);
        } catch (Exception e) {
            EventBuilder builder = new EventBuilder().withSentryInterface(new ExceptionInterface(e)).withExtra("Queue", message.getMessageProperties().getConsumerQueue()).withExtra("Message Payload", Utils.asJsonString(new String(message.getBody())));
            Sentry.capture(builder);
            throw new AmqpRejectAndDontRequeueException("Exception at consumer", e);
        }
    }

    void handleTransformations(String moduleName, Map<String, Object> fieldsMap) {
        if (moduleName.equalsIgnoreCase(UserServiceConstants.PERSON_MODULE_NAME)) {
            if (fieldsMap.containsKey(FieldConstants.PERSON_MOBILE_LIST)) {
                List<Map<String, Object>> mobileList = (List<Map<String, Object>>) fieldsMap.get(FieldConstants.PERSON_MOBILE_LIST);
                mobileList.forEach(mobile -> {
                    boolean primary = (boolean) mobile.get(Constants.PRIMARY);
                    if (primary) {
                        fieldsMap.put(FieldConstants.PERSON_PRIMARY_PHONE, mobile.get(Constants.NUMBER));
                    }
                });
            }
            fieldsMap.put(FieldConstants.EPISODE_FIELD_PERSON_ID, fieldsMap.get(FieldConstants.EPISODE_FIELD_ID));
            fieldsMap.remove(FieldConstants.EPISODE_FIELD_ID);
        }
    }

    private void setAuthentication(Long clientId) {
        SecurityContext ctx = SecurityContextHolder.createEmptyContext();
        ctx.setAuthentication(new UsernamePasswordAuthenticationToken(clientService.getClient(clientId), null, Collections.emptyList()));
        SecurityContextHolder.setContext(ctx);
    }
}

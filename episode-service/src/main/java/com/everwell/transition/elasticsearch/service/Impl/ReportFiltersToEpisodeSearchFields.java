package com.everwell.transition.elasticsearch.service.Impl;

import com.everwell.transition.constants.FieldConstants;
import com.everwell.transition.elasticsearch.ElasticConstants;
import com.everwell.transition.enums.episode.EpisodeValidation;
import com.everwell.transition.exceptions.ValidationException;
import com.everwell.transition.model.dto.EpisodeReportsFilter;
import com.everwell.transition.model.dto.EpisodeReportsRequest;
import com.everwell.transition.model.request.episodesearch.EpisodeSearchRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

@Component
public class ReportFiltersToEpisodeSearchFields {

    public EpisodeSearchRequest convert(EpisodeReportsRequest episodeReportsRequest) {
        EpisodeSearchRequest searchRequest = episodeReportsRequest.getEpisodeSearchRequest();
        if (null == searchRequest) {
            searchRequest = new EpisodeSearchRequest();
            searchRequest.setSearch(new EpisodeSearchRequest.SupportedSearchMethods(new HashMap<>(), new HashMap<>(), new HashMap<>()));
        }
        EpisodeReportsFilter episodeReportsFilter = episodeReportsRequest.getEpisodeReportsFilter();
        if (StringUtils.hasText(episodeReportsFilter.getDateType())) {
            String dateType;
            if (FieldConstants.FIELD_REGISTRATION_DATE.equalsIgnoreCase(episodeReportsFilter.getDateType())) {
                dateType = FieldConstants.EPISODE_FIELD_CREATED_DATE;
            } else if (FieldConstants.TB_TREATMENT_START_DATE.equalsIgnoreCase(episodeReportsFilter.getDateType())) {
                dateType = FieldConstants.EPISODE_FIELD_START_DATE;
            } else {
                throw new ValidationException(String.format(EpisodeValidation.INVALID_EPISODE_REPORTS_FILTER.getMessage(), episodeReportsFilter.getDateType()));
            }
            Map<String, String> rangeQuery = new HashMap<>();
            rangeQuery.put(ElasticConstants.ELASTIC_SEARCH_RANGE_GREATER_THAN_EQUAL, episodeReportsFilter.getStartDate());
            rangeQuery.put(ElasticConstants.ELASTIC_SEARCH_RANGE_LOWER_THAN_EQUAL, episodeReportsFilter.getEndDate());
            if (null == searchRequest.getRange()) {
                searchRequest.setRange(new HashMap<>());
            }
            searchRequest.getRange().put(dateType, rangeQuery);
        }
        if (!CollectionUtils.isEmpty(episodeReportsFilter.getEpisodeIdList())) {
            searchRequest.getSearch().getMust().put(FieldConstants.EPISODE_FIELD_ID, episodeReportsFilter.getEpisodeIdList());
        }
        if (!CollectionUtils.isEmpty(episodeReportsFilter.getTypeOfEpisodeList())) {
            searchRequest.getSearch().getMust().put(FieldConstants.EPISODE_FIELD_TYPE_OF_EPISODE, episodeReportsFilter.getTypeOfEpisodeList());
        }
        if (StringUtils.hasText(episodeReportsFilter.getHierarchyToSearch())) {
            searchRequest.getSearch().getMust().put(episodeReportsFilter.getHierarchyToSearch(), episodeReportsFilter.getHierarchyIds());
        }
        if (!CollectionUtils.isEmpty(episodeReportsFilter.getStageList())) {
            searchRequest.getSearch().getMust().put(FieldConstants.EPISODE_FIELD_STAGE_KEY, episodeReportsFilter.getStageList());
        }
        return  searchRequest;
    }

}

package com.everwell.transition.elasticsearch.service.Impl;

import com.everwell.transition.binders.EpisodeEsInsertUpdateBinder;
import com.everwell.transition.constants.Constants;
import com.everwell.transition.elasticsearch.ElasticConstants;
import com.everwell.transition.elasticsearch.service.ESEpisodeService;
import com.everwell.transition.model.db.EpisodeTag;
import com.everwell.transition.model.dto.ElasticDocTagsWrapper;
import com.everwell.transition.model.dto.EpisodeEsInsertUpdateDto;
import com.everwell.transition.model.dto.EventStreamingDto;
import com.everwell.transition.service.EpisodeService;
import com.everwell.transition.utils.Utils;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@EnableBinding(EpisodeEsInsertUpdateBinder.class)
public class EpisodeEsEventListener {

    @Autowired
    EpisodeEsInsertUpdateBinder episodeEsInsertUpdateBinder;

    @Autowired
    ESEpisodeService esEpisodeService;

    @Async
    @EventListener
    public void addEditEvent(EpisodeEsInsertUpdateDto episodeEsInsertUpdateDto) {
        EventStreamingDto<EpisodeEsInsertUpdateDto> result = new EventStreamingDto<>("add-edit-episode-elastic-event", episodeEsInsertUpdateDto);
        episodeEsInsertUpdateBinder
                .eventOutput()
                .send(MessageBuilder.withPayload(Utils.asJsonString(result))
                .setHeader(Constants.CLIENT_ID, episodeEsInsertUpdateDto.getClientId())
                .build());
    }

    @Async
    @EventListener
    public void tagsListener(ElasticDocTagsWrapper elasticDocTagsWrapper) {
        BulkRequest bulkRequest = new BulkRequest();
        elasticDocTagsWrapper.getEpisodeTagResponse().forEach(tags -> {
            List<String> nonStickyTags = tags.getTags().stream().map(EpisodeTag::getTagName).collect(Collectors.toList());
            List<String> stickyTags = tags.getCurrentTags();
            Map<String, Object> updateElasticDocMap = new HashMap<>();
            updateElasticDocMap.put("tags", nonStickyTags);
            updateElasticDocMap.put("stickyTags", stickyTags);
            UpdateRequest updateRequest = new UpdateRequest(ElasticConstants.EPISODE_INDEX, String.valueOf(tags.getEpisodeId())).doc(updateElasticDocMap);
            bulkRequest.add(updateRequest);
        });
        esEpisodeService.updateBulkElastic(bulkRequest);
    }

}

# Keystore Creation

### Issue
The issue of certification expiry and renewing it after every few months was repetitive. 
So, a keystore file was created using the Root Certificate and Intermediate Certificate of "Let's Encrypt" and it would trust all the sites that are signed by Let's Encrypt and hence trusting smsgov site as well.

### Steps to create jks file


1. `wget https://letsencrypt.org/certs/isrgrootx1.pem`
2. `wget https://letsencrypt.org/certs/lets-encrypt-r3.pem`
3. `keytool -import -keystore letsencrypt.jks -alias CAroot -file isrgrootx1.pem -trustcacerts`
4. `keytool -import -keystore letsencrypt.jks -alias intermediate-r3 -file lets-encrypt-r3.pem -trustcacerts`
5. `openssl s_client -verify 100 -showcerts -connect smsgw.sms.gov.in:443 -CAfile  <(keytool -list -rfc -keystore letsencrypt.jks -storepass <keystore password>`


For more info: https://gitlab.com/everwell/platform/-/issues/7167
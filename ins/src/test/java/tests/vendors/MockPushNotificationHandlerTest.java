package tests.vendors;

import com.everwell.ins.models.db.PushNotificationTemplate;
import com.everwell.ins.models.dto.vendorConfigs.FirebaseConfigDto;
import com.everwell.ins.models.dto.vendorCreds.JSONFormatDto;
import com.everwell.ins.repositories.PushTemplateRepository;
import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.models.db.PushNotificationLogs;
import com.everwell.ins.models.dto.FirebaseResponseDto;
import com.everwell.ins.models.dto.PushNotificationTemplateDto;
import com.everwell.ins.models.http.requests.PushNotificationRequest;
import com.everwell.ins.services.VendorService;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class MockPushNotificationHandlerTest extends PushNotificationHandlerTest{

    @InjectMocks
    private MockPushNotificationHandler pushNotificationHandler;

    @Mock
    private FirebaseConfigDto config;

    @Mock
    private JSONFormatDto creds;

    @Mock
    private ApplicationEventPublisher applicationEventPublisher;

    @Mock
    protected PushTemplateRepository pushTemplateRepository;

    @Mock
    protected Optional<PushNotificationTemplate> optionalPushNotificationTemplate;

    @Mock
    protected PushNotificationTemplate pushNotificationTemplate;



    @Test
    public void testCreateLogs() {
        List<PushNotificationTemplateDto> pushNotificationDtos = getPushNotificationDtos();
        FirebaseResponseDto responseDto = new FirebaseResponseDto(testFirebaseResponse);
        ReflectionTestUtils.setField(pushNotificationHandler, "isMock", true);

        List<PushNotificationLogs> responsePushNotificationLogCollection = pushNotificationHandler.createLogs(pushNotificationDtos, responseDto, 1L, 1L, 1L);
        List<PushNotificationLogs> expectedPushNotificationLogCollection = getMockPushNotificationLogs();
        for (int i = 0; i < 8; i++) {
            assertEquals(responsePushNotificationLogCollection.get(i).getVendorId(), expectedPushNotificationLogCollection.get(i).getVendorId());
            assertEquals(responsePushNotificationLogCollection.get(i).getEntityId(), expectedPushNotificationLogCollection.get(i).getEntityId());
            assertEquals(responsePushNotificationLogCollection.get(i).getDeviceId(), expectedPushNotificationLogCollection.get(i).getDeviceId());
            assertEquals(responsePushNotificationLogCollection.get(i).getFirebaseResponse(), expectedPushNotificationLogCollection.get(i).getFirebaseResponse());
        }
    }



    @Test
    public void testCreateLogsIsNotMock() {
        List<PushNotificationTemplateDto> pushNotificationDtos = getPushNotificationDtos();
        List<String> response = new ArrayList<>();
        for(int i = 0; i < 8; i++) {
            response.add(testFirebaseResponse);
        }
        FirebaseResponseDto responseDto = new FirebaseResponseDto(response);
        ReflectionTestUtils.setField(pushNotificationHandler, "isMock", false);

        List<PushNotificationLogs> responsePushNotificationLogCollection = pushNotificationHandler.createLogs(pushNotificationDtos, responseDto, 1L, 1L, 1L);
        List<PushNotificationLogs> expectedPushNotificationLogCollection = getPushNotificationLogs();
        for (int i = 0; i < 8; i++) {
            assertEquals(responsePushNotificationLogCollection.get(i).getVendorId(), expectedPushNotificationLogCollection.get(i).getVendorId());
            assertEquals(responsePushNotificationLogCollection.get(i).getEntityId(), expectedPushNotificationLogCollection.get(i).getEntityId());
            assertEquals(responsePushNotificationLogCollection.get(i).getDeviceId(), expectedPushNotificationLogCollection.get(i).getDeviceId());
            assertEquals(responsePushNotificationLogCollection.get(i).getFirebaseResponse(), expectedPushNotificationLogCollection.get(i).getFirebaseResponse());
        }
    }

    @Test
    public void testProcessNotification() throws InterruptedException, ExecutionException{
        List<PushNotificationTemplateDto> pushNotificationDtos = getPushNotificationDtos();
        PushNotificationRequest pushNotificationRequest = new PushNotificationRequest(pushNotificationDtos, 1L,1L,1L);
        ReflectionTestUtils.setField(pushNotificationHandler, "isMock", true);
        List<PushNotificationLogs> pushNotificationLogsCollection = getMockPushNotificationLogs();
        pushNotificationHandler = Mockito.spy(pushNotificationHandler);

        doReturn(pushNotificationLogsCollection).when(pushNotificationHandler).createLogs(eq(pushNotificationDtos), any(), eq(1L), eq(1L), eq(1L));

        CompletableFuture<List<PushNotificationLogs>> expectedCompletableFuture = CompletableFuture.completedFuture(pushNotificationLogsCollection);
        CompletableFuture<List<PushNotificationLogs>> actualCompletableFuture = pushNotificationHandler.processNotification(pushNotificationDtos, pushNotificationRequest);
        assertEquals(expectedCompletableFuture.get(), actualCompletableFuture.get());
    }

    @Test
    public void testProcessNotificationIsNotMock() throws InterruptedException, ExecutionException{
        List<PushNotificationTemplateDto> pushNotificationDtos = getPushNotificationDtos();
        PushNotificationRequest pushNotificationRequest = new PushNotificationRequest(pushNotificationDtos, 1L,1L,1L);
        FirebaseResponseDto responseDto = new FirebaseResponseDto();
        ReflectionTestUtils.setField(pushNotificationHandler, "isMock", false);
        List<PushNotificationLogs> pushNotificationLogsCollection = getPushNotificationLogs();
        pushNotificationHandler = Mockito.spy(pushNotificationHandler);

        doReturn(pushNotificationLogsCollection).when(pushNotificationHandler).createLogs(eq(pushNotificationDtos), any(), eq(1L), eq(1L), eq(1L));
        doReturn(responseDto).when(pushNotificationHandler).firebaseCall(any(), any());

        CompletableFuture<List<PushNotificationLogs>> expectedCompletableFuture = CompletableFuture.completedFuture(pushNotificationLogsCollection);
        CompletableFuture<List<PushNotificationLogs>> actualCompletableFuture = pushNotificationHandler.processNotification(pushNotificationDtos, pushNotificationRequest);
        assertEquals(expectedCompletableFuture.get(), actualCompletableFuture.get());
    }

    @Test
    public void testProcessNotificationIsNotMockStartsWithTest() throws InterruptedException, ExecutionException{
        List<PushNotificationTemplateDto> pushNotificationDtos = getPushNotificationDtosWithTestPrefix();
        PushNotificationRequest pushNotificationRequest = new PushNotificationRequest(pushNotificationDtos, 1L,1L,1L);
        FirebaseResponseDto responseDto = new FirebaseResponseDto();
        ReflectionTestUtils.setField(pushNotificationHandler, "isMock", false);
        List<PushNotificationLogs> pushNotificationLogsCollection = getPushNotificationLogs();
        pushNotificationHandler = Mockito.spy(pushNotificationHandler);

        doReturn(pushNotificationLogsCollection).when(pushNotificationHandler).createLogs(eq(pushNotificationDtos), any(), eq(1L), eq(1L), eq(1L));
        doReturn(responseDto).when(pushNotificationHandler).firebaseCall(any(), any());

        CompletableFuture<List<PushNotificationLogs>> expectedCompletableFuture = CompletableFuture.completedFuture(pushNotificationLogsCollection);
        CompletableFuture<List<PushNotificationLogs>> actualCompletableFuture = pushNotificationHandler.processNotification(pushNotificationDtos, pushNotificationRequest);
        assertEquals(expectedCompletableFuture.get(), actualCompletableFuture.get());
    }



    @Test
    public void testSendNotification() throws InterruptedException, ExecutionException, ValidationException{
        List<PushNotificationTemplateDto> pushNotificationDtos = getPushNotificationDtos();
        PushNotificationRequest pushNotificationRequest = new PushNotificationRequest(pushNotificationDtos, 1L,1L,1L);
        pushNotificationRequest.setMessage("test1");
        pushNotificationRequest.setHeading("test1");
        pushNotificationHandler = Mockito.spy(pushNotificationHandler);
        List<PushNotificationLogs> pushNotificationLogsList = new ArrayList<>();
        pushNotificationLogsList.add(new PushNotificationLogs(1L, 1L, 1L, "1", "test1", new Date(), "TEST", "TEST"));
        CompletableFuture<List<PushNotificationLogs>> completableFuture = CompletableFuture.completedFuture(pushNotificationLogsList);
        List<PushNotificationLogs> pushNotificationLogsCollection = new ArrayList<>();
        for(int i = 0; i < 8; i++) {
            pushNotificationLogsCollection.addAll(completableFuture.get());
        }

        ReflectionTestUtils.setField(pushNotificationHandler, "isMock", true);

        doNothing().when(pushNotificationHandler).setVendorParams(pushNotificationRequest.getVendorId());
        doReturn(pushNotificationRequest).when(pushNotificationHandler).preprocessParams(any());
        doReturn(completableFuture).when(pushNotificationHandler).processNotification(any(), any());

        pushNotificationHandler.sendNotification(pushNotificationRequest);
        verify(pushLogsRepository, Mockito.times(1)).saveAll(pushNotificationLogsCollection);
    }

    @Test
    public void testSendNotificationInvalidDeviceId() throws InterruptedException, ExecutionException, ValidationException{
        List<PushNotificationTemplateDto> pushNotificationDtos = getPushNotificationDtos();
        List<String> device = new ArrayList<>();
        device.add("test");
        PushNotificationRequest pushNotificationRequest = new PushNotificationRequest(pushNotificationDtos, 1L,1L,1L);
        pushNotificationRequest.setMessage("test1");
        pushNotificationRequest.setHeading("test1");
        pushNotificationRequest.setTimeToLive(2L);
        pushNotificationHandler = Mockito.spy(pushNotificationHandler);
        List<PushNotificationLogs> pushNotificationLogsList = new ArrayList<>();
        pushNotificationLogsList.add(new PushNotificationLogs(1L, 1L, 1L, "1", "test1", new Date(), "TEST", "TEST"));
        CompletableFuture<List<PushNotificationLogs>> completableFuture = CompletableFuture.completedFuture(pushNotificationLogsList);
        List<PushNotificationLogs> pushNotificationLogsCollection = new ArrayList<>();
        for(int i = 0; i < 8; i++) {
            pushNotificationLogsCollection.addAll(completableFuture.get());
        }

        ReflectionTestUtils.setField(pushNotificationHandler, "isMock", true);
        ReflectionTestUtils.setField(pushNotificationHandler,"invalidDeviceIds",device);

        doNothing().when(pushNotificationHandler).setVendorParams(pushNotificationRequest.getVendorId());
        doReturn(pushNotificationRequest).when(pushNotificationHandler).preprocessParams(any());
        doReturn(completableFuture).when(pushNotificationHandler).processNotification(any(), any());
        when(vendorService.getVendorCredentials(any(),any())).thenReturn(creds);
        when(creds.getProject_id()).thenReturn("nikshay-app");

        pushNotificationHandler.sendNotification(pushNotificationRequest);
        verify(pushLogsRepository, Mockito.times(1)).saveAll(pushNotificationLogsCollection);
        verify(creds,Mockito.times(1)).getProject_id();
    }

    @Test
    public void testPreProcessParams()
    {
        List<PushNotificationTemplateDto> pushNotificationDtos = getPushNotificationDtos();
        PushNotificationRequest pushNotificationRequest = new PushNotificationRequest(pushNotificationDtos,1L,1L,1L);
        pushNotificationRequest.setMessage("test");
        pushNotificationRequest.setIsNotificationCommon(true);

        PushNotificationRequest pushNotificationRequest2 = new PushNotificationRequest(pushNotificationDtos,1L,1L,1L);
        pushNotificationRequest2.setMessage("test");
        pushNotificationRequest2.setIsNotificationCommon(false);

        PushNotificationRequest response =  pushNotificationHandler.preprocessParams(pushNotificationRequest);
        PushNotificationRequest response2 = pushNotificationHandler.preprocessParams(pushNotificationRequest2);

        assertEquals("test",response.getMessage());
        assertEquals("test",response2.getPersonList().get(0).getMessage());

    }

}

package tests.vendors;

import com.everwell.ins.enums.Language;
import com.everwell.ins.enums.PushNotificationGateway;
import com.everwell.ins.models.dto.*;
import com.everwell.ins.models.http.requests.PushNotificationRequest;
import com.everwell.ins.vendors.PushNotificationHandler;

import java.util.List;

public class MockPushNotificationHandler extends PushNotificationHandler{

    @Override
    public PushNotificationGateway getVendorGatewayEnumerator() {
        return null;
    }

    @Override
    public void setVendorParams(Long vendorId) { }

    @Override
    public FirebaseResponseDto firebaseCall(List<PushNotificationTemplateDto> persons, PushNotificationRequest pushNotificationRequest) {
        return null;
    }

    @Override
    public String getLanguageMapping(Language language) {
        return null;
    }

}

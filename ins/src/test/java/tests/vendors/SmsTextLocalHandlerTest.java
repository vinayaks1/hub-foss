package tests.vendors;


import com.everwell.ins.enums.Language;
import com.everwell.ins.exceptions.NotImplementedException;
import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.exceptions.VendorException;
import com.everwell.ins.models.db.SmsLogs;
import com.everwell.ins.models.dto.SmsDto;
import com.everwell.ins.models.dto.VendorResponseDto;
import com.everwell.ins.models.dto.vendorConfigs.TextLocalConfigDto;
import com.everwell.ins.models.dto.vendorCreds.ApiKeySenderDto;
import com.everwell.ins.models.http.requests.SmsRequest;
import com.everwell.ins.vendors.SmsTextLocalHandler;
import org.asynchttpclient.*;
import org.asynchttpclient.netty.NettyResponseFuture;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

public class SmsTextLocalHandlerTest extends SmsHandlerTest {

    @InjectMocks
    private SmsTextLocalHandler smsTextLocalHandler;

    @Mock
    private TextLocalConfigDto config;

    @Mock
    private ApiKeySenderDto creds;


    @Test
    public void testSetVendorParams() {
        Long vendorId = 1L;
        when(vendorService.getVendorConfig(any(), any())).thenReturn(null);
        when(vendorService.getVendorCredentials(any(), any())).thenReturn(null);

        smsTextLocalHandler.setVendorParams(vendorId);
        verify(vendorService, Mockito.times(1)).getVendorConfig(any(), any());
        verify(vendorService, Mockito.times(1)).getVendorCredentials(any(), any());
    }

    @Test
    public void testProcessSms() throws InterruptedException, ExecutionException {
        Long vendorId = 1L;
        Long triggerId = 1L;
        Long templateId = 1L;
        List<SmsDto> smsDtos = getSmsDtos();
        Language language = Language.NON_UNICODE;
        ReflectionTestUtils.setField(smsTextLocalHandler, "isMock", true);
        List<SmsLogs> smsLogCollection = getSmsLogsWithoutResponse();
        smsTextLocalHandler = Mockito.spy(smsTextLocalHandler);
        when(smsLogsRepository.saveAll(smsLogCollection)).thenReturn(smsLogCollection);

        doReturn(smsLogCollection).when(smsTextLocalHandler).createLogs(eq(smsDtos), any(), eq(vendorId), eq(templateId), eq(triggerId));

        CompletableFuture<List<SmsLogs>> expectedCompletableFuture = CompletableFuture.completedFuture(smsLogCollection);
        CompletableFuture<List<SmsLogs>> actualCompletableFuture = smsTextLocalHandler.processSms(smsDtos, language, vendorId, templateId, triggerId);
        assertEquals(expectedCompletableFuture.get(), actualCompletableFuture.get());
    }

    @Test
    public void testProcessSmsIsNotMock() throws InterruptedException, ExecutionException {
        Long vendorId = 1L;
        Long triggerId = 1L;
        Long templateId = 1L;
        List<SmsDto> smsDtos = getSmsDtos();
        Language language = Language.NON_UNICODE;
        ReflectionTestUtils.setField(smsTextLocalHandler, "isMock", false);
        List<SmsLogs> smsLogCollection = getSmsLogsWithoutResponse();
        VendorResponseDto responseDto = new VendorResponseDto();
        smsTextLocalHandler = Mockito.spy(smsTextLocalHandler);
        when(smsLogsRepository.saveAll(smsLogCollection)).thenReturn(smsLogCollection);

        doReturn(smsLogCollection).when(smsTextLocalHandler).createLogs(eq(smsDtos), any(), eq(vendorId), eq(templateId), eq(triggerId));
        doReturn(responseDto).when(smsTextLocalHandler).vendorCallTextLocal(any(), eq(language), nullable(Long.class));

        CompletableFuture<List<SmsLogs>> expectedCompletableFuture = CompletableFuture.completedFuture(smsLogCollection);
        CompletableFuture<List<SmsLogs>> actualCompletableFuture = smsTextLocalHandler.processSms(smsDtos, language, vendorId, templateId, triggerId);
        assertEquals(expectedCompletableFuture.get(), actualCompletableFuture.get());
    }

    @Test
    public void testPreprocessParams() {
        Long vendorId = 1L;
        String text = "  Test";
        String trimText = "Test";
        List<SmsDto> smsDtos = getSmsDtos();
        SmsRequest smsRequest = new SmsRequest(text, false, smsDtos, vendorId, null, null, null);

        SmsRequest smsRequest1 = smsTextLocalHandler.preprocessParams(smsRequest);
        assertEquals(trimText, smsRequest1.getMessage());
        assertNotEquals(trimText, smsRequest1.getPersonList().get(0).getMessage());

        smsRequest.setIsMessageCommon(true);
        SmsRequest smsRequest2 = smsTextLocalHandler.preprocessParams(smsRequest);
        assertEquals(trimText, smsRequest2.getMessage());
        assertEquals(trimText, smsRequest2.getPersonList().get(0).getMessage());
    }

    @Test(expected = NotImplementedException.class)
    public void testGetLanguageMapping() {
        smsTextLocalHandler.getLanguageMapping(Language.NON_UNICODE);
    }

    @PrepareForTest({AsyncHttpClient.class, NettyResponseFuture.class, Dsl.class})
    @Test
    public void testVendorCall() throws InterruptedException, ExecutionException {
        List<SmsDto> persons = getSmsDtos();
        mockStatic(Dsl.class);
        DefaultAsyncHttpClient client = mock(DefaultAsyncHttpClient.class);
        Response response = mock(Response.class);
        NettyResponseFuture<Response> future = mock(NettyResponseFuture.class);
        Long customId = 1L;
        Language language = Language.NON_UNICODE;
        String testApiKey = "apikey";
        String testSender = "sender";

        when(creds.getUrl()).thenReturn(testUrl);
        when(creds.getApiKey()).thenReturn(testApiKey);
        when(creds.getSender()).thenReturn(testSender);
        when(Dsl.asyncHttpClient()).thenReturn(client);
        when(client.executeRequest(any(Request.class))).thenReturn(future);
        when(future.get()).thenReturn(response);
        when(response.getResponseBody()).thenReturn(testApiResponse);

        VendorResponseDto expectedDto = new VendorResponseDto();
        expectedDto.setApiResponse(testApiResponse);
        expectedDto.setMessageId(customId.toString());
        VendorResponseDto responseDto = smsTextLocalHandler.vendorCallTextLocal(persons, language, customId);
        assertEquals(expectedDto.getMessageId(), responseDto.getMessageId());
        assertEquals(expectedDto.getApiResponse(), responseDto.getApiResponse());
    }

    @PrepareForTest({AsyncHttpClient.class, NettyResponseFuture.class, Dsl.class})
    @Test
    public void testVendorCallUnicode() throws InterruptedException, ExecutionException {
        List<SmsDto> persons = getSmsDtos();
        mockStatic(Dsl.class);
        DefaultAsyncHttpClient client = mock(DefaultAsyncHttpClient.class);
        Response response = mock(Response.class);
        NettyResponseFuture<Response> future = mock(NettyResponseFuture.class);
        String testApiKey = "apikey";
        String testSender = "sender";
        Long customId = 1L;

        when(creds.getUrl()).thenReturn(testUrl);
        when(creds.getApiKey()).thenReturn(testApiKey);
        when(creds.getSender()).thenReturn(testSender);
        when(Dsl.asyncHttpClient()).thenReturn(client);
        when(client.executeRequest(any(Request.class))).thenReturn(future);
        when(future.get()).thenReturn(response);
        when(response.getStatusCode()).thenReturn(200);
        when(response.getResponseBody()).thenReturn(testApiResponse);

        VendorResponseDto expectedDto = new VendorResponseDto();
        expectedDto.setApiResponse(testApiResponse);
        expectedDto.setMessageId(customId.toString());
        VendorResponseDto responseDto = smsTextLocalHandler.vendorCallTextLocal(persons, Language.UNICODE, customId);
        assertEquals(responseDto.getMessageId(), expectedDto.getMessageId());
        assertEquals(responseDto.getApiResponse(), expectedDto.getApiResponse());
    }

    @PrepareForTest({AsyncHttpClient.class, NettyResponseFuture.class, Dsl.class})
    @Test(expected = VendorException.class)
    public void testVendorCallException() throws InterruptedException, ExecutionException {
        List<SmsDto> persons = getSmsDtos();
        mockStatic(Dsl.class);
        DefaultAsyncHttpClient client = mock(DefaultAsyncHttpClient.class);
        NettyResponseFuture<Response> future = mock(NettyResponseFuture.class);
        Language language = Language.NON_UNICODE;
        Long customId = 1L;
        String testApiKey = "apikey";
        String testSender = "sender";

        when(creds.getUrl()).thenReturn(testUrl);
        when(creds.getApiKey()).thenReturn(testApiKey);
        when(creds.getSender()).thenReturn(testSender);
        when(Dsl.asyncHttpClient()).thenReturn(client);
        when(client.executeRequest(any(Request.class))).thenReturn(future);
        when(future.get()).thenThrow(InterruptedException.class);

        smsTextLocalHandler.vendorCallTextLocal(persons, language, customId);
    }

    @Test(expected = ValidationException.class)
    public void testVendorCallNullException() {
        List<SmsDto> persons = null;
        Long customId = 1L;
        smsTextLocalHandler.vendorCallTextLocal(persons, Language.NON_UNICODE, customId);
    }

    @PrepareForTest({AsyncHttpClient.class, NettyResponseFuture.class, Dsl.class})
    @Test
    public void testVendorCallExecutionException() throws InterruptedException, ExecutionException {
        List<SmsDto> persons = getSmsDtos();
        mockStatic(Dsl.class);
        DefaultAsyncHttpClient client = mock(DefaultAsyncHttpClient.class);
        Response response = mock(Response.class);
        NettyResponseFuture<Response> future = mock(NettyResponseFuture.class);
        String testApiKey = "apikey";
        String testSender = "sender";
        Long customId = 1L;

        when(creds.getUrl()).thenReturn(testUrl);
        when(creds.getApiKey()).thenReturn(testApiKey);
        when(creds.getSender()).thenReturn(testSender);
        when(Dsl.asyncHttpClient()).thenReturn(client);
        when(client.executeRequest(any(Request.class))).thenReturn(future);
        when(future.get()).thenThrow(ExecutionException.class);

        VendorResponseDto expectedDto = new VendorResponseDto(null, customId.toString());
        VendorResponseDto responseDto = smsTextLocalHandler.vendorCallTextLocal(persons, Language.NON_UNICODE, customId);
        assertEquals(expectedDto.getMessageId(), responseDto.getMessageId());
        assertEquals(expectedDto.getApiResponse(), responseDto.getApiResponse());
    }

}

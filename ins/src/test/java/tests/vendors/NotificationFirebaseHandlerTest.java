package tests.vendors;

import com.everwell.ins.enums.Language;
import com.everwell.ins.exceptions.NotImplementedException;
import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.models.dto.*;
import com.everwell.ins.models.dto.vendorConfigs.FirebaseConfigDto;
import com.everwell.ins.models.dto.vendorCreds.JSONFormatDto;
import com.everwell.ins.models.http.requests.PushNotificationRequest;
import com.everwell.ins.utils.SentryUtils;
import com.everwell.ins.vendors.NotificationFirebaseHandler;
import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.*;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@PrepareForTest({FirebaseMessaging.class,SentryUtils.class})
@PowerMockIgnore({"javax.xml.*", "com.sun.org.apache.xerces.*", "org.slf4j.*", "org.xml.*","com.ibm.*"})
public class NotificationFirebaseHandlerTest extends PushNotificationHandlerTest{
    @InjectMocks
    private NotificationFirebaseHandler notificationFirebaseHandler;

    @Mock
    private FirebaseConfigDto config;

    @Mock
    private JSONFormatDto creds;


    @Mock
    FirebaseMessaging mockfirebaseMessaging;


    @Mock
    ApiFuture<String> s;


    String mockcred = "{\"type\": \"service_account\",\"project_id\": \"nikshay-app\",\"private_key_id\": \"f4066e043f0cd8081d464c079b50172fdf94257c\",\"private_key\": \"-----BEGIN PRIVATE KEY-----\\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDcFO6UgQC6t4gz\\nWt0W+GMPhS2LrVDfL8nXOw8oUzH+5vaBIVoipfWWwqWbCXtlDzSdHKpn0P/PwJW6\\n4DzA6tWjbq6xqu2KO1Dp1VhHu7+w2tbzAS9/7t/YYuXHg7k4jl5bh6FevxD7z1hY\\nrdUzfdtGasNYi638NfwXBsjewmQBKpy5Ah+eJCMMud8ZzWjlFKNmh1HLL4EA2lvT\\n7/0Tofh4SuSprBL+nDxI3oLvBTT5VyrpFkd3J8y7SAcQu2Wh42i4kcDRvgAVEslC\\noT2a9/UcDbKkDmiSY9Lp8AJA81RAdj8ZlU9Vlj9FGPnHAFsrCnuj0JKfEHwNRzTU\\nCh0BclzBAgMBAAECggEAAJpQEvkNevVwuo9Hpr30a4D2YHkPjSTw3RWM05PywZt5\\nVl40hH7G3uxWjsemXl79ymZfMxyX7sY8wr/FetPJ1QJDGTp6HxRNgu5yKPQjMMVQ\\nrGa27hdF5q5Sqcq7EaQWLxL+hqdoUQA+4jNLr7HrDVTShyYOym+NarS6jR2kL3qg\\nmSo31AmTVL+x2lC2QUNIPfb6ch6B1wJ8Epijg/cb44VP3mDh/va6foCGzPjOnqlM\\nO3S5nXOHeRU+ftFlme/gGJaJAOFRtK8O2NSs8WAsNpx8sMqTLHQYjtw41Kpe/OTZ\\nUxjpKGSjIfSASNrnJNzB9Uu1J7js1QhUZmTQHYitjwKBgQD2bA8sguN3dBB62/D4\\nUyCnE2Uwuyqgv/vezJj4tYR/Qdgr5EAOPQPo/GUFkv1/i5/1hX2H4wWqiCeAIX6a\\nVZxhNFqfP30YDlyNHm0dxWIh8C27vSLHBml5O48BemvgftukkPcwuzFXd5KtwA2x\\nWuG6cCtjm2luHrev3PCjKzZuzwKBgQDkosgsgKYd8Qxdzz1R8XRV0nZmzntOwjBg\\nY72K/mIjdOn3QG2ft0qoinvpCd7uhV2CwRKV98Kv8e0SnKwmz4UFbewgRBEBsXpK\\nj9kr4kGNmLEu1tqRGuzI/GttU279AU1HLbgj39s99I3MlrmeBn6U6fXVQ7dgtf7b\\npMpPRQ7fbwKBgQDmLrZV28Xi9VXXjc+gBvZ/WsyxIoSFOBBsZC2YVOZ5gOYOyd1T\\n74bRgcJT3KRTGffHfnRuFJBcZWaQd7ktLT8wRZlJHCMb735gi8Vdg91Q2mu97MSp\\nqMGpbwr8VICkaZt+M7PTn2hNzfIDv1yLCNvGLbU6DBgFt0faTMWzhVAJXQKBgAY4\\nY4fKbDqOT07SD5IGJqsbEbuUTIiFAfntXL/GUfcy/BaM+y6INxDEy8A4U4dbu4KO\\nLCpGVlahF1kVSB26MZ1HM1BhabEJGhGjPj5SZoQqrk9e6w1dlSmWrHmJ9pANlAOm\\nu8bGFVnnKGBzjVM+S/1TLCwELp4D9Y4WOqbTcycjAoGBALbOVLLO2MoJ7A/ceO59\\nGAgB/ry1fFcNUshOB+c9739xMDqFMwQpMLwo4jMVDm61UJ+4/kzbEBJ89785EEnJ\\nRM+DgzijVmUA0FYPT3j7AyfXZjhwtDL/CLDzVeLC+xxg5/ygds43x7nVt0SnO3M+\\noB7AHv2LyyR+SmbeOH4nXCzM\\n-----END PRIVATE KEY-----\\n\",\"client_email\": \"firebase-adminsdk-ofycz@nikshay-app.iam.gserviceaccount.com\",\"client_id\": \"107206072464165743155\",\"auth_uri\": \"https://accounts.google.com/o/oauth2/auth\",\"token_uri\": \"https://oauth2.googleapis.com/token\",\"auth_provider_x509_cert_url\": \"https://www.googleapis.com/oauth2/v1/certs\",\"client_x509_cert_url\": \"https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-ofycz%40nikshay-app.iam.gserviceaccount.com\",\"credentialType\":\"JSONFORMAT\"}";

    //private static Long vendorId = 1L;
    // private static Long templateId = 2L;
    //private static Long triggerId = 3L;

    @Test
    public void testSetVendorParams() {
        Long vendorId = 1L;
        when(vendorService.getVendorConfig(any(), any())).thenReturn(null);
        when(vendorService.getVendorCredentials(any(), any())).thenReturn(null);

        notificationFirebaseHandler.setVendorParams(vendorId);
        verify(vendorService, Mockito.times(1)).getVendorConfig(any(), any());
        verify(vendorService, Mockito.times(1)).getVendorCredentials(any(), any());
    }

    @Test
    public void testGetBatchSize()
    {
        Integer size = 5;
        when(config.getBulkPNLimit()).thenReturn("5");

        Integer response =  notificationFirebaseHandler.getBatchSize(true);
        assertEquals(size,response);

        size = 1;
        Integer response2 =  notificationFirebaseHandler.getBatchSize(false);
        assertEquals(size,response2);

    }

    @Test(expected = NotImplementedException.class)
    public void testGetLanguageMapping() {
        notificationFirebaseHandler.getLanguageMapping(Language.NON_UNICODE);
    }

    @Test
    public void testVendorCall() throws Exception {
        GoogleCredentials googleCredentials = GoogleCredentials
                .fromStream(new ByteArrayInputStream(mockcred.getBytes()));
        FirebaseOptions firebaseOptions = FirebaseOptions
                .builder()
                .setCredentials(googleCredentials)
                .build();
        FirebaseApp.initializeApp(firebaseOptions,"test");
        List<PushNotificationTemplateDto> persons = getPushNotificationDtos();
        PushNotificationRequest pushNotificationRequest = new PushNotificationRequest(persons,1L,1L,1L);

        mockStatic(FirebaseMessaging.class);

        pushNotificationRequest.setHeading("TEST");
        pushNotificationRequest.setMessage("TEST");
        pushNotificationRequest.setTimeToLive(2L);
        ReflectionTestUtils.setField(notificationFirebaseHandler, "serverPort", "");
        notificationFirebaseHandler.setBaseUrl();

        when(creds.getProject_id()).thenReturn("test");
        when(FirebaseMessaging.getInstance(any())).thenReturn(mockfirebaseMessaging);
        when(mockfirebaseMessaging.sendAsync(any())).thenReturn(s);
        when(s.get()).thenReturn(testFirebaseResponse);

        List<String> firebaseResponse = new ArrayList<>();
        for(int i = 0; i < 8; i++)
            firebaseResponse.add(testFirebaseResponse);

        FirebaseResponseDto expectedDto = new FirebaseResponseDto(firebaseResponse);
        FirebaseResponseDto responseDto = notificationFirebaseHandler.firebaseCall(persons, pushNotificationRequest);
        assertEquals(expectedDto.getFirebaseResponse(), responseDto.getFirebaseResponse());
    }

    @Test
    public void testVendorCallException() throws InterruptedException, ExecutionException, IOException {
        List<PushNotificationTemplateDto> persons = getPushNotificationDtos();
        PushNotificationRequest pushNotificationRequest = new PushNotificationRequest(persons,null,1L,1L);

        pushNotificationRequest.setHeading("TEST");
        pushNotificationRequest.setMessage("TEST");
        pushNotificationRequest.setTimeToLive(2L);

        mockStatic(SentryUtils.class);


        ReflectionTestUtils.setField(notificationFirebaseHandler, "serverPort", "");
        notificationFirebaseHandler.setBaseUrl();

        notificationFirebaseHandler.firebaseCall(persons, pushNotificationRequest);
        PowerMockito.verifyStatic(SentryUtils.class, Mockito.times(8));
        SentryUtils.captureException(any());
    }

    @Test(expected = ValidationException.class)
    public void testVendorCallNullException() {
        List<PushNotificationTemplateDto> persons = null;
        notificationFirebaseHandler.firebaseCall(persons,null);
    }


}

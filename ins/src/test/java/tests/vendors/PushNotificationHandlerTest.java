package tests.vendors;

import com.everwell.ins.models.db.PushNotificationLogs;
import com.everwell.ins.models.dto.PushNotificationTemplateDto;
import com.everwell.ins.repositories.PushLogsRepository;
import com.everwell.ins.services.VendorService;
import org.junit.platform.commons.logging.LoggerFactory;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RunWith(PowerMockRunner.class)
@PrepareForTest({LoggerFactory.class})
@PowerMockIgnore({"javax.xml.*", "com.sun.org.apache.xerces.*", "org.slf4j.*"})

public abstract class PushNotificationHandlerTest {
    @Mock
    protected VendorService vendorService;

    @Mock
    protected PushLogsRepository pushLogsRepository;

    protected String testString = "test";

    protected String testFirebaseResponse = "success";

    protected String testDeviceId = "1";

    protected String testUrl = "http://www.test.com";

    private static String entityId1 = "1";
    private static String entityId2 = "2";
    private static String entityId3 = "3";
    private static String entityId4 = "4";
    private static String entityId5 = "5";
    private static String entityId6 = "6";
    private static String entityId7 = "7";
    private static String entityId8 = "8";

    protected List<PushNotificationTemplateDto> getPushNotificationDtos() {
        List<PushNotificationTemplateDto> pushNotificationDtos = new ArrayList<>();
        pushNotificationDtos.add(new PushNotificationTemplateDto(entityId1, "1","test","test"));
        pushNotificationDtos.add(new PushNotificationTemplateDto(entityId2, "2","test","test"));
        pushNotificationDtos.add(new PushNotificationTemplateDto(entityId3, "3","test","test"));
        pushNotificationDtos.add(new PushNotificationTemplateDto(entityId4, "4","test","test"));
        pushNotificationDtos.add(new PushNotificationTemplateDto(entityId5, "5","test","test"));
        pushNotificationDtos.add(new PushNotificationTemplateDto(entityId6, "6","test","test"));
        pushNotificationDtos.add(new PushNotificationTemplateDto(entityId7, "7","test","test"));
        pushNotificationDtos.add(new PushNotificationTemplateDto(entityId8, "8","test","test"));

        return pushNotificationDtos;
    }

    protected List<PushNotificationTemplateDto> getPushNotificationDtosWithTestPrefix() {
        List<PushNotificationTemplateDto> pushNotificationDtos = new ArrayList<>();
        pushNotificationDtos.add(new PushNotificationTemplateDto(entityId1, "1","[TEST] test","[TEST] test"));
        pushNotificationDtos.add(new PushNotificationTemplateDto(entityId2, "2","[TEST] test","[TEST] test"));
        pushNotificationDtos.add(new PushNotificationTemplateDto(entityId3, "3","[TEST] test","[TEST] test"));
        pushNotificationDtos.add(new PushNotificationTemplateDto(entityId4, "4","[TEST] test","[TEST] test"));
        pushNotificationDtos.add(new PushNotificationTemplateDto(entityId5, "5","[TEST] test","[TEST] test"));
        pushNotificationDtos.add(new PushNotificationTemplateDto(entityId6, "6","[TEST] test","[TEST] test"));
        pushNotificationDtos.add(new PushNotificationTemplateDto(entityId7, "7","[TEST] test","[TEST] test"));
        pushNotificationDtos.add(new PushNotificationTemplateDto(entityId8, "8","[TEST] test","[TEST] test"));

        return pushNotificationDtos;
    }



    protected List<String> getToken() {
        List<String> tokens = new ArrayList<>();
        tokens.add("1");
        tokens.add("2");
        tokens.add("3");
        tokens.add("4");
        tokens.add("5");
        tokens.add("6");
        tokens.add("7");
        tokens.add("8");
        return tokens;
    }

    protected List<PushNotificationLogs> getMockPushNotificationLogs() {
        List<PushNotificationLogs> pushNotificationLogsCollection= new ArrayList<>();
        pushNotificationLogsCollection.add(new PushNotificationLogs(1L, 2L, 3L, entityId1,"test1", new Date(), "TEST", "1"));
        pushNotificationLogsCollection.add(new PushNotificationLogs(1L, 2L, 3L, entityId2,"test2", new Date(), "TEST", "2"));
        pushNotificationLogsCollection.add(new PushNotificationLogs(1L, 2L, 3L, entityId3,"test3", new Date(), "TEST", "3"));
        pushNotificationLogsCollection.add(new PushNotificationLogs(1L, 2L, 3L, entityId4,"test4", new Date(), "TEST", "4"));
        pushNotificationLogsCollection.add(new PushNotificationLogs(1L, 2L, 3L, entityId5,"test5", new Date(), "TEST", "5"));
        pushNotificationLogsCollection.add(new PushNotificationLogs(1L, 2L, 3L, entityId6,"test6", new Date(), "TEST", "6"));
        pushNotificationLogsCollection.add(new PushNotificationLogs(1L, 2L, 3L, entityId7,"test7", new Date(), "TEST", "7"));
        pushNotificationLogsCollection.add(new PushNotificationLogs(1L, 2L, 3L, entityId8,"test8", new Date(), "TEST", "8"));

        return pushNotificationLogsCollection;
    }

    protected List<PushNotificationLogs> getPushNotificationLogs() {
        List<PushNotificationLogs> pushNotificationLogsCollection= new ArrayList<>();
        pushNotificationLogsCollection.add(new PushNotificationLogs(1L, 2L, 3L, entityId1,"test1", new Date(), testFirebaseResponse, "1"));
        pushNotificationLogsCollection.add(new PushNotificationLogs(1L, 2L, 3L, entityId2,"test2", new Date(), testFirebaseResponse, "2"));
        pushNotificationLogsCollection.add(new PushNotificationLogs(1L, 2L, 3L, entityId3,"test3", new Date(), testFirebaseResponse, "3"));
        pushNotificationLogsCollection.add(new PushNotificationLogs(1L, 2L, 3L, entityId4,"test4", new Date(), testFirebaseResponse, "4"));
        pushNotificationLogsCollection.add(new PushNotificationLogs(1L, 2L, 3L, entityId5,"test5", new Date(), testFirebaseResponse, "5"));
        pushNotificationLogsCollection.add(new PushNotificationLogs(1L, 2L, 3L, entityId6,"test6", new Date(), testFirebaseResponse, "6"));
        pushNotificationLogsCollection.add(new PushNotificationLogs(1L, 2L, 3L, entityId7,"test7", new Date(), testFirebaseResponse, "7"));
        pushNotificationLogsCollection.add(new PushNotificationLogs(1L, 2L, 3L, entityId8,"test8", new Date(), testFirebaseResponse, "8"));

        return pushNotificationLogsCollection;
    }

    protected List<PushNotificationLogs> getPushNotificationLogsWithoutResponse() {
        List<PushNotificationLogs> pushNotificationLogsCollection= new ArrayList<>();
        pushNotificationLogsCollection.add(new PushNotificationLogs(1L, 2L, 3L, entityId1,"test1", new Date(), null, "1"));
        pushNotificationLogsCollection.add(new PushNotificationLogs(1L, 2L, 3L, entityId2,"test2", new Date(), null, "2"));
        pushNotificationLogsCollection.add(new PushNotificationLogs(1L, 2L, 3L, entityId3,"test3", new Date(), null, "3"));
        pushNotificationLogsCollection.add(new PushNotificationLogs(1L, 2L, 3L, entityId4,"test4", new Date(), null, "4"));
        pushNotificationLogsCollection.add(new PushNotificationLogs(1L, 2L, 3L, entityId5,"test5", new Date(), null, "5"));
        pushNotificationLogsCollection.add(new PushNotificationLogs(1L, 2L, 3L, entityId6,"test6", new Date(), null, "6"));
        pushNotificationLogsCollection.add(new PushNotificationLogs(1L, 2L, 3L, entityId7,"test7", new Date(), null, "7"));
        pushNotificationLogsCollection.add(new PushNotificationLogs(1L, 2L, 3L, entityId8,"test8", new Date(), null, "8"));

        return pushNotificationLogsCollection;
    }

    protected List<PushNotificationLogs> getPushNotificationLogsWithTestPrefix() {
        List<PushNotificationLogs> pushNotificationLogsCollection= new ArrayList<>();
        pushNotificationLogsCollection.add(new PushNotificationLogs(1L, 2L, 3L, entityId1,"[TEST] test1", new Date(), "TEST", "1"));
        pushNotificationLogsCollection.add(new PushNotificationLogs(1L, 2L, 3L, entityId2,"[TEST] test2", new Date(), "TEST", "2"));
        pushNotificationLogsCollection.add(new PushNotificationLogs(1L, 2L, 3L, entityId3,"[TEST] test3", new Date(), "TEST", "3"));
        pushNotificationLogsCollection.add(new PushNotificationLogs(1L, 2L, 3L, entityId4,"[TEST] test4", new Date(), "TEST", "4"));
        pushNotificationLogsCollection.add(new PushNotificationLogs(1L, 2L, 3L, entityId5,"[TEST] test5", new Date(), "TEST", "5"));
        pushNotificationLogsCollection.add(new PushNotificationLogs(1L, 2L, 3L, entityId6,"[TEST] test6", new Date(), "TEST", "6"));
        pushNotificationLogsCollection.add(new PushNotificationLogs(1L, 2L, 3L, entityId7,"[TEST] test7", new Date(), "TEST", "7"));
        pushNotificationLogsCollection.add(new PushNotificationLogs(1L, 2L, 3L, entityId8,"[TEST] test8", new Date(), "TEST", "8"));

        return pushNotificationLogsCollection;
    }

}

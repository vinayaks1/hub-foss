package tests.vendors;


import com.everwell.ins.enums.Language;
import com.everwell.ins.exceptions.NotImplementedException;
import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.exceptions.VendorException;
import com.everwell.ins.models.db.SmsLogs;
import com.everwell.ins.models.dto.SmsDto;
import com.everwell.ins.models.dto.VendorResponseDto;
import com.everwell.ins.models.dto.vendorConfigs.InterraConfigDto;
import com.everwell.ins.models.dto.vendorCreds.ApiKeySenderRouteDto;
import com.everwell.ins.vendors.SmsInterraHandler;
import org.springframework.test.util.ReflectionTestUtils;
import org.asynchttpclient.*;
import org.asynchttpclient.netty.NettyResponseFuture;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

public class SmsInterraHandlerTest extends SmsHandlerTest {

    @InjectMocks
    SmsInterraHandler smsInterraHandler;

    @Mock
    private ApiKeySenderRouteDto creds;

    @Mock
    private InterraConfigDto config;

    @Test(expected = NotImplementedException.class)
    public void testGetLanguageMapping() {
        smsInterraHandler.getLanguageMapping(Language.NON_UNICODE);
    }

    @Test
    public void testSetVendorParams() {
        Long vendorId = 1L;
        when(vendorService.getVendorConfig(any(), any())).thenReturn(null);
        when(vendorService.getVendorCredentials(any(), any())).thenReturn(null);

        smsInterraHandler.setVendorParams(vendorId);
        verify(vendorService, Mockito.times(1)).getVendorConfig(any(), any());
        verify(vendorService, Mockito.times(1)).getVendorCredentials(any(), any());
    }

    @PrepareForTest({AsyncHttpClient.class, NettyResponseFuture.class, Dsl.class})
    @Test
    public void testVendorCall() throws InterruptedException, ExecutionException {
        List<SmsDto> persons = getSmsDtos();
        mockStatic(Dsl.class);
        DefaultAsyncHttpClient client = mock(DefaultAsyncHttpClient.class);
        Response response = mock(Response.class);
        NettyResponseFuture<Response> future = mock(NettyResponseFuture.class);
        Long referenceNumber = 1L;

        when(creds.getUrl()).thenReturn(testUrl);
        when(creds.getOriginator()).thenReturn(testString);
        when(creds.getSenderId()).thenReturn(testString);
        when(creds.getCreatorId()).thenReturn(testString);
        when(creds.getCountry()).thenReturn(testString);
        when(creds.getCountryCode()).thenReturn(testString);
        when(creds.getUserName()).thenReturn(testString);
        when(creds.getApiKey()).thenReturn(testString);
        when(Dsl.asyncHttpClient()).thenReturn(client);
        when(client.executeRequest(any(Request.class))).thenReturn(future);
        when(future.get()).thenReturn(response);
        when(response.getStatusCode()).thenReturn(200);
        when(response.getResponseBody()).thenReturn(testApiResponse);

        VendorResponseDto expectedDto = new VendorResponseDto();
        expectedDto.setApiResponse(testApiResponse);
        expectedDto.setMessageId(referenceNumber.toString());
        VendorResponseDto responseDto = smsInterraHandler.vendorCallInterra(persons, Language.NON_UNICODE, referenceNumber);
        assertEquals(expectedDto.getApiResponse(), responseDto.getApiResponse());
        assertEquals(expectedDto.getMessageId(), responseDto.getMessageId());

    }

    @PrepareForTest({AsyncHttpClient.class, NettyResponseFuture.class, Dsl.class})
    @Test(expected = VendorException.class)
    public void testVendorCallException() throws InterruptedException, ExecutionException {
        List<SmsDto> persons = getSmsDtos();
        mockStatic(Dsl.class);
        DefaultAsyncHttpClient client = mock(DefaultAsyncHttpClient.class);
        NettyResponseFuture<Response> future = mock(NettyResponseFuture.class);

        when(creds.getUrl()).thenReturn(testUrl);
        when(Dsl.asyncHttpClient()).thenReturn(client);
        when(client.executeRequest(any(Request.class))).thenReturn(future);
        when(future.get()).thenThrow(InterruptedException.class);

        smsInterraHandler.vendorCallInterra(persons, Language.NON_UNICODE, null);
    }

    @Test(expected = ValidationException.class)
    public void testVendorCallNullException() {
        List<SmsDto> persons = null;
        smsInterraHandler.vendorCallInterra(persons, Language.NON_UNICODE, null);
    }

    @Test
    public void testGetBatchSize() {
        Integer size = 5;
        when(config.getBulkSmsLimit()).thenReturn("5");

        Integer batchSize1 = smsInterraHandler.getBatchSize(true);
        assertEquals(size, batchSize1);

        size = 1;
        Integer batchSize2 = smsInterraHandler.getBatchSize(false);
        assertEquals(size, batchSize2);
    }

    @Test
    public void testProcessSms() throws InterruptedException, ExecutionException {
        Long vendorId = 1L;
        Long triggerId = 1L;
        Long templateId = 1L;
        List<SmsDto> smsDtos = getSmsDtos();
        Language language = Language.NON_UNICODE;
        ReflectionTestUtils.setField(smsInterraHandler, "isMock", true);
        List<SmsLogs> smsLogCollection = getSmsLogsWithoutResponse();
        smsInterraHandler = Mockito.spy(smsInterraHandler);
        when(smsLogsRepository.saveAll(smsLogCollection)).thenReturn(smsLogCollection);

        doReturn(smsLogCollection).when(smsInterraHandler).createLogs(eq(smsDtos), any(), eq(vendorId), eq(templateId), eq(triggerId));

        CompletableFuture<List<SmsLogs>> expectedCompletableFuture = CompletableFuture.completedFuture(smsLogCollection);
        CompletableFuture<List<SmsLogs>> actualCompletableFuture = smsInterraHandler.processSms(smsDtos, language, vendorId, templateId, triggerId);
        assertEquals(expectedCompletableFuture.get(), actualCompletableFuture.get());
    }

    @Test
    public void testProcessSmsIsNotMock() throws InterruptedException, ExecutionException {
        Long vendorId = 1L;
        Long triggerId = 1L;
        Long templateId = 1L;
        List<SmsDto> smsDtos = getSmsDtos();
        Language language = Language.NON_UNICODE;
        ReflectionTestUtils.setField(smsInterraHandler, "isMock", false);
        List<SmsLogs> smsLogCollection = getSmsLogsWithoutResponse();
        VendorResponseDto responseDto = new VendorResponseDto();
        smsInterraHandler = Mockito.spy(smsInterraHandler);
        when(smsLogsRepository.saveAll(smsLogCollection)).thenReturn(smsLogCollection);

        doReturn(smsLogCollection).when(smsInterraHandler).createLogs(eq(smsDtos), any(), eq(vendorId), eq(templateId), eq(triggerId));
        doReturn(responseDto).when(smsInterraHandler).vendorCallInterra(any(), eq(language), nullable(Long.class));

        CompletableFuture<List<SmsLogs>> expectedCompletableFuture = CompletableFuture.completedFuture(smsLogCollection);
        CompletableFuture<List<SmsLogs>> actualCompletableFuture = smsInterraHandler.processSms(smsDtos, language, vendorId, templateId, triggerId);
        assertEquals(expectedCompletableFuture.get(), actualCompletableFuture.get());
    }

}

package tests.vendors;

import com.everwell.ins.enums.Language;
import com.everwell.ins.models.db.SmsLogs;
import com.everwell.ins.models.dto.SmsDto;
import com.everwell.ins.models.dto.VendorResponseDto;
import com.everwell.ins.models.http.requests.SmsRequest;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class MockSmsHandlerTest extends SmsHandlerTest {

    @InjectMocks
    private MockSmsHandler smsHandler;

    @Mock
    private ApplicationEventPublisher applicationEventPublisher;

    private static Long vendorId = 1L;
    private static Long templateId = 2L;
    private static Long triggerId = 3L;

    @Test
    public void testCreateLogs() {
        List<SmsDto> smsDtos = getSmsDtos();
        VendorResponseDto responseDto = new VendorResponseDto(testApiResponse, testMessageId);
        ReflectionTestUtils.setField(smsHandler, "isMock", true);

        List<SmsLogs> responseSmsLogCollection = smsHandler.createLogs(smsDtos, responseDto, vendorId, templateId, triggerId);
        List<SmsLogs> expectedSmsLogCollection = getMockSmsLogs();
        for (int i = 0; i < 8; i++) {
            assertEquals(responseSmsLogCollection.get(i).getVendorId(), expectedSmsLogCollection.get(i).getVendorId());
            assertEquals(responseSmsLogCollection.get(i).getEntityId(), expectedSmsLogCollection.get(i).getEntityId());
            assertEquals(responseSmsLogCollection.get(i).getPhoneNumber(), expectedSmsLogCollection.get(i).getPhoneNumber());
            assertEquals(responseSmsLogCollection.get(i).getMessage(), expectedSmsLogCollection.get(i).getMessage());
            assertEquals(responseSmsLogCollection.get(i).getMessageId(), expectedSmsLogCollection.get(i).getMessageId());
            assertEquals(responseSmsLogCollection.get(i).getApiResponse(), expectedSmsLogCollection.get(i).getApiResponse());
        }
    }

    @Test
    public void testCreateLogsIsNotMock() {
        List<SmsDto> smsDtos = getSmsDtos();
        VendorResponseDto responseDto = new VendorResponseDto(testApiResponse, testMessageId);
        ReflectionTestUtils.setField(smsHandler, "isMock", false);

        List<SmsLogs> responseSmsLogCollection = smsHandler.createLogs(smsDtos, responseDto, vendorId, templateId, triggerId);
        List<SmsLogs> expectedSmsLogCollection = getSmsLogs();
        for (int i = 0; i < 8; i++) {
            assertEquals(responseSmsLogCollection.get(i).getVendorId(), expectedSmsLogCollection.get(i).getVendorId());
            assertEquals(responseSmsLogCollection.get(i).getEntityId(), expectedSmsLogCollection.get(i).getEntityId());
            assertEquals(responseSmsLogCollection.get(i).getPhoneNumber(), expectedSmsLogCollection.get(i).getPhoneNumber());
            assertEquals(responseSmsLogCollection.get(i).getMessage(), expectedSmsLogCollection.get(i).getMessage());
            assertEquals(responseSmsLogCollection.get(i).getMessageId(), expectedSmsLogCollection.get(i).getMessageId());
            assertEquals(responseSmsLogCollection.get(i).getApiResponse(), expectedSmsLogCollection.get(i).getApiResponse());
        }
    }

    @Test
    public void testProcessSms() throws InterruptedException, ExecutionException {
        List<SmsDto> smsDtos = getSmsDtos();
        Language language = Language.NON_UNICODE;
        ReflectionTestUtils.setField(smsHandler, "isMock", true);
        List<SmsLogs> smsLogCollection = getMockSmsLogs();
        smsHandler = Mockito.spy(smsHandler);

        doReturn(smsLogCollection).when(smsHandler).createLogs(eq(smsDtos), any(), eq(vendorId), eq(templateId), eq(triggerId));

        CompletableFuture<List<SmsLogs>> expectedCompletableFuture = CompletableFuture.completedFuture(smsLogCollection);
        CompletableFuture<List<SmsLogs>> actualCompletableFuture = smsHandler.processSms(smsDtos, language, vendorId, templateId, triggerId);
        assertEquals(expectedCompletableFuture.get(), actualCompletableFuture.get());
    }

    @Test
    public void testProcessSmsIsNotMock() throws InterruptedException, ExecutionException {
        List<SmsDto> smsDtos = getSmsDtos();
        Language language = Language.NON_UNICODE;
        ReflectionTestUtils.setField(smsHandler, "isMock", false);
        List<SmsLogs> smsLogCollection = getSmsLogs();
        VendorResponseDto responseDto = new VendorResponseDto();
        smsHandler = Mockito.spy(smsHandler);

        doReturn(smsLogCollection).when(smsHandler).createLogs(eq(smsDtos), any(), eq(vendorId), eq(templateId), eq(triggerId));
        doReturn(responseDto).when(smsHandler).vendorCall(any(), any(), any());

        CompletableFuture<List<SmsLogs>> expectedCompletableFuture = CompletableFuture.completedFuture(smsLogCollection);
        CompletableFuture<List<SmsLogs>> actualCompletableFuture = smsHandler.processSms(smsDtos, language, vendorId, templateId, triggerId);
        assertEquals(expectedCompletableFuture.get(), actualCompletableFuture.get());
    }

    @Test
    public void testProcessSmsStartsWithTest() throws InterruptedException, ExecutionException {
        List<SmsDto> smsDtos = getSmsDtosWithTestPrefix();
        Language language = Language.NON_UNICODE;
        ReflectionTestUtils.setField(smsHandler, "isMock", true);
        List<SmsLogs> smsLogCollection = getSmsLogsWithTestPrefix();
        VendorResponseDto responseDto = new VendorResponseDto("TEST: Response", "TEST: Id");
        smsHandler = Mockito.spy(smsHandler);

        doReturn(smsLogCollection).when(smsHandler).createLogs(eq(smsDtos), eq(responseDto), eq(vendorId), eq(templateId), eq(triggerId));
        doReturn(responseDto).when(smsHandler).vendorCall(any(), any(), any());

        CompletableFuture<List<SmsLogs>> expectedCompletableFuture = CompletableFuture.completedFuture(smsLogCollection);
        CompletableFuture<List<SmsLogs>> actualCompletableFuture = smsHandler.processSms(smsDtos, language, vendorId, templateId, triggerId);
        assertEquals(expectedCompletableFuture.get(), actualCompletableFuture.get());
    }

    @Test
    public void testProcessSmsStartsWithTestIsNotMock() throws InterruptedException, ExecutionException {
        List<SmsDto> smsDtos = getSmsDtosWithTestPrefix();
        Language language = Language.NON_UNICODE;
        ReflectionTestUtils.setField(smsHandler, "isMock", false);
        List<SmsLogs> smsLogCollection = getSmsLogs();
        VendorResponseDto responseDto = new VendorResponseDto(testApiResponse, testMessageId);
        smsHandler = Mockito.spy(smsHandler);

        doReturn(smsLogCollection).when(smsHandler).createLogs(eq(smsDtos), eq(responseDto), eq(vendorId), eq(templateId), eq(triggerId));
        doReturn(responseDto).when(smsHandler).vendorCall(any(), any(), any());

        CompletableFuture<List<SmsLogs>> expectedCompletableFuture = CompletableFuture.completedFuture(smsLogCollection);
        CompletableFuture<List<SmsLogs>> actualCompletableFuture = smsHandler.processSms(smsDtos, language, vendorId, templateId, triggerId);
        assertEquals(expectedCompletableFuture.get(), actualCompletableFuture.get());
    }

    @Test
    public void testSendSms() throws InterruptedException, ExecutionException {
        String text = "  Test";
        List<SmsDto> smsDtos = getSmsDtos();
        SmsRequest smsRequest = new SmsRequest(text, false, smsDtos, vendorId, templateId, triggerId, true);
        smsHandler = Mockito.spy(smsHandler);
        List<SmsLogs> smsLogList = new ArrayList<>();
        smsLogList.add(new SmsLogs(vendorId, templateId, triggerId, "1", "1", "test1", new Date(), "TEST", "TEST"));
        CompletableFuture<List<SmsLogs>> completableFuture = CompletableFuture.completedFuture(smsLogList);
        List<SmsLogs> smsLogCollection = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            smsLogCollection.addAll(completableFuture.get());
        }
        ReflectionTestUtils.setField(smsHandler, "isMock", true);

        doNothing().when(smsHandler).setVendorParams(smsRequest.getVendorId());
        when(smsHandler.preprocessParams(smsRequest)).thenReturn(smsRequest);
        doReturn(completableFuture).when(smsHandler).processSms(any(), any(), any(), any(), any());

        smsHandler.sendSms(smsRequest);
        verify(smsLogsRepository, Mockito.times(1)).saveAll(smsLogCollection);
    }
}

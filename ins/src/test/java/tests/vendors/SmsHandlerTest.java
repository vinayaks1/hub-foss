package tests.vendors;


import com.everwell.ins.models.db.SmsLogs;
import com.everwell.ins.models.dto.SmsDto;
import com.everwell.ins.repositories.SmsLogsRepository;
import com.everwell.ins.services.VendorService;
import org.junit.platform.commons.logging.LoggerFactory;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RunWith(PowerMockRunner.class)
@PrepareForTest({LoggerFactory.class})
@PowerMockIgnore({"javax.xml.*", "com.sun.org.apache.xerces.*", "org.slf4j.*", "org.xml.*","com.ibm.*"})
public abstract class SmsHandlerTest {

    @Mock
    protected VendorService vendorService;

    @Mock
    protected SmsLogsRepository smsLogsRepository;

    protected String testString = "test";

    protected String testApiResponse = "success";

    protected String testMessageId = "1";

    protected String testUrl = "http://www.test.com";

    private static String entityId1 = "1";
    private static String entityId2 = "2";
    private static String entityId3 = "3";
    private static String entityId4 = "4";
    private static String entityId5 = "5";
    private static String entityId6 = "6";
    private static String entityId7 = "7";
    private static String entityId8 = "8";

    protected List<SmsDto> getSmsDtos() {
        List<SmsDto> smsDtos = new ArrayList<>();
        smsDtos.add(new SmsDto(entityId1, "1", "test1"));
        smsDtos.add(new SmsDto(entityId2, "2", "test2"));
        smsDtos.add(new SmsDto(entityId3, "3", "test3"));
        smsDtos.add(new SmsDto(entityId4, "4", "test4"));
        smsDtos.add(new SmsDto(entityId5, "5", "test5"));
        smsDtos.add(new SmsDto(entityId6, "6", "test6"));
        smsDtos.add(new SmsDto(entityId7, "7", "test7"));
        smsDtos.add(new SmsDto(entityId8, "8", "test8"));
        return smsDtos;
    }

    protected List<SmsDto> getSmsDtosWithTestPrefix() {
        List<SmsDto> smsDtos = new ArrayList<>();
        smsDtos.add(new SmsDto(entityId1, "1", "[TEST-SEND] test1"));
        smsDtos.add(new SmsDto(entityId2, "2", "[TEST-SEND] test2"));
        smsDtos.add(new SmsDto(entityId3, "3", "[TEST-SEND] test3"));
        smsDtos.add(new SmsDto(entityId4, "4", "[TEST-SEND] test4"));
        smsDtos.add(new SmsDto(entityId5, "5", "[TEST-SEND] test5"));
        smsDtos.add(new SmsDto(entityId6, "6", "[TEST-SEND] test6"));
        smsDtos.add(new SmsDto(entityId7, "7", "[TEST-SEND] test7"));
        smsDtos.add(new SmsDto(entityId8, "8", "[TEST-SEND] test8"));
        return smsDtos;
    }

    protected List<String> getPhones() {
        List<String> phones = new ArrayList<>();
        phones.add("1");
        phones.add("2");
        phones.add("3");
        phones.add("4");
        phones.add("5");
        phones.add("6");
        phones.add("7");
        phones.add("8");
        return phones;
    }

    protected List<SmsLogs> getMockSmsLogs() {
        List<SmsLogs> smsLogCollection = new ArrayList<>();
        smsLogCollection.add(new SmsLogs(1L, 2L, 3L, entityId1, "1", "test1", new Date(), "TEST", "TEST"));
        smsLogCollection.add(new SmsLogs(1L, 2L, 3L, entityId2, "2", "test2", new Date(), "TEST", "TEST"));
        smsLogCollection.add(new SmsLogs(1L, 2L, 3L, entityId3, "3", "test3", new Date(), "TEST", "TEST"));
        smsLogCollection.add(new SmsLogs(1L, 2L, 3L, entityId4, "4", "test4", new Date(), "TEST", "TEST"));
        smsLogCollection.add(new SmsLogs(1L, 2L, 3L, entityId5, "5", "test5", new Date(), "TEST", "TEST"));
        smsLogCollection.add(new SmsLogs(1L, 2L, 3L, entityId6, "6", "test6", new Date(), "TEST", "TEST"));
        smsLogCollection.add(new SmsLogs(1L, 2L, 3L, entityId7, "7", "test7", new Date(), "TEST", "TEST"));
        smsLogCollection.add(new SmsLogs(1L, 2L, 3L, entityId8, "8", "test8", new Date(), "TEST", "TEST"));
        return smsLogCollection;
    }

    protected List<SmsLogs> getSmsLogs() {
        List<SmsLogs> smsLogCollection = new ArrayList<>();
        smsLogCollection.add(new SmsLogs(1L, 2L, 3L, entityId1, "1", "test1", new Date(), testApiResponse, testMessageId));
        smsLogCollection.add(new SmsLogs(1L, 2L, 3L, entityId2, "2", "test2", new Date(), testApiResponse, testMessageId));
        smsLogCollection.add(new SmsLogs(1L, 2L, 3L, entityId3, "3", "test3", new Date(), testApiResponse, testMessageId));
        smsLogCollection.add(new SmsLogs(1L, 2L, 3L, entityId4, "4", "test4", new Date(), testApiResponse, testMessageId));
        smsLogCollection.add(new SmsLogs(1L, 2L, 3L, entityId5, "5", "test5", new Date(), testApiResponse, testMessageId));
        smsLogCollection.add(new SmsLogs(1L, 2L, 3L, entityId6, "6", "test6", new Date(), testApiResponse, testMessageId));
        smsLogCollection.add(new SmsLogs(1L, 2L, 3L, entityId7, "7", "test7", new Date(), testApiResponse, testMessageId));
        smsLogCollection.add(new SmsLogs(1L, 2L, 3L, entityId8, "8", "test8", new Date(), testApiResponse, testMessageId));
        return smsLogCollection;
    }

    protected List<SmsLogs> getSmsLogsWithoutResponse() {
        List<SmsLogs> smsLogCollection = new ArrayList<>();
        smsLogCollection.add(new SmsLogs(1L, 1L, 1L, entityId1, "1", "test1", new Date(), null, testMessageId));
        smsLogCollection.add(new SmsLogs(1L, 2L, 2L, entityId2, "2", "test2", new Date(), null, testMessageId));
        smsLogCollection.add(new SmsLogs(1L, 3L, 3L, entityId3, "3", "test3", new Date(), null, testMessageId));
        smsLogCollection.add(new SmsLogs(1L, 4L, 4L, entityId4, "4", "test4", new Date(), null, testMessageId));
        smsLogCollection.add(new SmsLogs(1L, 5L, 5L, entityId5, "5", "test5", new Date(), null, testMessageId));
        smsLogCollection.add(new SmsLogs(1L, 6L, 6L, entityId6, "6", "test6", new Date(), null, testMessageId));
        smsLogCollection.add(new SmsLogs(1L, 7L, 7L, entityId7, "7", "test7", new Date(), null, testMessageId));
        smsLogCollection.add(new SmsLogs(1L, 8L, 8L, entityId8, "8", "test8", new Date(), null, testMessageId));
        return smsLogCollection;
    }

    protected List<SmsLogs> getSmsLogsWithTestPrefix() {
        List<SmsLogs> smsLogCollection = new ArrayList<>();
        smsLogCollection.add(new SmsLogs(1L, 2L, 3L, entityId1, "1", "[TEST] test1", new Date(), "TEST: Response", "TEST: Id"));
        smsLogCollection.add(new SmsLogs(1L, 2L, 3L, entityId2, "2", "[TEST] test2", new Date(), "TEST: Response", "TEST: Id"));
        smsLogCollection.add(new SmsLogs(1L, 2L, 3L, entityId3, "3", "[TEST] test3", new Date(), "TEST: Response", "TEST: Id"));
        smsLogCollection.add(new SmsLogs(1L, 2L, 3L, entityId4, "4", "[TEST] test4", new Date(), "TEST: Response", "TEST: Id"));
        smsLogCollection.add(new SmsLogs(1L, 2L, 3L, entityId5, "5", "[TEST] test5", new Date(), "TEST: Response", "TEST: Id"));
        smsLogCollection.add(new SmsLogs(1L, 2L, 3L, entityId6, "6", "[TEST] test6", new Date(), "TEST: Response", "TEST: Id"));
        smsLogCollection.add(new SmsLogs(1L, 2L, 3L, entityId7, "7", "[TEST] test7", new Date(), "TEST: Response", "TEST: Id"));
        smsLogCollection.add(new SmsLogs(1L, 2L, 3L, entityId8, "8", "[TEST] test8", new Date(), "TEST: Response", "TEST: Id"));
        return smsLogCollection;
    }
}

package tests.vendors;


import com.everwell.ins.enums.Language;
import com.everwell.ins.exceptions.NotImplementedException;
import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.exceptions.VendorException;
import com.everwell.ins.models.db.TemplateMap;
import com.everwell.ins.models.dto.SmsDto;
import com.everwell.ins.models.dto.VendorResponseDto;
import com.everwell.ins.models.dto.vendorConfigs.NICConfigDto;
import com.everwell.ins.models.dto.vendorCreds.UserNamePinDto;
import com.everwell.ins.models.http.requests.SmsRequest;
import com.everwell.ins.services.TemplateMapService;
import com.everwell.ins.vendors.SmsNICHandler;
import org.asynchttpclient.*;
import org.asynchttpclient.netty.NettyResponseFuture;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import java.util.List;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@SpringBootTest
@TestPropertySource(properties = "ins.is.mock = true")
public class SmsNICHandlerTest extends SmsHandlerTest {

    @InjectMocks
    private SmsNICHandler smsNICHandler;

    @Mock
    private NICConfigDto config;

    @Mock
    private UserNamePinDto creds;

    @Mock
    private TemplateMapService templateMapService;

    @Value("${ins.is.mock}")
    private Boolean isMock;

    @Test
    public void testSetVendorParams() {
        Long vendorId = 1L;
        when(vendorService.getVendorConfig(any(), any())).thenReturn(null);
        when(vendorService.getVendorCredentials(any(), any())).thenReturn(null);

        smsNICHandler.setVendorParams(vendorId);
        verify(vendorService, Mockito.times(1)).getVendorConfig(any(), any());
        verify(vendorService, Mockito.times(1)).getVendorCredentials(any(), any());
    }

    @Test
    public void testGetBatchSize() {
        Integer size = 5;
        when(config.getBulkSmsLimit()).thenReturn("5");

        Integer batchSize1 = smsNICHandler.getBatchSize(true);
        assertEquals(size, batchSize1);

        size = 1;
        Integer batchSize2 = smsNICHandler.getBatchSize(false);
        assertEquals(size, batchSize2);
    }

    @Test
    public void testGetMessageContent() {
        String message = "Entwickeln Sie mit Vergnügen";

        String language1 = smsNICHandler.getMessageContent(message, Language.NON_UNICODE);
        assertEquals(message, language1);

        String language2 = smsNICHandler.getMessageContent(message, Language.UNICODE);
        assertEquals(message, language2);
    }

    @Test
    public void testGetVendorTemplateId() {
        Long templateId = 1L;
        String vendorTemplateId = "1L";
        TemplateMap templateMap = new TemplateMap(templateId, vendorTemplateId);
        when(templateMapService.getTemplateMap(any())).thenReturn(templateMap);

        String vendorTemplate = smsNICHandler.getVendorTemplateId(templateId);
        assertEquals(vendorTemplateId, vendorTemplate);
    }

    @Test
    public void testCreateBatches() {
        Integer size = 2;
        Long vendorId = 1L;
        String text = "Test";
        List<SmsDto> smsDtos = getSmsDtos();
        SmsRequest smsRequest = new SmsRequest(text, false, smsDtos, vendorId, null, null, null);
        when(config.getBulkSmsLimit()).thenReturn("2");

        List<List<SmsDto>> smsDtoList1 = smsNICHandler.createBatches(smsRequest);
        assertEquals(smsDtos.size(), smsDtoList1.size());

        smsRequest.setIsMessageCommon(true);
        List<List<SmsDto>> smsDtoList2 = smsNICHandler.createBatches(smsRequest);
        assertEquals(smsDtos.size() / size, smsDtoList2.size());

    }

//    @PrepareForTest({AsyncHttpClient.class, NettyResponseFuture.class, Dsl.class})
//    @Test
//    public void testVendorCall() throws InterruptedException, ExecutionException {
//        List<SmsDto> persons = getSmsDtos();
//        mockStatic(Dsl.class);
//        DefaultAsyncHttpClient client = mock(DefaultAsyncHttpClient.class);
//        Response response = mock(Response.class);
//        NettyResponseFuture<Response> future = mock(NettyResponseFuture.class);
//        String language = "English";
//        String requestId = "1111914721121631782146";
//        String testApiResponseNic = "Message Accepted for Request ID=" + requestId + "~";
//        Long templateId = 1L;
//        String vendorTemplateId = "1L";
//        TemplateMap templateMap = new TemplateMap(templateId, vendorTemplateId);
//
//        when(templateMapService.getTemplateMap(any())).thenReturn(templateMap);
//        when(creds.getUrl()).thenReturn(testUrl);
//        when(config.getLanguage()).thenReturn(language);
//        when(Dsl.asyncHttpClient()).thenReturn(client);
//        when(client.executeRequest(any(Request.class))).thenReturn(future);
//        when(future.get()).thenReturn(response);
//        when(response.getResponseBody()).thenReturn(testApiResponseNic);
//
//        VendorResponseDto expectedDto = new VendorResponseDto();
//        expectedDto.setApiResponse(testApiResponseNic);
//        expectedDto.setMessageId(requestId);
//        VendorResponseDto responseDto = smsNICHandler.vendorCall(persons, Language.NON_UNICODE, templateId);
//        assertEquals(expectedDto.getMessageId(), responseDto.getMessageId());
//        assertEquals(expectedDto.getApiResponse(), responseDto.getApiResponse());
//    }

    @PrepareForTest({AsyncHttpClient.class, NettyResponseFuture.class, Dsl.class})
    @Test(expected = VendorException.class)
    public void testVendorCallException() throws InterruptedException, ExecutionException {
        List<SmsDto> persons = getSmsDtos();
        mockStatic(Dsl.class);
        DefaultAsyncHttpClient client = mock(DefaultAsyncHttpClient.class);
        NettyResponseFuture<Response> future = mock(NettyResponseFuture.class);
        String language = "English";

        when(creds.getUrl()).thenReturn(testUrl);
        when(config.getLanguage()).thenReturn(language);
        when(Dsl.asyncHttpClient()).thenReturn(client);
        when(client.executeRequest(any(Request.class))).thenReturn(future);
        when(future.get()).thenThrow(InterruptedException.class);

        smsNICHandler.vendorCall(persons, Language.NON_UNICODE, null);
    }

    @Test(expected = ValidationException.class)
    public void testVendorCallNullException() {
        List<SmsDto> persons = null;
        smsNICHandler.vendorCall(persons, Language.NON_UNICODE, null);
    }

    @Test(expected = NotImplementedException.class)
    public void testGetLanguageMapping() {
        smsNICHandler.getLanguageMapping(Language.NON_UNICODE);
    }
}

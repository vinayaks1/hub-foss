package tests.vendors;

import com.everwell.ins.enums.Language;
import com.everwell.ins.enums.SmsGateway;
import com.everwell.ins.models.dto.SmsDto;
import com.everwell.ins.models.dto.VendorResponseDto;
import com.everwell.ins.vendors.SmsHandler;

import java.util.List;

public class MockSmsHandler extends SmsHandler {

    @Override
    public SmsGateway getVendorGatewayEnumerator() {
        return null;
    }

    @Override
    public void setVendorParams(Long vendorId) { }

    @Override
    public VendorResponseDto vendorCall(List<SmsDto> persons, Language language, Long templateId) {
        return null;
    }

    @Override
    public String getLanguageMapping(Language language) {
        return null;
    }

}

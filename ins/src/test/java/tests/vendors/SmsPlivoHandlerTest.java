package tests.vendors;


import com.everwell.ins.enums.Language;
import com.everwell.ins.exceptions.NotImplementedException;
import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.exceptions.VendorException;
import com.everwell.ins.models.dto.SmsDto;
import com.everwell.ins.models.dto.VendorResponseDto;
import com.everwell.ins.models.dto.vendorConfigs.PlivoConfigDto;
import com.everwell.ins.models.dto.vendorCreds.AuthIdAuthTokenDto;
import com.everwell.ins.vendors.SmsPlivoHandler;
import com.plivo.api.Plivo;
import com.plivo.api.exceptions.PlivoRestException;
import com.plivo.api.models.message.Message;
import com.plivo.api.models.message.MessageCreateResponse;
import com.plivo.api.models.message.MessageCreator;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@PrepareForTest({Plivo.class, Message.class})
public class SmsPlivoHandlerTest extends SmsHandlerTest {

    @InjectMocks
    private SmsPlivoHandler smsPlivoHandler;

    @Mock
    private PlivoConfigDto config;

    @Mock
    private AuthIdAuthTokenDto creds;

    @Mock
    MessageCreateResponse response;

    @Mock
    MessageCreator mockMessageCreator;

    @Test
    public void testSetVendorParams() {
        Long vendorId = 1L;
        when(vendorService.getVendorConfig(any(), any())).thenReturn(null);
        when(vendorService.getVendorCredentials(any(), any())).thenReturn(null);

        smsPlivoHandler.setVendorParams(vendorId);
        verify(vendorService, Mockito.times(1)).getVendorConfig(any(), any());
        verify(vendorService, Mockito.times(1)).getVendorCredentials(any(), any());
    }

    @Test
    public void testGetBatchSize() {
        Integer size = 5;
        when(config.getBulkSmsLimit()).thenReturn("5");

        Integer batchSize1 = smsPlivoHandler.getBatchSize(true);
        assertEquals(size, batchSize1);

        size = 1;
        Integer batchSize2 = smsPlivoHandler.getBatchSize(false);
        assertEquals(size, batchSize2);
    }

    @Test(expected = NotImplementedException.class)
    public void testGetLanguageMapping() {
        smsPlivoHandler.getLanguageMapping(Language.NON_UNICODE);
    }

    @Test
    public void testVendorCall() throws IOException, PlivoRestException {
        List<SmsDto> persons = getSmsDtos();
        List<String> messageUuid = new ArrayList<>();
        messageUuid.add(testMessageId);
        String phone = persons.stream().map(SmsDto::getPhone)
                .collect(Collectors.joining("<"));

        mockStatic(Plivo.class);
        mockStatic(Message.class);

        when(creds.getSrcNumber()).thenReturn(testString);
        when(Message.creator(testString, Collections.singletonList(phone), persons.get(0).getMessage())).thenReturn(mockMessageCreator);
        when(mockMessageCreator.url(any())).thenReturn(mockMessageCreator);
        when(mockMessageCreator.create()).thenReturn(response);
        when(response.getApiId()).thenReturn(testApiResponse);
        when(response.getMessageUuid()).thenReturn(messageUuid);
        when(config.getReconciliation()).thenReturn("");

        ReflectionTestUtils.setField(smsPlivoHandler, "serverPort", "");
        smsPlivoHandler.setBaseUrl();
        VendorResponseDto expectedDto = new VendorResponseDto(testApiResponse, testMessageId);
        VendorResponseDto responseDto = smsPlivoHandler.vendorCall(persons, Language.NON_UNICODE, null);
        assertEquals(expectedDto.getMessageId(), responseDto.getMessageId());
        assertEquals(expectedDto.getApiResponse(), responseDto.getApiResponse());
    }

    @Test(expected = VendorException.class)
    public void testVendorCallException() throws IOException, PlivoRestException {
        List<SmsDto> persons = getSmsDtos();
        String phone = persons.stream().map(SmsDto::getPhone)
                .collect(Collectors.joining("<"));

        mockStatic(Plivo.class);
        mockStatic(Message.class);

        ReflectionTestUtils.setField(smsPlivoHandler, "serverPort", "");
        smsPlivoHandler.setBaseUrl();
        when(creds.getSrcNumber()).thenReturn(testString);
        when(config.getReconciliation()).thenReturn("");
        when(Message.creator(testString, Collections.singletonList(phone), persons.get(0).getMessage())).thenReturn(mockMessageCreator);
        when(mockMessageCreator.url(any())).thenReturn(mockMessageCreator);
        when(mockMessageCreator.create()).thenThrow(IOException.class);

        smsPlivoHandler.vendorCall(persons, Language.NON_UNICODE, null);
    }

    @Test(expected = ValidationException.class)
    public void testVendorCallNullException() {
        List<SmsDto> persons = null;
        smsPlivoHandler.vendorCall(persons, Language.NON_UNICODE, null);
    }
}

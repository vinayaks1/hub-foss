package tests.controllers;

import com.everwell.ins.controllers.SmsController;
import com.everwell.ins.exceptions.CustomExceptionHandler;
import com.everwell.ins.models.db.Client;
import com.everwell.ins.models.db.Trigger;
import com.everwell.ins.models.dto.SmsDetailedRequestDto;
import com.everwell.ins.models.dto.SmsDto;
import com.everwell.ins.models.dto.SmsRequestDto;
import com.everwell.ins.models.dto.SmsTemplateDto;
import com.everwell.ins.models.http.requests.SmsDetailedRequest;
import com.everwell.ins.models.http.requests.SmsRequest;
import com.everwell.ins.models.http.requests.SmsTemplateRequest;
import com.everwell.ins.services.*;
import com.everwell.ins.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import tests.BaseTest;

import static org.mockito.ArgumentMatchers.*;

import java.util.*;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class SmsControllerTests extends BaseTest {

    private MockMvc mockMvc;

    @Mock
    private SmsService smsService;

    @Mock
    private TemplateService templateService;

    @Mock
    private TriggerService triggerService;

    @Mock
    private EngagementService engagementService;

    @InjectMocks
    private SmsController smsController;

    @Mock
    private ClientService clientService;

    private static Long id = 1L;
    private static Long vendorId = 5L;
    private static String entityId = "1";
    private static String test = "Test";
    private static String message = "Test";
    private static String phone = "9705197814";
    private static String date = "2019-08-31 18:30:00";
    private static Long client = 29L;
    private static Long client2 = 63L;

    private static final String MESSAGE_SMS_REQUEST_ACCEPTED = "sms request accepted";
    private static final String MESSAGE_SMS_REQUEST_PARTIALLY_FAILED = "sms request partially failed";
    private static final String MESSAGE_SMS_REQUEST_FAILED = "sms request failed";
    private static final String MESSAGE_VENDOR_ID_REQUIRED = "vendor id is required";
    private static final String MESSAGE_TRIGGER_ID_REQUIRED = "trigger id is required";
    private static final String MESSAGE_TEMPLATE_ID_REQUIRED = "template id is required";
    private static final String MESSAGE_DEFAULT_CONSENT_REQUIRED = "default consent is required";
    private static final String MESSAGE_IS_MANDATORY_REQUIRED = "is Mandatory required";
    private static final String MESSAGE_IS_DEFAULT_TIME_REQUIRED = "is Default Time required";
    private static final String MESSAGE_PERSON_LIST_SHOULD_NOT_BE_EMPTY = "personList should not be empty";
    private static final String MESSAGE_IS_MESSAGE_COMMON_REQUIRED = "is Message Common required";
    private static final String MESSAGE_MESSAGE_REQUIRED = "message is required";
    private static final String MESSAGE_TIME_OF_SMS_REQUIRED = "time of sms is required";

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(smsController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
        when(clientService.getCurrentClient()).thenReturn(new Client(29L, "test", null, null));
    }

    @Test
    public void testSendBulkSuccess() throws Exception {
        String uri = "/v1/sms/bulk";
        List<SmsDto> smsDto = new ArrayList<>();
        smsDto.add(new SmsDto(entityId, phone, message));
        SmsRequest smsRequest = new SmsRequest(message, true, smsDto, vendorId, null, null, null);

        doNothing().when(smsService).sendSms(any());

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(smsRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isAccepted())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_SMS_REQUEST_ACCEPTED));
    }

    @Test
    public void testSendBulkWithoutVendorId() throws Exception {
        String uri = "/v1/sms/bulk";
        List<SmsDto> smsDto = new ArrayList<>();
        smsDto.add(new SmsDto(entityId, phone, message));
        SmsRequest smsRequest = new SmsRequest(message, true, smsDto, null, null, null, null);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(smsRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_VENDOR_ID_REQUIRED));
    }

    @Test
    public void testSendBulkWithoutPersonList() throws Exception {
        String uri = "/v1/sms/bulk";
        List<SmsDto> smsDto = new ArrayList<>();
        SmsRequest smsRequest = new SmsRequest(message, true, smsDto, vendorId, null, null, null);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(smsRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_PERSON_LIST_SHOULD_NOT_BE_EMPTY));
    }

    @Test
    public void testSendBulkWithoutIsMessageCommon() throws Exception {
        String uri = "/v1/sms/bulk";
        List<SmsDto> smsDto = new ArrayList<>();
        smsDto.add(new SmsDto(entityId, phone, message));
        SmsRequest smsRequest = new SmsRequest(message, null, smsDto, vendorId, null, null, null);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(smsRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_IS_MESSAGE_COMMON_REQUIRED));
    }

    @Test
    public void testSendBulkWithoutMessage() throws Exception {
        String uri = "/v1/sms/bulk";
        List<SmsDto> smsDto = new ArrayList<>();
        smsDto.add(new SmsDto(entityId, phone, message));
        SmsRequest smsRequest = new SmsRequest(null, true, smsDto, vendorId, null, null, null);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(smsRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_MESSAGE_REQUIRED));
    }

    @Test
    public void testSendBulkWithTemplateIdWithoutPersonList() throws Exception {
        String uri = "/v2/sms/bulk";
        SmsTemplateRequest smsTemplateRequest = new SmsTemplateRequest(id, null, vendorId, null);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(smsTemplateRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_PERSON_LIST_SHOULD_NOT_BE_EMPTY));
    }

    @Test
    public void testSendBulkWithTemplateIdWithoutVendorId() throws Exception {
        String uri = "/v2/sms/bulk";
        List<SmsTemplateDto> smsTemplateDto = new ArrayList<>();
        smsTemplateDto.add(new SmsTemplateDto(entityId, phone, getParameters()));
        SmsTemplateRequest smsTemplateRequest = new SmsTemplateRequest(id, smsTemplateDto, null, null);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(smsTemplateRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_VENDOR_ID_REQUIRED));
    }

    @Test
    public void testSendBulkWithTemplateIdWithoutTemplateId() throws Exception {
        String uri = "/v2/sms/bulk";
        List<SmsTemplateDto> smsTemplateDto = new ArrayList<>();
        smsTemplateDto.add(new SmsTemplateDto(entityId, phone, getParameters()));
        SmsTemplateRequest smsTemplateRequest = new SmsTemplateRequest(null, smsTemplateDto, vendorId, 0L);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(smsTemplateRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_TEMPLATE_ID_REQUIRED));
    }

    @Test
    public void testSendBulkWithTemplateIdSuccess() throws Exception {
        String uri = "/v2/sms/bulk";
        List<SmsTemplateDto> smsTemplateDto = new ArrayList<>();
        smsTemplateDto.add(new SmsTemplateDto(entityId, phone, getParameters()));
        SmsTemplateRequest smsTemplateRequest = new SmsTemplateRequest(null, smsTemplateDto, vendorId, id);
        List<SmsDto> smsDto = new ArrayList<>();
        Trigger trigger = new Trigger(id, test, test, test, id);
        smsDto.add(new SmsDto(entityId, phone, message));
        SmsRequest smsRequest = new SmsRequest(message, true, smsDto, vendorId, id, id, null);
        SmsRequestDto smsRequestDto = new SmsRequestDto(smsRequest, null);

        when(templateService.createSms(any())).thenReturn(smsRequestDto);
        when(triggerService.getTrigger(any())).thenReturn(trigger);
        doNothing().when(smsService).sendSms(any());

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(smsTemplateRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isAccepted())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_SMS_REQUEST_ACCEPTED));
    }

    @Test
    public void testSendBulkWithTemplateIdFail() throws Exception {
        String uri = "/v2/sms/bulk";
        List<SmsTemplateDto> smsTemplateDto = new ArrayList<>();
        smsTemplateDto.add(new SmsTemplateDto(entityId, phone, getParameters()));
        SmsTemplateRequest smsTemplateRequest = new SmsTemplateRequest(id, smsTemplateDto, vendorId, null);
        SmsRequest smsRequest = new SmsRequest(message, true, null, vendorId, id, id, null);
        SmsRequestDto smsRequestDto = new SmsRequestDto(smsRequest, smsTemplateDto);

        when(templateService.createSms(any())).thenReturn(smsRequestDto);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(smsTemplateRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_SMS_REQUEST_FAILED));
    }

    @Test
    public void testSendBulkWithTemplateIdPartialSucess() throws Exception {
        String uri = "/v2/sms/bulk";
        List<SmsTemplateDto> smsTemplateDto = new ArrayList<>();
        smsTemplateDto.add(new SmsTemplateDto(entityId, phone, getParameters()));
        smsTemplateDto.add(new SmsTemplateDto(entityId, phone, getParameters()));
        SmsTemplateRequest smsTemplateRequest = new SmsTemplateRequest(id, smsTemplateDto, vendorId, null, false, true);
        List<SmsDto> smsDto = new ArrayList<>();
        smsDto.add(new SmsDto(entityId, phone, message));
        SmsRequest smsRequest = new SmsRequest(message, true, smsDto, vendorId, id, id, null);
        List<SmsTemplateDto> smsTemplateResponseDto = new ArrayList<>();
        smsTemplateResponseDto.add(new SmsTemplateDto(entityId, phone, getParameters()));
        SmsRequestDto smsRequestDto = new SmsRequestDto(smsRequest, smsTemplateResponseDto);

        when(templateService.createSms(any())).thenReturn(smsRequestDto);
        when(engagementService.modifyList(any(), anyLong())).thenReturn(smsRequestDto);
        doNothing().when(smsService).sendSms(any());

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(smsTemplateRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isAccepted())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_SMS_REQUEST_PARTIALLY_FAILED));
    }

    @Test
    public void testSendBulkSmsSuccess() throws Exception {
        String uri = "/v3/sms/bulk";
        List<SmsTemplateDto> smsTemplateDto = new ArrayList<>();
        smsTemplateDto.add(new SmsTemplateDto(entityId, phone, getParameters()));
        List<Long> templateIds = new ArrayList<>();
        templateIds.add(id);
        SmsDetailedRequest smsDetailedRequest = new SmsDetailedRequest(id, templateIds, smsTemplateDto, id, id, true, true, true, date, null, null, client);
        List<SmsDto> smsDto = new ArrayList<>();
        smsDto.add(new SmsDto(entityId, phone, message));
        SmsRequest smsRequest = new SmsRequest(message, true, smsDto, vendorId, id, id, null);
        List<SmsRequest> smsRequests = new ArrayList<>();
        smsRequests.add(smsRequest);
        SmsDetailedRequestDto smsDetailedRequestDto = new SmsDetailedRequestDto(smsRequests, null);

        when(engagementService.filterSmsList(any(), anyLong())).thenReturn(smsDetailedRequest);
        when(templateService.constructSms(any())).thenReturn(smsDetailedRequestDto);
        doNothing().when(smsService).sendBulkSms(any());

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(smsDetailedRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isAccepted())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_SMS_REQUEST_ACCEPTED));
    }

    @Test
    public void testSendBulkSmsFail() throws Exception {
        String uri = "/v3/sms/bulk";
        List<SmsTemplateDto> smsTemplateDto = new ArrayList<>();
        smsTemplateDto.add(new SmsTemplateDto(entityId, phone, getParameters()));
        List<Long> templateIds = new ArrayList<>();
        templateIds.add(id);
        SmsDetailedRequest smsDetailedRequest = new SmsDetailedRequest(id, templateIds, smsTemplateDto, id, id, true, true, true, date, null, null, client2);
        List<SmsDto> smsDto = new ArrayList<>();
        smsDto.add(new SmsDto(entityId, phone, message));
        SmsRequest smsRequest = new SmsRequest(message, true, smsDto, vendorId, id, id, null);
        List<SmsRequest> smsRequests = new ArrayList<>();
        smsRequests.add(smsRequest);
        SmsDetailedRequestDto smsDetailedRequestDto = new SmsDetailedRequestDto(smsRequests, smsTemplateDto);

        when(engagementService.filterSmsList(any(), anyLong())).thenReturn(smsDetailedRequest);
        when(templateService.constructSms(any())).thenReturn(smsDetailedRequestDto);
        doNothing().when(smsService).sendBulkSms(any());

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(smsDetailedRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_SMS_REQUEST_FAILED));
    }

    @Test
    public void testSendBulkSmsPartialSuccess() throws Exception {
        String uri = "/v3/sms/bulk";
        List<SmsTemplateDto> smsTemplateDtos = new ArrayList<>();
        SmsTemplateDto smsTemplateDto = new SmsTemplateDto(entityId, phone, getParameters());
        smsTemplateDtos.add(smsTemplateDto);
        smsTemplateDtos.add(smsTemplateDto);
        List<Long> templateIds = new ArrayList<>();
        templateIds.add(id);
        SmsDetailedRequest smsDetailedRequest = new SmsDetailedRequest(id, templateIds, smsTemplateDtos, id, id, true, true, true, date, null, null, client);
        List<SmsDto> smsDto = new ArrayList<>();
        smsDto.add(new SmsDto(entityId, phone, message));
        SmsRequest smsRequest = new SmsRequest(message, true, null, vendorId, id, id, null);
        List<SmsRequest> smsRequests = new ArrayList<>();
        smsRequests.add(smsRequest);
        List<SmsTemplateDto> failedSmsRequests = new ArrayList<>();
        failedSmsRequests.add(smsTemplateDto);
        SmsDetailedRequestDto smsDetailedRequestDto = new SmsDetailedRequestDto(smsRequests, failedSmsRequests);

        when(engagementService.filterSmsList(any(), anyLong())).thenReturn(smsDetailedRequest);
        when(templateService.constructSms(any())).thenReturn(smsDetailedRequestDto);
        doNothing().when(smsService).sendBulkSms(any());

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(smsDetailedRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isAccepted())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_SMS_REQUEST_PARTIALLY_FAILED));
    }

    @Test
    public void testSendBulkSmsWithoutTemplateId() throws Exception {
        String uri = "/v3/sms/bulk";
        List<SmsTemplateDto> smsTemplateDto = new ArrayList<>();
        smsTemplateDto.add(new SmsTemplateDto(entityId, phone, getParameters()));
        List<Long> templateIds = new ArrayList<>();
        templateIds.add(id);
        SmsDetailedRequest smsDetailedRequest = new SmsDetailedRequest(null, templateIds, smsTemplateDto, id, id, true, true, true, date, null, null, client);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(smsDetailedRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_TEMPLATE_ID_REQUIRED));
    }

    @Test
    public void testSendBulkSmsWithoutPersonList() throws Exception {
        String uri = "/v3/sms/bulk";
        List<Long> templateIds = new ArrayList<>();
        templateIds.add(id);
        SmsDetailedRequest smsDetailedRequest = new SmsDetailedRequest(id, templateIds, null, id, id, true, true, true, date, null, null, client);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(smsDetailedRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_PERSON_LIST_SHOULD_NOT_BE_EMPTY));
    }

    @Test
    public void testSendBulkSmsWithoutVendorId() throws Exception {
        String uri = "/v3/sms/bulk";
        List<SmsTemplateDto> smsTemplateDto = new ArrayList<>();
        smsTemplateDto.add(new SmsTemplateDto(entityId, phone, getParameters()));
        List<Long> templateIds = new ArrayList<>();
        templateIds.add(id);
        SmsDetailedRequest smsDetailedRequest = new SmsDetailedRequest(id, templateIds, smsTemplateDto, null, id, true, true, true, date, null, null, client);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(smsDetailedRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_VENDOR_ID_REQUIRED));
    }

    @Test
    public void testSendBulkSmsWithoutTriggerId() throws Exception {
        String uri = "/v3/sms/bulk";
        List<SmsTemplateDto> smsTemplateDto = new ArrayList<>();
        smsTemplateDto.add(new SmsTemplateDto(entityId, phone, getParameters()));
        List<Long> templateIds = new ArrayList<>();
        templateIds.add(id);
        SmsDetailedRequest smsDetailedRequest = new SmsDetailedRequest(id, templateIds, smsTemplateDto, id, null, true, true, true, date, null, null, client);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(smsDetailedRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_TRIGGER_ID_REQUIRED));
    }

    @Test
    public void testSendBulkSmsWithoutIsMandatory() throws Exception {
        String uri = "/v3/sms/bulk";
        List<SmsTemplateDto> smsTemplateDto = new ArrayList<>();
        smsTemplateDto.add(new SmsTemplateDto(entityId, phone, getParameters()));
        List<Long> templateIds = new ArrayList<>();
        templateIds.add(id);
        SmsDetailedRequest smsDetailedRequest = new SmsDetailedRequest(id, templateIds, smsTemplateDto, id, id, null, true, true, date, null, null, client);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(smsDetailedRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_IS_MANDATORY_REQUIRED));
    }

    @Test
    public void testSendBulkSmsWithoutDefaultConsent() throws Exception {
        String uri = "/v3/sms/bulk";
        List<SmsTemplateDto> smsTemplateDto = new ArrayList<>();
        smsTemplateDto.add(new SmsTemplateDto(entityId, phone, getParameters()));
        List<Long> templateIds = new ArrayList<>();
        templateIds.add(id);
        SmsDetailedRequest smsDetailedRequest = new SmsDetailedRequest(id, templateIds, smsTemplateDto, id, id, true, true, null, date, null, null, client);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(smsDetailedRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_DEFAULT_CONSENT_REQUIRED));
    }

    @Test
    public void testSendBulkSmsWithoutIsDefaultTime() throws Exception {
        String uri = "/v3/sms/bulk";
        List<SmsTemplateDto> smsTemplateDto = new ArrayList<>();
        smsTemplateDto.add(new SmsTemplateDto(entityId, phone, getParameters()));
        List<Long> templateIds = new ArrayList<>();
        templateIds.add(id);
        SmsDetailedRequest smsDetailedRequest = new SmsDetailedRequest(id, templateIds, smsTemplateDto, id, id, true, null, true, date, null, null, client);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(smsDetailedRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_IS_DEFAULT_TIME_REQUIRED));
    }

    @Test
    public void testSendBulkSmsWithoutTimeOfSms() throws Exception {
        String uri = "/v3/sms/bulk";
        List<SmsTemplateDto> smsTemplateDto = new ArrayList<>();
        smsTemplateDto.add(new SmsTemplateDto(entityId, phone, getParameters()));
        List<Long> templateIds = new ArrayList<>();
        templateIds.add(id);
        SmsDetailedRequest smsDetailedRequest = new SmsDetailedRequest(id, templateIds, smsTemplateDto, id, id, true, false, true, null, null, null, client);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(smsDetailedRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_TIME_OF_SMS_REQUIRED));
    }

    private Map<String, String> getParameters() {
        Map<String, String> entityParametersMap = new HashMap<>();
        entityParametersMap.put("Id", "1");
        entityParametersMap.put("Name", "Test");
        return entityParametersMap;
    }
}

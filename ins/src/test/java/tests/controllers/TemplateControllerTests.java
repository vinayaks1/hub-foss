package tests.controllers;

import com.everwell.ins.controllers.TemplateController;
import com.everwell.ins.exceptions.CustomExceptionHandler;
import com.everwell.ins.models.db.Language;
import com.everwell.ins.models.db.Template;
import com.everwell.ins.models.db.Type;
import com.everwell.ins.models.http.requests.TemplateRequest;
import com.everwell.ins.services.LanguageService;
import com.everwell.ins.services.TemplateService;
import com.everwell.ins.services.TypeService;
import com.everwell.ins.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import tests.BaseTest;


import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TemplateControllerTests extends BaseTest {

    private MockMvc mockMvc;

    @Mock
    private TemplateService templateService;

    @Mock
    private TypeService typeService;

    @Mock
    private LanguageService languageService;

    @InjectMocks
    private TemplateController templateController;

    private static Long id = 1L;
    private static String content = "Test";

    private static final String MESSAGE_LANGUAGEID_REQUIRED = "language id is required";
    private static final String MESSAGE_TYPEID_REQUIRED = "type id is required";
    private static final String MESSAGE_CONTENT_SHOULD_NOT_BE_EMPTY = "content should not be empty";


    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(templateController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void testSaveTemplate() throws Exception {
        String uri = "/v1/template";
        TemplateRequest templateRequest = new TemplateRequest(content, id, id, null, false);

        when(templateService.saveTemplate(any())).thenReturn(id);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(templateRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isAccepted())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value(id));
    }

    @Test
    public void testSaveTemplateWithoutLanguageId() throws Exception {
        String uri = "/v1/template";
        TemplateRequest templateRequest = new TemplateRequest(content, null, id, null, false);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(templateRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_LANGUAGEID_REQUIRED));
    }

    @Test
    public void testSaveTemplateWithoutTypeId() throws Exception {
        String uri = "/v1/template";
        TemplateRequest templateRequest = new TemplateRequest(content, id, null, null, false);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(templateRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_TYPEID_REQUIRED));
    }

    @Test
    public void testSaveTemplateWithoutContent() throws Exception {
        String uri = "/v1/template";
        TemplateRequest templateRequest = new TemplateRequest(null, id, id, null, false);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(templateRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_CONTENT_SHOULD_NOT_BE_EMPTY));
    }

    @Test
    public void testGetAllTemplates() throws Exception {
        String uri = "/v1/template";
        Type type = new Type(id, content);
        Language language = new Language(id, content);
        Template template = new Template(id, content, content, id, id, false);
        List<Template> templateList = new ArrayList<>();
        templateList.add(template);

        when(templateService.getAllTemplates()).thenReturn(templateList);
        when(typeService.getType(any())).thenReturn(type);
        when(languageService.getLanguage(any())).thenReturn(language);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].templateId").value(id));
    }

    @Test
    public void testGetTemplate() throws Exception {
        String uri = "/v1/template/" + id;
        Type type = new Type(id, content);
        Language language = new Language(id, content);
        Template template = new Template(id, content, content, id, id, false);

        when(templateService.getTemplate(any())).thenReturn(template);
        when(typeService.getType(any())).thenReturn(type);
        when(languageService.getLanguage(any())).thenReturn(language);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.templateId").value(id));
    }


}

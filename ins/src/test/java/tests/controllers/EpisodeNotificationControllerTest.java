package tests.controllers;

import com.everwell.ins.controllers.EpisodeNotificationController;
import com.everwell.ins.exceptions.CustomExceptionHandler;
import com.everwell.ins.models.db.Client;
import com.everwell.ins.models.http.requests.EpisodeNotificationSearchRequest;
import com.everwell.ins.models.http.requests.UpdateEpisodeNotificationRequest;
import com.everwell.ins.models.http.responses.EpisodeNotificationResponse;
import com.everwell.ins.services.ClientService;
import com.everwell.ins.services.EpisodeNotificationService;
import com.everwell.ins.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import tests.BaseTest;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class EpisodeNotificationControllerTest extends BaseTest {

    private MockMvc mockMvc;

    @Mock
    private EpisodeNotificationService episodeNotificationService;

    @Mock
    private ClientService clientService;

    @InjectMocks
    private EpisodeNotificationController episodeNotificationController;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(episodeNotificationController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }


    @Test
    public void getEpisodeNotificationTestSuccess() throws Exception {
        String uri = "/v1/notification/bulk";
        EpisodeNotificationSearchRequest episodeNotificationSearchRequest = new EpisodeNotificationSearchRequest(Collections.singletonList(1L));
        Client client = new Client(171L, "test", "test", null);
        EpisodeNotificationResponse episodeNotificationResponse = new EpisodeNotificationResponse();
        Mockito.when(clientService.getCurrentClient()).thenReturn(client);
        Mockito.when(episodeNotificationService.getNotificationForEpisodes(any(), eq(171L))).thenReturn(Collections.singletonList(episodeNotificationResponse));

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(episodeNotificationSearchRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk());
    }

    @Test
    public void getEpisodeNotificationTestEmptyEpisodeIdList() throws Exception {
        String uri = "/v1/notification/bulk";
        EpisodeNotificationSearchRequest episodeNotificationSearchRequest = new EpisodeNotificationSearchRequest();
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(episodeNotificationSearchRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateNotificationSuccess() throws Exception {
        String uri = "/v1/notification";
        UpdateEpisodeNotificationRequest updateEpisodeNotificationRequest = new UpdateEpisodeNotificationRequest(1L, "accept", true);
        doNothing().when(episodeNotificationService).updateNotification(any());
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(updateEpisodeNotificationRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk());
    }

    @Test
    public void testUpdateNotificationNullId() throws Exception {
        String uri = "/v1/notification";
        UpdateEpisodeNotificationRequest updateEpisodeNotificationRequest = new UpdateEpisodeNotificationRequest(null, "accept", true);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(updateEpisodeNotificationRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateNotificationNoUpdateFields() throws Exception {
        String uri = "/v1/notification";
        UpdateEpisodeNotificationRequest updateEpisodeNotificationRequest = new UpdateEpisodeNotificationRequest(1L, null, null);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(updateEpisodeNotificationRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest());
    }
}

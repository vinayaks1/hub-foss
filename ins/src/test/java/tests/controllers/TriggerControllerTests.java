package tests.controllers;

import com.everwell.ins.controllers.TriggerController;
import com.everwell.ins.exceptions.CustomExceptionHandler;
import com.everwell.ins.models.db.Trigger;
import com.everwell.ins.services.TriggerService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import tests.BaseTest;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TriggerControllerTests extends BaseTest {

    private MockMvc mockMvc;

    @Mock
    private TriggerService triggerService;

    @InjectMocks
    private TriggerController triggerController;

    private static Long id = 1L;
    private static String test = "Test";

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(triggerController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void testGetAllTriggers() throws Exception {
        String uri = "/v1/trigger";
        Trigger trigger = new Trigger(id, test, test, test, id);
        List<Trigger> triggerList = new ArrayList<>();
        triggerList.add(trigger);

        when(triggerService.getAllTriggers()).thenReturn(triggerList);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].triggerId").value(id));
    }

    @Test
    public void testGetTrigger() throws Exception {
        String uri = "/v1/trigger/" + id;
        Trigger trigger = new Trigger(id, test, test, test, id);

        when(triggerService.getTrigger(any())).thenReturn(trigger);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.triggerId").value(id));
    }


}

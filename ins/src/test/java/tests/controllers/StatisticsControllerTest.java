package tests.controllers;

import com.everwell.ins.controllers.StatisticsController;
import com.everwell.ins.exceptions.CustomExceptionHandler;
import com.everwell.ins.models.http.requests.StatisticsRequest;
import com.everwell.ins.services.StatisticsService;
import com.everwell.ins.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import tests.BaseTest;

import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class StatisticsControllerTest extends BaseTest {

    private MockMvc mockMvc;

    @Mock
    private StatisticsService statisticsService;

    @InjectMocks
    private StatisticsController statisticsController;

    private static Long id = 1L;
    private static String date = "01-01-2020 23:15:45";
    private static Long count = 5L;

    private static final String MESSAGE_VENDORID_REQUIRED = "vendor id is required";
    private static final String MESSAGE_FROM_FILTER_REQUIRED = "from range filter is required";
    private static final String MESSAGE_TO_FILTER_REQUIRED = "to range filter is required";
    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(statisticsController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void testGetStatistics() throws Exception {
        String uri = "/v1/statistics";
        StatisticsRequest statisticsRequest = new StatisticsRequest(null, null, id, date, date);

        when(statisticsService.getNotificationsCount(any())).thenReturn(count);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(statisticsRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.count").value(count));
    }

    @Test
    public void testGetStatisticsWithoutVendorId() throws Exception {
        String uri = "/v1/statistics";
        StatisticsRequest statisticsRequest = new StatisticsRequest(null, null, null, date, date);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(statisticsRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_VENDORID_REQUIRED));
    }

    @Test
    public void testGetStatisticsWithoutFromFilter() throws Exception {
        String uri = "/v1/statistics";
        StatisticsRequest statisticsRequest = new StatisticsRequest(null, null, id, null, date);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(statisticsRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_FROM_FILTER_REQUIRED));
    }

    @Test
    public void testGetStatisticsWithoutToFilter() throws Exception {
        String uri = "/v1/statistics";
        StatisticsRequest statisticsRequest = new StatisticsRequest(null, null, id, date, null);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(statisticsRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_TO_FILTER_REQUIRED));
    }

}

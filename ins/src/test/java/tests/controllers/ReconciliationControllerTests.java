package tests.controllers;

import com.everwell.ins.analytics.SpringEventAnalytics;
import com.everwell.ins.controllers.ReconciliationController;
import com.everwell.ins.exceptions.CustomExceptionHandler;
import com.everwell.ins.models.http.requests.AfricasTalkingReconciliationRequest;
import com.everwell.ins.models.http.requests.PlivoReconciliationRequest;
import com.everwell.ins.models.http.requests.TwilioReconciliationRequest;
import com.everwell.ins.services.ReconciliationService;
import com.everwell.ins.services.VendorService;
import com.everwell.ins.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.mockito.ArgumentMatchers.*;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import tests.BaseTest;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ReconciliationControllerTests extends BaseTest {

    private MockMvc mockMvc;

    @Mock
    private ReconciliationService reconciliationService;

    @Mock
    private VendorService vendorService;

    @Mock
    private ApplicationEventPublisher applicationEventPublisher;

    @Mock
    private SpringEventAnalytics springEventAnalytics;

    @InjectMocks
    private ReconciliationController reconciliationController;

    private static String messageId = "123";
    private static String phone = "9705197814";
    private static String messageStatus = "Received";
    private static List<Long> vendorIds = Arrays.asList(1L);

    private static final String MESSAGE_VENDORID_NOT_FOUND = "vendorId not found";

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(reconciliationController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void testReconciliationTwilioVendorIdNotFound() throws Exception {
        String uri = "/v1/reconciliation/twilio";
        TwilioReconciliationRequest request = new TwilioReconciliationRequest(messageId, messageStatus, phone);

        when(vendorService.getVendorIds(any())).thenReturn(null);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                                .param("MessageSid", request.getMessageSid())
                                .param("MessageStatus", request.getMessageStatus())
                                .param("To", request.getTo())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_VENDORID_NOT_FOUND));
    }

    @Test
    public void testReconciliationPlivoVendorIdNotFound() throws Exception {
        String uri = "/v1/reconciliation/plivo";
        PlivoReconciliationRequest request = new PlivoReconciliationRequest(messageId, messageStatus, phone);

        when(vendorService.getVendorIds(any())).thenReturn(null);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(request))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_VENDORID_NOT_FOUND));
    }

    @Test
    public void testReconciliationAfricasTalkingVendorIdNotFound() throws Exception {
        String uri = "/v1/reconciliation/africasTalking";
        AfricasTalkingReconciliationRequest request = new AfricasTalkingReconciliationRequest(messageId, messageStatus, phone);

        when(vendorService.getVendorIds(any())).thenReturn(null);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                                .param("id", request.getId())
                                .param("status", request.getStatus())
                                .param("phoneNumber", request.getPhoneNumber())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_VENDORID_NOT_FOUND));
    }

    @Test
    public void testReconciliationNICVendorIdNotFound() throws Exception {
        String acknowlegementId = "15";
        String phoneNumber = "1234567890";
        String status = "success";
        String uri = "/v1/reconciliation/nic?a2wackid=" + acknowlegementId + "&mnumber=" + phoneNumber + "&a2wstatus=" + status;

        when(vendorService.getVendorIds(any())).thenReturn(null);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_VENDORID_NOT_FOUND));
    }

    @Test
    public void testReconciliationTextLocalVendorIdNotFound() throws Exception {
        String customID = "15";
        String number = "1234567890";
        String status = "success";
        String datetime = "2018-04-23 12:35:26.153";
        String uri = "/v1/reconciliation/textlocal?number=" + number + "&status=" + status + "&customID=" + customID + "&datetime=" + datetime;

        when(vendorService.getVendorIds(any())).thenReturn(null);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_VENDORID_NOT_FOUND));
    }

    @Test
    public void testReconciliationInterraVendorIdNotFound() throws Exception {
        String uri = "/v1/reconciliation/interra";
        when(vendorService.getVendorIds(any())).thenReturn(null);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_VENDORID_NOT_FOUND));
    }

    @Test
    public void testReconciliationTwilioSuccess() throws Exception {
        String uri = "/v1/reconciliation/twilio";
        Boolean statusUpdated = true;

        TwilioReconciliationRequest request = new TwilioReconciliationRequest(messageId, messageStatus, phone);

        when(vendorService.getVendorIds(any())).thenReturn(vendorIds);
        when(reconciliationService.saveStatus(any())).thenReturn(statusUpdated);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                                .param("MessageSid", request.getMessageSid())
                                .param("MessageStatus", request.getMessageStatus())
                                .param("To", request.getTo())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk());
    }

    @Test
    public void testReconciliationPlivoSuccess() throws Exception {
        String uri = "/v1/reconciliation/plivo";
        Boolean statusUpdated = true;

        PlivoReconciliationRequest request = new PlivoReconciliationRequest(messageId, messageStatus, phone);

        when(vendorService.getVendorIds(any())).thenReturn(vendorIds);
        when(reconciliationService.saveStatus(any())).thenReturn(statusUpdated);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(request))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk());
    }

    @Test
    public void testReconciliationAfricasTalkingSuccess() throws Exception {
        String uri = "/v1/reconciliation/africasTalking";
        Boolean statusUpdated = true;

        AfricasTalkingReconciliationRequest request = new AfricasTalkingReconciliationRequest(messageId, messageStatus, phone);

        when(vendorService.getVendorIds(any())).thenReturn(vendorIds);
        when(reconciliationService.saveStatus(any())).thenReturn(statusUpdated);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                                .param("id", request.getId())
                                .param("status", request.getStatus())
                                .param("phoneNumber", request.getPhoneNumber())
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk());
    }

    @Test
    public void testReconciliationNICSuccess() throws Exception {
        String acknowlegementId = "15";
        String phoneNumber = "1234567890";
        String status = "success";
        String uri = "/v1/reconciliation/nic?a2wackid=" + acknowlegementId + "&mnumber=" + phoneNumber + "&a2wstatus=" + status;
        Boolean statusUpdated = true;
        when(vendorService.getVendorIds(any())).thenReturn(vendorIds);
        when(reconciliationService.saveStatus(any())).thenReturn(statusUpdated);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").value("OK"));
    }

    @Test
    public void testReconciliationNICFailure() throws Exception {
        String acknowlegementId = "15";
        String phoneNumber = "1234567890";
        String status = "success";
        String uri = "/v1/reconciliation/nic?a2wackid=" + acknowlegementId + "&mnumber=" + phoneNumber + "&a2wstatus=" + status;
        Boolean statusUpdated = false;
        when(vendorService.getVendorIds(any())).thenReturn(vendorIds);
        when(reconciliationService.saveStatus(any())).thenReturn(statusUpdated);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isInternalServerError())
                .andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"));
    }

    @Test
    public void testReconciliationTextLocalSuccess() throws Exception {
        String customID = "15";
        String number = "1234567890";
        String status = "success";
        String datetime = "2018-04-23 12:35:26.153";
        String uri = "/v1/reconciliation/textlocal?number=" + number + "&status=" + status + "&customID=" + customID + "&datetime=" + datetime;
        Boolean statusUpdated = true;

        when(vendorService.getVendorIds(any())).thenReturn(vendorIds);
        when(reconciliationService.saveStatus(any())).thenReturn(statusUpdated);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk());
    }

    @Test
    public void testReconciliationInterraSuccess() throws Exception {
        String uri = "/v1/reconciliation/interra";

        when(vendorService.getVendorIds(any())).thenReturn(vendorIds);
        doNothing().when(reconciliationService).fetchStatusInterra(any());

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk());
    }

}

package tests.controllers;

import com.everwell.ins.controllers.LanguageController;
import com.everwell.ins.enums.Language;
import com.everwell.ins.exceptions.CustomExceptionHandler;
import com.everwell.ins.models.dto.LanguageMapping;
import com.everwell.ins.models.http.requests.LanguageMappingRequest;
import com.everwell.ins.models.http.responses.LanguageResponse;
import com.everwell.ins.services.LanguageService;
import com.everwell.ins.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import tests.BaseTest;

import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class LanguageControllerTests extends BaseTest {

    private MockMvc mockMvc;

    @Mock
    private LanguageService languageService;

    @InjectMocks
    private LanguageController languageController;

    private static String english = "English";
    private static String bangla = "Bangla";
    private static final String MESSAGE_LANGUAGE_IDS_REQUIRED = "language ids are required";

    private List<LanguageMapping> languageMappings() {
        List<LanguageMapping> languageMapping = new ArrayList<>();
        languageMapping.add(new LanguageMapping(1L, english));
        languageMapping.add(new LanguageMapping(2L, bangla));
        return languageMapping;
    }

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(languageController)
                .setControllerAdvice(new CustomExceptionHandler())
                .build();
    }

    @Test
    public void testGetLanguageMappings() throws Exception {
        String uri = "/v1/language";
        List<Long> languageIds = new ArrayList<>();
        languageIds.add(1L);
        languageIds.add(2L);
        LanguageMappingRequest languageMappingRequest = new LanguageMappingRequest(languageIds);

        when(languageService.getLanguageMappings(any())).thenReturn(languageMappings());

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(languageMappingRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.languageMappings[0].language").value(english))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.languageMappings[1].language").value(bangla));
        verify(languageService, Mockito.times(1)).getLanguageMappings(any());
    }

    @Test
    public void testGetLanguageMappingsWithoutLanguageIds() throws Exception {
        String uri = "/v1/language";
        List<Long> languageIds = new ArrayList<>();
        LanguageMappingRequest languageMappingRequest = new LanguageMappingRequest(languageIds);

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(Utils.asJsonString(languageMappingRequest))
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(MESSAGE_LANGUAGE_IDS_REQUIRED));
    }

    @Test
    public void testGetAllLanguageMappings() throws Exception {
        String uri = "/v1/allLanguages";

        when(languageService.getAllLanguageMappings()).thenReturn(languageMappings());

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.languageMappings[0].language").value(english))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.languageMappings[1].language").value(bangla));
        verify(languageService, Mockito.times(1)).getAllLanguageMappings();
    }

}

package tests.services;

import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.models.db.EpisodeNotification;
import com.everwell.ins.models.db.PushNotificationLogs;
import com.everwell.ins.models.db.PushNotificationTemplate;
import com.everwell.ins.models.http.requests.EpisodeNotificationSearchRequest;
import com.everwell.ins.models.http.requests.UpdateEpisodeNotificationRequest;
import com.everwell.ins.models.http.responses.EpisodeNotificationResponse;
import com.everwell.ins.repositories.EpisodeNotificationRepository;
import com.everwell.ins.services.impl.EpisodeNotificationServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import tests.BaseTest;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class EpisodeNotificationServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    private EpisodeNotificationServiceImpl episodeNotificationService;

    @Mock
    private EpisodeNotificationRepository episodeNotificationRepository;

    @Test
    public void saveNotificationBulk() {
        Mockito.when(episodeNotificationRepository.saveAll(any())).thenReturn(Collections.singletonList(new EpisodeNotification()));

        episodeNotificationService.saveNotificationBulk(Collections.singletonList(new EpisodeNotification()));
        verify(episodeNotificationRepository, Mockito.times(1)).saveAll(any());
    }

    @Test
    public void testGetEpisodeNotifications() {
        PushNotificationTemplate pushNotificationTemplate = new PushNotificationTemplate();
        pushNotificationTemplate.setId(10L);
        pushNotificationTemplate.setIntentExtras("string_action");
        pushNotificationTemplate.setType("ACTION");
        PushNotificationLogs pushNotificationLogs = new PushNotificationLogs();
        pushNotificationLogs.setId(1L);
        pushNotificationLogs.setEntityId("123");
        pushNotificationLogs.setMessage("New connection request");
        pushNotificationLogs.setPushNotificationTemplate(pushNotificationTemplate);
        EpisodeNotification episodeNotification = new EpisodeNotification(1L, 1L, 171L, LocalDateTime.now(), false, null, "{\n" +
                "\t\t\t\t\"role\": \"Caregiver\",\n" +
                "\t\t\t\t\"nickname\": \"testing\",\n" +
                "\t\t\t\t\"relation\": \"Father\",\n" +
                "\t\t\t\t\"primaryUserId\": 3,\n" +
                "\t\t\t\t\"secondaryUserId\": 4\n" +
                "\t\t\t}", pushNotificationLogs);

        when(episodeNotificationRepository.findAllByEpisodeIdCustom(any(), eq(171L))).thenReturn(Collections.singletonList(episodeNotification));
        EpisodeNotificationSearchRequest episodeNotificationSearchRequest = new EpisodeNotificationSearchRequest(Collections.singletonList(123L));

        List<EpisodeNotificationResponse> responseList = episodeNotificationService.getNotificationForEpisodes(episodeNotificationSearchRequest, 171L);
        Assert.assertEquals( "New connection request", responseList.get(0).getMessage());
        Assert.assertEquals("ACTION", responseList.get(0).getNotificationType());
    }

    @Test
    public void testUpdateNotificationSuccess() {
        EpisodeNotification episodeNotification = new EpisodeNotification(1L, 1L, LocalDateTime.now(), false, null);
        episodeNotification.setId(1L);
        when(episodeNotificationRepository.findById(eq(1L))).thenReturn(Optional.of(episodeNotification));
        UpdateEpisodeNotificationRequest updateEpisodeNotificationRequest = new UpdateEpisodeNotificationRequest(1L, "accept", true);

        episodeNotificationService.updateNotification(updateEpisodeNotificationRequest);
        Assert.assertEquals(true, episodeNotification.getRead());
        Assert.assertEquals("accept", episodeNotification.getStatus());
    }

    @Test(expected = ValidationException.class)
    public void testUpdateNotificationNull() {
        when(episodeNotificationRepository.findById(eq(1L))).thenReturn(Optional.empty());
        UpdateEpisodeNotificationRequest updateEpisodeNotificationRequest = new UpdateEpisodeNotificationRequest(1L, "accept", true);

        episodeNotificationService.updateNotification(updateEpisodeNotificationRequest);
    }
}

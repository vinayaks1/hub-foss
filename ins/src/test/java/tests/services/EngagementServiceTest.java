package tests.services;


import com.everwell.ins.exceptions.ConflictException;
import com.everwell.ins.exceptions.NotFoundException;
import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.models.db.Engagement;
import com.everwell.ins.models.dto.PushNotificationTemplateDto;
import com.everwell.ins.models.dto.SmsDto;
import com.everwell.ins.models.dto.SmsRequestDto;
import com.everwell.ins.models.dto.SmsTemplateDto;
import com.everwell.ins.models.http.requests.*;
import com.everwell.ins.repositories.EngagementRepository;
import com.everwell.ins.services.impl.EngagementServiceImpl;
import com.everwell.ins.utils.Utils;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import tests.BaseTest;

import java.text.ParseException;
import java.util.*;

import static com.everwell.ins.enums.EngagementType.SMS;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class EngagementServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    private EngagementServiceImpl engagementService;

    @Mock
    private EngagementRepository engagementRepository;

    private static Long id = 1L;
    private static String text = "test";
    private static String entityId = "1";
    private static String device_id = "test123";
    private static Long vendorId = 5L;
    private static Long templateId = 2L;
    private static Long triggerId = 3L;
    private static Long typeId = 1L;
    private static Long languageId = 1L;
    private static Boolean consent = true;
    private static String timeOfDay = "2019-08-31 18:30:00";
    private static String timeOfDayNew = "2019-08-31 19:30:00";
    private static String incorrectTimeOfDay = "2019-08- 18:30:00";
    private static Date time = Utils.getCurrentDate();
    private static Date date;

    static {
        try {
            date = Utils.convertStringToDate(timeOfDay);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSaveEngagementConfig() {
        EngagementRequest request = new EngagementRequest(entityId, consent, timeOfDay, languageId, typeId);
        Engagement engagement = new Engagement(entityId, consent, time, languageId, typeId);
        when(engagementRepository.save(any())).thenReturn(engagement);

        engagementService.saveEngagementConfig(request);
        verify(engagementRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void testSaveEngagementConfigBulk() {
        EngagementBulkRequest request = new EngagementBulkRequest(Collections.singletonList(new EngagementRequest(entityId, consent, timeOfDay, languageId, typeId)));
        Engagement engagement = new Engagement(entityId, consent, time, languageId, typeId);

        when(engagementRepository.findByEntityIdAndTypeId(eq(entityId), eq(typeId))).thenReturn(null);
        when(engagementRepository.saveAll(any())).thenReturn(Collections.singletonList(engagement));

        engagementService.saveEngagementConfigBulk(request);
        verify(engagementRepository, Mockito.times(1)).saveAll(any());
    }

    @Test(expected = ConflictException.class)
    public void testSaveEngagementConfigBulkConflict() {
        EngagementBulkRequest request = new EngagementBulkRequest(Collections.singletonList(new EngagementRequest(entityId, consent, timeOfDay, languageId, typeId)));

        when(engagementRepository.findByEntityIdAndTypeId(eq(entityId), eq(typeId))).thenReturn(new Engagement());

        engagementService.saveEngagementConfigBulk(request);
    }

    @Test(expected = ConflictException.class)
    public void testSaveEngagementConfigAlreadyExists() {
        EngagementRequest request = new EngagementRequest(entityId, consent, timeOfDay, languageId, typeId);
        Engagement engagement = new Engagement(entityId, consent, time, languageId, typeId);
        when(engagementRepository.findByEntityIdAndTypeId(eq(entityId), eq(typeId))).thenReturn(engagement);

        engagementService.saveEngagementConfig(request);
    }

    @Test(expected = ValidationException.class)
    public void testSaveEngagementConfigIncorrectDate() {
        EngagementRequest request = new EngagementRequest(entityId, consent, incorrectTimeOfDay, languageId, typeId);
        when(engagementRepository.findByEntityIdAndTypeId(eq(entityId), eq(typeId))).thenReturn(null);

        engagementService.saveEngagementConfig(request);
    }

    @Test
    public void testGetEngagementConfig() {
        Engagement engagement = new Engagement(entityId, consent, time, languageId, typeId);
        when(engagementRepository.findByEntityIdAndTypeId(any(), anyLong())).thenReturn(engagement);

        engagementService.getEngagementConfig(entityId, 1L);
        verify(engagementRepository, Mockito.times(1)).findByEntityIdAndTypeId(any(), anyLong());
    }

    @Test(expected = NotFoundException.class)
    public void testGetEngagementConfigNotFound() {
        when(engagementRepository.findByEntityIdAndTypeId(any(), anyLong())).thenReturn(null);
        engagementService.getEngagementConfig(entityId, 1L);
    }

    @Test
    public void testGetEngagementConfigBulk() {
        Engagement engagement = new Engagement(entityId, consent, time, languageId, typeId);
        when(engagementRepository.findAllByEntityId(any())).thenReturn(Collections.singletonList(engagement));

        engagementService.getEngagementBulk(entityId);
        verify(engagementRepository, Mockito.times(1)).findAllByEntityId(eq(entityId));
    }

    @Test(expected = NotFoundException.class)
    public void testGetEngagementBulkConfigNotFound() {
        when(engagementRepository.findAllByEntityId(any())).thenReturn(null);
        engagementService.getEngagementBulk(entityId);
    }

    @Test
    public void testUpdateEngagementConfig() {
        EngagementRequest request = new EngagementRequest(entityId, consent, timeOfDay, languageId, typeId);
        Engagement engagement = new Engagement(entityId, null, null, null, null);
        when(engagementRepository.findByEntityIdAndTypeId(any(), anyLong())).thenReturn(engagement);

        engagementService.updateEngagementConfig(request);
        verify(engagementRepository, Mockito.times(1)).save(any());
    }

    @Test(expected = NotFoundException.class)
    public void testUpdateEngagementConfigNotFound() {
        EngagementRequest request = new EngagementRequest(entityId, consent, timeOfDay, languageId, typeId);

        engagementService.updateEngagementConfig(request);
    }

    @Test
    public void testModifyList() {
        SmsDto smsDto1 = new SmsDto("1", text, text);
        SmsDto smsDto2 = new SmsDto("2", text, text);
        SmsDto smsDto3 = new SmsDto("3", text, text);
        SmsDto smsDto4 = new SmsDto("4", text, text);
        List<SmsDto> smsDtos = new ArrayList<>();
        smsDtos.add(smsDto1);
        smsDtos.add(smsDto2);
        smsDtos.add(smsDto3);
        smsDtos.add(smsDto4);
        SmsRequest smsRequest = new SmsRequest(text, true, smsDtos, id, id, id, true);
        SmsRequestDto requestDto = new SmsRequestDto(smsRequest, null);
        engagementConfig();

        SmsRequestDto response = engagementService.modifyList(requestDto, SMS.getId());
        assertEquals(3, response.getSmsRequest().getPersonList().size());

        smsRequest.setPersonList(smsDtos);
        smsRequest.setDefaultConsent(false);
        response = engagementService.modifyList(requestDto, SMS.getId());
        assertEquals(2, response.getSmsRequest().getPersonList().size());
    }

    @Test
    public void testFilterSmsListWithIsMandatory() {
        SmsDetailedRequest smsDetailedRequest = new SmsDetailedRequest(id, null, getSmsTemplateDtos(), id, id, true, true, true, timeOfDay, null, null, 29L);
        engagementConfig();

        SmsDetailedRequest response = engagementService.filterSmsList(smsDetailedRequest, SMS.getId());
        assertEquals(7, response.getPersonList().size());
    }

    @Test(expected = ValidationException.class)
    public void testFilterSmsListIncorrectDate() {
        SmsDetailedRequest smsDetailedRequest = new SmsDetailedRequest(id, null, getSmsTemplateDtos(), id, id, true, true, true, incorrectTimeOfDay, null, null, 29L);
        engagementConfig();

        engagementService.filterSmsList(smsDetailedRequest, SMS.getId());
    }

    @Test
    public void testFilterSmsList() {
        SmsDetailedRequest smsDetailedRequest = new SmsDetailedRequest(id, null, getSmsTemplateDtos(), id, id, false, false, true, timeOfDay, null, null, 29L);
        engagementConfig();

        // DefaultConsent = true && DefaultTime = false
        SmsDetailedRequest response = engagementService.filterSmsList(smsDetailedRequest, SMS.getId());
        assertEquals(1, response.getPersonList().size());

        smsDetailedRequest.setPersonList(getSmsTemplateDtos());
        smsDetailedRequest.setTimeOfSms(timeOfDayNew);
        response = engagementService.filterSmsList(smsDetailedRequest, SMS.getId());
        assertEquals(0, response.getPersonList().size());

        // DefaultConsent = true && DefaultTime = true
        smsDetailedRequest.setPersonList(getSmsTemplateDtos());
        smsDetailedRequest.setIsDefaultTime(true);
        response = engagementService.filterSmsList(smsDetailedRequest, SMS.getId());
        assertEquals(2, response.getPersonList().size());

        // DefaultConsent = false && DefaultTime = true
        smsDetailedRequest.setPersonList(getSmsTemplateDtos());
        smsDetailedRequest.setDefaultConsent(false);
        response = engagementService.filterSmsList(smsDetailedRequest, SMS.getId());
        assertEquals(1, response.getPersonList().size());

        // DefaultConsent = false && DefaultTime = false
        smsDetailedRequest.setPersonList(getSmsTemplateDtos());
        smsDetailedRequest.setIsDefaultTime(false);
        response = engagementService.filterSmsList(smsDetailedRequest, SMS.getId());
        assertEquals(0, response.getPersonList().size());
    }

    private void engagementConfig() {
        List<Engagement> engagementConfig = new ArrayList<>();
        engagementConfig.add(new Engagement("2", true, null, id, null));
        engagementConfig.add(new Engagement("3", true, date, null, null));
        engagementConfig.add(new Engagement("4", false, null, null, null));
        engagementConfig.add(new Engagement("5", false, date, id, null));
        engagementConfig.add(new Engagement("6", null, null, null, null));
        engagementConfig.add(new Engagement("7", null, date, id, null));

        when(engagementRepository.findByEntityIdInAndTypeId(any(), anyLong())).thenReturn(engagementConfig);
    }

    private List<SmsTemplateDto> getSmsTemplateDtos() {
        SmsTemplateDto smsDto1 = new SmsTemplateDto("1", text, null);
        SmsTemplateDto smsDto2 = new SmsTemplateDto("2", text, null);
        SmsTemplateDto smsDto3 = new SmsTemplateDto("3", text, null);
        SmsTemplateDto smsDto4 = new SmsTemplateDto("4", text, null);
        SmsTemplateDto smsDto5 = new SmsTemplateDto("5", text, null);
        SmsTemplateDto smsDto6 = new SmsTemplateDto("6", text, null);
        SmsTemplateDto smsDto7 = new SmsTemplateDto("7", text, null);

        List<SmsTemplateDto> smsDtos = new ArrayList<>();
        smsDtos.add(smsDto1);
        smsDtos.add(smsDto2);
        smsDtos.add(smsDto3);
        smsDtos.add(smsDto4);
        smsDtos.add(smsDto5);
        smsDtos.add(smsDto6);
        smsDtos.add(smsDto7);
        return smsDtos;
    }

    @Test
    public void testFilterPushNotificationList() {
        String entityId1 = "1";
        String entityId2 = "2";
        String entityId3 = "3";
        String entityId4 = "4";

        List<PushNotificationTemplateDto> pushNotificationDto = new ArrayList<>();
        pushNotificationDto.add(new PushNotificationTemplateDto(entityId1, device_id, "test", "test"));
        pushNotificationDto.add(new PushNotificationTemplateDto(entityId2, device_id, "test", "test"));
        pushNotificationDto.add(new PushNotificationTemplateDto(entityId3, device_id, "test", "test"));
        PushNotificationTemplateDto pushNotificationTemplateDto = new PushNotificationTemplateDto(entityId4, device_id, "test", "test");
        pushNotificationTemplateDto.setDefaultConsent(false);
        pushNotificationDto.add(pushNotificationTemplateDto);

        PushNotificationRequest pushNotificationRequest = new PushNotificationRequest(pushNotificationDto, vendorId, templateId, triggerId);
        pushNotificationRequest.setIsMandatory(false);

        Engagement engagement1 = new Engagement(entityId1, consent, time, languageId, typeId);
        Engagement engagement2 = new Engagement(entityId2, false, time, languageId, typeId);

        when(engagementRepository.findByEntityIdAndTypeId(eq(entityId1), anyLong())).thenReturn(engagement1);
        when(engagementRepository.findByEntityIdAndTypeId(eq(entityId2), anyLong())).thenReturn(engagement2);

        PushNotificationRequest response = engagementService.filterPushNotificationList(pushNotificationRequest);

        assertEquals(1l, 1l);
        assertEquals(response.getPersonList().size(), 2);
        assertEquals(response.getPersonList().get(0).getId(), entityId1);
        assertEquals(response.getPersonList().get(1).getId(), entityId3);
    }
}

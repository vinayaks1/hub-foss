package tests.services;

import com.everwell.ins.enums.PushNotificationGateway;
import com.everwell.ins.factory.VendorFactory;
import com.everwell.ins.models.db.Vendor;
import com.everwell.ins.models.dto.PushNotificationRequestDto;
import com.everwell.ins.models.dto.PushNotificationTemplateDto;
import com.everwell.ins.models.http.requests.PushNotificationRequest;
import com.everwell.ins.repositories.VendorRepository;
import com.everwell.ins.services.EngagementService;
import com.everwell.ins.services.impl.PushNotificationServiceImpl;
import com.everwell.ins.services.impl.TemplateServiceImpl;
import com.everwell.ins.vendors.PushNotificationHandler;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import tests.BaseTest;

import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class PushNotificationServiceTest extends  BaseTest{

    @Spy
    @InjectMocks
    private PushNotificationServiceImpl pushNotificationService;

    @Mock
    private VendorRepository vendorRepository;

    @Mock
    private VendorFactory vendorFactory;

    @Mock
    private TemplateServiceImpl templateService;

    @Mock
    private PushNotificationRequestDto pnDTO;

    @Mock
    private PushNotificationHandler pushNotificationHandler;

    @Mock
    private EngagementService engagementService;

    private static Long id = 1L;
    private static Long vendorId = 5L;
    private static Long templateId = 2L;
    private static Long triggerId = 3L;
    private static String entityId = "1";
    private static String message = "Test";
    private static String device_id = "test123";
    private static String text = "Test";

    @Test
    public void testSendNotification() {
        List<PushNotificationTemplateDto> pushNotificationDto = new ArrayList<>();
        pushNotificationDto.add(new PushNotificationTemplateDto(entityId, device_id,"test","test"));
        PushNotificationRequest pushNotificationRequest = new PushNotificationRequest(pushNotificationDto, vendorId,templateId,triggerId);
        Vendor vendor = new Vendor(id, PushNotificationGateway.FIREBASE.toString(), text, text, text);

        when(vendorFactory.getPushNotificationHandler(any())).thenReturn(pushNotificationHandler);
        when(vendorRepository.findById(any())).thenReturn(Optional.of(vendor));

        pushNotificationService.sendNotification(pushNotificationRequest);
        verify(vendorRepository, Mockito.times(1)).findById(any());
        verify(vendorFactory, Mockito.times(1)).getPushNotificationHandler(any());
    }

    @Test
    public void testSendNotificationNotFound() {
        List<PushNotificationTemplateDto> pushNotificationDto = new ArrayList<>();
        pushNotificationDto.add(new PushNotificationTemplateDto(entityId, device_id,"test","test"));
        PushNotificationRequest pushNotificationRequest = new PushNotificationRequest(pushNotificationDto, vendorId,null,null);

        when(vendorRepository.findById(any())).thenReturn(Optional.empty());

        pushNotificationService.sendNotification(pushNotificationRequest);
        verify(vendorFactory, Mockito.times(0)).getPushNotificationHandler(any());

    }

    @Test
    public void testSendNotificationWithoutPersonList() {
        List<PushNotificationTemplateDto> pushNotificationDto = new ArrayList<>();
        PushNotificationRequest pushNotificationRequest = new PushNotificationRequest(pushNotificationDto, vendorId,templateId,triggerId);

        pushNotificationService.sendNotification(pushNotificationRequest);
        verify(vendorRepository, Mockito.times(0)).findById(any());
        verify(vendorFactory, Mockito.times(0)).getPushNotificationHandler(any());
    }

    @Test
    public void testSendBulkNotification() {
        List<PushNotificationTemplateDto> pushNotificationDto = new ArrayList<>();
        pushNotificationDto.add(new PushNotificationTemplateDto(entityId, device_id, "test","test"));
        List<PushNotificationRequest> pushNotificationRequests = new ArrayList<>();
        pushNotificationRequests.add(new PushNotificationRequest(pushNotificationDto, vendorId,templateId,triggerId));
        pushNotificationRequests.add(new PushNotificationRequest(pushNotificationDto, vendorId,templateId,triggerId));

        doNothing().when(pushNotificationService).sendNotification(any());

        pushNotificationService.sendBulkNotification(pushNotificationRequests);
        verify(pushNotificationService, Mockito.times(2)).sendNotification(any());
    }


    @Test
    public void testConstructNotification()
    {
        List<PushNotificationTemplateDto> pushNotificationDto = new ArrayList<>();
        pushNotificationDto.add(new PushNotificationTemplateDto(entityId, device_id,"test","test"));
        PushNotificationRequest pushNotificationRequest = new PushNotificationRequest(pushNotificationDto, vendorId,templateId,triggerId);
        pushNotificationRequest.setIsMandatory(true);
        List<PushNotificationTemplateDto> failedPNRequests = new ArrayList<>();
        PushNotificationRequestDto pushNotificationRequestDto = new PushNotificationRequestDto(pushNotificationRequest,failedPNRequests);
        when(templateService.constructNotification(any())).thenReturn(pushNotificationRequestDto);

       PushNotificationRequest response =  pushNotificationService.constructNotification(pushNotificationRequest);
       assertEquals(pushNotificationRequestDto.getPushNotificationRequest().getVendorId(),response.getVendorId());
       assertEquals(pushNotificationRequestDto.getPushNotificationRequest().getTemplateId(),response.getTemplateId());
       assertEquals(pushNotificationRequestDto.getPushNotificationRequest().getTriggerId(),response.getTriggerId());
       assertEquals(pushNotificationRequestDto.getPushNotificationRequest().getPersonList().get(0).getMessage(),response.getPersonList().get(0).getMessage());

    }

    @Test(expected = Exception.class)
    public void testSendNotificationException()
    {

        List<PushNotificationTemplateDto> pushNotificationDto = new ArrayList<>();
        pushNotificationDto.add(new PushNotificationTemplateDto(entityId, device_id,"test","test"));
        PushNotificationRequest pushNotificationRequest = new PushNotificationRequest(pushNotificationDto, vendorId,templateId,triggerId);

        when(vendorRepository.findById(any())).thenThrow(Exception.class);
        pushNotificationService.sendNotification(pushNotificationRequest);


    }

    @Test
    public void testConstructNotification_isMandatoryIsFalse() {
        List<PushNotificationTemplateDto> pushNotificationDto = new ArrayList<>();
        pushNotificationDto.add(new PushNotificationTemplateDto(entityId, device_id, "test", "test"));
        pushNotificationDto.add(new PushNotificationTemplateDto(entityId + 1, device_id + 1, "test", "test"));

        PushNotificationRequest pushNotificationRequest = new PushNotificationRequest(pushNotificationDto, vendorId, templateId, triggerId);
        pushNotificationRequest.setIsMandatory(false);

        List<PushNotificationTemplateDto> filteredPushNotificationDto = new ArrayList<>();
        filteredPushNotificationDto.add(new PushNotificationTemplateDto(entityId, device_id, "test", "test"));

        PushNotificationRequest filteredpushNotificationRequest = new PushNotificationRequest(pushNotificationDto, vendorId, templateId, triggerId);
        filteredpushNotificationRequest.setIsMandatory(false);

        when(engagementService.filterPushNotificationList(any())).thenReturn(filteredpushNotificationRequest);

        List<PushNotificationTemplateDto> failedPNRequests = new ArrayList<>();

        PushNotificationRequestDto pushNotificationRequestDto = new PushNotificationRequestDto(filteredpushNotificationRequest, failedPNRequests);
        when(templateService.constructNotification(any())).thenReturn(pushNotificationRequestDto);

        PushNotificationRequest response = pushNotificationService.constructNotification(filteredpushNotificationRequest);
        assertEquals(pushNotificationRequestDto.getPushNotificationRequest().getVendorId(), response.getVendorId());
        assertEquals(pushNotificationRequestDto.getPushNotificationRequest().getTemplateId(), response.getTemplateId());
        assertEquals(pushNotificationRequestDto.getPushNotificationRequest().getTriggerId(), response.getTriggerId());
        assertEquals(pushNotificationRequestDto.getPushNotificationRequest().getPersonList().get(0).getMessage(), response.getPersonList().get(0).getMessage());
        assertEquals(pushNotificationRequestDto.getPushNotificationRequest().getPersonList().size(), response.getPersonList().size());
    }

}

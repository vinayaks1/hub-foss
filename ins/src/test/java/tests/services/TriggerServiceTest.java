package tests.services;


import com.everwell.ins.exceptions.NotFoundException;
import com.everwell.ins.models.db.Trigger;
import com.everwell.ins.repositories.TriggerRepository;
import com.everwell.ins.services.impl.TriggerServiceImpl;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import tests.BaseTest;

import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TriggerServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    private TriggerServiceImpl triggerService;

    @Mock
    private TriggerRepository triggerRepository;

    private static Long id = 1L;
    private static String test = "Test";


    @Test
    public void testGetTrigger() {
        Trigger trigger = new Trigger(id, test, test, test, id);
        when(triggerRepository.findById(any())).thenReturn(Optional.of(trigger));

        triggerService.getTrigger(id);
        verify(triggerRepository, Mockito.times(1)).findById(any());
    }

    @Test
    public void testGetAllTriggers() {
        Trigger trigger = new Trigger(id, test, test, test, id);
        List<Trigger> triggerList = new ArrayList<>();
        triggerList.add(trigger);
        when(triggerRepository.findAll()).thenReturn(triggerList);

        triggerService.getAllTriggers();
        verify(triggerRepository, Mockito.times(1)).findAll();
    }

    @Test(expected = NotFoundException.class)
    public void testGetAllTriggersNotFound() {
        when(triggerRepository.findAll()).thenReturn(null);
        triggerService.getAllTriggers();
    }

}

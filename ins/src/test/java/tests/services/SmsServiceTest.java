package tests.services;


import com.everwell.ins.enums.SmsGateway;
import com.everwell.ins.exceptions.NotFoundException;
import com.everwell.ins.factory.VendorFactory;
import com.everwell.ins.models.db.Vendor;
import com.everwell.ins.models.dto.SmsDto;
import com.everwell.ins.models.http.requests.SmsRequest;
import com.everwell.ins.repositories.VendorRepository;
import com.everwell.ins.services.impl.SmsServiceImpl;
import com.everwell.ins.vendors.SmsHandler;
import org.junit.Test;
import org.mockito.*;
import tests.BaseTest;

import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class SmsServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    private SmsServiceImpl smsService;

    @Mock
    private VendorRepository vendorRepository;

    @Mock
    private VendorFactory vendorFactory;

    @Mock
    private SmsHandler smsHandler;

    private static Long id = 1L;
    private static Long vendorId = 5L;
    private static String entityId = "1";
    private static String message = "Test";
    private static String phone = "9705197814";
    private static String text = "Test";

    @Test
    public void testSendSms() {
        List<SmsDto> smsDto = new ArrayList<>();
        smsDto.add(new SmsDto(entityId, phone, message));
        SmsRequest smsRequest = new SmsRequest(message, true, smsDto, vendorId, null, null, null);
        Vendor vendor = new Vendor(id, SmsGateway.TEXTLOCAL.toString(), text, text, text);

        when(vendorFactory.getHandler(any())).thenReturn(smsHandler);
        when(vendorRepository.findById(any())).thenReturn(Optional.of(vendor));

        smsService.sendSms(smsRequest);
        verify(vendorRepository, Mockito.times(1)).findById(any());
        verify(vendorFactory, Mockito.times(1)).getHandler(any());
    }

    @Test(expected = NotFoundException.class)
    public void testSendSmsNotFound() {
        List<SmsDto> smsDto = new ArrayList<>();
        smsDto.add(new SmsDto(entityId, phone, message));
        SmsRequest smsRequest = new SmsRequest(message, true, smsDto, vendorId, null, null, true);

        when(vendorRepository.findById(any())).thenReturn(Optional.empty());

        smsService.sendSms(smsRequest);
    }

    @Test
    public void testSendSmsWithoutPersonList() {
        List<SmsDto> smsDto = new ArrayList<>();
        SmsRequest smsRequest = new SmsRequest(message, true, smsDto, vendorId, null, null, null);

        smsService.sendSms(smsRequest);
        verify(vendorRepository, Mockito.times(0)).findById(any());
        verify(vendorFactory, Mockito.times(0)).getHandler(any());
    }

    @Test
    public void testSendBulkSms() {
        List<SmsDto> smsDto = new ArrayList<>();
        smsDto.add(new SmsDto(entityId, phone, message));
        List<SmsRequest> smsRequests = new ArrayList<>();
        smsRequests.add(new SmsRequest(message, true, smsDto, vendorId, null, null, null));
        smsRequests.add(new SmsRequest(message, true, null, vendorId, null, null, null));

        doNothing().when(smsService).sendSms(any());

        smsService.sendBulkSms(smsRequests);
        verify(smsService, Mockito.times(1)).sendSms(any());
    }
}

package tests.services;


import com.everwell.ins.exceptions.NotFoundException;
import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.models.db.Client;
import com.everwell.ins.models.http.requests.RegisterClientRequest;
import com.everwell.ins.models.http.responses.ClientResponse;
import com.everwell.ins.repositories.ClientRepository;
import com.everwell.ins.services.impl.ClientServiceImpl;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import tests.BaseTest;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class ClientServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    private ClientServiceImpl clientService;

    @Mock
    private ClientRepository clientRepository;

    @Test(expected = ValidationException.class)
    public void testGetClientWithoutClientId() {
        clientService.getClient(null);
    }

    @Test(expected = NotFoundException.class)
    public void testGetClientNotFoundClient() {
        Long id = 1L;

        when(clientRepository.findById(id)).thenReturn(Optional.empty());

        clientService.getClient(id);
    }

    @Test
    public void testGetClientSuccess() {
        Long id = 1L;
        String text = "Test";
        Client client = new Client(text, text);

        when(clientRepository.findById(id)).thenReturn(Optional.of((client)));

        clientService.getClient(id);
    }

    @Test
    public void testGetClientWithToken() {
        Long id = 1L;
        String text = "abcdefhijklmnopqrstuvwxyzabcdefhijklmnopqrstuvwxyzabcdefhijklmnopqrstuvwxyzabcdefhijklmnopqrstuvwxyzabcdefhijklmnopqrstuvwxyz";
        Client client = new Client(text, text);

        when(clientRepository.findById(id)).thenReturn(Optional.of((client)));

        ClientResponse clientResponse = clientService.getClientWithToken(id);
        assertEquals(client.getName(), clientResponse.getName());
        verify(clientRepository, Mockito.times(1)).findById(any());
    }

    @Test(expected = ValidationException.class)
    public void testRegisterClientExistingClient() {
        String text = "Test";
        Client client = new Client(text, text);
        RegisterClientRequest clientRequest = new RegisterClientRequest(text, text);

        when(clientRepository.findByName(any())).thenReturn(client);

        clientService.registerClient(clientRequest);
    }

    @Test
    public void testRegisterClientSuccess() {
        String text = "Test";
        RegisterClientRequest clientRequest = new RegisterClientRequest(text, text);

        when(clientRepository.findByName(any())).thenReturn(null);
        when(clientRepository.save(any())).thenReturn(null);

        ClientResponse clientResponse = clientService.registerClient(clientRequest);
        assertEquals(clientRequest.getName(), clientResponse.getName());
        verify(clientRepository, Mockito.times(1)).findByName(any());
        verify(clientRepository, Mockito.times(1)).save(any());
    }

}

package tests.services;

import com.everwell.ins.enums.EmailGateway;
import com.everwell.ins.factory.VendorFactory;
import com.everwell.ins.models.db.Vendor;
import com.everwell.ins.models.dto.EmailRequestDto;
import com.everwell.ins.models.dto.EmailTemplateDto;
import com.everwell.ins.models.http.requests.EmailRequest;
import com.everwell.ins.repositories.VendorRepository;
import com.everwell.ins.services.impl.EmailServiceImpl;
import com.everwell.ins.services.impl.TemplateServiceImpl;
import com.everwell.ins.vendors.EmailHandler;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import tests.BaseTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class EmailServiceTest extends BaseTest {

    @Spy
    @InjectMocks
    private EmailServiceImpl emailService;

    @Mock
    private VendorRepository vendorRepository;

    @Mock
    private VendorFactory vendorFactory;

    @Mock
    private TemplateServiceImpl templateService;

    @Mock
    private EmailHandler emailHandler;

    private static String text = "Test";

    @Test
    public void testSendEmail() {
        List<EmailTemplateDto> emailTemplateDtos = new ArrayList<>();
        emailTemplateDtos.add(new EmailTemplateDto("recipient1", "body", "subject"));
        emailTemplateDtos.add(new EmailTemplateDto("recipient2", "body", "subject"));
        EmailRequest emailRequest = new EmailRequest(1L, 1L, 1L, "sender", emailTemplateDtos);
        Vendor vendor = new Vendor(1L, EmailGateway.SENDGRID.toString(), text, text, text);

        when(vendorFactory.getEmailHandler(any())).thenReturn(emailHandler);
        when(vendorRepository.findById(1L)).thenReturn(Optional.of(vendor));

        emailService.sendEmail(emailRequest);
        verify(vendorRepository, Mockito.times(1)).findById(any());
        verify(vendorFactory, Mockito.times(1)).getEmailHandler(any());
    }

    @Test
    public void testSendEmailVendorNotFound() {
        List<EmailTemplateDto> emailTemplateDtos = new ArrayList<>();
        emailTemplateDtos.add(new EmailTemplateDto("recipient1", "body", "subject"));
        emailTemplateDtos.add(new EmailTemplateDto("recipient2", "body", "subject"));
        EmailRequest emailRequest = new EmailRequest(1L, 1L, 1L, "sender", emailTemplateDtos);

        when(vendorRepository.findById(1L)).thenReturn(Optional.empty());

        emailService.sendEmail(emailRequest);
        verify(vendorFactory, Mockito.times(0)).getEmailHandler(any());
    }

    @Test
    public void testSendEmailWithoutRecipientList() {
        List<EmailTemplateDto> emailTemplateDtos = new ArrayList<>();
        EmailRequest emailRequest = new EmailRequest(1L, 1L, 1L, "sender", emailTemplateDtos);


        emailService.sendEmail(emailRequest);
        verify(vendorRepository, Mockito.times(0)).findById(any());
        verify(vendorFactory, Mockito.times(0)).getEmailHandler(any());

    }

    @Test(expected = Exception.class)
    public void testSendEmailException() {
        List<EmailTemplateDto> emailTemplateDtos = new ArrayList<>();
        emailTemplateDtos.add(new EmailTemplateDto("recipient1", "body", "subject"));
        EmailRequest emailRequest = new EmailRequest(1L, 1L, 1L, "sender", emailTemplateDtos);

        when(vendorRepository.findById(any())).thenThrow(Exception.class);
        emailService.sendEmail(emailRequest);
    }
    
    @Test
    public void testConstructEmail() {
        List<EmailTemplateDto> emailTemplateDtos = new ArrayList<>();
        emailTemplateDtos.add(new EmailTemplateDto("recipient", "body", "subject"));
        EmailRequest emailRequest = new EmailRequest(1L, 1L, 1L, "sender", emailTemplateDtos);
        List<EmailTemplateDto> failedEmailRequest = new ArrayList<>();
        EmailRequestDto emailRequestDto = new EmailRequestDto(emailRequest,failedEmailRequest);
        when(templateService.constructEmail(emailRequest)).thenReturn(emailRequestDto);
        
        EmailRequest response = emailService.constructEmail(emailRequest);
        assertEquals(emailRequestDto.getEmailRequest().getVendorId(),response.getVendorId());
        assertEquals(emailRequestDto.getEmailRequest().getTemplateId(),response.getTemplateId());
        assertEquals(emailRequestDto.getEmailRequest().getTriggerId(),response.getTriggerId());
        assertEquals(emailRequestDto.getEmailRequest().getRecipientList().get(0).getSubject(),response.getRecipientList().get(0).getSubject());
    }
}

package tests.services;


import com.everwell.ins.exceptions.NotFoundException;
import com.everwell.ins.models.db.SmsLogs;
import com.everwell.ins.models.dto.EventStreamingDto;
import com.everwell.ins.models.dto.ReconciliationDto;
import com.everwell.ins.models.dto.vendorCreds.ApiKeySenderRouteDto;
import com.everwell.ins.repositories.SmsLogsRepository;
import com.everwell.ins.services.VendorService;
import com.everwell.ins.services.impl.RabbitMQPublisherServiceImpl;
import com.everwell.ins.services.impl.ReconciliationServiceImpl;
import com.everwell.ins.utils.SentryUtils;
import com.everwell.ins.utils.Utils;
import org.asynchttpclient.*;
import org.asynchttpclient.netty.NettyResponseFuture;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static com.everwell.ins.constants.RabbitMQConstants.*;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore({"javax.naming.*","javax.xml.*", "com.sun.org.apache.xerces.*", "org.slf4j.*", "org.xml.*","com.ibm.*"})
public class ReconciliationServiceTest  {

    @InjectMocks
    private ReconciliationServiceImpl reconciliationService;

    @Mock
    private SmsLogsRepository smsLogsRepository;

    @Mock
    private VendorService vendorService;

    @Mock
    private ApiKeySenderRouteDto creds;

    @Mock
    private RabbitMQPublisherServiceImpl rabbitMQPublisherService;

    private String smsReconciliationQueue = "q.ins.process_sms_reconciliation";

    private Long reconciliationDelay = 1000*1L; // 1 sec delay
    @Before
    public void setUp() {
        ReflectionTestUtils.setField(reconciliationService, "smsReconciliationQueue", smsReconciliationQueue);
        ReflectionTestUtils.setField(reconciliationService,"reconciliationDelay", reconciliationDelay);
    }

    String testResponse = "{ \"tList\": [ { \"senderId\": \"HUB\", \"rateCost\": \"0.00\", \"message\": \"*******...******#####***???*****\", \"destinationNumber\": \"2347038709332\", \"dateSent\": \"2021-05-17T14:47:53.327Z\", \"status\": \"Completed\", \"referenceNumber\": \"ibekwestanleyc@gmail.com1f9420210517-0347542210\", \"stateChangeReason\": \"Message was flagged as sent using the COURE-ROUTE Mtn Subroute details with state of CommandStatus.ESME_ROK\", \"stateChangeUtc\": \"2021-05-17T14:48:35.588Z\", \"messageId\": \"8c94e0637568596755682687\" } ], \"tListCount\": 156, \"filteredCount\": 100 }";

    @Test
    public void testSaveStatus() {
        Long id = 1L;
        String entityId = "1";
        List<Long> vendorIds = new ArrayList<>();
        vendorIds.add(id);
        String text = "Test";
        ReconciliationDto reconciliation = new ReconciliationDto(vendorIds, text, text, text);
        List<SmsLogs> SmsLogs = new ArrayList<>();
        SmsLogs.add(new SmsLogs(id, id, id, entityId, text, text, null, text, text));

        when(smsLogsRepository.getSmsLogsByMessageIdAndVendorId(any(), any())).thenReturn(SmsLogs);
        doNothing().when(smsLogsRepository).updateStatus(any(), any(), any());

        boolean statusUpdated = reconciliationService.saveStatus(reconciliation);
        verify(smsLogsRepository, Mockito.times(1)).getSmsLogsByMessageIdAndVendorId(any(), any());
        verify(smsLogsRepository, Mockito.times(1)).updateStatus(any(), any(), any());
        assertTrue(statusUpdated);
    }

    @Test
    public void testSaveStatusSmsLogsNotFound_SendToQueue() {
        Long id = 1L;
        List<Long> vendorIds = new ArrayList<>();
        vendorIds.add(id);
        String text = "Test";
        Boolean retryTrue = true;
        Boolean retryFalse =false;
        ReconciliationDto reconciliation = new ReconciliationDto(vendorIds, text, text, text, retryTrue);
        List<SmsLogs> SmsLogs = new ArrayList<>();

        when(smsLogsRepository.getSmsLogsByMessageIdAndVendorId(any(), any())).thenReturn(SmsLogs);
        doNothing().when(rabbitMQPublisherService).send(any(),any(),any(),any());
        ReconciliationDto updatedReconciliationDto = new ReconciliationDto(vendorIds,text,text,text,retryFalse);
        EventStreamingDto<ReconciliationDto> event = new EventStreamingDto<>(smsReconciliationQueue, updatedReconciliationDto);
        String jsonEvent = Utils.asJsonString(event);
        Map<String, Object> msgHeaders = new HashMap<>();
        msgHeaders.put(DELAY_HEADER, reconciliationDelay);
        boolean statusUpdated = reconciliationService.saveStatus(reconciliation);

        verify(smsLogsRepository, Mockito.times(1)).getSmsLogsByMessageIdAndVendorId(any(), any());
        verify(smsLogsRepository, Mockito.times(0)).updateStatus(any(), any(), any());
        assertFalse(statusUpdated);
        verify(rabbitMQPublisherService, Mockito.times(1)).send(eq(jsonEvent),eq(smsReconciliationQueue),eq(DELAYED_EXCHANGE),eq(msgHeaders));
    }

    @PrepareForTest({AsyncHttpClient.class, NettyResponseFuture.class, Dsl.class})
    @Test
    public void fetchStatusInterra() throws ExecutionException, InterruptedException {
        Long id = 1L;
        String entityId = "1";
        List<Long> vendorIds = new ArrayList<>();
        vendorIds.add(id);
        String text = "Test";
        ReconciliationDto reconciliation = new ReconciliationDto(vendorIds, text, text, text);
        List<SmsLogs> SmsLogs = new ArrayList<>();
        SmsLogs.add(new SmsLogs(id, id, id, entityId, text, text, null, text, text));
        mockStatic(Dsl.class);
        DefaultAsyncHttpClient client = mock(DefaultAsyncHttpClient.class);
        Response response = mock(Response.class);
        NettyResponseFuture<Response> future = mock(NettyResponseFuture.class);

        when(smsLogsRepository.getSmsLogsByVendorIdAndMessageIdAndStatusAndAddedOnAfter(any(), any())).thenReturn(SmsLogs);
        doNothing().when(smsLogsRepository).updateStatus(any(), any(), any());
        when(vendorService.getVendorCredentials(any(),any())).thenReturn(creds);
        when(creds.getReconciliationUrl()).thenReturn("http://www.test.com");
        when(creds.getApiKey()).thenReturn(text);
        when(Dsl.asyncHttpClient()).thenReturn(client);
        when(client.executeRequest(any(Request.class))).thenReturn(future);
        when(future.get()).thenReturn(response);
        when(response.getResponseBody()).thenReturn(testResponse);

        reconciliationService.fetchStatusInterra(reconciliation);
        verify(smsLogsRepository, Mockito.times(1)).getSmsLogsByVendorIdAndMessageIdAndStatusAndAddedOnAfter(any(), any());
        verify(smsLogsRepository, Mockito.times(1)).updateStatus(any(), any(), any());
    }

    @Test
    @PrepareForTest({SentryUtils.class})
    public void fetchStatusInterraException() {
        Long id = 1L;
        String entityId = "1";
        List<Long> vendorIds = new ArrayList<>();
        vendorIds.add(id);
        String text = "Test";
        ReconciliationDto reconciliation = new ReconciliationDto(vendorIds, text, text, text);
        List<SmsLogs> SmsLogs = new ArrayList<>();
        SmsLogs.add(new SmsLogs(id, id, id, entityId, text, text, null, text, text));

        mockStatic(SentryUtils.class);

        when(smsLogsRepository.getSmsLogsByVendorIdAndMessageIdAndStatusAndAddedOnAfter(any(),any() )).thenReturn(SmsLogs);
        when(vendorService.getVendorCredentials(any(), any())).thenThrow(NotFoundException.class);

        reconciliationService.fetchStatusInterra(reconciliation);
        PowerMockito.verifyStatic(SentryUtils.class, Mockito.times(1));
        SentryUtils.captureException(any());

    }

}

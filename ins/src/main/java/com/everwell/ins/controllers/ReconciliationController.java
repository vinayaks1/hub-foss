package com.everwell.ins.controllers;

import com.everwell.ins.enums.SmsGateway;
import com.everwell.ins.exceptions.NotFoundException;
import com.everwell.ins.models.dto.ReconciliationDto;
import com.everwell.ins.models.http.requests.AfricasTalkingReconciliationRequest;
import com.everwell.ins.models.http.requests.PlivoReconciliationRequest;
import com.everwell.ins.models.http.requests.TwilioReconciliationRequest;
import com.everwell.ins.models.http.responses.Response;
import com.everwell.ins.services.ReconciliationService;
import com.everwell.ins.services.VendorService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
public class ReconciliationController {

    private static Logger LOGGER = LoggerFactory.getLogger(ReconciliationController.class);

    @Autowired
    private ReconciliationService reconciliationService;

    @Autowired
    private VendorService vendorService;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @ApiOperation(
            value = "Reconciliation from Twilio",
            notes = "Accepts and stores status of SMS sent from Twilio"
    )
    @RequestMapping(value = "/v1/reconciliation/twilio", method = RequestMethod.POST)
    public void reconciliationTwilio(TwilioReconciliationRequest request) throws IOException {
        ReconciliationDto reconciliationDto = new ReconciliationDto();
        String smsGateway = SmsGateway.TWILIO.toString();
        applicationEventPublisher.publishEvent(smsGateway);
        reconciliationDto.setVendorIds(vendorService.getVendorIds(smsGateway));
        if (null == reconciliationDto.getVendorIds()) {
            LOGGER.error("[reconciliationTwilio] vendorId not found for gateway Twilio");
            throw new NotFoundException("vendorId not found");
        }
        reconciliationDto.setTo(request.getTo());
        reconciliationDto.setMessageId(request.getMessageSid());
        reconciliationDto.setStatus(request.getMessageStatus());
        boolean statusUpdated = reconciliationService.saveStatus(reconciliationDto);
        if(statusUpdated) {
            LOGGER.info("[reconciliationTwilio] sms status updated" + request.getTo());
        }
    }

    @ApiOperation(
            value = "Reconciliation from Plivo",
            notes = "Accepts and stores status of SMS sent from Plivo"
    )
    @RequestMapping(value = "/v1/reconciliation/plivo", method = RequestMethod.POST)
    public void reconciliationPlivo(@RequestBody PlivoReconciliationRequest request) throws IOException {
        ReconciliationDto reconciliationDto = new ReconciliationDto();
        String smsGateway = SmsGateway.PLIVO.toString();
        applicationEventPublisher.publishEvent(smsGateway);
        reconciliationDto.setVendorIds(vendorService.getVendorIds(smsGateway));
        if (null == reconciliationDto.getVendorIds()) {
            LOGGER.error("[reconciliationPlivo] vendorId not found for gateway Plivo");
            throw new NotFoundException("vendorId not found");
        }
        reconciliationDto.setTo(request.getTo());
        reconciliationDto.setMessageId(request.getMessageUUID());
        reconciliationDto.setStatus(request.getStatus());
        boolean statusUpdated = reconciliationService.saveStatus(reconciliationDto);
        if(statusUpdated) {
            LOGGER.info("[reconciliationPlivo] sms status updated" + request.getTo());
        }
    }

    @ApiOperation(
            value = "Reconciliation from AfricasTalking",
            notes = "Accepts and stores status of SMS sent from AfricasTalking"
    )
    @RequestMapping(value = "/v1/reconciliation/africasTalking", method = RequestMethod.POST)
    public void reconciliationAfricasTalking(AfricasTalkingReconciliationRequest request) throws IOException {

        LOGGER.info("[reconciliationAfricasTalking] id: " + request.getId() + "; phone No: " + request.getPhoneNumber() + "; status: " + request.getStatus());

        ReconciliationDto reconciliationDto = new ReconciliationDto();
        String smsGateway = SmsGateway.AFRICASTALKING.toString();
        applicationEventPublisher.publishEvent(smsGateway);
        reconciliationDto.setVendorIds(vendorService.getVendorIds(smsGateway));
        if (null == reconciliationDto.getVendorIds()) {
            LOGGER.error("[reconciliationAfricasTalking] vendorId not found for gateway AfricasTalking");
            throw new NotFoundException("vendorId not found");
        }
        reconciliationDto.setTo(request.getPhoneNumber());
        reconciliationDto.setMessageId(request.getId());
        reconciliationDto.setStatus(request.getStatus());
        boolean statusUpdated = reconciliationService.saveStatus(reconciliationDto);
        if (statusUpdated) {
            LOGGER.info("[reconciliationAfricasTalking] sms status updated" + request.getPhoneNumber());
        }
    }

    @ApiOperation(
            value = "Reconciliation from NIC",
            notes = "Accepts and stores status of SMS sent from NIC"
    )
    @GetMapping(value = "/v1/reconciliation/nic")
    public ResponseEntity<Response<String>> reconciliationNIC(@RequestParam("a2wackid") String acknowledgementId,
                                                              @RequestParam("mnumber") String phoneNumber,
                                                              @RequestParam("a2wstatus") String status) {
        ReconciliationDto reconciliationDto = new ReconciliationDto(vendorService.getVendorIds(SmsGateway.NIC.toString()),
                acknowledgementId, status, phoneNumber);
        if (null == reconciliationDto.getVendorIds()) {
            LOGGER.error("[reconciliationNIC] vendorId not found for gateway NIC");
            throw new NotFoundException("vendorId not found");
        }
        boolean statusUpdated = reconciliationService.saveStatus(reconciliationDto);
        if (statusUpdated) {
            Response<String> response = new Response<>(true, "OK");
            LOGGER.info("[reconciliationNIC] sms status updated " + phoneNumber);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } else {
            Response<String> response = new Response<>(false, null);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(
            value = "Reconciliation from TextLocal",
            notes = "Accepts and stores status of SMS sent from TextLocal"
    )
    @RequestMapping(value = "/v1/reconciliation/textlocal", method = RequestMethod.POST)
    public void reconciliationTextLocal(@RequestParam("number") String number,
                                        @RequestParam("status") String status,
                                        @RequestParam("customID") String customID,
                                        @RequestParam("datetime") String dateTime) throws IOException {
        ReconciliationDto reconciliationDto = new ReconciliationDto(vendorService.getVendorIds(SmsGateway.TEXTLOCAL.toString()),
                customID, status, number);
        reconciliationDto.setVendorIds(vendorService.getVendorIds(SmsGateway.TEXTLOCAL.toString()));
        String smsGateway = SmsGateway.TEXTLOCAL.toString();
        applicationEventPublisher.publishEvent(smsGateway);
        if (null == reconciliationDto.getVendorIds()) {
            LOGGER.error("[reconciliationTextLocal] vendorId not found for gateway TextLocal");
            throw new NotFoundException("vendorId not found");
        }
        boolean statusUpdated = reconciliationService.saveStatus(reconciliationDto);
        if (statusUpdated) {
            LOGGER.info("[reconciliationTextLocal] sms status updated" + number);
        }
    }

    @ApiOperation(
            value = "Reconciliation from Interra",
            notes = "Fetch and stores status of SMS sent from Interra"
    )
    @RequestMapping(value = "/v1/reconciliation/interra", method = RequestMethod.GET)
    @Scheduled(cron = "0 0 2 * * *")
    public void reconciliationInterraNetworks() {
        ReconciliationDto reconciliationDto = new ReconciliationDto();
        reconciliationDto.setVendorIds(vendorService.getVendorIds(SmsGateway.INTERRA.toString()));
        String smsGateway = SmsGateway.INTERRA.toString();
        applicationEventPublisher.publishEvent(smsGateway);
        if (null == reconciliationDto.getVendorIds()) {
            LOGGER.error("[reconciliationInterraNetwors] vendorId not found for gateway INTERRA");
            throw new NotFoundException("vendorId not found");
        }
        reconciliationService.fetchStatusInterra(reconciliationDto);
    }

}

package com.everwell.ins.controllers;


import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.models.dto.LanguageMapping;
import com.everwell.ins.models.http.requests.LanguageMappingRequest;
import com.everwell.ins.models.http.responses.LanguageResponse;
import com.everwell.ins.models.http.responses.Response;
import com.everwell.ins.services.LanguageService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class LanguageController {

    private static Logger LOGGER = LoggerFactory.getLogger(LanguageController.class);

    @Autowired
    private LanguageService languageService;

    @ApiOperation(
            value = "Get Language Mapping",
            notes = "Fetch language Id mappings"
    )
    @PostMapping(value = "/v1/language")
    public ResponseEntity<Response<LanguageResponse>> getLanguageMappings(@RequestBody LanguageMappingRequest languageMappingRequest) {
        LOGGER.info("[getLanguageMappings] get language mapping request accepted");
        languageMappingRequest.validate(languageMappingRequest);
        List<LanguageMapping> languageList = languageService.getLanguageMappings(languageMappingRequest.getLanguageIds());
        Response<LanguageResponse> response = new Response<>(true, new LanguageResponse(languageList));
        LOGGER.info("[getLanguageMappings] response generated: " + response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @GetMapping(value = "/v1/allLanguages")
    public ResponseEntity<Response<LanguageResponse>> getAllLanguageMappings() {
        LOGGER.info("[getAllLanguageMappings] get all language mapping request accepted");
        List<LanguageMapping> languageList = languageService.getAllLanguageMappings();
        Response<LanguageResponse> response = new Response<>(true, new LanguageResponse(languageList));
        LOGGER.info("[getAllLanguageMappings] response generated: " + response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}

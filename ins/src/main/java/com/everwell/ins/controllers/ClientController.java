package com.everwell.ins.controllers;

import com.everwell.ins.constants.Constants;
import com.everwell.ins.models.http.responses.Response;
import com.everwell.ins.models.http.requests.RegisterClientRequest;
import com.everwell.ins.models.http.responses.ClientResponse;
import com.everwell.ins.services.ClientService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ClientController {

    private static Logger LOGGER = LoggerFactory.getLogger(ClientController.class);

    @Autowired
    ClientService clientService;

    @ApiOperation(
            value = "Get Client Data",
            notes = "Fetches authorization token along with next Refresh time for a given client"
    )
    @RequestMapping(value = "/v1/client", method = RequestMethod.GET)
    public ResponseEntity<Response<ClientResponse>> getClient(@RequestHeader(name = Constants.INS_CLIENT_ID) Long id) {
        LOGGER.info("[getClient] get client request accepted");
        return ResponseEntity.ok(new Response<>(true, clientService.getClientWithToken(id)));
    }

    @ApiOperation(
            value = "Register Client",
            notes = "Registers and stores client with the provided name and Id"
    )
    @RequestMapping(value = "/v1/client", method = RequestMethod.POST)
    public ResponseEntity<Response<ClientResponse>> registerClient(@RequestBody RegisterClientRequest registerClientRequest) {
        LOGGER.info("[registerClient] register client request accepted");
        registerClientRequest.validate();
        ClientResponse clientResponse = clientService.registerClient(registerClientRequest);
        Response<ClientResponse> response = new Response<>(clientResponse, "client registered successfully");
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

}

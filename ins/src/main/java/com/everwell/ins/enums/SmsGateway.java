package com.everwell.ins.enums;

public enum SmsGateway {
    NIC,
    TEXTLOCAL,
    ICCDDRB,
    TWILIO,
    PLIVO,
    GLOBELABS,
    AFRICASTALKING,
    COMMSERVICE,
    INTERRA,
    ADN
}

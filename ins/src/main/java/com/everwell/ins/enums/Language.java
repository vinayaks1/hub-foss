package com.everwell.ins.enums;

public enum Language {
    NON_UNICODE,
    UNICODE
}

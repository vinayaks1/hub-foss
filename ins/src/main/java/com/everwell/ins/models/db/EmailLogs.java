package com.everwell.ins.models.db;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Table(name = "ins_email_logs")
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class EmailLogs {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter
    private Long id;
    @Column(name = "recipient", columnDefinition = "TEXT")
    private String recipient;
    @Column(name = "sender", columnDefinition = "TEXT")
    private String sender;
    @Column(name = "body", columnDefinition = "TEXT")
    private String body;
    @Column(name = "subject", columnDefinition = "TEXT")
    private String subject;
    @Column(name = "sendgrid_response", columnDefinition = "TEXT")
    private String sendgridResponse;
    @Column(name = "vendor_id")
    private Long vendorId;
    @Column(name = "added_on")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime addedOn;
    @Column(name = "template_id")
    private Long templateId;
    @Column(name = "trigger_id")
    private Long triggerId;

    public EmailLogs(Long vendorId, Long templateId, Long triggerId, String recipient, String sender, String body, String subject, String sendgridResponse, LocalDateTime addedOn)
    {
        this.vendorId = vendorId;
        this.templateId = templateId;
        this.triggerId = triggerId;
        this.addedOn = addedOn;
        this.body = body;
        this.subject = subject;
        this.sender = sender;
        this.recipient = recipient;
        this.sendgridResponse = sendgridResponse;
    }
}

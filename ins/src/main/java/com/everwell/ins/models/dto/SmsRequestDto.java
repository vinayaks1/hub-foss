package com.everwell.ins.models.dto;

import com.everwell.ins.models.http.requests.SmsRequest;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@Getter
public class SmsRequestDto implements Serializable {
    SmsRequest smsRequest;
    List<SmsTemplateDto> failedSmsRequest;
}

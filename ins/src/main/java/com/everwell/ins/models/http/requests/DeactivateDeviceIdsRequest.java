package com.everwell.ins.models.http.requests;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class DeactivateDeviceIdsRequest {
    List<String> invalidDeviceIds;
    String clientId;

    public DeactivateDeviceIdsRequest(List<String> invalidDeviceIds) {
        this.invalidDeviceIds = invalidDeviceIds;
    }

    public DeactivateDeviceIdsRequest(List<String> invalidDeviceIds, String clientId) {
        this.invalidDeviceIds = invalidDeviceIds;
        this.clientId = clientId;
    }

}

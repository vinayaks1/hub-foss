package com.everwell.ins.models.http.requests;

import com.everwell.ins.exceptions.ValidationException;
import lombok.*;
import org.springframework.util.StringUtils;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class TemplateRequest {
    String content;
    Long languageId;
    Long typeId;
    String parameters;
    Boolean unicode = false;

    public void validate(TemplateRequest templateRequest) {
        if (StringUtils.isEmpty(templateRequest.getContent())) {
            throw new ValidationException("content should not be empty");
        }
        if (null == templateRequest.getLanguageId()) {
            throw new ValidationException("language id is required");
        }
        if (null == templateRequest.getTypeId()) {
            throw new ValidationException("type id is required");
        }
    }
}

package com.everwell.ins.models.dto.vendorConfigs;

import lombok.*;

@Data
public class PlivoConfigDto extends VendorConfigsBase {
    String bulkSmsLimit;
    String reconciliation;
}

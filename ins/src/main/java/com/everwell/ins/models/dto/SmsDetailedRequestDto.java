package com.everwell.ins.models.dto;

import com.everwell.ins.models.http.requests.SmsRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@Getter
public class SmsDetailedRequestDto implements Serializable {
    List<SmsRequest> smsRequests;
    List<SmsTemplateDto> failedSmsRequest;
}

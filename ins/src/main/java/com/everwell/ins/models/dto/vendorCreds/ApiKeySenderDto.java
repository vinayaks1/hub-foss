package com.everwell.ins.models.dto.vendorCreds;

import lombok.*;

@Data
public class ApiKeySenderDto extends VendorCredsBase {
    String apiKey;
    String url;
    String sender;
}

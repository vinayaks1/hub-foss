package com.everwell.ins.models.http.requests;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class PlivoReconciliationRequest {
    String MessageUUID;
    String Status;
    String To;
}

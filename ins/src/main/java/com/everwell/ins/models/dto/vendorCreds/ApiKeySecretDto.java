package com.everwell.ins.models.dto.vendorCreds;
import lombok.*;

@Data
public class ApiKeySecretDto extends VendorCredsBase {
    String apiKey;
    String apiSecret;
    String url;
}

package com.everwell.ins.models.dto;


import lombok.*;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class FirebaseResponseDto implements Serializable{
    List<String> firebaseResponse;
    List<String> invalidDeviceIds;

    public FirebaseResponseDto(String FirebaseResponse) {
    }

    public FirebaseResponseDto(List<String> firebaseResponse) {
        this.firebaseResponse= firebaseResponse;
    }
}

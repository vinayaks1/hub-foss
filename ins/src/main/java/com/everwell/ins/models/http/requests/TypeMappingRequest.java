package com.everwell.ins.models.http.requests;

import com.everwell.ins.exceptions.ValidationException;
import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class TypeMappingRequest {
    List<Long> typeIds;

    public void validate(TypeMappingRequest typeMappingRequest) {
        if (typeMappingRequest.getTypeIds().isEmpty()) {
            throw new ValidationException("type ids are required");
        }
    }
}

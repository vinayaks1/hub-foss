package com.everwell.ins.models.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EventStreamingAnalyticsDto {
    private String eventCategory;
    private String eventAction;
    private String eventName;
    private Long eventTime;

    public EventStreamingAnalyticsDto(String eventCategory, String eventAction, String eventName) {
        this.eventCategory = eventCategory;
        this.eventAction = eventAction;
        this.eventName = eventName;
        this.eventTime = new Date().getTime();
    }
}
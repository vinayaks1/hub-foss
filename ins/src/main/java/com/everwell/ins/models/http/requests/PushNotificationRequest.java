package com.everwell.ins.models.http.requests;

import com.everwell.ins.enums.Language;
import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.models.dto.PushNotificationTemplateDto;
import lombok.*;
import org.springframework.util.CollectionUtils;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@ToString

public class PushNotificationRequest {
    String message;
    String heading;
    Boolean isNotificationCommon;
    List<PushNotificationTemplateDto> personList;
    Long vendorId;
    Language language = Language.NON_UNICODE;
    Long templateId;
    Long triggerId;
    Long clientId;
    Long timeToLive;
    String classIntent;
    String intentExtras;
    Boolean sendNotificationData = true;
    Boolean isMandatory = false;
    Boolean episodeNotification = false;
    String notificationExtraData;
    
    public PushNotificationRequest(List<PushNotificationTemplateDto> personList, Long vendorId, Long templateId, Long triggerId) {
        this.personList = personList;
        this.vendorId = vendorId;
        this.templateId = templateId ;
        this.triggerId = triggerId ;
    }

    public void validate() {
        if (CollectionUtils.isEmpty(personList)) {
            throw new ValidationException("personList should not be empty");
        }

        if (null == vendorId) {
            throw new ValidationException("vendor id is required");
        }

        if (null == triggerId) {
            throw new ValidationException("Trigger id is required");
        }

        if (null == templateId) {
            throw new ValidationException("Template id is required");
        }
    }

}

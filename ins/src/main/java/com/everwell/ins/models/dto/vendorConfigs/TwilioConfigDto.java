package com.everwell.ins.models.dto.vendorConfigs;

import lombok.*;

@Data
public class TwilioConfigDto extends VendorConfigsBase {
    String language;
    String unicodeLanguage;
    String reconciliation;
}

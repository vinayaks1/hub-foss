package com.everwell.ins.models.dto.vendorConfigs;

import lombok.*;

@Data
public class ADNConfigDto extends VendorConfigsBase{
    String bulkSmsLimit;
}

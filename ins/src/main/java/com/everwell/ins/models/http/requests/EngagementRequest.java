package com.everwell.ins.models.http.requests;

import com.everwell.ins.exceptions.ValidationException;
import lombok.*;
import org.springframework.util.StringUtils;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class EngagementRequest {
    String entityId;
    Boolean consent;
    String timeOfDay;
    Long languageId;
    Long typeId = 1L;

    public void validate(EngagementRequest engagementRequest) {
        if (StringUtils.isEmpty(engagementRequest.getEntityId())) {
            throw new ValidationException("entity id is required");
        }
        if (null == consent) {
            throw new ValidationException("consent is required");
        }
    }
}

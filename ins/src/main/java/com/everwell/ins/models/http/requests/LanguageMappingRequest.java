package com.everwell.ins.models.http.requests;

import com.everwell.ins.exceptions.ValidationException;
import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class LanguageMappingRequest {
    List<Long> languageIds;

    public void validate(LanguageMappingRequest languageMappingRequest) {
        if (languageMappingRequest.getLanguageIds().isEmpty()) {
            throw new ValidationException("language ids are required");
        }
    }
}

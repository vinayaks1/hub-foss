package com.everwell.ins.models.dto.vendorConfigs;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "configType", include = JsonTypeInfo.As.EXISTING_PROPERTY)
@JsonSubTypes({
        @JsonSubTypes.Type(value = NICConfigDto.class, name = "NIC"),
        @JsonSubTypes.Type(value = TextLocalConfigDto.class, name = "TEXTLOCAL"),
        @JsonSubTypes.Type(value = ICCDDRBConfigDto.class, name = "ICCDDRB"),
        @JsonSubTypes.Type(value = TwilioConfigDto.class, name = "TWILIO"),
        @JsonSubTypes.Type(value = PlivoConfigDto.class, name = "PLIVO"),
        @JsonSubTypes.Type(value = GlobeLabsConfigDto.class, name = "GLOBELABS"),
        @JsonSubTypes.Type(value = AfricasTalkingConfigDto.class, name = "AFRICASTALKING"),
        @JsonSubTypes.Type(value = CommServiceConfigDto.class, name = "COMMSERVICE"),
        @JsonSubTypes.Type(value = InterraConfigDto.class, name = "INTERRA"),
        @JsonSubTypes.Type(value = FirebaseConfigDto.class, name = "FIREBASE"),
        @JsonSubTypes.Type(value = SendgridConfigDto.class, name = "SENDGRID"),
        @JsonSubTypes.Type(value = ADNConfigDto.class, name = "ADN")
})
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class VendorConfigsBase {
    String configType;
}

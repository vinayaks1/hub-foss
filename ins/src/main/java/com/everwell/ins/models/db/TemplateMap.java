package com.everwell.ins.models.db;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "ins_template_map")
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class TemplateMap {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter
    private Long id;
    @Column(name = "template_id")
    private Long templateId;
    @Column(name = "vendor_template_id")
    private String vendorTemplateId;

    public TemplateMap(Long templateId, String vendorTemplateId) {
        this.templateId = templateId;
        this.vendorTemplateId = vendorTemplateId;
    }

}

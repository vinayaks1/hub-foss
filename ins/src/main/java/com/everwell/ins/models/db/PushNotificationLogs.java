package com.everwell.ins.models.db;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "ins_pn_logs")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PushNotificationLogs {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter
    private Long id;
    @Column(name = "entity_id")
    private String entityId;
    @Column(name = "vendor_id")
    private Long vendorId;
    @Column(name = "message")
    private String message;
    @Column(name = "added_on")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private Date addedOn;
    @Column(name = "status")
    private String status;
    @Column(name = "template_id")
    private Long templateId;
    @Column(name = "trigger_id")
    private Long triggerId;
    @Column(name = "device_id", columnDefinition = "TEXT")
    private String deviceId;
    @Column(name = "firebase_response", columnDefinition = "TEXT")
    private String firebaseResponse;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "template_id", referencedColumnName = "id", insertable = false, updatable = false)
    private PushNotificationTemplate pushNotificationTemplate;

    public PushNotificationLogs(Long vendorId, Long templateId, Long triggerId, String entityId, String message, Date addedOn, String firebaseResponse, String deviceId) {
        this.entityId = entityId;
        this.vendorId = vendorId;
        this.templateId = templateId;
        this.triggerId = triggerId;
        this.addedOn = addedOn;
        this.message = message;
        this.firebaseResponse = firebaseResponse;
        this.deviceId = deviceId;
    }

}

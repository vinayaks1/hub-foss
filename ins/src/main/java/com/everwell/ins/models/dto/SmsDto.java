package com.everwell.ins.models.dto;

import lombok.*;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class SmsDto implements Serializable {
    String id;
    String phone;
    String message;
}

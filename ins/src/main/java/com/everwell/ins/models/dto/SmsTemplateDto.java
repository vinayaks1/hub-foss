package com.everwell.ins.models.dto;

import lombok.*;

import java.io.Serializable;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class SmsTemplateDto implements Serializable {
    String id;
    String phone;
    Map<String, String> parameters;
    Long languageId;

    public SmsTemplateDto(String id, String phone, Map<String, String> parameters) {
        this.id = id;
        this.phone = phone;
        this.parameters = parameters;
    }
}

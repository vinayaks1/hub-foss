package com.everwell.ins.models.db;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "ins_sms_logs")
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class SmsLogs {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter
    private Long id;
    @Column(name = "entity_id")
    private String entityId;
    @Column(name = "vendor_id")
    private Long vendorId;
    @Column(name = "phone_number")
    private String phoneNumber;
    @Column(name = "message")
    private String message;
    @Column(name = "added_on")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private Date addedOn;
    @Setter
    @Column(name = "api_response")
    private String apiResponse;
    @Setter
    @Column(name = "message_id")
    private String messageId;
    @Column(name = "status")
    private String status;
    @Column(name = "template_id")
    private Long templateId;
    @Column(name = "trigger_id")
    private Long triggerId;

    public SmsLogs(Long vendorId, Long templateId,Long triggerId, String entityId, String phoneNumber, String message, Date addedOn, String apiResponse, String messageId) {
        this.entityId = entityId;
        this.vendorId = vendorId;
        this.templateId = templateId;
        this.triggerId = triggerId;
        this.addedOn = addedOn;
        this.phoneNumber = phoneNumber;
        this.message = message;
        this.apiResponse = apiResponse;
        this.messageId = messageId;
    }

}

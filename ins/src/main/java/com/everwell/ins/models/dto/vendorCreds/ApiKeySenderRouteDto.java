package com.everwell.ins.models.dto.vendorCreds;

import lombok.Data;

@Data
public class ApiKeySenderRouteDto extends VendorCredsBase {
    String apiKey;
    String userName;
    String originator;
    String senderId;
    String creatorId;
    String url;
    String country;
    String countryCode;
    String reconciliationUrl;
}

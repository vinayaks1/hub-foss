package com.everwell.ins.models.dto.vendorCreds;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "credentialType", include = JsonTypeInfo.As.EXISTING_PROPERTY)
@JsonSubTypes({
        @JsonSubTypes.Type(value = ApiKeyFromDto.class, name = "APIKEYFROM"),
        @JsonSubTypes.Type(value = AuthIdAuthTokenDto.class, name = "AUTHIDAUTHTOKEN"),
        @JsonSubTypes.Type(value = AuthTokenFromDto.class, name = "AUTHTOKENFROM"),
        @JsonSubTypes.Type(value = ApiKeySenderDto.class, name = "APIKEYSENDER"),
        @JsonSubTypes.Type(value = UserNamePinDto.class, name = "USERNAMEPIN"),
        @JsonSubTypes.Type(value = UserKeyServerUrlDto.class, name = "USERKEYSERVERURL"),
        @JsonSubTypes.Type(value = PassPhraseAppIdDto.class, name = "PASSPHRASEAPPID"),
        @JsonSubTypes.Type(value = ClientIdUrlDto.class, name = "CLIENTIDURL"),
        @JsonSubTypes.Type(value = ApiKeySenderRouteDto.class, name = "APIKEYSENDERROUTE"),
        @JsonSubTypes.Type(value= JSONFormatDto.class, name = "JSONFORMAT"),
        @JsonSubTypes.Type(value = ApiKeyDto.class, name = "APIKEY"),
        @JsonSubTypes.Type(value = ApiKeySecretDto.class, name = "APIKEYSECRET")
})
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class VendorCredsBase {
    String credentialType;
}

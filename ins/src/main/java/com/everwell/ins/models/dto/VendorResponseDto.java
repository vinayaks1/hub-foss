package com.everwell.ins.models.dto;

import lombok.*;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class VendorResponseDto implements Serializable {
    String apiResponse;
    String messageId;
}

package com.everwell.ins.models.http.requests;

import com.everwell.ins.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EpisodeNotificationSearchRequest {
    List<Long> episodeIdList;

    public void validate(EpisodeNotificationSearchRequest episodeNotificationSearchRequest) {
        if (CollectionUtils.isEmpty(episodeNotificationSearchRequest.getEpisodeIdList())) {
            throw new ValidationException("Episode list cannot be empty!");
        }
    }
}

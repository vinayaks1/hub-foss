package com.everwell.ins.models.dto.vendorCreds;

import lombok.*;

@Data
public class PassPhraseAppIdDto extends VendorCredsBase {
    String passPhrase;
    String url;
    String appId;
    String appSecret;
}

package com.everwell.ins.models.dto;

import lombok.*;

import java.io.Serializable;
import java.util.Map;


@NoArgsConstructor
@Getter
@Setter
@ToString
public class PushNotificationTemplateDto implements Serializable {
    String id;
    String deviceId;
    Map<String, String> headingParameters;
    Map<String, String> contentParameters;
    String message;
    String heading;
    Long languageId;
    Boolean defaultConsent;

    public PushNotificationTemplateDto(String id, String deviceId, Map<String, String> contentParameters, Map<String, String> headingParameters) {
        this.id = id;
        this.deviceId = deviceId;
        this.contentParameters = contentParameters;
        this.headingParameters = headingParameters;
    }

    public PushNotificationTemplateDto(String id, String deviceId, String message, String heading) {
        this.id = id;
        this.deviceId = deviceId;
        this.message = message;
        this.heading = heading;
    }
}


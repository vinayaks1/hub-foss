package com.everwell.ins.models.http.requests;

import com.everwell.ins.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateEpisodeNotificationRequest {
    Long notificationId;
    String status;
    Boolean read;

    public void validate(UpdateEpisodeNotificationRequest updateEpisodeNotificationRequest) {
        if (null == updateEpisodeNotificationRequest.getNotificationId()) {
            throw new ValidationException("Notification id cannot be empty!");
        }

        if (StringUtils.isEmpty(updateEpisodeNotificationRequest.getStatus()) && null == updateEpisodeNotificationRequest.getRead()) {
            throw new ValidationException("Update parameters are missing");
        }
    }
}

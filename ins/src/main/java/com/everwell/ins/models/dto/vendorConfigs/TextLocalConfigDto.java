package com.everwell.ins.models.dto.vendorConfigs;

import lombok.*;

@Data
public class TextLocalConfigDto extends VendorConfigsBase {
    String bulkSmsLimit;
}

package com.everwell.ins.models.http.responses;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@AllArgsConstructor
@ToString
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TemplateResponse {
    Long templateId;
    String content;
    String parameters;
    String type;
    String language;
}

package com.everwell.ins.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JwtPayload {
    String sub;
    Long exp;
    Long iat;
}

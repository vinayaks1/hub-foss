package com.everwell.ins.models.db;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "episode_notification")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class EpisodeNotification {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter
    private Long id;

    @Column(name = "pn_log_id")
    private Long pnLogId;

    @Column(name = "client_id")
    private Long clientId;

    @Column(name = "created_on")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdOn;

    @Column(name = "read")
    private Boolean read;

    @Column(name = "status")
    private String status;

    @Column(name = "extras", columnDefinition = "jsonb")
    @org.hibernate.annotations.Type( type = "jsonb" )
    private Object extras;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "pn_log_id", referencedColumnName = "id", insertable = false, updatable = false)
    private PushNotificationLogs pushNotificationLogs;

    public EpisodeNotification(Long pnLogId, Long clientId, LocalDateTime createdOn, Boolean read, Object extras) {
        this.pnLogId = pnLogId;
        this.createdOn = createdOn;
        this.clientId = clientId;
        this.read = read;
        this.extras = extras;
    }
}

package com.everwell.ins.config;


import com.everwell.ins.models.db.Vendor;
import com.everwell.ins.models.dto.vendorCreds.JSONFormatDto;
import com.everwell.ins.services.VendorService;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import javax.annotation.PostConstruct;
import java.util.List;

@Configuration
public class FCMInitializer {

    @Autowired
    protected VendorService vendorService;

    private JSONFormatDto jsonCreds;

    @Bean
    @PostConstruct
    public void initialize() throws IOException {
        List<Vendor> vendors = vendorService.getVendors("FIREBASE");

        for (Vendor vendor : vendors) {
            String cred = vendor.getCredentials();
            jsonCreds = vendorService.getVendorProfileWithType(cred, JSONFormatDto.class);
            GoogleCredentials googleCredentials = GoogleCredentials
                    .fromStream(new ByteArrayInputStream(cred.getBytes()));
            FirebaseOptions firebaseOptions = FirebaseOptions
                    .builder()
                    .setCredentials(googleCredentials)
                    .build();
            FirebaseApp.initializeApp(firebaseOptions,jsonCreds.getProject_id());

        }

    }

}



package com.everwell.ins.config;

import com.everwell.ins.exceptions.rmq.RmqExceptionStrategy;
import com.everwell.ins.services.RabbitMQPublisherService;
import com.everwell.ins.services.impl.RabbitMQPublisherServiceImpl;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.RabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.amqp.SimpleRabbitListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class RabbitMQConfig {

    @Value("${q.ins.process_sms}")
    private String processSmsQueue;

    @Value("${q.ins.process_notification}")
    private String processNotificationQueue;

    @Value("${q.ins.process_email}")
    private String processEmailQueue;

    @Value("${q.ins.process_sms_reconciliation}")
    private String processSmsReconciliationQueue;

    @Profile("sender")
    @Bean
    public RabbitMQPublisherService sender() {
        return new RabbitMQPublisherServiceImpl();
    }

    //default prefetch count is of 250
    @Bean
    public RabbitListenerContainerFactory<SimpleMessageListenerContainer> rmqPrefetchCount20(
            SimpleRabbitListenerContainerFactoryConfigurer configurer,
            ConnectionFactory rabbitConnectionFactory
    ) {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        configurer.configure(factory, rabbitConnectionFactory);
        factory.setPrefetchCount(20);
        factory.setErrorHandler(new RmqExceptionStrategy());
        return factory;
    }

    @Bean
    Queue processSmsQueue() {
        return new Queue(processSmsQueue, true);
    }

    @Bean
    Queue processEmailQueue() {
        return new Queue(processEmailQueue, true);
    }

    @Bean
    Queue processNotificationQueue() {
        return new Queue(processNotificationQueue, true);
    }

    @Bean
    Queue processSmsReconciliationQueue() {
        return new Queue(processSmsReconciliationQueue, true);
    }
}

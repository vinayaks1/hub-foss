package com.everwell.ins.config;

import com.everwell.ins.constants.RabbitMQConstants;
import com.everwell.ins.services.RabbitMQPublisherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RmqDlxHelper {

    private static Logger LOGGER = LoggerFactory.getLogger(RmqDlxHelper.class);
    private static int BASE_RETRY_DELAY = 10000;

    public static RabbitMQPublisherService rabbitMQPublisherService;

    @Autowired
    RmqDlxHelper(RabbitMQPublisherService rabbitMQPublisherService) {
        this.rabbitMQPublisherService = rabbitMQPublisherService;
    }

    public static void triggerRetryMechanism(Message message, String deadLetterExchange, String routingKey, String waitExchange) {
        int retryCount = Integer.valueOf((String) message.getMessageProperties().getHeaders().getOrDefault(RabbitMQConstants.RETRY_COUNT_HEADER, "0"));
        if (retryCount >= RabbitMQConstants.RMQ_MAX_RETRY_COUNT) {
            LOGGER.info("[triggerRetryMechanism] moving to DLX: " + deadLetterExchange);
            rabbitMQPublisherService.send(new String(message.getBody()), routingKey, deadLetterExchange, message.getMessageProperties().getHeaders());
        } else {
            message.getMessageProperties().setHeader(RabbitMQConstants.RETRY_COUNT_HEADER, String.valueOf(retryCount + 1));
            rabbitMQPublisherService.send(new String(message.getBody()), routingKey, waitExchange, String.valueOf(BASE_RETRY_DELAY * (retryCount + 1)), message.getMessageProperties().getHeaders());
            LOGGER.info("[triggerRetryMechanism] retry count: " + (retryCount + 1) + " for routing key: " + routingKey);
        }
    }

}

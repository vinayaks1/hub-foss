package com.everwell.ins.factory;

import com.everwell.ins.enums.EmailGateway;
import com.everwell.ins.enums.SmsGateway;
import com.everwell.ins.enums.PushNotificationGateway;
import com.everwell.ins.vendors.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class VendorFactory {

    private Map<SmsGateway, SmsHandler> vendorMap = new HashMap<>();
    private Map<PushNotificationGateway, PushNotificationHandler> vendorMapPn = new HashMap<>();
    private Map<EmailGateway, EmailHandler> vendorMapEmail = new HashMap<>();

    @Autowired
    public VendorFactory(List<SmsHandler> smsHandlerList, List<PushNotificationHandler> PushNotificationHandlerList, List<EmailHandler> emailHandlerList) {
        if (!CollectionUtils.isEmpty(smsHandlerList)) {
            for (SmsHandler smsHandler : smsHandlerList) {
                vendorMap.put(smsHandler.getVendorGatewayEnumerator(), smsHandler);
            }
        }

        if (!CollectionUtils.isEmpty(PushNotificationHandlerList)) {
            for (PushNotificationHandler pushNotificationHandler : PushNotificationHandlerList) {
                vendorMapPn.put(pushNotificationHandler.getVendorGatewayEnumerator(), pushNotificationHandler);
            }
        }

        if (!CollectionUtils.isEmpty(emailHandlerList)) {
            for (EmailHandler emailHandler : emailHandlerList) {
                vendorMapEmail.put(emailHandler.getVendorGatewayEnumerator(), emailHandler);
            }
        }
    }

    public VendorFactory(List<SmsHandler> smsHandlerList)
    {
        new VendorFactory(smsHandlerList,null, null);
    }


    public SmsHandler getHandler(SmsGateway smsGateway) {
        return vendorMap.get(smsGateway);
    }

    public PushNotificationHandler getPushNotificationHandler(PushNotificationGateway pushNotificationGateway) {
        return vendorMapPn.get(pushNotificationGateway);
    }

    public EmailHandler getEmailHandler(EmailGateway emailGateway) {
        return vendorMapEmail.get(emailGateway);
    }

}

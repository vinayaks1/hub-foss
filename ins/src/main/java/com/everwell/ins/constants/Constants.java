package com.everwell.ins.constants;

import java.util.HashMap;
import java.util.Map;

public class Constants {
    public static Integer BATCH_SIZE_UNIQUE_SMS = 1;
    public static Integer BATCH_SIZE_UNIQUE_PN = 1;
    public static Integer BATCH_SIZE_UNIQUE_EMAIL = 1;
    public static String TEST = "TEST";
    public static final String INS_CLIENT_ID = "INS-Client-Id";
    public final static String DEFAULT_TEMPLATE_ID = "0";
    public static final String TEST_PREFIX = "[TEST-SEND]";
    public static final String[] AUTH_WHITELIST = {
            "/v1/client",
            // -- swagger ui
            "/v2/api-docs",
            "/configuration/ui",
            "/swagger-resources/**",
            "/configuration/security",
            "/swagger-ui.html",
            "/webjars/**",
            "/v1/reconciliation/**",
            "/actuator/**"
    };
    public static final String FIREBASE_EXCEPTION = "Requested entity was not found.";
    public static final Map<String,String> FIREBASE_PROJECTS = new HashMap<String,String>() {{
        put("nikshay-app","29");
        put("everwell-mobile","63");
    }};

    public final static String RMQ_HEADER_CLIENT_ID = "CLIENT_ID";

    public static final String CLASS_INTENT = "class_name";
    public static final String INTENT_EXTRAS = "intent_extras";
    public static final String NOTIFICATION_TITLE = "title";
    public static final String NOTIFICATION_BODY = "content";


}

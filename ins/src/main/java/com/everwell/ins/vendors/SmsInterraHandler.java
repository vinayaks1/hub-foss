package com.everwell.ins.vendors;

import com.everwell.ins.constants.Constants;
import com.everwell.ins.enums.Language;
import com.everwell.ins.enums.SmsGateway;
import com.everwell.ins.exceptions.NotImplementedException;
import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.exceptions.VendorException;
import com.everwell.ins.models.dto.SmsDto;
import com.everwell.ins.models.db.SmsLogs;
import com.everwell.ins.models.dto.VendorResponseDto;
import com.everwell.ins.models.dto.vendorConfigs.InterraConfigDto;
import com.everwell.ins.models.dto.vendorCreds.ApiKeySenderRouteDto;
import com.everwell.ins.utils.HttpUtils;
import com.everwell.ins.utils.Utils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Component
@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SmsInterraHandler extends SmsHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(SmsInterraHandler.class);

    private InterraConfigDto config;

    private ApiKeySenderRouteDto creds;

    @Override
    public void setVendorParams(Long vendorId) {
        config = vendorService.getVendorConfig(vendorId, InterraConfigDto.class);
        creds = vendorService.getVendorCredentials(vendorId, ApiKeySenderRouteDto.class);
    }

    @Override
    public SmsGateway getVendorGatewayEnumerator() {
        return SmsGateway.INTERRA;
    }

    @Override
    public VendorResponseDto vendorCall(List<SmsDto> persons, Language language, Long templateId) {
        return null;
    }

    @Override
    public String getLanguageMapping(Language language) {
        throw new NotImplementedException("Unimplemented method");
    }

    @Override
    public Integer getBatchSize(Boolean isMessageCommon) {
        return (isMessageCommon && null != config.getBulkSmsLimit()) ? Integer.parseInt(config.getBulkSmsLimit()) : Constants.BATCH_SIZE_UNIQUE_SMS;
    }

    @Override
    public CompletableFuture<List<SmsLogs>> processSms(List<SmsDto> smsDto, Language language, Long vendorId, Long templateId, Long triggerId) {
        List<SmsLogs> smsLogCollection = new ArrayList<>();
        VendorResponseDto responseDto = new VendorResponseDto();
        smsLogCollection.addAll(createLogs(smsDto, responseDto, vendorId, templateId, triggerId));
        smsLogCollection = smsLogsRepository.saveAll(smsLogCollection);
        if (!isMock) {
            responseDto = vendorCallInterra(smsDto, language, smsLogCollection.get(0).getId());
        }
        final VendorResponseDto finalResponseDto = responseDto;
        smsLogCollection.forEach(smsLogs -> {
            smsLogs.setApiResponse(finalResponseDto.getApiResponse());
            smsLogs.setMessageId(finalResponseDto.getMessageId());
        });
        return CompletableFuture.completedFuture(smsLogCollection);
    }

    public VendorResponseDto vendorCallInterra(List<SmsDto> persons, Language language, Long referenceNumber) {
        if (null == persons) {
            LOGGER.error("[vendorCall] The person list is empty");
            throw new ValidationException("The person list is empty");
        }
        VendorResponseDto responseDto = new VendorResponseDto();
        String phone = persons.stream().map(m -> Utils.removePrefix(m.getPhone(), "+"))
                .collect(Collectors.joining(","));
        String message = persons.get(0).getMessage();
        try {
            JSONObject params = new JSONObject();
            params.put("Content", message);
            params.put("Destination", phone);
            params.put("Originator", creds.getOriginator());
            params.put("SendorId", creds.getSenderId());
            params.put("CreatorId", creds.getCreatorId());
            params.put("Country", creds.getCountry());
            params.put("CountryCode", creds.getCountryCode());
            params.put("ReferenceNumber", referenceNumber.toString());
            Map<String, String> headers = new HashMap<String, String>() {{
                put("username", creds.getUserName());
                put("api-key", creds.getApiKey());
            }};
            LocalDateTime start = LocalDateTime.now();
            String response = HttpUtils.postJsonRequest(creds.getUrl(), params.toString(), headers);
            responseDto.setApiResponse(response);
            responseDto.setMessageId(referenceNumber.toString());
            Long elapsed = Duration.between(start, LocalDateTime.now()).toMillis();
            LOGGER.info("[VendorCall] :  Sent SMS " + message + " to " + phone + " took " + elapsed.toString() + "ms, response = " + responseDto.getApiResponse());
        } catch (Exception e) {
            String errorMessage = "Interra unable to send SMS to " + phone + ", exception: " + e;
            LOGGER.error("[vendorCall] " + errorMessage);
            throw new VendorException(errorMessage, fetchExtraData(getVendorGatewayEnumerator().toString(), phone, message, referenceNumber));
        }
        return responseDto;
    }
}


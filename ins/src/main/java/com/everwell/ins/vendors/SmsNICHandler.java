package com.everwell.ins.vendors;

import com.everwell.ins.constants.Constants;
import com.everwell.ins.enums.Language;
import com.everwell.ins.enums.SmsGateway;
import com.everwell.ins.exceptions.NotImplementedException;
import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.exceptions.VendorException;
import com.everwell.ins.models.dto.SmsDto;
import com.everwell.ins.models.dto.vendorConfigs.NICConfigDto;
import com.everwell.ins.models.dto.vendorCreds.UserNamePinDto;
import com.everwell.ins.models.dto.VendorResponseDto;
import com.everwell.ins.services.TemplateMapService;
import com.everwell.ins.utils.HttpUtilsSsl;
import com.everwell.ins.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import java.net.URLEncoder;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SmsNICHandler extends SmsHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(SmsNICHandler.class);

    @Autowired
    private TemplateMapService templateMapService;

    private NICConfigDto config;

    private UserNamePinDto creds;

    @Value("${server.port}")
    private String port;

    @Override
    public void setVendorParams(Long vendorId) {
        config = vendorService.getVendorConfig(vendorId, NICConfigDto.class);
        creds = vendorService.getVendorCredentials(vendorId, UserNamePinDto.class);
    }

    @Override
    public SmsGateway getVendorGatewayEnumerator() {
        return SmsGateway.NIC;
    }

    @Override
    public String getLanguageMapping(Language language) {
        throw new NotImplementedException("Unimplemented method");
    }

    public String getMessageContent(String message, Language language) {
        String finalMessage = message;
        if (language == Language.UNICODE) {
            finalMessage = Utils.convertToUnicode(message);
        }
        return finalMessage;
    }

    public String getVendorTemplateId(Long templateId) {
        return templateMapService.getTemplateMap(templateId).getVendorTemplateId();
    }

    @Override
    public Integer getBatchSize(Boolean isMessageCommon) {
        return (isMessageCommon) ? Integer.parseInt(config.getBulkSmsLimit()) : Constants.BATCH_SIZE_UNIQUE_SMS;
    }

    @Override
    public VendorResponseDto vendorCall(List<SmsDto> persons, Language language, Long templateId) {
        if (null == persons) {
            LOGGER.error("[vendorCall] The person list is empty");
            throw new ValidationException("The person list is empty");
        }
        VendorResponseDto responseDto = new VendorResponseDto();
        String phone = persons.stream().map(SmsDto::getPhone)
                .collect(Collectors.joining(","));
        String message = persons.get(0).getMessage();

        try {
            LocalDateTime start = LocalDateTime.now();
            String endpoint = "?username=" + creds.getUserName() + "&pin=" + creds.getPin() + "&message=" + URLEncoder.encode(getMessageContent(message, language), "UTF-8") + "&mnumber=" + phone + "&signature=" + creds.getSignature() +
                    "&dlt_entity_id=" + creds.getEntityId() + "&dlt_template_id=" + getVendorTemplateId(templateId);
            LOGGER.info("Vendor API call: " + creds.getUrl() + endpoint);
            String respString = HttpUtilsSsl.getRequestForNIC(creds.getUrl() + endpoint);
            responseDto.setApiResponse(respString);
            if (respString != null) {
                int startingIndex = respString.indexOf('=') + 1;
                int endingIndex = respString.indexOf('~');
                String requestId = respString.substring(startingIndex, endingIndex);
                responseDto.setMessageId(requestId);
            }
            Long elapsed = Duration.between(start, LocalDateTime.now()).toMillis();
            LOGGER.info("[VendorCall] :  Sent SMS " + message + " to " + phone + " took " + elapsed.toString() + "ms, response = " + responseDto.getApiResponse());
        } catch (Exception e) {
            String errorMessage = "NIC unable to send SMS to " + phone + ", exception: " + e;
            LOGGER.error("[vendorCall] " + errorMessage);
            throw new VendorException(errorMessage, fetchExtraData(getVendorGatewayEnumerator().toString(), phone, message, templateId));
        }
        return responseDto;
    }
}


package com.everwell.ins.vendors;

import com.everwell.ins.enums.Language;
import com.everwell.ins.enums.SmsGateway;
import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.exceptions.VendorException;
import com.everwell.ins.models.dto.SmsDto;
import com.everwell.ins.models.dto.vendorConfigs.ICCDDRBConfigDto;
import com.everwell.ins.models.dto.vendorCreds.UserKeyServerUrlDto;
import com.everwell.ins.models.dto.VendorResponseDto;
import com.everwell.ins.utils.HttpUtils;
import com.everwell.ins.exceptions.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SmsICDDRBHandler extends SmsHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(SmsICDDRBHandler.class);

    private ICCDDRBConfigDto config;

    private UserKeyServerUrlDto creds;

    @Override
    public void setVendorParams(Long vendorId) {
        config = vendorService.getVendorConfig(vendorId, ICCDDRBConfigDto.class);
        creds = vendorService.getVendorCredentials(vendorId, UserKeyServerUrlDto.class);
    }

    @Override
    public SmsGateway getVendorGatewayEnumerator() {
        return SmsGateway.ICCDDRB;
    }

    @Override
    public String getLanguageMapping(Language language) {
        throw new NotImplementedException("Unimplemented method");
    }

    @Override
    public VendorResponseDto vendorCall(List<SmsDto> persons, Language language, Long templateId) {
        if (null == persons) {
            LOGGER.error("[vendorCall] The person list is empty");
            throw new ValidationException("The person list is empty");
        }
        VendorResponseDto responseDto = new VendorResponseDto();
        String phone = persons.get(0).getPhone();
        String message = persons.get(0).getMessage();
        try {
            Map<String, List<String>> values = new HashMap<String, List<String>>() {{
                put("message", Arrays.asList(message));
                put("msisdn", Arrays.asList(phone));
                put("server", Arrays.asList(creds.getServerUrl()));
                put("userPassKey", Arrays.asList(creds.getUserKey()));
            }};
            LocalDateTime start = LocalDateTime.now();

            String response = HttpUtils.postFormRequest(creds.getUrl(), values);
            responseDto.setApiResponse(response);
            Long elapsed = Duration.between(start, LocalDateTime.now()).toMillis();
            LOGGER.info("[VendorCall] :  Sent SMS " + message + " to " + phone + " took " + elapsed.toString() + "ms, response = " + responseDto.getApiResponse());
        } catch (Exception e) {
            String errorMessage = "ICCDDRB unable to send SMS to " + phone + ", exception: " + e;
            LOGGER.error("[vendorCall] " + errorMessage);
            throw new VendorException(errorMessage, fetchExtraData(getVendorGatewayEnumerator().toString(), phone, message, templateId));
        }
        return responseDto;
    }
}


package com.everwell.ins.vendors;

import com.everwell.ins.constants.Constants;
import com.everwell.ins.enums.Language;
import com.everwell.ins.enums.PushNotificationGateway;
import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.models.dto.PushNotificationTemplateDto;
import com.everwell.ins.models.dto.vendorConfigs.FirebaseConfigDto;
import com.everwell.ins.models.dto.vendorCreds.JSONFormatDto;
import com.everwell.ins.models.dto.FirebaseResponseDto;
import com.everwell.ins.models.http.requests.PushNotificationRequest;
import com.everwell.ins.exceptions.NotImplementedException;
import com.everwell.ins.utils.SentryUtils;
import com.google.firebase.FirebaseApp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import com.google.firebase.messaging.*;


import javax.annotation.PostConstruct;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class NotificationFirebaseHandler extends PushNotificationHandler {


    private static Logger LOGGER = LoggerFactory.getLogger(NotificationFirebaseHandler.class);

    private FirebaseConfigDto config;

    private JSONFormatDto creds;

    @Value("${server.port}")
    private String serverPort;

    @PostConstruct
    public void setBaseUrl() {
        this.baseUrl = "http://" + baseUrl + ":" + serverPort;
    }

    @Override
    public void setVendorParams(Long vendorId) {
        config = vendorService.getVendorConfig(vendorId, FirebaseConfigDto.class);
        creds = vendorService.getVendorCredentials(vendorId, JSONFormatDto.class);
    }

    @Override
    public Integer getBatchSize(Boolean isNotificationCommon) {
        return (isNotificationCommon && null != config.getBulkPNLimit()) ? Integer.parseInt(config.getBulkPNLimit()) : Constants.BATCH_SIZE_UNIQUE_PN;
    }

    @Override
    public PushNotificationGateway getVendorGatewayEnumerator() {
        return PushNotificationGateway.FIREBASE;
    }

    @Override
    public String getLanguageMapping(Language language) {
        throw new NotImplementedException("Unimplemented method");
    }

    @Override
    public FirebaseResponseDto firebaseCall(List<PushNotificationTemplateDto> persons, PushNotificationRequest pushNotificationRequest) {
        if (null == persons) {
            LOGGER.error("[vendorCall] The person list is empty");
            throw new ValidationException("The person list is empty");
        }
        FirebaseResponseDto responseDto = new FirebaseResponseDto();
        List<String> firebaseResponse = new ArrayList<>();
        List<String> invalidDeviceIds = new ArrayList<>();
        String heading = persons.get(0).getHeading();
        String content = persons.get(0).getMessage();
        Long timeToLive = TimeUnit.MINUTES.toMillis(pushNotificationRequest.getTimeToLive());
        String classIntent = pushNotificationRequest.getClassIntent();
        String intentExtras = pushNotificationRequest.getIntentExtras();
        persons.forEach(p-> {
            try {
                Message message = null;
                Notification notification = Notification
                        .builder()
                        .setTitle(heading)
                        .setBody(content)
                        .build();

                AndroidConfig androidConfig = AndroidConfig
                        .builder()
                        .setTtl(timeToLive)
                        .build();

                Message.Builder builder = Message
                        .builder()
                        .setToken(p.getDeviceId())
                       .setAndroidConfig(androidConfig);
                if(pushNotificationRequest.getSendNotificationData()) {
                    builder.setNotification(notification);
                }

                if(classIntent != null ) {
                    builder.putData(Constants.CLASS_INTENT, classIntent)
                            .putData(Constants.INTENT_EXTRAS, intentExtras)
                            .putData(Constants.NOTIFICATION_TITLE, heading)
                            .putData(Constants.NOTIFICATION_BODY, content);
                }

                message = builder.build();

                LocalDateTime start = LocalDateTime.now();
                FirebaseApp app = FirebaseApp.getInstance(creds.getProject_id());
                String response = FirebaseMessaging.getInstance(app).sendAsync(message).get();
                firebaseResponse.add(response);

                Long elapsed = Duration.between(start, LocalDateTime.now()).toMillis();
                LOGGER.info("[vendorCall] :  Sent Notification " + pushNotificationRequest.getHeading() + " to " + p.getDeviceId() + " took " + elapsed.toString() + "ms, response = " + response);
            } catch (Exception e) {
                LOGGER.error("[vendorCall] Firebase unable to send Notification to " + p.getDeviceId() + ", exception: " + e.toString());
                SentryUtils.captureException(e);
                firebaseResponse.add(e.getMessage());
                if(e.getCause()!=null  && e.getCause().getMessage().equals(Constants.FIREBASE_EXCEPTION))
                    invalidDeviceIds.add(p.getDeviceId());
            }});
        responseDto.setFirebaseResponse(firebaseResponse);
        responseDto.setInvalidDeviceIds(invalidDeviceIds);
        return responseDto;
    }
}

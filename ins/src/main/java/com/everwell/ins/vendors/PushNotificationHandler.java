package com.everwell.ins.vendors;

import com.everwell.ins.constants.Constants;
import com.everwell.ins.enums.Language;
import com.everwell.ins.enums.PushNotificationGateway;
import com.everwell.ins.exceptions.InternalServerErrorException;
import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.models.db.EpisodeNotification;
import com.everwell.ins.models.db.PushNotificationLogs;
import com.everwell.ins.models.db.PushNotificationTemplate;
import com.everwell.ins.models.dto.FirebaseResponseDto;
import com.everwell.ins.models.dto.PushNotificationRequestDto;
import com.everwell.ins.models.dto.PushNotificationTemplateDto;
import com.everwell.ins.models.dto.SmsDto;
import com.everwell.ins.models.dto.vendorCreds.JSONFormatDto;
import com.everwell.ins.models.http.requests.DeactivateDeviceIdsRequest;
import com.everwell.ins.models.http.requests.PushNotificationRequest;
import com.everwell.ins.models.http.requests.SmsRequest;
import com.everwell.ins.repositories.PushLogsRepository;
import com.everwell.ins.repositories.PushTemplateRepository;
import com.everwell.ins.services.EpisodeNotificationService;
import com.everwell.ins.services.TemplateService;
import com.everwell.ins.services.VendorService;
import com.everwell.ins.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.collect.Lists;
import io.sentry.Sentry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Async;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public abstract class PushNotificationHandler {
    private static Logger LOGGER = LoggerFactory.getLogger(PushNotificationHandler.class);

    @Value("${ins.is.mock}")
    protected Boolean isMock;

    @Value("${ins.base.url}")
    protected String baseUrl;

    @Autowired
    protected VendorService vendorService;

    @Autowired
    protected PushLogsRepository pushLogsRepository;
    
    @Autowired
    private EpisodeNotificationService episodeNotificationService;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    protected List<String> invalidDeviceIds = new ArrayList<>();

    private JSONFormatDto creds;


    public void sendNotification(PushNotificationRequest pushNotificationRequest) throws InterruptedException, ExecutionException, ValidationException {
        setVendorParams(pushNotificationRequest.getVendorId());
        preprocessParams(pushNotificationRequest);
        applicationEventPublisher.publishEvent(pushNotificationRequest);
        List<List<PushNotificationTemplateDto>> batches = createBatches(pushNotificationRequest);
        List<PushNotificationLogs> pushNotificationLogCollection = new ArrayList<>();
        List<CompletableFuture<List<PushNotificationLogs>>> completableFutureList = new ArrayList<>();
        for (List<PushNotificationTemplateDto> batch : batches) {
            completableFutureList.add(processNotification(batch, pushNotificationRequest));
        }
        CompletableFuture.allOf(completableFutureList.toArray(new CompletableFuture[0])).join();

        completableFutureList.forEach(f -> {
            try {
                pushNotificationLogCollection.addAll(f.get());
            } catch (InterruptedException | ExecutionException e) {
                LOGGER.error("Unable to add to logs", e.toString());
                throw new InternalServerErrorException("Internal Server Error, Try again later!");
            }
        });
        List<PushNotificationLogs> logs = pushLogsRepository.saveAll(pushNotificationLogCollection);
        if(invalidDeviceIds.size() != 0)
        {
            creds = vendorService.getVendorCredentials(pushNotificationRequest.getVendorId(),JSONFormatDto.class);
            String clientId = Constants.FIREBASE_PROJECTS.get(creds.getProject_id());
            DeactivateDeviceIdsRequest deactivateDeviceIdsRequest = new DeactivateDeviceIdsRequest(invalidDeviceIds,clientId);
            applicationEventPublisher.publishEvent(deactivateDeviceIdsRequest);
        }
        if (pushNotificationRequest.getEpisodeNotification()) {
            List<EpisodeNotification> episodeNotifications = new ArrayList<>();
            logs.forEach(l -> {
                try {
                    Object extras = null;
                    if (StringUtils.hasLength(pushNotificationRequest.getNotificationExtraData())) {
                        extras = Utils.jsonToObject(pushNotificationRequest.getNotificationExtraData(), new TypeReference<Object>() {});
                    }
                    EpisodeNotification notification = new EpisodeNotification(l.getId(), pushNotificationRequest.getClientId(), LocalDateTime.now(), false,  extras);
                    episodeNotifications.add(notification);
                } catch (IOException e) {
                    Sentry.capture(e);
                    throw new RuntimeException(e);
                }
            });
            episodeNotificationService.saveNotificationBulk(episodeNotifications);
        }
    }

    @Async
    public CompletableFuture<List<PushNotificationLogs>> processNotification(List<PushNotificationTemplateDto> pushNotificationDto, PushNotificationRequest pushNotificationRequest) throws InterruptedException, ExecutionException {
        List<PushNotificationLogs> pushNotificationLogCollection = new ArrayList<>();
        FirebaseResponseDto responseDto = new FirebaseResponseDto();
        if (!isMock) {
            responseDto = firebaseCall(pushNotificationDto, pushNotificationRequest);
        }
        pushNotificationLogCollection.addAll(createLogs(pushNotificationDto, responseDto, pushNotificationRequest.getVendorId(), pushNotificationRequest.getTemplateId(), pushNotificationRequest.getTriggerId()));
        if(responseDto.getInvalidDeviceIds() != null)
            invalidDeviceIds.addAll(responseDto.getInvalidDeviceIds());
        return CompletableFuture.completedFuture(pushNotificationLogCollection);
    }


    public PushNotificationRequest preprocessParams(PushNotificationRequest pushNotificationRequest) throws ValidationException {

        pushNotificationRequest.setMessage(Utils.trimMessage(pushNotificationRequest.getMessage()));
        pushNotificationRequest.getPersonList().forEach(p -> {
            p.setMessage((pushNotificationRequest.getIsNotificationCommon()) ? pushNotificationRequest.getMessage() : Utils.trimMessage(p.getMessage()));
        });
        return pushNotificationRequest;
    }

    public List<List<PushNotificationTemplateDto>> createBatches(PushNotificationRequest pushNotificationRequest) {
        return Lists.partition(pushNotificationRequest.getPersonList(), getBatchSize(pushNotificationRequest.getIsNotificationCommon()));
    }

    public Integer getBatchSize(Boolean isNotificationCommon) {
        return Constants.BATCH_SIZE_UNIQUE_PN;
    }

    public abstract void setVendorParams(Long vendorId);

    public abstract String getLanguageMapping(Language language);

    public abstract PushNotificationGateway getVendorGatewayEnumerator();

    public abstract FirebaseResponseDto firebaseCall(List<PushNotificationTemplateDto> persons, PushNotificationRequest pushNotificationRequest);


    public List<PushNotificationLogs> createLogs(List<PushNotificationTemplateDto> persons, FirebaseResponseDto responseDto, Long vendorId, Long templateId, Long triggerId) {
        List<PushNotificationLogs> pushNotificationLogCollection = new ArrayList<>();
        Date addedOn = new Date();
        for(int index = 0; index < persons.size(); index++) {
            String firebase_response;
            if (isMock) {
                 firebase_response = Constants.TEST;
            }
            else {
                 firebase_response = responseDto.getFirebaseResponse().get(index);
            }
            pushNotificationLogCollection.add(new PushNotificationLogs(vendorId, templateId, triggerId, persons.get(index).getId(), persons.get(index).getMessage(), addedOn, firebase_response, persons.get(index).getDeviceId()));
        }
        return pushNotificationLogCollection;
    }


}

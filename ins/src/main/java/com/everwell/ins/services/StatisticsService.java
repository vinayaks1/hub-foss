package com.everwell.ins.services;

import com.everwell.ins.models.http.requests.StatisticsRequest;

public interface StatisticsService {

    Long getNotificationsCount(StatisticsRequest statisticsRequest);

}

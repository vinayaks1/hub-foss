package com.everwell.ins.services.impl;

import com.everwell.ins.exceptions.ConflictException;
import com.everwell.ins.exceptions.NotFoundException;
import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.models.db.Engagement;
import com.everwell.ins.models.dto.PushNotificationTemplateDto;
import com.everwell.ins.models.dto.SmsDto;
import com.everwell.ins.models.dto.SmsRequestDto;
import com.everwell.ins.models.dto.SmsTemplateDto;
import com.everwell.ins.models.http.requests.EngagementBulkRequest;
import com.everwell.ins.models.http.requests.EngagementRequest;
import com.everwell.ins.models.http.requests.PushNotificationRequest;
import com.everwell.ins.models.http.requests.SmsDetailedRequest;
import com.everwell.ins.models.http.requests.SmsRequest;
import com.everwell.ins.repositories.EngagementRepository;
import com.everwell.ins.services.EngagementService;
import com.everwell.ins.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.lang.reflect.GenericArrayType;
import java.text.ParseException;
import java.util.*;
import java.util.function.Function;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import static com.everwell.ins.enums.EngagementType.PUSH_NOTIFICATION;

@Service
public class EngagementServiceImpl implements EngagementService {

    @Autowired
    private EngagementRepository engagementRepository;

    public Long saveEngagementConfig(EngagementRequest engagementRequest) {
        Engagement engagement = engagementRepository.findByEntityIdAndTypeId(engagementRequest.getEntityId(), engagementRequest.getTypeId());
        if (null != engagement) {
            throw new ConflictException("Engagement Config with id " + engagementRequest.getEntityId() + " and type id " + engagementRequest.getTypeId() + " already exists!");
        }
        engagement = new Engagement(engagementRequest);
        return engagementRepository.save(engagement).getId();
    }

    public Engagement updateEngagementConfig(EngagementRequest engagementRequest) {
        Engagement engagement = engagementRepository.findByEntityIdAndTypeId(engagementRequest.getEntityId(), engagementRequest.getTypeId());
        if (null == engagement) {
            throw new NotFoundException("Engagement Config with id " + engagementRequest.getEntityId() + " and type id " + engagementRequest.getTypeId() + " not found!");
        }
        Engagement newEngagement = new Engagement(engagementRequest);
        engagement.update(newEngagement);
        return engagementRepository.save(engagement);
    }

    public Engagement getEngagementConfig(String entityId, Long typeId) {
        Engagement engagement = engagementRepository.findByEntityIdAndTypeId(entityId, typeId);
        if (null == engagement  ) {
            throw new NotFoundException("Engagement Config with id " + entityId + " and type id " + typeId + " not found!");
        }
        return engagement;
    }

    @Override
    public List<Engagement> getEngagementBulk(String entityId) {
        List<Engagement> engagement = engagementRepository.findAllByEntityId(entityId);
        if (CollectionUtils.isEmpty(engagement)) {
            throw new NotFoundException("Engagement Config with entity id " + entityId + " not found!");
        }
        return engagement;
    }

    public SmsRequestDto modifyList(SmsRequestDto smsRequestDto, Long engageTypeId) {
        List<String> entityList = smsRequestDto.getSmsRequest().getPersonList()
                .stream()
                .map(SmsDto::getId)
                .collect(Collectors.toList());
        List<Engagement> engagementConfig = engagementRepository.findByEntityIdInAndTypeId(entityList, engageTypeId);
        HashMap<String, Engagement> engagementHashMap = new HashMap<>();
        engagementConfig.forEach(engagement -> {
            engagementHashMap.put(engagement.getEntityId(), engagement);
        });
        List<SmsDto> personList = smsRequestDto.getSmsRequest().getPersonList()
                .stream()
                .filter(m -> {
                    Engagement engagement = engagementHashMap.getOrDefault(m.getId(), null);
                    if (null == engagement || null == engagement.getConsent()) {
                        return smsRequestDto.getSmsRequest().getDefaultConsent();
                    }
                    return engagement.getConsent();
                })
                .collect(Collectors.toList());
        smsRequestDto.getSmsRequest().setPersonList(personList);
        return smsRequestDto;
    }

    public SmsDetailedRequest filterSmsList(SmsDetailedRequest smsDetailedRequest, Long engageTypeId) {
        List<String> entityList = smsDetailedRequest.getPersonList()
                .stream()
                .map(SmsTemplateDto::getId)
                .collect(Collectors.toList());
        List<Engagement> engagementConfig = engagementRepository.findByEntityIdInAndTypeId(entityList, engageTypeId);
        HashMap<String, Engagement> engagementHashMap = new HashMap<>();
        engagementConfig.forEach(engagement -> {
            engagementHashMap.put(engagement.getEntityId(), engagement);
        });

        Boolean sharedConsent = true;
        Long sharedLanguage = null;
        if(null != smsDetailedRequest.getDefaultLanguage())
            sharedLanguage = Long.valueOf(smsDetailedRequest.getDefaultLanguage());

        if(null != smsDetailedRequest.getPatientList()) {
            List<String> entityListMappedToSamePhone = smsDetailedRequest.getPatientList()
                    .stream()
                    .map(Object::toString)
                    .collect(Collectors.toList());

            List<Engagement> engagementConfigWithSamePhone = engagementRepository.findByEntityIdInAndTypeId(entityListMappedToSamePhone, engageTypeId);
            sharedConsent = engagementConfigWithSamePhone
                    .stream()
                    .map(engagement -> engagement.getConsent())
                    .reduce((consent1, consent2) ->
                    {
                        return (consent1 && consent2);
                    }).get();
            if(!sharedConsent) {
                smsDetailedRequest.setPersonList(new ArrayList<>());
                return smsDetailedRequest;
            }
            HashSet<Long> languagePreferences = new HashSet<>();
            engagementConfigWithSamePhone.forEach(engagement -> languagePreferences.add(engagement.getLanguageId()));
            if (languagePreferences.size() == 1)
                sharedLanguage = languagePreferences.stream().findFirst().get();
        }

        String timeOfDay = null;
        if (null != smsDetailedRequest.getTimeOfSms()) {
            try {
                timeOfDay = Utils.getFormattedDate(Utils.convertStringToDate(smsDetailedRequest.getTimeOfSms()), "HH:mm:ss");
            } catch (ParseException e) {
                throw new ValidationException("unable to parse date");
            }
        }
        String finalTimeOfDay = timeOfDay;
        Long finalSharedLanguage = sharedLanguage;
        List<SmsTemplateDto> personList = smsDetailedRequest.getPersonList()
                .stream()
                .filter(m -> {
                    Engagement engagement = engagementHashMap.getOrDefault(m.getId(), null);
                    if(null != smsDetailedRequest.getPatientList())
                        m.setLanguageId(finalSharedLanguage);
                    else if (null != engagement && null != engagement.getLanguageId())
                        m.setLanguageId(engagement.getLanguageId());
                    if (smsDetailedRequest.getIsMandatory()) return true;
                    if (null == engagement) {
                        return smsDetailedRequest.getDefaultConsent() && smsDetailedRequest.getIsDefaultTime();
                    } else {
                        if (null == engagement.getConsent() || !engagement.getConsent()) return false;
                        String engagementTime = null;
                        if (null != engagement.getTime()) {
                            try {
                                engagementTime = Utils.getFormattedDate(engagement.getTime(), "HH:mm:ss");
                            } catch (Exception ignored) {
                            }
                        }
                        if (!smsDetailedRequest.getIsDefaultTime()) {
                            return null != engagement.getTime() && null != engagementTime && engagementTime.equals(finalTimeOfDay);
                        } else {
                            return null == engagement.getTime() || null == smsDetailedRequest.getTimeOfSms() || (null != engagementTime && engagementTime.equals(finalTimeOfDay));
                        }
                    }
                })
                .collect(Collectors.toList());
        smsDetailedRequest.setPersonList(personList);
        return smsDetailedRequest;
    }

    @Override
    public void saveEngagementConfigBulk(EngagementBulkRequest engagementBulkRequest) {
       List<EngagementRequest> engagementRequestList = engagementBulkRequest.getEngagementRequestList();
       List<Engagement> engagements = new ArrayList<>();
       engagementRequestList.forEach(engagementRequest -> {
           Engagement engagement = engagementRepository.findByEntityIdAndTypeId(engagementRequest.getEntityId(), engagementRequest.getTypeId());
           if (null != engagement) {
               throw new ConflictException("Engagement Config with id " + engagementRequest.getEntityId() + " already exists!");
           }
           engagement = new Engagement(engagementRequest);
           engagements.add(engagement);
       });
       engagementRepository.saveAll(engagements);
    }

    @Override
    public PushNotificationRequest filterPushNotificationList(PushNotificationRequest pushNotificationRequest) {

        List<PushNotificationTemplateDto> pushNotificationAllowedPersonList = pushNotificationRequest.getPersonList().stream().filter(person -> checkPushNotificationConsent(person.getId(), person.getDefaultConsent())).collect(Collectors.toList());

        pushNotificationRequest.setPersonList(pushNotificationAllowedPersonList);
        return pushNotificationRequest;
    }

    public boolean checkPushNotificationConsent(String entityId, Boolean defaultConsent) {
        Boolean pushNotificationConsent = defaultConsent != null ? defaultConsent : true;
        Engagement engagement = engagementRepository.findByEntityIdAndTypeId(entityId, PUSH_NOTIFICATION.getId());

        if (engagement != null) {
            pushNotificationConsent = engagement.getConsent();
        }
        return pushNotificationConsent;
    }

}

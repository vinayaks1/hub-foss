package com.everwell.ins.services;

import com.everwell.ins.models.db.Trigger;

import java.util.List;

public interface TriggerService {

    Trigger getTrigger(Long triggerId);

    List<Trigger> getAllTriggers();

}

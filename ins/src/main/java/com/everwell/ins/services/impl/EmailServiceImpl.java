package com.everwell.ins.services.impl;

import com.everwell.ins.enums.EmailGateway;
import com.everwell.ins.exceptions.NotFoundException;
import com.everwell.ins.factory.VendorFactory;
import com.everwell.ins.models.db.Vendor;
import com.everwell.ins.models.dto.EmailRequestDto;
import com.everwell.ins.models.http.requests.EmailRequest;
import com.everwell.ins.repositories.VendorRepository;
import com.everwell.ins.services.EmailService;
import com.everwell.ins.services.TemplateService;
import com.everwell.ins.utils.SentryUtils;
import com.everwell.ins.vendors.EmailHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmailServiceImpl implements EmailService {

    private static Logger LOGGER = LoggerFactory.getLogger(EmailServiceImpl.class);

    @Autowired
    private VendorRepository vendorRepository;

    @Autowired
    private VendorFactory vendorFactory;

    @Autowired
    private TemplateService templateService;

    @Override
    public void sendEmail(EmailRequest emailRequest) {
        LOGGER.info("[sendEmail] service called");
        try {
            if (!emailRequest.getRecipientList().isEmpty()) {
                Vendor vendor = vendorRepository.findById(emailRequest.getVendorId()).orElse(null);
                if (null == vendor) {
                    throw new NotFoundException("Vendor with id " + emailRequest.getVendorId() + " not found!");
                }
                EmailHandler emailHandler  = vendorFactory.getEmailHandler(EmailGateway.valueOf(vendor.getGateway()));
                emailHandler.sendEmail(emailRequest);
            }
        } catch (Exception e) {
            LOGGER.error("Unable to send email", e);
            SentryUtils.captureException(e);
        }
    }

    @Override
    public EmailRequest constructEmail(EmailRequest emailRequest) {
        EmailRequestDto emailRequestDto = null;
        emailRequestDto = templateService.constructEmail(emailRequest);
        return emailRequestDto.getEmailRequest();
    }
}

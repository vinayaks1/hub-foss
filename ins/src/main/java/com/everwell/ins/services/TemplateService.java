package com.everwell.ins.services;

import com.everwell.ins.models.db.PushNotificationTemplate;
import com.everwell.ins.models.db.Template;
import com.everwell.ins.models.dto.EmailRequestDto;
import com.everwell.ins.models.dto.SmsDetailedRequestDto;
import com.everwell.ins.models.dto.SmsRequestDto;
import com.everwell.ins.models.dto.PushNotificationRequestDto;
import com.everwell.ins.models.http.requests.*;

import java.util.List;

public interface TemplateService {

    Long saveTemplate(TemplateRequest templateRequest);

    Template getTemplate(Long templateId);

    PushNotificationTemplate getPNTemplate(Long templateId);

    List<Template> getAllTemplates();

    SmsRequestDto createSms(SmsTemplateRequest smsTemplateRequest);

    SmsDetailedRequestDto constructSms(SmsDetailedRequest smsDetailedRequest);

    PushNotificationRequestDto constructNotification(PushNotificationRequest pushNotificationRequest);

    EmailRequestDto constructEmail(EmailRequest emailRequest);

}

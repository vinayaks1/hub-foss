package com.everwell.ins.services.impl;

import com.everwell.ins.constants.Constants;
import com.everwell.ins.controllers.SmsController;
import com.everwell.ins.models.dto.EventStreamingDto;
import com.everwell.ins.models.dto.ReconciliationDto;
import com.everwell.ins.models.http.requests.EmailRequest;
import com.everwell.ins.models.http.requests.PushNotificationRequest;
import com.everwell.ins.models.http.requests.SmsDetailedRequest;
import com.everwell.ins.services.EventServiceConsumerService;
import com.everwell.ins.services.ReconciliationService;
import com.everwell.ins.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class EventServiceConsumerServiceImpl implements EventServiceConsumerService {

    @Autowired
    private SmsController smsController;

    @Autowired
    private PushNotificationServiceImpl pushNotificationServiceimpl;

    @Autowired
    private EmailServiceImpl emailService;

    @Autowired
    private ReconciliationService reconciliationService;

    private static Logger LOGGER = LoggerFactory.getLogger(EventServiceConsumerServiceImpl.class);

    @Override
    @RabbitListener(queues = "${q.ins.process_sms}", containerFactory = "rmqPrefetchCount20", concurrency = "2")
    public void processSmsConsumer(Message message) throws IOException {
        String body = new String(message.getBody());
        LOGGER.info("[processSmsConsumer] message received with body: " + body);
        EventStreamingDto<SmsDetailedRequest> eventStreamingDto = Utils.jsonToObject(body, new TypeReference<EventStreamingDto<SmsDetailedRequest>>(){});
        SmsDetailedRequest smsDetailedRequest = eventStreamingDto.getField();
        LOGGER.info("[processSmsConsumer] Sms received with personlist=" + smsDetailedRequest.getPersonList() + ", templateId=" + smsDetailedRequest.getTemplateId() + ", triggerId=" + smsDetailedRequest.getTriggerId() + ", vendorId=" + smsDetailedRequest.getVendorId());
        String clientId =  message.getMessageProperties().getHeader(Constants.RMQ_HEADER_CLIENT_ID);
        smsDetailedRequest.validate(smsDetailedRequest, clientId);
        smsDetailedRequest.setClientId(Long.parseLong(clientId));
        smsController.sendBulkSms(smsDetailedRequest);
    }

   @RabbitListener(queues = "${q.ins.process_notification}", containerFactory = "rmqPrefetchCount20", concurrency = "2")
    public void processNotificationConsumer(Message message) throws IOException {
        String body = new String(message.getBody());
        LOGGER.info("[processNotificationConsumer] message received with body: " + body);
        EventStreamingDto<PushNotificationRequest> eventStreamingDto = Utils.jsonToObject(body, new TypeReference<EventStreamingDto<PushNotificationRequest>>(){});
        PushNotificationRequest pushNotificationRequest = eventStreamingDto.getField();
        LOGGER.info("[processNotificationConsumer] Notification received with personlist=" + pushNotificationRequest.getPersonList() + ", templateId=" + pushNotificationRequest.getTemplateId() + ", triggerId=" + pushNotificationRequest.getTriggerId() + ", vendorId=" + pushNotificationRequest.getVendorId());
        String clientId = String.valueOf(message.getMessageProperties().getHeaders().getOrDefault(Constants.RMQ_HEADER_CLIENT_ID, null));
        if (!"null".equals(clientId)) {
            pushNotificationRequest.setClientId(Long.parseLong(clientId));
        }
        pushNotificationRequest.validate();
        PushNotificationRequest pushNotificationRequestsend = pushNotificationServiceimpl.constructNotification(pushNotificationRequest);
        pushNotificationServiceimpl.sendNotification(pushNotificationRequestsend);
    }

    @Override
    @RabbitListener(queues = "${q.ins.process_email}", containerFactory = "rmqPrefetchCount20", concurrency = "2")
    public void processEmailConsumer(Message message) throws IOException {
        String messageBody = new String(message.getBody());
        LOGGER.info("[processEmailConsumer] message received with body: " + messageBody);
        EventStreamingDto<EmailRequest> eventStreamingDto = Utils.jsonToObject(messageBody,new TypeReference<EventStreamingDto<EmailRequest>>(){});
        EmailRequest emailRequest = eventStreamingDto.getField();
        LOGGER.info("[processEmailConsumer] Email received with recipientList=" + emailRequest.getRecipientList() + "templateId=" + emailRequest.getTemplateId() + "vendorId=" + emailRequest.getVendorId() + "triggerId=" + emailRequest.getTriggerId());
        emailRequest.validate(emailRequest);
        EmailRequest emailRequestSend = emailService.constructEmail(emailRequest);
        emailService.sendEmail(emailRequestSend);

    }

    @Override
    @RabbitListener(queues = "${q.ins.process_sms_reconciliation}", containerFactory = "rmqPrefetchCount20", concurrency = "2")
    public void processSmsReconciliationConsumer(Message message) throws IOException {
        String messageBody = new String(message.getBody());
        LOGGER.info("[processSmsReconciliationConsumer] message received with body: " + messageBody);

        EventStreamingDto<ReconciliationDto> eventStreamingDto = Utils.jsonToObject(messageBody, new TypeReference<EventStreamingDto<ReconciliationDto>>() {
        });

        ReconciliationDto reconciliationDto = eventStreamingDto.getField();
        reconciliationDto.validate(reconciliationDto);
        reconciliationService.saveStatus(reconciliationDto);
    }

}

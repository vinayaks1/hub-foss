package com.everwell.ins.services.impl;

import com.everwell.ins.exceptions.NotFoundException;
import com.everwell.ins.models.db.Language;
import com.everwell.ins.models.dto.LanguageMapping;
import com.everwell.ins.repositories.LanguageRepository;
import com.everwell.ins.services.LanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class LanguageServiceImpl implements LanguageService {

    @Autowired
    private LanguageRepository languageRepository;

    public Language getLanguage(Long languageId) {
        Language language = languageRepository.findById(languageId).orElse(null);
        if (null == language) {
            throw new NotFoundException("Language with id " + languageId + " not found!");
        }
        return language;
    }

    public List<LanguageMapping> getLanguageMappings(List<Long> languageIds) {
        List<LanguageMapping> languageResponses = new ArrayList<>();
        List<Language> mappingList = languageRepository.findAllByIdIn(languageIds);
        mappingList.forEach(l -> {
            languageResponses.add(new LanguageMapping(l.getId(), l.getLanguage()));
        });
        return languageResponses;
    }

    public List<LanguageMapping> getAllLanguageMappings() {
        List<LanguageMapping> languageResponses = new ArrayList<>();
        List<Language> mappingList = languageRepository.findAll();
        mappingList.forEach(l -> {
            languageResponses.add(new LanguageMapping(l.getId(), l.getLanguage()));
        });
        return languageResponses;
    }
}

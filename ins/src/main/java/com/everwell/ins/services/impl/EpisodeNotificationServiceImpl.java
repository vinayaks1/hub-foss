package com.everwell.ins.services.impl;

import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.models.db.EpisodeNotification;
import com.everwell.ins.models.db.PushNotificationTemplate;
import com.everwell.ins.models.http.requests.EpisodeNotificationSearchRequest;
import com.everwell.ins.models.http.requests.UpdateEpisodeNotificationRequest;
import com.everwell.ins.models.http.responses.EpisodeNotificationResponse;
import com.everwell.ins.repositories.EpisodeNotificationRepository;
import com.everwell.ins.services.EpisodeNotificationService;
import com.everwell.ins.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EpisodeNotificationServiceImpl implements EpisodeNotificationService {

    @Autowired
    private EpisodeNotificationRepository episodeNotificationRepository;

    @Override
    public void saveNotificationBulk(List<EpisodeNotification> episodeNotificationList) {
        episodeNotificationRepository.saveAll(episodeNotificationList);
    }

    @Override
    public List<EpisodeNotificationResponse> getNotificationForEpisodes(EpisodeNotificationSearchRequest episodeNotificationSearchRequest, Long clientId) {
        List<String> episodeIdList = episodeNotificationSearchRequest.getEpisodeIdList().stream().map(String::valueOf).collect(Collectors.toList());
        List<EpisodeNotification> episodeNotifications = episodeNotificationRepository.findAllByEpisodeIdCustom(episodeIdList, clientId);
        List<EpisodeNotificationResponse> episodeNotificationResponses = new ArrayList<>();
        episodeNotifications.forEach(e -> {
            String message = e.getPushNotificationLogs().getMessage();
            Long episodeId = Long.parseLong(e.getPushNotificationLogs().getEntityId());
            PushNotificationTemplate pushNotificationTemplate = e.getPushNotificationLogs().getPushNotificationTemplate();
            String intentExtras = pushNotificationTemplate.getIntentExtras();
            String notificationType = pushNotificationTemplate.getType();
            episodeNotificationResponses.add(new EpisodeNotificationResponse(e.getId(), e.getCreatedOn(), e.getRead(), e.getStatus(), e.getExtras(), episodeId, intentExtras, message, notificationType));
        });
        return episodeNotificationResponses;
    }

    @Override
    public void updateNotification(UpdateEpisodeNotificationRequest episodeNotificationRequest) {
        EpisodeNotification episodeNotification = episodeNotificationRepository.findById(
                episodeNotificationRequest.getNotificationId()
        ).orElse(null);
        if (null == episodeNotification) {
            throw new ValidationException("Notification with id " + episodeNotificationRequest.getNotificationId() + "does not exist");
        }
        if (episodeNotificationRequest.getRead() != null) {
            episodeNotification.setRead(episodeNotificationRequest.getRead());
        }
        if (episodeNotificationRequest.getStatus() != null) {
            episodeNotification.setStatus(episodeNotificationRequest.getStatus());
        }
        episodeNotificationRepository.save(episodeNotification);
    }
}

package com.everwell.ins.services;

import org.springframework.amqp.core.Message;

import java.io.IOException;

public interface EventServiceConsumerService {
    void processSmsConsumer(Message message) throws IOException;

    void processNotificationConsumer(Message message) throws IOException;

    void processEmailConsumer(Message message) throws  IOException;

    void processSmsReconciliationConsumer(Message message) throws IOException;
}

package com.everwell.ins.services.impl;

import com.everwell.ins.models.db.SmsLogs;
import com.everwell.ins.models.dto.EventStreamingDto;
import com.everwell.ins.models.dto.ReconciliationDto;
import com.everwell.ins.models.dto.vendorCreds.ApiKeySenderRouteDto;
import com.everwell.ins.models.http.responses.InterraReconciliationResponse;
import com.everwell.ins.repositories.SmsLogsRepository;
import com.everwell.ins.services.ReconciliationService;
import com.everwell.ins.services.VendorService;
import com.everwell.ins.utils.HttpUtils;
import com.everwell.ins.utils.SentryUtils;
import com.everwell.ins.utils.Utils;
import javassist.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.everwell.ins.constants.RabbitMQConstants.DELAYED_EXCHANGE;
import static com.everwell.ins.constants.RabbitMQConstants.DELAY_HEADER;

@Service
public class ReconciliationServiceImpl implements ReconciliationService {

    @Autowired
    private SmsLogsRepository smsLogsRepository;

    @Autowired
    protected VendorService vendorService;

    @Autowired
    protected RabbitMQPublisherServiceImpl rabbitMQPublisherService;

    @Value("${q.ins.process_sms_reconciliation}")
    private String smsReconciliationQueue;

    @Value("${ins.reconciliation.sms.delay}")
    private Long reconciliationDelay;

    private static final Logger LOGGER = LoggerFactory.getLogger(ReconciliationServiceImpl.class);


    public boolean saveStatus(ReconciliationDto reconciliationDto) {
        boolean statusUpdated =  false;
        try {
            List<SmsLogs> smsLogs = smsLogsRepository.getSmsLogsByMessageIdAndVendorId(reconciliationDto.getMessageId(), reconciliationDto.getVendorIds());
            if (!smsLogs.isEmpty()) {
                smsLogsRepository.updateStatus(reconciliationDto.getMessageId(), reconciliationDto.getVendorIds(), reconciliationDto.getStatus());
                statusUpdated = true;
            } else if (reconciliationDto.getRetry()) {
                LOGGER.info("[reconciliationStatus] Sms logs not found for " + reconciliationDto.toString() + "; Sent to Rabbitmq to retry");
                reconciliationDto.setRetry(false);
                this.sendToQueue(reconciliationDto);
            } else {
                LOGGER.info("[reconciliationStatus] Sms logs not found for " + reconciliationDto.toString() + "; Even after retrying from Rabbitmq");
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            SentryUtils.captureException(e);
        }
        return statusUpdated;
    }

    public void fetchStatusInterra(ReconciliationDto reconciliationDto) {
        LOGGER.info("[fetchStatusInterra] Fetching Reconciliation From Interra");
        Date addedOnAfter = new Date(System.currentTimeMillis() - (3600 * 1000 * 24));
        List<SmsLogs> smsLogs = smsLogsRepository.getSmsLogsByVendorIdAndMessageIdAndStatusAndAddedOnAfter(reconciliationDto.getVendorIds(), addedOnAfter);
        if (!smsLogs.isEmpty()) {
            Map<String, List<SmsLogs>> smsLogsGroup = smsLogs.stream().collect(Collectors.groupingBy(SmsLogs::getMessageId));
            smsLogsGroup.keySet().forEach(messageId -> {
                try {
                    String referenceNumber = "?referenceNumber=" + URLEncoder.encode(messageId, "UTF-8");
                    String skip = "&skip=0";
                    String take = "&take=1";
                    ApiKeySenderRouteDto creds = vendorService.getVendorCredentials(reconciliationDto.getVendorIds().get(0), ApiKeySenderRouteDto.class);
                    Map<String, String> headers = new HashMap<String, String>() {{
                        put("api-key", creds.getApiKey());
                    }};
                    String response = HttpUtils.getRequest(creds.getReconciliationUrl() + referenceNumber + skip + take, headers);
                    InterraReconciliationResponse responseObject = Utils.jsonToObject(response, InterraReconciliationResponse.class);
                    if (responseObject != null && !CollectionUtils.isEmpty(responseObject.getTList())) {
                        String status = responseObject.getTList().get(0).getStatus();
                        smsLogsRepository.updateStatus(messageId, reconciliationDto.getVendorIds(), status);
                        LOGGER.info("[fetchStatusInterra] Status fetched for messageId" + messageId + "&status= " + status);
                    }
                } catch (Exception e) {
                    LOGGER.info("[fetchStatusInterra] Unable to fetch status for messageId" + messageId);
                    SentryUtils.captureException(e);
                }
            });
        }
    }

    public void sendToQueue(ReconciliationDto reconciliationDto) {
        EventStreamingDto<ReconciliationDto> event = new EventStreamingDto<>(smsReconciliationQueue, reconciliationDto);
        Map<String, Object> headers = new HashMap<>();
        headers.put(DELAY_HEADER, reconciliationDelay);
        rabbitMQPublisherService.send(Utils.asJsonString(event), smsReconciliationQueue, DELAYED_EXCHANGE, headers);
    }
}


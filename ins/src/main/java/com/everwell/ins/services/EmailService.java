package com.everwell.ins.services;

import com.everwell.ins.models.http.requests.EmailRequest;

public interface EmailService {
    void sendEmail(EmailRequest emailRequest);

    EmailRequest constructEmail(EmailRequest emailRequest);
}

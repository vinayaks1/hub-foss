package com.everwell.ins.services.impl;

import com.everwell.ins.exceptions.NotFoundException;
import com.everwell.ins.exceptions.ValidationException;
import com.everwell.ins.models.db.Vendor;
import com.everwell.ins.repositories.VendorRepository;
import com.everwell.ins.services.VendorService;
import com.everwell.ins.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class VendorServiceImpl implements VendorService {

    private static Logger LOGGER = LoggerFactory.getLogger(VendorServiceImpl.class);

    @Autowired
    private VendorRepository vendorRepository;

    public List<Long> getVendorIds(String smsGateway) {
        return vendorRepository.getVendorIdByGateway(smsGateway);
    }

    public List<Vendor> getVendors(String PNGateway) {
        return vendorRepository.getVendorsByGateway(PNGateway);
    }

    public Vendor getVendor(Long vendorId) {
        return vendorRepository.findById(vendorId).orElse(null);
    }

    public <T> T getVendorCredentials(Long vendorId, Class<T> valueType) {
        Vendor vendor = vendorRepository.findById(vendorId).orElse(null);
        if (null == vendor){
            throw new NotFoundException("Vendor with id " + vendorId + " not found!");
        }
        try {
            return getVendorProfileWithType(vendor.getCredentials(), valueType);
        } catch (Exception e) {
            LOGGER.error("[getVendorCredentials] unable to set vendorCredsBase");
            throw new ValidationException("Unable to set vendorCredsBase" + e.toString());
        }
    }

    public <T> T getVendorConfig(Long vendorId, Class<T> valueType) {
        Vendor vendor = vendorRepository.findById(vendorId).orElse(null);
        if (null == vendor){
            throw new NotFoundException("Vendor with id " + vendorId + " not found!");
        }
        try {
            return getVendorProfileWithType(vendor.getConfig(), valueType);
        } catch (Exception e) {
            LOGGER.error("[getVendorConfig] unable to set vendorConfigsBase");
            throw new ValidationException("Unable to set vendorConfigsBase" + e.toString());
        }
    }

    public <T> T getVendorProfileWithType(String profile, Class<T> valueType) {
        try {
            return Utils.convertStringToObject(profile, valueType);
        } catch (IOException e) {
            LOGGER.error("[getVendorProfileWithType] unable to set vendorCredsBase");
            throw new ValidationException("Unable to set vendorProfileBase" + e.toString());
        }
    }




}

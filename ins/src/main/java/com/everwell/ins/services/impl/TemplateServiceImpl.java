package com.everwell.ins.services.impl;

import com.everwell.ins.enums.Language;
import com.everwell.ins.exceptions.NotFoundException;
import com.everwell.ins.models.db.EmailTemplate;
import com.everwell.ins.models.db.Template;
import com.everwell.ins.models.db.PushNotificationTemplate;
import com.everwell.ins.models.dto.*;
import com.everwell.ins.models.http.requests.*;
import com.everwell.ins.repositories.EmailTemplateRepository;
import com.everwell.ins.repositories.PushTemplateRepository;
import com.everwell.ins.repositories.TemplateRepository;
import com.everwell.ins.services.TemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.apache.commons.lang3.text.StrSubstitutor;

import java.util.*;
import java.util.stream.Collectors;


@Service
public class TemplateServiceImpl implements TemplateService {

    @Autowired
    private TemplateRepository templateRepository;


    @Autowired
    private PushTemplateRepository pushTemplateRepository;

    @Autowired
    private EmailTemplateRepository emailTemplateRepository;

    public Long saveTemplate(TemplateRequest templateRequest) {
        Template template = new Template(templateRequest.getContent(), templateRequest.getParameters(), templateRequest.getLanguageId(), templateRequest.getTypeId(), templateRequest.getUnicode());
        return templateRepository.save(template).getId();
    }

    public List<Template> getAllTemplates() {
        List<Template> templates = templateRepository.findAll();
        if (CollectionUtils.isEmpty(templates)) {
            throw new NotFoundException("Templates not found!");
        }
        return templates;
    }

    public Template getTemplate(Long templateId) {
        Template template = templateRepository.findById(templateId).orElse(null);
        if (null == template) {
            throw new NotFoundException("Template with id " + templateId + " not found!");
        }
        return template;
    }

    public PushNotificationTemplate getPNTemplate(Long templateId) {
        PushNotificationTemplate PNTemplate = pushTemplateRepository.findById(templateId).orElse(null);
        if (null == PNTemplate) {
            throw new NotFoundException("Template with id " + templateId + " not found!");
        }

        return PNTemplate;
    }

    public EmailTemplate getEmailTemplate(Long templateId) {
        EmailTemplate emailTemplate = emailTemplateRepository.findById(templateId).orElse(null);
        if (null == emailTemplate) {
            throw new NotFoundException("Template with id " + templateId + " not found!");
        }
        return emailTemplate;
    }

    public SmsRequestDto createSms(SmsTemplateRequest smsTemplateRequest) {
        SmsRequest smsRequest = new SmsRequest();
        Template template = getTemplate(smsTemplateRequest.getTemplateId());
        List<SmsTemplateDto> failedSmsRequests = new ArrayList<>();
        if (null == template.getParameters()) {
            smsRequest.setMessage(template.getContent());
            smsRequest.setIsMessageCommon(true);
            List<SmsDto> personList = smsTemplateRequest.getPersonList()
                    .stream()
                    .map(m -> new SmsDto(m.getId(), m.getPhone(), template.getContent()))
                    .collect(Collectors.toList());
            smsRequest.setPersonList(personList);
        } else {
            smsRequest.setMessage(null);
            smsRequest.setIsMessageCommon(false);
            String[] parameterNameList = template.getParameters().split(",");
            List<SmsDto> personList = smsTemplateRequest.getPersonList()
                    .stream()
                    .map(m -> {
                        SmsDto smsDto = null;
                        boolean keysPresent = false;
                        for (String str : parameterNameList) {
                            keysPresent = null != m.getParameters() && m.getParameters().containsKey(str);
                            if (!keysPresent) break;
                        }
                        if (keysPresent) {
                            StrSubstitutor sub = new StrSubstitutor(m.getParameters(), "%(", ")");
                            String content = sub.replace(template.getContent());
                            smsDto = new SmsDto(m.getId(),
                                    m.getPhone(),
                                    content);
                        } else {
                            failedSmsRequests.add(m);
                        }
                        return smsDto;
                    })
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
            smsRequest.setPersonList(personList);
        }
        smsRequest.setTemplateId(smsTemplateRequest.getTemplateId());
        smsRequest.setVendorId(smsTemplateRequest.getVendorId());
        smsRequest.setTriggerId(smsTemplateRequest.getTriggerId());
        smsRequest.setLanguage(template.getUnicode() ? Language.UNICODE : Language.NON_UNICODE);
        smsRequest.setClientId(smsTemplateRequest.getClientId());
        return new SmsRequestDto(smsRequest,failedSmsRequests);
    }

    public SmsDetailedRequestDto constructSms(SmsDetailedRequest smsDetailedRequest) {
        List<Template> templateList = new ArrayList<>();
        Template defaultTemplate = getTemplate(smsDetailedRequest.getTemplateId());
        templateList.add(defaultTemplate);
        if (!CollectionUtils.isEmpty(smsDetailedRequest.getTemplateIds())) {
            smsDetailedRequest.getTemplateIds()
                    .forEach(f -> {
                        templateList.add(getTemplate(f));
                    });
        }
        Map<Long, List<SmsTemplateDto>> smsDtoList = smsDetailedRequest.getPersonList()
                .stream()
                .peek(m -> {
                    if (null == m.getLanguageId()) {
                        m.setLanguageId(defaultTemplate.getLanguageId());
                    }
                })
                .collect(Collectors
                        .groupingBy(
                                SmsTemplateDto::getLanguageId,
                                Collectors.mapping(m -> m, Collectors.toList())
                        )
                );
        List<SmsTemplateDto> failedSmsRequests = new ArrayList<>();
        List<SmsRequest> smsRequests = new ArrayList<>();
        smsDtoList.forEach((k, v) -> {
            Template template = templateList
                    .stream()
                    .filter(m -> m.getLanguageId().equals(k)).findAny().orElse(defaultTemplate);
            SmsRequest smsRequest = new SmsRequest();
            if (null == template.getParameters()) {
                smsRequest.setMessage(template.getContent());
                smsRequest.setIsMessageCommon(true);
                List<SmsDto> personList = v
                        .stream()
                        .map(m -> new SmsDto(m.getId(), m.getPhone(), template.getContent()))
                        .collect(Collectors.toList());
                smsRequest.setPersonList(personList);
            } else {
                smsRequest.setMessage(null);
                smsRequest.setIsMessageCommon(false);
                String[] parameterNameList = template.getParameters().split(",");
                List<SmsDto> personList = v
                        .stream()
                        .map(m -> {
                            SmsDto smsDto = null;
                            boolean keysPresent = false;
                            for (String str : parameterNameList) {
                                keysPresent = null != m.getParameters() && m.getParameters().containsKey(str);
                                if (!keysPresent) break;
                            }
                            if (keysPresent) {
                                StrSubstitutor sub = new StrSubstitutor(m.getParameters(), "%(", ")");
                                String content = sub.replace(template.getContent());
                                smsDto = new SmsDto(m.getId(),
                                        m.getPhone(),
                                        content);
                            } else {
                                failedSmsRequests.add(m);
                            }
                            return smsDto;
                        })
                        .filter(Objects::nonNull)
                        .collect(Collectors.toList());
                smsRequest.setPersonList(personList);
            }
            smsRequest.setTemplateId(template.getId());
            smsRequest.setVendorId(smsDetailedRequest.getVendorId());
            smsRequest.setTriggerId(smsDetailedRequest.getTriggerId());
            smsRequest.setLanguage(template.getUnicode() ? Language.UNICODE : Language.NON_UNICODE);
            smsRequest.setClientId(smsDetailedRequest.getClientId());
            if (!CollectionUtils.isEmpty(smsRequest.getPersonList()))
                smsRequests.add(smsRequest);
        });
        return new SmsDetailedRequestDto(smsRequests, failedSmsRequests);
    }

    public PushNotificationRequestDto constructNotification(PushNotificationRequest pushNotificationRequest) {
        PushNotificationTemplate template = getPNTemplate(pushNotificationRequest.getTemplateId());
        PushNotificationRequest pushNotificationRequestSend = new PushNotificationRequest();
        List<PushNotificationTemplateDto> failedPNRequests = new ArrayList<>();

        if (null == template.getContentParameters() && null == template.getHeadingParameters()) {
            pushNotificationRequestSend.setMessage(template.getContent());
            pushNotificationRequestSend.setHeading(template.getHeading());
            pushNotificationRequestSend.setIsNotificationCommon(true);
            pushNotificationRequestSend.setSendNotificationData(pushNotificationRequest.getSendNotificationData());
            List<PushNotificationTemplateDto> personList = pushNotificationRequest.getPersonList()
                    .stream()
                    .map(m -> new PushNotificationTemplateDto(m.getId(), m.getDeviceId(), template.getContent(),template.getHeading()))
                    .collect(Collectors.toList());
            pushNotificationRequestSend.setPersonList(personList);
        }
        else if (null == template.getContentParameters()){
            pushNotificationRequestSend.setIsNotificationCommon(false);
            pushNotificationRequestSend.setMessage(template.getContent());
            String[] headingParameterKeys =  template.getHeadingParameters().split(",");
            List<PushNotificationTemplateDto> personList = pushNotificationRequest.getPersonList()
                    .stream()
                    .map(m -> {
                        PushNotificationTemplateDto pushNotificationTemplateDto = null;
                        boolean headingkeysPresent = false;
                        for (String str : headingParameterKeys) {
                            headingkeysPresent = null != m.getHeadingParameters() && m.getHeadingParameters().containsKey(str);
                            if (!headingkeysPresent) break;
                        }

                        if (headingkeysPresent) {
                            StrSubstitutor subHeading = new StrSubstitutor(m.getHeadingParameters(), "%(", ")");
                            String heading = subHeading.replace(template.getHeading());
                            pushNotificationTemplateDto = new PushNotificationTemplateDto(m.getId(),
                                    m.getDeviceId(),pushNotificationRequestSend.getMessage(),heading);
                        } else {
                            failedPNRequests.add(m);
                        }
                        return pushNotificationTemplateDto;
                    })
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
            pushNotificationRequestSend.setPersonList(personList);
        }

        else if(null == template.getHeadingParameters())
        {
            pushNotificationRequestSend.setIsNotificationCommon(false);
            pushNotificationRequestSend.setHeading(template.getHeading());
            String[] contentParameterKeys =  template.getContentParameters().split(",");
            List<PushNotificationTemplateDto> personList = pushNotificationRequest.getPersonList()
                    .stream()
                    .map(m -> {
                        PushNotificationTemplateDto pushNotificationTemplateDto = null;
                        boolean contentkeysPresent = false;
                        for (String str : contentParameterKeys) {
                            contentkeysPresent = null != m.getContentParameters() && m.getContentParameters().containsKey(str);
                            if (!contentkeysPresent) break;
                        }

                        if (contentkeysPresent) {
                            StrSubstitutor subContent = new StrSubstitutor(m.getContentParameters(), "%(", ")");
                            String content = subContent.replace(template.getContent());
                            pushNotificationTemplateDto = new PushNotificationTemplateDto(m.getId(),
                                    m.getDeviceId(),
                                    content,pushNotificationRequestSend.getHeading());
                        } else {
                            failedPNRequests.add(m);
                        }
                        return pushNotificationTemplateDto;
                    })
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
            pushNotificationRequestSend.setPersonList(personList);
        }

        else {
            pushNotificationRequestSend.setMessage(null);
            pushNotificationRequestSend.setHeading(null);
            pushNotificationRequestSend.setIsNotificationCommon(false);
            String[] contentParameterKeys =  template.getContentParameters().split(",");
            String[] headingParameterKeys =  template.getHeadingParameters().split(",");
            List<PushNotificationTemplateDto> personList = pushNotificationRequest.getPersonList()
                    .stream()
                    .map(m -> {
                        PushNotificationTemplateDto pushNotificationTemplateDto = null;
                        boolean contentkeysPresent = false;
                        for (String str : contentParameterKeys) {
                            contentkeysPresent = null != m.getContentParameters() && m.getContentParameters().containsKey(str);
                            if (!contentkeysPresent) break;
                        }

                        boolean headingkeysPresent = false;
                        for (String str : headingParameterKeys) {
                            headingkeysPresent = null != m.getHeadingParameters() && m.getHeadingParameters().containsKey(str);
                            if (!headingkeysPresent) break;
                        }

                        if (contentkeysPresent && headingkeysPresent) {
                            StrSubstitutor sub = new StrSubstitutor(m.getContentParameters(), "%(", ")");
                            String content = sub.replace(template.getContent());
                            StrSubstitutor subHeading = new StrSubstitutor(m.getHeadingParameters(), "%(", ")");
                            String heading = subHeading.replace(template.getHeading());
                            pushNotificationTemplateDto = new PushNotificationTemplateDto(m.getId(),
                                    m.getDeviceId(),
                                    content,heading);
                        } else {
                            failedPNRequests.add(m);
                        }
                        return pushNotificationTemplateDto;
                    })
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
            pushNotificationRequestSend.setPersonList(personList);
        }
        pushNotificationRequestSend.setTemplateId(template.getId());
        pushNotificationRequestSend.setTimeToLive(template.getTimeToLive()); //ttl in minutes
        pushNotificationRequestSend.setClassIntent(template.getClassIntent());
        pushNotificationRequestSend.setIntentExtras(template.getIntentExtras());
        pushNotificationRequestSend.setVendorId(pushNotificationRequest.getVendorId());
        pushNotificationRequestSend.setTriggerId(pushNotificationRequest.getTriggerId());
        pushNotificationRequestSend.setClientId(pushNotificationRequest.getClientId());
        pushNotificationRequestSend.setNotificationExtraData(pushNotificationRequest.getNotificationExtraData());
        pushNotificationRequestSend.setEpisodeNotification(template.getEpisodeNotification());


        return new PushNotificationRequestDto(pushNotificationRequestSend, failedPNRequests);

    }

    @Override
    public EmailRequestDto constructEmail(EmailRequest emailRequest) {
        EmailTemplate template = getEmailTemplate(emailRequest.getTemplateId());
        EmailRequest emailRequestSend = new EmailRequest();
        List<EmailTemplateDto> failedEmailRequests = new ArrayList<>();

        if (null == template.getBodyParameters() && null == template.getSubjectParameters()) {
            emailRequestSend.setBody(template.getBody());
            emailRequestSend.setSubject(template.getSubject());
            emailRequestSend.setIsEmailCommon(true);
            List<EmailTemplateDto> emailList = emailRequest.getRecipientList()
                .stream()
                .map(m -> new EmailTemplateDto(m.getRecipient(),template.getBody(),template.getSubject()))
                .collect(Collectors.toList());
            emailRequestSend.setRecipientList(emailList);
        }
        else if (null == template.getBodyParameters()){
            emailRequestSend.setIsEmailCommon(false);
            emailRequestSend.setBody(template.getBody());
            String[] subjectParameterKeys =  template.getSubjectParameters().split(",");
            List<EmailTemplateDto> recipientList = emailRequest.getRecipientList()
                .stream()
                .map(m -> {
                    EmailTemplateDto emailTemplateDto = null;
                    boolean subjectKeysPresent = false;
                    for (String str : subjectParameterKeys) {
                        subjectKeysPresent = null != m.getSubjectParameters() && m.getSubjectParameters().containsKey(str);
                        if (!subjectKeysPresent) break;
                    }

                    if (subjectKeysPresent) {
                        StrSubstitutor subSubject = new StrSubstitutor(m.getSubjectParameters(), "%(", ")");
                        String subject = subSubject.replace(template.getSubject());
                        emailTemplateDto = new EmailTemplateDto(m.getRecipient(),emailRequestSend.getBody(),subject);
                    } else {
                        failedEmailRequests.add(m);
                    }
                    return emailTemplateDto;
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
            emailRequestSend.setRecipientList(recipientList);
        }

        else if(null == template.getSubjectParameters())
        {
            emailRequestSend.setIsEmailCommon(false);
            emailRequestSend.setSubject(template.getSubject());
            String[] bodyParameterKeys =  template.getBodyParameters().split(",");
            List<EmailTemplateDto> recipientList = emailRequest.getRecipientList()
                .stream()
                .map(m -> {
                    EmailTemplateDto emailTemplateDto = null;
                    boolean bodyKeysPresent = false;
                    for (String str : bodyParameterKeys) {
                        bodyKeysPresent = null != m.getBodyParameters() && m.getBodyParameters().containsKey(str);
                        if (!bodyKeysPresent) break;
                    }

                    if (bodyKeysPresent) {
                        StrSubstitutor subBody = new StrSubstitutor(m.getBodyParameters(), "%(", ")");
                        String body = subBody.replace(template.getBody());
                        emailTemplateDto = new EmailTemplateDto(m.getRecipient(),
                            body,emailRequestSend.getSubject());
                    } else {
                        failedEmailRequests.add(m);
                    }
                    return emailTemplateDto;
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
            emailRequestSend.setRecipientList(recipientList);
        }

        else {
            emailRequestSend.setSubject(null);
            emailRequestSend.setBody(null);
            emailRequestSend.setIsEmailCommon(false);
            String[] bodyParameterKeys =  template.getBodyParameters().split(",");
            String[] subjectParameterKeys =  template.getSubjectParameters().split(",");
            List<EmailTemplateDto> recipientList = emailRequest.getRecipientList()
                .stream()
                .map(m -> {
                    EmailTemplateDto emailTemplateDto = null;
                    boolean bodyKeysPresent = false;
                    for (String str : bodyParameterKeys) {
                        bodyKeysPresent = null != m.getBodyParameters() && m.getBodyParameters().containsKey(str);
                        if (!bodyKeysPresent) break;
                    }

                    boolean subjectKeysPresent = false;
                    for (String str : subjectParameterKeys) {
                        subjectKeysPresent = null != m.getSubjectParameters() && m.getSubjectParameters().containsKey(str);
                        if (!subjectKeysPresent) break;
                    }

                    if (bodyKeysPresent && subjectKeysPresent) {
                        StrSubstitutor sub = new StrSubstitutor(m.getBodyParameters(), "%(", ")");
                        String body = sub.replace(template.getBody());
                        StrSubstitutor subSubject = new StrSubstitutor(m.getSubjectParameters(), "%(", ")");
                        String subject = subSubject.replace(template.getSubject());
                        emailTemplateDto = new EmailTemplateDto(m.getRecipient(),
                            body,subject);
                    } else {
                        failedEmailRequests.add(m);
                    }
                    return emailTemplateDto;
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
            emailRequestSend.setRecipientList(recipientList);
        }

        emailRequestSend.setTemplateId(template.getId());
        emailRequestSend.setVendorId(emailRequest.getVendorId());
        emailRequestSend.setTriggerId(emailRequest.getTriggerId());
        emailRequestSend.setSender(emailRequest.getSender());
        emailRequestSend.setAttachmentDto(emailRequest.getAttachmentDto());

        return  new EmailRequestDto(emailRequestSend,failedEmailRequests);

    }
}

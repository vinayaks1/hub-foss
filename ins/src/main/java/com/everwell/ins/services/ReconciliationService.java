package com.everwell.ins.services;

import com.everwell.ins.models.dto.ReconciliationDto;

public interface ReconciliationService {

    boolean saveStatus(ReconciliationDto reconciliationDto);

    void fetchStatusInterra(ReconciliationDto reconciliationDto);

}

package com.everwell.ins.services.impl;

import com.everwell.ins.enums.SmsGateway;
import com.everwell.ins.exceptions.NotFoundException;
import com.everwell.ins.factory.VendorFactory;
import com.everwell.ins.models.db.Vendor;
import com.everwell.ins.models.http.requests.SmsRequest;
import com.everwell.ins.repositories.VendorRepository;
import com.everwell.ins.services.SmsService;
import com.everwell.ins.vendors.SmsHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
public class SmsServiceImpl implements SmsService {

    @Autowired
    private VendorRepository vendorRepository;

    @Autowired
    private VendorFactory vendorFactory;

    @Async
    @Override
    public void sendSms(SmsRequest smsRequest) {
        if (!smsRequest.getPersonList().isEmpty()) {
            Vendor vendor = vendorRepository.findById(smsRequest.getVendorId()).orElse(null);
            if (null == vendor){
                throw new NotFoundException("Vendor with id " + smsRequest.getVendorId() + " not found!");
            }
            SmsHandler smsHandler = vendorFactory.getHandler(SmsGateway.valueOf(vendor.getGateway()));
            smsHandler.sendSms(smsRequest);
        }
    }

    public void sendBulkSms(List<SmsRequest> smsRequests) {
        smsRequests.forEach(s -> {
            if (!CollectionUtils.isEmpty(s.getPersonList())) {
                sendSms(s);
            }
        });
    }
}

package com.everwell.ins.services;

import com.everwell.ins.models.http.requests.SmsRequest;

import java.util.List;

public interface SmsService {

    void sendSms(SmsRequest smsRequest);

    void sendBulkSms(List<SmsRequest> smsRequests);

}

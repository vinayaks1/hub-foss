package com.everwell.ins.services;

import com.everwell.ins.models.db.EpisodeNotification;
import com.everwell.ins.models.http.requests.EpisodeNotificationSearchRequest;
import com.everwell.ins.models.http.requests.UpdateEpisodeNotificationRequest;
import com.everwell.ins.models.http.responses.EpisodeNotificationResponse;

import java.util.List;

public interface EpisodeNotificationService {

    void saveNotificationBulk(List<EpisodeNotification> episodeNotificationList);

    List<EpisodeNotificationResponse> getNotificationForEpisodes(EpisodeNotificationSearchRequest episodeNotificationSearchRequest, Long clientId);

    void updateNotification(UpdateEpisodeNotificationRequest episodeNotificationRequest);
}

package com.everwell.ins.services;

import com.everwell.ins.models.db.Client;
import com.everwell.ins.models.http.requests.RegisterClientRequest;
import com.everwell.ins.models.http.responses.ClientResponse;

public interface ClientService {

    ClientResponse registerClient(RegisterClientRequest clientRequest);

    ClientResponse getClientWithToken(Long id);

    Client getClient(Long id);

    Client getCurrentClient();

}

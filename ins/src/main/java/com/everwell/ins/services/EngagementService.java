package com.everwell.ins.services;

import com.everwell.ins.models.db.Engagement;
import com.everwell.ins.models.dto.SmsRequestDto;
import com.everwell.ins.models.http.requests.EngagementBulkRequest;
import com.everwell.ins.models.http.requests.EngagementRequest;
import com.everwell.ins.models.http.requests.PushNotificationRequest;
import com.everwell.ins.models.http.requests.SmsDetailedRequest;

import java.util.List;

public interface EngagementService {

    Long saveEngagementConfig(EngagementRequest engagementRequest);

    Engagement getEngagementConfig(String entityId, Long typeId);

    List<Engagement> getEngagementBulk(String entityId);

    Engagement updateEngagementConfig(EngagementRequest engagementRequest);

    SmsRequestDto modifyList(SmsRequestDto smsRequestDto, Long engageTypeId);

    SmsDetailedRequest filterSmsList(SmsDetailedRequest smsDetailedRequest, Long engageTypeId);

    void saveEngagementConfigBulk(EngagementBulkRequest engagementBulkRequest);

    PushNotificationRequest filterPushNotificationList(PushNotificationRequest pushNotificationRequest);

}

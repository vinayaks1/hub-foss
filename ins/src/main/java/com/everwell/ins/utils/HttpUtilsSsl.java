package com.everwell.ins.utils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.net.URL;

@Component
public class HttpUtilsSsl {

    private static String sslStoreUrl;
    private static String password;
    private static String protocol;

    @Value("${nic.ssl.path}")
    public void setUrl(String url) {
        this.sslStoreUrl = url;
    }

    @Value("${nic.ssl.password}")
    public void setPassword(String password) {
        this.password = password;
    }

    @Value("${nic.ssl.protocol}")
    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    private static Logger LOGGER = LoggerFactory.getLogger(HttpUtils.class);

    public static String getRequestForNIC(String url) throws IOException {
        String respString = null;
        SSLContext sslContext;
        try {
            URL storeUrl = new URL(sslStoreUrl);
            char[] storePassword = password.toCharArray();
            sslContext = SSLContextBuilder.create()
                    .loadTrustMaterial(storeUrl, storePassword)
                    .setProtocol(protocol)
                    .build();
        } catch (Exception e) {
            throw new IllegalStateException("Failed to setup client SSL context", e);
        }

        HttpClient httpClient = HttpClientBuilder.create()
                .setSSLContext(sslContext)
                .build();

        HttpGet getRequest = new HttpGet(url);
        HttpResponse response = httpClient.execute(getRequest);
        HttpEntity httpEntity = response.getEntity();

        respString = EntityUtils.toString(httpEntity);
        return respString;
    }

}

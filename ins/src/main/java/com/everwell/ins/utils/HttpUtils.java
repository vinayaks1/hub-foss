package com.everwell.ins.utils;

import org.asynchttpclient.*;
import org.asynchttpclient.util.HttpConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class HttpUtils {

    public static AsyncHttpClient client = Dsl.asyncHttpClient();

    private static Logger LOGGER = LoggerFactory.getLogger(HttpUtils.class);

    public static String getRequest(String url, Map<String, String> headers) throws IOException, InterruptedException {
        String respString = null;
        RequestBuilder request = new RequestBuilder(HttpConstants.Methods.GET)
                .setUrl(url);
        if (headers != null && !headers.isEmpty()) {
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                request.addHeader(entry.getKey(), entry.getValue());
            }
        }
        Future<Response> future = client.executeRequest(request.build());
        try {
            Response response = future.get();
            respString = response.getResponseBody();
        } catch (ExecutionException e) {
            LOGGER.warn("[getRequest] exception occurred for url ", e);
        }
        return respString;
    }

    public static String postFormRequest(String url, Map<String, List<String>> requestForm, Map<String, String> headers) throws InterruptedException {
        String respString = null;

        RequestBuilder request = new RequestBuilder(HttpConstants.Methods.POST)
                .setFormParams(requestForm)
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .setUrl(url);
        if (headers != null && !headers.isEmpty()) {
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                request.addHeader(entry.getKey(), entry.getValue());
            }
        }
        Future<Response> future = client.executeRequest(request.build());
        try {
            Response response = future.get();
            respString = response.getResponseBody();
        } catch (ExecutionException e) {
            LOGGER.warn("[postFormRequest] exception occurred for url ", e);
        }
        return respString;
    }

    public static String postFormRequest(String url, Map<String, List<String>> requestForm) throws InterruptedException {
        return postFormRequest(url, requestForm, null);
    }

    public static String postJsonRequest(String url, String requestBody, Map<String, String> headers) throws InterruptedException {
        String respString = null;
        RequestBuilder request = new RequestBuilder(HttpConstants.Methods.POST)
                .setBody(requestBody)
                .setHeader("Content-Type", "application/json")
                .setUrl(url);
        if (headers != null) {
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                request.setHeader(entry.getKey(), entry.getValue());
            }
        }
        Future<Response> future = client.executeRequest(request.build());
        try {
            Response response = future.get();
            respString = response.getResponseBody();
        } catch (ExecutionException e) {
            LOGGER.warn("[postRequest] exception occurred for url ", e);
        }
        return respString;
    }
}

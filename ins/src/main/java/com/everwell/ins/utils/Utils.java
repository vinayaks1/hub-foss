package com.everwell.ins.utils;

import com.everwell.ins.models.http.responses.Response;
import com.everwell.ins.models.db.Client;
import com.everwell.ins.models.JwtPayload;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

public class Utils {

    private static ObjectMapper objectMapper = new ObjectMapper();

    public static Date getCurrentDate() {
        return formatDate(new Date());
    }

    public static Date formatDate(Date date) {
        return formatDate(date, "dd-MM-yyyy HH:mm:ss");
    }

    public static Date formatDate(Date date, String format) {
        try {
            date = convertStringToDate(getFormattedDate(date, format), format);
        } catch (ParseException e) {
        }
        return date;
    }

    public static Date convertStringToDate(String date, String format) throws ParseException {
        return new SimpleDateFormat(format).parse(date);
    }

    public static Date convertStringToDate(String date) throws ParseException {
        return convertStringToDate(date, "yyyy-MM-dd HH:mm:ss");
    }

    public static String getFormattedDate(Date date, String format) {
        DateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(date);
    }
    public static String Base64toString(String toDecoe) {
        return new String(Base64.getDecoder().decode(toDecoe));
    }

    public static JwtPayload getJwtPayload(String jwtToken) throws IOException {
        String toDecode = new String(Base64.getDecoder().decode(jwtToken.split("\\.")[1]));
        return convertStringToObject(toDecode, JwtPayload.class);
    }

    public static <T> T convertStringToObject(String json, Class<T> valueType) throws IOException {
        return objectMapper.readValue(json, valueType);
    }

    public static void writeCustomResponseToOutputStream(HttpServletResponse response, Exception e, HttpStatus httpStatus) throws IOException {
        writeCustomResponseToOutputStream(response, new Response(e.getMessage()), httpStatus);
    }

    public static String convertToUnicode(String message) {
        byte[] bytes = message.getBytes(StandardCharsets.UTF_8);
        return new String(bytes, StandardCharsets.UTF_8);
    }


    public static void writeCustomResponseToOutputStream(HttpServletResponse response, Response CustomResponse, HttpStatus httpStatus) throws IOException {
        response.setContentType("application/json");
        response.setStatus(httpStatus.value());
        response.getOutputStream()
                .println(Utils.asJsonString(CustomResponse));
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static String trimMessage(String message) {
        return StringUtils.isEmpty(message) ? message : StringUtils.trimLeadingWhitespace(message);
    }

    public static <T> T jsonToObject (String json, Class<T> klass) throws IOException {
        return objectMapper.readValue(json, klass);
    }

    //for generic classes
    public static <T> T jsonToObject (String json, TypeReference<T> typeReference) throws IOException {
        return objectMapper.readValue(json, typeReference);
    }

    public static String removePrefix(String s, String prefix)
    {
        if (s != null && prefix != null && s.startsWith(prefix)) {
            return s.substring(prefix.length());
        }
        return s;
    }



}

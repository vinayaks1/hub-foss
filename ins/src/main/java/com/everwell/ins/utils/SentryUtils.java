package com.everwell.ins.utils;

import io.sentry.Sentry;
import io.sentry.event.EventBuilder;
import io.sentry.event.interfaces.ExceptionInterface;
import io.sentry.event.interfaces.HttpInterface;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import java.util.Map;

public class SentryUtils {

    public static void captureException(Exception e, WebRequest request) {
        EventBuilder builder = new EventBuilder().withSentryInterface(new HttpInterface(((ServletWebRequest) request).getRequest()))
                .withSentryInterface(new ExceptionInterface(e));
        Sentry.capture(builder);
    }

    public static void captureException(Exception e, Map<String, String> data) {
        EventBuilder builder = new EventBuilder().withSentryInterface(new ExceptionInterface(e));
        if (data != null && !data.isEmpty()) {
            for (Map.Entry<String, String> entry : data.entrySet()) {
                builder.withExtra(entry.getKey(), entry.getValue());
            }
        }
        Sentry.capture(builder);
    }

    public static void captureException(Exception e) {
        Sentry.capture(e.toString());
    }

}

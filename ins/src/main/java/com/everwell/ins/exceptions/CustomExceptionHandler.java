package com.everwell.ins.exceptions;

import com.everwell.ins.models.http.responses.Response;
import com.everwell.ins.utils.SentryUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(CustomExceptionHandler.class);

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Response> handleAllExceptions(Exception ex, WebRequest request, InputStream inputStream) throws IOException {
        SentryUtils.captureException(ex, request);
        LOGGER.error("[handleAllExceptions] exception occurred for request body " + ex);
        return handleException("Internal Server Error occurred", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(ServiceException.class)
    public final ResponseEntity<Response> handleServiceExceptions(ServiceException ex, WebRequest request, InputStream inputStream) throws IOException {
        SentryUtils.captureException(ex, request);
        LOGGER.error("[handleServiceExceptions]", ex);
        return handleException(ex.getMessage(), HttpStatus.I_AM_A_TEAPOT);
    }

    @ExceptionHandler(NotFoundException.class)
    public final ResponseEntity<Response> handleNotFoundExceptions(NotFoundException ex, WebRequest request) {
        SentryUtils.captureException(ex);
        LOGGER.error("[handleNotFoundExceptions]", ex);
        return handleException(ex.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(UnauthorizedException.class)
    public final ResponseEntity<Response> handleUnauthorizedExceptions(UnauthorizedException ex, WebRequest request) {
        SentryUtils.captureException(ex);
        LOGGER.error("[handleUnauthorizedExceptions]", ex);
        return handleException(ex.getMessage(), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(VendorException.class)
    public final ResponseEntity<Response> handleVendorExceptions(VendorException ex, WebRequest request) {
        SentryUtils.captureException(ex, ex.getData());
        LOGGER.error("[handleVendorExceptions]", ex);
        return handleException(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(BadCredentialsException.class)
    public final ResponseEntity<Response> handleBadCredentialsExceptions(BadCredentialsException ex, WebRequest request, InputStream inputStream) throws IOException {
        SentryUtils.captureException(ex);
        LOGGER.error("[handleBadCredentialsExceptions]", ex);
        return handleException(ex.getMessage(), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(ConflictException.class)
    public final ResponseEntity<Response> handleConflictExceptions(ConflictException ex, WebRequest request) {
        SentryUtils.captureException(ex);
        LOGGER.error("[handleConflictExceptions]", ex);
        return handleException(ex.getMessage(), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(ValidationException.class)
    public final ResponseEntity<Response> handleValidationExceptions(ValidationException ex, WebRequest request) {
        SentryUtils.captureException(ex);
        LOGGER.error("[handleValidationExceptions]", ex);
        return handleException(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public final ResponseEntity<Response> handleIllegalArgumentExceptions(IllegalArgumentException ex, WebRequest request) {
        SentryUtils.captureException(ex);
        LOGGER.error("[handleIllegalArgumentExceptions]", ex);
        return handleException(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InternalServerErrorException.class)
    public final ResponseEntity<Response> handleInternalServerErrorException(InternalServerErrorException ex, WebRequest request) {
        SentryUtils.captureException(ex);
        LOGGER.error("[handleInternalServerErrorException]", ex);
        return handleException(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(InvalidParameterException.class)
    public final ResponseEntity<Response> handleInvalidParameterException(InvalidParameterException ex, WebRequest request) {
        SentryUtils.captureException(ex);
        LOGGER.error("[handleInvalidParameterException]", ex);
        return handleException(ex.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @Override
    public ResponseEntity<Object> handleMissingServletRequestParameter(
            MissingServletRequestParameterException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        String error = ex.getParameterName() + " parameter is missing";
        return handleExceptionInternal(ex, ex.getLocalizedMessage(), headers, Arrays.asList(error), request);
    }

    @Override
    public ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        List<String> errors = new ArrayList<String>();
        for (FieldError error : ex.getBindingResult().getFieldErrors()) {
            errors.add(error.getField() + ": " + error.getDefaultMessage());
        }
        for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
            errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
        }

        return handleExceptionInternal(
                ex, ex.getLocalizedMessage(), headers, errors, request);
    }

    private ResponseEntity<Response> handleException(String message, HttpStatus httpStatus) {
        Response errorResponse = new Response(message);
        return new ResponseEntity<>(errorResponse, httpStatus);
    }

    private ResponseEntity<Object> handleExceptionInternal(Exception ex, String message, HttpHeaders headers, List<String> errors, WebRequest request) {
        Response errorResponse = new Response(false, errors, message);
        return handleExceptionInternal(ex, errorResponse, headers, HttpStatus.BAD_REQUEST, request);
    }


}

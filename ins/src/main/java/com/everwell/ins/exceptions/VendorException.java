package com.everwell.ins.exceptions;

import lombok.Getter;

import java.util.Map;

public class VendorException extends RuntimeException {

    @Getter
    public Map<String, String> data;

    public VendorException(String exception, Map<String, String> extraData) {
        super(exception);
        data = extraData;
    }
}

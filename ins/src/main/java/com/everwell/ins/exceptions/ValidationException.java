package com.everwell.ins.exceptions;

public class ValidationException extends RuntimeException {

    public ValidationException(String exception) {
        super(exception);
    }
}

package com.everwell.ins.exceptions;

public class ServiceException extends RuntimeException {

    public ServiceException(String exception) {
        super(exception);
    }
}

package com.everwell.ins.repositories;

import com.everwell.ins.models.db.TemplateMap;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TemplateMapRepository extends JpaRepository<TemplateMap, Long> {

    TemplateMap findByTemplateId(Long templateId);

}

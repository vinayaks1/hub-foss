package com.everwell.ins.repositories;

import com.everwell.ins.models.db.PushNotificationLogs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

public interface PushLogsRepository extends JpaRepository<PushNotificationLogs, Long>{
}

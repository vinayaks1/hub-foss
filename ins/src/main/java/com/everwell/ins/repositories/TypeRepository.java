package com.everwell.ins.repositories;

import com.everwell.ins.models.db.Type;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TypeRepository extends JpaRepository<Type, Long> {

    List<Type> findAllByIdIn(List<Long> typeIds);
}

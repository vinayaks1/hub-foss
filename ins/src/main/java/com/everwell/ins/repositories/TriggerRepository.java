package com.everwell.ins.repositories;

import com.everwell.ins.models.db.Trigger;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TriggerRepository extends JpaRepository<Trigger, Long> {

}

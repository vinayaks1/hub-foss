package com.everwell.ins.repositories;

import com.everwell.ins.models.db.Vendor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface VendorRepository extends JpaRepository<Vendor, Long> {

    @Query(value = "SELECT v.id FROM ins_vendor v where v.gateway = :gateway", nativeQuery = true)
    List<Long> getVendorIdByGateway(String gateway);

    @Query(value = "SELECT * FROM ins_vendor v where v.gateway = :gateway", nativeQuery = true)
    List<Vendor> getVendorsByGateway(String gateway);

}

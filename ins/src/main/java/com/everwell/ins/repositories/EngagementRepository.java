package com.everwell.ins.repositories;

import com.everwell.ins.models.db.Engagement;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EngagementRepository extends JpaRepository<Engagement, Long> {


    Engagement findByEntityIdAndTypeId(String entityId, Long typeId);

    List<Engagement> findAllByEntityId(String entityId);

    List<Engagement> findByEntityIdInAndTypeId(List<String> entityIds, Long typeId);
}

package com.everwell.ins.repositories;

import com.everwell.ins.models.db.EpisodeNotification;
import com.everwell.ins.models.http.responses.EpisodeNotificationResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EpisodeNotificationRepository extends JpaRepository<EpisodeNotification, Long> {

    @Query(value = "select * from episode_notification en inner join ins_pn_logs ipl on en.pn_log_id = ipl.id inner join ins_pn_template ipt on ipl.template_id = ipt.id where ipl.entity_id in :episodeId and ipt.episode_notification is true and en.client_id = :clientId", nativeQuery = true)
    List<EpisodeNotification> findAllByEpisodeIdCustom(List<String> episodeId, Long clientId);
}

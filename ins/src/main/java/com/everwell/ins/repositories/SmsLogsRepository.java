package com.everwell.ins.repositories;

import com.everwell.ins.models.db.SmsLogs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

public interface SmsLogsRepository extends JpaRepository<SmsLogs, Long> {

    @Query(value = "select s.* from ins_sms_logs s where s.message_id = :messageId and s.vendor_id IN :vendorIds", nativeQuery = true)
    List<SmsLogs> getSmsLogsByMessageIdAndVendorId(String messageId, List<Long> vendorIds);

    @Query(value = "select count(*) from ins_sms_logs s where s.vendor_id = :vendorId and s.added_on > :from and s.added_on < :to " +
            "and s.trigger_id = :triggerId and s.template_id = :templateId", nativeQuery = true)
    Long getSmsLogsForNotificationStatsWithTemplateAndTriggerId(Long triggerId, Long templateId, Long vendorId, Date from, Date to);

    @Query(value = "select count(*) from ins_sms_logs s where s.vendor_id = :vendorId and s.added_on > :from and s.added_on < :to " +
            "and s.trigger_id = :triggerId", nativeQuery = true)
    Long getSmsLogsForNotificationStatsWithTriggerId(Long triggerId, Long vendorId, Date from, Date to);

    @Query(value = "select count(*) from ins_sms_logs s where s.vendor_id = :vendorId and s.added_on > :from and s.added_on < :to " +
            "and s.template_id = :templateId", nativeQuery = true)
    Long getSmsLogsForNotificationStatsWithTemplateId(Long templateId, Long vendorId, Date from, Date to);

    @Query(value = "select count(*) from ins_sms_logs s where s.vendor_id = :vendorId and s.added_on > :from and s.added_on < :to", nativeQuery = true)
    Long getSmsLogsForNotificationStats(Long vendorId, Date from, Date to);


    @Query(value = "select s.* from ins_sms_logs s where s.vendor_id IN :vendorIds and s.added_on > :addedOnAfter and s.message_id is not null and s.status is null", nativeQuery = true)
    List<SmsLogs> getSmsLogsByVendorIdAndMessageIdAndStatusAndAddedOnAfter(List<Long> vendorIds, Date addedOnAfter);

    @Modifying
    @Transactional
    @Query(value = "UPDATE ins_sms_logs SET status = :status where message_id = :messageId AND vendor_id IN :vendorIds", nativeQuery = true)
    void updateStatus(String messageId, List<Long> vendorIds, String status);
}

alter table ins_pn_template add column if not exists class_intent text;

alter table ins_pn_template add column if not exists intent_extras text;

UPDATE ins_pn_template
SET class_intent = 'org.a99dots.mobile99dots.ui.patientlist.HierarchyListActivity' , intent_extras = 'PatientFilters.USE_INITIATION:boolean#true,PatientFilters.STAGE:stage#PRESUMPTIVE_OPEN,HierarchyListActivity.PARENT_ID:hierarchy#3256,PatientFilters.TYPES:types#[]'
where id = 1;

UPDATE ins_pn_template
SET class_intent = 'org.a99dots.mobile99dots.ui.patientlist.HierarchyListActivity' , intent_extras = 'PatientFilters.USE_INITIATION:boolean#true,PatientFilters.STAGE:stage#DIAGNOSED_NOT_ON_TREATMENT,HierarchyListActivity.PARENT_ID:hierarchy#3256,PatientFilters.TYPES:types#[]'
where id = 2;
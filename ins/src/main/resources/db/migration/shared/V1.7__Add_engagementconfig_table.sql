/* Add foreign keys */
ALTER TABLE ins_engagement
ADD CONSTRAINT fk_type
FOREIGN KEY (type_id)
REFERENCES ins_type (id);

ALTER TABLE ins_engagement
ADD CONSTRAINT fk_lang
FOREIGN KEY (language_id)
REFERENCES ins_language (id);

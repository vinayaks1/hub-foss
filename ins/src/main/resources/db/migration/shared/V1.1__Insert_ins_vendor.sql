INSERT INTO ins_vendor
(config,
credential_type,
credentials, gateway, id)
VALUES ('{"bulkSmsLimit":"100","prefix":"Nikshay: ","language":"PM","unicodeLanguage":"UC","configType":"NIC"}','USERNAMEPIN','{"userName":"nikshay.auth","pin":"D*23bc$a","signature":"CTDSMS","url":"https://smsgw.sms.gov.in/failsafe/HttpLink","credentialType":"USERNAMEPIN"}','NIC',1);

INSERT INTO ins_vendor
(config,
credential_type,
credentials, gateway, id)
VALUES ('{"bulkSmsLimit":"100","prefix":"Nikshay: ","language":"PM","unicodeLanguage":"UC","configType":"NIC"}','USERNAMEPIN','{"userName":"nikshay.otp","pin":"K!hdTpi%252h","signature":"CTDSMS","url": "https://smsgw.sms.gov.in/failsafe/HttpLink","credentialType":"USERNAMEPIN"}','NIC',2);

INSERT INTO ins_vendor
(config,
credential_type,
credentials, gateway, id)
VALUES ('{"bulkSmsLimit":"500","configType":"TEXTLOCAL"}','APIKEYSENDER','{"apiKey":"zhpKgaxdYsM-uM81lbDMe4RsHphnFRGfLMsZ6TVmUw","url":"http://api.textlocal.in/send/","sender":"EVRWEL","credentialType":"APIKEYSENDER"}','TEXTLOCAL',3);

INSERT INTO ins_vendor
(config,
credential_type,
credentials, gateway, id)
VALUES ('{"bulkSmsLimit":"10","configType":"ICCDDRB"}','USERKEYSERVERURL','{"serverUrl":"http://sms.icddrb.org/sms_send_gateway/sendSmsTBicddrb","url":"http://dmtta.icddrb.org/sms/api.php","userKey":"69636464726224544225","credentialType":"USERKEYSERVERURL"}','ICCDDRB',4);

INSERT INTO ins_vendor
(config,
credential_type,
credentials, gateway, id)
VALUES ('{"language":"false","unicodeLanguage":"true","reconciliation":"/api/Reconciliation/ReconciliationTwilio","configType":"TWILIO"}','AUTHTOKENFROM','{"accountId":"AC021b8f80a4dcbcb52cfa567ea3f88d05","authToken":"c089122dbcbba6a37240ee8f5ccb056c","from":"SmartHelp","credentialType":"AUTHTOKENFROM"}','TWILIO',5);

INSERT INTO ins_vendor
(config,
credential_type,
credentials, gateway, id)
VALUES ('{"bulkSmsLimit":"10","reconciliation":"/api/Reconciliation/ReconciliationPlivo","configType":"PLIVO"}','AUTHIDAUTHTOKEN','{"srcNumber":"959778043827","authId":"MAYMNLMJNMZTC4NGE3NW","authToken":"MWMwN2E3Mjc1NWMzZDJlZGVhOTA3N2RiMWEzN2Y1","credentialType":"AUTHIDAUTHTOKEN"}','PLIVO',6);

INSERT INTO ins_vendor
(config,
credential_type,
credentials, gateway, id)
VALUES ('{"language":"false","unicodeLanguage":"true","configType":"AFRICASTALKING"}','APIKEYFROM','{"apiKey":"4606de5960debd66f3cd69934309d6a70f4c614d896a9ea1068983a949825076","userName":"tza99dots","from":"WIZARA-AFYA","credentialType":"APIKEYFROM"}','AFRICASTALKING',7);

INSERT INTO ins_vendor
(config,
credential_type,
credentials, gateway, id)
VALUES ('{"language":"false","unicodeLanguage":"true","configType":"AFRICASTALKING"}','APIKEYFROM','{"apiKey":"8bfc202f9398c000cb495c1f704b7ab58c6ef07d4e1344fa427fe737a6294b4a","userName":"uganda99dots","from":"99DOTS","credentialType":"APIKEYFROM"}','AFRICASTALKING',8);


INSERT INTO ins_vendor
(config,
credential_type,
credentials, gateway, id)
VALUES ('{"bulkSmsLimit":"10","configType":"GLOBELABS"}','PASSPHRASEAPPID','{"passPhrase":"rWHAhI92Cx","url":"https://devapi.globelabs.com.ph/smsmessaging/v1/outbound/0270/requests/","appId":"BroaUbdyoGhRdiEd8rcyMXhR9rk6U4Rk","appSecret":"7d23f1a4d5c372be460d6dfd5cd85d913e788b1ba587afd9fc7eb3bd88fc708a","credentialType":"PASSPHRASEAPPID"}','GLOBELABS',9);
/* Add foreign keys */
ALTER TABLE ins_template
ADD CONSTRAINT fk_type
FOREIGN KEY (type_id)
REFERENCES ins_type (id);

ALTER TABLE ins_template
ADD CONSTRAINT fk_lang
FOREIGN KEY (language_id)
REFERENCES ins_language (id);

/* Insert values */
INSERT INTO ins_type (id,type) select 1,'SMS' where not exists (select 1 from ins_type where id = 1);
INSERT INTO ins_language (id,language) select 1,'ENGLISH' where not exists (select 1 from ins_language where id = 1);

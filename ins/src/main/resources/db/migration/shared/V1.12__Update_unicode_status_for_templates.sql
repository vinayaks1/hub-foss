UPDATE ins_template SET unicode = false where unicode is NULL ;

ALTER TABLE ins_template ALTER COLUMN unicode SET DEFAULT false;

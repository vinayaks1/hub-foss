ALTER TABLE ins_vendor
ADD column if not exists reconciliation_type text;

UPDATE ins_vendor
Set reconciliation_type = 'PULL'
Where gateway = 'INTERRA';

UPDATE ins_vendor
Set reconciliation_type = 'PUSH'
Where gateway in ('NIC','AFRICASTALKING','TWILIO','PLIVO','TEXTLOCAL');
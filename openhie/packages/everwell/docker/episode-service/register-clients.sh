register_datagateway_client() {
  curl --location --silent --fail --request POST 'http://data-gateway:8080/token' \
    --header 'Content-Type: application/json' \
    --data-raw "{
        \"username\":\"${DATAGATEWAY_USERNAME}\",
        \"password\":\"${DATAGATEWAY_PASSWORD}\"
    }" > /dev/null
  if [ $? -ne 0 ]; then
      echo "GET Data Gateway client failed. registering new client..."
      curl --location --silent --fail --request POST --request POST \
        --url 'http://data-gateway:8080/register' \
        --header 'Cache-Control: no-cache' \
        --header 'Content-Type: application/json' \
        --data "{
          \"username\":\"${DATAGATEWAY_USERNAME}\",
          \"password\":\"${DATAGATEWAY_PASSWORD}\",
          \"failureEmail\":\"developers@everwell.org\",
          \"accessibleClient\":\"${EPISODE_CLIENT_ID}\"
      }" > /dev/null
      if [ $? -ne 0 ]; then
          echo "new Data Gateway client registration failed"
          exit 1
      fi
      echo "new Data Gateway client registered"
  else
      echo "Data Gateway client already exist. skipping client registration."
  fi
}

register_iam_client() {
  curl --location --silent --fail --request GET 'http://iam-backend:9090/v1/client' \
    --header "Content-Type: application/json" \
    --header "X-IAM-Client-Id: ${IAM_CLIENT_ID}" > /dev/null
  if [ $? -ne 0 ]; then
      echo "GET IAM client failed. registering new client..."
      response=$(curl --location --silent --fail --request POST --request POST \
        --url 'http://iam-backend:9090/v1/client' \
        --header 'Cache-Control: no-cache' \
        --header 'Content-Type: application/json' \
        --data "{
          \"name\":\"${IAM_USERNAME}\",
          \"password\":\"${IAM_PASSWORD}\"
      }")
      if [ $? -ne 0 ]; then
          echo "new IAM client registration failed"
          exit 1
      fi
      newClientID=$(echo $response | jq .data.id)
      if [ "${newClientID}" != "${IAM_CLIENT_ID}" ]; then
          echo "new IAM client ID \"${newClientID}\" is not equal to \"${IAM_CLIENT_ID}\". manually updating client ID in DB..."
          export PGPASSWORD=${POSTGRESQL_APPLICATION_USER_PASSWORD}
          psql -h ${POSTGRESQL_HOST} --port=${POSTGRESQL_PORT} -U ${POSTGRESQL_APPLICATION_USER} -d adherence -AXqtc "
            update iam_caccess set id='${IAM_CLIENT_ID}' where id='${newClientID}';
          "
            if [ $? -ne 0 ]; then
                echo "client ID update failed."
                exit 1
            fi
            echo "client ID updated in iam_caccess table"
      fi
      echo "new IAM client registered"
  else
      echo "IAM client already exist. skipping client registration."
  fi
}

register_ins_client() {
  curl --location --silent --fail --request GET 'http://ins:9100/v1/client' \
    --header "Content-Type: application/json" \
    --header "INS-Client-Id: ${INS_CLIENT_ID}" > /dev/null
  if [ $? -ne 0 ]; then
      echo "GET INS client failed. registering new client..."
      response=$(curl --location --silent --fail --request POST --request POST \
        --url 'http://ins:9100/v1/client' \
        --header 'Cache-Control: no-cache' \
        --header 'Content-Type: application/json' \
        --data "{
          \"name\":\"${INS_USERNAME}\",
          \"password\":\"${INS_PASSWORD}\"
      }")
      if [ $? -ne 0 ]; then
          echo "new INS client registration failed"
          exit 1
      fi
      newClientID=$(echo $response | jq .data.id)
      if [ "${newClientID}" != "${INS_CLIENT_ID}" ]; then
          echo "new INS client ID \"${newClientID}\" is not equal to \"${INS_CLIENT_ID}\". manually updating client ID in DB..."
          export PGPASSWORD=${POSTGRESQL_APPLICATION_USER_PASSWORD}
          psql -h ${POSTGRESQL_HOST} --port=${POSTGRESQL_PORT} -U ${POSTGRESQL_APPLICATION_USER} -d ins -AXqtc "
            update ins_client set id='${INS_CLIENT_ID}' where id='${newClientID}';
          "
            if [ $? -ne 0 ]; then
                echo "client ID update failed."
                exit 1
            fi
            echo "client ID updated in ins_client table"
      fi
      echo "new INS client registered"
  else
      echo "INS client already exist. skipping client registration."
  fi
}

register_episode_client() {
  export PGPASSWORD=${POSTGRESQL_APPLICATION_USER_PASSWORD}
  clientTableExist=$(psql -h ${POSTGRESQL_HOST} --port=${POSTGRESQL_PORT} -U ${POSTGRESQL_APPLICATION_USER} -d episode -AXqtc "select exists(select * from information_schema.tables where table_schema = 'public' and table_name = 'client');")
  if [ $? -ne 0 ]; then
    exit 1
  fi
  if [ "$clientTableExist" = "f" ]; then
    echo "client table is missing in the DB. creating client table."
    psql -h ${POSTGRESQL_HOST} --port=${POSTGRESQL_PORT} -U ${POSTGRESQL_APPLICATION_USER} -d episode -AXqtc "
      CREATE TABLE public.client (
      id bigserial NOT NULL,
      event_flow_id int8 NULL,
      created_date timestamp NULL,
      \"name\" varchar(255) NULL,
      \"password\" varchar(255) NULL,
      CONSTRAINT client_pkey PRIMARY KEY (id)
    );"
      if [ $? -ne 0 ]; then
          echo "client table creation failed"
          exit 1
      fi
      echo "created client table"
  else
      echo "client table already exists in episode db"
  fi

  clientRowExist=$(psql -h ${POSTGRESQL_HOST} --port=${POSTGRESQL_PORT} -U ${POSTGRESQL_APPLICATION_USER} -d episode -AXqtc "select exists(select * from public.client where id = '${EPISODE_CLIENT_ID}' );")
  if [ $? -ne 0 ]; then
    exit 1
  fi
  if [ "$clientRowExist" = "f" ]; then
    # todo password hash
    psql -h ${POSTGRESQL_HOST} --port=${POSTGRESQL_PORT} -U ${POSTGRESQL_APPLICATION_USER} -d episode -AXqtc "
      INSERT INTO public.client (id, event_flow_id,created_date,\"name\",\"password\") VALUES (${EPISODE_CLIENT_ID}, 1,current_timestamp,'${EPISODE_USERNAME}', '${EPISODE_PASSWORD}');
    "
    if [ $? -ne 0 ]; then
          echo "insert failed"
          exit 1
      fi
    echo "client row inserted"
  else
      echo "client row already exists in the client table"
  fi

}

register_user_service_client() {
  curl --location --silent --fail --request GET 'http://user-service:9099/v1/client' \
      --header "Content-Type: application/json" \
      --header "X-US-Client-Id: ${INS_CLIENT_ID}" > /dev/null
    if [ $? -ne 0 ]; then
        echo "GET User Service client failed. registering new client..."
        response=$(curl --location --silent --fail --request POST --request POST \
          --url 'http://user-service:9099/v1/client' \
          --header 'Cache-Control: no-cache' \
          --header 'Content-Type: application/json' \
          --data "{
            \"name\":\"${INS_USERNAME}\",
            \"password\":\"${INS_PASSWORD}\"
        }")
        if [ $? -ne 0 ]; then
            echo "new User Service client registration failed"
            exit 1
        fi
        newClientID=$(echo $response | jq .data.id)
        if [ "${newClientID}" != "${INS_CLIENT_ID}" ]; then
            echo "new User Service client ID \"${newClientID}\" is not equal to \"${INS_CLIENT_ID}\". manually updating client ID in DB..."
            export PGPASSWORD=${POSTGRESQL_APPLICATION_USER_PASSWORD}
            psql -h ${POSTGRESQL_HOST} --port=${POSTGRESQL_PORT} -U ${POSTGRESQL_APPLICATION_USER} -d userservice -AXqtc "
              update user_client set id='${INS_CLIENT_ID}' where id='${newClientID}';
            "
              if [ $? -ne 0 ]; then
                  echo "client ID update failed."
                  exit 1
              fi
              echo "client ID updated in user_client table"
        fi
        echo "new User Service client registered"
    else
        echo "User Service client already exist. skipping client registration."
    fi
}

# todo add package versions
apk add postgresql-client curl jq

register_datagateway_client
register_iam_client
register_ins_client
register_episode_client
register_user_service_client

#!/bin/bash

curl -XDELETE "http://elastic-search:9200/episode" -u ${ELASTIC_USERNAME}:${ELASTIC_PASSWORD} -s -o /dev/null
if [ $? -eq 0 ]; then
  echo "Deletion acknowledged"
  curl -XPUT http://elastic-search:9200/episode -u ${ELASTIC_USERNAME}:${ELASTIC_PASSWORD} -H "Content-Type: application/json" -d '
    {
    "mappings": {
        "properties": {
          "_class": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "addedBy": {
            "type": "long"
          },
          "associations": {
            "properties": {
              "CareContextCreated": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "artNumber": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "beneficiaryId": {
                "type": "long"
              },
              "deploymentCode": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "diagnosisBasis": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "externalId1": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "externalId3": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "notificationTestId": {
                "type": "long"
              },
              "preARTNumber": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "weightBand": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              }
            }
          },
          "clientId": {
            "type": "long"
          },
          "createdDate": {
            "type": "date",
            "format": "yyyy-MM-dd HH:mm:ss"
          },
          "currentStageId": {
            "type": "long"
          },
          "currentTags": {
            "type": "text"
          },
          "deleted": {
            "type": "boolean"
          },
          "disease": {
            "type": "text"
          },
          "diseaseId": {
            "type": "long"
          },
          "diseaseIdOptions": {
            "type": "text",
            "analyzer": "pattern"
          },
          "endDate": {
            "type": "date",
            "format": "yyyy-MM-dd HH:mm:ss"
          },
          "hierarchyMappings": {
            "properties": {
              "art": {
                "type": "long"
              },
              "art_all": {
                "type": "long"
              },
              "current": {
                "type": "long"
              },
              "current_all": {
                "type": "long"
              },
              "diagnosed": {
                "type": "long"
              },
              "diagnosed_all": {
                "type": "long"
              },
              "drtb": {
                "type": "long"
              },
              "drtb_all": {
                "type": "long"
              },
              "enrollment": {
                "type": "long"
              },
              "enrollment_all": {
                "type": "long"
              },
              "initiation": {
                "type": "long"
              },
              "initiation_all": {
                "type": "long"
              },
              "outcome": {
                "type": "long"
              },
              "outcome_all": {
                "type": "long"
              },
              "privateFacility": {
                "type": "long"
              },
              "privateFacility_all": {
                "type": "long"
              },
              "pvtHub": {
                "type": "long"
              },
              "pvtHub_all": {
                "type": "long"
              },
              "pvthub": {
                "type": "long"
              },
              "pvthub_all": {
                "type": "long"
              },
              "residence": {
                "type": "long"
              },
              "residence_all": {
                "type": "long"
              }
            }
          },
          "id": {
            "type": "long"
          },
          "lastActivityDate": {
            "type": "date",
            "format": "yyyy-MM-dd HH:mm:ss"
          },
          "personId": {
            "type": "long"
          },
          "riskStatus": {
            "type": "keyword"
          },
          "stageData": {
            "properties": {
              "addedBy": {
                "type": "long"
              },
              "addedByType": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "address": {
                "type": "text"
              },
              "adherenceDispensationRelation": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "adherenceString": {
                "type": "text"
              },
              "age": {
                "type": "long"
              },
              "area": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "asthmaSubscriptionStartDate": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "callCenterCalls": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "contactPersonAddress": {
                "type": "text"
              },
              "contactPersonName": {
                "type": "text"
              },
              "contactPersonPhone": {
                "type": "text"
              },
              "currentSchedule": {
                "type": "keyword"
              },
              "dataConsent": {
                "type": "boolean"
              },
              "dateOfBirth": {
                "type": "long"
              },
              "diabetesStatus": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "diagnosisBasis": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "diagnosisDate": {
                "type": "date",
                "format": "yyyy-MM-dd HH:mm:ss"
              },
              "diseaseClassification": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "drugResistance": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "duplicateOf": {
                "type": "long"
              },
              "duplicateStatus": {
                "type": "keyword",
                "null_value": "_null_"
              },
              "emailList": {
                "properties": {
                  "createdAt": {
                    "type": "long"
                  },
                  "emailId": {
                    "type": "text",
                    "fields": {
                      "keyword": {
                        "type": "keyword",
                        "ignore_above": 256
                      }
                    }
                  },
                  "id": {
                    "type": "long"
                  },
                  "primary": {
                    "type": "boolean"
                  },
                  "updatedAt": {
                    "type": "long"
                  },
                  "userId": {
                    "type": "long"
                  },
                  "verified": {
                    "type": "boolean"
                  }
                }
              },
              "endDate": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "endOfIp": {
                "type": "date"
              },
              "engagementExists": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "epSite": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "externalIds": {
                "properties": {
                  "type": {
                    "type": "text",
                    "fields": {
                      "keyword": {
                        "type": "keyword",
                        "ignore_above": 256
                      }
                    }
                  },
                  "value": {
                    "type": "text",
                    "fields": {
                      "keyword": {
                        "type": "keyword",
                        "ignore_above": 256
                      }
                    }
                  }
                }
              },
              "fatherName": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "fathersName": {
                "type": "text"
              },
              "firstName": {
                "type": "text"
              },
              "foregoBenefits": {
                "type": "boolean"
              },
              "gender": {
                "type": "keyword"
              },
              "height": {
                "type": "long"
              },
              "hivTestStatus": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "householdVisits": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "iamStartDate": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "isDuplicate": {
                "type": "boolean"
              },
              "isIAMEnabled": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "keyPopulation": {
                "type": "text"
              },
              "landmark": {
                "type": "text"
              },
              "language": {
                "type": "text"
              },
              "lastDosage": {
                "type": "date",
                "format": "yyyy-MM-dd HH:mm:ss"
              },
              "lastMissedDosage": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "lastName": {
                "type": "text"
              },
              "lastRefillDate": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "maritalStatus": {
                "type": "keyword"
              },
              "mermId": {
                "type": "text"
              },
              "mobileList": {
                "properties": {
                  "createdAt": {
                    "type": "long"
                  },
                  "id": {
                    "type": "long"
                  },
                  "number": {
                    "type": "text",
                    "fields": {
                      "keyword": {
                        "type": "keyword",
                        "ignore_above": 256
                      }
                    }
                  },
                  "owner": {
                    "type": "text",
                    "fields": {
                      "keyword": {
                        "type": "keyword",
                        "ignore_above": 256
                      }
                    }
                  },
                  "primary": {
                    "type": "boolean"
                  },
                  "stoppedAt": {
                    "type": "long"
                  },
                  "updatedAt": {
                    "type": "long"
                  },
                  "userId": {
                    "type": "long"
                  },
                  "verified": {
                    "type": "boolean"
                  }
                }
              },
              "monitoringMethod": {
                "type": "keyword"
              },
              "name": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "notificationTestId": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "numberOfDaysOfMedication": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "occupation": {
                "type": "text"
              },
              "patientTreatmentHistory": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "pincode": {
                "type": "text"
              },
              "primaryPhone": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "primaryPhoneNumber": {
                "type": "text"
              },
              "refillMonitoring": {
                "type": "boolean"
              },
              "regimenType": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "remarksOutcome": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "siteOfDisease": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "socioEconomicStatus": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "socioeconomicStatus": {
                "type": "keyword"
              },
              "symptom": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "tBCategory": {
                "type": "keyword"
              },
              "taluka": {
                "type": "text"
              },
              "tbCategory": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "town": {
                "type": "text"
              },
              "transferId": {
                "type": "long"
              },
              "treatmentOutcome": {
                "type": "keyword"
              },
              "treatmentPhase": {
                "type": "text"
              },
              "treatmentPlanId": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "treatmentStartDate": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "treatmentStartTimeStamp": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "typeOfCase": {
                "type": "keyword"
              },
              "typeOfCaseFinding": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              },
              "typeOfDOT": {
                "type": "keyword"
              },
              "verifications": {
                "properties": {
                  "entityType": {
                    "type": "text",
                    "fields": {
                      "keyword": {
                        "type": "keyword",
                        "ignore_above": 256
                      }
                    }
                  },
                  "entityValue": {
                    "type": "text",
                    "fields": {
                      "keyword": {
                        "type": "keyword",
                        "ignore_above": 256
                      }
                    }
                  },
                  "type": {
                    "type": "text",
                    "fields": {
                      "keyword": {
                        "type": "keyword",
                        "ignore_above": 256
                      }
                    }
                  }
                }
              },
              "ward": {
                "type": "text"
              },
              "weight": {
                "type": "long"
              },
              "weightBand": {
                "type": "text",
                "fields": {
                  "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                  }
                }
              }
            }
          },
          "stageKey": {
            "type": "keyword"
          },
          "stageName": {
            "type": "text"
          },
          "startDate": {
            "type": "date",
            "format": "yyyy-MM-dd HH:mm:ss"
          },
          "stickyTags": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "tags": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "typeOfEpisode": {
            "type": "keyword"
          }
        }
      }
  }' -s -o /dev/null
  if [ $? -eq 0 ]; then
    echo "Mappings updated successfully."
  else
    echo "Mapping updation failed."
  fi
else
  echo "Deletion failed."
fi

# Source: data-gateway/templates/deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: data-gateway
spec:
  replicas: 1
  selector:
    matchLabels:
      app.kubernetes.io/name: data-gateway
      app.kubernetes.io/instance: data-gateway
  template:
    metadata:
      labels:
        app.kubernetes.io/name: data-gateway
        app.kubernetes.io/instance: data-gateway
    spec:
      imagePullSecrets:
        - name: regcred
      serviceAccountName: data-gateway
      securityContext:
        {}
      initContainers:
        - name: wait-for-postgres
          image: docker.io/bitnami/postgresql:10.20.0-debian-10-r93
          command:
          - /bin/sh
          - -c
          - >
            timeout 120 /bin/sh -c -- \
            'while pg_isready -U postgres -d postgres -h postgres -p 5432 --timeout=10; exitCode=$? ; [ $exitCode -ne 0 ];  
            do 
                echo "not ready"; 
                sleep 10s; 
            done'
        - name: wait-for-redis
          image: docker.io/bitnami/redis:6.2.7-debian-11-r0
          env:
            - name: REDISCLI_AUTH
              valueFrom:
                secretKeyRef:
                  key: redis-password
                  name: redis
          command:
          - /bin/sh
          - -c
          - > 
            timeout 120 /bin/sh -c -- \
              'while response=$(redis-cli -h redis ping); [ "$response" != "PONG" ];  
              do 
                  echo "not ready"; 
                  sleep 10s; 
              done'
        - name: wait-for-rabbitmq
          image: subfuzion/netcat
          command:
          - /bin/sh
          - -c
          - >
            timeout -t 120 /bin/sh -c -- \
            'while nc -w 3 -v -z rabbitmq 5672; exitCode=$? ; [ $exitCode -ne 0 ];  
            do 
                echo "not ready"; 
                sleep 10s; 
            done'
        - name: wait-for-iambackend
          image: alpine/curl:3.14
          command:
          - /bin/sh
          - -c
          - >
            timeout 120 sh -c \
              'curl -f http://iam-backend:9090/actuator/health; exitCode=$? ; while [ $exitCode -ne 0 ];  
              do 
                  echo "not ready"; 
                  sleep 10s; 
              done'
        - name: wait-for-ins
          image: alpine/curl:3.14
          command:
          - /bin/sh
          - -c
          - >
            timeout 120 sh -c \
              'curl -f http://ins:9100/actuator/health; exitCode=$? ; while [ $exitCode -ne 0 ];  
              do 
                  echo "not ready"; 
                  sleep 10s; 
              done'
      volumes:
        - emptyDir: {}
          name: tmpfs
      containers:
        - name: data-gateway
          securityContext:
            readOnlyRootFilesystem: true
          image: "registry.gitlab.com/everwell/hub-foss/data-gateway:2023-03-09T1307110530-master"
          imagePullPolicy: Always
          ports:
            - name: http
              containerPort: 8080
              protocol: TCP
          volumeMounts:
          - name: tmpfs
            mountPath: /tmp
          readinessProbe:
            httpGet:
              port: 8080
              path: /test-connection
            initialDelaySeconds: 5
            periodSeconds: 10
          livenessProbe:
            httpGet:
              port: 8080
              path: /test-connection
            initialDelaySeconds: 15
            periodSeconds: 20
          resources:
            {}
          env:
            - name: POSTGRESQL_HOST
              value: postgres
            - name: POSTGRESQL_PORT
              value: "5432"
            - name: POSTGRESQL_DB
              value: datagateway
            - name: POSTGRESQL_APPLICATION_USER
              valueFrom:
                secretKeyRef:
                  key: username
                  name: pgsql-appuser
            - name: POSTGRESQL_APPLICATION_USER_PASSWORD
              valueFrom:
                secretKeyRef:
                  key: password
                  name: pgsql-appuser
            - name: RABBITMQ_USERNAME
              value: user
            - name: RABBITMQ_PORT
              value: "5672"
            - name: RABBITMQ_HOST
              value: rabbitmq
            - name: RABBITMQ_PASSWORD
              valueFrom:
                secretKeyRef:
                  key: rabbitmq-password
                  name: rabbitmq
            - name: REDIS_PASSWORD
              valueFrom:
                secretKeyRef:
                  key: redis-password
                  name: redis
            - name: REDIS_PORT
              value: "6379"
            - name: REDIS_HOST
              value: redis
            - name: JWT_SECRET
              valueFrom:
                secretKeyRef:
                  key: secret
                  name: jwt-secret
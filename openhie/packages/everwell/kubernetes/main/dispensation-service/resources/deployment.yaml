apiVersion: apps/v1
kind: Deployment
metadata:
  name: dispensation-service
spec:
  replicas: 1
  selector:
    matchLabels:
      app.kubernetes.io/name: dispensation-service
      app.kubernetes.io/instance: dispensation-service
  template:
    metadata:
      labels:
        app.kubernetes.io/name: dispensation-service
        app.kubernetes.io/instance: dispensation-service
    spec:
      imagePullSecrets:
        - name: regcred
      serviceAccountName: dispensation-service
      securityContext:
        {}
      initContainers:
        - name: wait-for-postgres
          image: docker.io/bitnami/postgresql:10.20.0-debian-10-r93
          command:
          - /bin/sh
          - -c
          - >
            timeout 120 /bin/sh -c -- \
            'while pg_isready -U postgres -d postgres -h postgres -p 5432 --timeout=10; exitCode=$? ; [ $exitCode -ne 0 ];  
            do 
                echo "not ready"; 
                sleep 10s; 
            done'
        - name: wait-for-redis
          image: docker.io/bitnami/redis:6.2.7-debian-11-r0
          env:
            - name: REDISCLI_AUTH
              valueFrom:
                secretKeyRef:
                  key: redis-password
                  name: redis
          command:
          - /bin/sh
          - -c
          - > 
            timeout 120 sh -c -- \
              'while response=$(redis-cli -h redis ping); [ "$response" != "PONG" ];  
              do 
                  echo "not ready"; 
                  sleep 10s; 
              done'
        - name: wait-for-rabbitmq
          image: subfuzion/netcat
          command:
          - /bin/sh
          - -c
          - >
            timeout -t 120 sh -c -- \
            'while nc -w 3 -v -z rabbitmq 5672; exitCode=$? ; [ $exitCode -ne 0 ];  
            do 
                echo "not ready"; 
                sleep 10s; 
            done'
        - name: wait-for-elasticsearch
          image: subfuzion/netcat
          command:
          - /bin/sh
          - -c
          - >
            timeout -t 120 sh -c -- \
            'while nc -w 3 -v -z elastic-search 9200; exitCode=$? ; [ $exitCode -ne 0 ];  
            do 
                echo "not ready"; 
                sleep 10s; 
            done'
      volumes:
        - emptyDir: {}
          name: tmpfs
      containers:
        - name: dispensation-service
          securityContext:
            readOnlyRootFilesystem: true
          image: "registry.gitlab.com/everwell/hub-foss/dispensation-service:2023-03-09T1307110530-master"
          imagePullPolicy: Always
          ports:
            - name: http
              containerPort: 9080
              protocol: TCP
          volumeMounts:
            - name: tmpfs
              mountPath: /tmp
          readinessProbe:
            httpGet:
              port: 9080
              path: /actuator/health
            initialDelaySeconds: 5
            periodSeconds: 10
          livenessProbe:
            httpGet:
              port: 9080
              path: /actuator/health
            initialDelaySeconds: 15
            periodSeconds: 20
          resources:
            {}
          env:
            - name: POSTGRESQL_HOST
              value: postgres
            - name: POSTGRESQL_PORT
              value: "5432"
            - name: POSTGRESQL_DB
              value: dispensation
            - name: POSTGRESQL_APPLICATION_USER
              valueFrom:
                secretKeyRef:
                  key: username
                  name: pgsql-appuser
            - name: POSTGRESQL_APPLICATION_USER_PASSWORD
              valueFrom:
                secretKeyRef:
                  key: password
                  name: pgsql-appuser
            - name: RABBITMQ_USERNAME
              value: user
            - name: RABBITMQ_PORT
              value: "5672"
            - name: RABBITMQ_HOST
              value: rabbitmq
            - name: RABBITMQ_PASSWORD
              valueFrom:
                secretKeyRef:
                  key: rabbitmq-password
                  name: rabbitmq
            - name: REDIS_PASSWORD
              valueFrom:
                secretKeyRef:
                  key: redis-password
                  name: redis
            - name: REDIS_PORT
              value: "6379"
            - name: REDIS_HOST
              value: redis
            - name: ELASTIC_HOST
              value: elastic-search
            - name: ELASTIC_PROTOCOL
              value: http
            - name: ELASTIC_PORT
              value: "9200"
            - name: ELASTIC_USERNAME
              valueFrom:
                secretKeyRef:
                  key: elastic-user
                  name: elasticsearch
            - name: ELASTIC_PASSWORD
              valueFrom:
                secretKeyRef:
                  key: elastic-password
                  name: elasticsearch
